<?
header("Access-Control-Allow-Origin: *");
header("Access-Control-Allow-Methods: GET, POST");
header("Connection:keep-alive");
header("Content-Type: application/json; charset=utf-8");
include ($_SERVER["DOCUMENT_ROOT"]. "/bitrix/modules/main/include/prolog_before.php");

$mess="";
$mess='<div class="success">
	<div class="ttl-p2">Вы успешно заказали обратный<br/>звонок!</div>
	<p class="dsc">Наши менеджеры свяжутся с вами в ближайшее время.</p>
</div>
';	

$messages=array(
	"ru"=>array(
		"enter_phone"=>"Укажите Ваш телефон",
		"wrong_phone"=>"Некорректный телефон",
		"db_error"=>"Не верный ввод данных, пожалуйста, повторите ввод!",
		"success"=>$mess,
	),
	"en"=>array(
		"enter_phone"=>"Enter your phone",
		"wrong_phone"=>"Phone is wrong",
		"db_error"=>"An error has occurred when sending the request. ",
		"success"=>	"Request sent successfully",
	)
);

$error=array();

if(!$_REQUEST["lang"])$_REQUEST["lang"]="ru";

if($_REQUEST["subj"]){
	$error[]=$messages[$_REQUEST["lang"]]["db_error"];
}else{
	if(!$_REQUEST["phone"]) $error["phone"]=$messages[$_REQUEST["lang"]]["enter_phone"];
	elseif(!preg_match("/^\+7\(\d{3}\)\d{3}\-\d{2}\-\d{2}$/",$_REQUEST["phone"])) $error["phone"]=$messages[$_REQUEST["lang"]]["wrong_phone"];
}
if(!$error)
{
	global $arGeoData;
	$arEventFields = array(
		"PHONE" => $_REQUEST['PHONE'],
		"CITY" => $arGeoData['CUR_CITY']['NAME'],
		"REGION" => $arGeoData['CUR_CITY']['REGION'],
	);
	if (strlen($arEventFields["PHONE"]) > 0) {
		CEvent::SendImmediate("CALLBACK_FORM", 's1', $arEventFields);
	}

	$arEventFields["CITY_ID"]=$arGeoData['CUR_CITY']['ID'];

	SendDataToExternalServer("CALLBACK",$arEventFields);
}

if(count($error)==0){
	echo json_encode(array("status"=>"ok","message"=>$messages[$_REQUEST["lang"]]["success"]));
}
else{
	echo json_encode(array("status"=>"error","message"=>'<div class="error">'.$messages[$_REQUEST["lang"]]["db_error"].'</div>',"error"=>$error));
}
?>