<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Бренд");
?>


<!-- cat-brands -->
<div class="cat-brands roll c">
	<h1 class="ttl1">О товарах бренда Ballu</h1>

	<!-- col-l -->
	<div class="col-l">
		<!-- v-gr -->
		<div class="v-gr c">
			<ul class="brand-list">
				<li><a href="#">Ad Hoc</a></li>
				<li><a href="#">Aerial</a></li>
				<li><a href="#">AN-TRAX IT</a></li>
				<li><a href="#">Atoll</a></li>
				<li><a href="#">Ballu</a></li>
				<li><a href="#">Ballu Machine</a></li>
				<li><a href="#">Ballu-Biemmedue</a></li>
				<li><a href="#">Barbi</a></li>
				<li><a href="#">Berke</a></li>
				<li><a href="#">Biasi</a></li>
				<li><a href="#">Big Foot Systems</a></li>
				<li><a href="#">BONECO Air-O-Swiss</a></li>
				<li><a href="#">Danfoss</a></li>
				<li><a href="#">De Dietrich</a></li>
				<li><a href="#">Delongi Professional</a></li>
				<li><a href="#">Dia Norm</a></li>
				<li><a href="#">Diaflex</a></li>
				<li><a href="#">Electrolux</a></li>
				<li><a href="#">Energoflex</a></li>
				<li><a href="#">ESBE</a></li>
				<li><a href="#">Grota</a></li>
				<li><a href="#">Grundfos</a></li>
				<li><a href="#">Gruner</a></li>
				<li><a href="#">Hitachi</a></li>
				<li><a href="#">Honeywell</a></li>
				<li><a href="#">HygroMatik</a></li>
				<li><a href="#">Magnetic Water Systems</a></li>
			</ul>
			<ul class="brand-list">
				<li><a href="#">Meibes</a></li>
				<li><a href="#">Mini B</a></li>
				<li><a href="#">Mitsubishi Electric</a></li>
				<li><a href="#">Mungo</a></li>
				<li><a href="#">Nibe</a></li>
				<li><a href="#">Noirot</a></li>
				<li><a href="#">Orkli</a></li>
				<li><a href="#">Reflex</a></li>
				<li><a href="#">Rhoss</a></li>
				<li><a href="#">Ridea</a></li>
				<li><a href="#">RoyalThermo</a></li>
				<li><a href="#">SFA</a></li>
				<li><a href="#">Shuft</a></li>
				<li><a href="#">TECEflex</a></li>
				<li><a href="#">Thermaflex</a></li>
				<li><a href="#">Uniflair</a></li>
				<li><a href="#">USFilter - Pentek</a></li>
				<li><a href="#">Vortice</a></li>
				<li><a href="#">Zanussi</a></li>
				<li><a href="#">АэроБлок</a></li>
				<li><a href="#">АэроБлок Контроль</a></li>
				<li><a href="#">Вектор</a></li>
				<li><a href="#">Гелис</a></li>
				<li><a href="#">Джилекс</a></li>
				<li><a href="#">Охта</a></li>
				<li><a href="#">Эван</a></li>
			</ul>
		</div>
		<!-- / v-gr end -->
		
		<!-- v-gr -->
		<div class="v-gr">
			<a href="card.html" class="pic-offer" style="background-image:url('<?= SITE_TEMPLATE_PATH ?>/i/blocks/offer356x320.png');"></a>
		</div>
		<!-- / v-gr end -->
		
	
		<!-- v-gr -->
		<div class="v-gr">
			<h2 class="ttl2">Требуется помощь?</h2>

			<a class="item b-hlp callback-pop" href="#">
				<div class="w">
					<div class="ttl">Закажите звонок!</div>
					<p class="dsc">Удобная функция заказа<br/>обратного звонка.</p>
					<div class="ico ico-1">&nbsp;</div>
				</div>
			</a>
			<a class="item b-hlp" href="#">
				<div class="w">
					<div class="ttl">+7 495 777-19-77</div>
					<p class="dsc">Помогаем по любым<br/>вопросам продажи и сервиса.</p>
					<div class="ico ico-2">&nbsp;</div>
				</div>
			</a>
		</div>
		<!-- / v-gr end -->
		
		<!-- v-gr -->
		<div class="v-gr">
			<h2 class="ttl2">Горячее предложение</h2>
			<!-- item w/timer -->
			<div class="item b-base">
				<div class="w c">
				
					<div class="timer">
						<div class="sum">Успей сэкономить 80 рублей</div>
						<div class="time" data-countdown="2015/08/08">
							<!-- <b>11</b> <span>дн.</span> <b>11</b> <span>ч.</span> <b>11</b> <span>мин.</span> <b class="sec">11</b> <span>сек.</span> -->
						</div>
					</div>

					<a class="lnk" href="card.html">
						<div class="ttl">Инверторная сплит система система система система</div>
						<div class="pic" style="background-image:url('<?= SITE_TEMPLATE_PATH ?>/i/blocks/hot1.png');">
							<div class="ico-hot">Успей<br/>купить</div>
						</div>
						<div class="dsc">Electrolux EACS/I - 13 HO/N3 серии Orlando DC Inverter система</div>
					</a>

					<div class="prc">
						<div class="curr">18 990 руб.</div>
						<div class="past">
							<span class="old">89 490</span> <span class="profit">1 666 выгода</span>
						</div>
					</div>
					<div class="btn btn-to-bsk">В корзину</div>

				</div>
			</div>
			<!-- / item w/timer -->
		</div>
		<!-- / v-gr end -->
		
			
	</div>
	<!-- / col-l end-->
	
	<!-- col-r -->
	<div class="col-r">
		<div class="cat-brand-logo" id="thisIsTop" style="background-image:url('<?= SITE_TEMPLATE_PATH ?>/i/blocks/cat-brand-logo.png');"></div>
	
		<!-- cat-type-in -->	
		<div class="cat-type-in">
			<h2 class="ttl2 c"><span>Кондиционирование</span> <div style="background-image:url('<?= SITE_TEMPLATE_PATH ?>/i/blocks/podcategor-ttl-1.png');" class="ico"></div></h2>
			
			<div class="items">
				<!-- item -->
				<div class="item c-block-l">
					<h3 class="ttl"><a href="#" class="lnk">Мобильные</a></h3>
					<ul class="list">
						<li><a href="#">Мобильные кондиционеры</a><span>от 100 руб. до 100 000 руб.</span></li>
						<li><a href="#">Сплит-системы с инверторным управлением</a><span>от 100 руб. до 100 000 руб.</span></li>
						<li><a href="#">Сплит-системы с инверторным управлением</a><span>от 100 руб. до 100 000 руб.</span></li>
						<li><a href="#">Мобильные кондиционеры</a><span>от 100 руб. до 100 000 руб.</span></li>
					</ul>
				</div>
				<!-- / item -->
				<!-- item -->
				<div class="item c-block-l">
					<h3 class="ttl"><a href="#" class="lnk">Полупромышленные кондиционеры Mitsubishi Electric</a></h3>
					<ul class="list">
						<li><a href="#">Мобильные кондиционеры</a><span>от 100 руб. до 100 000 руб.</span></li>
						<li><a href="#">Сплит-системы</a><span>от 100 руб. до 100 000 руб.</span></li>
						<li><a href="#">Сплит-системы с инверторным управлением</a><span>от 100 руб. до 100 000 руб.</span></li>
						<li><a href="#">Мульти сплит-системы с инверторным управлением</a><span>от 100 руб. до 100 000 руб.</span></li>
						<li><a href="#">Полупромышленные кондиционеры Mitsubishi Electric серии MR.Slim</a><span>от 100 руб. до 100 000 руб.</span></li>
						<li><a href="#">Пульты управления для кондиционеров</a><span>от 100 руб. до 100 000 руб.</span></li>
					</ul>
				</div>
				<!-- / item -->
				<!-- item -->
				<div class="item c-block-l">
					
					<h3 class="ttl"><a href="#" class="lnk">Кондиционеры</a></h3>
					<ul class="list">
						<li><a href="#">Мобильные кондиционеры</a><span>от 100 руб. до 100 000 руб.</span></li>
						<li><a href="#">Сплит-системы</a><span>от 100 руб. до 100 000 руб.</span></li>
						<li><a href="#">Сплит-системы с инверторным управлением</a><span>от 100 руб. до 100 000 руб.</span></li>
						<li><a href="#">Мульти сплит-системы с инверторным управлением</a><span>от 100 руб. до 100 000 руб.</span></li>
						<li><a href="#">Кассетные сплит системы</a><span>от 100 руб. до 100 000 руб.</span></li>
						<li><a href="#">Канальные сплит системы</a><span>от 100 руб. до 100 000 руб.</span></li>
						<li><a href="#">Универсальные внешние блоки</a><span>от 100 руб. до 100 000 руб.</span></li>
						<li><a href="#">Полупромышленные кондиционеры Mitsubishi Electric серии MR.Slim</a><span>от 100 руб. до 100 000 руб.</span></li>
						<li><a href="#">Пульты управления для кондиционеров</a><span>от 100 руб. до 100 000 руб.</span></li>
					</ul>
				</div>
				<!-- / item -->
			</div>
			<!-- / items end -->	
			<p class="to-top"><a href="#thisIsTop">Наверх</a></p>
		</div>
		<!-- / cat-type-in end -->	
		
		<!-- cat-type-in -->	
		<div class="cat-type-in">
			<h2 class="ttl2 c"><span>Отопление</span> <div style="background-image:url('<?= SITE_TEMPLATE_PATH ?>/i/blocks/podcategor-ttl-2.png');" class="ico"></div></h2>

			<div class="items">
				<!-- item -->
				<div class="item c-block-l">
					<h3 class="ttl"><a href="#" class="lnk">Сплит-системы</a></h3>
					<ul class="list">
						<li><a href="#">Мобильные кондиционеры</a><span>от 100 руб. до 100 000 руб.</span></li>
						<li><a href="#">Сплит-системы</a><span>от 100 руб. до 100 000 руб.</span></li>
						<li><a href="#">Сплит-системы с инверторным управлением</a><span>от 100 руб. до 100 000 руб.</span></li>
						<li><a href="#">Мульти сплит-системы с инверторным управлением</a><span>от 100 руб. до 100 000 руб.</span></li>
						<li><a href="#">Полупромышленные кондиционеры Mitsubishi Electric серии MR.Slim</a><span>от 100 руб. до 100 000 руб.</span></li>
						<li><a href="#">Пульты управления для кондиционеров</a><span>от 100 руб. до 100 000 руб.</span></li>
					</ul>
				</div>
				<!-- / item -->
				<!-- item -->
				<div class="item c-block-l">
					<h3 class="ttl"><a href="#" class="lnk">Пульты</a></h3>
					<ul class="list">
						<li><a href="#">Мобильные кондиционеры</a><span>от 100 руб. до 100 000 руб.</span></li>
						<li><a href="#">Сплит-системы</a><span>от 100 руб. до 100 000 руб.</span></li>
						<li><a href="#">Сплит-системы с инверторным управлением</a><span>от 100 руб. до 100 000 руб.</span></li>
						<li><a href="#">Полупромышленные кондиционеры Mitsubishi Electric серии MR.Slim</a><span>от 100 руб. до 100 000 руб.</span></li>
						<li><a href="#">Пульты управления для кондиционеров</a><span>от 100 руб. до 100 000 руб.</span></li>
					</ul>
				</div>
				<!-- / item -->
				<!-- item -->
				<div class="item c-block-l">
					<h3 class="ttl"><a href="#" class="lnk">Пульты Мобильные</a></h3>
					<ul class="list">
						<li><a href="#">Мобильные кондиционеры</a><span>от 100 руб. до 100 000 руб.</span></li>
						<li><a href="#">Сплит-системы с инверторным управлением</a><span>от 100 руб. до 100 000 руб.</span></li>
						<li><a href="#">Мобильные кондиционеры</a><span>от 100 руб. до 100 000 руб.</span></li>
						<li><a href="#">Сплит-системы с инверторным управлением</a><span>от 100 руб. до 100 000 руб.</span></li>
					</ul>
				</div>
				<!-- / item -->
			</div>
			<!-- / items end -->	
			<p class="to-top"><a href="#thisIsTop">Наверх</a></p>
		</div>
		<!-- / cat-type-in end -->	
		
		<div class="intro txt-block">
			<p>В 2009 году Ballu Industrial Group расширила ассортимент выпускаемой продукции и представляет новую линию профессионального климатического оборудования Ballu Machine.</p>
			<p>Новое оборудование рассчитано на широкий круг потребителей – от частных покупателей до профессиональных монтажных и строительных организаций, удовлетворяя самым высоким требованиям.</p>
			<p>Линейка оборудования Ballu Machine обеспечивает принципиально новый подход к созданию современных систем кондиционирования зданий. Комплекс уникальных конструктивных решений выгодно отличает новое оборудование от всего, что было ранее представлено на рынке и расширяет возможности создания современных систем кондиционирования зданий.</p>
			<p>Основные преимущества – это адаптация оборудования к российскому рынку, простота и гибкость при подборе, возможность регулирования мощности уже выбранной или установленной системы и высокое качество комплектующих изделий.</p>
			<p>Благодаря использованию компонентов ведущих мировых производителей, высоким требованиям к процессу сборки, а также 100% тестированию всей продукции оборудование Ballu Machine обладает высокой надежностью и долговечностью работы. Это подтверждается расширенной гарантией 20 месяцев на всю продукцию Ballu Machine.</p>
			<p>Мы предлагаем полный модельный ряд и сбалансированную складскую программу для своих партнеров.</p>		
		</div>
		
		<div class="brand-news">
			<h2 class="ttl2">Новости по бренду</h2>
			<div class="items c">
			
				<!-- item -->
				<a href="#" class="item">
					<div class="ttl">Всё о пользе газовых инфракрасных обогревателей Ballu</div>
					<div class="date">21.03.2014</div>
				</a>
				<!-- / item -->
				<!-- item -->
				<a href="#" class="item">
					<div class="ttl">Встречайте увлажнители BALLU Hello Kitty: профессиональную серию  профессиональную серию </div>
					<div class="date">15.03.2014</div>
				</a>
				<!-- / item -->
				<!-- item -->
				<a href="#" class="item">
					<div class="ttl">Водонагреватели Electrolux Formax. Новые возможности управления комфортом!</div>
					<div class="date">02.03.2014</div>
				</a>
				<!-- / item -->
				
			</div>
			<!-- / items end -->
		</div>
		<!-- / cat-news end -->

		
		
	</div>
	<!-- / col-r end -->
</div>
<!-- / cat-brands roll end -->

<!-- hot deals block -->
<div class="roll">
	<h2 class="ttl-r">Спецпредложения</h2>
	<div class="hot-deals c">

		<!-- slides -->
		<div class="slides c" id="specoff">
	
			<!-- item w/timer -->
			<div class="item b-base">
				<div class="w c">
				
					<div class="timer">
						<div class="sum">Успей сэкономить 80 рублей</div>
						<div class="time" data-countdown="2015/02/08">
							<!-- <b>11</b> <span>дн.</span> <b>11</b> <span>ч.</span> <b>11</b> <span>мин.</span> <b class="sec">11</b> <span>сек.</span> -->
						</div>
					</div>

					<a class="lnk" href="card.html">
						<div class="ttl">Инверторная сплит система система система система</div>
						<div class="pic" style="background-image:url('<?= SITE_TEMPLATE_PATH ?>/i/blocks/hot1.png');">
							<div class="ico-hot">Успей<br/>купить</div>
						</div>
						<div class="dsc">Electrolux EACS/I - 13 HO/N3 серии Orlando DC Inverter система</div>
					</a>

					<div class="prc">
						<div class="curr">18 990 руб.</div>
						<div class="past">
							<span class="old">89 490</span> <span class="profit">1 666 выгода</span>
						</div>
					</div>
					<div class="btn btn-to-bsk">В корзину</div>

				</div>
			</div>
			<!-- / item w/timer -->
		
			<!-- item w/timer -->
			<div class="item b-base">
				<div class="w c">
				
					<div class="timer">
						<div class="sum">Успей сэкономить 1500 рублей</div>
						<div class="time" data-countdown="2015/12/08">
							<!-- <b>11</b> <span>дн.</span> <b>11</b> <span>ч.</span> <b>11</b> <span>мин.</span> <b class="sec">11</b> <span>сек.</span> -->
						</div>
					</div>

					<a class="lnk" href="card.html">
						<div class="ttl">Инверторная сплит система система система система</div>
						<div class="pic" style="background-image:url('<?= SITE_TEMPLATE_PATH ?>/i/blocks/hot2.png');">
							<div class="ico-hot">Успей<br/>купить</div>
						</div>
						<div class="dsc">Orlando DC Inverter система</div>
					</a>

					<div class="prc">
						<div class="curr">8 990 руб.</div>
						<div class="past">
							<span class="old">490</span> <span class="profit">500 выгода</span>
						</div>
					</div>
					<div class="btn btn-to-bsk">В корзину</div>

				</div>
			</div>
			<!-- / item w/timer -->
			
			<!-- item w/timer -->
			<div class="item b-base">
				<div class="w c">
				
					<div class="timer">
						<div class="sum">Успей сэкономить 100500 рублей</div>
						<div class="time" data-countdown="2015/07/08">
							<!-- <b>11</b> <span>дн.</span> <b>11</b> <span>ч.</span> <b>11</b> <span>мин.</span> <b class="sec">11</b> <span>сек.</span> -->
						</div>
					</div>

					<a class="lnk" href="card.html">
						<div class="ttl">Система система система</div>
						<div class="pic" style="background-image:url('<?= SITE_TEMPLATE_PATH ?>/i/blocks/hot3.png');">
							<div class="ico-hot">Успей<br/>купить</div>
						</div>
						<div class="dsc">Electrolux EACS/I - 13 HO/N3 серии Orlando DC Inverter система</div>
					</a>

					<div class="prc">
						<div class="curr">88 990 руб.</div>
						<div class="past">
							<span class="old">89 490</span> <span class="profit">9 500 выгода</span>
						</div>
					</div>
					<div class="btn btn-to-bsk">В корзину</div>

				</div>
			</div>
			<!-- / item w/timer -->
			
			<!-- 3 -->
			
			<!-- item w/timer -->
			<div class="item b-base">
				<div class="w c">
				
					<div class="timer">
						<div class="sum">3Успей сэкономить 80 рублей</div>
						<div class="time" data-countdown="2015/02/08">
							<!-- <b>11</b> <span>дн.</span> <b>11</b> <span>ч.</span> <b>11</b> <span>мин.</span> <b class="sec">11</b> <span>сек.</span> -->
						</div>
					</div>

					<a class="lnk" href="card.html">
						<div class="ttl">Инверторная сплит система система система система</div>
						<div class="pic" style="background-image:url('<?= SITE_TEMPLATE_PATH ?>/i/blocks/hot1.png');">
							<div class="ico-hot">Успей<br/>купить</div>
						</div>
						<div class="dsc">Electrolux EACS/I - 13 HO/N3 серии Orlando DC Inverter система</div>
					</a>

					<div class="prc">
						<div class="curr">18 990 руб.</div>
						<div class="past">
							<span class="old">89 490</span> <span class="profit">1 666 выгода</span>
						</div>
					</div>
					<div class="btn btn-to-bsk">В корзину</div>

				</div>
			</div>
			<!-- / item w/timer -->
		
			<!-- item w/timer -->
			<div class="item b-base">
				<div class="w c">
				
					<div class="timer">
						<div class="sum">4Успей сэкономить 1500 рублей</div>
						<div class="time" data-countdown="2015/12/08">
							<!-- <b>11</b> <span>дн.</span> <b>11</b> <span>ч.</span> <b>11</b> <span>мин.</span> <b class="sec">11</b> <span>сек.</span> -->
						</div>
					</div>

					<a class="lnk" href="card.html">
						<div class="ttl">Инверторная сплит система система система система</div>
						<div class="pic" style="background-image:url('<?= SITE_TEMPLATE_PATH ?>/i/blocks/hot2.png');">
							<div class="ico-hot">Успей<br/>купить</div>
						</div>
						<div class="dsc">Orlando DC Inverter система</div>
					</a>

					<div class="prc">
						<div class="curr">8 990 руб.</div>
						<div class="past">
							<span class="old">490</span> <span class="profit">500 выгода</span>
						</div>
					</div>
					<div class="btn btn-to-bsk">В корзину</div>

				</div>
			</div>
			<!-- / item w/timer -->
			
			<!-- item w/timer -->
			<div class="item b-base">
				<div class="w c">
				
					<div class="timer">
						<div class="sum">5Успей сэкономить 100500 рублей</div>
						<div class="time" data-countdown="2015/07/08">
							<!-- <b>11</b> <span>дн.</span> <b>11</b> <span>ч.</span> <b>11</b> <span>мин.</span> <b class="sec">11</b> <span>сек.</span> -->
						</div>
					</div>

					<a class="lnk" href="card.html">
						<div class="ttl">Система система система</div>
						<div class="pic" style="background-image:url('<?= SITE_TEMPLATE_PATH ?>/i/blocks/hot3.png');">
							<div class="ico-hot">Успей<br/>купить</div>
						</div>
						<div class="dsc">Electrolux EACS/I - 13 HO/N3 серии Orlando DC Inverter система</div>
					</a>

					<div class="prc">
						<div class="curr">88 990 руб.</div>
						<div class="past">
							<span class="old">89 490</span> <span class="profit">9 500 выгода</span>
						</div>
					</div>
					<div class="btn btn-to-bsk">В корзину</div>

				</div>
			</div>
			<!-- / item w/timer -->


		</div>
		<!-- / slides end -->
	</div>
	<!-- / hot deals item container end -->
</div>
<!-- / hot deals roll end -->

<!-- other deals roll -->
<div class="roll">
	<h2 class="ttl-r">популярные товары</h2>
	<!-- other deals -->
	<div class="other-deals c">
		<!-- slides -->
		<div class="slides c" id="other">
		
			<!-- item w/stamps -->
			<div class="item b-base">
				<div class="w c">
					
					<a href="card.html" class="lnk">
						<div class="ttl">Orlando сплит система система система система</div>
						<div class="pic" style="background-image:url('<?= SITE_TEMPLATE_PATH ?>/i/blocks/hot1.png');"></div>
						<div class="stamps c">
							<div class="ico ico-1">Новейшее покрытие</div>
							<div class="ico ico-2">5 лет гарантии</div>
						</div>
						<div class="dsc">Electrolux EACS/I - 13 HO/N3 серии Orlando DC Inverter система</div>
					</a>
					<div class="prc">
						<div class="curr curr-only">17 234 руб.</div>
					</div>
					<div class="btn btn-to-bsk">В корзину</div>

				</div>
			</div>
			<!-- / item w/stamps -->
			
			<!-- item w/stamps -->
			<div class="item b-base">
				<div class="w c">
					
					<a href="card.html" class="lnk">
						<div class="ttl">Инверторная сплит система и еще что-то</div>
						<div class="pic" style="background-image:url('<?= SITE_TEMPLATE_PATH ?>/i/blocks/hot2.png');"></div>
						<div class="stamps c">
							<div class="ico ico-3">Ограниченная серия</div>
						</div>
						<div class="dsc">Inverter система</div>
					</a>
					<div class="prc">
						<div class="curr curr-only">690 руб.</div>
					</div>
					<div class="btn btn-to-bsk">В корзину</div>

				</div>
			</div>
			<!-- / item w/stamps -->
			
			<!-- item w/stamps -->
			<div class="item b-base">
				<div class="w c">
					
					<a href="card.html" class="lnk">
						<div class="ttl">Hello World</div>
						<div class="pic" style="background-image:url('<?= SITE_TEMPLATE_PATH ?>/i/blocks/hot3.png');"></div>
						<div class="stamps c">
							<div class="ico ico-5">Лидер<br/>продаж</div>
							<div class="ico ico-1 ico-r">Новейшее<br/>покрытие</div>
						</div>
						<div class="dsc">Electrolux EACS/I - 13 HO/N3 серии Orlando DC Inverter система</div>
					</a>
					<div class="prc">
						<div class="curr curr-only">66 666 руб.</div>
					</div>
					<div class="btn btn-to-bsk">В корзину</div>

				</div>
			</div>
			<!-- / item w/stamps -->
		
			<!-- 3 items end -->
						
			<!-- item w/stamps -->
			<div class="item b-base">
				<div class="w c">
					
					<a href="card.html" class="lnk">
						<div class="ttl">BBBBИнверторная сплит</div>
						<div class="pic" style="background-image:url('<?= SITE_TEMPLATE_PATH ?>/i/blocks/hot2.png');"></div>
						<div class="stamps c">
							<div class="ico ico-1">Новейшее<br/>покрытие</div>
							<div class="ico ico-2 ico-r">5 лет гарантии</div>
						</div>
						<div class="dsc">Electrolux EACS/Iter система</div>
					</a>
					<div class="prc">
						<div class="curr curr-only">881 690 руб.</div>
					</div>
					<div class="btn btn-to-bsk">В корзину</div>

				</div>
			</div>
			<!-- / item w/stamps -->
			
			<!-- item w/stamps -->
			<div class="item b-base">
				<div class="w c">
					
					<a href="card.html" class="lnk">
						<div class="ttl">CCCИнверторная сплит система и еще что-то</div>
						<div class="pic" style="background-image:url('<?= SITE_TEMPLATE_PATH ?>/i/blocks/hot2.png');"></div>
						<div class="stamps c">
							<div class="ico ico-3">Ограниченная серия</div>
						</div>
						<div class="dsc">Orlando DC Inverter система</div>
					</a>
					<div class="prc">
						<div class="curr curr-only">1 690 руб.</div>
					</div>
					<div class="btn btn-to-bsk">В корзину</div>

				</div>
			</div>
			<!-- / item w/stamps -->
			
			<!-- item w/stamps -->
			<div class="item b-base">
				<div class="w c">
					
					<a href="card.html" class="lnk">
						<div class="ttl">DDDDDИнверторная сплит система система система система</div>
						<div class="pic" style="background-image:url('<?= SITE_TEMPLATE_PATH ?>/i/blocks/hot1.png');"></div>
						<div class="stamps c">
							<div class="ico ico-5">Лидер<br/>продаж</div>
							<div class="ico ico-1 ico-r">Новейшее<br/>покрытие</div>
						</div>
						<div class="dsc">Electrolux EACS/I - 13 HO/N3 серии Orlando DC Inverter система</div>
					</a>
					<div class="prc">
						<div class="curr curr-only">8 880 руб.</div>
					</div>
					<div class="btn btn-to-bsk">В корзину</div>

				</div>
			</div>
			<!-- / item w/stamps -->
		
		</div>
		<!-- / slides end -->
	</div>
	<!-- / other deals end -->
</div>
<!-- / other deals roll end -->


<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>