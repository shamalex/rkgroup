<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Марки оборудования (бренды)");
?>


<!-- cat-brands -->
<div class="cat-brands roll c">
	<h1 class="ttl1">Торговые марки</h1>

	<div class="brands-all c">
		
			<!-- item -->
			<a href="/brands/detail.php" class="item c-block">
				<div class="pic" style="background-image:url('<?= SITE_TEMPLATE_PATH ?>/i/blocks/logo-ballu.png');"></div>
				<div class="dsc">Группа компаний Biasi – лидер по производству отопительного оборудования в Европе. Biasi завоевала доверие и признание клиентов во всем мире благодаря совершенствованию технологий производства и инновационному...</div>
			</a>
			<!-- / item -->
			<!-- item -->
			<a href="/brands/detail.php" class="item c-block">
				<div class="pic" style="background-image:url('<?= SITE_TEMPLATE_PATH ?>/i/blocks/logo-elec.png');"></div>
				<div class="dsc">Компания BERKE специализируется на производстве полипропиленовых труб и фитингов для систем отопления, холодного и горячего водоснабжения. </div>
			</a>
			<!-- / item -->
			<!-- item -->
			<a href="/brands/detail.php" class="item c-block">
				<div class="pic" style="background-image:url('<?= SITE_TEMPLATE_PATH ?>/i/blocks/logo-zanus.png');"></div>
				<div class="dsc">BONECO Air-O-Swiss принадлежит швейцарской компании PLASTON. Компания PLASTON (Швейцария) является признанным мировым лидером в области производства бытовых приборов для обработки воздуха...</div>
			</a>
			<!-- / item -->
			<!-- item -->
			<a href="/brands/detail.php" class="item c-block">
				<div class="pic" style="background-image:url('<?= SITE_TEMPLATE_PATH ?>/i/blocks/logo-ballu.png');"></div>
				<div class="dsc">Группа компаний Biasi – лидер по производству отопительного оборудования в Европе. Biasi завоевала доверие и признание клиентов во всем мире благодаря совершенствованию технологий производства и инновационному...</div>
			</a>
			<!-- / item -->
			<!-- item -->
			<a href="/brands/detail.php" class="item c-block">
				<div class="pic" style="background-image:url('<?= SITE_TEMPLATE_PATH ?>/i/blocks/logo-elec.png');"></div>
				<div class="dsc">Компания BERKE специализируется на производстве полипропиленовых труб и фитингов для систем отопления, холодного и горячего водоснабжения. </div>
			</a>
			<!-- / item -->
			<!-- item -->
			<a href="/brands/detail.php" class="item c-block">
				<div class="pic" style="background-image:url('<?= SITE_TEMPLATE_PATH ?>/i/blocks/logo-zanus.png');"></div>
				<div class="dsc">BONECO Air-O-Swiss принадлежит швейцарской компании PLASTON. Компания PLASTON (Швейцария) является признанным мировым лидером в области производства бытовых приборов для обработки воздуха...</div>
			</a>
			<!-- / item -->
			<!-- item -->
			<a href="/brands/detail.php" class="item c-block">
				<div class="pic" style="background-image:url('<?= SITE_TEMPLATE_PATH ?>/i/blocks/logo-ballu.png');"></div>
				<div class="dsc">Группа компаний Biasi – лидер по производству отопительного оборудования в Европе. Biasi завоевала доверие и признание клиентов во всем мире благодаря совершенствованию технологий производства и инновационному...</div>
			</a>
			<!-- / item -->
			<!-- item -->
			<a href="/brands/detail.php" class="item c-block">
				<div class="pic" style="background-image:url('<?= SITE_TEMPLATE_PATH ?>/i/blocks/logo-elec.png');"></div>
				<div class="dsc">Компания BERKE специализируется на производстве полипропиленовых труб и фитингов для систем отопления, холодного и горячего водоснабжения. </div>
			</a>
			<!-- / item -->
			<!-- item -->
			<a href="/brands/detail.php" class="item c-block">
				<div class="pic" style="background-image:url('<?= SITE_TEMPLATE_PATH ?>/i/blocks/logo-zanus.png');"></div>
				<div class="dsc">BONECO Air-O-Swiss принадлежит швейцарской компании PLASTON. Компания PLASTON (Швейцария) является признанным мировым лидером в области производства бытовых приборов для обработки воздуха...</div>
			</a>
			<!-- / item -->
	
	</div>
	<!-- / brands-all end -->

</div>
<!-- / cat-brands roll end -->

<!-- hot deals block -->
<div class="roll">
	<h2 class="ttl-r">Спецпредложения</h2>
	<div class="hot-deals c">

		<!-- slides -->
		<div class="slides c" id="specoff">
	
			<!-- item w/timer -->
			<div class="item b-base">
				<div class="w c">
				
					<div class="timer">
						<div class="sum">Успей сэкономить 80 рублей</div>
						<div class="time" data-countdown="2015/02/08">
							<!-- <b>11</b> <span>дн.</span> <b>11</b> <span>ч.</span> <b>11</b> <span>мин.</span> <b class="sec">11</b> <span>сек.</span> -->
						</div>
					</div>

					<a class="lnk" href="card.html">
						<div class="ttl">Инверторная сплит система система система система</div>
						<div class="pic" style="background-image:url('<?= SITE_TEMPLATE_PATH ?>/i/blocks/hot1.png');">
							<div class="ico-hot">Успей<br/>купить</div>
						</div>
						<div class="dsc">Electrolux EACS/I - 13 HO/N3 серии Orlando DC Inverter система</div>
					</a>

					<div class="prc">
						<div class="curr">18 990 руб.</div>
						<div class="past">
							<span class="old">89 490</span> <span class="profit">1 666 выгода</span>
						</div>
					</div>
					<div class="btn btn-to-bsk">В корзину</div>

				</div>
			</div>
			<!-- / item w/timer -->
		
			<!-- item w/timer -->
			<div class="item b-base">
				<div class="w c">
				
					<div class="timer">
						<div class="sum">Успей сэкономить 1500 рублей</div>
						<div class="time" data-countdown="2015/12/08">
							<!-- <b>11</b> <span>дн.</span> <b>11</b> <span>ч.</span> <b>11</b> <span>мин.</span> <b class="sec">11</b> <span>сек.</span> -->
						</div>
					</div>

					<a class="lnk" href="card.html">
						<div class="ttl">Инверторная сплит система система система система</div>
						<div class="pic" style="background-image:url('<?= SITE_TEMPLATE_PATH ?>/i/blocks/hot2.png');">
							<div class="ico-hot">Успей<br/>купить</div>
						</div>
						<div class="dsc">Orlando DC Inverter система</div>
					</a>

					<div class="prc">
						<div class="curr">8 990 руб.</div>
						<div class="past">
							<span class="old">490</span> <span class="profit">500 выгода</span>
						</div>
					</div>
					<div class="btn btn-to-bsk">В корзину</div>

				</div>
			</div>
			<!-- / item w/timer -->
			
			<!-- item w/timer -->
			<div class="item b-base">
				<div class="w c">
				
					<div class="timer">
						<div class="sum">Успей сэкономить 100500 рублей</div>
						<div class="time" data-countdown="2015/07/08">
							<!-- <b>11</b> <span>дн.</span> <b>11</b> <span>ч.</span> <b>11</b> <span>мин.</span> <b class="sec">11</b> <span>сек.</span> -->
						</div>
					</div>

					<a class="lnk" href="card.html">
						<div class="ttl">Система система система</div>
						<div class="pic" style="background-image:url('<?= SITE_TEMPLATE_PATH ?>/i/blocks/hot3.png');">
							<div class="ico-hot">Успей<br/>купить</div>
						</div>
						<div class="dsc">Electrolux EACS/I - 13 HO/N3 серии Orlando DC Inverter система</div>
					</a>

					<div class="prc">
						<div class="curr">88 990 руб.</div>
						<div class="past">
							<span class="old">89 490</span> <span class="profit">9 500 выгода</span>
						</div>
					</div>
					<div class="btn btn-to-bsk">В корзину</div>

				</div>
			</div>
			<!-- / item w/timer -->
			
			<!-- 3 -->
			
			<!-- item w/timer -->
			<div class="item b-base">
				<div class="w c">
				
					<div class="timer">
						<div class="sum">3Успей сэкономить 80 рублей</div>
						<div class="time" data-countdown="2015/02/08">
							<!-- <b>11</b> <span>дн.</span> <b>11</b> <span>ч.</span> <b>11</b> <span>мин.</span> <b class="sec">11</b> <span>сек.</span> -->
						</div>
					</div>

					<a class="lnk" href="card.html">
						<div class="ttl">Инверторная сплит система система система система</div>
						<div class="pic" style="background-image:url('<?= SITE_TEMPLATE_PATH ?>/i/blocks/hot1.png');">
							<div class="ico-hot">Успей<br/>купить</div>
						</div>
						<div class="dsc">Electrolux EACS/I - 13 HO/N3 серии Orlando DC Inverter система</div>
					</a>

					<div class="prc">
						<div class="curr">18 990 руб.</div>
						<div class="past">
							<span class="old">89 490</span> <span class="profit">1 666 выгода</span>
						</div>
					</div>
					<div class="btn btn-to-bsk">В корзину</div>

				</div>
			</div>
			<!-- / item w/timer -->
		
			<!-- item w/timer -->
			<div class="item b-base">
				<div class="w c">
				
					<div class="timer">
						<div class="sum">4Успей сэкономить 1500 рублей</div>
						<div class="time" data-countdown="2015/12/08">
							<!-- <b>11</b> <span>дн.</span> <b>11</b> <span>ч.</span> <b>11</b> <span>мин.</span> <b class="sec">11</b> <span>сек.</span> -->
						</div>
					</div>

					<a class="lnk" href="card.html">
						<div class="ttl">Инверторная сплит система система система система</div>
						<div class="pic" style="background-image:url('<?= SITE_TEMPLATE_PATH ?>/i/blocks/hot2.png');">
							<div class="ico-hot">Успей<br/>купить</div>
						</div>
						<div class="dsc">Orlando DC Inverter система</div>
					</a>

					<div class="prc">
						<div class="curr">8 990 руб.</div>
						<div class="past">
							<span class="old">490</span> <span class="profit">500 выгода</span>
						</div>
					</div>
					<div class="btn btn-to-bsk">В корзину</div>

				</div>
			</div>
			<!-- / item w/timer -->
			
			<!-- item w/timer -->
			<div class="item b-base">
				<div class="w c">
				
					<div class="timer">
						<div class="sum">5Успей сэкономить 100500 рублей</div>
						<div class="time" data-countdown="2015/07/08">
							<!-- <b>11</b> <span>дн.</span> <b>11</b> <span>ч.</span> <b>11</b> <span>мин.</span> <b class="sec">11</b> <span>сек.</span> -->
						</div>
					</div>

					<a class="lnk" href="card.html">
						<div class="ttl">Система система система</div>
						<div class="pic" style="background-image:url('<?= SITE_TEMPLATE_PATH ?>/i/blocks/hot3.png');">
							<div class="ico-hot">Успей<br/>купить</div>
						</div>
						<div class="dsc">Electrolux EACS/I - 13 HO/N3 серии Orlando DC Inverter система</div>
					</a>

					<div class="prc">
						<div class="curr">88 990 руб.</div>
						<div class="past">
							<span class="old">89 490</span> <span class="profit">9 500 выгода</span>
						</div>
					</div>
					<div class="btn btn-to-bsk">В корзину</div>

				</div>
			</div>
			<!-- / item w/timer -->


		</div>
		<!-- / slides end -->
	</div>
	<!-- / hot deals item container end -->
</div>
<!-- / hot deals roll end -->

<!-- other deals roll -->
<div class="roll">
	<h2 class="ttl-r">популярные товары</h2>
	<!-- other deals -->
	<div class="other-deals c">
		<!-- slides -->
		<div class="slides c" id="other">
		
			<!-- item w/stamps -->
			<div class="item b-base">
				<div class="w c">
					
					<a href="card.html" class="lnk">
						<div class="ttl">Orlando сплит система система система система</div>
						<div class="pic" style="background-image:url('<?= SITE_TEMPLATE_PATH ?>/i/blocks/hot1.png');"></div>
						<div class="stamps c">
							<div class="ico ico-1">Новейшее покрытие</div>
							<div class="ico ico-2">5 лет гарантии</div>
						</div>
						<div class="dsc">Electrolux EACS/I - 13 HO/N3 серии Orlando DC Inverter система</div>
					</a>
					<div class="prc">
						<div class="curr curr-only">17 234 руб.</div>
					</div>
					<div class="btn btn-to-bsk">В корзину</div>

				</div>
			</div>
			<!-- / item w/stamps -->
			
			<!-- item w/stamps -->
			<div class="item b-base">
				<div class="w c">
					
					<a href="card.html" class="lnk">
						<div class="ttl">Инверторная сплит система и еще что-то</div>
						<div class="pic" style="background-image:url('<?= SITE_TEMPLATE_PATH ?>/i/blocks/hot2.png');"></div>
						<div class="stamps c">
							<div class="ico ico-3">Ограниченная серия</div>
						</div>
						<div class="dsc">Inverter система</div>
					</a>
					<div class="prc">
						<div class="curr curr-only">690 руб.</div>
					</div>
					<div class="btn btn-to-bsk">В корзину</div>

				</div>
			</div>
			<!-- / item w/stamps -->
			
			<!-- item w/stamps -->
			<div class="item b-base">
				<div class="w c">
					
					<a href="card.html" class="lnk">
						<div class="ttl">Hello World</div>
						<div class="pic" style="background-image:url('<?= SITE_TEMPLATE_PATH ?>/i/blocks/hot3.png');"></div>
						<div class="stamps c">
							<div class="ico ico-5">Лидер<br/>продаж</div>
							<div class="ico ico-1 ico-r">Новейшее<br/>покрытие</div>
						</div>
						<div class="dsc">Electrolux EACS/I - 13 HO/N3 серии Orlando DC Inverter система</div>
					</a>
					<div class="prc">
						<div class="curr curr-only">66 666 руб.</div>
					</div>
					<div class="btn btn-to-bsk">В корзину</div>

				</div>
			</div>
			<!-- / item w/stamps -->
		
			<!-- 3 items end -->
						
			<!-- item w/stamps -->
			<div class="item b-base">
				<div class="w c">
					
					<a href="card.html" class="lnk">
						<div class="ttl">BBBBИнверторная сплит</div>
						<div class="pic" style="background-image:url('<?= SITE_TEMPLATE_PATH ?>/i/blocks/hot2.png');"></div>
						<div class="stamps c">
							<div class="ico ico-1">Новейшее<br/>покрытие</div>
							<div class="ico ico-2 ico-r">5 лет гарантии</div>
						</div>
						<div class="dsc">Electrolux EACS/Iter система</div>
					</a>
					<div class="prc">
						<div class="curr curr-only">881 690 руб.</div>
					</div>
					<div class="btn btn-to-bsk">В корзину</div>

				</div>
			</div>
			<!-- / item w/stamps -->
			
			<!-- item w/stamps -->
			<div class="item b-base">
				<div class="w c">
					
					<a href="card.html" class="lnk">
						<div class="ttl">CCCИнверторная сплит система и еще что-то</div>
						<div class="pic" style="background-image:url('<?= SITE_TEMPLATE_PATH ?>/i/blocks/hot2.png');"></div>
						<div class="stamps c">
							<div class="ico ico-3">Ограниченная серия</div>
						</div>
						<div class="dsc">Orlando DC Inverter система</div>
					</a>
					<div class="prc">
						<div class="curr curr-only">1 690 руб.</div>
					</div>
					<div class="btn btn-to-bsk">В корзину</div>

				</div>
			</div>
			<!-- / item w/stamps -->
			
			<!-- item w/stamps -->
			<div class="item b-base">
				<div class="w c">
					
					<a href="card.html" class="lnk">
						<div class="ttl">DDDDDИнверторная сплит система система система система</div>
						<div class="pic" style="background-image:url('<?= SITE_TEMPLATE_PATH ?>/i/blocks/hot1.png');"></div>
						<div class="stamps c">
							<div class="ico ico-5">Лидер<br/>продаж</div>
							<div class="ico ico-1 ico-r">Новейшее<br/>покрытие</div>
						</div>
						<div class="dsc">Electrolux EACS/I - 13 HO/N3 серии Orlando DC Inverter система</div>
					</a>
					<div class="prc">
						<div class="curr curr-only">8 880 руб.</div>
					</div>
					<div class="btn btn-to-bsk">В корзину</div>

				</div>
			</div>
			<!-- / item w/stamps -->
		
		</div>
		<!-- / slides end -->
	</div>
	<!-- / other deals end -->
</div>
<!-- / other deals roll end -->


<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>