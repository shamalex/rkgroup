<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Заказы");
?>

<div class="roll basket c">
	
	<? if(empty($_REQUEST['ORDER_ID'])): ?>
		<?$APPLICATION->IncludeComponent(
			"sws:sale.basket.basket", 
			".default", 
			array(
				"COUNT_DISCOUNT_4_ALL_QUANTITY" => "N",
				"COLUMNS_LIST" => array(
					0 => "NAME",
					1 => "PROPS",
					2 => "DELETE",
					3 => "PRICE",
					4 => "QUANTITY",
					5 => "SUM",
				),
				"AJAX_MODE" => "Y",
				"AJAX_OPTION_JUMP" => "N",
				"AJAX_OPTION_STYLE" => "N",
				"AJAX_OPTION_HISTORY" => "N",
				"PATH_TO_ORDER" => "/personal/order/make/",
				"HIDE_COUPON" => "N",
				"QUANTITY_FLOAT" => "N",
				"PRICE_VAT_SHOW_VALUE" => "Y",
				"SET_TITLE" => "N",
				"AJAX_OPTION_ADDITIONAL" => "",
				"OFFERS_PROPS" => array(
				),
				"COMPONENT_TEMPLATE" => ".default",
				"USE_PREPAYMENT" => "N",
				"ACTION_VARIABLE" => "action",
				"HIDE_BUTTONS" => "Y",
				"EMPTY_MOVE_TO" => "/personal/cart/",
			),
			false
		);?>
	<? endif; ?>
	
	<?$APPLICATION->IncludeComponent(
		"bitrix:sale.order.ajax", 
		".default", 
		array(
			"ALLOW_AUTO_REGISTER" => "Y",
			"ALLOW_NEW_PROFILE" => "Y",
			"COMPONENT_TEMPLATE" => ".default",
			"COUNT_DELIVERY_TAX" => "N",
			"DELIVERY_NO_AJAX" => "Y",
			"DELIVERY_NO_SESSION" => "Y",
			"DELIVERY_TO_PAYSYSTEM" => "d2p",
			"DISABLE_BASKET_REDIRECT" => "Y",
			"ONLY_FULL_PAY_FROM_ACCOUNT" => "N",
			"PATH_TO_AUTH" => "/auth/",
			"PATH_TO_BASKET" => "/personal/cart/",
			// "PATH_TO_PAYMENT" => "payment.php",
			"PATH_TO_PAYMENT" => "/personal/order/payment/",
			"PATH_TO_PERSONAL" => "/personal/order/",
			"PAY_FROM_ACCOUNT" => "N",
			"PRODUCT_COLUMNS" => array(
			),
			"PROP_1" => array(
			),
			"PROP_2" => array(
			),
			"SEND_NEW_USER_NOTIFY" => "Y",
			"SET_TITLE" => "Y",
			"SHOW_PAYMENT_SERVICES_NAMES" => "Y",
			"SHOW_STORES_IMAGES" => "N",
			"TEMPLATE_LOCATION" => "popup",
			"USE_PREPAYMENT" => "N"
		),
		false
	);?>
	
</div>
<!-- / basket roll end -->

<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>