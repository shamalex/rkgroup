<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Личный кабинет");
?>

<?
/*
<div class="bx_page">
	<p>В личном кабинете Вы можете проверить текущее состояние корзины, ход выполнения Ваших заказов, просмотреть или изменить личную информацию, а также подписаться на новости и другие информационные рассылки. </p>
	<div>
		<h2>Личная информация</h2>
		<a href="profile/">Изменить регистрационные данные</a>
	</div>
	<div>
		<h2>Заказы</h2>
		<a href="order/">Ознакомиться с состоянием заказов</a><br/>
		<a href="cart/">Посмотреть содержимое корзины</a><br/>
		<a href="order/">Посмотреть историю заказов</a><br/>
	</div>
	<div>
		<h2>Подписка</h2>
		<a href="subscribe/">Изменить подписку</a>
	</div>
</div>
*/
?>

<?
global $USER;
if ($_REQUEST['register'] == 'yes' && $USER && $USER->IsAuthorized()) {
	CUser::SendUserInfo($USER->GetID(), SITE_ID, "Вы были успешно зарегистрированы.", true);
	LocalRedirect(nfGetCurPageParam('registered=yes', ['register', 'registered']));
}
?>

<!-- basket roll -->
<div class="roll lk c">
	<h1 class="ttl1">Личный кабинет</h1>
	<? if(!$USER || !$USER->IsAuthorized()): ?>
		<?
		$APPLICATION->ShowAuthForm("");
		?>
	<? else: ?>
		<? if ($_REQUEST['registered'] == 'yes'): ?>
			<div class="roll full txt-block">
				<p>Вы зарегистрированы и успешно авторизовались.</p>
			</div>
		<? endif; ?>
		<!-- left column holder -->
		<div class="col-l">
			<h2 class="ttl2">Личные данные</h2>

			<div class="lk-ctrl">

				<?$APPLICATION->IncludeComponent("bitrix:main.profile", "personal_main", Array(
						"AJAX_MODE" => "N",	// Включить режим AJAX
						"AJAX_OPTION_ADDITIONAL" => "",	// Дополнительный идентификатор
						"AJAX_OPTION_HISTORY" => "N",	// Включить эмуляцию навигации браузера
						"AJAX_OPTION_JUMP" => "N",	// Включить прокрутку к началу компонента
						"AJAX_OPTION_STYLE" => "N",	// Включить подгрузку стилей
						"CHECK_RIGHTS" => "Y",	// Проверять права доступа
						"COMPONENT_TEMPLATE" => ".default",
						"SEND_INFO" => "N",	// Генерировать почтовое событие
						"SET_TITLE" => "N",	// Устанавливать заголовок страницы
						"USER_PROPERTY" => "",	// Показывать доп. свойства
						"USER_PROPERTY_NAME" => "",	// Название закладки с доп. свойствами
					),
					false
				);?>

				<a class="btn btn-30 btn-b btn-lk-change" href="/personal/profile/">Изменить личные данные или пароль</a>

				<?$APPLICATION->IncludeComponent("bitrix:catalog.compare.list", "personal_main", Array(
						"ACTION_VARIABLE" => "action_footer",	// Название переменной, в которой передается действие
						"AJAX_MODE" => "N",	// Включить режим AJAX
						"AJAX_OPTION_ADDITIONAL" => "",	// Дополнительный идентификатор
						"AJAX_OPTION_HISTORY" => "N",	// Включить эмуляцию навигации браузера
						"AJAX_OPTION_JUMP" => "N",	// Включить прокрутку к началу компонента
						"AJAX_OPTION_STYLE" => "N",	// Включить подгрузку стилей
						"COMPARE_URL" => "/catalog/compare/",	// URL страницы с таблицей сравнения
						"COMPONENT_TEMPLATE" => ".default",
						"DETAIL_URL" => "",	// URL, ведущий на страницу с содержимым элемента раздела
						"IBLOCK_ID" => "8",	// Инфоблок
						"IBLOCK_TYPE" => "catalog",	// Тип инфоблока
						"NAME" => "CATALOG_COMPARE_LIST",	// Уникальное имя для списка сравнения
						"POSITION" => "top left",
						"POSITION_FIXED" => "N",	// Отображать список сравнения поверх страницы
						"PRODUCT_ID_VARIABLE" => "add_id_footer",	// Название переменной, в которой передается код товара для покупки
					),
					false
				);?>
				<?$FAV_COOKIE_NAME = 'FAVORITES';
				$FAV_ELEMENTS = $APPLICATION->get_cookie($FAV_COOKIE_NAME);
				if (!empty($FAV_ELEMENTS)) {
					$FAV_ELEMENTS = explode('.', $FAV_ELEMENTS);

				} else {
					$FAV_ELEMENTS = [];
				}
				$itemCount = count($FAV_ELEMENTS);?>
				<div class="lnk-hold"><a class="lnk-comp" href="/catalog/favorites/"><div class="lnk">Товаров в избранном</div><div class="ico"><?=$itemCount?></div></a></div>
				<div class="btn btn-30 btn-lk-logout">Выйти из личного кабинета</div>
				<br>
				<p class="ttl customer-n">номер покупателя: <?= $USER->GetID()?></p>

			</div>

		</div>
		<!-- / left column holder end -->

		<!-- right column holder -->
		<div class="col-r">

			<h2 class="ttl2">История заказов</h2>

			<div class="brd tbl-w">

				<?
				$_REQUEST['show_all'] = 'Y';
				?>
				<?$APPLICATION->IncludeComponent(
					"bitrix:sale.personal.order.list",
					"personal_main",
					Array(
						"ACTIVE_DATE_FORMAT" => "d.m.Y H:i",
						"CACHE_GROUPS" => "Y",
						"CACHE_TIME" => "3600",
						"CACHE_TYPE" => "A",
						"COMPONENT_TEMPLATE" => "personal_main",
						"HISTORIC_STATUSES" => array("F"),
						"ID" => $ID,
						"NAV_TEMPLATE" => "",
						"ORDERS_PER_PAGE" => "100",
						"PATH_TO_BASKET" => "",
						"PATH_TO_CANCEL" => "",
						"PATH_TO_COPY" => "",
						"PATH_TO_DETAIL" => "/personal/order/detail/#ID#/",
						"SAVE_IN_SESSION" => "N",
						"SET_TITLE" => "N",
						"STATUS_COLOR_F" => "gray",
						"STATUS_COLOR_N" => "green",
						"STATUS_COLOR_P" => "yellow",
						"STATUS_COLOR_PSEUDO_CANCELLED" => "red"
					)
				);?>
				<?
				unset($_REQUEST['show_all']);
				?>

			</div>
			<!-- / brd end -->
		</div>
		<!-- / right column holder end -->

	<? endif; ?>

</div>
<!-- / lk roll end -->

<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>
