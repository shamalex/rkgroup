<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Корзина");
?>

<?$APPLICATION->IncludeComponent(
	"sws:sale.basket.basket", 
	".default", 
	array(
		"COUNT_DISCOUNT_4_ALL_QUANTITY" => "N",
		"COLUMNS_LIST" => array(
			0 => "NAME",
			1 => "PROPS",
			2 => "DELETE",
			3 => "PRICE",
			4 => "QUANTITY",
			5 => "SUM",
		),
		"AJAX_MODE" => "Y",
		"AJAX_OPTION_JUMP" => "N",
		"AJAX_OPTION_STYLE" => "N",
		"AJAX_OPTION_HISTORY" => "N",
		"PATH_TO_ORDER" => "/personal/order/make/",
		"HIDE_COUPON" => "N",
		"QUANTITY_FLOAT" => "N",
		"PRICE_VAT_SHOW_VALUE" => "Y",
		"SET_TITLE" => "Y",
		"AJAX_OPTION_ADDITIONAL" => "",
		"OFFERS_PROPS" => array(
		),
		"COMPONENT_TEMPLATE" => ".default",
		"USE_PREPAYMENT" => "N",
		"ACTION_VARIABLE" => "action"
	),
	false
);?>

<? /*$APPLICATION->IncludeFile("include/you_saw_block.php", Array(), Array("MODE"=>"php"));*/?>

<?/*
$GLOBALS['OTHER_DEALS_ROLL_NAME'] = "На что еще стоит обратить внимание?";
$GLOBALS['OTHER_DEALS_ROLL_ID'] = "other2";
?>
<?$APPLICATION->IncludeFile("include/other-deals-roll.php", Array(), Array("MODE"=>"html"));*/?>

<? /*$APPLICATION->IncludeFile("include/popular_items_block.php", Array(), Array("MODE"=>"php")); */?>

<? $APPLICATION->IncludeFile("include/need_help.php", Array(), Array("MODE"=>"php")); ?>

<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>