<?
header("Access-Control-Allow-Origin: *");
header("Access-Control-Allow-Methods: GET, POST");
header("Connection:keep-alive");
header("Content-Type: application/json; charset=utf-8");
include ($_SERVER["DOCUMENT_ROOT"]. "/bitrix/modules/main/include/prolog_before.php");

$mess="";


if(!$_REQUEST["source"] || !in_array($_REQUEST["source"],array(34,35)))$_REQUEST["source"]=34;

if($_REQUEST["source"]==34){
	$mess='<div class="success">
	Обращение к руководству успешно отправлено.
</div>
';	
}elseif($_REQUEST["source"]==35){
	$mess='<div class="success">
	Заявка на КП успешно отправлена.<br />Нашим менеджеры свяжутся с вами в ближайшее время.
</div>
';
}



$messages=array(
	"ru"=>array(
		"enter_name"=>"Укажите Ваше ФИО",
		"enter_phone"=>"Укажите Ваш телефон",
		"wrong_phone"=>"Некорректный телефон",
		"enter_email"=>"Укажите Ваш e-mail",
		"wrong_email"=>"Некорректный телефон",
		"enter_message"=>"Укажите Ваше сообщение",
		"wrong_file_size"=>"Загружаемый файл превышает максимальный размер",
		"db_error"=>"Не верный ввод данных, пожалуйста, повторите ввод!",
		"success"=>$mess,
	),
	"en"=>array(
		"enter_name"=>"Enter your name",
		"enter_phone"=>"Enter your phone",
		"wrong_phone"=>"Phone is wrong",
		"enter_email"=>"Enter your e-mail",
		"wrong_email"=>"E-mail is wrong",
		"enter_message"=>"Enter your message",
		"wrong_file_size"=>"File size is incorrect",
		"db_error"=>"An error has occurred when sending the request. ",
		"success"=>	"Request sent successfully",
	)
);

$error=array();

if(!$_REQUEST["lang"])$_REQUEST["lang"]="ru";

if($_REQUEST["subj"]){
	$error[]=$messages[$_REQUEST["lang"]]["db_error"];
}else{
	if(!$_REQUEST["name"]) $error["name"]=$messages[$_REQUEST["lang"]]["enter_name"];
	if(!$_REQUEST["phone"]) $error["phone"]=$messages[$_REQUEST["lang"]]["enter_phone"];
	elseif(!preg_match("/^\+7\(\d{3}\)\d{3}\-\d{2}\-\d{2}$/",$_REQUEST["phone"])) $error["phone"]=$messages[$_REQUEST["lang"]]["wrong_phone"];
	if(!$_REQUEST["email"]) $error["email"]=$messages[$_REQUEST["lang"]]["enter_email"];
	elseif(!preg_match("/^[a-zA-Z0-9_\-.]+@[a-zA-Z0-9\-]+\.[a-zA-Z0-9\-.]+$/",$_REQUEST["email"])) $error["phone"]=$messages[$_REQUEST["lang"]]["wrong_email"];
	if(!$_REQUEST["message"]) $error["message"]=$messages[$_REQUEST["lang"]]["enter_message"];
	if($_FILES['file'] && $_FILES['userfile']['size']>5*1024*1024) $error["file"]=$messages[$_REQUEST["lang"]]["wrong_file_size"];
}
if(!$error)
{
	CModule::IncludeModule("iblock");
	$arLoadArray=array(
		"IBLOCK_ID"=>$_REQUEST["source"],
		"ACTIVE"=>"Y",
		"PROPERTY_VALUES"=>array(
			"name" => $_REQUEST["name"],	// имя
			"phone" => $_REQUEST["phone"],	// телефон
			"email" => $_REQUEST["email"],	// e-mail
			"message" => $_REQUEST["message"],	// сообщение
			"product_link" => $_REQUEST["product_link"],	
		),
	);

	if($_FILES['file'])	$arLoadArray["PROPERTY_VALUES"]["file"] = CFile::MakeFileArray($_FILES['file']["tmp_name"]);
	
	if($_REQUEST["source"]==34) $arLoadArray["NAME"]="Письмо руководству от ".$_REQUEST["name"]."(".date("Y-m-d H:i:s").")";
	elseif($_REQUEST["source"]==35) $arLoadArray["NAME"]="Заявка на КП от ".$_REQUEST["name"]."(".date("Y-m-d H:i:s").")";

	
	$el=new CIBlockElement;
	if(!$res = $el->Add($arLoadArray)){
		$error[]=$messages[$_REQUEST["lang"]]["db_error"].$el->LAST_ERROR;
	}
}

if(count($error)==0){
	echo json_encode(array("status"=>"ok","message"=>$messages[$_REQUEST["lang"]]["success"]));
}
else{
	echo json_encode(array("status"=>"error","message"=>'<div class="error">'.$messages[$_REQUEST["lang"]]["db_error"].'</div>',"error"=>$error));
}
?>