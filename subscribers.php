<?
include $_SERVER['DOCUMENT_ROOT'].'/bitrix/modules/main/include/prolog_before.php';

if (!CSite::InGroup(array(1, 6, 7))) {
	LocalRedirect('/personal/');
}

CModule::IncludeModule("iblock");

header('Content-type: application/xml');

echo '<?xml version="1.0"?>';
echo "\n";
?>
<?
echo "<subscribers>";
echo "\n";
				
$arSort = ['NAME' => 'ASC'];
$arFilter = Array("IBLOCK_ID"=>22);
$arSelect = Array("ID", "NAME");
$res = CIBlockElement::GetList($arSort, $arFilter, false, false, $arSelect);  
while ($arFields = $res->Fetch()) {
	echo "\t<email>".$arFields['NAME']."</email>";
	echo "\n";
}
echo "</subscribers>";
?>