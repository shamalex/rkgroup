<?
header("Access-Control-Allow-Origin: *");
header("Access-Control-Allow-Methods: GET, POST");
header("Connection:keep-alive");
header("Content-Type: application/json; charset=utf-8");
include ($_SERVER["DOCUMENT_ROOT"]. "/bitrix/modules/main/include/prolog_before.php");

$messages=array(
	"enter_grade"=>"Укажите свою оценку",
	"enter_text"=>"Укажите текст отзыва",
	"db_error"=>"Произошла ошибка при отправке отзыва ",
	"success"=>"Ваш отзыв отправлен и будет опубликован после обработки модератором",
);

$error=array();
if(!$_REQUEST["grade"]) $error[]=$messages["enter_grade"];
if($_REQUEST["grade"]<=3 && !$_REQUEST["text"]) $error[]=$messages["enter_text"];

if($_REQUEST["grade"]>3 && !$_REQUEST["text"]) $active="N";
else $active="N";

if(!$error)
{
	CModule::IncludeModule("iblock");
	$arLoadArray=array(
		"IBLOCK_ID"=>13,
		"ACTIVE"=>$active,
		"PROPERTY_VALUES"=>array(
			"grade"=>IntVal($_REQUEST["grade"])-3,
			"text"=>$_REQUEST["text"],
			"author"=>$_REQUEST["author"],
			"ITEM_LINK"=>$_REQUEST["item"],
			"date"=>time()."000",
			"city"=>$_REQUEST["city"]
		),
		"NAME"=>"Отзыв от ".($_REQUEST["author"]?$_REQUEST["author"]:"Аноним")." (".date("Y-m-d H:i:s").")",
		"CODE"=>"r".date("YmdHis")
	);

	$el=new CIBlockElement;
	if(!$res = $el->Add($arLoadArray)){
		$error[]=$messages["db_error"].$el->LAST_ERROR;
	}
}
if(count($error)==0){
	echo json_encode(array("status"=>"success","message"=>$messages["success"]));
}
else{
	echo json_encode(array("status"=>"error","message"=>implode("<br>",$error)));
}
?>