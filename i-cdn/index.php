<?
include $_SERVER['DOCUMENT_ROOT'].'/bitrix/modules/main/include/prolog_before.php';

$FOUND = false;
$FILE = $_SERVER['REQUEST_URI'];

if (!empty($FILE)) {
	$FILE = explode('/', $FILE);
	if (!empty($FILE)) {
		$FILE = $FILE[count($FILE)-1];
		
		$res = CFile::GetList(array(), array("ORIGINAL_NAME"=>$FILE));
		while($arFile = $res->Fetch()) {
			if (!$FOUND || ($FOUND['WIDTH'] < $arFile['WIDTH'] || $FOUND['HEIGHT'] < $arFile['HEIGHT'])) {
				$arFile['PATH'] = CFile::GetPath($arFile["ID"]);
				$FOUND = $arFile;
			}
		}
		
		if (!empty($FOUND)) {
			// p($FOUND);
			header('Content-type: '.$FOUND['CONTENT_TYPE']);
			readfile($_SERVER['DOCUMENT_ROOT'].$FOUND['PATH']);
			die();
		}
		
	}
}

include($_SERVER['DOCUMENT_ROOT'].'/404.php');
