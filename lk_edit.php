<?
header("Access-Control-Allow-Origin: *");
header("Access-Control-Allow-Methods: GET, POST");
header("Connection:keep-alive");
header("Content-Type: application/json; charset=utf-8");
include $_SERVER['DOCUMENT_ROOT'].'/bitrix/modules/main/include/prolog_before.php';

CModule::IncludeModule('iblock');
global $USER;
$result=array("status"=>"","message"=>"");

$messages=array(
	"db_error"=>"Произошла ошибка ",
);


if($_POST["action"] && $USER->IsAuthorized()){
	switch($_POST["action"]){
		case "add_adr":
			$error=[];			
			$errorbyfields=[];
			$arLoadArray=array(
				"NAME"=>"",
				"IBLOCK_ID"=>30,
				"ACTIVE"=>"Y",
				"PROPERTY_VALUES"=>array(
					"city"=>$_REQUEST["city"],
					"street"=>$_REQUEST["street"],
					"house"=>$_REQUEST["house"],
					"housing"=>$_REQUEST["housing"],
					"access"=>$_REQUEST["access"],
					"apartment"=>$_REQUEST["apartment"],
					"USER"=>$USER->GetID(),
				),
			);
			
			$resEl=CIBlockElement::GetList(array(),array("IBLOCK_ID"=>30,"PROPERTY_USER"=>$USER->GetID()),false,false,array("ID"));
			if($arEl=$resEl->GetNext()){
				$arLoadArray["PROPERTY_VALUES"]["default"]="N";
			}else{
				$arLoadArray["PROPERTY_VALUES"]["default"]="Y";
			}
			
			if($_REQUEST["city_text"]) $arLoadArray["NAME"].=$_REQUEST["city_text"].", ";
			if($_REQUEST["street"]) $arLoadArray["NAME"].=$_REQUEST["street"].", ";
			if($_REQUEST["house"]) $arLoadArray["NAME"].=$_REQUEST["house"].", ";
			if($_REQUEST["housing"]) $arLoadArray["NAME"].="корп. ".$_REQUEST["housing"].", ";
			if($_REQUEST["build"]) $arLoadArray["NAME"].="стр. ".$_REQUEST["build"].", ";
			if($_REQUEST["apartment"]) $arLoadArray["NAME"].="кв./офис ".$_REQUEST["apartment"].", ";
			
			$arLoadArray["NAME"]=trim($arLoadArray["NAME"],", ");
			
			if(!$arLoadArray["NAME"]){
				$error[]="Укажите адрес";
			}else{
				$el=new CIBlockElement;
				if(!$res = $el->Add($arLoadArray)){
					$error[]=$messages["db_error"].$el->LAST_ERROR;
				}else{
					$result["ID"]=$res;
					$result["NAME"]=$arLoadArray["NAME"];
					$result["attr"]=($arLoadArray["PROPERTY_VALUES"]["default"]=="Y")?" checked":"";
				}			
			}
			
			if(count($error)>0){
				$result["status"]="error";
				$result["message"]=implode(". ",$error);
				$result["error"]=$errorbyfields;
			}else{
				$result["status"]="ok";
			}
			
			break;
		case "add_comp":
			$error=[];
			$errorbyfields=[];
			if($_REQUEST["org_name"] && (strlen($_REQUEST["org_name"])<2 || !preg_match('/([А-Яа-яA-Za-z0-9\-\.\"\, ])+/',$_REQUEST["org_name"]))){
				$errorbyfields["org_name"]="Некорректное название компании";
			}
			if($_REQUEST["inn"] && (strlen($_REQUEST["inn"])!=10 || !preg_match('/([0-9])+/',$_REQUEST["inn"]))){
				$errorbyfields["inn"]="Некорректный ИНН";
			}
			if($_REQUEST["kpp"] && (strlen($_REQUEST["kpp"])!=9 || !preg_match('/([0-9])+/',$_REQUEST["kpp"]))){
				$errorbyfields["kpp"]="Некорректный КПП";
			}
			if($_REQUEST["ks"] && (strlen($_REQUEST["ks"])!=20 || !preg_match('/([0-9])+/',$_REQUEST["ks"]))){
				$errorbyfields["ks"]="Некорректный кор.счет";
			}
			if($_REQUEST["bank"] && (strlen($_REQUEST["bank"])<2 || !preg_match('/([А-Яа-яA-Za-z0-9\-\" ])+/',$_REQUEST["bank"]))){
				$errorbyfields["bank"]="Некорректное название банка";
			}
			if($_REQUEST["bik"] && (strlen($_REQUEST["bik"])!=9 || !preg_match('/([0-9])+/',$_REQUEST["bik"]))){
				$errorbyfields["bik"]="Некорректный БИК";
			}
			if($_REQUEST["rs"] && (strlen($_REQUEST["rs"])!=20 || !preg_match('/([0-9])+/',$_REQUEST["rs"]))){
				$errorbyfields["rs"]="Некорректный рассчетный счет";
			}
			if($_REQUEST["ur_ard"] && (strlen($_REQUEST["ur_ard"])<2 || !preg_match('/([А-Яа-яA-Za-z0-9\-\.\"\, ])+/',$_REQUEST["ur_ard"]))){
				$errorbyfields["ur_ard"]="Некорректный юридический адрес";
			}
			if(count($errorbyfields)==0){
				$arLoadArray=array(
					"NAME"=>"",
					"IBLOCK_ID"=>31,
					"ACTIVE"=>"Y",
					"PROPERTY_VALUES"=>array(
						"org_type"=>$_REQUEST["org_type"],
						"org_name"=>$_REQUEST["org_name"],
						"inn"=>$_REQUEST["inn"],
						"kpp"=>$_REQUEST["kpp"],
						"ks"=>$_REQUEST["ks"],
						"bik"=>$_REQUEST["bik"],
						"bank"=>$_REQUEST["bank"],
						"rs"=>$_REQUEST["rs"],
						"ur_adr"=>$_REQUEST["ur_adr"],
						"USER"=>$USER->GetID(),
					),
				);
				
				$resEl=CIBlockElement::GetList(array(),array("IBLOCK_ID"=>31,"PROPERTY_USER"=>$USER->GetID()),false,false,array("ID"));
				if($arEl=$resEl->GetNext()){
					$arLoadArray["PROPERTY_VALUES"]["default"]="N";
				}else{
					$arLoadArray["PROPERTY_VALUES"]["default"]="Y";
				}
				
				if($_REQUEST["org_type_text"]) $arLoadArray["NAME"].=$_REQUEST["org_type_text"]." ";
				if($_REQUEST["org_name"]) $arLoadArray["NAME"].="\"".$_REQUEST["org_name"]."\"";
							
				if(!$arLoadArray["NAME"]){
					$errorbyfields["org_name"]="Укажите компанию";
				}else{
					$el=new CIBlockElement;
					if(!$res = $el->Add($arLoadArray)){
						$error[]=$messages["db_error"].$el->LAST_ERROR;
					}else{
						$result["ID"]=$res;
						$result["NAME"]=$arLoadArray["NAME"];
						$result["attr"]=($arLoadArray["PROPERTY_VALUES"]["default"]=="Y")?" checked":"";
					}			
				}
			}
			
			if(count($error)>0 || count($errorbyfields)>0){
				$result["status"]="error";
				$result["message"]=implode(". ",$error);
				$result["error"]=$errorbyfields;
			}else{
				$result["status"]="ok";
			}
		
			break;
		case "add_contact":
			$error=[];			
			$errorbyfields=[];
			if(!$_REQUEST["fio"] || (strlen($_REQUEST["fio"])<2 || !preg_match('/([А-Яа-яA-Za-z\- ])+/',$_REQUEST["fio"]))){
				$errorbyfields["fio"]="Некорректное ФИО";
			}
			if(!$_REQUEST["email"] || !preg_match('/([A-Za-z0-9_-]+\.)*[A-Za-z0-9_-]+@[A-Za-z0-9_-]+(\.[A-Za-z0-9_-]+)*\.[A-Za-z]{2,6}/',$_REQUEST["email"])){
				$errorbyfields["email"]="Некорректный email";
			}
			if(!$_REQUEST["phone"] || !preg_match('/\+7 ?\(\d\d\d\) ?\d\d\d-\d\d-\d\d/',$_REQUEST["phone"])){
				$errorbyfields["phone"]="Некорректный телефон";
			}
			
			
			if(count($errorbyfields)==0){
				$arLoadArray=array(
					"NAME"=>$_REQUEST["fio"]." (".$_REQUEST["email"].", ".$_REQUEST["phone"].")",
					"IBLOCK_ID"=>33,
					"ACTIVE"=>"Y",
					"PROPERTY_VALUES"=>array(
						"fio"=>$_REQUEST["fio"],
						"email"=>$_REQUEST["email"],
						"phone"=>$_REQUEST["phone"],
						"USER"=>$USER->GetID()
					),
				);
			
				$resEl=CIBlockElement::GetList(array(),array("IBLOCK_ID"=>33,"PROPERTY_USER"=>$USER->GetID()),false,false,array("ID"));
				if($arEl=$resEl->GetNext()){
					$arLoadArray["PROPERTY_VALUES"]["default"]="N";
				}else{
					$arLoadArray["PROPERTY_VALUES"]["default"]="Y";
				}
			

				$el=new CIBlockElement;
				if(!$res = $el->Add($arLoadArray)){
					$error[]=$messages["db_error"].$el->LAST_ERROR;
				}else{
					$result["ID"]=$res;
					$result["NAME"]=$arLoadArray["NAME"];
					$result["attr"]=($arLoadArray["PROPERTY_VALUES"]["default"]=="Y")?" checked":"";
				}			
			}
			
			if(count($error)>0 || count($errorbyfields)>0){
				$result["status"]="error";
				$result["message"]=implode(". ",$error);
				$result["error"]=$errorbyfields;
			}else{
				$result["status"]="ok";
			}
			
			break;
		case "default_adr":
			$error=[];
			$arLoadArray=[];
			$el=new CIBlockElement;
			$resEl=CIBlockElement::GetList(array(),array("IBLOCK_ID"=>30,"PROPERTY_USER"=>$USER->GetID(),"PROPERTY_default"=>"Y"),false,false,array("ID"));
			
			if($arEl=$resEl->GetNext()){
				$arLoadArray["PROPERTY_VALUES"]["default"]="N";
				if(!$el->Update($arEl["ID"],$arLoadArray)){
					$error[]=$messages["db_error"].$el->LAST_ERROR;
				}
			}
			
			if(count($error)==0){
				$arLoadArray["PROPERTY_VALUES"]["default"]="Y";
				if(!$el->Update($_REQUEST["ID"],$arLoadArray)){
					$error[]=$messages["db_error"].$el->LAST_ERROR;
				}
			}
			
			if(count($error)>0){
				$result["status"]="error";
				$result["message"]=implode(". ",$error);
			}else{
				$result["status"]="ok";
			}
		
			break;
		case "default_comp":
			$error=[];
			$arLoadArray=[];
			$el=new CIBlockElement;
			$resEl=CIBlockElement::GetList(array(),array("IBLOCK_ID"=>31,"PROPERTY_USER"=>$USER->GetID(),"PROPERTY_default"=>"Y"),false,false,array("ID"));
			
			if($arEl=$resEl->GetNext()){
				$arLoadArray["PROPERTY_VALUES"]["default"]="N";
				if(!$el->Update($arEl["ID"],$arLoadArray)){
					$error[]=$messages["db_error"].$el->LAST_ERROR;
				}
			}
			
			if(count($error)==0){
				$arLoadArray["PROPERTY_VALUES"]["default"]="Y";
				if(!$el->Update($_REQUEST["ID"],$arLoadArray)){
					$error[]=$messages["db_error"].$el->LAST_ERROR;
				}
			}
			
			if(count($error)>0){
				$result["status"]="error";
				$result["message"]=implode(". ",$error);
			}else{
				$result["status"]="ok";
			}
			
			break;
		case "default_contact":
			$error=[];
			$arLoadArray=[];
			$el=new CIBlockElement;
			$resEl=CIBlockElement::GetList(array(),array("IBLOCK_ID"=>33,"PROPERTY_USER"=>$USER->GetID(),"PROPERTY_default"=>"Y"),false,false,array("ID"));
			
			if($arEl=$resEl->GetNext()){
				$arLoadArray["PROPERTY_VALUES"]["default"]="N";
				if(!$el->Update($arEl["ID"],$arLoadArray)){
					$error[]=$messages["db_error"].$el->LAST_ERROR;
				}
			}
			
			if(count($error)==0){
				$arLoadArray["PROPERTY_VALUES"]["default"]="Y";
				if(!$el->Update($_REQUEST["ID"],$arLoadArray)){
					$error[]=$messages["db_error"].$el->LAST_ERROR;
				}
			}
			
			if(count($error)>0){
				$result["status"]="error";
				$result["message"]=implode(". ",$error);
			}else{
				$result["status"]="ok";
			}
			
			break;
		default:
			$result["status"]="error";
			$result["message"]="Действие недоступно";
			break;
	}
	
	$rsUser = CUser::GetByID($USER->GetID());
	$fields = $rsUser->Fetch();
	//p($fields);
	unset($fields["PASSWORD"]);
	unset($fields["CHECKWORD"]);
	$user = new CUser;
	$user->Update($USER->GetID(), $fields);
}

echo json_encode($result);