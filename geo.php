<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Geo");

@ini_set('memory_limit', '4096M');
@set_time_limit(595);

$file1 = './import/cidr_optim.txt';
$file2 = './import/cities.txt';
$file1utf = './import/cidr_optim_utf.txt';
$file2utf = './import/cities_utf.txt';
$file1utf_data = './import/cidr_optim_data.php';
$file2utf_data = './import/cities_data.php';

setlocale(LC_ALL, 'ru_RU.UTF-8');

if (!file_exists($file1utf)) {			
	$file_data = file_get_contents($file1);
	// if (!is_utf8($file_data)) {
		$file_data = iconv("CP1251//TRANSLIT//IGNORE", "UTF-8//TRANSLIT//IGNORE", $file_data);
		file_put_contents($file1utf, $file_data);
	// }
	unset($file_data);
}

if (!file_exists($file1utf_data)) {
	$file_data = file_get_contents($file1utf);
	$arr = explode("\n", $file_data);
	foreach($arr as $i=>$data) {
		$arr[$i] = explode("\t", $data);
	}
	// $cidr_optim = $arr;
	$cidr_optim = [];
	foreach($arr as $i=>$data) {
		$data[0] = intval($data[0]);
		$data[1] = intval($data[1]);
		if ($data[4] != '-') {
			$data[4] = intval($data[4]);
		}
		$cidr_optim[] = $data;
	}
	unset($arr);
	file_put_contents($file1utf_data, '<? $cidr_optim = '.var_export($cidr_optim, 1).';');
} else {
	include($file1utf_data);
}

if (!file_exists($file2utf)) {	
	$file_data = file_get_contents($file2);
	// if (!is_utf8($file_data)) {
		$file_data = iconv("CP1251//TRANSLIT//IGNORE", "UTF-8//TRANSLIT//IGNORE", $file_data);
		file_put_contents($file2utf, $file_data);
	// }
	unset($file_data);
}

if (!file_exists($file2utf_data)) {
	$file_data = file_get_contents($file2utf);
	$arr = explode("\n", $file_data);
	foreach($arr as $i=>$data) {
		$arr[$i] = explode("\t", $data);
	}
	// $cities = $arr;
	$cities = [];
	foreach($arr as $i=>$data) {
		$data[0] = intval($data[0]);
		$cities[$data[0]] = $data;
	}
	unset($arr);
	file_put_contents($file2utf_data, '<? $cities = '.var_export($cities, 1).';');
} else {
	include($file2utf_data);
}

// p($cidr_optim);
// p($cities);

if (!empty($_REQUEST['ip']) && filter_var($_REQUEST['ip'], FILTER_VALIDATE_IP)) {
	$IP = $_REQUEST['ip'];
} elseif (!empty($_SERVER['HTTP_CLIENT_IP'])) {
	$IP = $_SERVER['HTTP_CLIENT_IP'];
} elseif (!empty($_SERVER['HTTP_X_FORWARDED_FOR'])) {
	$IP = $_SERVER['HTTP_X_FORWARDED_FOR'];
} else {
	$IP = $_SERVER['REMOTE_ADDR'];
}
$IP_INT = ip2long($IP);
$CUR_DATA = false;
foreach($cidr_optim as $data) {
	if ($data[0] <= $IP_INT && $data[1] >= $IP_INT) {
		$CUR_DATA = $data;
		break;
	}
}
if (!empty($CUR_DATA) && $CUR_DATA[3] == 'RU' && !empty($CUR_DATA[4])) {
	$CUR_DATA[4] = $cities[$CUR_DATA[4]];
	
	foreach($arGeoData['CITIES_IDS'] as $arFields) {
		// if ($arFields["REGION"] == $CUR_DATA[4][2] && $arFields["NAME"] == $CUR_DATA[4][1]) {
		if ($arFields["NAME"] == $CUR_DATA[4][1]) {
			$arFields['AUTO_DETECTED'] = true;
			$arFields['CITY_DATA'] = $CUR_DATA;
			$arGeoData['CUR_CITY'] = $arFields;
			break;
		}
	}
}


// p($CUR_DATA);

// p($arGeoData['CUR_CITY']);

$arResult['GEO'] = [];
global $arGeoData;

CModule::IncludeModule('iblock');

// $arSort = array();
// $arFilter = Array("IBLOCK_ID"=>15, "ID"=>$arGeoData['CUR_CITY']['ID']);
// $arSelect = false;
// $res = CIBlockElement::GetList($arSort, $arFilter, false, false, $arSelect);  
// if ($ob = $res->GetNextElement()) {
	// $arResult['GEO'] = $ob->GetFields();
	// $arResult['GEO']['PROPERTIES'] = $ob->GetProperties();
// }

// p($arResult);

$arCurRegion = [];
$arFilter = Array("IBLOCK_ID"=>15, 'ID'=>$arGeoData['CUR_CITY']['ID']);
$arSelect = ['ID', 'NAME', 'PROPERTY_network_name', 'PROPERTY_network_data_code', 'PROPERTY_filial1', 'PROPERTY_filial2', 'PROPERTY_filial1_code', 'PROPERTY_filial2_code'];
$res = CIBlockElement::GetList(Array(), $arFilter, false, false, $arSelect);  
if ($arFields = $res->Fetch()) {
	$arCurRegion = $arFields;
}

?>

<form action="" method="get">
	IP: <input type="text" name="ip" value="" />
	<button type="submit">Проверить</button>
</form>

<p>
	<br />
</p>
<p>
	IP: <?= $IP ?>
</p>
<p>
	Регион выбранный: <?= $arGeoData['CUR_CITY']['REGION'] ?>
	<br />
	Регион автоопределения: <?= $arGeoData['CUR_CITY']['CITY_DATA'][4][3] ?><?= !empty($arGeoData['CUR_CITY']['CITY_DATA'][4][3]) ? ',' : '' ?> <?= $arGeoData['CUR_CITY']['CITY_DATA'][4][2] ?>
</p>
<p>
	Город выбранный: <?= $arGeoData['CUR_CITY']['NAME'] ?>
	<br />
	Город автоопределения: <?= $arGeoData['CUR_CITY']['CITY_DATA'][4][1] ?>
</p>

<p>
--
</p>

<p>
Куст: <?= $arCurRegion['PROPERTY_NETWORK_NAME_VALUE'] ?>
</p>

<p>
Код цен, валют, самовывоза: <?= $arGeoData['CUR_CITY']['CITY_CODE'] ?>
</p>

<p>
Код остатков: <?= $arGeoData['CUR_CITY']['CITY_CODE1'] ?>
</p>

<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>