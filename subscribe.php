<?
include $_SERVER['DOCUMENT_ROOT'].'/bitrix/modules/main/include/prolog_before.php';

CModule::IncludeModule('iblock');
@ini_set('memory_limit', '4096M');
@set_time_limit(595);

$SUBSCRIBERS_IBLOCK_ID = 22;
// $subscribers_file = $_SERVER['DOCUMENT_ROOT'].'/upload/subscribers.xml';
//$subscribers_file = $_SERVER['DOCUMENT_ROOT'].'/subscribers.php';
//$subscribers_file_tpl = $_SERVER['DOCUMENT_ROOT'].'/subscribers_tpl.php';

$EMAIL = $_REQUEST['EMAIL'];

$arResult = [
	'OK' => 0,
	'ERROR' => '',
];

if (!empty($EMAIL)) {
	$EMAIL = strtolower($EMAIL);
	if (check_email($EMAIL)) {
		$arFilter = Array("IBLOCK_ID"=>$SUBSCRIBERS_IBLOCK_ID, 'NAME'=>$EMAIL);
		$arSelect = Array("ID", "NAME");
		$arLimit = ['nTopCount'=>1];
		$res = CIBlockElement::GetList(Array(), $arFilter, false, $arLimit, $arSelect);  
		if ($arFields = $res->Fetch()) {  
			$arResult['ERROR'] = 'Вы уже подписаны на рассылку спецпредложений!';
		} else {
			$arLoadProductArray = Array(
				"IBLOCK_SECTION_ID" => false,
				"IBLOCK_ID"      	=> $SUBSCRIBERS_IBLOCK_ID,
				"NAME"  		 	=> $EMAIL,
				"CODE"           	=> '',
				"XML_ID"         	=> '',
				"ACTIVE"         	=> "Y",
				"SORT"   		 	=> '500',
			);
			$el = new CIBlockElement;
			if($PRODUCT_ID = $el->Add($arLoadProductArray, false, false, false)) {
				$arResult['OK'] = 1;
				
				//$result = file_get_contents($subscribers_file_tpl);
				/* $result .= '<?xml version="1.0"?>'; */
				// $result .= "\n";
				/*$result .= "<subscribers>";
				$result .= "\n";
				
				$arSort = ['NAME' => 'ASC'];
				$arFilter = Array("IBLOCK_ID"=>$SUBSCRIBERS_IBLOCK_ID);
				$arSelect = Array("ID", "NAME");
				$res = CIBlockElement::GetList($arSort, $arFilter, false, false, $arSelect);  
				while ($arFields = $res->Fetch()) {
					$result .= "\t<email>".$arFields['NAME']."</email>";
					$result .= "\n";
				}
				$result .= "</subscribers>";
				file_put_contents($subscribers_file, $result);
				*/
			} else {
				$arResult['ERROR'] = $el->LAST_ERROR;
			}
		}
	} else {
		$arResult['ERROR'] = 'Неверно указан адрес email,<br/>пожалуйста, повторите ввод!';
	}
} else {
	$arResult['ERROR'] = 'Заполните ваш email!';
}

echo json_encode($arResult);