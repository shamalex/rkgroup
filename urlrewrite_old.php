<?
$arUrlRewrite = array(
	array(
		"CONDITION" => "#^/bitrix/services/ymarket/#",
		"RULE" => "",
		"ID" => "",
		"PATH" => "/bitrix/services/ymarket/index.php",
	),
	array(
		"CONDITION" => "#^/personal/order/#",
		"RULE" => "",
		"ID" => "bitrix:sale.personal.order",
		"PATH" => "/personal/order/index.php",
	),
	array(
		"CONDITION" => "#^/catalog/brands/#",
		"RULE" => "",
		"ID" => "bitrix:news",
		"PATH" => "/catalog/brands/index.php",
	),
	array(
		"CONDITION" => "#^/about/news/#",
		"RULE" => "",
		"ID" => "bitrix:news",
		"PATH" => "/about/news/index.php",
	),
	array(
		"CONDITION" => "#^/portfolio/#",
		"RULE" => "",
		"ID" => "bitrix:news",
		"PATH" => "/portfolio/index.php",
	),
	array(
		"CONDITION" => "#^/articles/#",
		"RULE" => "",
		"ID" => "bitrix:catalog",
		"PATH" => "/articles/index.php",
	),
	array(
		"CONDITION" => "#^/service/#",
		"RULE" => "",
		"ID" => "bitrix:catalog",
		"PATH" => "/service/index.php",
	),
	array(
		"CONDITION" => "#^/special/#",
		"RULE" => "",
		"ID" => "bitrix:news",
		"PATH" => "/special/index.php",
	),
	array(
		"CONDITION" => "#^/service/#",
		"RULE" => "",
		"ID" => "bitrix:catalog",
		"PATH" => "/services/service/index.php",
	),
	array(
		"CONDITION" => "#^/catalog/#",
		"RULE" => "",
		"ID" => "bitrix:catalog",
		"PATH" => "/catalog/index.php",
	),
	array(
		"CONDITION" => "#^/store/#",
		"RULE" => "",
		"ID" => "bitrix:catalog.store",
		"PATH" => "/store/index.php",
	),
	array(
		"CONDITION" => "#^/i-cdn/#",
		"RULE" => "",
		"ID" => "bitrix:i.cdn",
		"PATH" => "/i-cdn/index.php",
	),
);

?>