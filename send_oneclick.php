<?
include $_SERVER['DOCUMENT_ROOT'].'/bitrix/modules/main/include/prolog_before.php';

$arEventFields = array(
	"PHONE" => $_REQUEST['PHONE'],
	"URL" => $_REQUEST['URL'],
	"CITY" => $arGeoData['CUR_CITY']['NAME'],
	"REGION" => $arGeoData['CUR_CITY']['REGION'],
);
if (strlen($arEventFields["PHONE"]) > 0 && strlen($arEventFields["URL"]) > 0) {
	CEvent::SendImmediate("ONECLICK_FORM", 's1', $arEventFields);
}