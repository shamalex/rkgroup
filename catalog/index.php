<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetPageProperty("keywords", "Каталог оборудования, интернет-магазин, климатическое оборудование и услуги, Москва");
$APPLICATION->SetPageProperty("description", "Ознакомьтесь с информацией на странице \"Каталог климатического оборудования\". Интернет-магазин климатического оборудования и услуг РУСКЛИМАТ (Москва), тел. 8 (495) 777-19-77");
$APPLICATION->SetPageProperty("title", "Каталог климатического оборудования компании Русклимат");
$APPLICATION->SetTitle("Каталог климатического оборудования");
?>
<?

$arrFilter = [];

if (!empty($arGeoData['CUR_CITY']['PRICE_ID'])) {
	$arrFilter['>CATALOG_PRICE_'.$arGeoData['CUR_CITY']['PRICE_ID']] = '0';
} else {
	$arrFilter['<CATALOG_PRICE_1'] = '0';
}

$QUANITY_PROP = preg_replace('/\-+/ui', '_', $arGeoData['CUR_CITY']['CITY_CODE1']);
$QUANITY_PROP = strtoupper('COUNT_'.$QUANITY_PROP);
$QUANITY_PROP2 = preg_replace('/\-+/ui', '_', $arGeoData['CUR_CITY']['CITY_CODE2']);
$QUANITY_PROP2 = strtoupper('COUNT_'.$QUANITY_PROP2);
CModule::IncludeModule('iblock');
$prop_list = [];
$properties = CIBlockProperty::GetList(Array("sort"=>"asc", "name"=>"asc"), Array("IBLOCK_ID"=>8));
while ($prop_fields = $properties->GetNext()) {
	
	$prop_list[] = $prop_fields['CODE'];
}


?>
<?$APPLICATION->IncludeComponent(
	"bitrix:catalog", 
	"catalog", 
	array(
		"ACTION_VARIABLE" => "action",
		"ADD_ELEMENT_CHAIN" => "N",
		"ADD_PICT_PROP" => "picture1",
		"ADD_PROPERTIES_TO_BASKET" => "Y",
		"ADD_SECTIONS_CHAIN" => !empty($curParams["ADD_SECTIONS_CHAIN"]) ? $curParams["ADD_SECTIONS_CHAIN"] : "Y",
		"AJAX_MODE" => "N",
		"AJAX_OPTION_ADDITIONAL" => "",
		"AJAX_OPTION_HISTORY" => "N",
		"AJAX_OPTION_JUMP" => "N",
		"AJAX_OPTION_STYLE" => "N",
		"ALSO_BUY_ELEMENT_COUNT" => "5",
		"ALSO_BUY_MIN_BUYES" => "1",
		"BASKET_URL" => "/personal/cart/",
		"BIG_DATA_RCM_TYPE" => "bestsell",
		"CACHE_FILTER" => "N",
		"CACHE_GROUPS" => "N",
		"CACHE_TIME" => "36000000",
		"CACHE_TYPE" => "A",
		"COMMON_ADD_TO_BASKET_ACTION" => "",
		"COMMON_SHOW_CLOSE_POPUP" => "Y",
		"COMPARE_ELEMENT_SORT_FIELD" => "sort",
		"COMPARE_ELEMENT_SORT_ORDER" => "asc",
		"COMPARE_FIELD_CODE" => array(
			0 => "",
			1 => "",
		),
		"COMPARE_NAME" => "CATALOG_COMPARE_LIST",
		"COMPARE_POSITION" => "top left",
		"COMPARE_POSITION_FIXED" => "N",
		"COMPARE_PROPERTY_CODE" => $prop_list,
		"COMPONENT_TEMPLATE" => "catalog",
		"CONVERT_CURRENCY" => "N",
		"DETAIL_ADD_DETAIL_TO_SLIDER" => "Y",
		"DETAIL_ADD_TO_BASKET_ACTION" => array(
			0 => "ADD",
		),
		"DETAIL_BACKGROUND_IMAGE" => "-",
		"DETAIL_BROWSER_TITLE" => "seo_title",
		"DETAIL_CHECK_SECTION_ID_VARIABLE" => "Y",
		"DETAIL_DETAIL_PICTURE_MODE" => "POPUP",
		"DETAIL_DISPLAY_NAME" => "Y",
		"DETAIL_DISPLAY_PREVIEW_TEXT_MODE" => "E",
		"DETAIL_FB_APP_ID" => "",
		"DETAIL_FB_USE" => "N",
		"DETAIL_META_DESCRIPTION" => "seo_descr",
		"DETAIL_META_KEYWORDS" => "seo_key",
		"DETAIL_PROPERTY_CODE" => $prop_list,
		"DETAIL_SET_CANONICAL_URL" => "Y",
		"DETAIL_SHOW_BASIS_PRICE" => "Y",
		"DETAIL_SHOW_MAX_QUANTITY" => "N",
		"DETAIL_USE_COMMENTS" => "Y",
		"DETAIL_USE_VOTE_RATING" => "Y",
		"DETAIL_VK_API_ID" => "API_ID",
		"DETAIL_VK_USE" => "N",
		"DETAIL_VOTE_DISPLAY_AS_RATING" => "rating",
		"DISPLAY_BOTTOM_PAGER" => "Y",
		"DISPLAY_ELEMENT_SELECT_BOX" => "N",
		"DISPLAY_TOP_PAGER" => "N",
		"ELEMENT_SORT_FIELD" => 'catalog_PRICE_'.$arGeoData['CUR_CITY']['PRICE_ID'],
		"ELEMENT_SORT_FIELD2" => "name",
		"ELEMENT_SORT_ORDER" => "asc",
		"ELEMENT_SORT_ORDER2" => "asc",
		"FIELDS" => "",
		"FILE_404" => "/catalog/404.php",
		"FILTER_FIELD_CODE" => array(
			0 => "",
			1 => "",
		),
		"FILTER_NAME" => "arrFilter",
		"FILTER_PRICE_CODE" => array(
			0 => $arGeoData['CUR_CITY']['PRICE_ID'],
		),
		"FILTER_PROPERTY_CODE" => $prop_list,
		"FILTER_VIEW_MODE" => "VERTICAL",
		"HIDE_NOT_AVAILABLE" => "Y",
		"IBLOCK_ID" => "8",
		"IBLOCK_ID_BRANDS" => "9",
		"GEO_DOMAINS_IBLOCK_ID" => "21",
		"IBLOCK_TYPE" => "catalog",
		"INCLUDE_SUBSECTIONS" => "A",
		"LABEL_PROP" => "-",
		"LINE_ELEMENT_COUNT" => "1",
		"LINK_ELEMENTS_URL" => "link.php?PARENT_ELEMENT_ID=#ELEMENT_ID#",
		"LINK_IBLOCK_ID" => "",
		"LINK_IBLOCK_TYPE" => "",
		"LINK_PROPERTY_SID" => "",
		"LIST_BROWSER_TITLE" => "UF_CAT_SEO_TITLE",
		"LIST_META_DESCRIPTION" => "UF_CAT_SEO_DESCRIPT",
		"LIST_META_KEYWORDS" => "UF_CAT_SEO_KEYWORDS",
		"LIST_PROPERTY_CODE" => $prop_list,
		"MAIN_TITLE" => "Наличие на складах",
		"MESSAGE_404" => "",
		"MESS_BTN_ADD_TO_BASKET" => "В корзину",
		"MESS_BTN_BUY" => "Купить",
		"MESS_BTN_COMPARE" => "Сравнение",
		"MESS_BTN_DETAIL" => "Подробнее",
		"MESS_NOT_AVAILABLE" => "Нет в наличии",
		"MIN_AMOUNT" => "10",
		"PAGER_BASE_LINK_ENABLE" => "N",
		"PAGER_DESC_NUMBERING" => "N",
		"PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
		"PAGER_SHOW_ALL" => "N",
		"PAGER_SHOW_ALWAYS" => "N",
		"PAGER_TEMPLATE" => ".default",
		"PAGER_TITLE" => "Товары",
		"PAGE_ELEMENT_COUNT" => "10",
		"PARTIAL_PRODUCT_PROPERTIES" => "Y",
		"PRICE_CODE" => array(
			0 => $arGeoData['CUR_CITY']['PRICEBUSH_ID'],
			1 => $arGeoData['CUR_CITY']['PRICEBUSH_ID'].'_NODISCOUNT',
		),
		"PRICE_VAT_INCLUDE" => "Y",
		"PRICE_VAT_SHOW_VALUE" => "N",
		"PRODUCT_ID_VARIABLE" => "add_id",
		"PRODUCT_PROPERTIES" => array(),
		"PRODUCT_PROPS_VARIABLE" => "prop",
		"PRODUCT_QUANTITY_VARIABLE" => "",
		"SECTIONS_SHOW_PARENT_NAME" => "Y",
		"SECTIONS_VIEW_MODE" => "LIST",
		"SECTION_ADD_TO_BASKET_ACTION" => "ADD",
		"SECTION_BACKGROUND_IMAGE" => "-",
		"SECTION_COUNT_ELEMENTS" => "N",
		"SECTION_ID_VARIABLE" => "SECTION_ID",
		"SECTION_TOP_DEPTH" => "3",
		"SEF_FOLDER" => !empty($curParams["SEF_FOLDER"]) ? $curParams["SEF_FOLDER"] : "/catalog/",
		"SEF_MODE" => "Y",
		"SET_LAST_MODIFIED" => "Y",
		"SET_STATUS_404" => "Y",
		"SET_TITLE" => "Y",
		"SHOW_404" => "Y",
		"SHOW_DEACTIVATED" => "N",
		"SHOW_DISCOUNT_PERCENT" => "Y",
		"SHOW_EMPTY_STORE" => "Y",
		"SHOW_GENERAL_STORE_INFORMATION" => "N",
		"SHOW_OLD_PRICE" => "Y",
		"SHOW_PRICE_COUNT" => "1",
		"SHOW_TOP_ELEMENTS" => "N",
		"SIDEBAR_DETAIL_SHOW" => "Y",
		"SIDEBAR_PATH" => "",
		"SIDEBAR_SECTION_SHOW" => "Y",
		"STORES" => "",
		"STORE_PATH" => "/store/#store_id#",
		"TEMPLATE_THEME" => "black",
		"TOP_ADD_TO_BASKET_ACTION" => "ADD",
		"TOP_ELEMENT_COUNT" => "9",
		"TOP_ELEMENT_SORT_FIELD" => "sort",
		"TOP_ELEMENT_SORT_FIELD2" => "id",
		"TOP_ELEMENT_SORT_ORDER" => "asc",
		"TOP_ELEMENT_SORT_ORDER2" => "desc",
		"TOP_LINE_ELEMENT_COUNT" => "3",
		"TOP_PROPERTY_CODE" => array(
			0 => "",
			1 => "",
		),
		"TOP_VIEW_MODE" => "SECTION",
		"USER_FIELDS" => "",
		"USE_ALSO_BUY" => "N",
		"USE_BIG_DATA" => "N",
		"USE_COMMON_SETTINGS_BASKET_POPUP" => "N",
		"USE_COMPARE" => "Y",
		"USE_ELEMENT_COUNTER" => "Y",
		"USE_FILTER" => "Y",
		"USE_MAIN_ELEMENT_SECTION" => "N",
		"USE_MIN_AMOUNT" => "Y",
		"USE_PRICE_COUNT" => "N",
		"USE_PRODUCT_QUANTITY" => "N",
		"USE_SALE_BESTSELLERS" => "Y",
		"USE_STORE" => "N",
		"SEF_URL_TEMPLATES" => array(
			"sections" => "",
			"section" => "#SECTION_CODE#/",
			"element" => "#SECTION_CODE#/#ELEMENT_CODE#/",
			"compare" => "compare/",
			"smart_filter" => "#SECTION_CODE#/#SMART_FILTER_PATH#/apply/",
		),
		"CUR_MAIN_CITY_CODE" => $arGeoData['CUR_CITY']['PRICEBUSH_ID'],
		"CUR_MAIN_CITY_CODE2" => $arGeoData['CUR_CITY']['CITY_CODE1'],
		"QUANITY_PROP" => $QUANITY_PROP,
		"QUANITY_PROP2" => $QUANITY_PROP2,
		"PRICE_ID" => $arGeoData['CUR_CITY']['PRICE_ID'],
		"PRICE_NODISCOUNT_ID" => $arGeoData['CUR_CITY']['PRICE_NODISCOUNT_ID'],
	),
	false
);?>
<?

// if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED!==true) {
	// include_once($_SERVER['DOCUMENT_ROOT'].'/bitrix/modules/main/include/prolog_before.php');
/*$uri = explode('/', $_SERVER['REQUEST_URI']);
	if (count($uri) > 3 && strpos($_SERVER['REQUEST_URI'], '/catalog/') === 0) {
		$url = preg_replace('/^\/catalog/ui', '', $_SERVER['REQUEST_URI']);
		LocalRedirect($url, false, '301 Moved Permanently');
}*/
// }
?>
<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>