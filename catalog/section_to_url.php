<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Категории и их адреса");

CModule::IncludeModule('iblock');
@ini_set('memory_limit', '4096M');
@set_time_limit(60);

$made_els = [];

$CATALOG_IBLOCK_ID = 8;
$BRANDS_IBLOCK_ID = 9;
$PRICE_TYPE_ID = 1;

$arSort = Array("UF_CAT_CODE"=>"ASC");
$arFilter = Array("IBLOCK_ID"=>$CATALOG_IBLOCK_ID);
$arSelect = Array("ID", "NAME", "CODE", "XML_ID", "SECTION_PAGE_URL", "UF_CAT_CODE");
$res = CIBlockSection::GetList($arSort, $arFilter, false, $arSelect);  
while($arFields = $res->GetNext()) {  
	// p($arFields);
	// break;
	$made_els[$arFields['ID']] = $arFields;
}


// p($made_els['977']);



?>

<style>
	#table {
		border-collapse: collapse;
		border: 1px solid black;
		margin-bottom: 20px;
		width: 100%;
	}
	#table pre {
		white-space: pre-wrap;
	}
	#table tr td {
		text-aling: left;
		vertical-align: top;
		padding: 10px;
	}
	#table tr td.item_desc {
		min-width: 300px;
	}
	#table tr td.item_desc a {
		text-decoration: underline;
	}
</style>

<table cellpadding="10" cellspacing="0" border="1" id="table">
	<tr>
		<th>
			#
		</th>
		<th>
			Раздел
		</th>
		<th>
			Код
		</th>
		<th>
			URL
		</th>
	</tr>
	<?
	$cnt = 1;
	?>
	<? foreach($made_els as $arItem): ?>
		<tr>
			<td>
				<?= $cnt ?>
			</td>
			<td>
				<a href="<?= $arItem['SECTION_PAGE_URL'] ?>" target="_blank"><?= $arItem['NAME'] ?></a>
			</td>
			<td>
				<?= $arItem['UF_CAT_CODE'] ?>
			</td>
			<td>
				<?= $arItem['SECTION_PAGE_URL'] ?>
			</td>
		</tr>
		<?
		$cnt++;
		?>
	<? endforeach; ?>
</table>



<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>