<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Товары и их адреса");

CModule::IncludeModule('iblock');
@ini_set('memory_limit', '4096M');
@set_time_limit(60);

$made_els = [];

$CATALOG_IBLOCK_ID = 8;
$BRANDS_IBLOCK_ID = 9;
$PRICE_TYPE_ID = 1;

$arSort = Array("PROPERTY_CODE"=>"ASC");
$arFilter = Array("IBLOCK_ID"=>$CATALOG_IBLOCK_ID);
$arSelect = Array("ID", "NAME", "CODE", "XML_ID", "DETAIL_PAGE_URL", "PROPERTY_CODE", "PROPERTY_NS_CODE", "PROPERTY_FULL_NAME", "PROPERTY_ITEM_NAME");
// $arLimit = ['iNumPage' => $cur_page, 'nPageSize' => 10];
$arLimit = false;
$res = CIBlockElement::GetList($arSort, $arFilter, false, $arLimit, $arSelect);  
while($arFields = $res->GetNext()) {
	$made_els[$arFields['ID']] = $arFields;
}


// p($made_els['977']);



?>

<style>
	#table {
		border-collapse: collapse;
		border: 1px solid black;
		margin-bottom: 20px;
		width: 100%;
	}
	#table pre {
		white-space: pre-wrap;
	}
	#table tr td {
		text-aling: left;
		vertical-align: top;
		padding: 10px;
	}
	#table tr td.item_desc {
		min-width: 300px;
	}
	#table tr td.item_desc a {
		text-decoration: underline;
	}
</style>

<table cellpadding="10" cellspacing="0" border="1" id="table">
	<tr>
		<th>
			#
		</th>
		<th>
			Товар
		</th>
		<th>
			Код
		</th>
		<th>
			URL
		</th>
	</tr>
	<?
	$cnt = 1;
	?>
	<? foreach($made_els as $arItem): ?>
		<tr>
			<td>
				<?= $cnt ?>
			</td>
			<td>
				<?
				$NAME = $arItem['NAME'];
				if (!empty($arItem['PROPERTY_ITEM_NAME_VALUE'])) {
					$NAME = $arItem['PROPERTY_ITEM_NAME_VALUE'];
				} elseif (!empty($arItem['PROPERTY_FULL_NAME_VALUE'])) {
					$NAME = $arItem['PROPERTY_FULL_NAME_VALUE'];
				}
				?>
				<a href="<?= $arItem['DETAIL_PAGE_URL'] ?>" target="_blank"><?= $NAME ?></a>
			</td>
			<td>
				<?= $arItem['PROPERTY_CODE_VALUE'] ?>
			</td>
			<td>
				<?= $arItem['DETAIL_PAGE_URL'] ?>
			</td>
		</tr>
		<?
		$cnt++;
		?>
	<? endforeach; ?>
</table>



<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>