<?
include_once($_SERVER['DOCUMENT_ROOT'].'/bitrix/modules/main/include/urlrewrite.php');
CHTTP::SetStatus("404 Not Found");
@define("ERROR_404","Y");

require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");

$APPLICATION->SetTitle("Ошибка 404. Увы, такой страницы не существует!");
$APPLICATION->SetPageProperty("MAIN_CLASS", "inner line");
$APPLICATION->SetPageProperty("HIDE_BREADCRUMS", "Y");
$APPLICATION->SetPageProperty("CONTENT_PAGE", "N");


$CATALOG_IBLOCK_ID = 8;
$CATALOG_IBLOCK_TYPE = 'catalog';
$GEO_DOMAINS_IBLOCK_ID = 21;

$SELECT_CITY = false;

$CUR_HOST = $_SERVER['HTTP_HOST'];
$CUR_HOST = preg_replace('/^www\./ui', '', $CUR_HOST);

$arFilter = Array("IBLOCK_ID"=>$GEO_DOMAINS_IBLOCK_ID);
$arLimit = Array('nTopCount'=>1);
$arSelect = Array("ID", "NAME", "PROPERTY_CITY");
$res = CIBlockElement::GetList(Array(), $arFilter, false, $arLimit, $arSelect);  
if ($arFields = $res->Fetch()) {  
	$SELECT_CITY = $arFields['PROPERTY_CITY_VALUE'];
}


include($_SERVER['DOCUMENT_ROOT'].'/catalog/redirect_rules.php');	// $REDIRECTS
		
$URL = !empty($_SERVER['REDIRECT_URL']) ? $_SERVER['REDIRECT_URL'] : $_SERVER['DOCUMENT_URI'];

if (isset($REDIRECTS[$URL])) {
	if ($REDIRECTS[$URL][0] == 0) {
		// cat
		$arFilter = Array("UF_CAT_CODE" => $REDIRECTS[$URL][1], "ACTIVE"=>"");
		$items = GetIBlockSectionList($CATALOG_IBLOCK_ID, false, Array("sort"=>"asc"), 1, $arFilter);
		if ($arItem = $items->GetNext()) {
			$SEND_URL = nfGetCurPageParam($SELECT_CITY ? 'CITY='.$SELECT_CITY : '', ['CITY'], false, $arItem['SECTION_PAGE_URL'].(!empty($_SERVER['QUERY_STRING']) ? '?'.$_SERVER['QUERY_STRING'] : ''));
			LocalRedirect($SEND_URL, false, '301 Moved Permanently');
		}
	} else {
		// item
		$arFilter = Array("IBLOCK_ID"=>$CATALOG_IBLOCK_ID, "PROPERTY_CODE" => $REDIRECTS[$URL][1], "ACTIVE"=>"");
		$arSelectFields = ['ID', 'DETAIL_PAGE_URL', 'LIST_PAGE_URL'];
		$res = GetIBlockElementListEx($CATALOG_IBLOCK_TYPE, $CATALOG_IBLOCK_ID, false, false, 1, $arFilter, $arSelectFields, false); 
		if ($arFields = $res->GetNext()) {
			$SEND_URL = nfGetCurPageParam($SELECT_CITY ? 'CITY='.$SELECT_CITY : '', ['CITY'], false, $arFields['DETAIL_PAGE_URL'].(!empty($_SERVER['QUERY_STRING']) ? '?'.$_SERVER['QUERY_STRING'] : ''));
			LocalRedirect($SEND_URL, false, '301 Moved Permanently');
		}
	}
}
unset($REDIRECTS);

$URL = explode('/', $URL);
// p($URL);

$CLEAR_URL = [];
foreach(array_reverse($URL) as $link) {
	if (!empty($link)) {
		$CLEAR_URL[] = $link;
	}
}
/*if ($CLEAR_URL[count($CLEAR_URL)-1] == 'catalog') {
	unset($CLEAR_URL[count($CLEAR_URL)-1]);
	
	if (substr($CLEAR_URL[0], -5, 5) == '.html') {
		$FIND_ID = substr($CLEAR_URL[0], 0, -5);
		// p('html');
		// p('$FIND_ID is '.$FIND_ID);
		
		$arFilter = Array("IBLOCK_ID"=>$CATALOG_IBLOCK_ID, "=PROPERTY_CODE" => $FIND_ID);
		$arSelectFields = ['ID', 'DETAIL_PAGE_URL', 'LIST_PAGE_URL'];
		$res = GetIBlockElementListEx($CATALOG_IBLOCK_TYPE, $CATALOG_IBLOCK_ID, false, false, 1, $arFilter, $arSelectFields, false); 
		if ($arFields = $res->GetNext()) {
			// p($arFields);
			$SEND_URL = nfGetCurPageParam($SELECT_CITY ? 'CITY='.$SELECT_CITY : '', ['CITY'], false, $arFields['DETAIL_PAGE_URL'].(!empty($_SERVER['QUERY_STRING']) ? '?'.$_SERVER['QUERY_STRING'] : ''));
			LocalRedirect($SEND_URL, false, '301 Moved Permanently');
		}
	} else {
		
		// p('not html');
		
		$arFilter = Array("IBLOCK_ID"=>$CATALOG_IBLOCK_ID, "PROPERTY_OLD_CODE1" => $CLEAR_URL[0]);
		// p($arFilter);
		$arSelectFields = ['ID', 'DETAIL_PAGE_URL', 'LIST_PAGE_URL'];
		$res = GetIBlockElementListEx($CATALOG_IBLOCK_TYPE, $CATALOG_IBLOCK_ID, false, false, 1, $arFilter, $arSelectFields, false); 
		if ($arFields = $res->GetNext()) {
			// p('old code');
			// p($arFields);
			$SEND_URL = nfGetCurPageParam($SELECT_CITY ? 'CITY='.$SELECT_CITY : '', ['CITY'], false, $arFields['DETAIL_PAGE_URL'].(!empty($_SERVER['QUERY_STRING']) ? '?'.$_SERVER['QUERY_STRING'] : ''));
			LocalRedirect($SEND_URL, false, '301 Moved Permanently');
		} else {
			// p('new section');
			
			$arFilter = Array(
				// array(
					// "LOGIC" => "OR",
					// array("UF_CAT_OLD_URL" => $CLEAR_URL[0]),
					// array("UF_CAT_NEW_URL" => $CLEAR_URL[0]),
				// ),
				"UF_CAT_NEW_URL" => $CLEAR_URL[0],
			);
			$items = GetIBlockSectionList($CATALOG_IBLOCK_ID, false, Array("sort"=>"asc"), 1, $arFilter);
			if ($arItem = $items->GetNext()) {
				// p($arItem);
				$SEND_URL = nfGetCurPageParam($SELECT_CITY ? 'CITY='.$SELECT_CITY : '', ['CITY'], false, $arItem['SECTION_PAGE_URL'].(!empty($_SERVER['QUERY_STRING']) ? '?'.$_SERVER['QUERY_STRING'] : ''));
				LocalRedirect($SEND_URL, false, '301 Moved Permanently');
			} else {
				
				// p('old section');
				
				$SEARCH_ARR = array_reverse($CLEAR_URL);
				$CUR_INDEX = 0;
				$arSections = [];
				
				foreach($SEARCH_ARR as $CUR_INDEX => $SEARCH_LINK) {
					$arSort = Array('depth_level' => 'asc', 'SORT' => 'asc', 'NAME' => 'asc', 'XML_ID' => 'asc');
					$arFilter = Array("IBLOCK_ID"=>$CATALOG_IBLOCK_ID);
					$arFilter["UF_CAT_OLD_URL"] = $SEARCH_LINK;
					$arFilter["DEPTH_LEVEL"] = $CUR_INDEX + 1;
					if ($CUR_INDEX != 0 && !empty($arSections[$CUR_INDEX-1])) {
						$arFilter["SECTION_ID"] = $arSections[$CUR_INDEX-1]['ID'];
					}
					$arSelect = Array("ID", "NAME", "CODE", "XML_ID", "SECTION_PAGE_URL", "IBLOCK_SECTION_ID", "DEPTH_LEVEL", "UF_CAT_OLD_URL", "UF_CAT_NEW_URL");
					$res = CIBlockSection::GetList(Array(), $arFilter, false, $arSelect);  
					if ($arFields = $res->Fetch()) {
						$arSections[$CUR_INDEX] = $arFields;
					} else {
						break;
					}
				}
				
				// p($arSections);
				
				if (count($arSections) == count($SEARCH_ARR)) {
					$arFilter = Array(
						"ID" => $arSections[count($arSections)-1]['ID'],
					);
					$items = GetIBlockSectionList($CATALOG_IBLOCK_ID, false, Array("sort"=>"asc"), 1, $arFilter);
					if ($arItem = $items->GetNext()) {
						// p($arItem);
						$SEND_URL = nfGetCurPageParam($SELECT_CITY ? 'CITY='.$SELECT_CITY : '', ['CITY'], false, $arItem['SECTION_PAGE_URL'].(!empty($_SERVER['QUERY_STRING']) ? '?'.$_SERVER['QUERY_STRING'] : ''));
						LocalRedirect($SEND_URL, false, '301 Moved Permanently');
					}
				}
				
			}
			
		}
	}
	
}*/
// p($CLEAR_URL);


?>
	
<!-- roll -->
<div class="roll txt-block">
	<?$APPLICATION->IncludeFile("include/404_text.php", Array(), Array("MODE"=>"html"));?>
	
	<?$APPLICATION->IncludeFile("include/404_search_form.php", Array(), Array("MODE"=>"html"));?>
	
	<?$APPLICATION->IncludeFile("include/404_text_bottom.php", Array(), Array("MODE"=>"html"));?>
	
</div>
<!-- / roll end -->

<?$APPLICATION->IncludeFile("include/pop-cat.php", Array(), Array("MODE"=>"php"));?>

<?
$OTHER_DEALS_ROLL_NAME = "Популярные товары";
?>
<?$APPLICATION->IncludeFile("include/other-deals-roll.php", Array(), Array("MODE"=>"php"));?>

<?$APPLICATION->IncludeFile("include/need_help.php", Array(), Array("MODE"=>"php"));?>


<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>