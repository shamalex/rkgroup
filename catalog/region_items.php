<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Товары, доступные в каталоге");

// if (!$USER->IsAdmin()) {
	// $APPLICATION->RestartBuffer();
	// die('admins only!');
// }

CModule::IncludeModule('iblock');
CModule::IncludeModule('catalog');
@ini_set('memory_limit', '4096M');
@set_time_limit(595);

$made_els = [];
$made_els_total = [];

$CATALOG_IBLOCK_ID = 8;
$BRANDS_IBLOCK_ID = 9;
$GEO_IBLOCK_ID = 15;

// $arrFilter = [];

// if (!empty($arGeoData['CUR_CITY']['PRICE_ID'])) {
	// $arrFilter['>CATALOG_PRICE_'.$arGeoData['CUR_CITY']['PRICE_ID']] = '0';
// }

$QUANITY_PROP = strtoupper('COUNT_'.$arGeoData['CUR_CITY']['CITY_CODE1']);

if (!empty($arGeoData['CUR_CITY']['PRICE_ID'])) {
	$res = CPrice::GetList(
		array(),
		array(
			// "PRODUCT_ID" => $PRODUCT_ID,
			"CATALOG_GROUP_ID" => [$arGeoData['CUR_CITY']['PRICE_ID'], $arGeoData['CUR_CITY']['PRICE_NODISCOUNT_ID']]
		)
	);
	// p('CPrice::GetList cnt is '.$res->SelectedRowsCount());

	$FIRST_ID = 0;
	
	while ($arr = $res->Fetch()) {
		// p($arr);
		if (!$FIRST_ID) {
			$FIRST_ID = $arr['PRODUCT_ID'];
		}
		$made_els[$arr['PRODUCT_ID']]['Price'][$arr['CATALOG_GROUP_ID']] = $arr;
		// break;
	}
	
	$catalogPrices = [];

	$dbPriceType = CCatalogGroup::GetList(
		array("SORT" => "ASC")
	);
	while ($arPriceType = $dbPriceType->Fetch()) {
		$catalogPrices[$arPriceType['ID']] = $arPriceType['NAME'];
	}
	
	
	if ($_REQUEST['show_all_prices'] == 'Y') {
		$res = CPrice::GetList(
			array(),
			array(
				"PRODUCT_ID" => array_keys($made_els),
				// "PRODUCT_ID" => $FIRST_ID,
				// "CATALOG_GROUP_ID" => [$arGeoData['CUR_CITY']['PRICE_ID'], $arGeoData['CUR_CITY']['PRICE_NODISCOUNT_ID']]
			)
		);
		// p('CPrice::GetList cnt is '.$res->SelectedRowsCount());

		while ($arr = $res->Fetch()) {
			// p($arr);
			$made_els_total[$arr['PRODUCT_ID']]['Price'][$catalogPrices[$arr['CATALOG_GROUP_ID']]] = $arr['PRICE'];
			// $made_els_total[$arr['PRODUCT_ID']]['Price'][$arr['CATALOG_GROUP_NAME']] = $arr['PRICE'];
			// break;
		}
	}


	// $arFilter = Array("IBLOCK_ID"=>$CATALOG_IBLOCK_ID, "ID"=>array_keys($made_els));
	$arFilter = Array("IBLOCK_ID"=>$CATALOG_IBLOCK_ID, '>CATALOG_PRICE_'.$arGeoData['CUR_CITY']['PRICE_ID']=>'0');
	$arSelect = Array("ID", "ACTIVE", "NAME", "CODE", "XML_ID", "DETAIL_PAGE_URL", "PROPERTY_CODE", "PROPERTY_NS_CODE", "PROPERTY_FULL_NAME", "PROPERTY_ITEM_NAME", 'PROPERTY_'.$QUANITY_PROP);
	// $arLimit = ['iNumPage' => $cur_page, 'nPageSize' => 10];
	$arLimit = false;
	$res = CIBlockElement::GetList(Array(), $arFilter, false, $arLimit, $arSelect);  
	while($arFields = $res->GetNext()) {
		$made_els[$arFields['ID']]['IBlockElement'] = $arFields;
	}
}

$arRegions_codes = [];
$arCurRegion = [];
$arFilter = Array("IBLOCK_ID"=>$GEO_IBLOCK_ID);
$arSelect = ['ID', 'NAME', 'PROPERTY_network_name', 'PROPERTY_network_data_code', 'PROPERTY_filial1', 'PROPERTY_filial2', 'PROPERTY_filial1_code', 'PROPERTY_filial2_code'];
$res = CIBlockElement::GetList(Array(), $arFilter, false, false, $arSelect);  
while($arFields = $res->Fetch()) {
	if ($arFields['ID'] == $arGeoData['CUR_CITY']['ID']) {
		$arCurRegion = $arFields;
	}
	if (!empty($arFields['PROPERTY_NETWORK_DATA_CODE_VALUE'])) {
		$arRegions_codes[$arFields['PROPERTY_NETWORK_DATA_CODE_VALUE']]++;
	}
}

// ksort($arRegions_codes);
// p($arRegions_codes);


// p($made_els['977']);


// p($arGeoData['CUR_CITY']);

?>

<style>
	#table {
		border-collapse: collapse;
		border: 1px solid black;
		margin-bottom: 20px;
		width: 100%;
	}
	#table pre {
		white-space: pre-wrap;
	}
	#table tr th {
		padding-left: 5px;
		padding-right: 5px;
	}
	#table tr td {
		text-aling: left;
		vertical-align: top;
		padding: 10px;
	}
	#table tr td.item_desc {
		min-width: 300px;
	}
	#table tr td.item_desc a {
		text-decoration: underline;
	}
	#table tr td.active_bad {
		background-color: red;
		color: white;
	}
	#table .txt_right {
		text-align: right;
	}
	.mid.inner.line {
		width: 1110px;
		margin: 0 auto;
	}
	.right_link {
		float: right;
		margin: 0;
	}
	.right_link a {
		text-decoration: underline;
	}
	.right_link a:hover {
		text-decoration: none;
	}
</style>

<p class="right_link">
	<? if($_REQUEST['show_all_prices'] != 'Y'): ?>
		<a href="<?= nfGetCurPageParam('show_all_prices=Y', ['show_all_prices']); ?>">Показать с диапазонами цен всех городов (долго)</a>
	<? else: ?>
		<a href="<?= nfGetCurPageParam('', ['show_all_prices']); ?>">Показать без диапазонов цен всех городов (быстро)</a>
	<? endif; ?>
</p>

<p>
Регион: <?= $arGeoData['CUR_CITY']['REGION'] ?>
</p>

<p>
Город: <?= $arGeoData['CUR_CITY']['NAME'] ?>
</p>

<p>
Куст: <?= $arCurRegion['PROPERTY_NETWORK_NAME_VALUE'] ?>
</p>

<p>
Код цен, валют, самовывоза: <?= $arGeoData['CUR_CITY']['CITY_CODE'] ?>
</p>

<p>
Код остатков: <?= $arGeoData['CUR_CITY']['CITY_CODE1'] ?>
</p>

<?/*
<p>
Код остатков 2: <?= $arGeoData['CUR_CITY']['CITY_CODE2'] ?>
</p>
*/?>

<table cellpadding="10" cellspacing="0" border="1" id="table">
	<tr>
		<th>
			#
		</th>
		<th>
			Товар
		</th>
		<?/*<th>
			Активность
		</th>*/?>
		<th>
			Код
		</th>
		<th>
			НС-код
		</th>
		<th>
			Цена
		</th>
		<th>
			Цена <br>без <br>скидки
		</th>
		<? if(!empty($made_els_total)): ?>
			<th>
				Диапазон <br>цен
			</th>
		<? endif; ?>
		<th>
			Наличие
		</th>
	</tr>
	<?
	$cnt = 1;
	?>
	<? foreach($made_els as $ID => $arItem): ?>
		<tr>
			<td class="txt_right">
				<?= $cnt ?>
			</td>
			<td>
				<?
				$NAME = $arItem['IBlockElement']['NAME'];
				if (!empty($arItem['IBlockElement']['PROPERTY_ITEM_NAME_VALUE'])) {
					$NAME = $arItem['IBlockElement']['PROPERTY_ITEM_NAME_VALUE'];
				} elseif (!empty($arItem['IBlockElement']['PROPERTY_FULL_NAME_VALUE'])) {
					$NAME = $arItem['IBlockElement']['PROPERTY_FULL_NAME_VALUE'];
				}
				?>
				<a href="<?= $arItem['IBlockElement']['DETAIL_PAGE_URL'] ?>" target="_blank"><?= $NAME ?></a>
			</td>
			<?/*<td class="<?= $arItem['IBlockElement']['ACTIVE'] == 'Y' ? 'active_ok' : 'active_bad' ?>">
				<?= ($arItem['IBlockElement']['ACTIVE'] == 'Y' ? 'Да' : 'Нет') ?>
			</td>*/?>
			<td>
				<?= $arItem['IBlockElement']['PROPERTY_CODE_VALUE'] ?>
			</td>
			<td>
				<?= $arItem['IBlockElement']['PROPERTY_NS_CODE_VALUE'] ?>
			</td>
			<td>
				<?= floatval($arItem['Price'][$arGeoData['CUR_CITY']['PRICE_ID']]['PRICE']) ?>
			</td>
			<td>
				<?= !empty($arItem['Price'][$arGeoData['CUR_CITY']['PRICE_NODISCOUNT_ID']]['PRICE']) ? floatval($arItem['Price'][$arGeoData['CUR_CITY']['PRICE_NODISCOUNT_ID']]['PRICE']) : '-' ?>
			</td>
			<? if(!empty($made_els_total)): ?>
				<td>
					<?
					$one = [];
					foreach($made_els_total[$ID]['Price'] as $code => $price) {
						$real_code = $code;
						$is_disc = strpos($code, '_NODISCOUNT') !== false;
						if ($is_disc) {
							$real_code = str_replace('_NODISCOUNT', '', $code);
						}
						if (isset($arRegions_codes[$real_code])) {
							if (intval($price) != floatval($arItem['Price'][$arGeoData['CUR_CITY']['PRICE_ID']]['PRICE'])) {
								echo $real_code;
								if ($is_disc) {
									echo ' без скидки';
								}
								echo ': ';
								echo intval($price);
								echo '<br />';
							} else {
								$one[] = $real_code.($is_disc ? ' без скидки' : '');
							}
						}
					}
					if (!empty($one)) {
						echo 'Одинаковые цены: ';
						echo implode(', ', $one);
					}
					// p($made_els_total[$ID]['Price']);
					?>
				</td>
			<? endif; ?>
			<td>
				<?= !empty($arItem['IBlockElement']['PROPERTY_'.$QUANITY_PROP.'_VALUE']) ? 'true' : 'false' ?>
			</td>
		</tr>
		<?
		$cnt++;
		// break;
		?>
	<? endforeach; ?>
</table>



<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>