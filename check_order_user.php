<?
header("Access-Control-Allow-Origin: *");
header("Access-Control-Allow-Methods: GET, POST");
header("Connection:keep-alive");
header("Content-Type: application/json; charset=utf-8");
include $_SERVER['DOCUMENT_ROOT'].'/bitrix/modules/main/include/prolog_before.php';

$result=array("action"=>$_REQUEST["action"]);
if($_REQUEST["action"]=="check_user"){
	$filter=array();

	if($_REQUEST["phone"]) $filter["PERSONAL_PHONE"]=$_REQUEST["phone"];
	/*elseif($_REQUEST["email"]) $filter["EMAIL"]=$_REQUEST["email"];*/

	if(count($filter)>0){
		$rsUsers = CUser::GetList(($by="id"), ($order="asc"), $filter, array("SELECT"=>array("UF_*")));
		if($arUser=$rsUsers->GetNext()){
			$user = new CUser;
			$fields = Array( 
				"UF_SMS_CODE" => randString(5,"0123456789"), 
			); 
			if($user->Update($arUser["ID"], $fields)){
				$result["status"]="Y";
			}else{
				$result["message"]=$user->LAST_ERROR;
				$result["status"]="E";
			}
		}else{
			$result["status"]="N";
		}
	}else{
		$result["status"]="N";
	}
}elseif($_REQUEST["action"]=="send_code"){
	$filter=array();
	if($_REQUEST["email"]) $filter["EMAIL"]=$_REQUEST["email"];
	if($_REQUEST["phone"]) $filter["PERSONAL_PHONE"]=$_REQUEST["phone"];
	$rsUsers = CUser::GetList(($by="id"), ($order="asc"), $filter,array("SELECT"=>array("UF_*")));
	if($arUser=$rsUsers->GetNext()){
		if(CModule::IncludeModule("smsc.sms")) {
			$sms = new SMSC_Send;
			$sms->Send_SMS($arUser["PERSONAL_PHONE"],"РУСКЛИМАТ.РУ: используйте код подтверждения входа -  ".$arUser["UF_SMS_CODE"]);
			$result["status"]="Y";
		}else{
			$result["status"]="E";
			$result["message"]="Ошибка работы сервиса, повторите попытку позже";
		}
	}else{
		$result["status"]="E";
		$result["message"]="Пользователь не найден";
	}
}elseif($_REQUEST["action"]=="check_code"){
	$filter=array();
	if($_REQUEST["email"]) $filter["EMAIL"]=$_REQUEST["email"];
	if($_REQUEST["phone"]) $filter["PERSONAL_PHONE"]=$_REQUEST["phone"];
	$rsUsers = CUser::GetList(($by="id"), ($order="asc"), $filter,array("SELECT"=>array("UF_*")));
	if($arUser=$rsUsers->GetNext()){
		if($arUser["UF_SMS_CODE"] == $_REQUEST["code"]){
			$USER->Authorize($arUser["ID"]);
			$result["status"]="Y";
			$result["message"]="Код подтвержден";
			$result["phone"]=$arUser["PERSONAL_PHONE"];
			$result["email"]=$arUser["EMAIL"];
			$result["name"]=$arUser["LAST_NAME"]." ".$arUser["NAME"]." ".$arUser["SECOND_NAME"];
		}else{
			$result["status"]="N";
			$result["message"]="Введен неверный код";
		}
	}else{
		$result["status"]="E";
		$result["message"]="Пользователь не найден";
	}
}

echo json_encode($result);?>