<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Точки самовывоза");
?>


<!-- roll -->
<div class="roll">
	<h1 class="ttl1">Точки самовывоза</h1>
	
	<!-- gmap -->
	<div class="map">
		<!-- gmap header -->
		<div class="map-hd c">
			<div class="map-city l">
				<div class="map-city-ttl">Ваш город</div>
				<select name="" id="map-select">
					<option value="">Населенный</option>
					<option value="">Пункт</option>
					<option value="">Александровск-Сахалинский</option>
					<option value="">Населенный</option>
					<option value="">Электрозаводск</option>
					<option value="">Населенный</option>
					<option value="">Населенный</option>
					<option value="">Пункт</option>
					<option value="">Александровск-Сахалинский</option>
					<option value="">Населенный</option>
					<option value="">Электрозаводск</option>
					<option value="">Населенный</option>
				</select>
			</div>
		</div>
		<!-- / gmap header end -->
		
		<!-- gmap holder (gmap.js) -->
		<div  id="map" class="map-container map-inner"></div>
		<!-- / gmap holder end -->
	</div>
	<!-- / gmap end -->
</div>
<div class="roll">
	<!-- contacts -->
	<div class="contacts">
	
		<!-- item -->
		<div class="item">
			<!-- item .w-->
			<div class="w c">
				<div class="pic" style="background-image:url('<?= SITE_TEMPLATE_PATH ?>/i/blocks/contact2.png');"></div>
				<div class="dsc">
					<h2 class="ttl c"><div class="ico ico-1"></div>Климатический центр Русклимат</h2>
					<div class="inf">
						<p>На площади 300 м2 представлен полный ассортимент климатической техники: кондиционеры, водонагреватели, электрические обогреватели, теплые полы, радиаторы, увлажнители и осушители воздуха, воздухоочистители, системы питьевой и механической очистки воды, вытяжные вентиляторы, насосы и водяная арматура, котельное и вентиляционное оборудование.</p>
						<p>На площади 300 м2 представлен полный ассортимент климатической техники: кондиционеры, водонагреватели, электрические обогреватели, теплые полы, радиаторы, увлажнители и осушители воздуха, воздухоочистители, системы питьевой и механической очистки воды, вытяжные вентиляторы, насосы и водяная арматура, котельное и вентиляционное оборудование.</p>
					</div>
					<table class="tbl-contact">
						<tr>
							<td class="col-1">
								<p class="tl">Адрес</p>
								<p>Кунцевская, 50-й км МКАД, (внешняя сторона), развязка Очаково-Заречье, Магазин: 1-й этаж, центр зала, магазин В-20</p>
							</td>
							<td class="col-2">
								<p class="tl">График работы</p>
								<p>Пн - Пт: 9.00-19.00, Сб - 9.00-16.00</p>
							</td>
							<td>
								<p class="tl">Контакты</p>
								<p>8 (495) 777-19-77</p>
							</td>
						</tr>
					</table>
				</div>
				<!-- dsc end -->
			</div>
			<!-- / item .w -->
		</div>
		<!-- / item -->
		
		<!-- item -->
		<div class="item">
			<!-- item .w-->
			<div class="w c">
				<div class="pic" style="background-image:url('<?= SITE_TEMPLATE_PATH ?>/i/blocks/contact1.png');"></div>
				<div class="dsc">
					<h2 class="ttl c"><div class="ico ico-3"></div>Климатический центр Русклимат Климатический центр Русклимат Климатический центр Русклимат Климатический центр Русклимат</h2>
					<div class="inf">
						<p>На площади 300 м2 представлен полный ассортимент климатической техники: кондиционеры, водонагреватели, электрические обогреватели, теплые полы, радиаторы, увлажнители и осушители воздуха, воздухоочистители, системы питьевой и механической очистки воды, вытяжные вентиляторы, насосы и водяная арматура, котельное и вентиляционное оборудование.</p>
					</div>
					<table class="tbl-contact">
						<tr>
							<td class="col-1">
								<p class="tl">Адрес</p>
								<p>Кунцевская, 50-й км МКАД, (внешняя сторона), развязка Очаково-Заречье, Магазин: 1-й этаж, центр зала, магазин В-20</p>
							</td>
							<td class="col-2">
								<p class="tl">График работы</p>
								<p>Пн - Пт: 9.00-19.00, Сб - 9.00-16.00</p>
							</td>
							<td>
								<p class="tl">Контакты</p>
								<p>8 (495) 777-19-77</p>
							</td>
						</tr>
						<tr>
							<td class="col-1">
								<p class="tl">парковка</p>
								<p>150 парковочных мест</p>
							</td>
							<td class="col-2">
								<p class="tl">Способы оплаты</p>
								<p>Наличный расчет, Visa, Master Card</p>
							</td>
							<td>
								&nbsp;
							</td>
						</tr>
					</table>
				</div>
				<!-- dsc end -->
			</div>
			<!-- / item .w -->
		</div>
		<!-- / item -->
		
		<!-- item -->
		<div class="item">
			<!-- item .w-->
			<div class="w c">
				<div class="pic" style="background-image:url('<?= SITE_TEMPLATE_PATH ?>/i/blocks/contact3.png');"></div>
				<div class="dsc">
					<h2 class="ttl c"><div class="ico ico-2"></div>Русклимат</h2>
					<div class="inf">
						<p>Системы питьевой и механической очистки воды, вытяжные вентиляторы, насосы и водяная арматура, котельное и вентиляционное оборудование.</p>
					</div>
					<table class="tbl-contact">
						<tr>
							<td class="col-1">
								<p class="tl">Адрес</p>
								<p>Кунцевская, 50-й км МКАД, (внешняя сторона), развязка Очаково-Заречье, Магазин: 1-й этаж, центр зала, магазин В-20</p>
							</td>
							<td class="col-2">
								<p class="tl">График работы</p>
								<p>Пн - Пт: 9.00-19.00, Сб - 9.00-16.00</p>
							</td>
							<td>
								<p class="tl">Контакты</p>
								<p>8 (495) 777-19-77</p>
							</td>
						</tr>
						<tr>
							<td class="col-1">
								<p class="tl">парковка</p>
								<p>150 парковочных мест</p>
							</td>
							<td class="col-2">
								<p class="tl">Способы оплаты</p>
								<p>Наличный расчет, Visa, Master Card</p>
							</td>
							<td>
								&nbsp;
							</td>
						</tr>
					</table>
				</div>
				<!-- dsc end -->
			</div>
			<!-- / item .w -->
		</div>
		<!-- / item -->
		
	</div>
	<!-- / contacts end -->


</div>
<!-- / roll end -->

<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>