<?
include $_SERVER['DOCUMENT_ROOT'].'/bitrix/modules/main/include/prolog_before.php';

if (!CSite::InGroup(array(1, 6, 7))) {
	LocalRedirect('/personal/');
}

header('Content-type: application/xml');

echo '<?xml version="1.0"?>';
echo "\n";
?>
