<?
include $_SERVER['DOCUMENT_ROOT'].'/bitrix/modules/main/include/prolog_before.php';

CModule::IncludeModule('iblock');

$FAV_COOKIE_NAME = 'FAVORITES';

$FAV_ELEMENTS = $APPLICATION->get_cookie($FAV_COOKIE_NAME);
if (!empty($FAV_ELEMENTS)) {
	$FAV_ELEMENTS = explode('.', $FAV_ELEMENTS);
	
} else {
	$FAV_ELEMENTS = [];
}
$itemCount = count($FAV_ELEMENTS);


$ID = $_REQUEST['ID'];
$ACTION = $_REQUEST['action'] == 'del' ? 'del' : 'add';

if (!empty($ID)) {
	$ID = intval($ID);
	if ($ID > 0) {
		$was = false;
		global $USER;
		$rsUser = CUser::GetByID($USER->GetID()); 
		$arUser = $rsUser->Fetch();
		$fav=unserialize($arUser["UF_FAV"]);
		
		if (in_array(strval($ID), $FAV_ELEMENTS)) {
			unset($FAV_ELEMENTS[array_search(strval($ID), $FAV_ELEMENTS)]);
			unset($fav[$ID]);
			$was = true;
			$itemCount--;
		}
		if ($ACTION == 'add') {
			array_unshift($FAV_ELEMENTS, $ID);
			$fav[$ID]=time();
			$itemCount++;
		}
		asort($fav);
		$user = new CUser;
		$fields = Array( 
			"UF_FAV" => serialize($fav), 
		); 
		$user->Update($USER->GetID(), $fields);
		
		$FAV_ELEMENTS = array_slice($FAV_ELEMENTS, 0, 60);
		$FAV_ELEMENTS_TXT = implode('.', $FAV_ELEMENTS);
		$APPLICATION->set_cookie($FAV_COOKIE_NAME, $FAV_ELEMENTS_TXT);
	}
}

$productS_txt = getNumEnding($itemCount, ['товар', 'товара', 'товаров']);

?>
<div class="fav item <?= $itemCount > 0 ? '' : 'empty' ?>" data-full="<?= $itemCount > 0 ? '1' : '0' ?>">
	<div class="i-w">
	
		<a class="lnk" href="/catalog/favorites/"><span>Избранное</span></a>
		
		<div class="inf">
			<span class="count favCount"><?= $itemCount; ?></span>
			<span class="txt favTxt"><?= $productS_txt ?></span>
		</div>
		
		<div id="favHint" class="ttp"><div class="w">Товар добавлен в избранное!</div></div>
		<div id="favHint2" class="ttp"><div class="w">Товар удален из избранного!</div></div>
		
	</div>
	<script>
		$(window).trigger('scroll');
		<? foreach($FAV_ELEMENTS as $FAV_ELEMENT): ?>
			$('#addFav_<?= intval($FAV_ELEMENT) ?>, #addFav2_<?= intval($FAV_ELEMENT) ?>').attr('checked', true).trigger('refresh');
			$("label[for=addFav_<?= intval($FAV_ELEMENT) ?>],label[for=addFav2_<?= intval($FAV_ELEMENT) ?>]").text("В избранном");
		<? endforeach; ?>
	</script>
</div>