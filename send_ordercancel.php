<?
header("Access-Control-Allow-Origin: *");
header("Access-Control-Allow-Methods: GET, POST");
header("Connection:keep-alive");
header("Content-Type: application/json; charset=utf-8");
include ($_SERVER["DOCUMENT_ROOT"]. "/bitrix/modules/main/include/prolog_before.php");

$mess="";

$mess='<div class="success">
	Ваш отказ по заказу отправлен.<br />Ждем Ваших новых заказов.
</div>
';	




$messages=array(
	"ru"=>array(
		"check_user" => "Вы не можете отменить этот заказ",
		"db_error"=>"Ошибка отмены заказа, пожалуйста, повторите попытку!",
		"success"=>$mess,
	),
	"en"=>array(
		"check_user" => "Access denied",
		"db_error"=>"An error has occurred when sending the request. ",
		"success"=>	"Request sent successfully",
	)
);

global $USER;
CModule::IncludeModule("sale");

$error=array();

if(!$_REQUEST["lang"])$_REQUEST["lang"]="ru";

if($_REQUEST["subj"]){
	$error[]=$messages[$_REQUEST["lang"]]["db_error"];
}else{
	if(!$_REQUEST["orderId"]) $error[]=$messages[$_REQUEST["lang"]]["db_error"];
	else{
		$arOrder = CSaleOrder::GetByID($_REQUEST["orderId"]);
		
		if(!$arOrder || $USER->GetID()!=$arOrder["USER_ID"]) $error[]=$messages[$_REQUEST["lang"]]["check_user"];
	}

}
if(!$error)
{
	CModule::IncludeModule("iblock");
	$arLoadArray=array(
		"IBLOCK_ID"=>36,
		"ACTIVE"=>"Y",
		"ACTIVE_FROM" => date("d.m.Y H:i:s"),
		"NAME" => "Отмена заказа ".$_REQUEST["orderId"],
		"PROPERTY_VALUES"=>array(
			"order" => $_REQUEST["orderId"],
		),
	);
	
	$el=new CIBlockElement;
	if(!$res = $el->Add($arLoadArray)){
		$error[]=$messages[$_REQUEST["lang"]]["db_error"].$el->LAST_ERROR;
	}
}

if(count($error)==0){
	// отправка в шлюз
	SendDataToExternalServer(
		"ORDER_CANCEL",
		array(
			"type"=>"order_cancel",
			"type_text"=>"Хочу отменить",
			"user"=>$USER->GetID(),
			"orderId"=>$_REQUEST["orderId"],
			"siteId"=>SITE_ID
		)
	);	
	echo json_encode(array("status"=>"ok","message"=>$messages[$_REQUEST["lang"]]["success"]));
}
else{
	echo json_encode(array("status"=>"error","message"=>'<div class="error">'.$messages[$_REQUEST["lang"]]["db_error"].'</div>',"error"=>$error));
}
?>