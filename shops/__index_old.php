<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetPageProperty("keywords", "");
$APPLICATION->SetPageProperty("description", "Адреса магазинов. Выгодные предложения, привлекательные цены, большой ассортимент выбора в интернет магазине www.rusklimat-omsk.ru, тел: 8(800) 777-19-77");
$APPLICATION->SetPageProperty("title", "Где купить продукцию Русклимат в Омске");
$APPLICATION->SetTitle("Адреса магазинов");
?><!-- contacts -->
		<div class="contacts">
		
			<!-- item -->
			<div class="item">
				<!-- item .w-->
				<div class="w c">
					<div class="pic" style="background-image:url('/bitrix/templates/rusklimat/i/blocks/contact3.png');"></div>
					<div class="dsc">
						<h2 class="ttl c"><div class="ico ico-1"></div>Интернет-магазин "Русклимат-Омск"</h2>
						<div class="inf">
							<!-- <p>...</p> -->
						</div>
						<table class="tbl-contact">
							<tr>
								<td class="col-1">
									&nbsp;
								</td>
								<td class="col-2">
									<p class="tl">График работы</p>
									<p>ежедневно: 9:00 – 20:00</p>
								</td>
								<td>
									<p class="tl">Телефон для заказов</p>
									<p>8 (800) 777-19-77,<br>звонок бесплатный </p>
								</td>
							</tr>
							<tr>
								<td class="col-1">
									<p class="tl">E-mail</p>
									<p><a itemprop="email" href="mailto:online@rusklimat.ru">online@rusklimat.ru</a></p>
								</td>
								<td class="col-2">
									<p class="tl">Способы оплаты</p>
									<p>наличные, перевод <br>для физических и юридических лиц</p>
								</td>
								<td>
									&nbsp;
								</td>
							</tr>
						</table>
					</div>
					<!-- dsc end -->
				</div>
				<!-- / item .w -->
			</div>
			<!-- / item -->

			<!-- item -->
			<div class="item">
				<!-- item .w-->
				<div class="w c">
					<div class="pic" style="background-image:url('/bitrix/templates/rusklimat/i/blocks/contact3.png');"></div>
					<div class="dsc">
						<h2 class="ttl c"><div class="ico ico-1"></div>Склад/офис</h2>
						<div class="inf">
							<!-- <p>...</p> -->
						</div>
						<table class="tbl-contact">
							<tr>
								<td class="col-1">
									<p class="tl">Адрес</p>
									<p>г. Омск, ул. Ипподромная, 2</p>
								</td>
								<td class="col-2">
									<p class="tl">График работы</p>
									<p>пн-пт: 10:00 – 17:00</p>
								</td>
								<td>
									<p class="tl">Телефон для заказов</p>
									<p>8 (800) 777-19-77,<br>звонок бесплатный </p>
								</td>
							</tr>
							<tr>
								<td class="col-1">
									<p class="tl">E-mail</p>
									<p><a itemprop="email" href="mailto:online@rusklimat.ru">online@rusklimat.ru</a></p>
								</td>
								<td class="col-2">
									<p class="tl">Способы оплаты</p>
									<p>наличные, перевод <br>для физических и юридических лиц</p>
								</td>
								<td>
									&nbsp;
								</td>
							</tr>
						</table>
					</div>
					<!-- dsc end -->
				</div>
				<!-- / item .w -->
			</div>
			<!-- / item -->
			
			<!-- item -->
			<div class="item">
				<!-- item .w-->
				<div class="w c">
					<div class="pic" style="background-image:url('/bitrix/templates/rusklimat/i/blocks/contact3.png');"></div>
					<div class="dsc">
						<h2 class="ttl c"><div class="ico ico-1"></div>Розничный магазин</h2>
						<div class="inf">
							<!-- <p>...</p> -->
						</div>
						<table class="tbl-contact">
							<tr>
								<td class="col-1">
									<p class="tl">Адрес</p>
									<p>г. Омск, ул. Карла Маркса, 47</p>
								</td>
								<td class="col-2">
									<p class="tl">График работы</p>
									<p>пн-пт: 10:00 – 19:00,<br>
									сб-вс:  10:00 – 17:00</p>
								</td>
								<td>
									<p class="tl">Телефон для заказов</p>
									<p>8(3812) 46-77-77,<br>8(3812) 46-74-17<br><br>8(800) 777-19-77 звонок бесплатный </p>
								</td>
							</tr>
							<tr>
								<td class="col-1">
									<p class="tl">E-mail</p>
									<a href="mailto:rkm47@omsk.rusklimat.ru">rkm47@omsk.rusklimat.ru</a>
								</td>
								<td class="col-2">
									<p class="tl">Способы оплаты</p>
									<p>наличные, перевод <br>для физических и юридических лиц</p>
								</td>
								<td>
									&nbsp;
								</td>
							</tr>
						</table>
					</div>
					<!-- dsc end -->
				</div>
				<!-- / item .w -->
			</div>
			<!-- / item -->
			
			<!-- item -->
			<div class="item">
				<!-- item .w-->
				<div class="w c">
					<div class="pic" style="background-image:url('/bitrix/templates/rusklimat/i/blocks/contact3.png');"></div>
					<div class="dsc">
						<h2 class="ttl c"><div class="ico ico-1"></div>Розничный магазин</h2>
						<div class="inf">
							<!-- <p>...</p> -->
						</div>
						<table class="tbl-contact">
							<tr>
								<td class="col-1">
									<p class="tl">Адрес</p>
									<p>г. Омск, ул. Октябрьская, 127а</p>
								</td>
								<td class="col-2">
									<p class="tl">График работы</p>
									<p>пн-пт: 10:00 – 18:00,<br>сб: 10:00 – 17:00,<br>вс: 10:00 – 14:00</p>
								</td>
								<td>
									<p class="tl">Телефон для заказов</p>
									<p>8 (800) 777-19-77, звонок бесплатный </p>
								</td>
							</tr>
							<tr>
								<td class="col-1">
									<p class="tl">E-mail</p>
									<p><a itemprop="email" href="mailto:online@rusklimat.ru">online@rusklimat.ru</a></p>
								</td>
								<td class="col-2">
									<p class="tl">Способы оплаты</p>
									<p>наличные, перевод <br>для физических и юридических лиц</p>
								</td>
								<td>
									&nbsp;
								</td>
							</tr>
						</table>
					</div>
					<!-- dsc end -->
				</div>
				<!-- / item .w -->
			</div>
			<!-- / item -->
			
		</div>
		<!-- / contacts end --><?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>