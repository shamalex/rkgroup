<?
include_once($_SERVER['DOCUMENT_ROOT'].'/bitrix/modules/main/include/urlrewrite.php');
include_once($_SERVER['DOCUMENT_ROOT'].'/bitrix/modules/main/include/prolog_before.php');

CModule::IncludeModule('iblock');

$uri = explode('/', $_SERVER['REQUEST_URI']);

if (count($uri) == 3) {
	/*$search_data = $uri[1];
 
	if ($search_data == 'compare') {

		$curParams = [
			'ADD_SECTIONS_CHAIN' => 'N',
			'SEF_FOLDER' => '/',
			'SEF_URL_TEMPLATES' => [
				'sections' => '',
				'section' => './',
				'element' => '#ELEMENT_CODE#/',
				'compare' => 'compare/',
			],
		];

		$APPLICATION->SetPageProperty('CONTENT_PAGE', 'N');
		$APPLICATION->AddChainItem('Каталог', '/catalog/');

		include($_SERVER['DOCUMENT_ROOT'].'/catalog/index.php');
		die();

	}

	$CATALOG_IBLOCK_ID = 8;

	$arSort = [];
	$arFilter = Array("IBLOCK_ID"=>$CATALOG_IBLOCK_ID, "CODE" => $search_data, "GLOBAL_ACTIVE" => "Y", "ACTIVE" => "Y");
	$arSelectFields = ['ID', 'SECTION_PAGE_URL'];
	$res = CIBlockSection::GetList($arSort, $arFilter, false, $arSelectFields);
	if ($arFields = $res->GetNext()) {

		$curParams = [
			'SEF_FOLDER' => '/',
		];

		$APPLICATION->SetPageProperty('CONTENT_PAGE', 'N');
		$APPLICATION->AddChainItem('Каталог', '/catalog/');

		include($_SERVER['DOCUMENT_ROOT'].'/catalog/index.php');
		die();

	}

	$arSort = [];
	$arFilter = Array("IBLOCK_ID"=>$CATALOG_IBLOCK_ID, "CODE" => $search_data, "ACTIVE" => "Y");
	$arSelectFields = ['ID', 'DETAIL_PAGE_URL'];
	$arLimit = ['nTopCount'=>1];
	$res = CIBlockElement::GetList($arSort, $arFilter, false, $arLimit, $arSelect);
	if ($arFields = $res->GetNext()) {

		$curParams = [
			'ADD_SECTIONS_CHAIN' => 'N',
			'SEF_FOLDER' => '/',
			'SEF_URL_TEMPLATES' => [
				'sections' => '',
				'section' => './',
				'element' => '#ELEMENT_CODE#/',
				'compare' => 'catalog/compare/',
			],
		];

		$APPLICATION->SetPageProperty('CONTENT_PAGE', 'N');
		$APPLICATION->AddChainItem('Каталог', '/catalog/');

		include($_SERVER['DOCUMENT_ROOT'].'/catalog/index.php');
		die();
	}*/

} elseif (count($uri) == 4) {
	$search_data = $uri[1];

	$CATALOG_IBLOCK_ID = 8;

	$arSort = [];
	$arFilter = Array("IBLOCK_ID"=>$CATALOG_IBLOCK_ID, "CODE" => $search_data, "GLOBAL_ACTIVE" => "Y", "ACTIVE" => "Y");
	$arSelectFields = ['ID', 'SECTION_PAGE_URL'];
	$res = CIBlockSection::GetList($arSort, $arFilter, false, $arSelectFields);
	if ($arFields = $res->GetNext()) {

		$curParams = [
			'SEF_FOLDER' => '/',
			'SEF_URL_TEMPLATES' => [
				'sections' => '',
				'section' => '#SECTION_CODE#/#BRAND_CODE#/',
				'element' => '#SECTION_CODE#/#BRAND_CODE#/#ELEMENT_CODE#/',
				'compare' => 'catalog/compare/',
			],
		];

		$APPLICATION->SetPageProperty('CONTENT_PAGE', 'N');
		$APPLICATION->AddChainItem('Каталог', '/catalog/');

		include($_SERVER['DOCUMENT_ROOT'].'/catalog/index.php');
		die();

	}

}

$find_url = parse_url($_SERVER['REQUEST_URI']);
$find_url = $find_url['path'];

$GEO_CONTENT = 19;

$arFoundPage = [];
$arSort = ['PROPERTY_IS_DEFAULT' => 'DESC,nulls', 'SORT'=>'ASC', 'ID'=>'DESC'];
$arFilter = Array("IBLOCK_ID"=>$GEO_CONTENT, "=PROPERTY_URL" => $find_url, "ACTIVE" => "Y", [
	['PROPERTY_IS_DEFAULT'=>1],
	['PROPERTY_GEO.CODE'=>$arGeoData['CUR_CITY']['CITY_CODE']],
	'LOGIC' => 'OR',
]);
$arSelect = ['ID', 'NAME', 'DETAIL_TEXT', 'PROPERTY_seo_title', 'PROPERTY_seo_keywords', 'PROPERTY_seo_description', 'PROPERTY_GEO', 'PROPERTY_GEO.CODE', 'PROPERTY_IS_DEFAULT'];
// $arLimit = ['nTopCount'=>2];
$arLimit = false;
$res = CIBlockElement::GetList($arSort, $arFilter, false, $arLimit, $arSelect);
while ($arFields = $res->GetNext()) {
	$arFoundPage[] = $arFields;
}

if (count($arFoundPage) > 1) {
	foreach($arFoundPage as $ID => $page) {
		if ($page['PROPERTY_GEO_CODE'] == $arGeoData['CUR_CITY']['CITY_CODE']) {
			$arFoundPage = $arFoundPage[$ID];
			break;
		}
	}
	if (!isset($arFoundPage['ID'])) {
		$arFoundPage = $arFoundPage[0];
	}
} else {
	$arFoundPage = $arFoundPage[0];
}

if (!empty($arFoundPage) && $arFoundPage['ID']) {
	$APPLICATION->SetPageProperty('CONTENT_PAGE', 'Y');
	$APPLICATION->SetPageProperty('HIDE_BREADCRUMS', 'N');
	$APPLICATION->SetPageProperty("MAIN_CLASS", "inner line");
	$APPLICATION->SetPageProperty("keywords", $arFoundPage['~PROPERTY_SEO_KEYWORDS_VALUE']);
	$APPLICATION->SetPageProperty("description", $arFoundPage['~PROPERTY_SEO_DESCRIPTION_VALUE']);
	$APPLICATION->SetPageProperty("title", $arFoundPage['~PROPERTY_SEO_TITLE_VALUE']);
	$APPLICATION->SetTitle($arFoundPage['~NAME']);

	$uri_arr = explode('/', $find_url);
	unset($uri_arr[count($uri_arr)-1]);
	unset($uri_arr[count($uri_arr)-1]);
	unset($uri_arr[0]);
	if (count($uri_arr) > 0) {
		$found_page = '/';
		foreach($uri_arr as $uri_root) {
			$found_page .= $uri_root.'/';

			if (!file_exists($_SERVER['DOCUMENT_ROOT'].$found_page.'.section.php')) {
				$arFoundPageRoot = [];
				$arSort = ['PROPERTY_IS_DEFAULT' => 'DESC,nulls', 'SORT'=>'ASC', 'ID'=>'DESC'];
				$arFilter = Array("IBLOCK_ID"=>$GEO_CONTENT, "=PROPERTY_URL" => $found_page, "ACTIVE" => "Y", [
					['PROPERTY_IS_DEFAULT'=>1],
					['PROPERTY_GEO.CODE'=>$arGeoData['CUR_CITY']['CITY_CODE']],
					'LOGIC' => 'OR',
				]);
				$arSelect = ['ID', 'NAME', 'DETAIL_TEXT', 'PROPERTY_URL', 'PROPERTY_seo_title', 'PROPERTY_seo_keywords', 'PROPERTY_seo_description', 'PROPERTY_GEO', 'PROPERTY_GEO.CODE', 'PROPERTY_IS_DEFAULT'];
				$arLimit = false;
				$res = CIBlockElement::GetList($arSort, $arFilter, false, $arLimit, $arSelect);
				while ($arFields = $res->GetNext()) {
					$arFoundPageRoot[] = $arFields;
				}

				if (count($arFoundPageRoot) > 1) {
					foreach($arFoundPageRoot as $ID => $page) {
						if ($page['PROPERTY_GEO_CODE'] == $arGeoData['CUR_CITY']['CITY_CODE']) {
							$arFoundPageRoot = $arFoundPageRoot[$ID];
							break;
						}
					}
					if (!isset($arFoundPageRoot['ID'])) {
						$arFoundPageRoot = $arFoundPageRoot[0];
					}
				} else {
					$arFoundPageRoot = $arFoundPageRoot[0];
				}

				if (!empty($arFoundPageRoot) && $arFoundPageRoot['ID']) {
					$APPLICATION->AddChainItem($arFoundPageRoot['~NAME'], $arFoundPageRoot['~PROPERTY_URL_VALUE']);
				}
			}

		}
	}

	$APPLICATION->AddChainItem($arFoundPage['~NAME'], $arFoundPage['~PROPERTY_URL_VALUE']);

	include($_SERVER['DOCUMENT_ROOT'].'/bitrix/header.php');
	echo $arFoundPage['DETAIL_TEXT'];
	include($_SERVER['DOCUMENT_ROOT'].'/bitrix/footer.php');
	die();
}



CHTTP::SetStatus("404 Not Found");
@define("ERROR_404","Y");

require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");

$APPLICATION->SetTitle("Ошибка 404. Увы, такой страницы не существует!");
$APPLICATION->SetPageProperty("MAIN_CLASS", "inner line");
$APPLICATION->SetPageProperty("HIDE_BREADCRUMS", "Y");
$APPLICATION->SetPageProperty("CONTENT_PAGE", "N");

?>
<!-- roll -->
<div class="roll txt-block">
	<?$APPLICATION->IncludeFile("include/404_text.php", Array(), Array("MODE"=>"html"));?>

	<?$APPLICATION->IncludeFile("include/404_search_form.php", Array(), Array("MODE"=>"html"));?>

	<?$APPLICATION->IncludeFile("include/404_text_bottom.php", Array(), Array("MODE"=>"html"));?>

</div>
<!-- / roll end -->

<?/*$APPLICATION->IncludeFile("include/pop-cat.php", Array(), Array("MODE"=>"php"));*/?>

<?$APPLICATION->IncludeFile("include/popular_items_block.php", Array(), Array("MODE"=>"php"));?>

<?$APPLICATION->IncludeFile("include/need_help.php", Array(), Array("MODE"=>"php"));?>


<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>
