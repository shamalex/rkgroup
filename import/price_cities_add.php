<?
include $_SERVER['DOCUMENT_ROOT'].'/bitrix/modules/main/include/prolog_before.php';
// require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Импорт цен и остатков");

CModule::IncludeModule('iblock');
Cmodule::IncludeModule('catalog');
@ini_set('memory_limit', '4096M');
@set_time_limit(595);

$CATALOG_IBLOCK_ID = 8;
$BRANDS_IBLOCK_ID = 9;
$GEO_IBLOCK_ID = 15;
$EXT_WAY = 'http://rusklimat.webway.ru';

function get_web_page( $url ) {
	$res = array();
	$options = array( 
		CURLOPT_RETURNTRANSFER => true,	 // return web page 
		CURLOPT_HEADER		 => false,	// do not return headers 
		CURLOPT_FOLLOWLOCATION => true,	 // follow redirects 
		// CURLOPT_USERAGENT	  => "spider", // who am i 
		CURLOPT_USERAGENT	  => "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/45.0.2454.85 Safari/537.36", // who am i 
		CURLOPT_AUTOREFERER	=> true,	 // set referer on redirect 
		CURLOPT_CONNECTTIMEOUT => 120,	  // timeout on connect 
		CURLOPT_TIMEOUT		=> 120,	  // timeout on response 
		CURLOPT_MAXREDIRS	  => 10,	   // stop after 10 redirects 
	); 
	$ch	  = curl_init( $url ); 
	curl_setopt_array( $ch, $options ); 
	$content = curl_exec( $ch ); 
	$err	 = curl_errno( $ch ); 
	$errmsg  = curl_error( $ch ); 
	$header  = curl_getinfo( $ch ); 
	curl_close( $ch ); 

	$res['content'] = $content;	 
	$res['url'] = $header['url'];
	return $res; 
}

$catalogPrices = [];

$dbPriceType = CCatalogGroup::GetList(
	array("SORT" => "ASC")
);
while ($arPriceType = $dbPriceType->Fetch()) {
	$catalogPrices[$arPriceType['NAME']] = $arPriceType['ID'];
}

// p($catalogPrices);

$prices_get_url = $EXT_WAY.'/goods/prices';
$quanity_get_url = $EXT_WAY.'/goods/quanity';


$prices = [];
$quanity = [];

if (!file_exists('./data_prices.php')) {
	$prices = get_web_page($prices_get_url);
	$prices = json_decode($prices['content'], true);
	// file_put_contents('./data_prices.php', '<? $prices = '.var_export($prices, 1).';');
} else {
	include('./data_prices.php');
}
// p($prices);

if (!file_exists('./data_quanity.php')) {
	$quanity = get_web_page($quanity_get_url);
	$quanity = json_decode($quanity['content'], true);
	// file_put_contents('./data_quanity.php', '<? $quanity = '.var_export($quanity, 1).';');
} else {
	include('./data_quanity.php');
}
// p($quanity);

if (!empty($prices) && !empty($quanity) && !empty($catalogPrices)) {
	
	$catalog_items = [];
	
	// $arCatalogGroups = [];
	// foreach($catalogPrices as $PRICE_TYPE_ID) {
		// $arCatalogGroups[] = "CATALOG_GROUP_".$PRICE_TYPE_ID;
	// }
	
	$geo_items = [];
	$arQuanityParams = [];
	// foreach($CITY_NAMES2 as $name => $code) {
		// $code = preg_replace('/\-+/ui', '_', $code);
		// $arQuanityParams[] = strtoupper("PROPERTY_count_".$code);
	// }
	
	$arFilter = Array("IBLOCK_ID"=>$GEO_IBLOCK_ID);
	$arSelect = array('ID', 'PROPERTY_filial1_code', 'PROPERTY_filial2_code');
	$res = CIBlockElement::GetList(Array(), $arFilter, false, false, $arSelect);  
	while($arFields = $res->Fetch()) {
		$code = $arFields['PROPERTY_FILIAL1_CODE_VALUE'];
		$code = preg_replace('/\-+/ui', '_', $code);
		$arQuanityParams[strtoupper("PROPERTY_count_".$code)]++;
		
		$code = $arFields['PROPERTY_FILIAL2_CODE_VALUE'];
		$code = preg_replace('/\-+/ui', '_', $code);
		$arQuanityParams[strtoupper("PROPERTY_count_".$code)]++;
		
		$geo_items[$arFields['PROPERTY_FILIAL1_CODE_VALUE']]++;
		$geo_items[$arFields['PROPERTY_FILIAL2_CODE_VALUE']]++;
	}
	$arQuanityParams = array_keys($arQuanityParams);

	$arFilter = Array("IBLOCK_ID"=>$CATALOG_IBLOCK_ID);
	$arSelect = Array("ID", "PROPERTY_ID_1C", "PROPERTY_CODE");
	// $arSelect = array_merge($arSelect, $arCatalogGroups);
	$arSelect = array_merge($arSelect, $arQuanityParams);
	$res = CIBlockElement::GetList(Array(), $arFilter, false, false, $arSelect);  
	while($arFields = $res->Fetch()) {
		
		$ID_1C = strtolower($arFields['PROPERTY_ID_1C_VALUE']);
		$ID_CODE = $arFields['PROPERTY_CODE_VALUE'];
		$arFields['PRICE'] = isset($prices[$ID_CODE]) ? $prices[$ID_CODE] : false;
		$arFields['QTY'] = isset($quanity[$ID_1C]) ? $quanity[$ID_1C] : false;
		
		$catalog_items[] = $arFields;
	}

	// p(count($catalog_items));
	// p($catalog_items[0]);
	// for ($i=0;$i<100;$i++) {
		// p($catalog_items[$i]);
	// }
	// die();
	
	if (true) {
		
		foreach($catalog_items as $arItem) {
			
			$wasUpdated = false;
			
			// p($arItem);
			
			$PRODUCT_ID = $arItem['ID'];
			
			$arFields = array(
				"ID" => $PRODUCT_ID, 
				"QUANTITY" => 1000,
			);
			if (!empty($arItem['PRICE'])) {
				if(CCatalogProduct::Add($arFields)) {
					// p("Добавили параметры товара к элементу каталога ".$PRODUCT_ID.'');
					
					$arUpdateQuanity = [];
					// foreach($CITY_NAMES2 as $name => $code) {
						// $code = preg_replace('/\-+/ui', '_', $code);
						// $prop = strtoupper("count_".$code);
						// if ($arItem['PROPERTY_'.$prop.'_VALUE'] != intval($arItem['QTY'][$code])) {
							// if (!empty($arItem['QTY'][$code])) {
								// $arUpdateQuanity[$prop] = 1;
							// } else {
								// $arUpdateQuanity[$prop] = false;
							// }
						// }
					// }
					foreach($geo_items as $code=>$e) {
						$code_prop = preg_replace('/\-+/ui', '_', $code);
						$prop = strtoupper("count_".$code_prop);
						if ($arItem['PROPERTY_'.$prop.'_VALUE'] != intval($arItem['QTY'][$code])) {
							if (!empty($arItem['QTY'][$code])) {
								$arUpdateQuanity[$prop] = 1;
							} else {
								$arUpdateQuanity[$prop] = false;
							}
						}
					}
					if (!empty($arUpdateQuanity)) {
						CIBlockElement::SetPropertyValuesEx($PRODUCT_ID, $CATALOG_IBLOCK_ID, $arUpdateQuanity);
						$wasUpdated = true;
						// p($PRODUCT_ID);
						// p($arUpdateQuanity);
					}
					
					$res = CPrice::GetList(
						array(
							"CATALOG_GROUP_ID" => "ASC",
						),
						array(
							"PRODUCT_ID" => $PRODUCT_ID,
						)
					);
					$arPrices = [];
					while($arPrice = $res->Fetch()) {
						$arPrices[$arPrice['CATALOG_GROUP_ID']] = [
							'ID' => $arPrice['ID'],
							'PRICE' => $arPrice['PRICE'],
						];
					}
					
					foreach($arItem['PRICE'] as $city => $prices) {
						
						$prices[0] = floatval($prices[0]);
						$prices[1] = floatval($prices[1]);
						
						if (!isset($catalogPrices[$city])) {
							continue;
						}
						
						$price_code = $catalogPrices[$city];
						$price_code_NODISCOUNT = $catalogPrices[$city.'_NODISCOUNT'];
						
						$arPriceFields = Array(
							"PRODUCT_ID" => $PRODUCT_ID,
							"CATALOG_GROUP_ID" => $price_code,
							"PRICE" => $prices[0],
							"CURRENCY" => "RUB",
							// "QUANTITY_FROM" => false,
							// "QUANTITY_TO" => false,
						);
						
						$arPriceFields2 = Array(
							"PRODUCT_ID" => $PRODUCT_ID,
							"CATALOG_GROUP_ID" => $price_code_NODISCOUNT,
							"PRICE" => $prices[1],
							"CURRENCY" => "RUB",
							// "QUANTITY_FROM" => false,
							// "QUANTITY_TO" => false,
						);
							
						$update_arr = [
							[$price_code, $arPriceFields],
							[$price_code_NODISCOUNT, $arPriceFields2],
						];
						
						// p($update_arr);
						
						foreach($update_arr as $update_data) {
							if (isset($arPrices[$update_data[0]])) {
								if (floatval($arPrices[$update_data[0]]['PRICE']) != $update_data[1]['PRICE']) {
									if ($update_data[1]['PRICE'] > 0) {
										if ($ID = CPrice::Update($arPrices[$update_data[0]]['ID'], $update_data[1])) {
											p('Updated ID: '.$ID);
											$wasUpdated = true;
										} else {
											p('Ошибка обновления: '.$APPLICATION->GetException());
										}
									} else {
										if ($ID = CPrice::Delete($arPrices[$update_data[0]]['ID'])) {
											p('Deleted ID: '.$arPrices[$update_data[0]]["ID"]);
											$wasUpdated = true;
										} else {
											p('Ошибка удаления: '.$APPLICATION->GetException());
										}
									}
								}
								unset($arPrices[$update_data[0]]);
							} else {
								if ($update_data[1]['PRICE'] > 0) {
									if ($ID = CPrice::Add($update_data[1])) {
										$wasUpdated = true;
										p('New ID: '.$ID);
									} else {
										// p('Ошибка добавления: '.$APPLICATION->GetException());
										p($APPLICATION->GetException());
									}
								}
							}
						}
						
					}
					
					// p($arItem);
					// p($arItem['PRICE']);
					// p($arPrices);
					
					foreach($arPrices as $PRICE_ID => $PRICE_DATA) {
						if ($ID = CPrice::Delete($PRICE_DATA['ID'])) {
							$wasUpdated = true;
							p('Deleted ID: '.$PRICE_DATA["ID"]);
						} else {
							p('Ошибка удаления: '.$APPLICATION->GetException());
						}
					}
					
					// break;
					
					
				} else {
					p('Ошибка добавления параметров к элементу каталога '.$PRODUCT_ID.'');
				}
			} else {
				if(CCatalogProduct::Delete($PRODUCT_ID)) {
					$wasUpdated = true;
					// p("Удалили параметры товара к элементу каталога ".$PRODUCT_ID.'');
				} else {
					p('Ошибка удаления параметров к элементу каталога '.$PRODUCT_ID.'');
				}
			}
			
			if ($wasUpdated) {
				\Bitrix\Iblock\PropertyIndex\Manager::updateElementIndex($CATALOG_IBLOCK_ID, $PRODUCT_ID);
			}
			
		}
	}

	BXClearCache(true, "/s1/bitrix/catalog.element/");
	BXClearCache(true, "/s1/bitrix/catalog.section/");
	BXClearCache(true, "/s1/bitrix/catalog.section.list/");
	BXClearCache(true, "/s1/bitrix/catalog.smart.filter/");
	
}

?>


<?
//require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");
?>