<?
include $_SERVER['DOCUMENT_ROOT'].'/bitrix/modules/main/include/prolog_before.php';
// require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Импорт цен и остатков");

CModule::IncludeModule('iblock');
Cmodule::IncludeModule('catalog');
@ini_set('memory_limit', '4096M');
@set_time_limit(595);

$CATALOG_IBLOCK_ID = 8;
$BRANDS_IBLOCK_ID = 9;
$GEO_IBLOCK_ID = 15;
$EXT_WAY = 'http://rusklimat.webway.ru';

function get_web_page( $url ) {
	$res = array();
	$options = array( 
		CURLOPT_RETURNTRANSFER => true,	 // return web page 
		CURLOPT_HEADER		 => false,	// do not return headers 
		CURLOPT_FOLLOWLOCATION => true,	 // follow redirects 
		// CURLOPT_USERAGENT	  => "spider", // who am i 
		CURLOPT_USERAGENT	  => "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/45.0.2454.85 Safari/537.36", // who am i 
		CURLOPT_AUTOREFERER	=> true,	 // set referer on redirect 
		CURLOPT_CONNECTTIMEOUT => 120,	  // timeout on connect 
		CURLOPT_TIMEOUT		=> 120,	  // timeout on response 
		CURLOPT_MAXREDIRS	  => 10,	   // stop after 10 redirects 
	); 
	$ch	  = curl_init( $url ); 
	curl_setopt_array( $ch, $options ); 
	$content = curl_exec( $ch ); 
	$err	 = curl_errno( $ch ); 
	$errmsg  = curl_error( $ch ); 
	$header  = curl_getinfo( $ch ); 
	curl_close( $ch ); 

	$res['content'] = $content;	 
	$res['url'] = $header['url'];
	return $res; 
}


$geo_get_url = $EXT_WAY.'/goods/geo';


$geo = [];

if (!file_exists('./data_geo.php')) {
	$geo = get_web_page($geo_get_url);
	$geo = json_decode($geo['content'], true);
	// file_put_contents('./data_geo.php', '<? $geo = '.var_export($geo, 1).';');
} else {
	//include('./data_geo.php');
}
//p($geo);

if (!empty($geo)) {
	
	$geo_items = [];

	$arFilter = Array("IBLOCK_ID"=>$GEO_IBLOCK_ID);
	$arSelect = false;
	$res = CIBlockElement::GetList(Array(), $arFilter, false, false, $arSelect);  
	while($obFields = $res->GetNextElement()) {
		
		$arFields = $obFields->GetFields();
		$arFields['PROPERTY_VALUES'] = $obFields->GetProperties();
		
		$geo_items[$arFields["XML_ID"]] = $arFields;
	}
	
	foreach($geo as $id => $geo_item) {
		
		$arLoadProductArray = Array(
			"IBLOCK_SECTION_ID" => false,
			"IBLOCK_ID"      => $GEO_IBLOCK_ID,
			"NAME"           => $geo_item['city_name'],
			"CODE"           => '',
			"XML_ID"         => $geo_item['city_id'],
			"ACTIVE"         => "Y",
			"SORT"    => '500',
			"PROPERTY_VALUES"    => [
				"region" => $geo_item['region'],
				"network_name" => $geo_item['network_name'],
				"filial1" => $geo_item['filial1'],
				"filial2" => $geo_item['filial2'],
				"predoplata_required" => $geo_item['predoplata_required'],
				"shipping_1" => $geo_item['shipping_1'],
				"shipping_2" => $geo_item['shipping_2'],
				"shipping_3" => $geo_item['shipping_3'],
				"tarif_1" => $geo_item['tarif_1'],
				"tarif_2" => $geo_item['tarif_2'],
				"tarif_3" => $geo_item['tarif_3'],
				"tarif_4" => $geo_item['tarif_4'],
				"tarif_5" => $geo_item['tarif_5'],
				"tarif_6" => $geo_item['tarif_6'],
				"tarif_7" => $geo_item['tarif_7'],
				"tarif_8" => $geo_item['tarif_8'],
				"samov_1" => $geo_item['samov_1'],
				"samov_2" => $geo_item['samov_2'],
				"samov_3" => $geo_item['samov_3'],
				"network_data_site_id" => $geo_item['network_data']['site_id'],
				"network_data_status" => $geo_item['network_data']['status'],
				"network_data_code" => $geo_item['network_data']['code'],
				"filial1_code" => $geo_item['filial1_code'],
				"filial2_code" => $geo_item['filial2_code'],
			],
		);
		
		if (isset($geo_items[$arLoadProductArray['XML_ID']])) {
			
			$geo_item = $geo_items[$arLoadProductArray['XML_ID']];
			
			foreach($arLoadProductArray as $key => $val) {
				
				if ($key == 'PROPERTY_VALUES') {
					foreach($val as $prop_key => $prop_val) {
						if ($prop_val == $geo_item['PROPERTY_VALUES'][$prop_key]['~VALUE']) {
							unset($arLoadProductArray['PROPERTY_VALUES'][$prop_key]);
						}
					}
					if (empty($arLoadProductArray['PROPERTY_VALUES'])) {
						unset($arLoadProductArray['PROPERTY_VALUES']);
					}
				} else {
					if ($val == $geo_item['~'.$key]) {
						unset($arLoadProductArray[$key]);
					}
				}
				
			}
			
			if (!empty($arLoadProductArray)) {
				if (!empty($arLoadProductArray['PROPERTY_VALUES'])) {
					p('update props');
					p($arLoadProductArray['PROPERTY_VALUES']);
					CIBlockElement::SetPropertyValuesEx($geo_item['ID'], $GEO_IBLOCK_ID, $arLoadProductArray['PROPERTY_VALUES']);
					unset($arLoadProductArray['PROPERTY_VALUES']);
				}
				if (!empty($arLoadProductArray)) {
					p('update data');
					p($arLoadProductArray);
					$el = new CIBlockElement;
					$res = $el->Update($geo_item['ID'], $arLoadProductArray);
				}
			}
			
			unset($geo_items[$geo_item['XML_ID']]);
			
		} else {
			
			$el = new CIBlockElement;
			if($PRODUCT_ID = $el->Add($arLoadProductArray, false, false, false)) {
				p("New ID: ".$PRODUCT_ID);
				// $geo_items[$arLoadProductArray['XML_ID']] = $PRODUCT_ID;
			} else {
				echo '<div class="make_error">';
				p("Error add: ".$el->LAST_ERROR);
				echo '</div>';
			}
			// p($arLoadProductArray);
			// p('-----------------------------------------------------------------------------------');
			
		}
		
	}
	
	if (!empty($geo_items)) {
		foreach($geo_items as $geo_item) {
			p('delete');
			p($geo_item);
			CIBlockElement::Delete($geo_item['ID']);
		}
	}

	// p(count($geo_items));
	// p($geo_items);
	// p($geo_items[0]);
	// for ($i=0;$i<100;$i++) {
		// p($geo_items[$i]);
	// }
	// die();
	
}

$arPrices = [];
$dbPriceType = CCatalogGroup::GetList(
	array("SORT" => "ASC"),
	array()
);
while ($arPriceType = $dbPriceType->Fetch()) {
	$arPrices[$arPriceType['NAME']] = $arPriceType;
}

// p($arPrices);

$arCountProps = [];
$properties = CIBlockProperty::GetList(Array("sort"=>"asc", "name"=>"asc"), Array("ACTIVE"=>"Y", "IBLOCK_ID"=>$CATALOG_IBLOCK_ID, "CODE"=>"COUNT_%"));
while ($prop_fields = $properties->Fetch()) {
	// p($prop_fields);
	if ($prop_fields['SORT'] == 50) {
		$arCountProps[$prop_fields['CODE']] = $prop_fields;
	}
}

// p($arCountProps);


$SORT = 100;
$SORT1 = 50;
$geo_items = [];

$arFilter = Array("IBLOCK_ID"=>$GEO_IBLOCK_ID);
$arSelect = ['ID', 'NAME', 'PROPERTY_network_name', 'PROPERTY_network_data_code', 'PROPERTY_filial1', 'PROPERTY_filial2', 'PROPERTY_filial1_code', 'PROPERTY_filial2_code'];
$res = CIBlockElement::GetList(Array(), $arFilter, false, false, $arSelect);  
while($arFields = $res->Fetch()) {
	$code = $arFields["PROPERTY_NETWORK_DATA_CODE_VALUE"];
	$name = $arFields['PROPERTY_NETWORK_NAME_VALUE'];
	$name = str_replace('Русклимат ', '', $name);
	
	// p($name);
	// p($code);
	
	if (!empty($name) && !empty($code)) {
	
		if (empty($arPrices[$code])) {
			$arFieldsAdd = array(
			   "NAME" => $code,
			   "XML_ID" => $code,
			   "SORT" => $SORT,
			   "USER_GROUP" => array(1, 2),   // видят цены члены групп 2 и 4
			   "USER_GROUP_BUY" => array(1, 2),  // покупают по этой цене
											  // только члены группы 2
			   "USER_LANG" => array(
				  "ru" => $name,
				  )
			);
			
			p($arFieldsAdd);

			$ID = CCatalogGroup::Add($arFieldsAdd);
			if ($ID<=0) {
			  p("Ошибка добавления типа цены");
			   
			} else {
				p('price add ok');
			}
		}
		
		if (empty($arPrices[$code.'_NODISCOUNT'])) {
			$arFieldsAdd = array(
			   "NAME" => $code.'_NODISCOUNT',
			   "XML_ID" => $code.'_NODISCOUNT',
			   "SORT" => $SORT,
			   "USER_GROUP" => array(1, 2),   // видят цены члены групп 2 и 4
			   "USER_GROUP_BUY" => array(1, 2),  // покупают по этой цене
											  // только члены группы 2
			   "USER_LANG" => array(
				  "ru" => $name.' без скидки',
				  )
			);
			
			p($arFieldsAdd);

			$ID = CCatalogGroup::Add($arFieldsAdd);
			if ($ID<=0) {
			  p("Ошибка добавления типа цены");
			   
			} else {
				p('price add ok');
			}
		}
		
	}
	
	$codes1 = ['filial1', 'filial2'];
	
	foreach($codes1 as $code1_original) {
		$code1_name = $arFields["PROPERTY_".strtoupper($code1_original)."_VALUE"];
		$code1 = $arFields["PROPERTY_".strtoupper($code1_original."_code")."_VALUE"];
		$code1 = 'COUNT_'.$code1;
		$code1 = preg_replace('/\-+/ui', '_', $code1);
		$code1 = strtoupper($code1);
		
		if (!empty($code1_name) && !empty($code1)) {
		
			if (empty($arCountProps[$code1])) {
				$arNewPropFields = Array(
					"NAME" => 'Наличие '.$code1_name,
					"ACTIVE" => 'Y',
					"SORT" => $SORT1,
					"CODE" => $code1,
					"XML_ID" => $code1,
					"COL_COUNT" => "1",
					"FILTRABLE" => "N",
					"SECTION_PROPERTY" => "N",
					// "DEFAULT_VALUE" => "",
					"PROPERTY_TYPE" => "N",
					"USER_TYPE" => "SASDCheckboxNum",
					"IBLOCK_ID" => $CATALOG_IBLOCK_ID,
				);
				
				p($arNewPropFields);
				
				$el = new CIBlockProperty;
				
				if($PRODUCT_ID = $el->Add($arNewPropFields)) {
					p("New ID: ".$PRODUCT_ID);
					$arCountProps[$code1] = $arNewPropFields;
					// $SORT += 100;
				} else {
					p("Error add: ".$el->LAST_ERROR);
				}
				
				// p('-----------------------------------------------------------------------------------');
			}
			
		}
	}
	
	$SORT += 100;
}

// p($geo_items);


?>


<?
//require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");
?>