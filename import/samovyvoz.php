<?
include $_SERVER['DOCUMENT_ROOT'].'/bitrix/modules/main/include/prolog_before.php';
// require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Импорт цен и остатков");

CModule::IncludeModule('iblock');
Cmodule::IncludeModule('catalog');
@ini_set('memory_limit', '4096M');
@set_time_limit(595);

$CATALOG_IBLOCK_ID = 8;
$BRANDS_IBLOCK_ID = 9;
$GEO_IBLOCK_ID = 15;
$SAMOVYVOZ_IBLOCK_ID = 16;
$EXT_WAY = 'http://rusklimat.webway.ru';

function get_web_page( $url ) {
	$res = array();
	$options = array( 
		CURLOPT_RETURNTRANSFER => true,	 // return web page 
		CURLOPT_HEADER		 => false,	// do not return headers 
		CURLOPT_FOLLOWLOCATION => true,	 // follow redirects 
		// CURLOPT_USERAGENT	  => "spider", // who am i 
		CURLOPT_USERAGENT	  => "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/45.0.2454.85 Safari/537.36", // who am i 
		CURLOPT_AUTOREFERER	=> true,	 // set referer on redirect 
		CURLOPT_CONNECTTIMEOUT => 120,	  // timeout on connect 
		CURLOPT_TIMEOUT		=> 120,	  // timeout on response 
		CURLOPT_MAXREDIRS	  => 10,	   // stop after 10 redirects 
	); 
	$ch	  = curl_init( $url ); 
	curl_setopt_array( $ch, $options ); 
	$content = curl_exec( $ch ); 
	$err	 = curl_errno( $ch ); 
	$errmsg  = curl_error( $ch ); 
	$header  = curl_getinfo( $ch ); 
	curl_close( $ch ); 

	$res['content'] = $content;	 
	$res['url'] = $header['url'];
	return $res; 
}


$samovyvoz_get_url = $EXT_WAY.'/goods/samovyvoz';


$samovyvoz = [];

if (!file_exists('./data_samovyvoz.php')) {
	$samovyvoz = get_web_page($samovyvoz_get_url);
	$samovyvoz = json_decode($samovyvoz['content'], true);
	file_put_contents('./data_samovyvoz.php', '<? $samovyvoz = '.var_export($samovyvoz, 1).';');
} else {
	include('./data_samovyvoz.php');
}
// p($samovyvoz);

if (!empty($samovyvoz)) {
	
	$samovyvoz_items = [];

	$arFilter = Array("IBLOCK_ID"=>$SAMOVYVOZ_IBLOCK_ID);
	$arSelect = false;
	$res = CIBlockElement::GetList(Array(), $arFilter, false, false, $arSelect);  
	while($obFields = $res->GetNextElement()) {
		
		$arFields = $obFields->GetFields();
		$arFields['PROPERTY_VALUES'] = $obFields->GetProperties();
		
		$samovyvoz_items[$arFields["XML_ID"]] = $arFields;
	}
	
	foreach($samovyvoz as $city => $city_items) {
		foreach($city_items as $id => $samovyvoz_item) {
			
			$arLoadProductArray = Array(
				"IBLOCK_SECTION_ID" => false,
				"IBLOCK_ID"      => $SAMOVYVOZ_IBLOCK_ID,
				"NAME"           => $samovyvoz_item['name'],
				"CODE"           => $samovyvoz_item['city'],
				"XML_ID"         => $samovyvoz_item['city'].'_'.$samovyvoz_item['id'],
				"ACTIVE"         => "Y",
				"SORT"    => $samovyvoz_item['visible_id'],
				"PROPERTY_VALUES"    => [
					"address" => $samovyvoz_item['address'],
				],
			);
			
			if (isset($samovyvoz_items[$arLoadProductArray['XML_ID']])) {
				
				$samovyvoz_item = $samovyvoz_items[$arLoadProductArray['XML_ID']];
				
				foreach($arLoadProductArray as $key => $val) {
					
					if ($key == 'PROPERTY_VALUES') {
						foreach($val as $prop_key => $prop_val) {
							if ($prop_val == $samovyvoz_item['PROPERTY_VALUES'][$prop_key]['~VALUE']) {
								unset($arLoadProductArray['PROPERTY_VALUES'][$prop_key]);
							}
						}
						if (empty($arLoadProductArray['PROPERTY_VALUES'])) {
							unset($arLoadProductArray['PROPERTY_VALUES']);
						}
					} else {
						if ($val == $samovyvoz_item['~'.$key]) {
							unset($arLoadProductArray[$key]);
						}
					}
					
				}
				
				if (!empty($arLoadProductArray)) {
					if (!empty($arLoadProductArray['PROPERTY_VALUES'])) {
						p('update props');
						p($arLoadProductArray['PROPERTY_VALUES']);
						CIBlockElement::SetPropertyValuesEx($samovyvoz_item['ID'], $SAMOVYVOZ_IBLOCK_ID, $arLoadProductArray['PROPERTY_VALUES']);
						unset($arLoadProductArray['PROPERTY_VALUES']);
					}
					if (!empty($arLoadProductArray)) {
						p('update data');
						$el = new CIBlockElement;
						$res = $el->Update($samovyvoz_item['ID'], $arLoadProductArray);
					}
				}
				
				unset($samovyvoz_items[$samovyvoz_item['XML_ID']]);
				
			} else {
				
				$el = new CIBlockElement;
				if($PRODUCT_ID = $el->Add($arLoadProductArray, false, false, false)) {
					p("New ID: ".$PRODUCT_ID);
					// $samovyvoz_items[$arLoadProductArray['XML_ID']] = $PRODUCT_ID;
				} else {
					echo '<div class="make_error">';
					p("Error add: ".$el->LAST_ERROR);
					echo '</div>';
				}
				// p($arLoadProductArray);
				// p('-----------------------------------------------------------------------------------');
				
			}
			
		}
	}
	
	if (!empty($samovyvoz_items)) {
		foreach($samovyvoz_items as $samovyvoz_item) {
			p('delete');
			p($samovyvoz_item);
			CIBlockElement::Delete($samovyvoz_item['ID']);
		}
	}

	// p(count($geo_items));
	// p($geo_items);
	// p($geo_items[0]);
	// for ($i=0;$i<100;$i++) {
		// p($geo_items[$i]);
	// }
	// die();
	
}

?>


<?
//require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");
?>