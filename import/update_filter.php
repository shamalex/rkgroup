<?
include $_SERVER['DOCUMENT_ROOT'].'/bitrix/modules/main/include/prolog_before.php';
// require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Обновление фильтра");

CModule::IncludeModule('iblock');
Cmodule::IncludeModule('catalog');
@ini_set('memory_limit', '4096M');
@set_time_limit(595);

$CATALOG_IBLOCK_ID = 8;
$PRICE_TYPE_ID = 1;

// $index = \Bitrix\Iblock\PropertyIndex\Manager::createIndexer($CATALOG_IBLOCK_ID);
// $index->startIndex();
// $index->continueIndex(0); // создание без ограничения по времени
// $index->endIndex();

// p('done');
die();

$file = './goods_descr_html/params.csv';
$file2 = './goods_descr_html/params_vals.csv';

$code_to_id = [];

// $arFilter = Array("IBLOCK_ID"=>$CATALOG_IBLOCK_ID, "ACTIVE"=>"Y");
$arFilter = Array("IBLOCK_ID"=>$CATALOG_IBLOCK_ID);
$arSelect = Array("ID", "NAME", "CODE", "XML_ID");
$res = CIBlockElement::GetList(Array(), $arFilter, false, false, $arSelect);  
while($arFields = $res->Fetch()) {  
	$code_to_id[$arFields['XML_ID']] = $arFields['ID'];
}
// p(count($code_to_id));
// p($code_to_id);


// remove all filter props

// $updateFields = array();
// for($i=1;$i<=59;$i++) {
	// $updateFields['filt'.$i] = '';
// }
// p($updateFields);

// foreach($code_to_id as $XML_ID => $ELEMENT_ID) {
	// CIBlockElement::SetPropertyValuesEx($ELEMENT_ID, $CATALOG_IBLOCK_ID, $updateFields);
// }




$data = file_get_contents($file);
$lines = explode("\n", $data);
$lines_header = [];
foreach($lines as $i=>$line) {
	$arr = explode(';', $line);
	unset($arr[count($arr)-1]);
	if ($i == 0) {
		unset($lines[$i]);
		$lines_header = $arr;
		continue;
	}
	$arr = array_combine($lines_header, $arr);
	$lines[$i] = $arr;
}
// p($lines);

$tmp = [];
foreach($lines as $line) {
	if (substr($line['КодНаСайте'], 0, 4) == 'filt') {
		$tmp[$line['ИдентификаторВБазе']] = $line;
	}
}
// p($tmp);

$data = file_get_contents($file2);
$lines = explode("\n", $data);
$lines_header = [];
foreach($lines as $i=>$line) {
	$arr = explode(';', $line);
	// unset($arr[count($arr)-1]);
	if ($i == 0) {
		unset($lines[$i]);
		$lines_header = $arr;
		continue;
	}
	$arr = array_combine($lines_header, $arr);
	$lines[$i] = $arr;
}
// p($lines);

$site_codes = [];

$tmp2 = [];
foreach($lines as $line) {
	if (!isset($tmp[$line['Характеристика']])) {
		continue;
	}
	if ($line['Значение_тип'] != 'Строка') {
		p('not line param!');
		continue;
	}
	if (!isset($code_to_id[$line['ИдентификаторВладельца']])) {
		// p('not found id to code!');
		// p($line);
		continue;
	}
	$line['ИдентификаторВладельца'] = $code_to_id[$line['ИдентификаторВладельца']];
	if (isset($tmp2[$line['ИдентификаторВладельца']][$tmp[$line['Характеристика']]['КодНаСайте']]) && $tmp2[$line['ИдентификаторВладельца']][$tmp[$line['Характеристика']]['КодНаСайте']] != $line['Значение']) {
		// p('dublicate param! now / old');
		// p($line);
		// p($tmp2[$line['ИдентификаторВладельца']][$tmp[$line['Характеристика']]['КодНаСайте']]);
		continue;
	}
	// $tmp2[$line['ИдентификаторВладельца']][$tmp[$line['Характеристика']]['КодНаСайте']] = $line;
	$tmp2[$line['ИдентификаторВладельца']][$tmp[$line['Характеристика']]['КодНаСайте']] = $line['Значение'];
	$site_codes['PROPERTY_'.$tmp[$line['Характеристика']]['КодНаСайте']]++;
}

// $site_codes = array_keys($site_codes);
// p($site_codes);

// p(count($tmp2));
// p($tmp2);

/* 
$old_data = [];

$arFilter = Array("IBLOCK_ID"=>$CATALOG_IBLOCK_ID, 'ID'=>array_keys($tmp2));
// $arSelect = Array("ID", "NAME", "CODE", "XML_ID", "PROPERTY_*");
$arSelect = false;
// $arSelect = array_merge($arSelect, $site_codes);
$res = CIBlockElement::GetList(Array(), $arFilter, false, false, $arSelect);  
while($el = $res->GetNextElement()) {
	$arFields = $el->GetFields();
	// p($arFields);
	$arParams = $el->GetProperties();
	// p($arParams);
	foreach($arParams as $field_name=>$field) {
		if (substr($field_name, 0, 4) == 'filt' && $field['VALUE'] != '') {
			$arParams[$field_name] = $field['VALUE'];
		} else {
			unset($arParams[$field_name]);
		}
	}
	$old_data[$arFields['ID']] = $arParams;
	// break;
}
// p($old_data);

// echo '<pre>';
file_put_contents('./update_filter_old_data.php', '<? $OLD_DATA = '.var_export($old_data, 1).';');
// echo '</pre>';
 */
 
$updateArr = [];
for($i=1;$i<=59;$i++){
	$updateArr['filt'.$i] = '';
}
// p($updateArr);
/* 
foreach($tmp2 as $ELEMENT_ID => $updateData) {
	$updateData = array_merge($updateArr, $updateData);
	CIBlockElement::SetPropertyValuesEx($ELEMENT_ID, $CATALOG_IBLOCK_ID, $updateData);
	p($ELEMENT_ID);
	p($updateData);
	// \Bitrix\Iblock\PropertyIndex\Manager::updateElementIndex($CATALOG_IBLOCK_ID, $ELEMENT_ID);
	// break;
}
 */


/* 
$index = \Bitrix\Iblock\PropertyIndex\Manager::createIndexer($CATALOG_IBLOCK_ID);
$index->startIndex();
$index->continueIndex(0); // создание без ограничения по времени
$index->endIndex();
 */
?>


<?
// require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");
?>