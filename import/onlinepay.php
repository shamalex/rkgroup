<?
ini_set("display_errors", 1);
error_reporting (E_ALL);
include $_SERVER['DOCUMENT_ROOT'].'/bitrix/modules/main/include/prolog_before.php';
$APPLICATION->SetTitle("Импорт геозависимости платежных систем");

CModule::IncludeModule('iblock');
Cmodule::IncludeModule('sale');

$xml = simplexml_load_file ("./onlinepay.xml");
$elements= [];

$xml=json_decode(json_encode((array)$xml),TRUE);
//p($xml);

foreach($xml["geocard"] as $geocard){
	if($geocard["network"]){
		$geoID=0;
		$resGD=CIBlockElement::GetList(array(),array("IBLOCK_ID"=>32,"PROPERTY_network"=>$geocard["network"]),false,false,array("ID"));
		if($arGD=$resGD->GetNext())
			$geoID=$arGD["ID"];
			
		$fields=array();
		$fields["IBLOCK_ID"]=32;
		$fields["NAME"]=$geocard["bank"]." (".$geocard["network"].")";
		$fields["ACTIVE"]="Y";
		$fields["PROPERTY_VALUES"]["network"]=$geocard["network"];
		$fields["PROPERTY_VALUES"]["bank"]=$geocard["bank"];
		$fields["PROPERTY_VALUES"]["url"]=$geocard["parameters"]["api_url"];
		$fields["PROPERTY_VALUES"]["login"]=$geocard["parameters"]["api_login"];
		$fields["PROPERTY_VALUES"]["password"]=$geocard["parameters"]["api_pass"];
		$fields["PROPERTY_VALUES"]["stage2"]=($geocard["parameters"]["api_2stage"]=="true")?"Y":"N";

			
		$el = new CIBlockElement;
		if(!$geoID){
			if(!$el->Add($fields)) {
				$errors[]=$el->LAST_ERROR;
			}
		}else{
			if(!$el->Update($geoID,$fields)) {
				$errors[]=$el->LAST_ERROR;
			}
		}
	}

	if (!empty($errors))
	{
		echo "<b>Ошибки в элементе ".$geocard["bank"]." (".$geocard["network"].")"."</b><br />";
		echo implode("<br />",$errors);
	}	
}