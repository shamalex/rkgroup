<?
include $_SERVER['DOCUMENT_ROOT'].'/bitrix/modules/main/include/prolog_before.php';
// require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Импорт цен и остатков");

CModule::IncludeModule('iblock');
Cmodule::IncludeModule('catalog');
@ini_set('memory_limit', '4096M');
@set_time_limit(595);

$CATALOG_IBLOCK_ID = 8;
$BRANDS_IBLOCK_ID = 9;
$GEO_IBLOCK_ID = 15;
$EXT_WAY = 'http://rusklimat.webway.ru';

function get_web_page( $url ) {
	$res = array();
	$options = array( 
		CURLOPT_RETURNTRANSFER => true,	 // return web page 
		CURLOPT_HEADER		 => false,	// do not return headers 
		CURLOPT_FOLLOWLOCATION => true,	 // follow redirects 
		// CURLOPT_USERAGENT	  => "spider", // who am i 
		CURLOPT_USERAGENT	  => "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/45.0.2454.85 Safari/537.36", // who am i 
		CURLOPT_AUTOREFERER	=> true,	 // set referer on redirect 
		CURLOPT_CONNECTTIMEOUT => 120,	  // timeout on connect 
		CURLOPT_TIMEOUT		=> 120,	  // timeout on response 
		CURLOPT_MAXREDIRS	  => 10,	   // stop after 10 redirects 
	); 
	$ch	  = curl_init( $url ); 
	curl_setopt_array( $ch, $options ); 
	$content = curl_exec( $ch ); 
	$err	 = curl_errno( $ch ); 
	$errmsg  = curl_error( $ch ); 
	$header  = curl_getinfo( $ch ); 
	curl_close( $ch ); 

	$res['content'] = $content;	 
	$res['url'] = $header['url'];
	return $res; 
}

$big_size_get_url = $EXT_WAY.'/goods/big_size';


$big_size = [];

if (!file_exists('./data_prices.php')) {
	$big_size = get_web_page($big_size_get_url);
	$big_size = json_decode($big_size['content'], true);
	// file_put_contents('./data_big_size.php', '<? $big_size = '.var_export($big_size, 1).';');
} else {
	include('./data_big_size.php');
}

if (!empty($big_size)) {
	
	$catalog_items = [];

	$arFilter = Array("IBLOCK_ID"=>$CATALOG_IBLOCK_ID);
	$arSelect = Array("ID", "PROPERTY_IS_BIG_SIZE", "PROPERTY_CODE");
	$res = CIBlockElement::GetList(Array(), $arFilter, false, false, $arSelect);  
	while($arFields = $res->Fetch()) {
		
		$ID_CODE = $arFields['PROPERTY_CODE_VALUE'];
		$arFields['IS_BIG_SIZE'] = isset($big_size[$ID_CODE]) ? $big_size[$ID_CODE] : false;
		
		$catalog_items[$ID_CODE] = $arFields;
	}

	// p(count($catalog_items));
	// p($catalog_items[0]);
	// for ($i=0;$i<1000;$i++) {
		// p($catalog_items[$i]);
	// }
	// die();
	
	if (true) {
		
		foreach($catalog_items as $arItem) {
			
			// p($arItem);
			
			if (intval($arItem['PROPERTY_IS_BIG_SIZE_VALUE']) != intval($arItem['IS_BIG_SIZE'])) {
				CIBlockElement::SetPropertyValuesEx($arItem['ID'], $CATALOG_IBLOCK_ID, array('IS_BIG_SIZE' => intval($arItem['IS_BIG_SIZE'])));
				p($arItem['ID'].' updated');
			}
			
		}
	}
	
}

?>


<?
//require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");
?>