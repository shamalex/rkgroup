<?
header("Access-Control-Allow-Origin: *");
header("Access-Control-Allow-Methods: GET, POST");
header("Connection:keep-alive");
header("Content-Type: application/json; charset=utf-8");
include $_SERVER['DOCUMENT_ROOT'].'/bitrix/modules/main/include/prolog_before.php';
CModule::IncludeModule("sale");


$json = file_get_contents('php://input'); 
AddMessage2Log($json, "ch_order_status Input json");
$input=json_decode($json,true);
AddMessage2Log($input, "ch_order_status Input");

$status_list = [];
$obStatus=CSaleStatus::GetList();
while($arStatus=$obStatus->GetNext()){
	$status_list[$arStatus["NAME"]] = $arStatus["ID"];
}
AddMessage2Log($status_list, "ch_order_status Status list");

$result = [];

if(isset($status_list[$input["status"]])){
	if (!CSaleOrder::StatusOrder($input["order_id"], $status_list[$input["status"]])){
		$result["message"]="Ошибка установки нового статуса заказа";
		$result["status"]="ERROR";
	}else{
		$result["message"]="Статус установлен";
		$result["status"]="SUCCESS";
	}
}else{
	$result["message"]="Статус не существует";
	$result["status"]="ERROR";
}
AddMessage2Log($result, "ch_order_status Result");

echo json_encode($result);?>