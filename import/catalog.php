<?

//print_r($argv);

if(is_array($argv)){
	$path=$argv[0];
	$path=explode('/',$path);
	$f=false;
	foreach ($path as $k => $v) {
		if($f){
			unset($path[$k]);
		}
		if($v=='www'){
			$f=true;		
		}
	}
	
	$path=implode('/',$path);
	$_SERVER['DOCUMENT_ROOT']=$path;
	$_REQUEST['act']=$argv[1];
	$_REQUEST['step']=$argv[2];
	$_REQUEST['get']=$argv[3];
}
include ($_SERVER['DOCUMENT_ROOT'].'/bitrix/modules/main/include/prolog_before.php');
include($_SERVER["DOCUMENT_ROOT"].'/local/php_interface/classes/CParser.php');
@ini_set('memory_limit', '4096M');
@set_time_limit(0);
ini_set('max_execution_time', 0);


include ($_SERVER['DOCUMENT_ROOT'].'/import/includes/arTypes.php');
//p($_SERVER['DOCUMENT_ROOT']."/upload/");
p($arTypes);
p($_REQUEST);

$parser_id=$_REQUEST['act'];
$EXT_WAY = 'http://rusklimat.webway.ru';
$FILES_PATH = '/files_test/';
$dataItemHarTable='items_har';
$CATALOG_IBLOCK_ID=8;
$CATALOG_GROUP_PROP_ID=42;
$CATALOG_MARKI_ID=9;
$CATALOG_ACTIONS_ID=23;
$CATALOG_PROPS_ID=43;
$CATALOG_PRICEBUSH_ID=20;
$CATALOG_AD_ID=14;
$CATALOG_TIME2BUY_SECTION=6790;
$WEBSITE_ID='10ed05aa-e8ce-45c6-a116-7eab2cc38220';
switch ($_REQUEST['act']) {
	case 'system':
		 switch ($_REQUEST['step']) {
		 	case 'catalog':
		 		switch ($_REQUEST['get']) {
		 			case 'clear':
		 				$times=array(
							'start'=>date('Y-m-d H:i:s'),
							'stop'=>'');
		 				$counts=array(
		 					'Eelement_deleted'=>0,
		 					'Section_deleted'=>0,
		 					'Error'=>0
		 					);
		 				CModule::IncludeModule("iblock");

		 				$arFilter = Array("IBLOCK_ID"=>$CATALOG_IBLOCK_ID);
						$arSelect = Array("ID");
						$res = CIBlockSection::GetList(Array(), $arFilter, false, $arSelect);  
						while($arFields = $res->Fetch()) {  
								$arFilter1 = Array("IBLOCK_ID"=>$CATALOG_IBLOCK_ID, "SECTION_ID"=>$arFields['ID']);
								
								$res1 = CIBlockElement::GetList(Array(), $arFilter1, false, false, $arSelect);  
								while($arItem = $res1->Fetch()) {
									
									if(CIBlockElement::Delete($arItem['ID'])){
										$counts['Eelement_deleted']++;
									}else{
										$counts['Error']++;
									}
								}
								if(CIBlockSection::Delete($arFields['ID'])){
									$counts['Section_deleted']++;
								}else{
									$counts['Error']++;
								}	
							
						}
						p($counts);
						CParser::AddLog('system','CatalogClear',array('MESSAGE'=>'Полная очистка каталога - быстрый метод','DATA'=>json_encode(array('counts'=>$counts,'errors'=>$errors))));
		 				break;
		 			case 'propgroupset':
 						$n=0;
 						CModule::IncludeModule("iblock");
 						$arFilter1 = Array("IBLOCK_ID"=>$CATALOG_IBLOCK_ID);
						$arSelect=array('ID','IBLOCK_SECTION_ID');
						$res1 = CIBlockElement::GetList(Array(), $arFilter1, false, false, $arSelect);  
						while($arItem = $res1->Fetch()) {
							CIBlockElement::SetPropertyValuesEx($arItem['ID'],$CATALOG_IBLOCK_ID,array('GROUP'=>$arItem['IBLOCK_SECTION_ID']));
							\Bitrix\Iblock\PropertyIndex\Manager::updateElementIndex($CATALOG_IBLOCK_ID, $arItem['ID']);
							$n++;
						}
						p($n);
		 						break;		
		 			default:
		 				# code...
		 				break;
		 		}
		 		break;
		 	
		 	default:
		 		# code...
		 		break;
		 }
		break;
	case 'pump':
		switch ($_REQUEST['step']) {
			case 'getdata':
				$data=CParser::GetJSON($arTypes[$_REQUEST['act']]);
				p($data);
				/*if(count($data)>0){
					CParser::AddDataTable($parser_id,$data[0]);
					CParser::InsertsToDataTable($parser_id,$data);
				}else{
					CParser::AddLog($parser_id,'HarakteristikiGetData',array('MESSAGE'=>'Ошибка получения данных от шлюза','DATA'=>''));
				}*/
				break;
		}
		break;
	case 'actions':
		switch ($_REQUEST['step']) {
			case 'getdata':
				$data=CParser::GetJSON($arTypes[$_REQUEST['act']]);
				//p($data);
				if(count($data)>0){
					CParser::AddDataTable($parser_id,$data[0]);
					CParser::InsertsToDataTable($parser_id,$data);
				}else{
					CParser::AddLog($parser_id,'ActionsGetData',array('MESSAGE'=>'Ошибка получения данных от шлюза','DATA'=>''));
				}
				break;
			case 'printdata':
				$pars=$parser_id;
				$data=CParser::GetDataFromTable($pars,array(),array());
				$arParams=array();
				while ($arParam=$data->GetNext()) {
					foreach ($arParam as $k => $v) {
						if(isJSON($arParam['~'.$k])){
							$arParam[$k]=json_decode($arParam['~'.$k],true);
						}

					}
					$arParams[]=$arParam;
				}

				p($arParams);
				
				break;
			case 'import':
				$times=array(
							'start'=>date('Y-m-d H:i:s'),
							'stop'=>'');
				CModule::IncludeModule('iblock');
				$errors=array();
				$counts=array(
					'Added'=>0,
					'Updated'=>0,
					'Deleted'=>0,
					'Error'=>0
					);
				$arPriceBushes=[];
				$arSelect = Array("ID","XML_ID");
				$arFilter = Array("IBLOCK_ID"=>$CATALOG_PRICEBUSH_ID);
				$res = CIBlockElement::GetList(Array(), $arFilter, false, false, $arSelect);
				while($arFields = $res->GetNext())
				{
				 	$arPriceBushes[$arFields['XML_ID']]=$arFields['ID'];
				}
				$arItems=[];
				$arSelect = Array("ID","XML_ID");
				$arFilter = Array("IBLOCK_ID"=>$CATALOG_IBLOCK_ID);
				$res = CIBlockElement::GetList(Array(), $arFilter, false, false, $arSelect);
				while($arFields = $res->GetNext())
				{
				 	$arItems[$arFields['XML_ID']]=$arFields['ID'];
				}
				$arFilter = Array("IBLOCK_ID"=>$CATALOG_ACTIONS_ID);
				$arSelect = Array("ID", "NAME", "CODE", "XML_ID");
				$res = CIBlockElement::GetList(Array(), $arFilter, false, false, $arSelect);  
				while($arFields = $res->Fetch()) {  
					$made_els[$arFields['XML_ID']] = $arFields['ID'];
					$made_els_left[$arFields['XML_ID']] = $arFields['ID'];
					$made_els_codes[$arFields['CODE']] = $arFields['ID'];
				}
				$data=CParser::GetDataFromTable($parser_id,array('WebSite_ID'=>$WEBSITE_ID));
				while ($arItem=$data->GetNext()) {
					foreach ($arItem as $k => $v) {
						if(isJSON($arItem['~'.$k])){
							$arItem[$k]=json_decode($arItem['~'.$k],true);
							unset($arItem['~'.$k]);
						}
					}
					$arLoadProductArray = Array(
								"IBLOCK_ID"      => $CATALOG_ACTIONS_ID,
								"NAME"           => empty($arItem['~ActionTitle']) ? "[пустое_значение]" : $arItem['~ActionTitle'],
								"CODE"           => '',
								"XML_ID"         => $arItem['Action_ID'],
								"ACTIVE"         =>  "Y",
								"SORT"    => $arItem['OrderSpecialOffer'],
								"DATE_ACTIVE_FROM"=>date('d.m.Y H:i:s',strtotime($arItem['DateStart'])),
								"DATE_ACTIVE_TO"=>date('d.m.Y H:i:s',strtotime($arItem['DateEnd']))
					);
					
					if (empty($arLoadProductArray['CODE'])) {
						$arLoadProductArray['CODE'] = Cutil::translit($arLoadProductArray['NAME'], "ru", array("replace_space"=>"-", "replace_other"=>"-"));
					}
					if (!isset($made_els[$arItem['ИдентификаторНСИ']])) {
						if (isset($made_els_codes[$arLoadProductArray['CODE']])) {
							$original_code = $arLoadProductArray['CODE'];
							$cntr = 2;
							do {
								$arLoadProductArray['CODE'] = $original_code.'-'.$cntr;
								$cntr++;
								} while(isset($made_els_codes[$arLoadProductArray['CODE']]));
						}	
						
					}else{
						if($made_els[$arItem['Action_ID']]!=$made_els_codes[$arLoadProductArray['CODE']]){
							if (isset($made_els_codes[$arLoadProductArray['CODE']])) {
								$original_code = $arLoadProductArray['CODE'];
								$cntr = 2;
								do {
									if($made_els[$arItem['Action_ID']]!=$made_els_codes[$arLoadProductArray['CODE']]){
										//$arLoadProductArray['CODE'] = $original_code.'-'.$cntr;
										break;
									}else{
										$arLoadProductArray['CODE'] = $original_code.'-'.$cntr;
										$cntr++;
									}
									
									} while(isset($made_els_codes[$arLoadProductArray['CODE']]));
							}
						}
					}
					$arLoadProductArray['DETAIL_TEXT']='';
					$arLoadProductArray['DETAIL_TEXT_TYPE']='html';
					if($arItem['~Title01']!=''){
						$arLoadProductArray['DETAIL_TEXT'].='<h3>'.$arItem['~Title01'].'</h3>';
						
					}
					if($arItem['~Description01']!=''){
						$arLoadProductArray['DETAIL_TEXT'].='<p>'.$arItem['~Description01'].'</p>';
					}
					if($arItem['~Title02']!=''){
						$arLoadProductArray['DETAIL_TEXT'].='<h3>'.$arItem['~Title02'].'</h3>';
						
					}
					if($arItem['~Description02']!=''){
						$arLoadProductArray['DETAIL_TEXT'].='<p>'.$arItem['~Description02'].'</p>';
					}
					if($arItem['~Title03']!=''){
						$arLoadProductArray['DETAIL_TEXT'].='<h3>'.$arItem['~Title03'].'</h3>';
						
					}
					if($arItem['~Description03']!=''){
						$arLoadProductArray['DETAIL_TEXT'].='<p>'.$arItem['~Description03'].'</p>';
					}
					$arLoadProductArray['PROPERTY_VALUES']['special_template']=$arItem['VariantActionPage'];
					foreach ($arItem['regions'] as $k => $v) {
						if($arPriceBushes[$v['PriceBush_ID']]!=''){
							$arLoadProductArray['PROPERTY_VALUES']['geo_networks'][]=$arPriceBushes[$v['PriceBush_ID']];
						}
					}
					if($arItem['goodsApplying'][0]['Goods_ID']=='AllGoods'){
						if($arLoadProductArray['PROPERTY_VALUES']['special_template']!=0){
							$arLoadProductArray['PROPERTY_VALUES']['special_template']=1;
						}
					}else{
						foreach ($arItem['goodsApplying'] as $k => $v) {
							if($arItems[$v['Goods_ID']]!=''){
								$arLoadProductArray['PROPERTY_VALUES']['special_products'][]=$arItems[$v['Goods_ID']];
							}
						}
					}
					if($arItem["BannerMain_file"]!=''){
						$remote_file = $EXT_WAY.'/'.$arItem["BannerMain_file"];
						$name=explode('/',$arItem["BannerMain_file"])[1];
						$content=file_get_contents($remote_file);
						if(!empty($content)&&!empty($name)){
							if(!file_exists($_SERVER['DOCUMENT_ROOT']."/upload/special/")){
								mkdir($_SERVER['DOCUMENT_ROOT']."/upload/special/");
							}
							if(!file_exists($_SERVER['DOCUMENT_ROOT']."/upload/special/".$arItem['Action_ID']."/")){
								mkdir($_SERVER['DOCUMENT_ROOT']."/upload/special/".$arItem['Action_ID']."/");
							}
							if(!file_exists($_SERVER['DOCUMENT_ROOT']."/upload/special/".$arItem['Action_ID']."/".$name)){
								file_put_contents($_SERVER['DOCUMENT_ROOT']."/upload/special/".$arItem['Action_ID']."/".$name, $content);
							}
							$arLoadProductArray['PROPERTY_VALUES']['MAIN_BANNER'] = CFile::MakeFileArray($_SERVER['DOCUMENT_ROOT']."/upload/special/".$arItem['Action_ID']."/".$name);
							$arLoadProductArray['PROPERTY_VALUES']['SHOW_IN_MAIN']=1;
						}

					}
					if($arItem["BannerHome_file"]!=''){
						$remote_file = $EXT_WAY.'/'.$arItem["BannerHome_file"];
						$name=explode('/',$arItem["BannerHome_file"])[1];
						$content=file_get_contents($remote_file);
						if(!empty($content)&&!empty($name)){
							if(!file_exists($_SERVER['DOCUMENT_ROOT']."/upload/special/")){
								mkdir($_SERVER['DOCUMENT_ROOT']."/upload/special/");
							}
							if(!file_exists($_SERVER['DOCUMENT_ROOT']."/upload/special/".$arItem['Action_ID']."/")){
								mkdir($_SERVER['DOCUMENT_ROOT']."/upload/special/".$arItem['Action_ID']."/");
							}
							if(!file_exists($_SERVER['DOCUMENT_ROOT']."/upload/special/".$arItem['Action_ID']."/".$name)){
								file_put_contents($_SERVER['DOCUMENT_ROOT']."/upload/special/".$arItem['Action_ID']."/".$name, $content);
							}
							$arLoadProductArray['PREVIEW_PICTURE'] = CFile::MakeFileArray($_SERVER['DOCUMENT_ROOT']."/upload/special/".$arItem['Action_ID']."/".$name);
						}
					}
					if($arItem["BannerProductPageSmall_file"]!=''){
						$remote_file = $EXT_WAY.'/'.$arItem["BannerProductPageSmall_file"];
						$name=explode('/',$arItem["BannerProductPageSmall_file"])[1];
						$content=file_get_contents($remote_file);
						if(!empty($content)&&!empty($name)){
							if(!file_exists($_SERVER['DOCUMENT_ROOT']."/upload/special/")){
								mkdir($_SERVER['DOCUMENT_ROOT']."/upload/special/");
							}
							if(!file_exists($_SERVER['DOCUMENT_ROOT']."/upload/special/".$arItem['Action_ID']."/")){
								mkdir($_SERVER['DOCUMENT_ROOT']."/upload/special/".$arItem['Action_ID']."/");
							}
							if(!file_exists($_SERVER['DOCUMENT_ROOT']."/upload/special/".$arItem['Action_ID']."/".$name)){
								file_put_contents($_SERVER['DOCUMENT_ROOT']."/upload/special/".$arItem['Action_ID']."/".$name, $content);
							}
							$arLoadProductArray['PROPERTY_VALUES']['PIC_PRODUCT'] = CFile::MakeFileArray($_SERVER['DOCUMENT_ROOT']."/upload/special/".$arItem['Action_ID']."/".$name);
						}
					}
					if($arItem["BannerActionTitle_file"]!=''){
						$remote_file = $EXT_WAY.'/'.$arItem["BannerActionTitle_file"];
						$name=explode('/',$arItem["BannerActionTitle_file"])[1];
						$content=file_get_contents($remote_file);
						if(!empty($content)&&!empty($name)){
							if(!file_exists($_SERVER['DOCUMENT_ROOT']."/upload/special/")){
								mkdir($_SERVER['DOCUMENT_ROOT']."/upload/special/");
							}
							if(!file_exists($_SERVER['DOCUMENT_ROOT']."/upload/special/".$arItem['Action_ID']."/")){
								mkdir($_SERVER['DOCUMENT_ROOT']."/upload/special/".$arItem['Action_ID']."/");
							}
							if(!file_exists($_SERVER['DOCUMENT_ROOT']."/upload/special/".$arItem['Action_ID']."/".$name)){
								file_put_contents($_SERVER['DOCUMENT_ROOT']."/upload/special/".$arItem['Action_ID']."/".$name, $content);
							}
							$arLoadProductArray['DETAIL_PICTURE'] = CFile::MakeFileArray($_SERVER['DOCUMENT_ROOT']."/upload/special/".$arItem['Action_ID']."/".$name);
						}
					}	
            			
					
					/*p($arLoadProductArray);
					p($arItem);*/
					$el = new CIBlockElement;
					if (!isset($made_els[$arItem['Action_ID']])) {
						if($PRODUCT_ID = $el->Add($arLoadProductArray, false, false, false)) {
							$counts['Added']++;
							$made_els_codes[$arLoadProductArray['CODE']] = $PRODUCT_ID;
						} else {
							$counts['Error']++;
							$errors[]=$el->LAST_ERROR;									
						}

					}else{
						if($el->Update($made_els_left[$arItem['Action_ID']], $arLoadProductArray)){
							$counts['Updated']++;
						} else {
							$counts['Error']++;
							$errors[]=$el->LAST_ERROR;									
						}
						unset($made_els_left[$arItem['Action_ID']]);
					}
					/*p($arLoadProductArray);
					p($arItem);*/
				}
				foreach($made_els_left as $key=>$left_good_id){
					CIBlockElement::Delete($left_good_id);
					$counts['Deleted']++;
				}
				$times['stop']=date('Y-m-d H:i:s');
				p($counts);
				p($errors);
				p($times);
				CParser::AddLog($parser_id,'ActionsImport',array('MESSAGE'=>'Импорт Акций','DATA'=>json_encode(array('times'=>$times,'counts'=>$counts,'errors'=>$errors))));
				break;
		}
		break;

	case 'harakteristiki':
		switch ($_REQUEST['step']) {
			case 'getdata':
				$data=CParser::GetJSON($arTypes[$_REQUEST['act']]);
				//p($data);
				if(count($data)>0){
					CParser::AddDataTable($parser_id,$data[0]);
					CParser::InsertsToDataTable($parser_id,$data);
				}else{
					CParser::AddLog($parser_id,'HarakteristikiGetData',array('MESSAGE'=>'Ошибка получения данных от шлюза','DATA'=>''));
				}
				break;
			case 'printdata':
				$pars=$parser_id;
				$data=CParser::GetDataFromTable($pars,array(),array());
				$arParams=array();
				while ($arParam=$data->GetNext()) {
					foreach ($arParam as $k => $v) {
						if(isJSON($arParam['~'.$k])){
							$arParam[$k]=json_decode($arParam['~'.$k],true);
						}

					}
					$arParams[]=$arParam;
				}
				p($counts);
				p($errors);
				
				break;
			case 'import':
				$times=array(
							'start'=>date('Y-m-d H:i:s'),
							'stop'=>'');
				$params_parser_id='items_params';
				CModule::IncludeModule('iblock');
				$errors=array();
				$counts=array(
					'Added'=>0,
					'Updated'=>0,
					'Deleted'=>0,
					'Error'=>0
					);
				$arFilter = Array("IBLOCK_ID"=>$CATALOG_GROUP_PROP_ID);
				$arSelect = Array("ID", "NAME", "CODE", "XML_ID");
				$res = CIBlockElement::GetList(Array(), $arFilter, false, false, $arSelect);  
				while($arFields = $res->Fetch()) {  
					$made_els[$arFields['XML_ID']] = $arFields['ID'];
					$made_els_left[$arFields['XML_ID']] = $arFields['ID'];
					$made_els_codes[$arFields['CODE']] = $arFields['ID'];
				}
				$data=CParser::GetDataFromTable($parser_id);
				while ($arItem=$data->GetNext()) {
					foreach ($arItem as $k => $v) {
						if(isJSON($arItem['~'.$k])){
							$arItem[$k]=json_decode($arItem['~'.$k],true);
						}
					}
					$arPrms=[];
					$res=CParser::GetDataFromTable($params_parser_id,array('ТипХарактеристики'=>$arItem['ИдентификаторНСИ']),array());
					while ($arPrm=$res->GetNext()) {
						$arPrms[]=$arPrm['Идентификатор'];
					}
					$arItem['PARAMS']=$arPrms;
					$arParams[]=$arParam;
					$arLoadProductArray = Array(
								"IBLOCK_ID"      => $CATALOG_GROUP_PROP_ID,
								"NAME"           => empty($arItem['Наименование']) ? "[пустое_значение]" : $arItem['Наименование'],
								"CODE"           => '',
								"XML_ID"         => $arItem['ИдентификаторНСИ'],
								"ACTIVE"         => $arItem['ПометкаУдаления'] == 0 ? "Y" : "N",
								"SORT"    => floatval($arItem['Порядок'])*100,
								"PROPERTY_VALUES"    => array('PROPS'=>$arItem['PARAMS']),
							);
					if (empty($arLoadProductArray['CODE'])) {
						$arLoadProductArray['CODE'] = Cutil::translit($arLoadProductArray['NAME'], "ru", array("replace_space"=>"-", "replace_other"=>"-"));
					}
					if (!isset($made_els[$arItem['ИдентификаторНСИ']])) {
						if (isset($made_els_codes[$arLoadProductArray['CODE']])) {
							$original_code = $arLoadProductArray['CODE'];
							$cntr = 2;
							do {
								$arLoadProductArray['CODE'] = $original_code.'-'.$cntr;
								$cntr++;
								} while(isset($made_els_codes[$arLoadProductArray['CODE']]));
						}	
						
					}else{
						if($made_els[$arItem['ИдентификаторНСИ']]!=$made_els_codes[$arLoadProductArray['CODE']]){
							if (isset($made_els_codes[$arLoadProductArray['CODE']])) {
								$original_code = $arLoadProductArray['CODE'];
								$cntr = 2;
								do {
									if($made_els[$arItem['ИдентификаторНСИ']]!=$made_els_codes[$arLoadProductArray['CODE']]){
										//$arLoadProductArray['CODE'] = $original_code.'-'.$cntr;
										break;
									}else{
										$arLoadProductArray['CODE'] = $original_code.'-'.$cntr;
										$cntr++;
									}
									
									} while(isset($made_els_codes[$arLoadProductArray['CODE']]));
							}
						}
					}
					$el = new CIBlockElement;
					
					if (!isset($made_els[$arItem['ИдентификаторНСИ']])) {
						if($PRODUCT_ID = $el->Add($arLoadProductArray, false, false, false)) {
							$counts['Added']++;
							$made_els_codes[$arLoadProductArray['CODE']] = $PRODUCT_ID;
						} else {
							$counts['Error']++;
							$errors[]=$el->LAST_ERROR;									
						}

					}else{
						if($el->Update($made_els_left[$arItem['ИдентификаторНСИ']], $arLoadProductArray)){
							$counts['Updated']++;
						} else {
							$counts['Error']++;
							$errors[]=$el->LAST_ERROR;									
						}
						unset($made_els_left[$arItem['ИдентификаторНСИ']]);
					}
				}
				foreach($made_els_left as $key=>$left_good_id){
					CIBlockElement::Delete($left_good_id);
					$counts['Deleted']++;
				}
				/*p($made_els);
				p($made_els_left);
				p($made_els_codes);*/
				p($counts);
				p($errors);
				$times['stop']=date('Y-m-d H:i:s');
				CParser::AddLog($parser_id,'PropGroupImport',array('MESSAGE'=>'Импорт Групп свойств','DATA'=>json_encode(array('times'=>$times,'counts'=>$counts,'errors'=>$errors))));
				break;
			}
		break;
		case 'proplink':
		switch ($_REQUEST['step']) {
			case 'getdata':
				$data=CParser::GetJSON($arTypes[$_REQUEST['act']]);
				//p($data);
				if(count($data)>0){
					CParser::AddDataTable($parser_id,$data[0]);
					CParser::InsertsToDataTable($parser_id,$data);
				}else{
					CParser::AddLog($parser_id,'PropLinkGetData',array('MESSAGE'=>'Ошибка получения данных от шлюза','DATA'=>''));
				}
				break;
			case 'printdata':
				//p($parser_id);
				$data=CParser::GetDataFromTable($parser_id,array(),array());
				$arParams=array();
				while ($arParam=$data->GetNext()) {
					foreach ($arParam as $k => $v) {
						if(isJSON($arParam['~'.$k])){
							$arParam[$k]=json_decode($arParam['~'.$k],true);
						}

					}
					$arParams[]=$arParam;
				}
				p($arParams);
				p($counts);
				p($errors);
				
				break;
			case 'import':
				$times=array(
							'start'=>date('Y-m-d H:i:s'),
							'stop'=>'');
				$params_parser_id='items_params';
				CModule::IncludeModule('iblock');
				$errors=array();
				$counts=array(
					'Added'=>0,
					'Updated'=>0,
					'Deleted'=>0,
					'Error'=>0
					);
				$arFilter = Array("IBLOCK_ID"=>$CATALOG_IBLOCK_ID);
				$arSelect = Array("ID", "NAME", "CODE", "XML_ID");
				$res = CIBlockSection::GetList(Array(), $arFilter, false, $arSelect);  
				while($arFields = $res->Fetch()) {  
					$made_cats[$arFields['XML_ID']] = $arFields['ID'];
				}
				$arFilter = Array("IBLOCK_ID"=>$CATALOG_PROPS_ID);
				$arSelect = Array("ID", "NAME", "CODE", "XML_ID");
				$res = CIBlockElement::GetList(Array(), $arFilter, false, false, $arSelect);  
				while($arFields = $res->Fetch()) {  
					/*p($arFields['NAME'].'-'.$arFields['XML_ID']);*/
					$PROP_TILE=[];
					$PROP_TILE_XML=[];
					$PROP_FILTER=[];
					$PROP_FILTER_XML=[];
					$data=CParser::GetDataFromTable($parser_id,array('Характеристика'=>$arFields['XML_ID']),array());
					$arParams=array();
					while ($arParam=$data->GetNext()) {
						if($arParam['Титул']==1){
							if($made_cats[$arParam['ROOT_ID']]!=''){
								$PROP_TILE[]=$made_cats[$arParam['ROOT_ID']];
								$PROP_TILE_XML[]=$arParam['ROOT_ID'];
								$counts['Added']++;
							}else{
								$counts['Error']++;
								$errors[]=$arParam['ROOT_ID'].' Категория не найдена';
							}
						}
						if($arParam['ХарактеристикиФильтров']==1){
							if($made_cats[$arParam['ROOT_ID']]!=''){
								$PROP_FILTER[]=$made_cats[$arParam['ROOT_ID']];
								$PROP_FILTER_XML[]=$arParam['ROOT_ID'];
								$counts['Added']++;
							}else{
								$counts['Error']++;
								$errors[]=$arParam['ROOT_ID'].' Категория не найдена';
							}
							
						}
					}
					/*p($PROP_TILE);
					p($PROP_TILE_XML);
					p($PROP_FILTER);
					p($PROP_FILTER_XML);*/
					$res1=CIBlockElement::SetPropertyValuesEx($arFields['ID'],$CATALOG_PROPS_ID,array('TILE_CATS'=>$PROP_TILE,'FILTER_CATS'=>$PROP_FILTER,'TILE_CATS_XML'=>$PROP_TILE_XML,'FILTER_CATS_XML'=>$PROP_FILTER_XML));
					p($res1);
				}

				
				/*p($made_els);
				p($made_els_left);
				p($made_els_codes);*/
				p($counts);
				p($errors);
				$times['stop']=date('Y-m-d H:i:s');
				p($times);
				CParser::AddLog($parser_id,'PropLinkImport',array('MESSAGE'=>'Импорт Связей свойств','DATA'=>json_encode(array('times'=>$times,'counts'=>$counts,'errors'=>$errors))));
				break;
			}
		break;
	case 'groups':
		$cats_parser_id=$parser_id.'_cats';
		$params_parser_id=$parser_id.'_params';
		switch ($_REQUEST['step']) {
			case 'getdata':
				$data=CParser::GetJSON($arTypes[$_REQUEST['act']]);
				/*p($data);
					die();*/
				if(count($data)>0){

					CParser::AddDataTable($cats_parser_id,$data['cats'][0]);
					CParser::InsertsToDataTable($cats_parser_id,$data['cats']);
					CParser::AddDataTable($params_parser_id,current($data['params']));
					CParser::InsertsToDataTable($params_parser_id,$data['params']);
				}else{
					CParser::AddLog($parser_id,'GroupsGetData',array('MESSAGE'=>'Ошибка получения данных от шлюза','DATA'=>''));
				}
				
				break;
			case 'printdata':
				$pars=$parser_id."_".$_REQUEST['get'];
				$data=CParser::GetDataFromTable($pars,array(),array());
				$arParams=array();
				while ($arParam=$data->GetNext()) {
					foreach ($arParam as $k => $v) {
						if(isJSON($arParam['~'.$k])){
							$arParam[$k]=json_decode($arParam['~'.$k],true);
						}

					}
					$arParams[]=$arParam;
				}
				p($arParams);
				break;
			case 'import':
				//$datacnt=CParser::GetDataCountTable($params_parser_id,array(),array(0,10));
				//$cats_parser_id=
				switch ($_REQUEST['get']) {
					case 'params':
						$times=array(
							'start'=>date('Y-m-d H:i:s'),
							'stop'=>'');
						$errors=array();
						$counts=array(
							'Added'=>0,
							'Updated'=>0,
							'Deleted'=>0,
							'Error'=>0
							);
						$data=CParser::GetDataFromTable($params_parser_id,array());
						$arParamsP=array();
						while ($arParamP=$data->GetNext()) {
							foreach ($arParamP as $k => $v) {
								if(isJSON($arParamP['~'.$k])){
									$arParamP[$k]=json_decode($arParamP['~'.$k],true);
								}

							}
							$arParamsP[]=$arParamP;
						}
						$rsData = CUserTypeEntity::GetList( array($by=>$order), array('ENTITY_ID'=>"IBLOCK_".$CATALOG_IBLOCK_ID."_SECTION") );
						while($arRes = $rsData->Fetch())
						{
							$arParams[$arRes['XML_ID']]=$arRes;
						}
						//p($arParamsP);
						foreach ($arParamsP as $k => $v) {
							$arFields = Array(
								'ENTITY_ID'=>"IBLOCK_".$CATALOG_IBLOCK_ID."_SECTION",
								"FIELD_NAME" => "UF_".$v['Код'],
								"XML_ID"=>$v['Идентификатор'],
								'HELP_MESSAGE'      => array(
								        'ru'    => $v['Идентификатор'],
								'en'    => $v['Идентификатор'],
								    ),
								"EDIT_FORM_LABEL" => Array("ru"=>$v['~Наименование'], "en"=>$v['~Наименование']),

								);
								switch ($v['ТипЗначения']) {
									case 'Файл, Строка':
										$arFields['USER_TYPE_ID']='file';
										break;
									
									default:
										$arFields['USER_TYPE_ID']='string';
										break;
								}
							if(!$arParams[$v['Идентификатор']]){
								$obUserField  = new CUserTypeEntity;
								$obUserField->Add($arFields);
								$counts['Added']++;
							}else{
								$obUserField  = new CUserTypeEntity;
								$obUserField->Update($arParams[$v['Идентификатор']]['ID'],$arFields);
								$counts['Updated']++;
							}
						}
						p($counts);
						p($errors);
						$times['stop']=date('Y-m-d H:i:s');
						CParser::AddLog($params_parser_id,'GroupsPropertiesImport',array('MESSAGE'=>'Импорт свойств групп','DATA'=>json_encode(array('times'=>$times,'counts'=>$counts,'errors'=>$errors))));		
						
						
						break;
					case 'cats':
						$times=array(
							'start'=>date('Y-m-d H:i:s'),
							'stop'=>'');
						CModule::IncludeModule('iblock');
						$rsData = CUserTypeEntity::GetList( array($by=>$order), array('ENTITY_ID'=>"IBLOCK_".$CATALOG_IBLOCK_ID."_SECTION") );
						while($arRes = $rsData->Fetch())
						{
							$arParams[$arRes['XML_ID']]=$arRes;
						}
						$made_cats = [];
						
						$arFilter = Array("IBLOCK_ID"=>$CATALOG_IBLOCK_ID);
						$arSelect = Array("ID", "NAME", "CODE", "XML_ID");
						$res = CIBlockSection::GetList(Array(), $arFilter, false, $arSelect);  
						while($arFields = $res->Fetch()) {  
							$made_cats[$arFields['XML_ID']] = $arFields['ID'];
							$made_cats_c[$arFields['XML_ID']] = $arFields['CODE'];
							$made_cats_els[$arFields['XML_ID']] = $arFields['ID'];
							$made_cats_code[$arFields['CODE']] = $arFields['ID'];
						}
						$errors=array();
						$counts=array(
							'Added'=>0,
							'Updated'=>0,
							'Deleted'=>0,
							'Error'=>0
							);
						function GetTree($id){
								global $arParams;
								global $made_cats;
								global $cats_parser_id;
								global $CATALOG_IBLOCK_ID;
								global $counts;
								global $errors;
								global $made_cats_code;
								global $made_cats_els;
								global $made_cats_c;
								$arCats=[];
								$data=CParser::GetDataFromTable($cats_parser_id,array('Родитель'=>$id));
								while ($arCat=$data->GetNext()) {
									$arCat['harakteristiki']=json_decode($arCat['~harakteristiki'],true);
									$el = new CIBlockSection;
									
									$arLoadProductArray = Array(
										"IBLOCK_SECTION_ID" => empty($arCat['Родитель']) ? false : $made_cats[$arCat['Родитель']],
										"IBLOCK_ID"      => $CATALOG_IBLOCK_ID,
										"NAME"           => empty($arCat['~ПолноеНаименование']) ? "[пустое_значение]" : $arCat['~ПолноеНаименование'],
										"CODE"           => $made_cats_c[$arCat['ИдентификаторНСИ']],
										"XML_ID"           => $arCat['ИдентификаторНСИ'],
										"ACTIVE"         => $arCat['ПометкаУдаления'] == 0 ? "Y" : "N",
									);

									if (empty($arLoadProductArray['CODE'])) {
										$arLoadProductArray['CODE'] = Cutil::translit($arLoadProductArray['NAME'], "ru", array("replace_space"=>"-", "replace_other"=>"-"));
									}
									
									if($made_cats[$arCat['ИдентификаторНСИ']]!=$made_cats_code[$arLoadProductArray['CODE']]){
										if (isset($made_cats_code[$arLoadProductArray['CODE']])) {
											$original_code = $arLoadProductArray['CODE'];
											$cntr = 2;
											do {
												if($made_cats[$arCat['ИдентификаторНСИ']]==$made_cats_code[$arLoadProductArray['CODE']]){
													//$arLoadProductArray['CODE'] = $original_code.'-'.$cntr;
													break;
												}else{
													$arLoadProductArray['CODE'] = $original_code.'-'.$cntr;
													$cntr++;
												}
												
												} while(isset($made_cats_code[$arLoadProductArray['CODE']]));
										}
									}
									
									if(!empty($arCat['harakteristiki'])){
										foreach ($arCat['harakteristiki'] as $k => $v) {
											if($arParams[$arCat['Характеристика']]['FIELD_NAME']!=''&&$v['Значение']!=''){
												$arLoadProductArray[$arParams[$arCat['Характеристика']]['FIELD_NAME']]=$v['Значение'];
											}
										}

									}
																		
									if ($made_cats[$arCat['ИдентификаторНСИ']]=='') {

										if($PRODUCT_ID = $el->Add($arLoadProductArray)) {
											$counts['Added']++;
											$made_cats[$arCat['ИдентификаторНСИ']] = $PRODUCT_ID;
											$made_cats_code[$arLoadProductArray['CODE']] = $PRODUCT_ID;
										} else {
											$counts['Error']++;
											//p($arLoadProductArray);
											$errors[]="Error Section add: ".$arLoadProductArray['NAME']." ".$el->LAST_ERROR;
										}
										unset($made_cats_els[$arCat['ИдентификаторНСИ']]);
									} else {
										
										if($el->Update($made_cats[$arCat['ИдентификаторНСИ']], $arLoadProductArray)){
											$counts['Updated']++;
										}else{
											$counts['Error']++;
											//p($arLoadProductArray);
											$errors[]="Error Section add: ".$arLoadProductArray['NAME']." ".$el->LAST_ERROR;
										}
										unset($made_cats_els[$arCat['ИдентификаторНСИ']]);
									}

									$arCat['CATS']=GetTree($arCat['ИдентификаторНСИ']);
									if(count($arCat['CATS'])==0)unset($arCat['CATS']);
									$arCats[]=$arCat;
									$n++;
								}
								return $arCats;
								
						}
						$tree = GetTree('');
						//p($tree);
						
						
						
						
						
						
						if (!empty($made_cats_els)) {
							foreach($made_cats_els as $ELEMENT_ID => $i) {
								$arFilter = Array("IBLOCK_ID"=>$CATALOG_IBLOCK_ID, "SECTION_ID"=>$i);
								$arSelect = Array("ID", "NAME", "CODE", "XML_ID");
								$res = CIBlockElement::GetList(Array(), $arFilter, false, false, $arSelect);  
								$inner_items = array();
								while($arFields = $res->Fetch()) { 
									CIBlockElement::Delete($arFields['ID']);
									\Bitrix\Iblock\PropertyIndex\Manager::deleteElementIndex($CATALOG_IBLOCK_ID,$arFields['ID']);
								}
								if(CIBlockSection::Delete($i)){
									$counts['Deleted']++;
								}else{
									$counts['Error']++;
								}
						
							}
						}

						p($counts);
						p($errors);
						$times['stop']=date('Y-m-d H:i:s');
						CParser::AddLog($cats_parser_id,'GroupsImport',array('MESSAGE'=>'Импорт групп','DATA'=>json_encode(array('times'=>$times,'counts'=>$counts,'errors'=>$errors))));
						break;
					default:
						# code...
						break;
				}
				break;
			default:

				break;
		}
		break;
	case 'items':
		$params_parser_id=$parser_id."_params";
		$marki_parser_id=$parser_id."_marki";
		$items_parser_id=$parser_id."_items";
		$files_parser_id=$parser_id."_files";
		switch ($_REQUEST['step']) {
			case 'getdata':
				$data=CParser::GetJSON($arTypes[$_REQUEST['act']]);
				if(count($data)>0){
				foreach ($data as $key => $value) {
					if($key!="count"){
							CParser::AddDataTable($parser_id."_".$key,current($data[$key]));
							CParser::InsertsToDataTable($parser_id."_".$key,$data[$key]);
					}
				}
				
				}else{
					CParser::AddLog($parser_id,'ItemsGetData',array('MESSAGE'=>'Ошибка получения данных от шлюза','DATA'=>''));
				
				}
				break;
			case 'printdata':
				$pars=$parser_id."_".$_REQUEST['get'];
				if($_REQUEST['limit']){
					$data=CParser::GetDataFromTable($pars,array(),array(0,3));
				}elseif($_REQUEST['xml']){
					$data=CParser::GetDataFromTable($pars,array('ИдентификаторНСИ'=>$_REQUEST['xml']),array());
				}else{
					$data=CParser::GetDataFromTable($pars,array(),array());
				}
				$arParams=array();
				while ($arParam=$data->GetNext()) {
					foreach ($arParam as $k => $v) {
						if(isJSON($arParam['~'.$k])){
							$arParam[$k]=json_decode($arParam['~'.$k],true);
						}

					}
					$arParams[]=$arParam;
				}
				p($arParams);
				break;
			case 'import':
				switch ($_REQUEST['get']) {
					case 'params':
						$times=array(
							'start'=>date('Y-m-d H:i:s'),
							'stop'=>'');
						$errors=array();
						$counts=array(
							'Added'=>0,
							'Updated'=>0,
							'Deleted'=>0,
							'Error'=>0
							);
						$data=CParser::GetDataFromTable($parser_id."_".$_REQUEST['get'],array(),array());
						//CParser::AddDataTable('items_har',array('XML_ID','CODE'));
						$arParams=array();
						while ($arParam=$data->GetNext()) {
							foreach ($arParam as $k => $v) {
								if(isJSON($arParam['~'.$k])){
									$arParam[$k]=json_decode($arParam['~'.$k],true);
								}

							}
							$arParams[]=$arParam;
						}
						$filter_arr=array('103a479e-6104-11e5-b5fc-ac162d7b6f40');

						CModule::IncludeModule('iblock');
						$arFilter = Array("IBLOCK_ID"=>$CATALOG_PROPS_ID);
						$arSelect = Array("ID", "NAME", "CODE", "XML_ID");
						$res = CIBlockElement::GetList(Array(), $arFilter, false, false, $arSelect);  
						while($arFields = $res->Fetch()) {  
							$made_els[$arFields['XML_ID']] = $arFields['ID'];
							$made_els_left[$arFields['XML_ID']] = $arFields['ID'];
							$made_els_codes[$arFields['CODE']] = $arFields['ID'];
						}
						$prop_list = [];
						$properties = CIBlockProperty::GetList(Array("sort"=>"asc", "name"=>"asc"), Array("IBLOCK_ID"=>$CATALOG_IBLOCK_ID));
						while ($prop_fields = $properties->GetNext()) {
							if ($prop_fields["SORT"] < 100) {
								continue;
							}
							$prop_list[$prop_fields['CODE']] = $prop_fields;
						}
						$SORT=100;
						
						foreach ($arParams as $key => $param) {
							$param['КодНаСайте'] = preg_replace('/[\.\s\t]/ui', '_', $param['КодНаСайте']);
			
							if ($param['КодНаСайте'] == '') {
								if($param['ХарактеристикиФильтров']==1||in_array($param['Идентификатор'],$filter_arr)){
									$param['КодНаСайте'] = 'filter_'.$param['Код'];
								}else{
									$param['КодНаСайте'] = 'prop_'.$param['Код'];
								}
							}
							$param['КодНаСайте'] = preg_replace('/[\s\t\.]+/ui', '', $param['КодНаСайте']);
							
							$arFields = Array(
								"NAME" => $param['~Наименование'],
								"ACTIVE" => $param['ПометкаУдаления'] == 0 ? "Y" : "N",
								"SORT" => $SORT,
								"CODE" => $param['КодНаСайте'],
								"XML_ID" => $param['Идентификатор'],
								"HINT" => $param['edinicaIzmereniya']['Наименование'],
								"COL_COUNT" => "50",
								"FILTRABLE" => "Y",
								"PROPERTY_TYPE" => "S",
								"USER_TYPE" => "",
								"IBLOCK_ID" => $CATALOG_IBLOCK_ID,
							);
							$arLoadProductArray = Array(
								"NAME" => $param['~Наименование'],
								"ACTIVE" => $param['ПометкаУдаления'] == 0 ? "Y" : "N",
								"SORT" => $param['НомерТитула'],
								"CODE" => $param['КодНаСайте'],
								"XML_ID" => $param['Идентификатор'],
								"IBLOCK_ID" => $CATALOG_PROPS_ID,
							);
							if ($param['ТипЗначения'] == 'Изображения') {
								$arFields["PROPERTY_TYPE"] = 'F';
								$arFields["FILE_TYPE"] = 'jpg, gif, bmp, png, jpeg';
								$arFields["FILTRABLE"] = 'N';
							} elseif ($param['ТипЗначения'] == 'Файл') {
								$arFields["PROPERTY_TYPE"] = 'F';
								$arFields["FILE_TYPE"] = '';
								$arFields["FILTRABLE"] = 'N';
							} elseif ($param['КодНаСайте'] == 'seo_title' || $param['КодНаСайте'] == 'seo_key' || $param['КодНаСайте'] == 'seo_descr') {
								$arFields["ROW_COUNT"] = '3';
							}else{
								
							}

							if($param['ХарактеристикиФильтров']==1||in_array($param['Идентификатор'],$filter_arr)){
								$arFields['SMART_FILTER']="Y";
								if ($param['Диапазон'] == 1) {
									$arFields['DISPLAY_TYPE']='A';
									$arFields['PROPERTY_TYPE']='N';
								}
							}else{
								$arFields['SMART_FILTER']="N";
							}
							if (substr($arFields["CODE"], 0, 2) == '3d') {
								$arFields["CODE"] = 'prop_'.$arFields["CODE"];
								$arFields["FILE_TYPE"] = '';
							}
							

							$elp = new CIBlockProperty;
							$el = new CIBlockElement;
							
							if (!isset($prop_list[$arFields["CODE"]])) {
								if($PRODUCT_ID = $elp->Add($arFields)) {
									$counts['Added']++;
								} else {
									$counts['Error']++;
									$errors[]="Error prop add: ".$elp->LAST_ERROR;
								}
							} else {
								if($elp->Update($prop_list[$arFields["CODE"]]['ID'], $arFields)){
									$counts['Updated']++;
								}else{
									$counts['Error']++;
									$errors[]="Error prop update: ".$elp->LAST_ERROR;
								}
								
							}

							if (!isset($made_els[$param['Идентификатор']])) {
								if($PRODUCT_ID = $el->Add($arLoadProductArray, false, false, false)) {
									$made_els[$arLoadProductArray['CODE']] = $PRODUCT_ID;
									$made_els_codes[$param['Идентификатор']] = $PRODUCT_ID;
								} else {
									$counts['Error']++;
									$errors[]=$el->LAST_ERROR;									
								}

							}else{
								if($el->Update($made_els_left[$param['Идентификатор']], $arLoadProductArray)){

								} else {
									$counts['Error']++;
									$errors[]=$el->LAST_ERROR;									
								}
								unset($made_els_left[$param['Идентификатор']]);
							}
							if($param['Титул']==1){
								CIBlockElement::SetPropertyValuesEx($made_els[$param['Идентификатор']],$CATALOG_PROPS_ID,array('TILE'=>100));
							}else{
								CIBlockElement::SetPropertyValuesEx($made_els[$param['Идентификатор']],$CATALOG_PROPS_ID,array('TILE'=>false));
							}
							unset($prop_list[$arFields["CODE"]]);
							$SORT += 1;
						}
						foreach($made_els_left as $key=>$left_good_id){
							CIBlockElement::Delete($left_good_id);
							
						}
						foreach ($prop_list as $k => $v) {
							CIBlockProperty::Delete($v['ID']);
							$counts['Deleted']++;
						}
						
						$times['stop']=date('Y-m-d H:i:s');
						p($counts);
						p($errors);
						p($times);
						CParser::AddLog($params_parser_id,'PropertiesImport',array('MESSAGE'=>'Импорт свойств','DATA'=>json_encode(array('times'=>$times,'counts'=>$counts,'errors'=>$errors))));
						if($counts['Added']>0||$counts['Deleted']>0){
							$times=array(
							'start'=>date('Y-m-d H:i:s'),
							'stop'=>'');
							$errors=array();
							$counts=array();
							$index = \Bitrix\Iblock\PropertyIndex\Manager::createIndexer($CATALOG_IBLOCK_ID);
							$index->startIndex();
							$index->continueIndex(0); // создание без ограничения по времени
							$index->endIndex();
							$times['stop']=date('Y-m-d H:i:s');
							CParser::AddLog($params_parser_id,'IndexRecreateImport',array('MESSAGE'=>'Пересоздание фасетного индекса','DATA'=>json_encode(array('times'=>$times,'counts'=>$counts,'errors'=>$errors))));		
						}
						break;
					case 'marki':

						CModule::IncludeModule('iblock');
						$times=array(
							'start'=>date('Y-m-d H:i:s'),
							'stop'=>'');
						$errors=array();
						$counts=array(
							'Added'=>0,
							'Updated'=>0,
							'Deleted'=>0,
							'Error'=>0
							);
						$arFilter = Array("IBLOCK_ID"=>$CATALOG_MARKI_ID);
						$arSelect = Array("ID", "NAME", "CODE", "XML_ID");
						$res = CIBlockElement::GetList(Array(), $arFilter, false, false, $arSelect);  
						while($arFields = $res->Fetch()) {  
							$made_els[$arFields['XML_ID']] = $arFields['ID'];
							$made_els_left[$arFields['XML_ID']] = $arFields['ID'];
							$made_els_codes[$arFields['CODE']] = $arFields['ID'];
						}
						$data=CParser::GetDataFromTable($marki_parser_id);
						while ($arItem=$data->GetNext()) {
							foreach ($arItem as $k => $v) {
								if(isJSON($arItem['~'.$k])){
									$arItem[$k]=json_decode($arItem['~'.$k],true);
								}
							}
							$arLoadProductArray = Array(
										"IBLOCK_ID"      => $CATALOG_MARKI_ID,
										"NAME"           => empty($arItem['~Наименование']) ? "[пустое_значение]" : $arItem['~Наименование'],
										"CODE"           => '',
										"XML_ID"         => $arItem['ИдентификаторНСИ'],
										"ACTIVE"         => $arItem['ПометкаУдаления'] == 0 ? "Y" : "N",
										"SORT"    => '500',
										"PROPERTY_VALUES"    => [],
									);
							if (empty($arLoadProductArray['CODE'])) {
								$arLoadProductArray['CODE'] = Cutil::translit($arLoadProductArray['NAME'], "ru", array("replace_space"=>"-", "replace_other"=>"-"));
							}
							if (!isset($made_els[$arItem['ИдентификаторНСИ']])) {
								if (isset($made_els_codes[$arLoadProductArray['CODE']])) {
									$original_code = $arLoadProductArray['CODE'];
									$cntr = 2;
									do {
										$arLoadProductArray['CODE'] = $original_code.'-'.$cntr;
										$cntr++;
										} while(isset($made_els_codes[$arLoadProductArray['CODE']]));
								}	
								
							}else{
								if($made_els[$arItem['ИдентификаторНСИ']]!=$made_els_codes[$arLoadProductArray['CODE']]){
									if (isset($made_els_codes[$arLoadProductArray['CODE']])) {
										$original_code = $arLoadProductArray['CODE'];
										$cntr = 2;
										do {
											if($made_els[$arItem['ИдентификаторНСИ']]==$made_els_codes[$arLoadProductArray['CODE']]){
												//$arLoadProductArray['CODE'] = $original_code.'-'.$cntr;
												break;
											}else{
												$arLoadProductArray['CODE'] = $original_code.'-'.$cntr;
												$cntr++;
											}
											
											} while(isset($made_els_codes[$arLoadProductArray['CODE']]));
									}
								}
							}
							$el = new CIBlockElement;
							/*if($arItem['ИдентификаторНСИ']=='00081e43-15dd-47cb-b374-d3a5639525c2'){
								p($arLoadProductArray);

							}*/
							if (!isset($made_els[$arItem['ИдентификаторНСИ']])) {
								if($PRODUCT_ID = $el->Add($arLoadProductArray, false, false, false)) {
									$counts['Added']++;
									$made_els_codes[$arLoadProductArray['CODE']] = $PRODUCT_ID;
								} else {
									$counts['Error']++;
									$errors[]=$el->LAST_ERROR;									
								}

							}else{
								if($el->Update($made_els_left[$arItem['ИдентификаторНСИ']], $arLoadProductArray)){
									$counts['Updated']++;
								} else {
									$counts['Error']++;
									$errors[]=$el->LAST_ERROR;									
								}
								unset($made_els_left[$arItem['ИдентификаторНСИ']]);
							}
						}
						foreach($made_els_left as $key=>$left_good_id){
							CIBlockElement::Delete($left_good_id);
							$counts['Deleted']++;
						}
						/*p($made_els);
						p($made_els_left);
						p($made_els_codes);*/
						p($counts);
						p($errors);
						$times['stop']=date('Y-m-d H:i:s');
						CParser::AddLog($marki_parser_id,'MarkiImport',array('MESSAGE'=>'Импорт Брендов','DATA'=>json_encode(array('times'=>$times,'counts'=>$counts,'errors'=>$errors))));
						break;
					case 'itemsfull':
						$times=array(
							'start'=>date('Y-m-d H:i:s'),
							'stop'=>'');
						$errors=array();
						$counts=array(
							'Added'=>0,
							'Updated'=>0,
							'NoUpdated'=>0,
							'Deleted'=>0,
							'Error'=>0
							);
						Cmodule::IncludeModule('iblock');
						$arItems=[];
						$prop_list = [];
						$properties = CIBlockProperty::GetList(Array("sort"=>"asc", "name"=>"asc"), Array("IBLOCK_ID"=>$CATALOG_IBLOCK_ID));
						while ($prop_fields = $properties->GetNext()) {
							
							$prop_list[$prop_fields['XML_ID']] = $prop_fields;
						}

						$made_els = [];
						$made_els_codes = [];
						$made_md5=[];
						$arFilter = Array("IBLOCK_ID"=>$CATALOG_MARKI_ID);
						$arSelect = Array("ID", "NAME", "CODE", "XML_ID");
						$res = CIBlockElement::GetList(Array(), $arFilter, false, false, $arSelect);  
						while($arFields = $res->Fetch()) {  
							$marki[$arFields['XML_ID']] = $arFields['ID'];
							
						}

						$arFilter = Array("IBLOCK_ID"=>$CATALOG_IBLOCK_ID);
						$arSelect = Array("ID", "NAME", "CODE", "XML_ID",'PROPERTY_MD5DATA');
						$res = CIBlockElement::GetList(Array(), $arFilter, false, false, $arSelect);  
						while($arFields = $res->Fetch()) {  
							$made_els[$arFields['XML_ID']] = $arFields['ID'];
							$made_els_left[$arFields['XML_ID']] = $arFields['ID'];
							$made_els_codes[$arFields['CODE']] = $arFields['ID'];
							$made_md5[$arFields['XML_ID']]=$arFields['PROPERTY_MD5DATA_VALUE'];
						}
						$files=[];
						$data=CParser::GetDataFromTable($files_parser_id,array());
						while ($arItem=$data->GetNext()) {
							foreach ($arItem as $k => $v) {
								if(isJSON($arItem['~'.$k])){
									$arItem[$k]=json_decode($arItem['~'.$k],true);
								}
							}
							$files[$arItem['ИдентификаторНСИ']]=$arItem;
						}
						$arFilter = Array("IBLOCK_ID"=>$CATALOG_IBLOCK_ID);
						$arSelect = Array("ID", "NAME", "CODE", "XML_ID");
						$res = CIBlockSection::GetList(Array(), $arFilter, false, $arSelect);  
						while($arFields = $res->Fetch()) {  
							$data=CParser::GetDataFromTable($items_parser_id,array('cats'=>$arFields['XML_ID']));
							while ($arItem=$data->GetNext()) {
								unset($arItem['NUMBER']);
								unset($arItem['~NUMBER']);
								$md5summ=sha1(json_encode($arItem));
								//if($md5summ!=$made_md5[$arItem['ИдентификаторНСИ']]){
									foreach ($arItem as $k => $v) {
										if(isJSON($arItem['~'.$k])){
											$arItem[$k]=json_decode($arItem['~'.$k],true);
										}
									}
									$arLoadProductArray = Array(
											"IBLOCK_SECTION_ID" => $arFields['ID'],
											"IBLOCK_ID"      => $CATALOG_IBLOCK_ID,
											"NAME"           => empty($arItem['~Наименование']) ? "[пустое_значение]" : $arItem['~Наименование'],
											"CODE"           => '',
											"XML_ID"         => $arItem['ИдентификаторНСИ'],
											"ACTIVE"         => $arItem['ПометкаУдаления'] == 0 ? "Y" : "N",
											"SORT"    => '500',
											"PROPERTY_VALUES"    => [],
									);
									$arLoadProductArray["PROPERTY_VALUES"]['CODE'] = $arItem['Код'];
									$arLoadProductArray["PROPERTY_VALUES"]['GROUP'] = $arFields['ID'];
									$arLoadProductArray["PROPERTY_VALUES"]['OWNER'] = $arItem['ВладелецНоменклатуры'];
									$arLoadProductArray["PROPERTY_VALUES"]['ID_1C'] = $arItem['nomenclatura_1c_id'];
									$arLoadProductArray["PROPERTY_VALUES"]['NAME'] = $arItem['~Наименование'];
									$arLoadProductArray["PROPERTY_VALUES"]['FULL_NAME'] = $arItem['~ПолноеНаименование'];
									$arLoadProductArray["PROPERTY_VALUES"]['EL_BRAND'] = $arItem['Марка'];
									$arLoadProductArray["PROPERTY_VALUES"]['MD5DATA'] = $md5summ;
									if (!empty($arItem['~Марка']) && isset($marki[$arItem['~Марка']])) {
										$arLoadProductArray["PROPERTY_VALUES"]['EL_BRAND'] = $marki[$arItem['~Марка']];
									}
									
									
									if (!empty($arItem['harakteristiki'])) {
										//////p$prop_list);
										foreach($arItem['harakteristiki'] as $param_code => $param_value) {
											//////p$param_value);
											$cur_param = $prop_list[$param_value['Характеристика']];
											
											if(!empty($param_value['Значение'])){
												if(isset($files[$param_value['Значение']])){
													$remote_file = $EXT_WAY.$FILES_PATH.$files[$param_value['Значение']]['ТекущаяВерсияПутьКФайлу'];
													$name=$files[$param_value['Значение']]['ТекущаяВерсияПутьКФайлу'];
													$content=file_get_contents($remote_file);
													if(!empty($content)&&!empty($name)){
														if(!file_exists($_SERVER['DOCUMENT_ROOT']."/upload/goods/")){
															mkdir($_SERVER['DOCUMENT_ROOT']."/upload/goods/");
														}
														if(!file_exists($_SERVER['DOCUMENT_ROOT']."/upload/goods/item_".$arFields['ID']."/")){
															mkdir($_SERVER['DOCUMENT_ROOT']."/upload/goods/item_".$arFields['ID']."/");
														}
														if(!file_exists($_SERVER['DOCUMENT_ROOT']."/upload/goods/item_".$arFields['ID']."/".$name)){
															file_put_contents($_SERVER['DOCUMENT_ROOT']."/upload/goods/item_".$arFields['ID']."/".$name, $content);
														}
														$param_value['Значение'] = CFile::MakeFileArray($_SERVER['DOCUMENT_ROOT']."/upload/goods/item_".$arFields['ID']."/".$name);
													}else{
														$param_value['Значение']='';
													}
												}elseif(isset($items['marki'][$param_value['Значение']])){
													$param_value['Значение'] = $items['marki'][$param_value['Значение']]['Наименование'];
												}else{
												
												}
											}
											
											if(!empty($param_value['Значение'])){
												if($param_value['Характеристика']=='c0da4812-6381-11e5-b5fc-ac162d7b6f40'){
													$arItem['item_name']=$param_value['Значение'];
												}else{
													$arLoadProductArray["PROPERTY_VALUES"][$cur_param['ID']] = $param_value['Значение'];
												}
											}
										}
									}

									$NAME = $arLoadProductArray['NAME'];
									if (!empty($arItem['item_name'])) {
										$NAME = $arItem['item_name'];
									} elseif (!empty($arItem['~ПолноеНаименование'])) {
										$NAME = $arItem['~ПолноеНаименование'];
									}
									$arLoadProductArray['NAME'] = $NAME;
									
									if (empty($arLoadProductArray['CODE'])) {
										$arLoadProductArray['CODE'] = Cutil::translit($arLoadProductArray['NAME'], "ru", array("replace_space"=>"-", "replace_other"=>"-"));
									}
									if (!isset($made_els[$arItem['ИдентификаторНСИ']])) {
										if (isset($made_els_codes[$arLoadProductArray['CODE']])) {
											$original_code = $arLoadProductArray['CODE'];
											$cntr = 2;
											do {
												$arLoadProductArray['CODE'] = $original_code.'-'.$cntr;
												$cntr++;
												} while(isset($made_els_codes[$arLoadProductArray['CODE']]));
										}	
										
									}else{
										if($made_els[$arItem['ИдентификаторНСИ']]!=$made_els_codes[$arLoadProductArray['CODE']]){
											if (isset($made_els_codes[$arLoadProductArray['CODE']])) {
												$original_code = $arLoadProductArray['CODE'];
												$cntr = 2;
												do {
													if($made_els[$arItem['ИдентификаторНСИ']]==$made_els_codes[$arLoadProductArray['CODE']]){
														//$arLoadProductArray['CODE'] = $original_code.'-'.$cntr;
														break;
													}else{
														$arLoadProductArray['CODE'] = $original_code.'-'.$cntr;
														$cntr++;
													}
													
													} while(isset($made_els_codes[$arLoadProductArray['CODE']]));
											}
										}
									}
									$el = new CIBlockElement;
									/*if($arItem['ИдентификаторНСИ']=='00081e43-15dd-47cb-b374-d3a5639525c2'){
										p($arLoadProductArray);

									}*/
									if (!isset($made_els[$arItem['ИдентификаторНСИ']])) {
										if($PRODUCT_ID = $el->Add($arLoadProductArray, false, false, false)) {
											$counts['Added']++;
											$made_els_codes[$arLoadProductArray['CODE']] = $PRODUCT_ID;
										} else {
											$counts['Error']++;
											$errors[]=$el->LAST_ERROR;									
										}

									}else{
										if($el->Update($made_els_left[$arItem['ИдентификаторНСИ']], $arLoadProductArray)){
											$counts['Updated']++;
										} else {
											$counts['Error']++;
											$errors[]=$el->LAST_ERROR;									
										}
										unset($made_els_left[$arItem['ИдентификаторНСИ']]);
									}
								/*}else{
									$counts['NoUpdated']++;
									unset($made_els_left[$arItem['ИдентификаторНСИ']]);
								}*/
							}
						}
						foreach($made_els_left as $key=>$left_good_id){
							CIBlockElement::Delete($left_good_id);
							\Bitrix\Iblock\PropertyIndex\Manager::deleteElementIndex($CATALOG_IBLOCK_ID,$left_good_id);
							$counts['Deleted']++;
						}
						p($counts);
						p($errors);
						$times['stop']=date('Y-m-d H:i:s');
						CParser::AddLog($items_parser_id,'ItemsImportFull',array('MESSAGE'=>'Полный Импорт товаров','DATA'=>json_encode(array('times'=>$times,'counts'=>$counts,'errors'=>$errors))));
						
						
						
						
						break;
						case 'items':
						$times=array(
							'start'=>date('Y-m-d H:i:s'),
							'stop'=>'');
						$errors=array();
						$counts=array(
							'Added'=>0,
							'Updated'=>0,
							'NonUpdated'=>0,
							'Deleted'=>0,
							'Error'=>0
							);
						Cmodule::IncludeModule('iblock');
						$arItems=[];
						$prop_list = [];
						$properties = CIBlockProperty::GetList(Array("sort"=>"asc", "name"=>"asc"), Array("IBLOCK_ID"=>$CATALOG_IBLOCK_ID));
						while ($prop_fields = $properties->GetNext()) {
							
							$prop_list[$prop_fields['XML_ID']] = $prop_fields;
						}

						$made_els = [];
						$made_els_codes = [];
						$made_md5=[];
						$arFilter = Array("IBLOCK_ID"=>$CATALOG_MARKI_ID);
						$arSelect = Array("ID", "NAME", "CODE", "XML_ID");
						$res = CIBlockElement::GetList(Array(), $arFilter, false, false, $arSelect);  
						while($arFields = $res->Fetch()) {  
							$marki[$arFields['XML_ID']] = $arFields['ID'];
							
						}

						$arFilter = Array("IBLOCK_ID"=>$CATALOG_IBLOCK_ID);
						$arSelect = Array("ID", "NAME", "CODE", "XML_ID",'PROPERTY_MD5DATA');
						$res = CIBlockElement::GetList(Array(), $arFilter, false, false, $arSelect);  
						while($arFields = $res->Fetch()) {  
							//p($arFields);
							$made_els[$arFields['XML_ID']] = $arFields['ID'];
							$made_els_left[$arFields['XML_ID']] = $arFields['ID'];
							$made_els_codes[$arFields['CODE']] = $arFields['ID'];
							$made_md5[$arFields['XML_ID']]=$arFields['PROPERTY_MD5DATA_VALUE'];
						}
						//p($made_md5);
						//die();
						$files=[];
						$data=CParser::GetDataFromTable($files_parser_id,array());
						while ($arItem=$data->GetNext()) {
							foreach ($arItem as $k => $v) {
								if(isJSON($arItem['~'.$k])){
									$arItem[$k]=json_decode($arItem['~'.$k],true);
								}
							}
							$files[$arItem['ИдентификаторНСИ']]=$arItem;
						}
						$arFilter = Array("IBLOCK_ID"=>$CATALOG_IBLOCK_ID);
						$arSelect = Array("ID", "NAME", "CODE", "XML_ID");
						$res = CIBlockSection::GetList(Array(), $arFilter, false, $arSelect);  
						while($arFields = $res->Fetch()) {  
							$data=CParser::GetDataFromTable($items_parser_id,array('cats'=>$arFields['XML_ID']));
							while ($arItem=$data->GetNext()) {
								unset($arItem['NUMBER']);
								unset($arItem['~NUMBER']);
								$md5summ=sha1(json_encode($arItem));
								if($md5summ!=$made_md5[$arItem['ИдентификаторНСИ']]){

									foreach ($arItem as $k => $v) {
										if(isJSON($arItem['~'.$k])){
											$arItem[$k]=json_decode($arItem['~'.$k],true);
										}
									}
									$arLoadProductArray = Array(
											"IBLOCK_SECTION_ID" => $arFields['ID'],
											"IBLOCK_ID"      => $CATALOG_IBLOCK_ID,
											"NAME"           => empty($arItem['~Наименование']) ? "[пустое_значение]" : $arItem['~Наименование'],
											"CODE"           => '',
											"XML_ID"         => $arItem['ИдентификаторНСИ'],
											"ACTIVE"         => $arItem['ПометкаУдаления'] == 0 ? "Y" : "N",
											"SORT"    => '500',
											"PROPERTY_VALUES"    => [],
									);
									$arLoadProductArray["PROPERTY_VALUES"]['CODE'] = $arItem['Код'];
									$arLoadProductArray["PROPERTY_VALUES"]['GROUP'] = $arFields['ID'];
									$arLoadProductArray["PROPERTY_VALUES"]['OWNER'] = $arItem['ВладелецНоменклатуры'];
									$arLoadProductArray["PROPERTY_VALUES"]['ID_1C'] = $arItem['nomenclatura_1c_id'];
									$arLoadProductArray["PROPERTY_VALUES"]['NAME'] = $arItem['~Наименование'];
									$arLoadProductArray["PROPERTY_VALUES"]['FULL_NAME'] = $arItem['~ПолноеНаименование'];
									$arLoadProductArray["PROPERTY_VALUES"]['EL_BRAND'] = $arItem['Марка'];
									$arLoadProductArray["PROPERTY_VALUES"]['MD5DATA'] = $md5summ;
									if (!empty($arItem['~Марка']) && isset($marki[$arItem['~Марка']])) {
										$arLoadProductArray["PROPERTY_VALUES"]['EL_BRAND'] = $marki[$arItem['~Марка']];
									}
									
									
									if (!empty($arItem['harakteristiki'])) {
										//////p$prop_list);
										foreach($arItem['harakteristiki'] as $param_code => $param_value) {
											//////p$param_value);
											$cur_param = $prop_list[$param_value['Характеристика']];
											
											if(!empty($param_value['Значение'])){
												if(isset($files[$param_value['Значение']])){
													$remote_file = $EXT_WAY.$FILES_PATH.$files[$param_value['Значение']]['ТекущаяВерсияПутьКФайлу'];
													$name=$files[$param_value['Значение']]['ТекущаяВерсияПутьКФайлу'];
													$content=file_get_contents($remote_file);
													if(!empty($content)&&!empty($name)){
														if(!file_exists($_SERVER['DOCUMENT_ROOT']."/upload/goods/")){
															mkdir($_SERVER['DOCUMENT_ROOT']."/upload/goods/");
														}
														if(!file_exists($_SERVER['DOCUMENT_ROOT']."/upload/goods/item_".$arFields['ID']."/")){
															mkdir($_SERVER['DOCUMENT_ROOT']."/upload/goods/item_".$arFields['ID']."/");
														}
														if(!file_exists($_SERVER['DOCUMENT_ROOT']."/upload/goods/item_".$arFields['ID']."/".$name)){
															file_put_contents($_SERVER['DOCUMENT_ROOT']."/upload/goods/item_".$arFields['ID']."/".$name, $content);
														}
														$param_value['Значение'] = CFile::MakeFileArray($_SERVER['DOCUMENT_ROOT']."/upload/goods/item_".$arFields['ID']."/".$name);
													}else{
														$param_value['Значение']='';
													}
												}elseif(isset($items['marki'][$param_value['Значение']])){
													$param_value['Значение'] = $items['marki'][$param_value['Значение']]['Наименование'];
												}else{
												
												}
											}
											
											if(!empty($param_value['Значение'])){
												if($param_value['Характеристика']=='c0da4812-6381-11e5-b5fc-ac162d7b6f40'){
													$arItem['item_name']=$param_value['Значение'];
												}else{
													$arLoadProductArray["PROPERTY_VALUES"][$cur_param['ID']] = $param_value['Значение'];
												}
											}
										}
									}

									$NAME = $arLoadProductArray['NAME'];
									if (!empty($arItem['item_name'])) {
										$NAME = $arItem['item_name'];
									} elseif (!empty($arItem['~ПолноеНаименование'])) {
										$NAME = $arItem['~ПолноеНаименование'];
									}
									$arLoadProductArray['NAME'] = $NAME;
									
									if (empty($arLoadProductArray['CODE'])) {
										$arLoadProductArray['CODE'] = Cutil::translit($arLoadProductArray['NAME'], "ru", array("replace_space"=>"-", "replace_other"=>"-"));
									}
									if (!isset($made_els[$arItem['ИдентификаторНСИ']])) {
										if (isset($made_els_codes[$arLoadProductArray['CODE']])) {
											$original_code = $arLoadProductArray['CODE'];
											$cntr = 2;
											do {
												$arLoadProductArray['CODE'] = $original_code.'-'.$cntr;
												$cntr++;
												} while(isset($made_els_codes[$arLoadProductArray['CODE']]));
										}	
										
									}else{
										if($made_els[$arItem['ИдентификаторНСИ']]!=$made_els_codes[$arLoadProductArray['CODE']]){
											if (isset($made_els_codes[$arLoadProductArray['CODE']])) {
												$original_code = $arLoadProductArray['CODE'];
												$cntr = 2;
												do {
													if($made_els[$arItem['ИдентификаторНСИ']]==$made_els_codes[$arLoadProductArray['CODE']]){
														//$arLoadProductArray['CODE'] = $original_code.'-'.$cntr;
														break;
													}else{
														$arLoadProductArray['CODE'] = $original_code.'-'.$cntr;
														$cntr++;
													}
													
													} while(isset($made_els_codes[$arLoadProductArray['CODE']]));
											}
										}
									}
									$el = new CIBlockElement;
									/*if($arItem['ИдентификаторНСИ']=='00081e43-15dd-47cb-b374-d3a5639525c2'){
										p($arLoadProductArray);

									}*/
									if (!isset($made_els[$arItem['ИдентификаторНСИ']])) {
										if($PRODUCT_ID = $el->Add($arLoadProductArray, false, false, false)) {
											$counts['Added']++;
											$made_els_codes[$arLoadProductArray['CODE']] = $PRODUCT_ID;
										} else {
											$counts['Error']++;
											$errors[]=$el->LAST_ERROR;									
										}

									}else{
										if($el->Update($made_els_left[$arItem['ИдентификаторНСИ']], $arLoadProductArray)){
											$counts['Updated']++;
										} else {
											$counts['Error']++;
											$errors[]=$el->LAST_ERROR;									
										}
										unset($made_els_left[$arItem['ИдентификаторНСИ']]);
									}
								}else{
									$counts['NoUpdated']++;
									unset($made_els_left[$arItem['ИдентификаторНСИ']]);
								}
							}
						}
						foreach($made_els_left as $key=>$left_good_id){
							CIBlockElement::Delete($left_good_id);
							\Bitrix\Iblock\PropertyIndex\Manager::deleteElementIndex($CATALOG_IBLOCK_ID,$left_good_id);
							$counts['Deleted']++;
						}
						$times['stop']=date('Y-m-d H:i:s');
						p($counts);
						p($errors);
						
						p($times);
						//die();
						CParser::AddLog($items_parser_id,'ItemsImportHash',array('MESSAGE'=>'Импорт товаров по Хэш сумме','DATA'=>json_encode(array('times'=>$times,'counts'=>$counts,'errors'=>$errors))));
						
						
						
						
						break;

					default:
						# code...
						break;
				}
				
				break;
			default:
				# code...
				break;
		}
		
		break;
	case 'prices':
		switch ($_REQUEST['step']) {
			case 'getdata':
				$data=CParser::GetJSON($arTypes[$_REQUEST['act']]);
				if(count($data)>0){
					CParser::AddDataTable($parser_id,$data[0]);
					CParser::InsertsToDataTable($parser_id,$data);
				}else{
					CParser::AddLog($parser_id,'PricesGetData',array('MESSAGE'=>'Ошибка получения данных от шлюза','DATA'=>''));
				}
				break;
			case 'pricebushadd':
				CModule::IncludeModule('iblock');
				$PriceBushes=array();
				$arSelect = Array("ID", "NAME", "DATE_ACTIVE_FROM","XML_ID");
				$arFilter = Array("IBLOCK_ID"=>$CATALOG_PRICEBUSH_ID, "ACTIVE_DATE"=>"Y", "ACTIVE"=>"Y","XML_ID"=>$XML_ID);
				$res = CIBlockElement::GetList(Array(), $arFilter, false, false, $arSelect);
				while($arFields = $res->GetNext())
				{
				 	$PriceBushes[]=$arFields;
				}
				$arPriceTypes=array();
				$dbPriceType = CCatalogGroup::GetList(array("SORT" => "ASC"));
				while ($arPriceType = $dbPriceType->Fetch())
				{
				    $arPriceTypes[$arPriceType['XML_ID']]=$arPriceType;
				}
				
				foreach ($PriceBushes as $key => $value) {
					$n=$key+1;
					$arFields_NODISC = array(
					   "NAME"=>$value['XML_ID']."_NODISCOUNT",
					   "XML_ID"=>$value['XML_ID']."_NODISCOUNT",
					   "SORT" => $n."01",
					   "USER_GROUP" => array(2, 1), 
					   "USER_GROUP_BUY" => array(2,1), 
					   "USER_LANG" => array(
					      "ru" => $value['NAME']." без скидки",
					      "en" => $value['NAME']." без скидки",
					      )
					);

					$arFields = array(
					   "NAME"=>$value['XML_ID'],
					   "XML_ID"=>$value['XML_ID'],
					   "SORT" => $n."00",
					   "USER_GROUP" => array(2, 1),   
					   "USER_GROUP_BUY" => array(2,1),  
					   "USER_LANG" => array(
					      "ru" => $value['NAME'],
					      "en" => $value['NAME'],
					      )
					);


					if(is_array($arPriceTypes[$value['XML_ID']])){
						CCatalogGroup::Update($arPriceTypes[$value['XML_ID']]['ID'],$arFields);
						CCatalogGroup::Update($arPriceTypes[$value['XML_ID'].'_NODISCOUNT']['ID'],$arFields_NODISC);
						unset($arPriceTypes[$value['XML_ID']]);
						unset($arPriceTypes[$value['XML_ID']."_NODISCOUNT"]);
					}else{
						$ID_NODISC = CCatalogGroup::Add($arFields_NODISC);
						if ($ID<=0)
						   echo "Ошибка добавления типа цены";
						$ID = CCatalogGroup::Add($arFields);
						if ($ID<=0)
						   echo "Ошибка добавления типа цены";
					}
				}
				foreach ($arPriceTypes as $k => $v) {
					if($v['SORT']>=100){
						CCatalogGroup::Delete($v['ID']);
					}
				}
				break;
			case 'import':
				$errors=array();
				$times=array('start'=>date('Y-m-d H:i:s'),
					'stop'=>'');
				$counts=array(
					'Added'=>0,
					'Updated'=>0,
					'Deleted'=>0,
					'Error'=>0,
					'Products'=>0
					);
				$CATALOG_IBLOCK_ID=8;
				$arPriceTypes=CParser::GetPriceTypes();
				$arProducts=array();
				$arSelect = Array("ID","NAME","XML_ID");
				$arFilter = Array("IBLOCK_ID"=>IntVal($CATALOG_IBLOCK_ID), "ACTIVE_DATE"=>"Y", "ACTIVE"=>"Y");
				$res = CIBlockElement::GetList(Array(), $arFilter, false, false, $arSelect);
				while($arFields = $res->GetNext())
				{
					$wasUpdated=false;
					
					$arFields['PRICES']=array();
					$arCatalog = array(
							"ID" => $arFields['ID'], 
							"QUANTITY" => 1000,
						);
					$data=CParser::GetDataFromTable($parser_id,array('WebSite_ID'=>$WEBSITE_ID,'Goods_ID'=>$arFields['XML_ID']));
					while($arPrice=$data->GetNext()){
						$arFields['PRICES'][$arPrice['PriceBush_ID']]=$arPrice;
					}
					/*p($arFields);*/
					if(!empty($arFields['PRICES'])){
						$counts['Products']++;
						//echo $arFields['NAME'].'-'.$arFields['XML_ID']."<br>";
						if(CCatalogProduct::Add($arCatalog)) {
							

							$resp = CPrice::GetList(
								array(
									"CATALOG_GROUP_ID" => "ASC",
								),
								array(
									"PRODUCT_ID" => $arFields['ID'],
								)
							);
							$arPrices = [];
							while($arPrice = $resp->Fetch()) {
								$arPrices[$arPrice['CATALOG_GROUP_ID']] = [
									'ID' => $arPrice['ID'],
									'PRICE' => $arPrice['PRICE'],
								];
							}
							
							foreach ($arFields['PRICES'] as $k => $v) {
								$prices[0] = floatval($v['Price']);
								$prices[1] = floatval($v['PriceOLD']);
								
								if (is_array($arPriceTypes[$k])) {
									$price_code = $k;
									$price_code_NODISCOUNT = $k."_NODISCOUNT";
									
									$arPriceFields = Array(
										"PRODUCT_ID" => $arFields['ID'],
										"CATALOG_GROUP_ID" => $arPriceTypes[$price_code]['ID'],
										"PRICE" => $prices[0],
										"CURRENCY" => "RUB",
									);
									
									$arPriceFields2 = Array(
										"PRODUCT_ID" => $arFields['ID'],
										"CATALOG_GROUP_ID" => $arPriceTypes[$price_code_NODISCOUNT]['ID'],
										"PRICE" => $prices[1],
										"CURRENCY" => "RUB",
									);
									$update_arr = [
										[$arPriceTypes[$price_code]['ID'], $arPriceFields],
										[$arPriceTypes[$price_code_NODISCOUNT]['ID'], $arPriceFields2],
									];
									//p($update_arr);
									foreach($update_arr as $update_data) {
										if (isset($arPrices[$update_data[0]])) {
											if (floatval($arPrices[$update_data[0]]['PRICE']) != $update_data[1]['PRICE']) {
												if ($update_data[1]['PRICE'] > 0) {
													if ($ID = CPrice::Update($arPrices[$update_data[0]]['ID'], $update_data[1])) {
														$counts['Updated']++;
														$wasUpdated = true;
													} else {
														$counts['Error']++;
														$errors[]='Ошибка обновления: '.$APPLICATION->GetException();
													}
												} else {
													if ($ID = CPrice::Delete($arPrices[$update_data[0]]['ID'])) {
														$counts['Deleted']++;
														$wasUpdated = true;
													} else {
														$counts['Error']++;
														$errors[]='Ошибка удаления: '.$APPLICATION->GetException();
													}
												}
											}
											unset($arPrices[$update_data[0]]);
										} else {
											if ($update_data[1]['PRICE'] > 0) {
												//p($update_data[1]);
												if ($ID = CPrice::Add($update_data[1])) {
													$wasUpdated = true;
													$counts['Added']++;
													//p('New ID: '.$ID);
												} else {
											
													$counts['Error']++;
													$errors[]='Ошибка добавления: '.$APPLICATION->GetException();
												}
											}
										}
									}
								}
							}
						}else {
							$counts['Error']++;
							$errors[]='Ошибка добавления параметров к элементу каталога '.$arFields['ID'];
						}
					}else {
						if(CCatalogProduct::Delete($arFields['ID'])) {
							$wasUpdated = true;
							
							// p("Удалили параметры товара к элементу каталога ".$PRODUCT_ID.'');
						} else {
							$counts['Error']++;
							$errors[]='Ошибка удаления параметров к элементу каталога '.$arFields['ID'];
						}
					}

					if ($wasUpdated) {
						\Bitrix\Iblock\PropertyIndex\Manager::updateElementIndex($CATALOG_IBLOCK_ID, $arFields['ID']);
					}
					

				}
				BXClearCache(true, "/s1/bitrix/catalog.element/");
				BXClearCache(true, "/s1/bitrix/catalog.section/");
				BXClearCache(true, "/s1/bitrix/catalog.section.list/");
				BXClearCache(true, "/s1/bitrix/catalog.smart.filter/");
				$times['stop']=date('Y-m-d H:i:s');
				p($counts);
				p($errors);
				
				CParser::AddLog($parser_id,'ItemsPriceImport',array('MESSAGE'=>'Импорт цен','DATA'=>json_encode(array('times'=>$times,'counts'=>$counts,'errors'=>$errors))));
				/*p($arPriceTypes);*/
				
				break;			
			default:
				# code...
				break;
		}
		break;
	case 'time2buy':
		switch ($_REQUEST['step']) {
			case 'getdata':
				$data=CParser::GetJSON($arTypes[$_REQUEST['act']]);
				//p($data);
				if(count($data)>0){
					CParser::AddDataTable($parser_id,$data[0]);
					CParser::InsertsToDataTable($parser_id,$data);
				}else{
					CParser::AddLog($parser_id,'Time2BuyGetData',array('MESSAGE'=>'Ошибка получения данных от шлюза','DATA'=>''));
				}
				break;
			case 'printdata':
				$pars=$parser_id;
				
				$data=CParser::GetDataFromTable($pars,array(),array());
				$arParams=array();
				while ($arParam=$data->GetNext()) {
					foreach ($arParam as $k => $v) {
						if(isJSON($arParam['~'.$k])){
							$arParam[$k]=json_decode($arParam['~'.$k],true);
						}

					}
					$arParams[]=$arParam;
				}

				p($arParams);
				
				break;
			case 'import':
				$times=array(
							'start'=>date('Y-m-d H:i:s'),
							'stop'=>'');
				CModule::IncludeModule('iblock');
				$errors=array();
				$counts=array(
					'Added'=>0,
					'Updated'=>0,
					'Deleted'=>0,
					'Error'=>0
					);
				$arPriceBushes=[];
				$arSelect = Array("ID","XML_ID");
				$arFilter = Array("IBLOCK_ID"=>$CATALOG_PRICEBUSH_ID);
				$res = CIBlockElement::GetList(Array(), $arFilter, false, false, $arSelect);
				while($arFields = $res->GetNext())
				{
				 	$arPriceBushes[$arFields['XML_ID']]=$arFields['ID'];
				}
				$arPriceTypes=CParser::GetPriceTypes();
				$arItems=[];
				$arSelect = Array("ID","XML_ID",'NAME');
				$arFilter = Array("IBLOCK_ID"=>$CATALOG_IBLOCK_ID);
				$res = CIBlockElement::GetList(Array(), $arFilter, false, false, $arSelect);
				while($arFields = $res->GetNext())
				{
				 	$arItems[$arFields['XML_ID']]['ID']=$arFields['ID'];
				 	$arItems[$arFields['XML_ID']]['NAME']=$arFields['NAME'];
				}
				$arFilter = Array("IBLOCK_ID"=>$CATALOG_AD_ID,'SECTION_ID'=>$CATALOG_TIME2BUY_SECTION);
				$arSelect = Array("ID", "NAME", "CODE", "XML_ID");
				$res = CIBlockElement::GetList(Array(), $arFilter, false, false, $arSelect);  
				while($arFields = $res->Fetch()) {  
					$made_els[$arFields['XML_ID']] = $arFields['ID'];
					$made_els_left[$arFields['XML_ID']] = $arFields['ID'];
					$made_els_codes[$arFields['CODE']] = $arFields['ID'];
				}
				$arTovars=array();
				$data=CParser::GetDataFromTable($parser_id,array('WebSite_ID'=>$WEBSITE_ID),array());
				$arParams=array();
				while ($arItem=$data->GetNext()) {
					foreach ($arItem as $k => $v) {
						if(isJSON($arItem['~'.$k])){
							$arItem[$k]=json_decode($arItem['~'.$k],true);
							unset($arItem['~'.$k]);
						}

					}
					$arTovars[$arItem['Goods_ID']][md5($arItem['DataStart'].$arItem['DataEnd'])]['DATE_FROM']=date('d.m.Y H:i:s',strtotime($arItem['DataStart']));
					$arTovars[$arItem['Goods_ID']][md5($arItem['DataStart'].$arItem['DataEnd'])]['DATE_TO']=date('d.m.Y H:i:s',strtotime($arItem['DataEnd']));
					$arTovars[$arItem['Goods_ID']][md5($arItem['DataStart'].$arItem['DataEnd'])]['PRICES'][$arItem['PriceBush_ID']]=array('Price'=>$arItem['PriceNew'],'PriceOLD'=>$arItem['PriceOld']);
				}
				foreach ($arTovars as $k => $v) {
					if($arItems[$k]['ID']!=''){
						foreach ($v as $k2 => $v2) {
							$xml_id=md5($k.'-'.$k2);
							$arLoadProductArray = Array(
									"IBLOCK_ID"      => $CATALOG_AD_ID,
									"IBLOCK_SECTION_ID"=>$CATALOG_TIME2BUY_SECTION,
									"NAME"           => empty($arItems[$k]['NAME']) ? "[пустое_значение]" : $arItems[$k]['NAME'],
									"XML_ID"         => $xml_id,
									"ACTIVE"         =>  "Y",
									"DATE_ACTIVE_FROM"=>$v2['DATE_FROM'],
									"DATE_ACTIVE_TO"=>$v2['DATE_TO']
							);
							$arLoadProductArray['PROPERTY_VALUES']['ITEM']=$arItems[$k]['ID'];
							foreach ($v2['PRICES'] as $k3 => $v3) {
								if($arPriceBushes[$k3]!=''){
									$arLoadProductArray['PROPERTY_VALUES']['geo_networks']=$arPriceBushes[$k3];
								}
								
							}
							//p($arLoadProductArray);
							$el = new CIBlockElement;
							if (!isset($made_els[$xml_id])) {
								if($PRODUCT_ID = $el->Add($arLoadProductArray, false, false, false)) {
									$counts['Added']++;
								} else {
									$counts['Error']++;
									$errors[]=$el->LAST_ERROR;									
								}

							}else{
								if($el->Update($made_els_left[$xml_id], $arLoadProductArray)){
									$counts['Updated']++;
								} else {
									$counts['Error']++;
									$errors[]=$el->LAST_ERROR;									
								}
								unset($made_els_left[$xml_id]);
							}
							$time=time();
												

							if(strtotime($v2['DATE_FROM'])<$time&&strtotime($v2['DATE_TO'])>$time){
								
								
								$resp = CPrice::GetList(
									array(
										"CATALOG_GROUP_ID" => "ASC",
									),
									array(
										"PRODUCT_ID" => $arItems[$k]['ID'],
									)
								);
								$arPrices = [];
								while($arPrice = $resp->Fetch()) {
									$arPrices[$arPrice['CATALOG_GROUP_ID']] = [
										'ID' => $arPrice['ID'],
										'PRICE' => $arPrice['PRICE'],
									];
								}
								
								foreach ($v2['PRICES'] as $k4 => $v4) {
									$prices[0] = floatval($v4['Price']);
									$prices[1] = floatval($v4['PriceOLD']);
									
									if (is_array($arPriceTypes[$k4])) {
										$price_code = $k4;
										$price_code_NODISCOUNT = $k4."_NODISCOUNT";
										
										$arPriceFields = Array(
											"PRODUCT_ID" => $arItems[$k]['ID'],
											"CATALOG_GROUP_ID" => $arPriceTypes[$price_code]['ID'],
											"PRICE" => $prices[0],
											"CURRENCY" => "RUB",
										);
										
										$arPriceFields2 = Array(
											"PRODUCT_ID" => $arItems[$k]['ID'],
											"CATALOG_GROUP_ID" => $arPriceTypes[$price_code_NODISCOUNT]['ID'],
											"PRICE" => $prices[1],
											"CURRENCY" => "RUB",
										);
										$update_arr = [
											[$arPriceTypes[$price_code]['ID'], $arPriceFields],
											[$arPriceTypes[$price_code_NODISCOUNT]['ID'], $arPriceFields2],
										];
										//p($update_arr);
										foreach($update_arr as $update_data) {
											if (isset($arPrices[$update_data[0]])) {
												if (floatval($arPrices[$update_data[0]]['PRICE']) != $update_data[1]['PRICE']) {
													if ($update_data[1]['PRICE'] > 0) {
														if ($ID = CPrice::Update($arPrices[$update_data[0]]['ID'], $update_data[1])) {
															$counts['Updated']++;
															$wasUpdated = true;
														} else {
															$counts['Error']++;
															$errors[]='Ошибка обновления: '.$APPLICATION->GetException();
														}
													} else {
														if ($ID = CPrice::Delete($arPrices[$update_data[0]]['ID'])) {
															$counts['Deleted']++;
															$wasUpdated = true;
														} else {
															$counts['Error']++;
															$errors[]='Ошибка удаления: '.$APPLICATION->GetException();
														}
													}
												}
												unset($arPrices[$update_data[0]]);
											} else {
												if ($update_data[1]['PRICE'] > 0) {
													//p($update_data[1]);
													if ($ID = CPrice::Add($update_data[1])) {
														$wasUpdated = true;
														$counts['Added']++;
														//p('New ID: '.$ID);
													} else {
												
														$counts['Error']++;
														$errors[]='Ошибка добавления: '.$APPLICATION->GetException();
													}
												}
											}
										}
									}
								}
							}
						}	
					}else{
						$count['Error']++;
						$errors[]='Товара '.$k.' нет в каталоге';
					}
				}
				foreach($made_els_left as $key=>$left_good_id){
					CIBlockElement::Delete($left_good_id);
					$counts['Deleted']++;
				}
				$times['stop']=date('Y-m-d H:i:s');
				p($counts);
				p($errors);
				p($times);
				
				CParser::AddLog($parser_id,'Time2BuyPriceImport',array('MESSAGE'=>'Импорт цен для Успей купить','DATA'=>json_encode(array('times'=>$times,'counts'=>$counts,'errors'=>$errors))));
				break;
			}
		break;	
	case 'parsers':
		switch ($_REQUEST['step']) {
			case 'add':
				if($_REQUEST['send']!=''){
					CParser::Add($_REQUEST['CODE'],array('NAME'=>$_REQUEST['NAME'],'URL'=>$_REQUEST['URL'],'STATUS'=>0));
				}
				?>
				<form method="post">
					Код:<input type="text" name="CODE"><br>
					Название:<input type="text" name="NAME"><br>
					URL:<input type="text" name="URL"><br>
					<input type="submit" name="send" value="Добавить">
				</form>
				<?
				break;
			case 'install':
				CParser::Install();		
				break;
			case 'list':
				$res=CParser::GetList();
				while ($arParser=$res->GetNext()) {
					p($arParser);
				}
				break;
			case 'logs':
				if($_REQUEST['action']){
					$filter=array('ACTION'=>$_REQUEST['action']);
				}else{
					$filter=array();	
				}
				$res=CParser::GetLogs($filter,array(0,40),array('ID'=>'DESC'));
				while ($arParser=$res->GetNext()) {
					$arParser['DATA']=json_decode($arParser['~DATA'],true);
					p($arParser);
				}
				break;
			default:
				
				break;
		}
		
		break;
	case 'getdata':
		$data=CParser::GetJSON($arTypes[$_REQUEST['step']]);
		p($data);
		break;
	default:
		require_once($_SERVER['DOCUMENT_ROOT'].'/import/templates/'.$_REQUEST['act'].'.php');
		break;
}
