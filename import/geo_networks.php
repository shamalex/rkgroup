<?
include $_SERVER['DOCUMENT_ROOT'].'/bitrix/modules/main/include/prolog_before.php';
// require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Импорт цен и остатков");

CModule::IncludeModule('iblock');
Cmodule::IncludeModule('catalog');
@ini_set('memory_limit', '4096M');
@set_time_limit(595);

$CATALOG_IBLOCK_ID = 8;
$BRANDS_IBLOCK_ID = 9;
$GEO_IBLOCK_ID = 15;
$GEO_NETWORKS_IBLOCK_ID = 20;
$EXT_WAY = 'http://rusklimat.webway.ru';

function get_web_page( $url ) {
	$res = array();
	$options = array( 
		CURLOPT_RETURNTRANSFER => true,	 // return web page 
		CURLOPT_HEADER		 => false,	// do not return headers 
		CURLOPT_FOLLOWLOCATION => true,	 // follow redirects 
		// CURLOPT_USERAGENT	  => "spider", // who am i 
		CURLOPT_USERAGENT	  => "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/45.0.2454.85 Safari/537.36", // who am i 
		CURLOPT_AUTOREFERER	=> true,	 // set referer on redirect 
		CURLOPT_CONNECTTIMEOUT => 120,	  // timeout on connect 
		CURLOPT_TIMEOUT		=> 120,	  // timeout on response 
		CURLOPT_MAXREDIRS	  => 10,	   // stop after 10 redirects 
	); 
	$ch	  = curl_init( $url ); 
	curl_setopt_array( $ch, $options ); 
	$content = curl_exec( $ch ); 
	$err	 = curl_errno( $ch ); 
	$errmsg  = curl_error( $ch ); 
	$header  = curl_getinfo( $ch ); 
	curl_close( $ch ); 

	$res['content'] = $content;	 
	$res['url'] = $header['url'];
	return $res; 
}


$geo_get_url = $EXT_WAY.'/goods/geo_networks';


$geo = [];

if (!file_exists('./data_geo_networks.php')) {
	$geo = get_web_page($geo_get_url);
	$geo = json_decode($geo['content'], true);
	// file_put_contents('./data_geo_networks.php', '<? $geo = '.var_export($geo, 1).';');
} else {
	include('./data_geo_networks.php');
}
p($geo);
die();

if (!empty($geo)) {
	
	$geo_items = [];

	$arFilter = Array("IBLOCK_ID"=>$GEO_NETWORKS_IBLOCK_ID);
	$arSelect = false;
	$res = CIBlockElement::GetList(Array(), $arFilter, false, false, $arSelect);  
	while($obFields = $res->GetNextElement()) {
		
		$arFields = $obFields->GetFields();
		$arFields['PROPERTY_VALUES'] = $obFields->GetProperties();
		
		$geo_items[$arFields["XML_ID"]] = $arFields;
	}
	
	foreach($geo as $id => $geo_item) {
		
		$arLoadProductArray = Array(
			"IBLOCK_SECTION_ID" => false,
			"IBLOCK_ID"      => $GEO_NETWORKS_IBLOCK_ID,
			"NAME"           => $geo_item['name'],
			"CODE"           => $geo_item['code'],
			"XML_ID"         => $geo_item['site_id'],
			"ACTIVE"         => "Y",
			"SORT"    => '500',
			"PROPERTY_VALUES"    => [
				"site_id" => $geo_item['site_id'],
				"city_id" => $geo_item['city_id'],
				"db_name" => $geo_item['db_name'],
				"domain_name" => $geo_item['domain_name'],
				"type_id" => $geo_item['type_id'],
				"parent_id" => $geo_item['parent_id'],
				"show_full_cat" => ($geo_item['show_full_cat'] == 'true' ? 1 : 0),
				"status" => ($geo_item['status'] == 'active' ? 1 : 0),
			],
		);
		
		if (isset($geo_items[$arLoadProductArray['XML_ID']])) {
			
			$geo_item = $geo_items[$arLoadProductArray['XML_ID']];
			
			foreach($arLoadProductArray as $key => $val) {
				
				if ($key == 'PROPERTY_VALUES') {
					foreach($val as $prop_key => $prop_val) {
						if ($prop_val == $geo_item['PROPERTY_VALUES'][$prop_key]['~VALUE']) {
							unset($arLoadProductArray['PROPERTY_VALUES'][$prop_key]);
						}
					}
					if (empty($arLoadProductArray['PROPERTY_VALUES'])) {
						unset($arLoadProductArray['PROPERTY_VALUES']);
					}
				} else {
					if ($val == $geo_item['~'.$key]) {
						unset($arLoadProductArray[$key]);
					}
				}
				
			}
			
			if (!empty($arLoadProductArray)) {
				if (!empty($arLoadProductArray['PROPERTY_VALUES'])) {
					p('update props');
					p($arLoadProductArray['PROPERTY_VALUES']);
					CIBlockElement::SetPropertyValuesEx($geo_item['ID'], $GEO_NETWORKS_IBLOCK_ID, $arLoadProductArray['PROPERTY_VALUES']);
					unset($arLoadProductArray['PROPERTY_VALUES']);
				}
				if (!empty($arLoadProductArray)) {
					p('update data');
					p($arLoadProductArray);
					$el = new CIBlockElement;
					$res = $el->Update($geo_item['ID'], $arLoadProductArray);
				}
			}
			
			unset($geo_items[$geo_item['XML_ID']]);
			
		} else {
			
			$el = new CIBlockElement;
			if($PRODUCT_ID = $el->Add($arLoadProductArray, false, false, false)) {
				p("New ID: ".$PRODUCT_ID);
				// $geo_items[$arLoadProductArray['XML_ID']] = $PRODUCT_ID;
			} else {
				echo '<div class="make_error">';
				p("Error add: ".$el->LAST_ERROR);
				echo '</div>';
			}
			// p($arLoadProductArray);
			// p('-----------------------------------------------------------------------------------');
			
		}
		
	}
	
	if (!empty($geo_items)) {
		foreach($geo_items as $geo_item) {
			p('delete');
			p($geo_item);
			CIBlockElement::Delete($geo_item['ID']);
		}
	}

	// p(count($geo_items));
	// p($geo_items);
	// p($geo_items[0]);
	// for ($i=0;$i<100;$i++) {
		// p($geo_items[$i]);
	// }
	// die();
	
}

?>


<?
//require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");
?>