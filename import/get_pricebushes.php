<?
include $_SERVER['DOCUMENT_ROOT'].'/bitrix/modules/main/include/prolog_before.php';
// require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("������ ������");

CModule::IncludeModule('iblock');
@ini_set('memory_limit', '4096M');
@set_time_limit(595);

$PRICEBUSHES_IBLOCK_ID = 20;
$EXT_WAY = 'http://rusklimat.webway.ru';

function get_web_page( $url ) {
	$res = array();
	$options = array( 
		CURLOPT_RETURNTRANSFER => true,	 // return web page 
		CURLOPT_HEADER		 => false,	// do not return headers 
		CURLOPT_FOLLOWLOCATION => true,	 // follow redirects 
		// CURLOPT_USERAGENT	  => "spider", // who am i 
		CURLOPT_USERAGENT	  => "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/45.0.2454.85 Safari/537.36", // who am i 
		CURLOPT_AUTOREFERER	=> true,	 // set referer on redirect 
		CURLOPT_CONNECTTIMEOUT => 120,	  // timeout on connect 
		CURLOPT_TIMEOUT		=> 120,	  // timeout on response 
		CURLOPT_MAXREDIRS	  => 10,	   // stop after 10 redirects 
	); 
	$ch	  = curl_init( $url ); 
	curl_setopt_array( $ch, $options ); 
	$content = curl_exec( $ch ); 
	$err	 = curl_errno( $ch ); 
	$errmsg  = curl_error( $ch ); 
	$header  = curl_getinfo( $ch ); 
	curl_close( $ch ); 

	$res['content'] = $content;	 
	$res['url'] = $header['url'];
	return $res; 
}


$geo_get_url = $EXT_WAY.'/api/get/pricebushes';


$pricebushes = [];
$ib_pricebushes = [];

if (!file_exists('./data_pricebushes.php')) {
	$pricebushes = get_web_page($geo_get_url);
	//p($warehouses);
	$pricebushes = json_decode($pricebushes['content'], true);
	//file_put_contents('./data_pricebushes.php', '<? $pricebushes = '.var_export($pricebushes, 1).';');
} else {
	include('./data_pricebushes.php');
}

if(!empty($pricebushes)){
	$pricebushesOb=CIBlockElement::GetList(array(),array("IBLOCK_ID"=>$PRICEBUSHES_IBLOCK_ID),false,false,array("ID","NAME","XML_ID"));
	while($pricebushesAr=$pricebushesOb->GetNext()){
		$ib_pricebushes[$pricebushesAr["XML_ID"]]=$pricebushesAr;
	}

	$el = new CIBlockElement();
	$res_id = [];
	foreach($pricebushes as $pricebushe){
		$arFieldsPricebushes=array(
			"IBLOCK_ID" => $PRICEBUSHES_IBLOCK_ID,
			"XML_ID" => $pricebushe["ID"],
			"NAME" => $pricebushe["Name"],
			"CODE" => Cutil::translit($pricebushe["Name"],"ru",array("replace_space"=>"-","replace_other"=>"-")),
		);
		
		$res=false;
		if(isset($ib_pricebushes[$pricebushe["ID"]])){
			$res=$el->Update($ib_pricebushes[$pricebushe["ID"]]["ID"], $arFieldsPricebushes);
			if($res){
				p('Update data');
				p($arFieldsPricebushes);
				
				$res_id[] = $arFieldsPricebushes["XML_ID"];
			}else{
				p("Error update: ".$el->LAST_ERROR);
				p($arFieldsPricebushes);
			}
		}else{
			$res=$el->Add($arFieldsPricebushes);
			if($res){
				p('Add data, new id: '.$res);
				p($arFieldsPricebushes);
				
				$res_id[] = $arFieldsPricebushes["XML_ID"];
			}else{
				p("Error add: ".$el->LAST_ERROR);
				p($arFieldsPricebushes);
			}
		}
	}
	
	foreach($ib_pricebushes as $xml_id=>$ib_pricebushe){
		if(!in_array($xml_id,$res_id)){
			p('Delete');
			p($ib_pricebushe);
			CIBlockElement::Delete($ib_pricebushe['ID']);
		}
	}
	
	// ���� ���
	CModule::IncludeModule('iblock');
    $PriceBushes=array();
    $arSelect = Array("ID", "NAME", "DATE_ACTIVE_FROM","XML_ID");
    $arFilter = Array("IBLOCK_ID"=>IntVal(20), "ACTIVE_DATE"=>"Y", "ACTIVE"=>"Y","XML_ID"=>$XML_ID);
    $res = CIBlockElement::GetList(Array(), $arFilter, false, false, $arSelect);
    while($arFields = $res->GetNext())
    {
		$PriceBushes[]=$arFields;
    }
    $arPriceTypes=array();
    $dbPriceType = CCatalogGroup::GetList(array("SORT" => "ASC"));
    while ($arPriceType = $dbPriceType->Fetch())
    {
        $arPriceTypes[$arPriceType['XML_ID']]=$arPriceType;
    }
    
    foreach ($PriceBushes as $key => $value) {
		$n=$key+1;
		$arFields_NODISC = array(
			"NAME"=>$value['XML_ID']."_NODISCOUNT",
			"XML_ID"=>$value['XML_ID']."_NODISCOUNT",
			"SORT" => $n."01",
			"USER_GROUP" => array(2, 1), 
			"USER_GROUP_BUY" => array(2,1), 
			"USER_LANG" => array(
				"ru" => $value['NAME']." ��� ������",
				"en" => $value['NAME']." ��� ������",
			)
		);

		$arFields = array(
			"NAME"=>$value['XML_ID'],
			"XML_ID"=>$value['XML_ID'],
			"SORT" => $n."00",
			"USER_GROUP" => array(2, 1),   
			"USER_GROUP_BUY" => array(2,1),  
			"USER_LANG" => array(
				"ru" => $value['NAME'],
				"en" => $value['NAME'],
			)
		);


		if(is_array($arPriceTypes[$value['XML_ID']])){
			CCatalogGroup::Update($arPriceTypes[$value['XML_ID']]['ID'],$arFields);
			CCatalogGroup::Update($arPriceTypes[$value['XML_ID'].'_NODISCOUNT']['ID'],$arFields_NODISC);
			unset($arPriceTypes[$value['XML_ID']]);
			unset($arPriceTypes[$value['XML_ID']."_NODISCOUNT"]);
		}else{
			$ID_NODISC = CCatalogGroup::Add($arFields_NODISC);
			if ($ID<=0)
				echo "������ ���������� ���� ����";
			$ID = CCatalogGroup::Add($arFields);
			if ($ID<=0)
				echo "������ ���������� ���� ����";
		}
    }
    foreach ($arPriceTypes as $k => $v) {
		if($v['SORT']>=100){
			CCatalogGroup::Delete($v['ID']);
		}
    }
}	
?>