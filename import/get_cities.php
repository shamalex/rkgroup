<?
include $_SERVER['DOCUMENT_ROOT'].'/bitrix/modules/main/include/prolog_before.php';
// require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Импорт складов");

CModule::IncludeModule('iblock');
CModule::IncludeModule('sale');
@ini_set('memory_limit', '4096M');
@set_time_limit(595);

$CITIES_IBLOCK_ID = 15;
$WAREHOUSES_IBLOCK_ID = 38;
$DELIVERY_TARIF_INTERCITIES_IBLOCK_ID = 40;
$properties = CIBlockProperty::GetList(Array("sort"=>"asc", "name"=>"asc"), Array("ACTIVE"=>"Y", "IBLOCK_ID"=>$CITIES_IBLOCK_ID, "CODE"=>"region_list"));
if ($prop_fields = $properties->GetNext()){
	$PROP_REGION_LIST_ID = $prop_fields["ID"];
}else{
	$PROP_REGION_LIST_ID = 0;
}

$EXT_WAY = 'http://rusklimat.webway.ru';

function get_web_page( $url ) {
	$res = array();
	$options = array( 
		CURLOPT_RETURNTRANSFER => true,	 // return web page 
		CURLOPT_HEADER		 => false,	// do not return headers 
		CURLOPT_FOLLOWLOCATION => true,	 // follow redirects 
		// CURLOPT_USERAGENT	  => "spider", // who am i 
		CURLOPT_USERAGENT	  => "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/45.0.2454.85 Safari/537.36", // who am i 
		CURLOPT_AUTOREFERER	=> true,	 // set referer on redirect 
		CURLOPT_CONNECTTIMEOUT => 120,	  // timeout on connect 
		CURLOPT_TIMEOUT		=> 120,	  // timeout on response 
		CURLOPT_MAXREDIRS	  => 10,	   // stop after 10 redirects 
	); 
	$ch	  = curl_init( $url ); 
	curl_setopt_array( $ch, $options ); 
	$content = curl_exec( $ch ); 
	$err	 = curl_errno( $ch ); 
	$errmsg  = curl_error( $ch ); 
	$header  = curl_getinfo( $ch ); 
	curl_close( $ch ); 

	$res['content'] = $content;	 
	$res['url'] = $header['url'];
	return $res; 
}


$geo_get_url = $EXT_WAY.'/api/get/geo';


$geo = [];
$ib_geo = [];
$ib_region = [];
$ib_warehouses = [];
$ib_deliveryTariffIntercities = [];
$sale_payment = [];

if (!file_exists('./data_geo.php')) {
	$geo = get_web_page($geo_get_url);
	//p($geo);
	$geo = json_decode($geo['content'], true);
	//file_put_contents('./data_geo.php', '<? $geo = '.var_export($geo, 1).';');
} else {
	include('./data_geo.php');
}

if(!empty($geo)){
	$property_enums = CIBlockPropertyEnum::GetList(Array("DEF"=>"DESC", "SORT"=>"ASC"), Array("IBLOCK_ID"=>$CITIES_IBLOCK_ID, "CODE"=>"region_list"));
	while($enum_fields = $property_enums->GetNext())
	{
		$ib_region[$enum_fields["XML_ID"]]=$enum_fields;
	}
	
	$geoOb=CIBlockElement::GetList(array(),array("IBLOCK_ID"=>$CITIES_IBLOCK_ID),false,false,array("ID","NAME","XML_ID"));
	while($geoAr=$geoOb->GetNext()){
		$ib_geo[$geoAr["XML_ID"]]=$geoAr;
	}

	$warehousesOb=CIBlockElement::GetList(array(),array("IBLOCK_ID"=>$WAREHOUSES_IBLOCK_ID),false,false,array("ID","NAME","XML_ID","PROPERTY_ID","PROPERTY_ShiftPickup","PROPERTY_ShiftPickupFLL","PROPERTY_ShiftPickupRRC","PROPERTY_ShiftPickupFRC","PROPERTY_TimeOrders01","PROPERTY_TimeOrders02","PROPERTY_TimeOrders03","PROPERTY_consists"));
	while($warehousesAr=$warehousesOb->GetNext()){
		$ib_warehouses[$warehousesAr["XML_ID"]]=$warehousesAr;
	}
	
	$deliveryTariffIntercitiesOb=CIBlockElement::GetList(array(),array("IBLOCK_ID"=>$DELIVERY_TARIF_INTERCITIES_IBLOCK_ID),false,false,array("ID","NAME","XML_ID","PROPERTY_PriorityIntercityDelivery","PROPERTY_PriorityWarehouse","PROPERTY_Warehouse_ID","PROPERTY_AmountDays","PROPERTY_Summ"));
	while($deliveryTariffIntercitiesAr=$deliveryTariffIntercitiesOb->GetNext()){
		$ib_deliveryTariffIntercities[$deliveryTariffIntercitiesAr["XML_ID"]]=$deliveryTariffIntercitiesAr;
	}	
	
	$dbPtype = CSalePaySystem::GetList(Array(), Array());
	while ($arPtype = $dbPtype->Fetch())
	{
		$sale_payment[$arPtype["NAME"]] = $arPtype;
	}
	
	$el = new CIBlockElement();
	$ibpenum = new CIBlockPropertyEnum;
	$ps = new CSalePaySystem;
	$res_id = [];
	$tarif_ic_id = [];
	foreach($geo as $item){
		if($item["RegionFlag"]=="1"){
			if(!$ib_region[$item["ID"]]){
				$regionFeilds=array(
					"PROPERTY_ID" => $PROP_REGION_LIST_ID,
					"VALUE" => $item["Name"],
					"XML_ID" => $item["ID"]
				);

				if($PropID = $ibpenum->Add($regionFeilds)){
					$regionFeilds["ID"]=$PropID;
					$ib_region[$regionFeilds["XML_ID"]] = $regionFeilds;
				}
			}
		}else{		
			$payments = [];
			foreach($item["availablePaymentTips"] as $Ptype){
				if(!isset($sale_payment[$Ptype["PaymentTip"]])){
					if($psID = $ps->Add(array("CURRENCY" => "RUB", "NAME" => $Ptype["PaymentTip"], "ACTIVE" => "Y"))){
						$sale_payment[$Ptype["PaymentTip"]]=array("CURRENCY" => "RUB", "NAME" => $Ptype["PaymentTip"], "ACTIVE" => "Y");
					}
				}
				$payments[] = $Ptype["PaymentTip"];
			}
			
			$deliveryTariffs = array("st" => array(),"kr" => Array());
			foreach($item["deliveryTariffs"] as $tarif){
				if($tarif["TariffTip"] == "Стандарт"){
					$deliveryTariffs["st"][] = array("VALUE" => $tarif["TariffSumm"], "DESCRIPTION" => $tarif["OrderSummLow"]."-".$tarif["OrderSummUp"]);
				}elseif($tarif["TariffTip"] == "Крупногабарит"){
					$deliveryTariffs["kr"][] = array("VALUE" => $tarif["TariffSumm"], "DESCRIPTION" => $tarif["OrderSummLow"]."-".$tarif["OrderSummUp"]);
				}
			}
			
			$deliveryTariffIntercities = [];
			foreach($item["deliveryTariffIntercities"] as $tarifIntercities){
				$arFieldsTarifIntercities=array(
					"IBLOCK_ID" => $DELIVERY_TARIF_INTERCITIES_IBLOCK_ID,
					"ACTIVE" => "Y",
					"XML_ID" => $item["ID"]."#".$tarifIntercities["Warehouse_ID"],
					"NAME" => $item["Name"]." / ".$ib_warehouses[$tarifIntercities["Warehouse_ID"]]["NAME"],
					"PROPERTY_VALUES" => array(
						"PriorityIntercityDelivery" => $tarifIntercities["PriorityIntercityDelivery"],
						"PriorityWarehouse" => $tarifIntercities["PriorityWarehouse"],
						"Warehouse_ID" => $tarifIntercities["Warehouse_ID"],
						"Warehouse" => $tarifIntercities["Warehouse_ID"],
						"AmountDays" => $tarifIntercities["AmountDays"],
						"Summ" => $tarifIntercities["Summ"]
					)
				);
				
				$res=false;
				if(isset($ib_deliveryTariffIntercities[$arFieldsTarifIntercities["XML_ID"]])){
					$res=$el->Update($ib_deliveryTariffIntercities[$arFieldsTarifIntercities["XML_ID"]]["ID"], $arFieldsTarifIntercities);
					if($res){
						p('Update data tarif intercities');
						p($arFieldsTarifIntercities);
						
						$tarif_ic_id[] = $arFieldsTarifIntercities["XML_ID"];
						$deliveryTariffIntercities[] = $ib_deliveryTariffIntercities[$arFieldsTarifIntercities["XML_ID"]]["ID"];
					}else{
						p("Error update tarif intercities: ".$el->LAST_ERROR);
						p($arFieldsTarifIntercities);
					}
				}else{
					$res=$el->Add($arFieldsTarifIntercities);
					if($res){
						p('Add data tarif intercities, new id: '.$res);
						p($arFieldsTarifIntercities);
						
						$tarif_ic_id[] = $arFieldsTarifIntercities["XML_ID"];
						$deliveryTariffIntercities[] = $res;
					}else{
						p("Error add tarif intercities: ".$el->LAST_ERROR);
						p($arFieldsTarifIntercities);
					}
				}
			}
			
			$cityFields = array(
				"IBLOCK_ID" => $CITIES_IBLOCK_ID,
				"ACTIVE" => ($item["Activity"]==1)?"Y":"N",
				"XML_ID" => $item["ID"],
				"NAME" => $item["Name"],
				"PROPERTY_VALUES"=>array(
					"region_list" => $ib_region[$item["Region_ID"]],
					"region" => $item["Region_Name"],
					"FLL_ID" => $item["FLL_ID"],
					"RRC_ID" => $item["RRC_ID"],
					"FRC_ID" => $item["FRC_ID"],
					"DeliverySat" => $item["DeliverySat"],
					"DeliverySun" => $item["DeliverySun"],
					"PickupSat" => $item["PickupSat"],
					"PickupSun" => $item["PickupSun"],
					"PickupSun" => $item["PickupSun"],
					"Prepayment" => $item["Prepayment"],
					"shipping_1" => $item["ShiftLocalDeliveryFLL"],
					"shipping_2" => $item["ShiftLocalDeliveryRRC"],
					"shipping_3" => $item["ShiftLocalDeliveryFRC"],
					"TimeWorkStart" => $item["TimeWorkStart"],
					"TimeWorkEnd" => $item["TimeWorkEnd"],
					"ORG_ID" => $item["ORG_ID"],
					"ORG_Name" => $item["ORG_Name"],
					"ORG_FullName" => $item["ORG_FullName"],
					"ORG_UrAdress" => $item["ORG_UrAdress"],
					"ORG_INN" => $item["ORG_INN"],
					"ORG_KPP" => $item["ORG_KPP"],
					"ORG_Chec_account" => $item["ORG_Chec_account"],
					"ORG_Bank" => $item["ORG_Bank"],
					"ORG_Bank_Corr_account" => $item["ORG_Bank_Corr_account"],
					"ORG_Bank_BIK" => $item["ORG_Bank_BIK"],
					"ORG_Phone" => $item["ORG_Phone"],
					"ORG_Email" => $item["ORG_Email"],
					"availablePaymentTips" => $payments,
					"deliveryTariffs_st" => $deliveryTariffs["st"],
					"deliveryTariffs_kr" => $deliveryTariffs["kr"],
					"deliveryTariffIntercities" => $deliveryTariffIntercities,
					"network_data_site_id" => $item["pricebush"]["ID"],
					"network_name" => $item["pricebush"]["Name"],		
					"network_data_code" => Cutil::translit($item["pricebush"]["Name"],"ru",array("replace_space"=>"-","replace_other"=>"-")),
					
					//заполняем старые свойства, возможна избыточность, возможно будут лишние после переработки сайта, лишние можно удалить
					"filial1_code" => $item["RRC_ID"],
					"filial1" => $ib_warehouses[$item["RRC_ID"]]["NAME"],
					"filial2_code" => $item["FRC_ID"],
					"filial2" => $ib_warehouses[$item["FRC_ID"]]["NAME"],
					"predoplata_required" => ($item["Prepayment"]==1)?"да":"нет",
				)
			);
		
			$res=false;
			if(isset($ib_geo[$item["ID"]])){
				$res=$el->Update($ib_geo[$item["ID"]]["ID"], $cityFields);
				if($res){
					p('Update data');
					p($cityFields);
					
					$res_id[] = $cityFields["XML_ID"];
				}else{
					p("Error update: ".$el->LAST_ERROR);
					p($cityFields);
				}
			}else{
				$res=$el->Add($cityFields);
				if($res){
					p('Add data, new id: '.$res);
					p($cityFields);
					
					$res_id[] = $cityFields["XML_ID"];
				}else{
					p("Error add: ".$el->LAST_ERROR);
					p($cityFields);
				}
			}
		}
	}
	
	foreach($ib_deliveryTariffIntercities as $xml_id=>$ib_tarif_ic){
		if(!in_array($xml_id,$tarif_ic_id)){
			p('Delete tarif intercities');
			p($ib_tarif_ic);
			CIBlockElement::Delete($ib_tarif_ic['ID']);
		}
	}
	
	foreach($ib_geo as $xml_id=>$ib_city){
		if(!in_array($xml_id,$res_id)){
			p('Delete');
			p($ib_city);
			CIBlockElement::Delete($ib_city['ID']);
		}
	}	
}	

?>