<?
include $_SERVER['DOCUMENT_ROOT'].'/bitrix/modules/main/include/prolog_before.php';
// require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("������ �������");

CModule::IncludeModule('iblock');
@ini_set('memory_limit', '4096M');
@set_time_limit(595);

$WAREHOUSES_IBLOCK_ID = 38;
$WAREHOUSES_CONSISTS_IBLOCK_ID = 39;
$EXT_WAY = 'http://rusklimat.webway.ru';

function get_web_page( $url ) {
	$res = array();
	$options = array( 
		CURLOPT_RETURNTRANSFER => true,	 // return web page 
		CURLOPT_HEADER		 => false,	// do not return headers 
		CURLOPT_FOLLOWLOCATION => true,	 // follow redirects 
		// CURLOPT_USERAGENT	  => "spider", // who am i 
		CURLOPT_USERAGENT	  => "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/45.0.2454.85 Safari/537.36", // who am i 
		CURLOPT_AUTOREFERER	=> true,	 // set referer on redirect 
		CURLOPT_CONNECTTIMEOUT => 120,	  // timeout on connect 
		CURLOPT_TIMEOUT		=> 120,	  // timeout on response 
		CURLOPT_MAXREDIRS	  => 10,	   // stop after 10 redirects 
	); 
	$ch	  = curl_init( $url ); 
	curl_setopt_array( $ch, $options ); 
	$content = curl_exec( $ch ); 
	$err	 = curl_errno( $ch ); 
	$errmsg  = curl_error( $ch ); 
	$header  = curl_getinfo( $ch ); 
	curl_close( $ch ); 

	$res['content'] = $content;	 
	$res['url'] = $header['url'];
	return $res; 
}


$geo_get_url = $EXT_WAY.'/api/get/warehouses';


$warehouses = [];
$ib_warehouses = [];
$ib_warehouses_consists = [];

if (!file_exists('./data_warehouses.php')) {
	$warehouses = get_web_page($geo_get_url);
	//p($warehouses);
	$warehouses = json_decode($warehouses['content'], true);
	//file_put_contents('./data_warehouses.php', '<? $warehouses = '.var_export($warehouses, 1).';');
} else {
	include('./data_warehouses.php');
}

if(!empty($warehouses)){
	$consistsOb=CIBlockElement::GetList(array(),array("IBLOCK_ID"=>$WAREHOUSES_CONSISTS_IBLOCK_ID),false,false,array("ID","NAME","XML_ID"));
	while($consistsAr=$consistsOb->GetNext()){
		$ib_warehouses_consists[$consistsAr["XML_ID"]]=$consistsAr;
	}

	$warehousesOb=CIBlockElement::GetList(array(),array("IBLOCK_ID"=>$WAREHOUSES_IBLOCK_ID),false,false,array("ID","NAME","XML_ID","PROPERTY_ID","PROPERTY_ShiftPickup","PROPERTY_ShiftPickupFLL","PROPERTY_ShiftPickupRRC","PROPERTY_ShiftPickupFRC","PROPERTY_TimeOrders01","PROPERTY_TimeOrders02","PROPERTY_TimeOrders03","PROPERTY_consists"));
	while($warehousesAr=$warehousesOb->GetNext()){
		$ib_warehouses[$warehousesAr["XML_ID"]]=$warehousesAr;
	}

	$el = new CIBlockElement();
	$res_id = [];
	foreach($warehouses as $warehouse){
		$arFieldsWarehouse=array(
			"IBLOCK_ID" => $WAREHOUSES_IBLOCK_ID,
			"XML_ID" => $warehouse["ID"],
			"NAME" => $warehouse["Name"],
			"PROPERTY_VALUES"=>array(
				"ShiftPickup" => $warehouse["ShiftPickup"],
				"ShiftPickupFLL" => $warehouse["ShiftPickupFLL"],
				"ShiftPickupRRC" => $warehouse["ShiftPickupRRC"],
				"ShiftPickupFRC" => $warehouse["ShiftPickupFRC"],
				"TimeOrders01" => $warehouse["TimeOrders01"],
				"TimeOrders02" => $warehouse["TimeOrders02"],
				"TimeOrders03" => $warehouse["TimeOrders03"],
				"consists" => Array()
			)
		);
		
		if(is_array($warehouse['consists'])){
			foreach($warehouse['consists'] as $consist){
				if(!isset($ib_warehouses_consists[$consist["Consist_ID"]])){
					$arFieldsConsist=array(
						"XML_ID" => $consist["Consist_ID"],
						"NAME" => $consist["Consist_Name"],
						"IBLOCK_ID" => $WAREHOUSES_CONSISTS_IBLOCK_ID
					);
					$res=$el->Add($arFieldsConsist);
					if($res){
						$ib_warehouses_consists[$consist["Consist_ID"]]=array(
							"XML_ID" => $consist["Consist_ID"],
							"NAME" => $consist["Consist_Name"],
							"ID" => $res
						);
					}
				}
				$arFieldsWarehouse["PROPERTY_VALUES"]["consists"]=$ib_warehouses_consists[$consist["Consist_ID"]]["ID"];
			}
			
		}
		
		$res=false;
		if(isset($ib_warehouses[$warehouse["ID"]])){
			$res=$el->Update($ib_warehouses[$warehouse["ID"]]["ID"], $arFieldsWarehouse);
			if($res){
				p('Update data');
				p($arFieldsWarehouse);
				
				$res_id[] = $arFieldsWarehouse["XML_ID"];
			}else{
				p("Error update: ".$el->LAST_ERROR);
				p($arFieldsWarehouse);
			}
		}else{
			$res=$el->Add($arFieldsWarehouse);
			if($res){
				p('Add data, new id: '.$res);
				p($arFieldsWarehouse);
				
				$res_id[] = $arFieldsWarehouse["XML_ID"];
			}else{
				p("Error add: ".$el->LAST_ERROR);
				p($arFieldsWarehouse);
			}
		}
	}
	
	foreach($ib_warehouses as $xml_id=>$ib_warehouse){
		if(!in_array($xml_id,$res_id)){
			p('Delete');
			p($ib_warehouse);
			CIBlockElement::Delete($ib_warehouse['ID']);
		}
	}
}	
?>