<?
include $_SERVER['DOCUMENT_ROOT'].'/bitrix/modules/main/include/prolog_before.php';
// require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("РеИмпорт текстовых разделов");

CModule::IncludeModule('iblock');
Cmodule::IncludeModule('catalog');
@ini_set('memory_limit', '4096M');
@set_time_limit(595);

$CATALOG_IBLOCK_ID = 8;
$BRANDS_IBLOCK_ID = 9;
$GEO_IBLOCK_ID = 15;
$GEO_NETWORKS_IBLOCK_ID = 20;
$OLD_CONTENT = 10;
$GEO_CONTENT = 19;
$EXT_WAY = 'http://rusklimat.webway.ru';

$TMP_FILE = './tmp_content_file.php';

$old_elements = [];

$arFilter = Array("IBLOCK_ID"=>$OLD_CONTENT);
$arSelect = ['NAME', 'PROPERTY_url'];
$res = CIBlockElement::GetList(Array(), $arFilter, false, false, $arSelect);  
while($arFields = $res->GetNext()) {
	if ($arFields['PROPERTY_URL_VALUE'] == '/') {
		continue;
	}
	if (!isset($old_elements[$arFields['PROPERTY_URL_VALUE']])) {
		$old_elements[$arFields['PROPERTY_URL_VALUE']] = $arFields;
	} else {
		p('PAGE ALREADY ADDED: '.$arFields['PROPERTY_URL_VALUE']);
	}
}

// p($old_elements);
die();

foreach($old_elements as $old_element) {
	$url = $old_element['PROPERTY_URL_VALUE'];
	$file = $_SERVER['DOCUMENT_ROOT'].$url.'index.php';
	if (!file_exists($file)) {
		p('NO FILE '.$file);
	} else {
		$data = file_get_contents($file);
		$data = str_replace([
			'require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");',
			'require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");',
			'$_SERVER["DOCUMENT_ROOT"].',
		], '', $data);
		// p($data);
		
		
		
		ob_start();
		
		file_put_contents($TMP_FILE, $data);
		include($TMP_FILE);
		
		$good_data = ob_get_contents();
		$good_data = trim($good_data);
		ob_end_clean();
		
		$title = $APPLICATION->GetTitle();
		$seo_title = $APPLICATION->GetPageProperty('title');
		$seo_description = $APPLICATION->GetPageProperty('description');
		$seo_keywords = $APPLICATION->GetPageProperty('keywords');
		
		p('$title is '.$title);
		p('$seo_title is '.$seo_title);
		// p('$seo_description is '.$seo_description);
		// p('$seo_keywords is '.$seo_keywords);
		// p('$good_data is '.$good_data);
		
		$arLoadProductArray = Array(
			"IBLOCK_SECTION_ID" => false,
			"IBLOCK_ID"      => $GEO_CONTENT,
			"NAME"           => $title,
			"CODE"           => '',
			"XML_ID"         => '',
			"ACTIVE"         => "Y",
			"SORT"    => '500',
			"DETAIL_TEXT"    => $good_data,
			"DETAIL_TEXT_TYPE"    => 'html',
			"PROPERTY_VALUES"    => [
				"url" => $url,
				"seo_title" => $seo_title,
				"seo_keywords" => $seo_keywords,
				"seo_description" => $seo_description,
				"IS_DEFAULT" => 1,
				"GEO" => 42167,
			],
		);
		
		$el = new CIBlockElement;
		if($PRODUCT_ID = $el->Add($arLoadProductArray, false, false, false)) {
			p("New ID: ".$PRODUCT_ID);
			// $geo_items[$arLoadProductArray['XML_ID']] = $PRODUCT_ID;
		} else {
			echo '<div class="make_error">';
			p("Error add: ".$el->LAST_ERROR);
			echo '</div>';
		}
		// p($arLoadProductArray);
		// p('-----------------------------------------------------------------------------------');
		
		// break;
	}
}
if (file_exists($TMP_FILE)) {
	unlink($TMP_FILE);
}

?>


<?
//require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");
?>