<?
include $_SERVER['DOCUMENT_ROOT'].'/bitrix/modules/main/include/prolog_before.php';
// require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Добавление счетчиков гео");

CModule::IncludeModule('iblock');
Cmodule::IncludeModule('catalog');
@ini_set('memory_limit', '4096M');
@set_time_limit(595);

$CATALOG_IBLOCK_ID = 8;
$GEO_NETWORKS_IBLOCK_ID = 20;
$VIEWS_IBLOCK_ID = 25;
$PRICE_TYPE_ID = 1;

$geo = [];

$arFilter = Array("IBLOCK_ID"=>$GEO_NETWORKS_IBLOCK_ID);
$arSelect = Array("ID", "NAME", "CODE", "XML_ID");
$res = CIBlockElement::GetList(Array(), $arFilter, false, false, $arSelect);  
while($arFields = $res->Fetch()) { 
	if (!empty($arFields['CODE'])) {
		$arFields['NAME'] = str_replace('Русклимат ', '', $arFields['NAME']);
		$geo[$arFields['ID']] = [$arFields['CODE'], $arFields['NAME']];
	}
}

// p($geo);

$prop_list = [];
$properties = CIBlockProperty::GetList(Array("sort"=>"asc", "name"=>"asc"), Array("IBLOCK_ID"=>$VIEWS_IBLOCK_ID));
while ($prop_fields = $properties->GetNext()) {
	
	if ($prop_fields["SORT"] == 51) {
		$prop_list[$prop_fields['CODE']] = $prop_fields['ID'];
		// CIBlockProperty::Delete($prop_fields['ID']);
	}
	
	// p($prop_fields);
	
	
	
}
p($prop_list);
// die();

foreach($geo as $geo_id => $geo_data) {
	
	$code = $geo_data[0];
	$code = preg_replace('/\-+/ui', '_', $code);
	$code = strtoupper($code);
	
	$arFields = Array(
		"NAME" => 'Счетчик куста '.$geo_data[1].' ['.$geo_data[0].']',
		"ACTIVE" => "Y",
		"SORT" => 51,
		"CODE" => "SEE_".$code,
		"XML_ID" => $geo_data[0],
		// "HINT" => $param['ТипХарактеристики'],
		// "FILTER_HINT" => $param['ЕдиницаИзмерения'],
		"COL_COUNT" => "10",
		"FILTRABLE" => "N",
		"SEARCHABLE" => "N",
		"SMART_FILTER" => "N",
		"PROPERTY_TYPE" => "N",
		"SECTION_PROPERTY" => "N",
		"USER_TYPE" => "",
		// "DEFAULT_VALUE" => "0",
		"IBLOCK_ID" => $VIEWS_IBLOCK_ID,
	);
	if (!empty($prop_list[$arFields['CODE']])) {
		continue;
	}
	

	$el = new CIBlockProperty;
	
	if($PRODUCT_ID = $el->Add($arFields)) {
		p("New ID: ".$PRODUCT_ID);
	} else {
		p("Error add: ".$el->LAST_ERROR);
	}
	
	p($arFields);
	// break;
}