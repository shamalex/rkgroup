#!/bin/sh
SITE_DIR='/sites/rusklimat_denis/www';

if [ -f "$SITE_DIR/import/parser.stat" ]
then
	echo "������ ��� �����������:  `date +%Y-%m-%d:%H:%M:%S`">>$SITE_DIR/import/bash_log.txt
else
	/usr/bin/touch $SITE_DIR/import/parser.stat
	/usr/bin/find $SITE_DIR/upload/parser_files/ -type f -mtime +1 -print0 | /usr/bin/xargs -0 /bin/rm -f
	echo "��������� ������ ������: `date +%Y-%m-%d:%H:%M:%S`">>$SITE_DIR/import/bash_log.txt
	/usr/local/bin/php $SITE_DIR/import/catalog.php groups getdata
	/usr/local/bin/php $SITE_DIR/import/catalog.php items getdata
	/usr/local/bin/php $SITE_DIR/import/catalog.php prices getdata
	/usr/local/bin/php $SITE_DIR/import/catalog.php harakteristiki getdata
	/usr/local/bin/php $SITE_DIR/import/catalog.php proplink getdata
	/usr/local/bin/php $SITE_DIR/import/catalog.php actions getdata
	/usr/local/bin/php $SITE_DIR/import/catalog.php time2buy getdata
	echo "��������� ������ �����: `date +%Y-%m-%d:%H:%M:%S`">>$SITE_DIR/import/bash_log.txt
	echo "������ ������ ������: `date +%Y-%m-%d:%H:%M:%S`">>$SITE_DIR/import/bash_log.txt
	/usr/local/bin/php $SITE_DIR/import/catalog.php harakteristiki import
	/usr/local/bin/php $SITE_DIR/import/catalog.php groups import params
	/usr/local/bin/php $SITE_DIR/import/catalog.php groups import cats
	/usr/local/bin/php $SITE_DIR/import/catalog.php items import params
	/usr/local/bin/php $SITE_DIR/import/catalog.php proplink import
	/usr/local/bin/php $SITE_DIR/import/catalog.php items import marki
	if [ "`date +%H`" = "01" ]; then
		/usr/local/bin/php $SITE_DIR/import/catalog.php items import itemsfull
		/usr/local/bin/php $SITE_DIR/import/catalog.php actions import
	else
		/usr/local/bin/php $SITE_DIR/import/catalog.php items import items
	fi
	/usr/local/bin/php $SITE_DIR/import/catalog.php prices import
	/usr/local/bin/php $SITE_DIR/import/catalog.php time2buy import
	echo "������ ������ �����: `date +%Y-%m-%d:%H:%M:%S`">>$SITE_DIR/import/bash_log.txt
	/bin/rm $SITE_DIR/import/parser.stat
fi