<?
ini_set("display_errors", 1);
error_reporting (E_ALL);
include $_SERVER['DOCUMENT_ROOT'].'/bitrix/modules/main/include/prolog_before.php';
$APPLICATION->SetTitle("Импорт скидок");

use Bitrix\Main;
use Bitrix\Sale\Internals;
CModule::IncludeModule('iblock');
Cmodule::IncludeModule('sale');
@ini_set('memory_limit', '4096M');
@set_time_limit(595);

$exists_discount=[];

$getListParams = array(
	'select' => array("ID","XML_ID"),
	'filter' => array(),
	'order' => array("ID" => "ASC")
);
$resDisc=Internals\DiscountTable::getList($getListParams);
while($arDisc=$resDisc->Fetch()){
	if($arDisc["XML_ID"])
		$exists_discount[$arDisc["XML_ID"]]=$arDisc["ID"];
}


$xml = simplexml_load_file ("./discount.xml");
$elements= [];
$paysystems = [];
$delivery = [];

$xml=json_decode(json_encode((array)$xml),TRUE);
//p($xml);
foreach($xml["promocode"] as $promocode){
	if(!is_array($promocode["items"]["items_include"]["item"])){
		$promocode["items"]["items_include"]["item"]=array($promocode["items"]["items_include"]["item"]);
	}
	if(!is_array($promocode["payments"]["payments_include"]["payment"])){
		$promocode["payments"]["payments_include"]["payment"]=array($promocode["payments"]["payments_include"]["payment"]);
	}
	if(!is_array($promocode["shipping"]["shippings_include"]["shipping"])){
		$promocode["shippings"]["shippings_include"]["shipping"]=array($promocode["shippings"]["shippings_include"]["shipping"]);
	}
	if(!is_array($promocode["codes"]["code"])){
		$promocode["codes"]["code"]=array($promocode["codes"]["code"]);
	}
	if(!is_array($promocode["sitys"]["sity_include"]["sity"])){
		$promocode["sitys"]["sity_include"]["sity"]=array($promocode["sitys"]["sity_include"]["sity"]);
	}
//p($promocode);
	$discount_arr=[];
	$discount_arr["ACTIVE"]=($promocode["params"]["active"]=="true")?"Y":"N";
	$discount_arr["LID"]=SITE_ID;
	$discount_arr["NAME"]=$promocode["params"]["name"];
	$discount_arr["ACTIVE_FROM_FILTER_PERIOD"]="interval";
	$discount_arr["ACTIVE_FROM_FILTER_DIRECTION"]="previous";
	$discount_arr["ACTIVE_FROM"]=date("d.m.Y H:i:s",MakeTimeStamp($promocode["params"]["start_date"],"YYYY.MM.DD HH:MI"));
	$discount_arr["ACTIVE_TO"]=date("d.m.Y H:i:s",MakeTimeStamp($promocode["params"]["finish_date"],"YYYY.MM.DD HH:MI"));
	$discount_arr["PRIORITY"]=1; // ???
	$discount_arr["SORT"]=100; // ???
	$discount_arr["LAST_DISCOUNT"]="Y"; // ???
	$discount_arr["actrl"]=Array();
	$discount_arr["rule"]=Array(
		"0" => Array(
			"controlId" => "CondGroup",
            "aggregator" => "AND",
			"value" => "True"
        )
	);

	$rule_index=0;
	if($promocode["discount"]["discount_percent"]!="false"){
		$discount_arr["actrl"] = Array(
			"0" => Array(
				"controlId" => "CondGroup",
			),
			"0__0" => Array(
				"controlId" => "ActSaleBsktGrp",
				"extra" => "Discount",
				"extra_size" => $promocode["discount"]["discount_percent"],
				"extra_unit" => "Perc",
				"aggregator" => "OR",
			)
		);
		if($promocode["items"]["all"]=="false" && count($promocode["items"]["items_include"]["item"])>0){
			if(count($elements)==0){
				$resEl=CIBlockElement::GetList(array(),array("IBLOCK_ID"=>8),false,false,array("ID","PROPERTY_CODE"));
				while($arEl=$resEl->GetNext()){
					$elements[$arEl["PROPERTY_CODE_VALUE"]]=$arEl["ID"];
				}
			}
			foreach($promocode["items"]["items_include"]["item"] as $key=>$item){
				if($item && $elements[$item]){
					$discount_arr["actrl"]["0__0__".$key]=Array(
						"controlId" => "CondIBElement",
						"logic" => "Equal",
						"value" => $elements[$item]
					);
				}
			}
		}
	}elseif($promocode["discount"]["discount_money"]!="false"){
		$discount_arr["actrl"] = Array(
			"0" => Array(
				"controlId" => "CondGroup",
			),
			"0__0" => Array(
				"controlId" => "ActSaleBsktGrp",
				"extra" => "Discount",
				"extra_size" => $promocode["discount"]["discount_money"],
				"extra_unit" => "CurEach",
				"aggregator" => "OR",
			)
		);
		if($promocode["items"]["all"]=="false" && count($promocode["items"]["items_include"]["item"])>0){
			if(count($elements)==0){
				$resEl=CIBlockElement::GetList(array(),array("IBLOCK_ID"=>8),false,false,array("ID","PROPERTY_CODE"));
				while($arEl=$resEl->GetNext()){
					$elements[$arEl["PROPERTY_CODE_VALUE"]]=$arEl["ID"];
				}
			}
			
			foreach($promocode["items"]["items_include"]["item"] as $key=>$item){
				if($item && $elements[$item]){
					$discount_arr["actrl"]["0__0__".$key]=Array(
						"controlId" => "CondIBElement",
						"logic" => "Equal",
						"value" => $elements[$item]
					);
				}
			}
		}		
	}elseif($promocode["discount"]["free_shipping"]!="false"){
		$discount_arr["actrl"] = Array(
			"0" => Array(
				"controlId" => "CondGroup",
			),
			"0__0" => Array(
				"controlId" => "ActSaleDelivery",
				"extra" => "Discount",
				"extra_size" => 100,
				"extra_unit" => "Perc",
			)
		);	

		if($promocode["items"]["all"]=="false" && count($promocode["items"]["items_include"]["item"])>0){
			if(count($elements)==0){
				$resEl=CIBlockElement::GetList(array(),array("IBLOCK_ID"=>8),false,false,array("ID","PROPERTY_CODE"));
				while($arEl=$resEl->GetNext()){
					$elements[$arEl["PROPERTY_CODE_VALUE"]]=$arEl["ID"];
				}
			}
			foreach($promocode["items"]["items_include"]["item"] as $key=>$item){
				if($item && $elements[$item]){
					$discount_arr["rule"]["0__".$rule_index."__".$key]=Array(
						"controlId" => "CondIBElement",
						"logic" => "Equal",
						"value" => $elements[$item]
					);
				}
			}
			$rule_index++;
		}
	}

	if($promocode["payments"]["all"]=="false" && count($promocode["payments"]["payments_include"]["payment"])>0){

		if(count($paysystems)==0){
			$resPS=CSalePaySystem::GetList($arOrder = Array("SORT"=>"ASC", "PSA_NAME"=>"ASC"), Array(),false,false,array());
			while($arPS=$resPS->GetNext()){
				$paysystems[$arPS["ID"]]=$arPS["NAME"];
			}
		}

		$discount_arr["rule"]["0__".$rule_index]=Array(
			"controlId" => "CondSalePaySystem",
			"logic" => "Equal",
            "value" => Array()
		);

		foreach($promocode["payments"]["payments_include"]["payment"] as $key=>$payment)
		{
			if(in_array($payment,$paysystems)){
				$discount_arr["rule"]["0__".$rule_index]["value"][$key]=array_search($payment,$paysystems);
			}
		}
		$rule_index++;
	}

	if($promocode["shippings"]["all"]=="false" && count($promocode["shippings"]["shippings_include"]["shipping"])>0){
		if(count($delivery)==0){
			$resDel=CSaleDelivery::GetList(array("SORT" => "ASC","NAME" => "ASC"),array(),false,false,array("ID","NAME"));
			while($arDel=$resDel->GetNext()){
				$delivery[$arDel["ID"]]=$arDel["NAME"];
			}
		}
		$discount_arr["rule"]["0__".$rule_index]=Array(
			"controlId" => "CondSaleDelivery",
			"logic" => "Equal",
            "value" => Array()
		);

		$i=0;
		foreach($promocode["shippings"]["shippings_include"]["shipping"] as $shipping)
		{
			$IDdelivery=array_keys($delivery,$shipping);
			if(is_array($IDdelivery)){
				foreach($IDdelivery as $id){
					$discount_arr["rule"]["0__".$rule_index]["value"][$i]=$id;
					$i++;
				}
			}
		}
		$rule_index++;
	}

	$discount_arr["USER_GROUPS"] = Array(0 => 2);
    $discount_arr["COUPON_ADD"] = "";
    $discount_arr["COUPON_COUNT"] = 0;
    $discount_arr["COUPON"] = Array(
		"ACTIVE_FROM" => "",
		"ACTIVE_TO" => "",
		"TYPE" => 1,
		"MAX_USE" => ""
    );
	$discount_arr["XML_ID"] = $promocode["group_id"];
    $discount_arr["save"] = "Сохранить";
    $discount_arr["Update"] = "Y";
    $discount_arr["lang"] = "ru";
    $discount_arr["action"]="";
	if($exists_discount[$promocode["group_id"]])
		$discount_arr["ID"]=$exists_discount[$promocode["group_id"]];
	
//p($discount_arr);
	
	$errors = array();
	$discountID = 0;
	if (isset($discount_arr["ID"]))
	{
		$discountID = (int)$discount_arr["ID"];
		if ($discountID < 0)
			$discountID = 0;
	}
	
	$obCond3 = new CSaleCondTree();

	$boolCond = $obCond3->Init(BT_COND_MODE_PARSE, BT_COND_BUILD_SALE, array('INIT_CONTROLS' => array(
		'SITE_ID' => $discount_arr['LID'],
		'CURRENCY' => CSaleLang::GetLangCurrency($discount_arr['LID']),
	)));
	if (!$boolCond)
	{
		if ($ex = $APPLICATION->GetException())
			$errors[] = "1".$ex->GetString();
		else
			$errors[] = (0 < $discountID ? "Ошибка при изменении правила с кодом ".$discountID : "Ошибка при добавлении правила");
	}
	else
	{
		$boolCond = false;
		if (array_key_exists('CONDITIONS', $discount_arr) && array_key_exists('CONDITIONS_CHECK', $discount_arr))
		{
			if (is_string($discount_arr['CONDITIONS']) && is_string($discount_arr['CONDITIONS_CHECK']) && md5($discount_arr['CONDITIONS']) == $discount_arr['CONDITIONS_CHECK'])
			{
				$CONDITIONS = base64_decode($discount_arr['CONDITIONS']);
				if (CheckSerializedData($CONDITIONS))
				{
					$CONDITIONS = unserialize($CONDITIONS);
					$boolCond = true;
				}
				else
				{
					$boolCondParseError = true;
				}
			}
		}
//p($obCond3);
		if (!$boolCond)
			$CONDITIONS = $obCond3->Parse($discount_arr["rule"]);
		if (empty($CONDITIONS))
		{
			if ($ex = $APPLICATION->GetException())
				$errors[] = "2".$ex->GetString();
			else
				$errors[] = (0 < $discountID ? "Ошибка при изменении правила с кодом ".$discountID : "Ошибка при добавлении правила");
			$boolCondParseError = true;
		}
	}

	$obAct3 = new CSaleActionTree();

	$boolAct = $obAct3->Init(BT_COND_MODE_PARSE, BT_COND_BUILD_SALE_ACTIONS, array('PREFIX' => 'actrl', 'INIT_CONTROLS' => array(
		'SITE_ID' => $discount_arr['LID'],
		'CURRENCY' => CSaleLang::GetLangCurrency($discount_arr['LID']),
	)));
	if (!$boolAct)
	{
		if ($ex = $APPLICATION->GetException())
			$errors[] = "3".$ex->GetString();
		else
			$errors[] = (0 < $discountID ? "Ошибка при изменении правила с кодом ".$discountID : "Ошибка при добавлении правила");
	}
	else
	{
		$boolAct = false;
		if (array_key_exists('ACTIONS', $discount_arr) && array_key_exists('ACTIONS_CHECK', $discount_arr))
		{
			if (is_string($discount_arr['ACTIONS']) && is_string($discount_arr['ACTIONS_CHECK']) && md5($discount_arr['ACTIONS']) == $discount_arr['ACTIONS_CHECK'])
			{
				$ACTIONS = base64_decode($discount_arr['ACTIONS']);
				if (CheckSerializedData($ACTIONS))
				{
					$ACTIONS = unserialize($ACTIONS);
					$boolAct = true;
				}
				else
				{
					$boolActParseError = true;
				}
			}
		}

		if (!$boolAct)
			$ACTIONS = $obAct3->Parse($discount_arr["actrl"]);
		if (empty($ACTIONS))
		{
			if ($ex = $APPLICATION->GetException())
				$errors[] = "4".$ex->GetString();
			else
				$errors[] = (0 < $discountID ? "Ошибка при изменении правила с кодом ".$discountID : "Ошибка при добавлении правила");
			$boolActParseError = true;
		}
	}

	$arGroupID = array();
	if (array_key_exists('USER_GROUPS', $discount_arr) && is_array($discount_arr['USER_GROUPS']))
	{
		foreach ($discount_arr['USER_GROUPS'] as &$intValue)
		{
			$intValue = intval($intValue);
			if ($intValue > 0)
			{
				$arGroupID[] = $intValue;
			}
		}
		if (isset($intValue))
			unset($intValue);
	}

	$arFields = array(
		"LID" => (array_key_exists('LID', $discount_arr) ? $discount_arr['LID'] : ''),
		"NAME" => (array_key_exists('NAME', $discount_arr) ? $discount_arr['NAME'] : ''),
		"ACTIVE_FROM" => (array_key_exists('ACTIVE_FROM', $discount_arr) ? $discount_arr['ACTIVE_FROM'] : ''),
		"ACTIVE_TO" => (array_key_exists('ACTIVE_TO', $discount_arr) ? $discount_arr['ACTIVE_TO'] : ''),
		"ACTIVE" => (array_key_exists('ACTIVE', $discount_arr) && 'Y' == $discount_arr['ACTIVE'] ? 'Y' : 'N'),
		"SORT" => (array_key_exists('SORT', $discount_arr) ? $discount_arr['SORT'] : 500),
		"PRIORITY" => (array_key_exists('PRIORITY', $discount_arr) ? $discount_arr['PRIORITY'] : ''),
		"LAST_DISCOUNT" => (array_key_exists('LAST_DISCOUNT', $discount_arr) && 'N' == $discount_arr['LAST_DISCOUNT'] ? 'N' : 'Y'),
		"XML_ID" => (array_key_exists('XML_ID', $discount_arr) ? $discount_arr['XML_ID'] : ''),
		'CONDITIONS' => $CONDITIONS,
		'ACTIONS' => $ACTIONS,
		'USER_GROUPS' => $arGroupID,
	);
//p($arFields);
	if (empty($errors))
	{
		if ($discountID > 0)
		{
			if (!CSaleDiscount::Update($discountID, $arFields))
			{
				if ($ex = $APPLICATION->GetException())
					$errors[] = "5".$ex->GetString();
				else
					$errors[] = "Ошибка при изменении правила с кодом ".$discountID;
			}
			else{
				$discountList = array();
				$listID=array();
				$couponIterator = Internals\DiscountCouponTable::getList(array(
					'select' => array('ID', 'DISCOUNT_ID'),
					'filter' => array("DISCOUNT_ID"=>$discountID)
				));
				while ($coupon = $couponIterator->fetch())
				{
					$listID[] = $coupon['ID'];
					$discountList[$coupon['DISCOUNT_ID']] = $coupon['DISCOUNT_ID'];
				}
				
				Internals\DiscountCouponTable::setDiscountCheckList($discountList);
				Internals\DiscountCouponTable::disableCheckCouponsUse();
				foreach ($listID as &$couponID)
				{
					$result = Internals\DiscountCouponTable::delete($couponID);
					if (!$result->isSuccess())
						$errors=implode('<br>', $result->getErrorMessages());
					unset($result);
				}
				unset($couponID);			
			}
		}
		else
		{
			$discountID = (int)CSaleDiscount::Add($arFields);
			if ($discountID <= 0)
			{
				if ($ex = $APPLICATION->GetException())
					$errors[] = "6".$ex->GetString();
				else
					$errors[] = "Ошибка при добавлении правила";
			}
		}
		if($discountID && count($promocode["codes"]["code"])>0){
			foreach($promocode["codes"]["code"] as $code){
				$fields=array();
				$fields['DISCOUNT_ID'] = $discountID;
				$fields['COUPON'] = $code;
				$fields['ACTIVE'] = $discount_arr["ACTIVE"];
				$fields['ACTIVE_FROM'] = new Main\Type\DateTime($discount_arr["ACTIVE_FROM"]);
				$fields['ACTIVE_TO'] = new Main\Type\DateTime($discount_arr['ACTIVE_TO']);
				$fields['TYPE'] = ($promocode["params"]["multiple"]=="true")?4:2;
				$fields['MAX_USE'] = "";
				$fields['USER_ID'] = "";
				$fields['DESCRIPTION'] = $promocode["params"]["comment"];			
				$result = Internals\DiscountCouponTable::add($fields);
				if (!$result->isSuccess())
				{
					$errors=array_merge($errors, $result->getErrorMessages());
				}
			}
			Internals\DiscountCouponTable::enableCheckCouponsUse();
			Internals\DiscountCouponTable::updateUseCoupons();		
		}
		if($discountID){
			$geoID=0;
			$resGD=CIBlockElement::GetList(array(),array("IBLOCK_ID"=>29,"PROPERTY_discountID"=>$discountID),false,false,array("ID"));
			if($arGD=$resGD->GetNext())
				$geoID=$arGD["ID"];
			
			$fields=array();
			$fields["IBLOCK_ID"]=29;
			$fields["NAME"]=$discount_arr["NAME"];
			$fields["ACTIVE"]=$discount_arr["ACTIVE"];
			$fields['ACTIVE_FROM'] = $discount_arr["ACTIVE_FROM"];
			$fields['ACTIVE_TO'] = $discount_arr['ACTIVE_TO'];
			$fields['PREVIEW_TEXT'] = $promocode["params"]["comment"];			
			$fields["PROPERTY_VALUES"]["discountID"]=$discountID;
			$fields["PROPERTY_VALUES"]["city"]=$promocode["sitys"]["sity_include"]["sity"];
			$fields["PROPERTY_VALUES"]["coupon"]=$promocode["codes"]["code"];
			
			$el = new CIBlockElement;
			if(!$geoID){
				if(!$el->Add($fields)) {
					$errors[]=$el->LAST_ERROR;
				}
			}else{
				if(!$el->Update($geoID,$fields)) {
					$errors[]=$el->LAST_ERROR;
				}
			}
		}
	}
	if (!empty($errors))
	{
		echo "<b>Ошибки в элементе [".$discount_arr["XML_ID"]."] ".$discount_arr["NAME"]."</b><br />";
		echo implode("<br />",$errors);
	}	
}