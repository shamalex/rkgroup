<? $APPLICATION->AddBufferContent('ShowContentWrapFooter'); ?></section>
<!-- / midsection -->

<!-- footer wrap-->
<div class="footer-w">
	<!-- footer -->
	<footer class="footer c">
		<!-- ft-tp -->
		<div class="ft-tp c">
			<?$APPLICATION->IncludeFile("include/footer_logo.php", Array(), Array("MODE"=>"html"));?>

			<div class="ft-em">
				<div class="eml"><a href="mailto:online@rusklimat.ru">online@rusklimat.ru</a></div>
				<p class="cmt">Мы любим получать письма</p>
			</div>

			<div class="ft-phn">
				<div class="phn"><?= $arGeoData['CUR_CITY']['PHONE'] ?></div>
				<p class="cmt">И отвечать на ваши звонки.</p>
			</div>

			<a href="#callback" class="callback callback-pop">Обратный звонок</a>

		</div>
		<!-- /ft-tp end -->

		<?$APPLICATION->IncludeComponent("bitrix:menu", "bottom", Array(
			"ROOT_MENU_TYPE" => "bottom",	// Тип меню для первого уровня
			"MENU_CACHE_TYPE" => "A",	// Тип кеширования
			"MENU_CACHE_TIME" => "36000000",	// Время кеширования (сек.)
			"MENU_CACHE_USE_GROUPS" => "N",	// Учитывать права доступа
			"MENU_THEME" => "site",
			"CACHE_SELECTED_ITEMS" => "N",
			"MENU_CACHE_GET_VARS" => "",	// Значимые переменные запроса
			"MAX_LEVEL" => "1",	// Уровень вложенности меню
			"CHILD_MENU_TYPE" => "",	// Тип меню для остальных уровней
			"USE_EXT" => "N",	// Подключать файлы с именами вида .тип_меню.menu_ext.php
			"DELAY" => "N",	// Откладывать выполнение шаблона меню
			"ALLOW_MULTI_SELECT" => "N",	// Разрешить несколько активных пунктов одновременно
			"COMPONENT_TEMPLATE" => ".default"
			),
			false
		);?>

		<a href="#feedback" class="feedback feedback-pop">написать руководителю</a>

		<!-- ft-media -->
		<div class="ft-media">
			<?php /*<div class="ft-sub c">
				<p class="cmt">Подпишитесь на рассылку спецпредложений</p>
				<form action="#" class="sub-form" id="subscribeMeForm">
					<input type="email" class="sub-field" placeholder="Ваш email" name="email" required />
					<input type="submit" class="sub-btn" name="sub-btn" value="ОК" />
				</form>
			</div>*/?>

			<?
			/*
			<div class="ft-social c">
				<p class="cmt">Мы в социальных сетях</p>
				<ul class="social-ul">
					<li><a class="social-fb" href="#">&nbsp;</a></li>
					<li><a class="social-tw" href="#">&nbsp;</a></li>
					<li><a class="social-ok" href="#">&nbsp;</a></li>
					<li><a class="social-vk" href="#">&nbsp;</a></li>
					<li><a class="social-gp" href="#">&nbsp;</a></li>
				</ul>
			</div>
			*/
			?>

			<div class="ft-cards c">
				<p class="cmt">Мы принимаем к оплате</p>
				<div class="i-visa-master c">
					<div class="ico i-visa"></div>
					<div class="ico i-master"></div>
				</div>
			</div>

			<?
			/*
			<div class="ft-rnb">
				<?$APPLICATION->IncludeFile("include/market_yandex.php", Array(), Array("MODE"=>"html"));?>
			</div>
			*/
			?>

		</div>
		<!-- /ft-media end -->

		<?$APPLICATION->IncludeFile("include/ww.php", Array(), Array("MODE"=>"html"));?>

		</footer>
	<!-- /footer end -->
</div>
<!-- footer wrap end-->


<!-- fixed pseudo header -->
<div class="fix-hd-w">
	<!-- fx-hd -->
	<div class="fix-hd c">

		<div class="cphn item">
			<div class="i-w c">
				<a class="city city-pop" href="#select_city"><span class="city_name_here"><!--Омск--></span></a>
				<div class="phn"><?= $arGeoData['CUR_CITY']['PHONE'] ?></div>
			</div>
		</div>

		<div id="fav_header_wrap">
			<? include($_SERVER['DOCUMENT_ROOT'].'/add_to_fav.php'); ?>
		</div>

		<?
		// basket loads before compare for preload num word function
		ob_start();
		?>
		<?$APPLICATION->IncludeComponent("bitrix:sale.basket.basket.line", "header_fixed", Array(
				"COMPONENT_TEMPLATE" => ".default",
				"PATH_TO_BASKET" => SITE_DIR."personal/cart/",	// Страница корзины
				"PATH_TO_PERSONAL" => SITE_DIR."personal/",	// Страница персонального раздела
				"PATH_TO_PROFILE" => SITE_DIR."personal/",	// Страница профиля
				"PATH_TO_REGISTER" => SITE_DIR."login/",	// Страница регистрации
				"POSITION_FIXED" => "N",	// Отображать корзину поверх шаблона
				"SHOW_AUTHOR" => "N",	// Добавить возможность авторизации
				"SHOW_EMPTY_VALUES" => "Y",	// Выводить нулевые значения в пустой корзине
				"SHOW_NUM_PRODUCTS" => "Y",	// Показывать количество товаров
				"SHOW_PERSONAL_LINK" => "N",	// Отображать персональный раздел
				"SHOW_PRODUCTS" => "Y",	// Показывать список товаров
				"SHOW_TOTAL_PRICE" => "Y",	// Показывать общую сумму по товарам
			),
			false
		);?>
		<?
		$BASKET_LINE_DATA = ob_get_contents();
		ob_end_clean();
		?>

		<?$APPLICATION->IncludeComponent("bitrix:catalog.compare.list", "top", Array(
				"ACTION_VARIABLE" => "action_footer",	// Название переменной, в которой передается действие
				"AJAX_MODE" => "N",	// Включить режим AJAX
				"AJAX_OPTION_ADDITIONAL" => "",	// Дополнительный идентификатор
				"AJAX_OPTION_HISTORY" => "N",	// Включить эмуляцию навигации браузера
				"AJAX_OPTION_JUMP" => "N",	// Включить прокрутку к началу компонента
				"AJAX_OPTION_STYLE" => "N",	// Включить подгрузку стилей
				"COMPARE_URL" => "/catalog/compare/",	// URL страницы с таблицей сравнения
				"COMPONENT_TEMPLATE" => ".default",
				"DETAIL_URL" => "",	// URL, ведущий на страницу с содержимым элемента раздела
				"IBLOCK_ID" => "8",	// Инфоблок
				"IBLOCK_TYPE" => "catalog",	// Тип инфоблока
				"NAME" => "CATALOG_COMPARE_LIST",	// Уникальное имя для списка сравнения
				"POSITION" => "top left",
				"POSITION_FIXED" => "N",	// Отображать список сравнения поверх страницы
				"PRODUCT_ID_VARIABLE" => "add_id_footer",	// Название переменной, в которой передается код товара для покупки
			),
			false
		);?>
		<? $APPLICATION->AddBufferContent('CatalogCompareList'); ?>

		<?
		echo $BASKET_LINE_DATA;
		unset($BASKET_LINE_DATA);
		?>

	</div>
	<!-- / fx-hd end -->
</div>
<!-- / fixed pseudo header end -->

<? if(CheckServerIPs()) :?>
	<?$APPLICATION->IncludeFile("include/counters_footer.php", Array(), Array("MODE"=>"html", "SHOW_BORDER"=>false));?>
<? endif; ?>

</body>
</html>
