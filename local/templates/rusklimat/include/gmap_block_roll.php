<?
global $arGeoData;
?>
<? if(!empty($arGeoData['CUR_CITY']['CITY_CODE'])): ?>
	<!-- gmap block roll -->
	<div class="roll">
		<div class="map">
			<!-- gmap header -->
			<div class="map-hd c">
				<h1 class="ttl-r">точки самовывоза и сервисные центры</h1>
				<div class="map-city r">
					<div class="map-city-ttl">Ваш город</div>
					<?/*<select name="" id="map-select">
						<option value=""><?=$arGeoData['CUR_CITY']['NAME']?></option>
					</select>*/?>
					<div class="ik_select map-select" id="map-city-select" style="position: relative;"><div class="ik_select_link map-select-link ik_select_link_novalue"><div class="ik_select_link_text"><?=$arGeoData['CUR_CITY']['NAME']?></div></div></div>
				</div>
			</div>
			<!-- / gmap header end -->
			
			<!-- gmap holder (gmap.js) -->
			<!--<div  id="map" class="map-container map-inner"></div>-->
			<?
			global $arGeoData;
			$GLOBALS['arrFilter'] = [
				"CODE" => $arGeoData['CUR_CITY']['CITY_CODE'],
			];
			?>
			<?$APPLICATION->IncludeComponent(
				"bitrix:news.list",
				"index_map",
				Array(
					"ACTIVE_DATE_FORMAT" => "d.m.Y",
					"ADD_SECTIONS_CHAIN" => "N",
					"AJAX_MODE" => "N",
					"AJAX_OPTION_ADDITIONAL" => "",
					"AJAX_OPTION_HISTORY" => "N",
					"AJAX_OPTION_JUMP" => "N",
					"AJAX_OPTION_STYLE" => "N",
					"CACHE_FILTER" => "Y",
					"CACHE_GROUPS" => "N",
					"CACHE_TIME" => "36000000",
					"CACHE_TYPE" => "N",
					"CHECK_DATES" => "Y",
					"COMPONENT_TEMPLATE" => "index_map",
					"DETAIL_URL" => "",
					"DISPLAY_BOTTOM_PAGER" => "N",
					"DISPLAY_DATE" => "N",
					"DISPLAY_NAME" => "Y",
					"DISPLAY_PICTURE" => "N",
					"DISPLAY_PREVIEW_TEXT" => "N",
					"DISPLAY_TOP_PAGER" => "N",
					"FIELD_CODE" => array("ID","CODE","XML_ID","NAME","SORT",""),
					"FILTER_NAME" => "arrFilter",
					"HIDE_LINK_WHEN_NO_DETAIL" => "N",
					"IBLOCK_ID" => "16",
					"IBLOCK_TYPE" => "catalog",
					"INCLUDE_IBLOCK_INTO_CHAIN" => "N",
					"INCLUDE_SUBSECTIONS" => "N",
					"MESSAGE_404" => "",
					"NEWS_COUNT" => "100",
					"PAGER_BASE_LINK_ENABLE" => "N",
					"PAGER_DESC_NUMBERING" => "N",
					"PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
					"PAGER_SHOW_ALL" => "N",
					"PAGER_SHOW_ALWAYS" => "N",
					"PAGER_TEMPLATE" => ".default",
					"PAGER_TITLE" => "",
					"PARENT_SECTION" => "",
					"PARENT_SECTION_CODE" => "",
					"PREVIEW_TRUNCATE_LEN" => "",
					"PROPERTY_CODE" => array("address","place",""),
					"SET_BROWSER_TITLE" => "N",
					"SET_LAST_MODIFIED" => "N",
					"SET_META_DESCRIPTION" => "N",
					"SET_META_KEYWORDS" => "N",
					"SET_STATUS_404" => "N",
					"SET_TITLE" => "N",
					"SHOW_404" => "N",
					"SORT_BY1" => "SORT",
					"SORT_BY2" => "NAME",
					"SORT_ORDER1" => "ASC",
					"SORT_ORDER2" => "ASC",
					"CITY_CODE" => $arGeoData['CUR_CITY']['CITY_CODE'],
				)
			);?>
			<!-- / gmap holder end -->

		</div>
	</div>
	<!-- / map block roll end -->
<? endif; ?>