<div class="w">
			<a href="#" class="x">&nbsp;</a>
			<div class="ttl-p1">Купить в кредит просто!</div>
			<div class="ttl-p2">Всего 4 355 руб./мес.</div>
			<form action="#">
				<div class="items">
					<!-- item -->
					<div class="item brd c">
						<table>
							<tr>
								<td class="rad">
									<input name="credRadio" class="check-custom" type="radio" value="cred1" id="cred1" checked>
								</td>
								<td class="bank">
									<label class="pic" style="background-image:url('../local/templates/rusklimat/i/blocks/cred1.png');" for="cred1"></label>
								</td>
								<td class="dsc">
									<p>Сумма кредита до 200 000 руб.</p>
									<p>Быстрое решение без заполнения анкет.</p>
									<p>Без комиссий за открытие счета и досрочное погашение кредита.</p>
									<p>Точки погашения кредита по всей России.</p>
								</td>
							</tr>
						</table>
					</div>
					<!-- / item -->
					<!-- item -->
					<div class="item brd c">
						<table>
							<tr>
								<td class="rad">
									<input name="credRadio" class="check-custom" type="radio" value="cred2" id="cred2">
								</td>
								<td class="bank">
									<label class="pic" style="background-image:url('../local/templates/rusklimat/i/blocks/cred2.png');" for="cred2"></label>
								</td>
								<td class="dsc">
									<p>Сумма кредита до 200 000 руб.</p>
									<p>Быстрое решение без заполнения анкет.</p>
									<p>Без комиссий за открытие счета и досрочное погашение кредита.</p>
									<p>Точки погашения кредита по всей России.</p>
								</td>
							</tr>
						</table>
					</div>
					<!-- / item -->
					<!-- item -->
					<div class="item brd c">
						<table>
							<tr>
								<td class="rad">
									<input name="credRadio" class="check-custom" type="radio" value="cred3" id="cred3">
								</td>
								<td class="bank">
									<label class="pic" style="background-image:url('../local/templates/rusklimat/i/blocks/cred3.png');" for="cred3"></label>
								</td>
								<td class="dsc">
									<p>Сумма кредита до 200 000 руб.</p>
									<p>Быстрое решение без заполнения анкет.</p>
									<p>Без комиссий за открытие счета и досрочное погашение кредита.</p>
									<p>Точки погашения кредита по всей России.</p>
								</td>
							</tr>
						</table>
					</div>
					<!-- / item -->
					<!-- item -->
					<div class="item brd c">
						<table>
							<tr>
								<td class="rad">
									<input name="credRadio" class="check-custom" type="radio" value="cred4" id="cred4">
								</td>
								<td class="bank">
									<label class="pic" style="background-image:url('../local/templates/rusklimat/i/blocks/cred4.png');" for="cred4"></label>
								</td>
								<td class="dsc">
									<p>Сумма кредита до 200 000 руб.</p>
									<p>Быстрое решение без заполнения анкет.</p>
									<p>Без комиссий за открытие счета и досрочное погашение кредита.</p>
									<p>Точки погашения кредита по всей России.</p>
								</td>
							</tr>
						</table>
					</div>
					<!-- / item -->
				</div>
				<!-- / items end -->

				<div class="btns c">
					<input type="submit" class="btn credit-picked-btn" name="credit-picked-btn" value="купить в кредит" />
					<a class="lnk-consult" href="#"><span>Помощь консультанта</span></a>
				</div>

			</form>

		</div>
		<!-- / credit w end -->

		<!-- pop-ft -->
		<div class="pop-ft">
			<p>Для дополнительной информации обратитесь к менеджеру<br/>кредитного отдела 8 800 567 09 00, доб. 1117</p>
		</div>
		<!-- / pop-ft end -->
