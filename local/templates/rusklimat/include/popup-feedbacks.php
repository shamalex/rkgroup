<?
include $_SERVER['DOCUMENT_ROOT'].'/bitrix/modules/main/include/prolog_before.php';

global $USER;
$rsUser = CUser::GetByID($USER->GetID());
$arUser = $rsUser->Fetch();
?>
<!-- feedback popup, triggered by .feedback-pop -->
<div class="pop pop-feedback pop-bg-1" id="popFeedback">
	<div class="w">
		<a href="#" class="x">&nbsp;</a>
		<h1 class="ttl">Письмо руководству</h1>
		<div class="feed-form">
			<form action="/send_feedback.php" autocomplete="off" enctype="multipart/form-data" method="post">
				<p>
					<input type="text" name="name" class="feed-field" id="feedbackName" placeholder="Ваше ФИО" value="<?=trim($arUser['NAME']." ".$arUser['SECOND_NAME']." ".$arUser['LAST_NAME'])?>" />
					<span class="comment" data-name="name"></span>
				</p>
				<p>
					<input type="text" name="phone" class="feed-field" id="feedbackPhone" placeholder="Телефон" value="<?= $arUser['PERSONAL_PHONE'] ?>" />
					<span class="comment" data-name="phone"></span>
				</p>
				<p>
					<input type="text" name="email" class="feed-field" id="feedbackEmail" placeholder="E-mail" value="<?= $arUser['EMAIL'] ?>" />
					<span class="comment" data-name="email"></span>
				</p>				
				<p>
					<textarea name="message" class="feed-field" id="feedbackMessage" placeholder="Текст сообщения"></textarea>
					<span class="comment" data-name="message"></span>
				</p>				
				<p>
					Приложить файл (до 5 Мб): <input type="file" name="file" class="" id="feedbackFile" />
					<span class="comment" data-name="file"></span>
				</p>
				<input type="text" name="subj" class="feed-field" id="feedbackSubj" value="" />
				<input type="hidden" name="source" value="34" />
				<div class="btn feed-btn test-feedback-pop-ok" id="feedbackBtn">Отправить письмо</div>
			</form>
		</div>
	</div>
</div>
<!-- / feedback popup end -->