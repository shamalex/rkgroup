<div class="empty c">
	<div class="ico basket-empty">
	</div>
	<div class="dsc">
		<h2 class="ttl">Вы пока не добавили товар в корзину!</h2>
		<p>
			 Чтобы найти нужный товар, воспользуйтесь <a href="/catalog/"><strong>каталогом</strong></a> нашей продукции. <br>
			 И не забудьте просмотреть действующие <a href="/special/"><strong>акции</strong></a>, которые помогут приобрести товар с максимальной выгодой.
		</p>
		<p>
			 Удачных покупок!
		</p>
	</div>
</div>
 <br>
