<!-- help block roll -->
<div class="roll">
	<h3 class="ttl-r">ТРЕБУЕТСЯ ПОМОЩЬ?</h3>
	<div class="help c">
		<a class="item b-hlp callback-pop" href="#callback">
			<div class="w">
				<div class="ttl">Закажите звонок!</div>
				<p class="dsc">Удобная функция заказа<br/>обратного звонка.</p>
				<div class="ico ico-1">&nbsp;</div>
			</div>
		</a>
		<a class="item b-hlp" href="tel:<?= $GLOBALS['arGeoData']['CUR_CITY']['PHONE_NUM'] ?>">
			<div class="w">
				<div class="ttl"><?= $GLOBALS['arGeoData']['CUR_CITY']['PHONE'] ?></div>
				<p class="dsc">Помогаем по любым<br/>вопросам продажи и сервиса.</p>
				<div class="ico ico-2">&nbsp;</div>
			</div>
		</a>
		<a class="item b-hlp" href="mailto:online@rusklimat.ru">
			<div class="w">
				<div class="ttl">online@rusklimat.ru</div>
				<p class="dsc">Не стесняйтесь<br/>написать нам письмо.</p>
				<div class="ico ico-3">&nbsp;</div>
			</div>
		</a>
	</div>
</div>
<!-- / help block roll end -->