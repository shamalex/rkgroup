<div class="w">
	<a href="#" class="x">&nbsp;</a>
	<div class="ttl-p1">точки самовывоза</div>
	<form action="#">
		<div class="items">
			<!-- item -->
			<div class="item brd c">
				<table>
					<tr>
						<td class="rad">
							<input name="credRadio" class="check-custom" type="radio" value="self1" id="self1" checked>
						</td>
						<td class="adr">
							<label for="self1">
								<p><strong>Климатический центр Русклимат</strong></p>
								<p>Водный стадион, ул. Нарвская, д.21</p>
							</label>
						</td>
						<td class="dsc">
							<p><strong>График работы</strong></p>
							<p class="str">Пн - Вс: 9.00-16.00</p>
							<p><strong>парковка</strong></p>
							<p class="str">150 парковочных мест</p>
							<p><strong>Способы оплаты</strong></p>
							<p class="str">Наличный расчет, Visa, Master Card</p>
						</td>
					</tr>
				</table>
			</div>
			<!-- / item -->
			<!-- item -->
			<div class="item brd c">
				<table>
					<tr>
						<td class="rad">
							<input name="credRadio" class="check-custom" type="radio" value="self2" id="self2" checked>
						</td>
						<td class="adr">
							<label for="self2">
								<p><strong>Климатический центр Русклимат</strong></p>
								<p>Водный стадион, ул. Нарвская, д.21</p>
							</label>
						</td>
						<td class="dsc">
							<p><strong>График работы</strong></p>
							<p class="str">Пн - Вс: 9.00-16.00</p>
							<p><strong>парковка</strong></p>
							<p class="str">150 парковочных мест</p>
							<p><strong>Способы оплаты</strong></p>
							<p class="str">Наличный расчет, Visa, Master Card</p>
						</td>
					</tr>
				</table>
			</div>
			<!-- / item -->
			<!-- item -->
			<div class="item brd c">
				<table>
					<tr>
						<td class="rad">
							<input name="credRadio" class="check-custom" type="radio" value="self3" id="self3" checked>
						</td>
						<td class="adr">
							<label for="self3">
								<p><strong>Климатический центр Русклимат</strong></p>
								<p>Водный стадион, ул. Нарвская, д.21</p>
							</label>
						</td>
						<td class="dsc">
							<p><strong>График работы</strong></p>
							<p class="str">Пн - Вс: 9.00-16.00</p>
							<p><strong>парковка</strong></p>
							<p class="str">150 парковочных мест</p>
							<p><strong>Способы оплаты</strong></p>
							<p class="str">Наличный расчет, Visa, Master Card</p>
						</td>
					</tr>
				</table>
			</div>
			<!-- / item -->

		</div>
		<!-- / items end -->

		<div class="btns c">
			<input type="submit" class="btn self-picked-btn" name="self-picked-btn" value="Оформит заказ" />
			<a class="lnk-consult" href="#"><span>Помощь консультанта</span></a>
		</div>

	</form>

</div>
<!-- / self w end -->
