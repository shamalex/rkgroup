
<h1 class="ttl1">Ошибка 404. Увы, такой страницы не существует!</h1>
<p>Возможно, эта страница была удалена, переименована, или она временно недоступна.</p>
<p><strong>Попробуйте следующее:</strong></p>
<!-- ul + green circles -->
<ul class="circle">
	<li class="sngl"><span class="gr"><b>1</b></span><span>Перейти на <a href="/">главную страницу</a> или воспользоваться <a href="/catalog/">каталогом товаров</a>.</span></li>
	<li class="sngl"><span class="gr"><b>2</b></span><span>Воспользуйтесь поиском для нахождения нужной Вам информации на сайте.</span></li>
</ul>
<!-- / ul + green circles end -->
