<?
include $_SERVER['DOCUMENT_ROOT'].'/bitrix/modules/main/include/prolog_before.php';
?>
<!-- ordercancel popup -->
<div class="pop pop-ordercancel pop-bg-1" id="popOrdercancel">
	<div class="w">
		<a href="#" class="x">&nbsp;</a>
		<h1 class="ttl">Запрос на отмену заказа</h1>
		<div class="feed-form">
			<form action="/send_ordercancel.php" autocomplete="off" enctype="multipart/form-data" method="post">
				<input type="text" name="subj" class="feed-field" id="ordermessageSubj" value="" />
				<input type="hidden" name="orderId" value="" />
				<?/*<div class="btn feed-btn test-ordercancel-pop-ok" id="ordercancelBtn">Отправить запрос</div>*/?>
			</form>
		</div>
	</div>
</div>
<!-- / ordercancel popup end -->