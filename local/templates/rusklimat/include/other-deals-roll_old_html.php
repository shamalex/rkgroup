
<!-- other deals roll -->
<div class="roll">
	<h2 class="ttl-r"><?= !empty($GLOBALS['OTHER_DEALS_ROLL_NAME']) ? $GLOBALS['OTHER_DEALS_ROLL_NAME'] : "Популярные товары" ?></h2>
	<!-- other deals -->
	<div class="other-deals c">
		<!-- slides -->
		<div class="slides c" id="<?= !empty($GLOBALS['OTHER_DEALS_ROLL_ID']) ? $GLOBALS['OTHER_DEALS_ROLL_ID'] : "other" ?>">

			<!-- item w/stamps -->
			<div class="item b-base">
				<div class="w c">

					<a href="/catalog/warm-floor/eefm_2_150_1/" class="lnk">
						<div class="ttl">Мат нагревательный</div>
						<div class="pic" style="background-image:url('/upload/iblock/b08/20738_23261.png');"></div>
						<div class="stamps c">
							<div class="ico ico-1">Инновационная текстильная основа</div>
							<div class="ico ico-2">Гарантия 20 лет</div>
						</div>
						<div class="dsc">Electrolux EEFM 2-150-1</div>
					</a>
					<div class="prc">
						<div class="curr curr-only"> 5 816 руб.</div>
					</div>
					<a class="btn btn-to-bsk" href="/catalog/seriya-easy-fix-mat/mat-nagrevatelnyy-electrolux-eefm-2-150-1-komplekt-teplogo-pola/?action=ADD2BASKET&add_id=18952" id="bx_117848907_18952_add_basket_link_block2">Купить</a>

					<script>
						$('#bx_117848907_18952_add_basket_link_block2').on('click', function() {
							window.dataLayer = window.dataLayer || [];
							window.dataLayer.push({
								'event': 'addToCart',
								"ecommerce": {
									"currencyCode": "RUB",
									"add": {
										"products": [
											{
												"id": "20737",
												"name" : "Мат нагревательный Electrolux EEFM 2-150-1 (комплект теплого пола)",
												"price": 4570.00,
												"brand": "Electrolux",
												"category": "Теплые полы/Нагревательные маты Electrolux/Серия EASY FIX MAT",
												"quantity": 1
											}
										]
									}
								}
							});
						});
					</script>

				</div>
			</div>
			<!-- / item w/stamps -->

			<!-- item w/stamps -->
			<div class="item b-base">
				<div class="w c">

					<a href="/catalog/radiators/royal_thermo_pianoforte_500_noir_sable_10_sekts/" class="lnk">
						<div class="ttl">Биметаллический дизайн-радиатор</div>
						<div class="pic" style="background-image:url('/upload/iblock/816/22698_27132_a.png');"></div>
						<div class="stamps c">
							<div class="ico ico-3">Мощность каждой секции<br>увеличена на 3-5%</div>
						</div>
						<div class="dsc">Royal Thermo PianoForte 500/Noir Sable - 10 секц.</div>
					</a>
					<div class="prc">
						<div class="curr curr-only">13 880 руб.</div>
					</div>
					<a class="btn btn-to-bsk" href="/catalog/radiatory-royal-thermo-pianoforte/radiator-royal-thermo-pianoforte-500-noir-sable-10-sekts/?action=ADD2BASKET&add_id=21325" id="bx_117848907_21325_add_basket_link_block2">Купить</a>

					<script>
						$('#bx_117848907_21325_add_basket_link_block2').on('click', function() {
							window.dataLayer = window.dataLayer || [];
							window.dataLayer.push({
								'event': 'addToCart',
								"ecommerce": {
									"currencyCode": "RUB",
									"add": {
										"products": [
											{
												"id": "22697",
												"name" : "Радиатор Royal Thermo PianoForte 500/Noir Sable - 10 секц.",
												"price": 12719.00,
												"brand": "RoyalThermo",
												"category": "Радиаторы/Секционные биметаллические радиаторы/Радиаторы Royal Thermo PianoForte",
												"quantity": 1
											}
										]
									}
								}
							});
						});
					</script>

				</div>
			</div>
			<!-- / item w/stamps -->

			<!-- item w/stamps -->
			<div class="item b-base">
				<div class="w c">

					<a href="/catalog/elecrtic-fireplaces/mini/efp_m_5012w/" class="lnk">
						<div class="ttl">Электрический мини-камин</div>
						<div class="pic" style="background-image:url('/upload/iblock/43d/22961_27423_a.png');"></div>
						<div class="stamps c">
							<div class="ico ico-5">Занимает предельно мало места</div>
							<div class="ico ico-1">Реалистичный эффект пламени</div>
						</div>
						<div class="dsc">Electrolux EFP/M- 5012W</div>
					</a>
					<div class="prc">
						<div class="curr curr-only">3 999 руб.</div>
					</div>
					<a class="btn btn-to-bsk" href="/catalog/mini-kaminy/mini-kamin-electrolux-efp-m-5012w/?action=ADD2BASKET&add_id=21689" id="bx_117848907_21689_add_basket_link_block2">Купить</a>

					<script>
						$('#bx_117848907_21689_add_basket_link_block2').on('click', function() {
							window.dataLayer = window.dataLayer || [];
							window.dataLayer.push({
								'event': 'addToCart',
								"ecommerce": {
									"currencyCode": "RUB",
									"add": {
										"products": [
											{
												"id": "22961",
												"name" : "Мини-камин Electrolux EFP/M- 5012W",
												"price": 3999.00,
												"brand": "Electrolux",
												"category": "Электрические камины/Мини-камины",
												"quantity": 1
											}
										]
									}
								}
							});
						});
					</script>

				</div>
			</div>
			<!-- / item w/stamps -->

			<!-- 3 items end -->

			<!-- item w/stamps -->
			<div class="item b-base">
				<div class="w c">

					<a href="/catalog/household-heaters/fan-heaters/nastennye_teploventilyatory_electrolux/efh_w_7020/" class="lnk">
						<div class="ttl">Настенный тепловентилятор</div>
						<div class="pic" style="background-image:url('/upload/iblock/606/21833_25049_a.png');"></div>
						<div class="stamps c">
							<div class="ico ico-1">Класс пылевлагозащищенности IP24</div>
							<!-- <div class="ico ico-2 ico-r">5 лет гарантии</div> -->
						</div>
						<div class="dsc">Electrolux EFH/W - 7020</div>
					</a>
					<div class="prc">
						<div class="curr curr-only">4 650 руб.</div>
					</div>
					<a class="btn btn-to-bsk" href="/catalog/nastennye-teploventilyatory-electrolux/nastennyy-teploventilyator-electrolux-efh-w-7020/?action=ADD2BASKET&add_id=21252" id="bx_117848907_21252_add_basket_link_block2">Купить</a>

					<script>
						$('#bx_117848907_21252_add_basket_link_block2').on('click', function() {
							window.dataLayer = window.dataLayer || [];
							window.dataLayer.push({
								'event': 'addToCart',
								"ecommerce": {
									"currencyCode": "RUB",
									"add": {
										"products": [
											{
												"id": "21833",
												"name" : "Настенный тепловентилятор Electrolux EFH/W - 7020",
												"price": 3750.00,
												"brand": "Electrolux",
												"category": "Тепловентиляторы/Тепловентиляторы Electrolux/Настенные тепловентиляторы Electrolux",
												"quantity": 1
											}
										]
									}
								}
							});
						});
					</script>

				</div>
			</div>
			<!-- / item w/stamps -->

			<!-- item w/stamps -->
			<div class="item b-base">
				<div class="w c">

					<a href="/catalog/household-heaters/fan-heaters/efh_c_5120/" class="lnk">
						<div class="ttl">Тепловентилятор</div>
						<div class="pic" style="background-image:url('/upload/iblock/290/21593_24627_a.png');"></div>
						<div class="stamps c">
							<div class="ico ico-3">Дизайнерская<br>ART-серия</div>
								<div class="ico ico-2 ico-r">Не сушит<br>воздух</div>
						</div>
						<div class="dsc">Electrolux EFH/C-5120</div>
						</a>
					<div class="prc">
						<div class="curr curr-only">1 599 руб.</div>
					</div>
					<a class="btn btn-to-bsk" href="/catalog/nastolnye-teploventilyatory-electrolux/teploventilyator-electrolux-efh-c-5120/?action=ADD2BASKET&add_id=11243" id="bx_117848907_11243_add_basket_link_block2">Купить</a>

					<script>
						$('#bx_117848907_11243_add_basket_link_block2').on('click', function() {
							window.dataLayer = window.dataLayer || [];
							window.dataLayer.push({
								'event': 'addToCart',
								"ecommerce": {
									"currencyCode": "RUB",
									"add": {
										"products": [
											{
												"id": "21593",
												"name" : "Тепловентилятор Electrolux EFH/C-5120",
												"price": 1599.00,
												"brand": "Electrolux",
												"category": "Тепловентиляторы/Тепловентиляторы Electrolux/Настольные тепловентиляторы Electrolux",
												"quantity": 1
											}
										]
									}
								}
							});
						});
					</script>

				</div>
			</div>
			<!-- / item w/stamps -->

			<!-- item w/stamps -->
			<div class="item b-base">
				<div class="w c">

					<a href="/catalog/humidifier-and-dehumidifiers/sport-line/uvlazhniteli_vozdukha_ballu_disney/uhb_240_yellow_zheltyy/" class="lnk">
						<div class="ttl">Увлажнитель ультразвуковой</div>
						<div class="pic" style="background-image:url('/upload/iblock/88e/24351_29819_a.png');"></div>
						<div class="stamps c">
							<div class="ico ico-5">Встроенный светильник – ночник</div>
							<div class="ico ico-1 ico-r">Автоотключение при низком уровне воды</div>
						</div>
						<div class="dsc">Ballu UHB-240 yellow / желтый Disney</div>
					</a>
					<div class="prc">
						<div class="curr curr-only">4 108 руб.</div>
					</div>
					<a class="btn btn-to-bsk" href="/catalog/uvlazhniteli-vozdukha-ballu-disney/uvlazhnitel-ultrazvukovoy-ballu-uhb-240-yellow-zheltyy-disney/?action=ADD2BASKET&add_id=22931" id="bx_117848907_22931_add_basket_link_block2">Купить</a>

					<script>
						$('#bx_117848907_22931_add_basket_link_block2').on('click', function() {
							window.dataLayer = window.dataLayer || [];
							window.dataLayer.push({
								'event': 'addToCart',
								"ecommerce": {
									"currencyCode": "RUB",
									"add": {
										"products": [
											{
												"id": "24351",
												"name" : "Увлажнитель ультразвуковой  Ballu UHB-240 yellow / желтый Disney",
												"price": 3313.00,
												"brand": "Ballu",
												"category": "Увлажнители и очистители воздуха/Увлажнители воздуха Ballu Kids/Увлажнители воздуха Ballu Disney",
												"quantity": 1
											}
										]
									}
								}
							});
						});
					</script>

				</div>
			</div>
			<!-- / item w/stamps -->

		</div>
		<!-- / slides end -->
	</div>
	<!-- / other deals end -->
</div>
<!-- / other deals roll end -->
