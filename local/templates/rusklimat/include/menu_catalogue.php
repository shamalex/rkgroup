<!-- catalogue menu with dropdowns  -->
<ul class="mn-cat" id="menuCatalogue">

	<!-- dropdown menu items -->
	
	<!-- Кондиционеры Вентиляторы -->
	<li class="mn-cat-cond" data-submenu-id="submenu-cond">
		<a href="#">Кондиционеры<br/>Вентиляторы</a>
		
		<!-- lvl2 menu -->
		<div class="lvl2" id="submenu-cond">
			<!-- shadow -->
			<div class="shd">
				<div class="w c">
				
					<!-- item 1 -->
					<div class="item">
						<div class="ttl"><a href="/catalog/air-conditioner/">Кондиционеры</a></div>
						<div class="ttl2"><a href="/catalog/air-conditioner/">Кондиционеры</a></div>
						<ul class="list">
							<li><a href="/catalog/air-conditioner/mobile-type-air-conditioners/">Мобильные кондиционеры</a></li>
							<li><a href="/catalog/air-conditioner/split-type-systems/">Сплит-системы</a></li>
							<li><a href="/catalog/air-conditioner/inverter-split-type-systems/">Сплит-системы с инверторным управлением</a></li>
							<li><a href="/catalog/air-conditioner/mr_slim_puh-p/">Мульти сплит-системы с инверторным управлением</a></li>
							<li><a href="/catalog/air-conditioner/floor-standing-split-systems/">Колонные сплит системы</a></li>
							<li><a href="/catalog/napolno-potolochnye-split-sistemy/">Напольно-потолочные сплит системы</a></li>
							<li><a href="/catalog/kassetnye-split-sistemy/">Кассетные сплит системы</a></li>
						</ul>
					</div>
					<!-- / item 1 -->
					
					<!-- item 2 -->
					<div class="item">
						<div class="ttl">&nbsp;</div>
						<div class="ttl2"><a href="/catalog/accessories-for-air-conditioner/">Доп. элементы для монтажа кондиционеров</a></div>
						<ul class="list">
							<li><a href="/catalog/accessories-for-air-conditioner/truba-majdanpek/">Медные трубы</a></li>
							<li><a href="/catalog/accessories-for-air-conditioner/insulation-termafleh/">Теплоизоляция</a></li>
							<li><a href="/catalog/accessories-for-air-conditioner/protective-visors/">Кронштейны и металлоконструкции</a></li>
							<li><a href="/catalog/accessories-for-air-conditioner/freon/">Фреон</a></li>
							<li><a href="/catalog/accessories-for-air-conditioner/pump-drainage/">Насос дренажный</a></li>
							<li><a href="/catalog/accessories-for-air-conditioner/winter-cond/">Комплект зимний</a></li>
						</ul>
					</div>
					<!-- / item 2 -->
					
					<!-- item 3 -->
					<div class="item">
						<div class="ttl"><a href="/catalog/air-ventilation-system/">Вентиляторы</a></div>
						
						<div class="ttl2"><a href="/catalog/household-fans-vortice/">Бытовые вентиляторы</a></div>
						<ul class="list">
							<li><a href="/catalog/household-fans-vortice/axial-fan/">Вытяжные бытовые вентиляторы</a></li>
						</ul>
						
						<div class="ttl2"><a href="/catalog/air-ventilation-system/">Вентиляторы</a></div>
						<ul class="list">
							<li><a href="/catalog/air-ventilation-system/wind/">Канальные вентиляторы</a></li>
							<li><a href="/catalog/air-ventilation-system/soundproofed-channel-fans/">Звукоизолированные канальные вентиляторы</a></li>
							<li><a href="/catalog/air-ventilation-system/round-duct-fans-wall/">Вытяжные осевые вентиляторы</a></li>
						</ul>
					</div>
					<!-- / item 3 -->
				
				</div>
				<!-- / shadow > .w end -->
			</div>
			<!-- / shadow end -->
		</div>
		<!-- / lvl2 end -->
	</li>
	<!-- / Кондиционеры Вентиляторы -->
	
	<!-- Увлажнители Осушители -->
	<li class="mn-cat-hum" data-submenu-id="submenu-hum">
		<a href="#">Увлажнители<br/>Осушители</a>
		<!-- lvl2 menu -->
		<div class="lvl2" id="submenu-hum">
			<!-- shadow -->
			<div class="shd">
				<div class="w c">
				
					<!-- item 1 -->
					<div class="item">
						<div class="ttl"><a href="/catalog/humidifier-and-dehumidifiers/">Увлажнители и очистители воздуха</a></div>
						<div class="ttl2"><a href="/catalog/humidifier-and-dehumidifiers/">Увлажнители и очистители воздуха</a></div>
						<ul class="list">
							<li><a href="/catalog/humidifier-and-dehumidifiers/ultrasonic-humidifiers/">Увлажнители воздуха</a></li>
							<li><a href="/catalog/humidifier-and-dehumidifiers/sport-line/">Увлажнители воздуха Ballu Kids</a></li>
							<li><a href="/catalog/humidifier-and-dehumidifiers/water/">Мойки воздуха</a></li>
							<li><a href="/catalog/humidifier-and-dehumidifiers/air-cleaner/">Воздухоочистители</a></li>
							<li><a href="/catalog/humidifier-and-dehumidifiers/climate-complex/">Климатические комплексы</a></li>
						</ul>
					</div>
					<!-- / item 1 -->
					
					<!-- item 2 -->
					<div class="item">
						<div class="ttl"><a href="/catalog/air-dryings/">Осушители воздуха</a></div>
						<div class="ttl2"><a href="/catalog/air-dryings/">Осушители воздуха</a></div>
						<ul class="list">
							<li><a href="/catalog/air-dryings/air-dryings/">Осушители воздуха</a></li>
							<li><a href="/catalog/air-dryings/air-dryings-complex/">Сушильные комплексы</a></li>
						</ul>
						<div class="ttl"><a href="/catalog/hand-dryers/">Сушилки для рук</a></div>
						<div class="ttl2"><a href="/catalog/hand-dryers/">Сушилки для рук</a></div>
						<ul class="list">
							<li><a href="/catalog/hand-dryers/electrolux/">Сушилки для рук Electrolux</a></li>
							<li><a href="/catalog/hand-dryers/modern/">Сушилки для рук Ballu</a></li>
						</ul>
					</div>
					<!-- / item 2 -->
					
					<!-- item 3 -->
					<div class="item">
						<div class="ttl"><a href="/catalog/bath-convector/">Электрические полотенцесушители</a></div>
						<div class="ttl2"><a href="/catalog/bath-convector/">Электрические полотенцесушители</a></div>
						<ul class="list">
							<li><a href="/catalog/bath-convector/corelia/">Электрические полотенцесушители Noirot, серия Corelia</a></li>
							<li><a href="/catalog/bath-convector/tcaey-thaey-67-315/">Электрические полотенцесушители Noirot, серия Helios</a></li>
						</ul>
					</div>
					<!-- / item 3 -->
				
				</div>
				<!-- / shadow > .w end -->
			</div>
			<!-- / shadow end -->
		</div>
		<!-- / lvl2 menu end -->
	</li>
	<!-- / Увлажнители Осушители -->
	
	<!--  Обогреватели Камины -->
	<li class="mn-cat-heat" data-submenu-id="submenu-heat">
		<a href="#">Обогреватели<br/>Камины</a>
		<!-- lvl2 menu -->
		<div class="lvl2" id="submenu-heat">
			<!-- shadow -->
			<div class="shd">
				<div class="w c">
				
					<!-- item 1 -->
					<div class="item">
						<div class="ttl"><a href="/catalog/convector-heaters/">Электрические обогреватели</a></div>
						<div class="ttl2"><a href="/catalog/poluprom-ik/">Инфракрасные обогреватели</a></div>
						<ul class="list">
							<li><a href="/catalog/poluprom-ik/electric-infrared/">Электрические инфракрасные обогреватели</a></li>
							<li><a href="/catalog/poluprom-ik/ballu-ik-bigh/">Газовые инфракрасные обогреватели</a></li>
						</ul>
						<div class="ttl2"><a href="/catalog/convector-heaters/">Электрические обогреватели</a></div>
						<ul class="list">
							<li><a href="/catalog/convector-heaters/convective/">Электрические обогреватели (конвекторы)</a></li>
							<li><a href="/catalog/convector-heaters/convective-infra-red/">Конвективно-инфракрасные электрические обогреватели (конвекторы)</a></li>
							<li><a href="/catalog/convector-heaters/programmator/">Программаторы и аксессуары</a></li>
						</ul>
					</div>
					<!-- / item 1 -->
					
					<!-- item 2 -->
					<div class="item">
						<div class="ttl">&nbsp;</div>
						<div class="ttl2"><a href="/catalog/household-heaters/">Тепловентиляторы</a></div>
						<ul class="list">
							<li><a href="/catalog/household-heaters/ballu/">Тепловентиляторы Ballu</a></li>
							<li><a href="/catalog/household-heaters/fan-heaters/">Тепловентиляторы Electrolux</a></li>
						</ul>
						<div class="ttl2"><a href="/catalog/oil-radiator/">Масляные радиаторы</a></div>
						<ul class="list">
							<li><a href="/catalog/oil-radiator/electrolux/">Масляные радиаторы Electrolux</a></li>
							<li><a href="/catalog/oil-radiator/air-dryings/">Масляные радиаторы Ballu</a></li>
						</ul>
					</div>
					<!-- / item 2 -->
					
					<!-- item 3 -->
					<div class="item">
						<div class="ttl"><a href="/catalog/elecrtic-fireplaces/">Электрические камины</a></div>
						<div class="ttl2"><a href="/catalog/elecrtic-fireplaces/">Электрические камины</a></div>
						<ul class="list">
							<li><a href="/catalog/elecrtic-fireplaces/mini/">Мини-камины</a></li>
							<li><a href="/catalog/elecrtic-fireplaces/floor/">Напольные камины</a></li>
							<li><a href="/catalog/elecrtic-fireplaces/wall/">Настенные камины</a></li>
							<li><a href="/catalog/elecrtic-fireplaces/portal/">Портальные камины</a></li>
							<li><a href="/catalog/elecrtic-fireplaces/ochag/">Очаги для портальных каминов</a></li>
							<li><a href="/catalog/elecrtic-fireplaces/portal-only/">Порталы</a></li>
						</ul>
					</div>
					<!-- / item 3 -->
				
				</div>
				<!-- / shadow > .w end -->
			</div>
			<!-- / shadow end -->
		</div>
		<!-- / lvl2 menu end -->
	</li>
	<!--  Обогреватели Камины -->
	
	<!--  Пушки Завесы -->
	<li class="mn-cat-can" data-submenu-id="submenu-can">
		<a href="#">Пушки<br/>Завесы</a>
		<!-- lvl2 menu -->
		<div class="lvl2" id="submenu-can">
			<!-- shadow -->
			<div class="shd">
				<div class="w c">
				
					<!-- item 1 -->
					<div class="item">
						<div class="ttl"><a href="/catalog/teplovie-zavesi/">Тепловые завесы</a></div>
						<div class="ttl2"><a href="/catalog/teplovie-zavesi/">Тепловые завесы и водяные тепловентиляторы</a></div>
						<ul class="list">
							<li><a href="/catalog/teplovie-zavesi/teplovie-zavesi/">Электрические тепловые завесы</a></li>
							<li><a href="/catalog/teplovie-zavesi/water/">Водяные тепловые завесы</a></li>
							<li><a href="/catalog/teplovie-zavesi/water-fan-heaters/">Водяные тепловентиляторы</a></li>
						</ul>
					</div>
					<!-- / item 1 -->
					
					<!-- item 2 -->
					<div class="item">
						<div class="ttl"><a href="/catalog/teplovie-pushki-zavesi/">Тепловые пушки</a></div>
						<div class="ttl2"><a href="/catalog/teplovie-pushki-zavesi/">Тепловые пушки и теплогенераторы</a></div>
						<ul class="list">
							<li><a href="/catalog/teplovie-pushki-zavesi/teplovie-pushki/">Электрические тепловые пушки</a></li>
							<li><a href="/catalog/teplovie-pushki-zavesi/dizel-pr/">Дизельные тепловые пушки прямого нагрева</a></li>
							<li><a href="/catalog/teplovie-pushki-zavesi/disel-pushka/">Дизельные тепловые пушки непрямого нагрева</a></li>
							<li><a href="/catalog/teplovie-pushki-zavesi/dizel-infrared/">Дизельные нагреватели инфракрасного излучения</a></li>
							<li><a href="/catalog/teplovie-pushki-zavesi/teplogenerator-dizel/">Теплогенераторы на дизельном топливе</a></li>
							<li><a href="/catalog/teplovie-pushki-zavesi/gas-pushka/">Газовые тепловые пушки</a></li>
							<li><a href="/catalog/teplovie-pushki-zavesi/teplogenerator-gaz/">Теплогенераторы на сжиженном газе</a></li>
							<li><a href="/catalog/teplovie-pushki-zavesi/teplogenerator-prir-gaz/">Теплогенераторы на природном газе</a></li>
							<li><a href="/catalog/teplovie-pushki-zavesi/accessories-ballu-biemmedue/">Аксессуары для теплового оборудования Ballu-Biemmedue</a></li>
						</ul>
					</div>
					<!-- / item 2 -->
				
				</div>
				<!-- / shadow > .w end -->
			</div>
			<!-- / shadow end -->
		</div>
		<!-- / lvl2 menu end -->
	</li>
	<!-- /  Пушки Завесы -->
	
	<!--  Теплый пол -->
	<li class="mn-cat-flo" data-submenu-id="submenu-flo">
		<a href="#">Теплый<br/>пол</a>
		<!-- lvl2 menu -->
		<div class="lvl2" id="submenu-flo">
			<!-- shadow -->
			<div class="shd">
				<div class="w c">
				
					<!-- item 1 -->
					<div class="item">
						<div class="ttl"><a href="/catalog/warm-floor/">Теплые полы</a></div>
						<div class="ttl2"><a href="/catalog/warm-floor/">Теплые полы</a></div>
						<ul class="list">
							<li><a href="/catalog/warm-floor/heating-mats-electrolux/">Нагревательные маты Electrolux</a></li>
							<li><a href="/catalog/warm-floor/heating-sections-electrolux/">Нагревательные секции Electrolux</a></li>
							<li><a href="/catalog/warm-floor/thermoregulations-electrolux/">Терморегуляторы Electrolux</a></li>
							<li><a href="/catalog/warm-floor/panels/">Сменные цветные панели для терморегуляторов</a></li>
						</ul>
					</div>
					<!-- / item 1 -->
	
				</div>
				<!-- / shadow > .w end -->
			</div>
			<!-- / shadow end -->
		</div>
		<!-- / lvl2 menu end -->
	</li>
	<!-- / Теплый пол -->
	
	<!-- Водонагреватели Котлы -->
	<li class="mn-cat-wat" data-submenu-id="submenu-wat">
		<a href="#">Водонагреватели<br/>Котлы</a>
		<!-- lvl2 menu -->
		<div class="lvl2" id="submenu-wat">
			<!-- shadow -->
			<div class="shd">
				<div class="w c">
				
					<!-- item 1 -->
					<div class="item">
						<div class="ttl"><a href="/catalog/water-heaters/">Водонагреватели</a></div>
						<div class="ttl2"><a href="/catalog/water-heaters/">Водонагреватели</a></div>
						<ul class="list">
							<li><a href="/catalog/water-heaters/electrical-accumulative-water-heaters/">Электрические накопительные водонагреватели</a></li>
							<li><a href="/catalog/water-heaters/instant-water-heaters/">Электрические проточные водонагреватели</a></li>
							<li><a href="/catalog/water-heaters/gas-water-heaters/">Газовые колонки</a></li>
						</ul>
						<div class="ttl"><a href="/catalog/tubing-fittings/">Трубы и фитинги</a></div>
						<div class="ttl2"><a href="/catalog/tubing-fittings/">Трубы и фитинги</a></div>
						<ul class="list">
							<li><a href="/catalog/sistema-truboprovodov-royal-thermo-axiopress/">Система трубопроводов Royal Thermo AXIOpress</a></li>
						</ul>
					</div>
					<!-- / item 1 -->
					
					<!-- item 2 -->
					<div class="item">
						<div class="ttl"><a href="/catalog/boilers/">Котлы</a></div>
						<div class="ttl2"><a href="/catalog/boilers/">Котлы</a></div>
						<ul class="list">
							<li><a href="/catalog/boilers/wall-boilers/">Настенные газовые котлы</a></li>
							<li><a href="/catalog/boilers/wall-condensation-boilers/">Настенные конденсационные котлы</a></li>
							<li><a href="/catalog/boilers/atmospheric-floor-boilers/">Напольные атмосферные котлы</a></li>
							<li><a href="/catalog/boilers/pressurization-floor-castiron-boilers/">Напольные наддувные чугунные котлы</a></li>
							<li><a href="/catalog/boilers/gas-condensate-floor-boilers/">Напольные газовые конденсационные котлы</a></li>
							<li><a href="/catalog/boilers/household-floor-boilers/">Котлы бытовые напольные</a></li>
							<li><a href="/catalog/boilers/tt-boilers/">Твердотопливные котлы</a></li>
							<li><a href="/catalog/boilers/electric-boilers/">Электрические котлы</a></li>
							<li><a href="/catalog/boilers/automatic/">Автоматика</a></li>
						</ul>
					</div>
					<!-- / item 2 -->
					
					<!-- item 3 -->
					<div class="item">
						<div class="ttl"><a href="/catalog/heating-mountings/">Арматура для систем отопления</a></div>
						<div class="ttl2"><a href="/catalog/heating-mountings/">Арматура для систем отопления</a></div>
						<ul class="list">
							<li><a href="/catalog/sharovye-krany-royal-thermo-dlya-vody/">Шаровые краны Royal Thermo для воды</a></li>
							<li><a href="/catalog/sharovye-krany-royal-thermo-dlya-gaza/">Шаровые краны Royal Thermo для газа</a></li>
							<li><a href="/catalog/sharovye-krany-royal-thermo-dlya-tyazhelykh-usloviy-ekspluatatsii/">Шаровые краны Royal Thermo для тяжелых условий эксплуатации</a></li>
							<li><a href="/catalog/sharovye-krany-royal-thermo-dlya-podklyucheniya-bytovoy-tekhniki/">Шаровые краны Royal Thermo для подключения бытовой техники</a></li>
							<li><a href="/catalog/fitingi-royal-thermo/">Фитинги Royal Thermo</a></li>
							<li><a href="/catalog/ventili-radiatornye-royal-thermo/">Вентили радиаторные Royal Thermo</a></li>
							<li><a href="/catalog/ventili-termostaticheskie-royal-thermo/">Вентили термостатические Royal Thermo</a></li>
							<li><a href="/catalog/uzly-smesitelnye-royal-thermo/">Узлы смесительные Royal Thermo</a></li>
							<li><a href="/catalog/kollektory/">Коллекторы</a></li>
							<li><a href="/catalog/klapany-obratnye-i-filtry-kosye/">Клапаны обратные и фильтры косые</a></li>
						</ul>
					</div>
					<!-- / item 3 -->
				
				</div>
				<!-- / shadow > .w end -->
			</div>
			<!-- / shadow end -->
		</div>	
		<!-- / lvl2 menu end -->
	</li>
	<!-- Водонагреватели Котлы -->
	
	<!-- Радиаторы отопления -->
	<li class="mn-cat-rad" data-submenu-id="submenu-rad">
		<a href="#">Радиаторы<br/>отопления</a>
		<!-- lvl2 menu -->
		<div class="lvl2" id="submenu-rad">
			<!-- shadow -->
			<div class="shd">
				<div class="w c">
				
					<!-- item 1 -->
					<div class="item">
						<div class="ttl"><a href="/catalog/radiators/">Радиаторы отопления</a></div>
						<div class="ttl2"><a href="/catalog/radiators/">Радиаторы</a></div>
						<ul class="list">
							<li><a href="/catalog/radiators/aluminium-radiators/">Секционные алюминиевые радиаторы</a></li>
							<li><a href="/catalog/radiators/bimetallic-radiators/">Секционные биметаллические радиаторы</a></li>
							<li><a href="/catalog/radiators/access-sections/">Аксессуары для секционных радиаторов</a></li>
							<li><a href="/catalog/radiators/tubular-heaters/">Трубчатые радиаторы Dia Norm LaserDelta</a></li>
							<li><a href="/catalog/radiators/panel-radiators/">Стальные панельные радиаторы</a></li>
						</ul>
					</div>
					<!-- / item 1 -->
					
					<!-- item 2 -->
					<div class="item">
						<div class="ttl"><a href="/catalog/tubing-fittings/">Трубы и фитинги</a></div>
						<div class="ttl2"><a href="/catalog/tubing-fittings/">Трубы и фитинги</a></div>
						<ul class="list">
							<li><a href="/catalog/sistema-truboprovodov-royal-thermo-axiopress/">Система трубопроводов Royal Thermo AXIOpress</a></li>
						</ul>
					</div>
					<!-- / item 2 -->
					
					<!-- item 3 -->
					<div class="item">
						<div class="ttl"><a href="/catalog/heating-mountings/">Арматура для систем отопления</a></div>
						<div class="ttl2"><a href="/catalog/heating-mountings/">Арматура для систем отопления</a></div>
						<ul class="list">
							<li><a href="/catalog/sharovye-krany-royal-thermo-dlya-vody/">Шаровые краны Royal Thermo для воды</a></li>
							<li><a href="/catalog/sharovye-krany-royal-thermo-dlya-gaza/">Шаровые краны Royal Thermo для газа</a></li>
							<li><a href="/catalog/sharovye-krany-royal-thermo-dlya-tyazhelykh-usloviy-ekspluatatsii/">Шаровые краны Royal Thermo для тяжелых условий эксплуатации</a></li>
							<li><a href="/catalog/sharovye-krany-royal-thermo-dlya-podklyucheniya-bytovoy-tekhniki/">Шаровые краны Royal Thermo для подключения бытовой техники</a></li>
							<li><a href="/catalog/fitingi-royal-thermo/">Фитинги Royal Thermo</a></li>
							<li><a href="/catalog/ventili-radiatornye-royal-thermo/">Вентили радиаторные Royal Thermo</a></li>
							<li><a href="/catalog/ventili-termostaticheskie-royal-thermo/">Вентили термостатические Royal Thermo</a></li>
							<li><a href="/catalog/uzly-smesitelnye-royal-thermo/">Узлы смесительные Royal Thermo</a></li>
							<li><a href="/catalog/kollektory/">Коллекторы</a></li>
							<li><a href="/catalog/klapany-obratnye-i-filtry-kosye/">Клапаны обратные и фильтры косые</a></li>
						</ul>
					</div>
					<!-- / item 3 -->
				
				</div>
				<!-- / shadow > .w end -->
			</div>
			<!-- / shadow end -->
		</div>
		<!-- / lvl2 menu end -->
	</li>
	<!-- Радиаторы отопления -->
	
	<!-- Насосы водяные -->
	<li class="mn-cat-pum" data-submenu-id="submenu-pum">
		<a href="#">Насосы<br/>водяные</a>
		<!-- lvl2 menu -->
		<div class="lvl2" id="submenu-pum">
			<!-- shadow -->
			<div class="shd">
				<div class="w c">
				
					<!-- item 1 -->
					<div class="item">
						<div class="ttl"><a href="/catalog/pumps/">Насосы</a></div>
						<div class="ttl2"><a href="/catalog/pumps/">Насосы</a></div>
						<ul class="list">
							<li><a href="/catalog/pumps/circulation-pumps/">Циркуляционные насосы</a></li>
							<li><a href="/catalog/pumps/drain-pumps/">Насосы для дренажа и канализации</a></li>
							<li><a href="/catalog/pumps/borehole-pump/">Скважинные насосы</a></li>
							<li><a href="/catalog/pumps/sewage-installation/">Канализационные установки</a></li>
							<li><a href="/catalog/pumps/compact/">Компактные повысительные насосы</a></li>
							<li><a href="/catalog/pumps/pumps-stations/">Насосные станции</a></li>
							<li><a href="/catalog/pumps/submerged-pumps/">Погружные насосы для колодца</a></li>
							<li><a href="/catalog/pumps/automatics/">Автоматика и принадлежности</a></li>
						</ul>
					</div>
					<!-- / item 1 -->
					
					<!-- item 2 -->
					<div class="item">
						<div class="ttl"><a href="/catalog/tubing-fittings/">Трубы и фитинги</a></div>
						<div class="ttl2"><a href="/catalog/tubing-fittings/">Трубы и фитинги</a></div>
						<ul class="list">
							<li><a href="/catalog/sistema-truboprovodov-royal-thermo-axiopress/">Система трубопроводов Royal Thermo AXIOpress</a></li>
						</ul>
					</div>
					<!-- / item 2 -->
					
					<!-- item 3 -->
					<div class="item">
						<div class="ttl"><a href="/catalog/heating-mountings/">Арматура для систем отопления</a></div>
						<div class="ttl2"><a href="/catalog/heating-mountings/">Арматура для систем отопления</a></div>
						<ul class="list">
							<li><a href="/catalog/sharovye-krany-royal-thermo-dlya-vody/">Шаровые краны Royal Thermo для воды</a></li>
							<li><a href="/catalog/sharovye-krany-royal-thermo-dlya-gaza/">Шаровые краны Royal Thermo для газа</a></li>
							<li><a href="/catalog/sharovye-krany-royal-thermo-dlya-tyazhelykh-usloviy-ekspluatatsii/">Шаровые краны Royal Thermo для тяжелых условий эксплуатации</a></li>
							<li><a href="/catalog/sharovye-krany-royal-thermo-dlya-podklyucheniya-bytovoy-tekhniki/">Шаровые краны Royal Thermo для подключения бытовой техники</a></li>
							<li><a href="/catalog/fitingi-royal-thermo/">Фитинги Royal Thermo</a></li>
							<li><a href="/catalog/ventili-radiatornye-royal-thermo/">Вентили радиаторные Royal Thermo</a></li>
							<li><a href="/catalog/ventili-termostaticheskie-royal-thermo/">Вентили термостатические Royal Thermo</a></li>
							<li><a href="/catalog/uzly-smesitelnye-royal-thermo/">Узлы смесительные Royal Thermo</a></li>
							<li><a href="/catalog/kollektory/">Коллекторы</a></li>
							<li><a href="/catalog/klapany-obratnye-i-filtry-kosye/">Клапаны обратные и фильтры косые</a></li>
						</ul>
					</div>
					<!-- / item 3 -->
				
				</div>
				<!-- / shadow > .w end -->
			</div>
			<!-- / shadow end -->
		</div>
		<!-- / lvl2 menu end -->
	</li>
	<!-- / Насосы водяные -->
	
	<!-- Очистка воды -->
	<li class="mn-cat-pur" data-submenu-id="submenu-pur">
		<a href="#">Очистка<br/>воды</a>
		<!-- lvl2 menu -->
		<div class="lvl2" id="submenu-pur">
			<!-- shadow -->
			<div class="shd">
				<div class="w c">
				
					<!-- item 1 -->
					<div class="item">
						<div class="ttl"><a href="/catalog/water-purification-systems/">Системы очистки воды</a></div>
						<div class="ttl2"><a href="/catalog/water-purification-systems/">Системы очистки воды</a></div>
						<ul class="list">
							<li><a href="/catalog/water-purification-systems/atoll/">Бытовые системы очистки воды atoll</a></li>
							<li><a href="/catalog/water-purification-systems/zenner/">Фильтры Honeywell</a></li>
							<li><a href="/catalog/water-purification-systems/prom/">Магнитные преобразователи воды MWS</a></li>
						</ul>
					</div>
					<!-- / item 1 -->
					
				</div>
				<!-- / shadow > .w end -->
			</div>
			<!-- / shadow end -->
		</div>
		<!-- / lvl2 menu end -->
	</li>
	<!-- Очистка воды -->
	
	<!-- / dropdown menu items end -->
</ul>
<!-- / catalogue  menu with dropdowns end -->