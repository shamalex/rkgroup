<!-- callback popup, triggered by .callback-pop -->
<div class="pop pop-callback pop-bg-1" id="popCallback">
	<div class="w">
		<a href="#" class="x">&nbsp;</a>
		<h1 class="ttl">Обратный звонок</h1>
		<div class="call-form">
			<form action="/send_callback.php" autocomplete="off" enctype="multipart/form-data" method="post">
				<input type="text" name="phone" class="call-field" id="callbackPhone" placeholder="Ваш номер телефона" value="" />
				<span class="comment" data-name="phone"></span>
				<input type="text" name="subj" class="call-field" id="callbackSubj" value="" />
				<div class="btn call-btn test-callback-pop-ok" id="callbackBtn">Заказать обратный звонок</div>
			</form>
		</div>
	</div>
</div>
<!-- / callback popup end -->

<!-- subscribtion success popup, triggered by .sub-btn + success -->
<div class="pop pop-callback pop-bg-3" id="popSubYes">
	<div class="w">
		<a href="#" class="x">&nbsp;</a>
		<div class="ttl-p2">Вы успешно подписались<br/>на рассылку спецпредложений!</div>
		<p class="dsc" style="display: none;"><a href="#" class="test-sub-pop-fail">Неверно указан email.</a></p>
	</div>
</div>
<!-- / subscribtion success popup end -->

<!-- subscribtion fail popup, triggered by .sub-btn + fail -->
<div class="pop pop-callback pop-bg-2" id="popSubNo">
	<div class="w">
		<a href="#" class="x">&nbsp;</a>
		<div class="ttl-p2">Неверно указан адрес email,<br/>пожалуйста, повторите ввод!</div>
	</div>
</div>
<!-- / subscribtion fail popup end -->
