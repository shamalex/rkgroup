<?$APPLICATION->IncludeComponent(
	"bitrix:catalog.section.list",
	"articles_block",
	Array(
		"ADD_SECTIONS_CHAIN" => "N",
		"CACHE_GROUPS" => "Y",
		"CACHE_TIME" => "36000000",
		"CACHE_TYPE" => "A",
		"COMPONENT_TEMPLATE" => "articles_block",
		"COUNT_ELEMENTS" => "Y",
		"IBLOCK_ID" => "7",
		"IBLOCK_TYPE" => "articles",
		"SECTION_CODE" => "",
		"SECTION_FIELDS" => array("NAME","PICTURE","DESCRIPTION"),
		"SECTION_ID" => "",
		"SECTION_URL" => "",
		"SECTION_USER_FIELDS" => array("",""),
		"SHOW_PARENT_NAME" => "N",
		"TOP_DEPTH" => "1",
		"VIEW_MODE" => "LINE"
	)
);?>