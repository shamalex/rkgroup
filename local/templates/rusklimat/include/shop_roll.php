<!-- shop roll -->
<div class="roll">
	<h2 class="ttl-r">Не просто магазин</h2>
	 <!-- shop item container -->
	<div class="shop c">
		 <!-- item 1 --> <a href="/catalog/" class="item">
		<div class="w">
			<div class="ico ico-1">
				 &nbsp;
			</div>
			<h2 class="ttl">Собственное производство</h2>
			<p class="dsc">
				 Мы производим климатическую технику на своих предприятиях
			</p>
		</div>
 </a>
		<!-- / item 1 --> <!-- item 2 --> <a href="/special/" class="item pic"> <img src="/bitrix/templates/rusklimat/i/blocks/pic-shop-1.png">
		<div class="w">
			<h2 class="ttl">15 лет на рынке климатических услуг</h2>
			<p class="dsc">
				 53 филиала по всей России
			</p>
		</div>
 </a>
		<!-- / item 2 --> <!-- item 3 --> <a href="/personal/" class="item">
		<div class="w">
			<div class="ico ico-2">
				 &nbsp;
			</div>
			<h2 class="ttl">техническая <br>
			 поддержка</h2>
			<p class="dsc">
				 Сервисное обслуживание клиентов и тех. поддержка
			</p>
		</div>
 </a>
		<!-- / item 3 --> <!-- item 4 --> <a href="/catalog/" class="item pic"> <img src="/bitrix/templates/rusklimat/i/blocks/pic-shop-2.png">
		<div class="w">
			<h2 class="ttl">Монтаж в городе</h2>
			<p class="dsc">
				 Осуществляем непосредственно монтаж оборудования в вашем городе
			</p>
		</div>
 </a>
		<!-- / item 4 --> <!-- item 5 --> <a href="/catalog/" class="item pic"> <img src="/bitrix/templates/rusklimat/i/blocks/pic-shop-3.png">
		<div class="w">
			<h2 class="ttl">члены сообществ<br>
			 Апик, Авок</h2>
			<p class="dsc">
				 Являемся постоянными членами профессиональных сообществ
			</p>
		</div>
 </a>
		<!-- / item 5 --> <!-- item 6 --> <a href="/catalog/" class="item">
		<div class="w">
			<div class="ico ico-3">
				 &nbsp;
			</div>
			<h2 class="ttl">Собственный склад</h2>
			<p class="dsc">
				 В каждом городе имеем свои склады продукции
			</p>
		</div>
 </a>
		<!-- / item 6 --> <!-- item 7 --> <a href="/catalog/" class="item pic"> <img src="/bitrix/templates/rusklimat/i/blocks/pic-shop-4.png">
		<div class="w">
			<h2 class="ttl">Доставка по городу<br>
			 за 2-3 дня</h2>
			<p class="dsc">
				 Наша доставка работает профессионально, быстро
			</p>
		</div>
 </a>
		<!-- / item 7 --> <!-- item 8 --> <a href="/catalog/" class="item">
		<div class="w">
			<div class="ico ico-4">
				 &nbsp;
			</div>
			<h2 class="ttl">помощь онлайн</h2>
			<p class="dsc">
				 Оказываем информационную поддержку в онлайн-режиме
			</p>
		</div>
 </a>
		<!-- / item 8 -->
	</div>
	 <!-- / shop item container end -->
</div>
<!-- / shop roll end -->