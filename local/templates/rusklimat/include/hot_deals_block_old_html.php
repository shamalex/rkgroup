<!-- hot deals block -->
<div class="roll">
	<h2 class="ttl-r">Самые горячие предложения</h2>
	<div class="hot-deals c">

		<!-- slides -->
		<div class="slides c" id="hotDeals">

			<!-- item w/timer -->
			<div class="item b-base">
				<div class="w c">

					<div class="timer">
						<!-- <div class="sum">Успей сэкономить 1 000 рублей</div> -->
						<div class="sum">Успей купить</div>
							<div class="time" data-countdown="2016/01/22">
								<!-- <b>11</b> <span>дн.</span> <b>11</b> <span>ч.</span> <b>11</b> <span>мин.</span> <b class="sec">11</b> <span>сек.</span> -->
							</div>
						</div>

					<a class="lnk" href="/catalog/humidifier-and-dehumidifiers/ultrasonic-humidifiers/ultrazvukovye_uvlazhniteli/ballu_uhb_200/">
						<div class="ttl">Ультразвуковой увлажнитель воздуха</div>
							<div class="pic" style="background-image:url('/upload/iblock/0c2/22674_27025.png');">
								<div class="ico-hot">Успей<br/>купить</div>
							</div>
							<div class="dsc">Ballu UHB-200</div>
						</a>

					<div class="prc">
						<div class="curr">2 713 руб.</div>
					<!--	<div class="past">
							<span class="old">3 363</span> <span class="profit">1 000 выгода</span>
						</div>
					-->
					</div>
					<a class="btn btn-to-bsk" href="/catalog/ultrazvukovye-uvlazhniteli/ultrazvukovoy-uvlazhnitel-vozdukha-ballu-uhb-200/?action=ADD2BASKET&add_id=1222" id="bx_117848907_1222_add_basket_link_block2">Купить</a>

					<script>
						$('#bx_117848907_1222_add_basket_link_block2').on('click', function() {
							window.dataLayer = window.dataLayer || [];
							window.dataLayer.push({
								'event': 'addToCart',
								"ecommerce": {
									"currencyCode": "RUB",
									"add": {
										"products": [
											{
												"id": "22674",
												"name" : "Ультразвуковой увлажнитель воздуха Ballu UHB-200",
												"price": 2188.00,
												"brand": "Ballu",
												"category": "Увлажнители и очистители воздуха/Увлажнители воздуха/Ультразвуковые увлажнители",
												"quantity": 1
											}
										]
									}
								}
							});
						});
					</script>

				</div>
			</div>
			<!-- / item w/timer -->

			<!-- item w/timer -->
			<div class="item b-base">
				<div class="w c">

					<div class="timer">
						<div class="sum">Успей купить</div>
						<div class="time" data-countdown="2016/01/25">
							<!-- <b>11</b> <span>дн.</span> <b>11</b> <span>ч.</span> <b>11</b> <span>мин.</span> <b class="sec">11</b> <span>сек.</span> -->
						</div>
					</div>

					<a class="lnk" href="/catalog/convector-heaters/convective/bep_ext_1000/">
						<div class="ttl">Электрический обогреватель</div>
						<div class="pic" style="background-image:url('/upload/iblock/527/22908_27344_a.png');">
							<div class="ico-hot">Успей<br/>купить</div>
						</div>
						<div class="dsc">Ballu Plaza EXT BEP/EXT-1000</div>
					</a>

					<div class="prc">
						<div class="curr">5 190 руб.</div>
					<!--	<div class="past">
							<span class="old">5 590</span> <span class="profit">400 выгода</span>
						</div> -->
					</div>
					<a class="btn btn-to-bsk" href="/catalog/elektricheskie-obogrevateli-ballu-serii-plaza-ext/elektropanel-ballu-plaza-ext-bep-ext-1000/?action=ADD2BASKET&add_id=21518" id="bx_117848907_21518_add_basket_link_block2">Купить</a>

					<script>
						$('#bx_117848907_21518_add_basket_link_block2').on('click', function() {
							window.dataLayer = window.dataLayer || [];
							window.dataLayer.push({
								'event': 'addToCart',
								"ecommerce": {
									"currencyCode": "RUB",
									"add": {
										"products": [
											{
												"id": "22908",
												"name" : "Электропанель Ballu Plaza EXT BEP/EXT-1000",
												"price": 5190.00,
												"brand": "Ballu",
												"category": "Электрические обогреватели/Электрические обогреватели (конвекторы)/Электрические обогреватели Ballu серии Plaza EXT",
												"quantity": 1
											}
										]
									}
								}
							});
						});
					</script>

				</div>
			</div>
			<!-- / item w/timer -->

			<!-- item w/timer -->
			<div class="item b-base">
				<div class="w c">

					<div class="timer">
						<div class="sum">Успей купить</div>
						<div class="time" data-countdown="2016/01/22">
							<!-- <b>11</b> <span>дн.</span> <b>11</b> <span>ч.</span> <b>11</b> <span>мин.</span> <b class="sec">11</b> <span>сек.</span> -->
							</div>
					</div>

					<a class="lnk" href="/catalog/teplovie-pushki-zavesi/teplovie-pushki/elektricheskie_teplovye_pushki_ballu_serii_expert/bhp_3_000cl/">
						<div class="ttl">Электрическая тепловая пушка</div>
						<div class="pic" style="background-image:url('/upload/iblock/fd1/21015_23669_a.png');">
							<div class="ico-hot">Успей<br/>купить</div>
						</div>
						<div class="dsc">Ballu BHP-3.000CL</div>
					</a>

					<div class="prc">
						<div class="curr">2 421 руб.</div>
						<!--<div class="past">
							<span class="old">3 890</span> <span class="profit">1 200 выгода</span>
						</div> -->
					</div>
					<a class="btn btn-to-bsk" href="/catalog/elektricheskie-teplovye-pushki-ballu-serii-expert/teplovaya-pushka-ballu-bhp-3-000cl/?action=ADD2BASKET&add_id=19241" id="bx_117848907_19241_add_basket_link_block2">Купить</a>

					<script>
						$('#bx_117848907_19241_add_basket_link_block2').on('click', function() {
							window.dataLayer = window.dataLayer || [];
							window.dataLayer.push({
								'event': 'addToCart',
								"ecommerce": {
									"currencyCode": "RUB",
									"add": {
										"products": [
											{
												"id": "21015",
												"name" : "Тепловая пушка Ballu BHP-3.000CL",
												"price": 2421.00,
												"brand": "Ballu",
												"category": "Тепловые пушки и теплогенераторы/Электрические тепловые пушки/Электрические тепловые пушки Ballu серии Expert",
												"quantity": 1
											}
										]
									}
								}
							});
						});
					</script>

				</div>
			</div>
			<!-- / item w/timer -->

			<!-- item w/timer -->
			<div class="item b-base">
				<div class="w c">

					<div class="timer">
						<div class="sum">Успей купить</div>
						<div class="time" data-countdown="2016/01/22">
							<!-- <b>11</b> <span>дн.</span> <b>11</b> <span>ч.</span> <b>11</b> <span>мин.</span> <b class="sec">11</b> <span>сек.</span> -->
						</div>
					</div>

					<a class="lnk" href="/catalog/oil-radiator/ballu-plaza-arc/boh_md_11bb/">
						<div class="ttl">Масляный радиатор</div>
						<div class="pic" style="background-image:url('/upload/iblock/62a/21783_24924.png');">
							<div class="ico-hot">Успей<br/>купить</div>
						</div>
						<div class="dsc">Ballu Modern BOH/MD-11BB 2200 (11 секций)</div>
					</a>

					<div class="prc">
						<div class="curr">4 805 руб.</div>
						<!-- <div class="past">
							<span class="old">5 185</span> <span class="profit">1 000 выгода</span>
						</div> -->
					</div>
					<a class="btn btn-to-bsk" href="/catalog/seriya-modern/maslyanyy-radiator-ballu-modern-boh-md-11bb-2200-11-sektsiy/?action=ADD2BASKET&add_id=21267" id="bx_117848907_21267_add_basket_link_block2">Купить</a>

					<script>
						$('#bx_117848907_21267_add_basket_link_block2').on('click', function() {
							window.dataLayer = window.dataLayer || [];
							window.dataLayer.push({
								'event': 'addToCart',
								"ecommerce": {
									"currencyCode": "RUB",
									"add": {
										"products": [
											{
												"id": "21783",
												"name" : "Масляный радиатор Ballu Modern BOH/MD-11BB 2200 (11 секций)",
												"price": 3875.00,
												"brand": "Ballu",
												"category": "Масляные радиаторы/Масляные радиаторы Ballu/Серия Modern",
												"quantity": 1
											}
										]
									}
								}
							});
						});
					</script>

				</div>
			</div>
			<!-- / item w/timer -->

			<!-- item w/timer -->
			<div class="item b-base">
				<div class="w c">

					<div class="timer">
						<div class="sum">Успей купить</div>
						<div class="time" data-countdown="2016/01/25">
							<!-- <b>11</b> <span>дн.</span> <b>11</b> <span>ч.</span> <b>11</b> <span>мин.</span> <b class="sec">11</b> <span>сек.</span> -->
						</div>
					</div>

					<a class="lnk" href="/catalog/hand-dryers/electrolux/ehda_n_2500/">
						<div class="ttl">Cушилка для рук</div>
						<div class="pic" style="background-image:url('/upload/iblock/02c/9958_21581_a.png');">
							<div class="ico-hot">Успей<br/>купить</div>
						</div>
						<div class="dsc">Electrolux EHDA/N-2500</div>
					</a>

					<div class="prc">
						<div class="curr">13 253 руб.</div>
						<!-- <div class="past">
							<span class="old">13 025 руб.</span> <span class="profit">200 выгода</span>
						</div> -->
					</div>
					<a class="btn btn-to-bsk" href="/catalog/sushilki-dlya-ruk-electrolux/cushilka-dlya-ruk-electrolux-ehda-n-2500/?action=ADD2BASKET&add_id=21230" id="bx_117848907_21230_add_basket_link_block2">Купить</a>

					<script>
						$('#bx_117848907_21230_add_basket_link_block2').on('click', function() {
							window.dataLayer = window.dataLayer || [];
							window.dataLayer.push({
								'event': 'addToCart',
								"ecommerce": {
									"currencyCode": "RUB",
									"add": {
										"products": [
											{
												"id": "9958",
												"name" : "Cушилка для рук Electrolux EHDA/N-2500",
												"price": 10688.00,
												"brand": "Electrolux",
												"category": "Сушилки для рук/Сушилки для рук Electrolux",
												"quantity": 1
											}
										]
									}
								}
							});
						});
					</script>

				</div>
			</div>
			<!-- / item w/timer -->

			<!-- item w/timer -->
			<div class="item b-base">
				<div class="w c">

					<div class="timer">
						<div class="sum">Успей купить</div>
						<div class="time" data-countdown="2016/01/26">
							<!-- <b>11</b> <span>дн.</span> <b>11</b> <span>ч.</span> <b>11</b> <span>мин.</span> <b class="sec">11</b> <span>сек.</span> -->
						</div>
					</div>

					<a class="lnk" href="/catalog/uvlazhniteli-vozdukha-ballu-hello-kitty/uvlazhnitel-ultrazvukovoy-ballu-uhb-260-hello-kitty-aroma-mekhanika/">
						<div class="ttl">Ультразвуковой увлажнитель воздуха</div>
						<div class="pic" style="background-image:url('/upload/iblock/322/22622_27003.png?1448867922267851');">
							<div class="ico-hot">Успей<br/>купить</div>
						</div>
						<div class="dsc">Ballu UHB-260 Hello Kitty Aroma (механика)</div>
					</a>

					<div class="prc">
						<div class="curr">2 190 руб.</div>
						<!-- <div class="past">
							<span class="old">7 637</span> <span class="profit">500 выгода</span>
						</div> -->
					</div>
					<a class="btn btn-to-bsk" href="/catalog/uvlazhniteli-vozdukha-ballu-hello-kitty/uvlazhnitel-ultrazvukovoy-ballu-uhb-260-hello-kitty-aroma-mekhanika/?action=ADD2BASKET&add_id=21274" id="bx_117848907_21274_add_basket_link_block2">Купить</a>

					<script>
						$('#bx_117848907_21274_add_basket_link_block2').on('click', function() {
							window.dataLayer = window.dataLayer || [];
							window.dataLayer.push({
								'event': 'addToCart',
								"ecommerce": {
									"currencyCode": "RUB",
									"add": {
										"products": [
											{
												"id": "22622",
												"name" : "Увлажнитель ультразвуковой Ballu UHB-260 Hello Kitty Aroma (механика)",
												"price": 2190.00,
												"brand": "Ballu",
												"category": "Увлажнители и очистители воздуха/Увлажнители воздуха Ballu Kids/Увлажнители воздуха Ballu Hello Kitty",
												"quantity": 1
											}
										]
									}
								}
							});
						});
					</script>

				</div>
			</div>
			<!-- / item w/timer -->

		</div>
		<!-- slides -->
	</div>
	<!-- / hot deals item container end -->
</div>
<!-- / hot deals roll end -->
