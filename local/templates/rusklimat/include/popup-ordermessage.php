<?
include $_SERVER['DOCUMENT_ROOT'].'/bitrix/modules/main/include/prolog_before.php';
?>
<!-- ordermessage popup -->
<div class="pop pop-ordermessage pop-bg-1" id="popOrdermessage">
	<div class="w">
		<a href="#" class="x">&nbsp;</a>
		<h1 class="ttl">Вопрос по заказу</h1>
		<div class="feed-form">
			<form action="/send_ordermessage.php" autocomplete="off" enctype="multipart/form-data" method="post">
				<p>
					<textarea name="message" class="feed-field" id="ordermessageMessage" placeholder="Текст сообщения"></textarea>
					<span class="comment" data-name="message"></span>
				</p>				
				<input type="text" name="subj" class="feed-field" id="ordermessageSubj" value="" />
				<input type="hidden" name="orderId" value="" />
				<div class="btn feed-btn test-ordermessage-pop-ok" id="ordermessageBtn">Отправить вопрос</div>
			</form>
		</div>
	</div>
</div>
<!-- / ordermessage popup end -->