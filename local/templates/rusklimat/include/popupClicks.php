<!-- one click order popup, triggered by .one-click-pop -->
<div class="pop pop-one-click pop-bg-1" id="popOneClick">
	<div class="w">
		<a href="#" class="x">&nbsp;</a>
		<div class="ttl-p1">Оставьте номер вашего телефона<br/>и мы вам перезвоним</div>
		<div class="call-form">
			<form action="#" autocomplete="off">
				<input type="text" class="call-field" id="oneClickPhone" placeholder="Ваш номер телефона" value="" />
				<div class="btn call-btn test-one-click-pop-ok" id="callClickBtn">Заказать обратный звонок</div>
			</form>
		</div>
	</div>
</div>
<!-- / one click order popup end -->

<!-- one click order success popup, triggered on success -->
<div class="pop pop-one-click pop-bg-1" id="popOneClickYes">
	<div class="w">
		<a href="#" class="x">&nbsp;</a>
		<div class="ttl-p2">Вы успешно заказали покупку<br/>товара в один клик!</div>
		<p class="dsc">Наши менеджеры свяжутся с вами в ближайшее время.</p>
		<p class="dsc"><a href="#" class="test-one-click-pop-fail" style="display: none;">Тест: если неверно указан номер телефона.</a></p>
	</div>
</div>
<!-- / one click order success popup end -->

<!-- one click order fail popup, triggered on fail -->
<div class="pop pop-one-click pop-bg-2" id="popOneClickNo">
	<div class="w">
		<a href="#" class="x">&nbsp;</a>
		<div class="ttl-p2">Неверно указан номер телефона,<br/>пожалуйста, повторите ввод!</div>
	</div>
</div>
<!-- / one click order fail popup end -->
