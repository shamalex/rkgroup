function notSpecialKey(key) {
    return (key != 9 && // Tab
        key != 13 && // Enter
        key != 16 && // Shift
       // key != 17 && // Ctrl
        key != 37 && // Left Arrow
        key != 39); // Right Arrow
}

function removeNonLetters(strInput) {
    return strInput.replace(/[^A-Za-z�-��-��\s\-]|/g, '');
}

function removeNonNums(strInput) {
    return strInput.replace(/[^0-9\+]/g, '');
}

function oneclickValid() {
    $('#oneClickPhone').keyup(
        function (event) {
            if (notSpecialKey(event.keyCode)) {
                var phone = removeNonNums($(this).val());

                $(this).val(phone);

                if (phone.length < 7) {
                    $('#oneClickPhone').css({
                        'box-shadow': '0px 0px 5px 0px rgba(255,0,0,1)'
                    });
                } else {
                    $('#oneClickPhone').css({
                        'box-shadow': 'none'
                    });
                }
            }
        });


    //������� ������ ���� �� ������ ������ ���������� ������
    $("#callClickBtn").attr('disabled', true).removeClass('test-one-click-pop-ok').addClass('test-one-click-pop-fail');
    // ������ ���� ����� ���������� ����� �� ����� = 8
    $('#oneClickPhone').bind('keydown keyup change', function () {
        var cal_phone = $('#oneClickPhone').val();
        // ���� ����� cal_name >= 5 � cal_phone > 5
        if (cal_phone.length >= 7) {
            $("#callClickBtn").removeAttr('disabled', false).removeClass('test-one-click-pop-fail').addClass('test-one-click-pop-ok');
        } else {
            $("#callClickBtn").attr('disabled', true).removeClass('test-one-click-pop-ok').addClass('test-one-click-pop-fail');
        }
    });
}


