﻿$(document).ready(function () {

	// style 4 map
	var gmapStyleArr = [
		{
			"stylers": [
				{
					"saturation": -100
				},
				{
					"gamma": 1
				}
			]
		},
		{
			"elementType": "labels.text.stroke",
			"stylers": [
				{
					"visibility": "off"
				}
			]
		},
		{
			"featureType": "poi.business",
			"elementType": "labels.text",
			"stylers": [
				{
					"visibility": "off"
				}
			]
		},
		{
			"featureType": "poi.business",
			"elementType": "labels.icon",
			"stylers": [
				{
					"visibility": "off"
				}
			]
		},
		{
			"featureType": "poi.place_of_worship",
			"elementType": "labels.text",
			"stylers": [
				{
					"visibility": "off"
				}
			]
		},
		{
			"featureType": "poi.place_of_worship",
			"elementType": "labels.icon",
			"stylers": [
				{
					"visibility": "off"
				}
			]
		},
		{
			"featureType": "road",
			"elementType": "geometry",
			"stylers": [
				{
					"visibility": "simplified"
				}
			]
		},
		{
			"featureType": "water",
			"stylers": [
				{
					"visibility": "on"
				},
				{
					"saturation": 50
				},
				{
					"gamma": 0
				},
				{
					"hue": "#c5dfed"
				}
			]
		},
		{
			"featureType": "administrative.neighborhood",
			"elementType": "labels.text.fill",
			"stylers": [
				{
					"color": "#333333"
				}
			]
		},
		{
			"featureType": "road.local",
			"elementType": "labels.text",
			"stylers": [
				{
					"weight": 0.5
				},
				{
					"color": "#333333"
				}
			]
		},
		{
			"featureType": "transit.station",
			"elementType": "labels.icon",
			"stylers": [
				{
					"gamma": 1
				},
				{
					"saturation": 50
				}
			]
		}
	];
	
	// иконки для почек самовывоза
	// var iconBase = 'i/d/'; //путь к
	var iconBase = window.SITE_TEMPLATE_PATH+'/i/d/'; //путь к
	var icons = {
		// магаз
		store: {
			icon: iconBase + 'i-map-mrk-blue.png'
		},
		//  склад
		warehouse: {
			icon: iconBase + 'i-map-mrk-grey.png'
		},
		// хз что
		torg: {
			icon: iconBase + 'i-map-mrk-green.png'
		}
	};
	
	// самовывоза точек координаты и т.д.
	var locations = [
		['Yusupovskiy Sad<br>text here', 59.922877, 30.313645, 'store'],
		['Nikolskiy sad', 59.923044, 30.299703, 'warehouse'],
		['Poliklinika SPb.<br> gosudarstvennogo universiteta putey soobshcheniya<br>nab. Reki Fontanki, 113 Saint Petersburg', 59.9222462, 30.3144432, 'torg'],
		['<b>Метро Пушкинская</b>', 59.9212206, 30.325918, 'store']
	];

	// When the window has finished loading create our google map below
	google.maps.event.addDomListener(window, 'load', init);

	function init() {
		// Basic options for a simple Google Map
		// For more options see: https://developers.google.com/maps/documentation/javascript/reference#MapOptions
		var myLatLng = new google.maps.LatLng(59.922877, 30.313645);

		var mapOptions = {
			// How zoomed in you want the map to start at (always required)
			zoom: 15,

			// The latitude and longitude to center the map (always required)
			center: myLatLng,

			// hide controls
			disableDefaultUI: true,
			
			// How you would like to style the map. 
			// This is where you would paste any style found on Snazzy Maps.
			styles: gmapStyleArr
		};

		// Get the HTML DOM element that will contain your map 
		// We are using a div with id="map" seen below in the <body>
		var mapElement = document.getElementById('map');
		
		if (mapElement) {

			// Create the Google Map using our element and options defined above
			var map = new google.maps.Map(mapElement, mapOptions);
			
			
			// place markers + infowindows
			var infowindow = new google.maps.InfoWindow();

			var marker, i;

			for (i = 0; i < locations.length; i++) {  
				marker = new google.maps.Marker({
					position: new google.maps.LatLng(locations[i][1], locations[i][2]),
					icon: icons[locations[i][3]].icon,
					map: map
				});
			
				google.maps.event.addListener(marker, 'click', (function(marker, i) {
					return function() {
						infowindow.setContent(locations[i][0]);
						infowindow.open(map, marker);
					}
				})(marker, i));
			}
		
		}
		

	} //end init

});