// $.cookie.defaults.expires = 365;
// $.cookie.defaults.path = '/';
// $.cookie.defaults.domain = window.COOKIE_NAME;

function waitForWebfonts(fonts, callback) {
    var loadedFonts = 0;
    for(var i = 0, l = fonts.length; i < l; ++i) {
        (function(font) {
            var node = document.createElement('span');
            // Characters that vary significantly among different fonts
            node.innerHTML = 'giItT1WQy@!-/#';
            // Visible - so we can measure it - but not on the screen
            node.style.position      = 'absolute';
            node.style.left          = '-10000px';
            node.style.top           = '-10000px';
            // Large font size makes even subtle changes obvious
            node.style.fontSize      = '300px';
            // Reset any font properties
            node.style.fontFamily    = 'sans-serif';
            node.style.fontVariant   = 'normal';
            node.style.fontStyle     = 'normal';
            node.style.fontWeight    = 'normal';
            node.style.letterSpacing = '0';
            document.body.appendChild(node);

            // Remember width with no applied web font
            var width = node.offsetWidth;

            node.style.fontFamily = font;

            var interval;
            function checkFont() {
                // Compare current width with original width
                if(node && node.offsetWidth != width) {
                    ++loadedFonts;
                    node.parentNode.removeChild(node);
                    node = null;
                }

                // If all fonts have been loaded
                if(loadedFonts >= fonts.length) {
                    if(interval) {
                        clearInterval(interval);
                    }
                    if(loadedFonts == fonts.length) {
                        callback();
                        return true;
                    }
                }
            };

            if(!checkFont()) {
                interval = setInterval(checkFont, 50);
            }
        })(fonts[i]);
    }
};

$(document).ready(function () {

	$('body').on('click', 'a[href="#"]', function(e) {
		e.preventDefault();
	});
	
	$(document).on('change', '#sortSelect, #amountSelect, #sortSelect_bottom, #amountSelect_bottom', function() {
		window.location = $(this).val();
	});

	
	$('#regions_block').on('click', 'ul li a', function(e) {
		e.preventDefault();
		var cur_text = $(this).text();
		$('#cities_block .scroll').hide().filter('[-data-region="'+cur_text+'"]').show();
		$('#regions_block ul li').removeClass('active');
		$(this).parent().addClass('active');
	});
	
	var cur_region = $('#cities_block .scroll ul li').removeClass('active').filter('[-data-city-id="'+window.CUR_CITY_ID+'"]').addClass('active').eq(0).parents('.scroll');
	$('#regions_block ul li').removeClass('active').filter('[-data-region="'+cur_region.attr('-data-region')+'"]').addClass('active');
	$('#cities_block .scroll').hide();
	cur_region.show();
	
	// if (typeof($.cookie(window.COOKIE_PREFIX+'CITY_ID_NEW')) == 'undefined') {
		// $.cookie(window.COOKIE_PREFIX+'CITY_ID_NEW', window.CUR_CITY_ID);
	// }
	
	
	// $('#cities_block .scroll').on('click', 'ul li a[href="#city"]', function(e) {
		// e.preventDefault();
		// $('#cities_block .scroll ul li').removeClass('active');
		// var cur_parent = $(this).parent().addClass('active');
		// $.cookie(window.COOKIE_PREFIX+'CITY_ID_NEW', cur_parent.attr('-data-city-id'));
		
		// window.location.reload();
	// });
	
	$('.city_name_here').text(window.CUR_CITY);
	
	
	if ($('#personal_edit_form').length) {
		$('#personal_edit_form input[name="NEW_PASSWORD_CONFIRM"]').attr('disabled', true);
		$('#personal_edit_form input[name="NEW_PASSWORD"]').on('keyup change', function(e) {
			if ($(this).val().length > 0) {
				$('#personal_edit_form input[name="NEW_PASSWORD_CONFIRM"]').removeAttr('disabled').attr('required', true);
			} else {
				$('#personal_edit_form input[name="NEW_PASSWORD_CONFIRM"]').removeAttr('required').val('').attr('disabled', true);
			}
		});
	}
	
	if ($('#userEmailHere').length && $('#userLoginHere').length) {
		$('#userEmailHere').on('keyup change', function() {
			$('#userLoginHere').val($(this).val());
		}).trigger('change');
	}

	$(".btn-revw-add").on("click",function(){
		$(".form-revw-add").slideToggle('slow');
	});
	$(".btn-revw-send").on("click",function(){
		$.post(
			"/review_add.php",
			{item: $(".form-revw-add input[name=product]").val(), grade: $(".form-revw-add input[name=score]").val(), author: $(".form-revw-add input[name=NAME]").val(), text: $(".form-revw-add textarea[name=COMMENT]").val(), city: $(".form-revw-add input[name=city]").val()},
			function(data){
				$(".form-revw-add .result").removeClass("error success");
				$(".form-revw-add .result").addClass(data.status);
				$(".form-revw-add .result").html(data.message);
				if(data.status=="success"){
					$(".form-revw-add form").hide();
					$(".btn-revw-add").hide();
				}
			},
			"json"
		);
	});
	
	$(".btn-lk-logout").on("click",function(){
		window.location=$(".mn-func-logout").attr("href");
	});
	
	//выравнивание блоков ссылок
	
	
	var line = $(".c-block");
	if (line.length) {
		var setCBlockHeight = function() {
			var times = 0, rows = Array(), max = 0;
			line.css("height", '').css("overflow", '').each(function() {
				if (times == 4) {                
					for (var i = 0; i < rows.length; i++) {
						rows[i].css("height", max).css("overflow", 'hidden');
					}
					max = 0;
					times = 0;
					rows = [];
				}
				if (max < $(this).outerHeight()) {
				   max = $(this).outerHeight();
				}
				rows[times] = $(this);
				times += 1;
			});
			
			if (times != 0) {       
				for (var i = 0; i < rows.length; i++) {
					rows[i].css("height", max).css("overflow", 'hidden');
				}          
			}
		}
		setCBlockHeight();
		$(window).load(function() {
			setTimeout(function() {
				waitForWebfonts(['CirceWebBold'], setCBlockHeight);
			}, 50);
		});
	}
});
	