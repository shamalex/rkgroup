$(document).ready(function () {

/*** header catalogue menu */

	//https://github.com/kamens/jQuery-menu-aim
	
		$("#menuCatalogue > li[data-submenu-id] > a").on('click', function(e) {
			e.preventDefault();
		});
		
        var $menu = $("#menuCatalogue");
		
        $menu.menuAim({
            activate: activateSubmenu,
            deactivate: deactivateSubmenu,
		 	exitMenu: function() {
				return true;
				},
			submenuDirection: "below",
			rowSelector: "> li[data-submenu-id]"
        });

        function activateSubmenu(row) {
            var $row = $(row),
                submenuId = $row.data("submenuId"),
                $submenu = $("#" + submenuId);
            $submenu.css({
                display: "block"
            });

			$row.addClass("liHover");
            $row.find("a").addClass("maintainHover");
        }

        function deactivateSubmenu(row) {
            var $row = $(row),
                submenuId = $row.data("submenuId"),
                $submenu = $("#" + submenuId);

            $submenu.css("display", "none");
			$("li.liHover").removeClass("liHover");
            $row.find("a").removeClass("maintainHover");
        }

/*** header catalogue menu end */





/*** fixed header */

	var heyCheckMeOut = function(e) {
		var hd = $('.fix-hd-w'),
			its = $('.fix-hd-w .item[data-full="1"]');

		if ($(window).scrollTop() > 116 && its.length) {
			hd.slideDown(200);
		}
		else {
			hd.slideUp(200);
		}
	}
	
	$(window).scroll(
		function(e) {
			new heyCheckMeOut;
			}
	);

/*** fixed header end */


/*** custom checbox and radio */

$('.checkset input, .check-custom').iCheck({
    checkboxClass: 'icheckbox_minimal',
    radioClass: 'iradio_minimal',
	activeClass: 'active'
});

/*** custom checbox and radio end */


/*** popups */

	/** city pop-up */
	$('.city-pop').on('click', function(e) {

		e.preventDefault();

		$('#popCity').bPopup(
				{
					closeClass:'x',
					modalColor: '#415562',
					opacity:0.98,
					position: ["auto",50]
				}
		);
		
		// scrollbars for city pop-up
		$(".region .scroll, .cities .scroll").mCustomScrollbar({
			theme:"grey",
			scrollButtons:{ enable: true },
			autoDraggerLength: false
		});
		
		$(".region .scroll").mCustomScrollbar('scrollTo', $('.reg-ul li.active'));
		// scrollbars for city pop-up end

	});
	/** city pop-up end */ 
	
	
	
	/** callback pop-up */
	$('.callback-pop').on('click', function(e) {

		e.preventDefault();

		$('#popCallback').bPopup(
				{
					closeClass:'x',
					modalColor: '#415562',
					opacity:0.98,
					position: ["auto",50],
					onOpen: function() { new opepe(); }, // validate
					onClose: function() { $('#callbackPhone').val(); } //clear
				}
		);
	
	});
	
	//success
	$('.test-callback-pop-ok').on('click', function(e) {

		e.preventDefault();
		
		$('#popCallbackYes').bPopup(
				{
					closeClass:'x',
					modalColor: '#415562',
					opacity:0.98,
					position: ["auto",50]
				}
		); 
		$('#popCallback').bPopup().close();

	});

	//fail
	$('.test-callback-pop-fail').on('click', function(e) {

		e.preventDefault();
		
		$('#popCallbackNo').bPopup(
				{
					closeClass:'x',
					modalColor: '#415562',
					opacity:0.98,
					position: ["auto",50]
				}
		); 
		$('#popCallbackYes').bPopup().close();
		
	});
	
	/** callback pop-up end */ 
	
	/** subscription pop-up */
	$('.sub-btn').on('click', function(e) {

		e.preventDefault();

		$('#popSubYes').bPopup(
				{
					closeClass:'x',
					modalColor: '#415562',
					opacity:0.98,
					position: ["auto",50]
				}
		);
		
	});

	//fail
	$('.test-sub-pop-fail').on('click', function(e) {

		e.preventDefault();
		
		$('#popSubNo').bPopup(
				{
					closeClass:'x',
					modalColor: '#415562',
					opacity:0.98,
					position: ["auto",50]
				}
		); 
		$('#popSubYes').bPopup().close();
		
	});
	
	/** subscription pop-up end */ 
	
	/** credit pop-up */
	$('.credit-pop').on('click', function(e) {

		e.preventDefault();

		$('#popCredit').bPopup(
				{
					closeClass:'x',
					modalColor: '#415562',
					opacity:0.98,
					position: ["auto",50]
				}
		);
		$('.check-custom').iCheck({
			checkboxClass: 'icheckbox_minimal',
			radioClass: 'iradio_minimal',
			activeClass: 'active'
		});
	
	});
	/** credit pop-up end */ 
	
	/** self pop-up */
	$('.self-pop').on('click', function(e) {

		e.preventDefault();

		$('#popSelf').bPopup(
				{
					closeClass:'x',
					modalColor: '#415562',
					opacity:0.98,
					position: ["auto",50]
				}
		);
		$('.check-custom').iCheck({
			checkboxClass: 'icheckbox_minimal',
			radioClass: 'iradio_minimal',
			activeClass: 'active'
		});
	
	});
	/** self pop-up end */ 
	
	
	/** one click order pop-up */
	$('.one-click-pop').on('click', function(e) {

		e.preventDefault();

		$('#popOneClick').bPopup(
				{
					closeClass:'x',
					modalColor: '#415562',
					opacity:0.98,
					position: ["auto",50],
					onOpen: function() { new oneclickValid(); }, // validate
					onClose: function() { $('#oneClickPhone').val(); } //clear
				}
		);
	
	});
	
	//success
	$('.test-one-click-pop-ok').on('click', function(e) {

		e.preventDefault();
		
		$('#popOneClickYes').bPopup(
				{
					closeClass:'x',
					modalColor: '#415562',
					opacity:0.98,
					position: ["auto",50]
				}
		); 
		$('#popOneClick').bPopup().close();

	});

	//fail
	$('.test-one-click-pop-fail').on('click', function(e) {

		e.preventDefault();
		
		$('#popOneClickNo').bPopup(
				{
					closeClass:'x',
					modalColor: '#415562',
					opacity:0.98,
					position: ["auto",50]
				}
		); 
		$('#popOneClickYes').bPopup().close();
		
	});
	
	/** one click order pop-up end */ 
	


/*** popups end */	


/*** autocomplete */	

	/** header & 404 search */
	// data sample
	var autocompleteTest = [
		{ value: 'Пусть компьютер отдохнет,', data: 'line1' }, 
		{ value: 'Монитор пускай заснет,', data: 'line2' }, 
		{ value: 'Мышь день отдыха имеет.', data: 'line3' }, 
		{ value: 'Не до них нам всем сейчас.', data: 'line4' }, 
		{ value: 'Мы пришли поздравить вас', data: 'line5' }, 
		{ value: 'С вашим славным юбилеем.', data: 'line6' }, 
		{ value: 'Служба службой, но она', data: 'line7' }, 
		{ value: 'Будни занимать должна.', data: 'line8' }, 
		{ value: 'В праздник нужно веселиться.', data: 'line9' }, 
		{ value: 'Стол накрыть, на стулья сесть, Пить вино и вкусно есть,', data: 'line10' }, 
		{ value: 'Видеть радостные лица.', data: 'line12' }, 
		{ value: 'Скажет громко тамада:', data: 'line13' }, 
		{ value: '«Что нам возраст, что года!', data: 'line14' }, 
		{ value: 'Ведь душа, она – гитара:', data: 'line15' }, 
		{ value: 'Если струны есть — поет…»', data: 'line16' }, 
		{ value: 'И воскликнет весь народ: «Честь и слава юбиляру!»', data: 'line17' }, 
		{ value: 'И за тостом тост пойдет,', data: 'line19' }, 
		{ value: 'И один другого лучше.', data: 'line20' }, 
		{ value: 'А компьютер подождет. Что ему? Ведь он непьющий…', data: 'line21' },
		{ value: 'abcdefghijklmnopqrstuvwxyz', data: 'abc' }
	];

	//autocomplete header
	$('#hdSearchAutocomplete').autocomplete({
		lookup: autocompleteTest,
		lookupLimit: 10,
		autoSelectFirst: true,
		maxHeight: 'auto',
		appendTo: '.hd-srch',
		onSelect: function (suggestion) {
		   // alert('You selected: ' + suggestion.value + ', ' + suggestion.data);
		}
	});
	
	//autocomplete 404
	$('#searchAutocomplete404').autocomplete({
		lookup: autocompleteTest,
		lookupLimit: 10,
		autoSelectFirst: true,
		maxHeight: 'auto',
		appendTo: '.srch-404-holder',
		onSelect: function (suggestion) {
		   // alert('You selected: ' + suggestion.value + ', ' + suggestion.data);
		}
	});
	
	//autocomplete search results
	$('#searchAutocompleteResults').autocomplete({
		lookup: autocompleteTest,
		lookupLimit: 10,
		autoSelectFirst: true,
		maxHeight: 'auto',
		appendTo: '.srch-holder',
		onSelect: function (suggestion) {
		   // alert('You selected: ' + suggestion.value + ', ' + suggestion.data);
		}
	});
/*** autocomplete end */	





/** blocks with timer */
if( $('.time').length){	
	$('[data-countdown]').each(function() {
		var $this = $(this), finalDate = $(this).data('countdown');
		$this.countdown(finalDate, function(event) {
			$this.html(event.strftime('<b>%D</b> <span>дн.</span> <b>%H</b> <span>ч.</span> <b>%M</b> <span>мин.</span> <b class="sec">%S</b> <span>сек.</span>'));
		});
	});
}
/** blocks with timer end */

/** test fixed header popup */
	$('.btn-to-bsk, #btnToBasket').on('click', function(e) {
		e.preventDefault();
		$('#bskHint').slideDown(300).delay(3000).slideUp(300);
	});
	$('.btn-to-comp').on('click', function(e) {
		e.preventDefault();
		$('#compHint').slideDown(300).delay(3000).slideUp(300);
	});
	$('.btn-to-fav').on('click', function(e) {
		e.preventDefault();
		$('#favHint').slideDown(300).delay(3000).slideUp(300);
	});
	
	// card series and side
	$('.tbl-series .icheckbox_minimal, #addComp').on('ifChecked', function(event){
		$('#compHint').slideDown(300).delay(3000).slideUp(300);
	});
		
	$('#addFav').on('ifChecked', function(event){
		$('#favHint').slideDown(300).delay(3000).slideUp(300);
	});
	
/** test fixed header popup end */





/*** card gallery */

//360
function initRotate(){
	var rotate;
	rotate = $('.rotate').ThreeSixty({
		totalFrames: 36, // Total no. of image you have for 360 slider
		endFrame: 36, // end frame for the auto spin animation
		currentFrame: 1, // This the start frame for auto spin
		imgList: '.threesixty_images', // selector for image list
		progress: '.spinner', // selector to show the loading progress
		imagePath:'i/rotate/', // path of the image assets
		filePrefix: '', // file prefix if any
		ext: '.jpg', // extention for the assets
		height: 448,
		width: 448,
		navigation: false
	});

}
if( $('.threesixty').length){
	new initRotate();
}


//card gallery
function cardGal(){
	$('.c-gal').on('click', 'a', function(e) {
	
		e.preventDefault();
		
		if (!$(this).hasClass("active")) {
		
			var that = $(this),
				href = that.attr('href'),
				navID = '#' + that.closest('.c-gal').attr('id'),
				picID = (navID).replace('-Nav', '-Pic'),
				pic = $(picID);

			$(navID+' a').removeClass('active');
			that.addClass("active");

			if (!$(this).parent().hasClass("d3")) {
				$('.threesixty').hide();
				pic.css("background-image", "url('"+href+"')");
			}
			
			else if ($(this).parent().hasClass("d3")) {
				$('.threesixty').show();
				pic.css("background-image", "none");
			}
			
		}
		//end if active
		
	});
}
new cardGal();

//card popup
$('.maximize').on("click", function(e){
	e.preventDefault();
		var that = $(this),
			picID = $('.maximize .c-pic').attr('id'),
			navID = (picID).replace('-Pic', '-Nav'),
			href = $('#' + navID + ' .active').attr('href'),
			buildGal = $('#' + navID).html(),
			buildModal =  '<div class="w"><a href="#" class="x">&nbsp;</a><div class="c-pic pic" id="' + picID + '-pop" style="background-image:url(' + href + ')"><div class="threesixty car rotate"><div class="spinner"><span>0%</span></div><ol class="threesixty_images"></ol></div></div><ul class="c-gal c" id="' + navID + '-pop">' + buildGal + '</ul></div>';
						
		e.preventDefault();
		$('<div class="pop pop-card pop-bg-1" style="width:750px" id="popCard" />').html(buildModal).bPopup(
			{
				closeClass:'x',
				modalColor: '#415562',
				opacity:0.98,
				position: ["auto",50],
				onClose: function() { $(this).remove(); }
			}
		);
		new cardGal();
		
		new initRotate();
		
		if( $('#popCard .d3').children().hasClass('active') ){
			$('#popCard .threesixty').show();
		}
		
		
});


/*** card gallery end */

/*** card tabs */

if( $('#cardTabs').length){
	$('#cardTabs').easytabs();
}

/*** card tabs end */

/*** rating start */

if( $('.stars').length){
	$('.stars').raty({
		hints: ['не спс', 'эээ', 'нормуль', 'гут', 'збс'],
		score: 3,
		readOnly: true,
		path: 'i/d/rate'
	});
}

/*** rating end */

/*** catalogue view switch */

$('#catControl a').on("click", function(e){
	e.preventDefault();
	if (!$(this).hasClass("active")) {

		var that = $(this),
			holder = $('.cat-types');

		$('#catControl a').removeClass('active');
		that.addClass('active');
		
		if (that.hasClass("tldr")) {
			holder.removeClass('with').addClass('without')
		}
		else {
			holder.removeClass('without').addClass('with')
		}
		
	}

});

/*** catalogue view switch end */


/*** catalogue filter */

	//switch filter view
	$('#filterSwitcher').on('click', 'a', function(e) {
		e.preventDefault();
	
		if (!$(this).parent().hasClass("active")) {

			var that = $(this),
				li = that.parent();
				//holder = $('.cat-types');

			$('#filterSwitcher li').removeClass('active');
			li.addClass('active');
			
		}
		
	});
	

if( $('.hider').length){
	
	
	
	//hide or show items in filter
	$('.hider').on('click', function(e) {
		
		e.preventDefault();
		
		var that = $(this),
			item = that.parent().parent(),
			hideme = item.find('.hideme');

		if (that.hasClass("minus")) {
				that.removeClass('minus').addClass('plus');
				hideme.slideUp(200);
				item.removeClass('open');
		}
		else if (that.hasClass("plus")) {
				that.removeClass('plus').addClass('minus');
				hideme.slideDown(200);
				item.addClass('open');
				
		}
		
	});
}

/*** catalogue filter end */


/*** catalogue filter result view switch */

$('#resultsControl a').on("click", function(e){
	e.preventDefault();
	if (!$(this).hasClass("active")) {

		var that = $(this);

		$('#resultsControl a').removeClass('active');
		that.addClass('active');
		
 		if (that.hasClass("tiles")) {
			$('.results-line').fadeIn(300);
			$('.results-block').fadeOut(250);
		}
		else {
			$('.results-block').fadeIn(250);
			$('.results-line').fadeOut(250);
		}
		
	}

});

/*** catalogue view switch end */

/*** remove blocks in Favorites, Compare */

function favMidCount(){
	var its = $(".favorites .item").length;
	$("#favMidCount").text(its);
}

$('.b-x').on("click", function(e){
	e.preventDefault();
	$(this).parent().remove();
	new favMidCount();
});

if( $('.favorites').length){
	//favorites block count

	new favMidCount();
}

/*** remove blocks in Favorites, Compare end */

/*** remove rows from basket */

$('.td-x .x').on("click", function(e){
	e.preventDefault();
	var that = $(this),
		parent = that.parent().parent('tr');
	
	//delete separator after
	if( parent.next().length){
		parent.next().remove();
	}
	//delete separator before
	else {
		parent.prev().remove();
	}
	//delet row
	parent.remove();

});

/*** remove rows from basket end */



/***  basket - +  */

$(".amt-bt").on("click", function() {

  var $button = $(this);
  var oldValue = $button.parent().find(".bsk-amt").val();

  if ($button.text() == "+") {
	  var newVal = parseFloat(oldValue) + 1;
	} else {
   // Don't allow decrementing below zero
    if (oldValue > 0) {
      var newVal = parseFloat(oldValue) - 1;
    } else {
      newVal = 0;
    }
  }

  $button.parent().find(".bsk-amt").val(newVal);

});

/***  basket - +  end */


/***  basket promo code show/hide */

$(".prm-hider").on("click", function() {

	if ($('.prm-hidden').is(':visible')) {
		$('.prm-hidden').slideUp(200);
	}
	else {
		$('.prm-hidden').slideDown(200);
	}

});

/***  basket promo code show/hide end */


/***  basket city selector  */

//open pop
$(".city-sel").on("click", function() {
	var pop = $('.city-sel-pop');

	if (pop.is(':visible')) {
		pop.slideUp(0);
	}
	else {
		pop.slideDown(0);
	}
});

//pick city
$(".city-sel-pop .city-ul a").on("click", function(e) {
	e.preventDefault();
	if (!$(this).parent().hasClass("active")) {
		$(".city-sel-pop .city-ul li").removeClass('active');
		$(this).parent().addClass('active');
		$(".city-sel").text($(this).text());
		$('.city-sel-pop').slideUp(0);
	

	}
	
});

//city-sel pop windows switch
$(".city-sel-srch-btn").on("click", function(e) {
	e.preventDefault();
	$('#citySel1').slideUp(200);
	$('#citySel2').slideDown(200);
	
	//search for city not in list
	// scrolls
	$(".city-sel-srch-list .scroll").mCustomScrollbar({
		theme:"grey",
		scrollButtons:{ enable: true },
		autoDraggerLength: false
	});
});
$(".city-sel-srch-back").on("click", function(e) {
	e.preventDefault();
	$('#citySel2').slideUp(200);
	$('#citySel1').slideDown(200);
});
		
	

		
/***  basket city selector end */





/***  basket delivery table radios */

$(".tbl-del-options .iradio_minimal").on('ifChecked', function(e){
	$(this).parent().parent('tr').find('.del-method-hidden').css('display', 'block');
});
$(".tbl-del-options .iradio_minimal").on('ifUnchecked', function(e){
	$(this).parent().parent('tr').find('.del-method-hidden').css('display', 'none');
});


/***  basket delivery table radios end */



/*** order switch */

$('#switchDost a').on("click", function(e){
	e.preventDefault();
	if (!$(this).hasClass("active")) {

		var that = $(this),
			holder = that.parent().parent();

		that.siblings().removeClass('active');
		that.addClass('active');
			
			/*** 
			// yur/fiz basket switch
			//yurik
			if (that.is("#yurik")) {
				$(".basket-prm").slideUp(200);
			}
			//fizik
			else if (that.is("#fizik")) {
				$(".basket-prm").slideDown(200);
			}
			
			*/
			//samo
			if (that.is("#samo")) {
				$("#basketDelAdr").slideUp(200);
				$("#dostTbl").hide();
				$("#samTbl").show();
			}
			//dost
			else if (that.is("#dost")) {
				$("#basketDelAdr").slideDown(200);
				$("#dostTbl").show();
				$("#samTbl").hide();
			}

	}
});



$('#switchYurFiz a').on("click", function(e){

	if ($(this).hasClass("active")) {

		e.preventDefault();

	}
});

/*** order switch end */



/*** custom selects */

// catalogue filter result selects

$('#sortSelect').ikSelect({
	autoWidth: false,
	ddFullWidth: false,
	customClass: 'sort-select'
});


$('#amountSelect').ikSelect({
	autoWidth: false,
	ddFullWidth: false,
	customClass: 'amount-select'
});

//basket selects
$('#payMethodSelect').ikSelect({
	autoWidth: false,
	ddFullWidth: false,
	customClass: 'pay-method-select'
});

$('#payMethodSelect').on('change', function() {
	//hide old/other tooltips
	$.powerTip.hide();

	//get text val
	var val = $(this).val(),
		text = $("#payMethodSelect option[value='"+ val +"']").text();
	//alert(text);
	
	//submit button text
	$('#payText').text(text);
	//tooltip body text
	$('#ttPayMethod .tt-dsc p').text(text);
	
	
	//tooltip
	$('.pay-method-select').powerTip({ manual: true , placement: 'e'});
	$('.pay-method-select').data('powertiptarget', 'ttPayMethod');

	$.powerTip.show($('.pay-method-select'));
	
  
});


//delivery method selects
$('.del-method-select').ikSelect({
	autoWidth: false,
	ddFullWidth: false,
	customClass: 'ik-green del-method-select'
});

//delivery method selects
$('.yur-select').ikSelect({
	autoWidth: false,
	ddFullWidth: false,
	customClass: 'yur-select'
});

//gmap select
$('#map-select').ikSelect({
	autoWidth: false,
	ddFullWidth: false,
	customClass: 'map-select'
});

$('.budget-select').ikSelect({
	autoWidth: false,
	ddFullWidth: false,
	customClass: 'ik-30-lt'
});

//scrollbar in select
$('.ik_select_list_inner').mCustomScrollbar({
	theme:"map-select",
	scrollButtons:{ enable: true },
	autoDraggerLength: false
});

/*** custom selects end */


/*** personal account */

$('#addCompUnhide').on("click", function(e) {
	$('#addComp').slideDown(200);
	$(this).css('display', 'none');
});

$('#addComp .lk-btn-add, #addComp .lk-btn-cancel').on("click", function(e) {
	$('#addComp').slideUp(200);
	$('#addCompUnhide').css('display', 'block');
});


$('#addAdrUnhide').on("click", function(e) {
	$('#addAdr').slideDown(200);
	$(this).css('display', 'none');
});

$('#addAdr .lk-btn-add, #addAdr .lk-btn-cancel').on("click", function(e) {
	$('#addAdr').slideUp(200);
	$('#addAdrUnhide').css('display', 'block');
});


/*** personal account end */


/***  basket tooltips */

//switch tooltips
if( $('.ttSwitch1').length){
		$('.ttSwitch1').powerTip({ placement: 'nw', mouseOnToPopup: true });
		$('.ttSwitch1').data('powertiptarget', 'ttSwitch1');
}
if( $('.ttSwitch2').length){
		$('.ttSwitch2').powerTip({ placement: 'nw'});
		$('.ttSwitch2').data('powertiptarget', 'ttSwitch2');
}

//fake tooltips for phone and email if when matches already registered
if( $('.ttPhone').length){

	$('.ttPhone').on("click", function(e) {
		$('#ttPhone').css('display', 'block');
	
	});
	
 	$('.btn-tt-sms1').on("click", function(e) {
		e.preventDefault();
		$("#ttSms1").hide();
		$("#ttSms2").show();
	}); 
 	$('.btn-tt-sms2').on("click", function(e) {
		e.preventDefault();
		$('#ttPhone').css('display', 'none');
	}); 
}

if( $('.ttEmail').length){

	$('.ttEmail').on("click", function(e) {
		$('#ttEmail').css('display', 'block');
	
	});
	
 	$('.btn-tt-em1').on("click", function(e) {
		e.preventDefault();
		$("#ttEm1").hide();
		$("#ttEm2").show();
	}); 
 	$('.btn-tt-em2').on("click", function(e) {
		e.preventDefault();
		$('#ttEmail').css('display', 'none');
	}); 
}

/***  basket tooltips end */


/*** basked upload cover */
$(".fileUpload").hover(function() {
	$(".del-upload-btn").addClass( "hover" );
	}, function() {
	$(".del-upload-btn").removeClass( "hover" );
	}
);


/*** basked upload cover end */


/***  basket map show/hide */

$(".sm-map-pop").on("click", function() {
	var rel = $(this).parent().parent().find('.relat .sm-map');
		
 
	if (rel.is(':visible')) {

		rel.slideUp(200);
	}
	else {
		rel.slideDown(200);
	} 

});
$(".map-x").on("click", function() {
	$(this).parent().parent().hide()

});


/***  basket map show/hide end */



/*** scroll to top */

var $root = $('html, body');
$('.to-top a').click(function(e) {
	e.preventDefault();
    var href = $.attr(this, 'href');

    $root.animate({
        scrollTop: $(href).offset().top-50
    }, 500);
    return false;
});	
/*** scroll to top end */


/*** range bar */

if( $('.slider-input').length){

	var rangeInputs = function(e) {
		var values = $('.slider-input').val().split(',');
		$('#inputLow').val(values[0]);
		$('#inputHigh').val(values[1]);
	}

	$('.slider-input').jRange({
		from: 0,
		to: 10000,
		step: 1,
		scale: [0,10000],
		format: '%s',
		width: '100%',
		showLabels: false,
		showScale: false,
		isRange : true,
		onstatechange: function() {	new rangeInputs;}
	});

} 
	
/*** range bar end */

/*** silders start */

	/** other deals slider start */

 	if( $('#other').length){
		$('#other').slick({
			dots: true,
			speed: 1000,
			slidesToShow: 3,
			slidesToScroll: 3,
			variableWidth: true
		});
	} 
	/** other deals slider end */
	
	/** other deals 2 slider start */

 	if( $('#other2').length){
		$('#other2').slick({
			dots: true,
			speed: 1000,
			slidesToShow: 3,
			slidesToScroll: 3,
			variableWidth: true
		});
	} 
	/** other deals 2 slider end */
	
	/** spec offers in cat-brands start */

 	if( $('#specoff').length){
		$('#specoff').slick({
			dots: true,
			speed: 1000,
			slidesToShow: 3,
			slidesToScroll: 3,
			variableWidth: true
		});
	} 
	/** spec offers in cat-brands end */

	/** news slider start */
	if( $('#news').length){
		$('#news').slick({
			dots: true,
			speed: 1000,
			slidesToShow: 4,
			slidesToScroll: 4,
			variableWidth: true
		});
	}
	/** news slider end */
	
	/** brands 4 catalogue slider start */
	if( $('#brandsSlider').length){
		$('#brandsSlider').slick({
			dots: false,
			speed: 1000,
			slidesToShow: 5,
			slidesToScroll: 5,
			variableWidth: true
		});
	}
	/** brands 4 catalogue slider end */
	
	/** hot deals slider start */
	if( $('#hotDeals').length){
		$('#hotDeals').slick({
			dots: false,
			speed: 1000,
			slidesToShow: 3,
			slidesToScroll: 3,
			variableWidth: true
		});
	}
	/** hot deals slider end */
	
	/** brand logos 4 catalogue slider start */
	if( $('#brandsLogoSlider').length){
		$('#brandsLogoSlider').slick({
			dots: false,
			speed: 1000,
			slidesToShow: 6,
			slidesToScroll: 6,
			variableWidth: true
		});
	}
	/** brands logos 4 catalogue slider end */
	
	/** porfolio slider display start */
 	if( $('#portDisplay').length){

		$('#portDisplay').slick({
		  speed: 700,
		  slidesToShow: 1,
		  slidesToScroll: 1,
		  arrows: true,
		  fade: true,
		//  variableWidth: true,
		  asNavFor: '#portNav'
		});
		$('#portNav').slick({
		  slidesToShow: 5,
		  slidesToScroll: 1,
		  speed: 700,
		  asNavFor: '#portDisplay',
		  focusOnSelect: true,
		  variableWidth: true,
		  centerMode:true,
		  centerPadding: 0
		});
	}
	/** porfolio slider nav end */
	
	/** infopage slider start */
	if( $('#infoSlider').length){
		$('#infoSlider').slick({
			dots: true,
			speed: 500,
			slidesToShow: 1,
			slidesToScroll: 1,
			fade: true,
			variableWidth: false,
			appendArrows: $('.info-slider-nav .w'),
			appendDots: $('.info-slider-nav .w'),
			prevArrow: '<a class="slick-prev-sm">Previous</a>',
			nextArrow: '<a class="slick-next-sm">Next</a>',
		});
	}
	/** infopage slider end */


	
	/** promo slider start */
	if( $('#promo').length){

		$('#promo').on('beforeChange', function(e, slickSlider, i, j){
			$('.pr-nav li').removeClass('slick-active');
			$('.pr-nav li').eq(j).addClass('slick-active'); 
		}); 


		$('#promo').slick({
			speed: 800,
			fade: true,
			slidesToShow: 1,
			slidesToScroll: 1,
			autoplay: true
		});

		$('.pr-nav li').on('click', function(e) {
			var n = $(this).index();
			$('#promo').slick('slickGoTo', parseInt(n));
			$(this).siblings().removeClass('slick-active');
			$(this).addClass('slick-active');
		});
		
	}

	$('.pr-nav li').eq(0).addClass('slick-active');
	/** promo slider end */

/*** sliders end */

	
});
