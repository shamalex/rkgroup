$(document).ready(function () {

/*** header catalogue menu */

	//https://github.com/kamens/jQuery-menu-aim

		// если в локальном хранилище уже есть данные о кликнутом фильтре, то центрируем его
		if(localStorage.getItem('label')) {
			var new_label = localStorage.getItem('label');
			$( ".filter label" ).each(function() {
				if ($(this).html() == new_label) {
					var elementClick = $(this);
					var height = outerHeight/2.5;
					var destination = $(elementClick).offset().top-height;
					jQuery("html:not(:animated),body:not(:animated)").animate({scrollTop: destination}, 0);
				}
			});
		}

		//Плавная анимация в центр выбранного фильтра
		$(".filter label").click(function () {
			var elementClick = $(this);
			var height = outerHeight/2.5;
			var destination = $(elementClick).offset().top-height;

			/*проверка инпута для центрирования*/
			var label = $(this).html();
			if ($(this).closest('.checkline').find('input').attr('checked') == 'checked') {
				localStorage.removeItem('label');
			} else {
				/* Save scroll position */
				localStorage.setItem('label',label);
			}

			return false;
		});
		$('.filter .checkset input').on('ifChanged', function(event){
				var elementClick = $(this);
				var height = outerHeight/2.5;
				var destination = $(elementClick).offset().top-height;
				return false;
		});
		//поиск элементов в обласити видимости экрана
		!function(a){"function"==typeof define&&define.amd?define(["jquery"],a):"object"==typeof exports?module.exports=a(require("jquery")):a(jQuery)}(function(a){function i(){var b,c,d={height:f.innerHeight,width:f.innerWidth};return d.height||(b=e.compatMode,(b||!a.support.boxModel)&&(c="CSS1Compat"===b?g:e.body,d={height:c.clientHeight,width:c.clientWidth})),d}function j(){return{top:f.pageYOffset||g.scrollTop||e.body.scrollTop,left:f.pageXOffset||g.scrollLeft||e.body.scrollLeft}}function k(){if(b.length){var e=0,f=a.map(b,function(a){var b=a.data.selector,c=a.$element;return b?c.find(b):c});for(c=c||i(),d=d||j();e<b.length;e++)if(a.contains(g,f[e][0])){var h=a(f[e]),k={height:h[0].offsetHeight,width:h[0].offsetWidth},l=h.offset(),m=h.data("inview");if(!d||!c)return;l.top+k.height>d.top&&l.top<d.top+c.height&&l.left+k.width>d.left&&l.left<d.left+c.width?m||h.data("inview",!0).trigger("inview",[!0]):m&&h.data("inview",!1).trigger("inview",[!1])}}}var c,d,h,b=[],e=document,f=window,g=e.documentElement;a.event.special.inview={add:function(c){b.push({data:c,$element:a(this),element:this}),!h&&b.length&&(h=setInterval(k,250))},remove:function(a){for(var c=0;c<b.length;c++){var d=b[c];if(d.element===this&&d.data.guid===a.guid){b.splice(c,1);break}}b.length||(clearInterval(h),h=null)}},a(f).on("scroll resize scrollstop",function(){c=d=null}),!g.addEventListener&&g.attachEvent&&g.attachEvent("onfocusin",function(){d=null})});

		var $init = $('body').find('.results-line .btn.btn-to-bsk,.results-block .btn.btn-to-bsk, .buy-btns .btn.btn-to-bsk, .slick-slider .btn.btn-to-bsk');
		if ($init.length) {
		    $init.bind('inview', function(event, isInView) {
		          var $this = $(this);
		          if (isInView) {
		              $this.addClass('blink');
		          } else {
		              $this.removeClass('blink');
		          }
		    });
		}

		//Пробегающий блик на кнопке В корзину
		setRandomClass();
		setInterval(function () {
		    setRandomClass();
		}, 7000);

		function setRandomClass() {
		    //var ul = $(".btn.btn-to-bsk");
		    if ($(".results-block").is(":visible") == true) {
		    	var items = $('body .results-block').find(".btn.btn-to-bsk.blink");
			    items.removeClass("special");
			    var number = items.length;
			    var random = Math.floor((Math.random() * number));
			    setTimeout(function() {
			    	items.eq(random).addClass("special");
			    }, 100);
		    }
            else {
			    var items_line = $('body .results-line').find(".btn.btn-to-bsk.blink");
			    items_line.removeClass("special");
			    var number_line = items_line.length;
			    var random_line = Math.floor((Math.random() * number_line));
			    setTimeout(function() {
			    	items_line.eq(random_line).addClass("special");
			    }, 100);
            }
            var items_all = $('body .card.c, .slick-slider').find(".btn.btn-to-bsk.blink");
			    items_all.removeClass("special");
			    var number_all = items_all.length;
			    var random_all = Math.floor((Math.random() * number_all));
			    setTimeout(function() {
			    	items_all.eq(random_all).addClass("special");
			    }, 100);
		}

		$('#cardGal-Nav').on('click', 'li', function(event) {
			event.preventDefault();
			/* Act on the event */
			var bg_url = $(this).attr('data-image');
			console.log(bg_url)
			$('.print_only.main img').attr('src', bg_url);
		});



		$("#menuCatalogue > li[data-submenu-id] > a").on('click', function(e) {
			e.preventDefault();
		});

        var $menu = $("#menuCatalogue");

        $menu.menuAim({
            activate: activateSubmenu,
            deactivate: deactivateSubmenu,
		 	exitMenu: function() {
				return true;
				},
			submenuDirection: "below",
			rowSelector: "> li[data-submenu-id]"
        });

        function activateSubmenu(row) {
            var $row = $(row),
                submenuId = $row.data("submenuId"),
                $submenu = $("#" + submenuId);
            $submenu.css({
                display: "block"
            });

			$row.addClass("liHover");
            $row.find("a").addClass("maintainHover");
        }

        function deactivateSubmenu(row) {
            var $row = $(row),
                submenuId = $row.data("submenuId"),
                $submenu = $("#" + submenuId);

            $submenu.css("display", "none");
			$("li.liHover").removeClass("liHover");
            $row.find("a").removeClass("maintainHover");
        }

        $('.tooltip').tooltipster({
        	animation: "fade",
			delay: 200,
			touchDevices: true,
			interactive: true,
			position: "bottom"
		});

/*** header catalogue menu end */

	//popus adding into head to div class .popup-callbacks, move to file popup-callbacks
	$('.popup-callbacks').load(window.SITE_TEMPLATE_PATH+'/include/popup-callbacks.php',function(){
		$("#popCallback form").ajaxForm({success: callbackResponse/*, type: 'json'*/});
		$("#popCallback form input[name=phone]").mask("+7(999)999-99-99");
	});
	$('.popup-feedbacks').load(window.SITE_TEMPLATE_PATH+'/include/popup-feedbacks.php',function(){
		$("#popFeedback form").ajaxForm({success: feedbackResponse/*, type: 'json'*/});
		$("#popFeedback form input[name=phone]").mask("+7(999)999-99-99");
	});
	$('.popup-changebasket').load(window.SITE_TEMPLATE_PATH+'/include/popup-changebasket.php',function(){
		$('#popChangeBasket').bPopup(
			{
				closeClass:'x',
				modalColor: '#415562',
				opacity:0.98,
				position: ["auto",50],
				onOpen: function() { }, // validate
				onClose: function() { $.cookie('RUSKLIMAT_CHANGE_BASKET', null); } //clear
			}
		);
	});
	$('.popup-kp').load(window.SITE_TEMPLATE_PATH+'/include/popup-kp.php',function(){
		$("#popKP form").ajaxForm({success: kpResponse/*, type: 'json'*/});
		$("#popKP form input[name=phone]").mask("+7(999)999-99-99");
		$("#popKP form input[name=product_link]").val(window.location.href);
	});
	if($("div").is(".popup-ordermessage")){
		$('.popup-ordermessage').load(window.SITE_TEMPLATE_PATH+'/include/popup-ordermessage.php',function(){
			$("#popOrdermessage form").ajaxForm({success: ordermessageResponse /*, type: 'json'*/});
		});
	}
	if($("div").is(".popup-ordercancel")){
		$('.popup-ordercancel').load(window.SITE_TEMPLATE_PATH+'/include/popup-ordercancel.php',function(){
			$("#popOrdercancel form").ajaxForm({success: ordercancelResponse /*, type: 'json'*/});
		});
	}
	//add text for divs with class .st-no-text
	$('.st-no-text').text('нет в наличии');
	//all divs with class past move to file past.php
	$('.past-inject').load(window.SITE_TEMPLATE_PATH+'/include/past.php');
	$('#popSelf').load(window.SITE_TEMPLATE_PATH+'/include/popSelf.php');
	$('#popupClicks').load(window.SITE_TEMPLATE_PATH+'/include/popupClicks.php');
	$('#popCredit').load(window.SITE_TEMPLATE_PATH+'/include/popCredit.php');



/*** fixed header */

	window.need_to_show_fixed_header = false;
	var heyCheckMeOut = function(e) {
		var hd = $('.fix-hd-w'),
			its = $('.fix-hd-w .item[data-full="1"]');

		if (($(window).scrollTop() > 116 && its.length) || window.need_to_show_fixed_header) {
			if (!hd.is(':visible')) {
				hd.slideDown(200);
			}
		} else {
			if (hd.is(':visible')) {
				hd.slideUp(200);
			}
		}
	}
	window.heyCheckMeOut = heyCheckMeOut;

	$(window).scroll(function(e) {
		new heyCheckMeOut;
	});

/*** fixed header end */


/*** custom checkbox and radio */
$('.checkset input, .check-custom').iCheck({
    checkboxClass: 'icheckbox_minimal',
    radioClass: 'iradio_minimal',
	activeClass: 'active'
});

/*** custom checkbox and radio end */


/*** popups */

	/** city pop-up */
	$('body').on('click', '.city-pop', function(e) {

		e.preventDefault();

		$('#popCity').bPopup(
				{
					closeClass:'x',
					modalColor: '#415562',
					opacity:0.98,
					position: ["auto",50]
				}
		);

		// scrollbars for city pop-up
		$(".region .scroll, .cities .scroll").mCustomScrollbar({
			theme:"dark",
			scrollButtons:{ enable: true },
			autoDraggerLength: false
		});

		$(".region .scroll").mCustomScrollbar('scrollTo', $('.reg-ul li.active'));
		// scrollbars for city pop-up end

	});
	/** city pop-up end */



	/** callback pop-up */
	$('.callback-pop').on('click', function(e) {

		e.preventDefault();

		$('#popCallback').bPopup(
				{
					closeClass:'x',
					modalColor: '#415562',
					opacity:0.98,
					position: ["auto",50],
					onOpen: function() { }, // validate
					onClose: function() { $('#popCallback form').show();$("#popCallback h1.ttl").show();$('#popCallback div.success').remove();$('#popCallback div.error').remove();$('#popCallback form input,#popCallback form textarea').val(''); } //clear
				}
		);

	});

	$('.call-field').each(function() {
		var cur_placeholder = $(this).attr('placeholder');
		$(this).on('focus', function() {
			if ($(this).val() == '') {
				$(this).attr('placeholder', '');
			}
		}).on('blur', function() {
			if ($(this).val() == '') {
				$(this).attr('placeholder', cur_placeholder);
			}
		});
	});

	//send
	$('body').on('click', '.test-callback-pop-ok', function(e) {

		$("#popCallback form").submit();

	});

	// post-submit callback
	function callbackResponse(responseText, statusText, xhr, $form)  {
		responseObject=JSON.parse(responseText);
		$('#popCallback form').show();
		$("#popCallback .call-field").removeClass("error");
		$("#popCallback .comment").text("");
		$('#popCallback div.success').remove();
		$('#popCallback div.error').remove();

		if(responseObject.status=="error"){
			$(responseObject.message).prependTo($("#popCallback .call-form"));
			for (var key in responseObject.error) {
				$("#popCallback .call-field[name="+key+"]").addClass("error");
				$("#popCallback .comment[data-name="+key+"]").text(responseObject.error[key]);
			}
		}else if(responseObject.status=="ok"){
			$(responseObject.message).prependTo($("#popCallback .call-form"));
			$("#popCallback h1.ttl").hide();
			$("#popCallback .call-form form").hide();
		}
	}
	/** callback pop-up end */

	/** feedback pop-up */
	$('.feedback-pop').on('click', function(e) {

		e.preventDefault();

		$('#popFeedback').bPopup(
				{
					closeClass:'x',
					modalColor: '#415562',
					opacity:0.98,
					position: ["auto",50],
					onOpen: function() { }, // validate
					onClose: function() { $('#popFeedback form').show();$('#popFeedback div.success').remove();$('#popFeedback div.error').remove();$('#popFeedback form input,#popFeedback form textarea').val(''); } //clear
				}
		);

	});

	$('.feed-field').each(function() {
		var cur_placeholder = $(this).attr('placeholder');
		$(this).on('focus', function() {
			if ($(this).val() == '') {
				$(this).attr('placeholder', '');
			}
		}).on('blur', function() {
			if ($(this).val() == '') {
				$(this).attr('placeholder', cur_placeholder);
			}
		});
	});

	//send
	$('body').on('click', '.test-feedback-pop-ok', function(e) {

		$("#popFeedback form").submit();

	});

	// post-submit feedback
	function feedbackResponse(responseText, statusText, xhr, $form)  {
		responseObject=JSON.parse(responseText);

		$('#popFeedback form').show();
		$("#popFeedback .feed-field").removeClass("error");
		$("#popFeedback .comment").text("");
		$('#popFeedback div.success').remove();
		$('#popFeedback div.error').remove();

		if(responseObject.status=="error"){
			$(responseObject.message).prependTo($("#popFeedback .feed-form"));
			for (var key in responseObject.error) {
				$("#popFeedback .feed-field[name="+key+"]").addClass("error");
				$("#popFeedback .comment[data-name="+key+"]").text(responseObject.error[key]);
			}
		}else if(responseObject.status=="ok"){
			$(responseObject.message).prependTo($("#popFeedback .feed-form"));
			$("#popFeedback .feed-form form").hide();
		}
	}
	/** feedback pop-up end */

	$(document).on('click','.orderSendMessage',function(){
		$('#popOrdermessage input[name=orderId]').val($(this).attr("data-id"));
		$('#popOrdermessage').bPopup(
				{
					closeClass:'x',
					modalColor: '#415562',
					opacity:0.98,
					position: ["auto",50],
					onOpen: function() { }, // validate
					onClose: function() { $('#popOrdermessage form').show();$('#popOrdermessage div.success').remove();$('#popOrdermessage div.error').remove();$('#popOrdermessage form input,#popOrdermessage form textarea').val(''); } //clear
				}
		);
	});

	$('body').on('click', '.test-ordermessage-pop-ok', function(e) {

		$("#popOrdermessage form").submit();

	});

	// post-submit ordermessage
	function ordermessageResponse(responseText, statusText, xhr, $form)  {
		responseObject=JSON.parse(responseText);

		$('#popOrdermessage form').show();
		$("#popOrdermessage .feed-field").removeClass("error");
		$("#popOrdermessage .comment").text("");
		$('#popOrdermessage div.success').remove();
		$('#popOrdermessage div.error').remove();

		if(responseObject.status=="error"){
			$(responseObject.message).prependTo($("#popOrdermessage .feed-form"));
			for (var key in responseObject.error) {
				$("#popOrdermessage .feed-field[name="+key+"]").addClass("error");
				$("#popOrdermessage .comment[data-name="+key+"]").text(responseObject.error[key]);
			}
		}else if(responseObject.status=="ok"){
			$(responseObject.message).prependTo($("#popOrdermessage .feed-form"));
			$("#popOrdermessage .feed-form form").hide();
		}
	}

	$(document).on('click','.orderSendCancel',function(){
		$('#popOrdercancel input[name=orderId]').val($(this).attr("data-id"));
		$('#popOrdercancel').bPopup(
				{
					closeClass:'x',
					modalColor: '#415562',
					opacity:0.98,
					position: ["auto",50],
					onOpen: function() { $('#popOrdercancel form').submit(); }, // validate
					onClose: function() { $('#popOrdercancel form').show();$('#popOrdercancel div.success').remove();$('#popOrdercancel div.error').remove();$('#popOrdercancel form input,#popOrdercancel form textarea').val(''); } //clear
				}
		);
	});

	// post-submit ordermessage
	function ordercancelResponse(responseText, statusText, xhr, $form)  {
		responseObject=JSON.parse(responseText);

		$('#popOrdercancel form').show();
		$("#popOrdercancel .feed-field").removeClass("error");
		$("#popOrdercancel .comment").text("");
		$('#popOrdercancel div.success').remove();
		$('#popOrdercancel div.error').remove();

		if(responseObject.status=="error"){
			$(responseObject.message).prependTo($("#popOrdercancel .feed-form"));
			for (var key in responseObject.error) {
				$("#popOrdercancel .feed-field[name="+key+"]").addClass("error");
				$("#popOrdercancel .comment[data-name="+key+"]").text(responseObject.error[key]);
			}
		}else if(responseObject.status=="ok"){
			$(responseObject.message).prependTo($("#popOrdercancel .feed-form"));
			$("#popOrdercancel .feed-form form").hide();
		}
	}


	/** kp pop-up */
	$('.kp-pop').on('click', function(e) {

		e.preventDefault();

		$('#popKP').bPopup(
				{
					closeClass:'x',
					modalColor: '#415562',
					opacity:0.98,
					position: ["auto",50],
					onOpen: function() { }, // validate
					onClose: function() { $('#popKP form').show();$('#popKP div.success').remove();$('#popKP div.error').remove();$('#popKP form input,#popKP form textarea').val(''); } //clear
				}
		);

	});

	//send
	$('body').on('click', '.test-kp-pop-ok', function(e) {

		$("#popKP form").submit();

	});

	// post-submit callback
	function kpResponse(responseText, statusText, xhr, $form)  {
		responseObject=JSON.parse(responseText);

		$('#popKP form').show();
		$("#popKP .feed-field").removeClass("error");
		$("#popKP .comment").text("");
		$('#popKP div.success').remove();
		$('#popKP div.error').remove();

		if(responseObject.status=="error"){
			$(responseObject.message).prependTo($("#popKP .feed-form"));
			for (var key in responseObject.error) {
				$("#popKP .feed-field[name="+key+"]").addClass("error");
				$("#popKP .comment[data-name="+key+"]").text(responseObject.error[key]);
			}
		}else if(responseObject.status=="ok"){
			$(responseObject.message).prependTo($("#popKP .feed-form"));
			$("#popKP .feed-form form").hide();
		}
	}
	/** kp pop-up end */

	/** subscription pop-up */
	$('#subscribeMeForm').on('submit', function(e) {
		e.preventDefault();
		$.get('/subscribe.php', {'EMAIL': $('#subscribeMeForm input[name="email"]').val()}, function(data) {

			if (data['OK'] == 1) {
				$('#popSubYes').bPopup({
					closeClass:'x',
					modalColor: '#415562',
					opacity:0.98,
					position: ["auto",50]
				});
			} else {
				$('#popSubYes').bPopup().close();
				$('#popSubNo .ttl-p2').html(data['ERROR']);
				$('#popSubNo').bPopup({
					closeClass:'x',
					modalColor: '#415562',
					opacity:0.98,
					position: ["auto",50]
				});
			}

		}, 'json');
	});
	/** subscription pop-up end */

	/** credit pop-up */
	$('.credit-pop').on('click', function(e) {

		e.preventDefault();

		$('#popCredit').bPopup(
				{
					closeClass:'x',
					modalColor: '#415562',
					opacity:0.98,
					position: ["auto",50]
				}
		);
		$('.check-custom').iCheck({
			checkboxClass: 'icheckbox_minimal',
			radioClass: 'iradio_minimal',
			activeClass: 'active'
		});

	});
	/** credit pop-up end */

	/** self pop-up */
	$('.self-pop').on('click', function(e) {

		e.preventDefault();

		$('#popSelf').bPopup(
				{
					closeClass:'x',
					modalColor: '#415562',
					opacity:0.98,
					position: ["auto",50]
				}
		);
		$('.check-custom').iCheck({
			checkboxClass: 'icheckbox_minimal',
			radioClass: 'iradio_minimal',
			activeClass: 'active'
		});

	});
	/** self pop-up end */


	/** one click order pop-up */
	$('.one-click-pop').on('click', function(e) {

		e.preventDefault();

		$('#popOneClick').bPopup(
				{
					closeClass:'x',
					modalColor: '#415562',
					opacity:0.98,
					position: ["auto",50],
					onOpen: function() { new oneclickValid(); }, // validate
					onClose: function() { $('#oneClickPhone').val(''); } //clear
				}
		);

		if (typeof window.yandexID !== "undefined" && typeof window['yaCounter'+window.yandexID] !== "undefined") {
			window['yaCounter'+window.yandexID].reachGoal('KUPIT_KLIK');
		}

	});

	//success
	$('body').on('click', '.test-one-click-pop-ok', function(e) {

		e.preventDefault();

		$('#popOneClickYes').bPopup(
				{
					closeClass:'x',
					modalColor: '#415562',
					opacity:0.98,
					position: ["auto",50]
				}
		);
		$('#popOneClick').bPopup().close();

		$.get('/send_oneclick.php', {'PHONE': $('#popOneClick #oneClickPhone').val(), 'URL': window.location.href}, function(data) {

			if (typeof window.yandexID !== "undefined" && typeof window['yaCounter'+window.yandexID] !== "undefined") {
				window['yaCounter'+window.yandexID].reachGoal('KUPIT_KLIK_SDELAN');
			}

		}, 'html');

	});

	//fail
	$('body').on('click', '.test-one-click-pop-fail', function(e) {

		e.preventDefault();

		$('#popOneClickNo').bPopup(
				{
					closeClass:'x',
					modalColor: '#415562',
					opacity:0.98,
					position: ["auto",50]
				}
		);
		$('#popOneClick').bPopup().close();
		$('#popOneClickYes').bPopup().close();

	});

	/** one click order pop-up end */



/*** popups end */


/*** autocomplete */

	/** header & 404 search */
	// data sample
	var autocompleteTest = [
		{ value: 'Пусть компьютер отдохнет,', data: 'line1' },
		{ value: 'Монитор пускай заснет,', data: 'line2' },
		{ value: 'Мышь день отдыха имеет.', data: 'line3' },
		{ value: 'Не до них нам всем сейчас.', data: 'line4' },
		{ value: 'Мы пришли поздравить вас', data: 'line5' },
		{ value: 'С вашим славным юбилеем.', data: 'line6' },
		{ value: 'Служба службой, но она', data: 'line7' },
		{ value: 'Будни занимать должна.', data: 'line8' },
		{ value: 'В праздник нужно веселиться.', data: 'line9' },
		{ value: 'Стол накрыть, на стулья сесть, Пить вино и вкусно есть,', data: 'line10' },
		{ value: 'Видеть радостные лица.', data: 'line12' },
		{ value: 'Скажет громко тамада:', data: 'line13' },
		{ value: '«Что нам возраст, что года!', data: 'line14' },
		{ value: 'Ведь душа, она – гитара:', data: 'line15' },
		{ value: 'Если струны есть — поет…»', data: 'line16' },
		{ value: 'И воскликнет весь народ: «Честь и слава юбиляру!»', data: 'line17' },
		{ value: 'И за тостом тост пойдет,', data: 'line19' },
		{ value: 'И один другого лучше.', data: 'line20' },
		{ value: 'А компьютер подождет. Что ему? Ведь он непьющий…', data: 'line21' },
		{ value: 'abcdefghijklmnopqrstuvwxyz', data: 'abc' }
	];

	//autocomplete header
	/*
	$('#hdSearchAutocomplete').autocomplete({
		lookup: autocompleteTest,
		lookupLimit: 10,
		autoSelectFirst: true,
		maxHeight: 'auto',
		appendTo: '.hd-srch',
		onSelect: function (suggestion) {
		   // alert('You selected: ' + suggestion.value + ', ' + suggestion.data);
		}
	});
	 */

	//autocomplete 404
	/*
	$('#searchAutocomplete404').autocomplete({
		lookup: autocompleteTest,
		lookupLimit: 10,
		autoSelectFirst: true,
		maxHeight: 'auto',
		appendTo: '.srch-404-holder',
		onSelect: function (suggestion) {
		   // alert('You selected: ' + suggestion.value + ', ' + suggestion.data);
		}
	});
	 */

	//autocomplete search results
	/*
	$('#searchAutocompleteResults').autocomplete({
		lookup: autocompleteTest,
		lookupLimit: 10,
		autoSelectFirst: true,
		maxHeight: 'auto',
		appendTo: '.srch-holder',
		onSelect: function (suggestion) {
		   // alert('You selected: ' + suggestion.value + ', ' + suggestion.data);
		}
	});
	 */
/*** autocomplete end */





/** blocks with timer */
if( $('.time').length){
	$('[data-countdown]').each(function() {
		var $this = $(this), finalDate = $(this).data('countdown');
		$this.countdown(finalDate, function(event) {
			$this.html(event.strftime('<b>%D</b> <span>дн.</span> <b>%H</b> <span>ч.</span> <b>%M</b> <span>мин.</span> <b class="sec">%S</b> <span>сек.</span>'));
		});
	});
}
/** blocks with timer end */

/** test fixed header popup */
	$(document).on('click', '.btn-to-bsk, #btnToBasket', function(e) {
		var href = $(this).attr('href');
		if (href && href.indexOf('ADD2BASKET') + 1) {
			e.preventDefault();
			BX.showWait();
			$.get(href, function(data) {
				BX.closeWait();
				var addDelay = 0;
				window.need_to_show_fixed_header = true;
				if (!$('.fix-hd-w').is(':visible')) {
					$(window).trigger('scroll');
					addDelay = 200;
				}
				if ($('#linkForUpdateBasket').length) {
					$('#linkForUpdateBasket').trigger('click');
				}
				$('#bskHint').show().slideUp(0).delay(addDelay).slideDown(300).delay(3000).slideUp(300).queue(function() {
					$(this).dequeue();
					window.need_to_show_fixed_header = false;
					BX.onCustomEvent('OnBasketChange');
				});
			}, 'html');
			var addDelay = 200;
			$(this).addClass("in_basket").delay(addDelay).text("Уже в корзине");
			var data_id = $(this).attr('data-id');
			$('.results-line,.results-block').find("[data-id='" + data_id + "']").text("Уже в корзине").addClass("in_basket");
		}
	});
	$(document).on('click', '.btn-to-comp', function(e) {
		e.preventDefault();
		$('#compHint').show().slideUp(0).slideDown(300).delay(3000).slideUp(300);
	});
	$(document).on('click', '.btn-to-fav', function(e) {
		e.preventDefault();
		$('#favHint').show().slideUp(0).slideDown(300).delay(3000).slideUp(300);
	});

	// card series and side
	$(document).on('ifChecked ifUnchecked', '.tbl-series .icheckbox_minimal, #addComp, .addComp', function(event){

		var href = event.type == 'ifUnchecked' ? $(this).attr('-data-href2') : $(this).attr('-data-href');
		if (href && href != '' && (event.type == 'ifUnchecked' ? href.indexOf('DELETE_FROM_COMPARE_LIST') + 1 : href.indexOf('ADD_TO_COMPARE_LIST') + 1)) {
			// e.preventDefault();
			if(typeof $(this).attr('-data-addid') !== 'undefined' && $(this).attr('-data-addid') != '') {
				$('#'+$(this).attr('-data-addid')).attr('checked', event.type == 'ifUnchecked' ? false : true).iCheck('update');
			}
			BX.showWait();
			$.get(href, function(data) {
				BX.closeWait();
				var addDelay = 0;
				window.need_to_show_fixed_header = true;
				if (!$('.fix-hd-w').is(':visible')) {
					$(window).trigger('scroll');
					addDelay = 200;
				}
				$(event.type == 'ifUnchecked' ? '#compHint2' : '#compHint').show().slideUp(0).delay(addDelay).slideDown(300).delay(3000).slideUp(300).queue(function() {
					$(this).dequeue();
					window.need_to_show_fixed_header = false;
					BX.onCustomEvent('OnCompareChange');
				});
			}, 'html');
		} else if(typeof $(this).attr('-data-mainid') !== 'undefined' && $(this).attr('-data-mainid') != '') {
			$('#'+$(this).attr('-data-mainid')).attr('checked', event.type == 'ifUnchecked' ? false : true).iCheck('update').trigger(event.type);
		}
	});

	$(document).on('ifChecked ifUnchecked', '#addFav, .addFav', function(event){
		console.log($(this));

		if (typeof $(this).attr('-data-id') !== 'undefined' && $(this).attr('-data-id') != '') {
			if(typeof $(this).attr('-data-addid') !== 'undefined' && $(this).attr('-data-addid') != '') {
				$('#'+$(this).attr('-data-addid')).attr('checked', event.type == 'ifUnchecked' ? false : true).iCheck('update');
			}
			BX.showWait();
			$.get('/add_to_fav.php?ID='+$(this).attr('-data-id')+'&action='+(event.type == 'ifUnchecked' ? 'del' : 'add'), function(data) {
				BX.closeWait();
				var addDelay = 0;
				window.need_to_show_fixed_header = true;
				if (!$('.fix-hd-w').is(':visible')) {
					$(window).trigger('scroll');
					addDelay = 200;
				}
				$(event.type == 'ifUnchecked' ? '#favHint2' : '#favHint').show().slideUp(0).delay(addDelay).slideDown(300).delay(3000).slideUp(300).queue(function() {
					$(this).dequeue();
					window.need_to_show_fixed_header = false;
					$('#fav_header_wrap').html(data);
				});
				console.log(event.target.id);
				if(event.type == 'ifUnchecked') $("label[for="+event.target.id+"]").text("В избранное");
				else $("label[for="+event.target.id+"]").text("В избранном");
			}, 'html');
		} else if(typeof $(this).attr('-data-mainid') !== 'undefined' && $(this).attr('-data-mainid') != '') {
			$('#'+$(this).attr('-data-mainid')).attr('checked', event.type == 'ifUnchecked' ? false : true).iCheck('update').trigger(event.type);
		}

	});

/** test fixed header popup end */





/*** card gallery */

//360
function initRotate(){
	var rotate;
	/*
	rotate = $('.rotate').ThreeSixty({
		totalFrames: 36, // Total no. of image you have for 360 slider
		endFrame: 36, // end frame for the auto spin animation
		currentFrame: 1, // This the start frame for auto spin
		imgList: '.threesixty_images', // selector for image list
		progress: '.spinner', // selector to show the loading progress
		imagePath:'i/rotate/', // path of the image assets
		filePrefix: '', // file prefix if any
		ext: '.jpg', // extention for the assets
		height: 448,
		width: 448,
		navigation: false
	});
	*/
}
// if( $('.threesixty').length){
	// new initRotate();
// }


//card gallery
function cardGal(){
	$('.c-gal').on('click', 'a', function(e) {

		e.preventDefault();

		if (!$(this).hasClass("active")) {

			var that = $(this),
				href = that.attr('href'),
				navID = '#' + that.closest('.c-gal').attr('id'),
				picID = (navID).replace('-Nav', '-Pic'),
				pic = $(picID);

			$(navID+' a').removeClass('active');
			that.addClass("active");

			if (!$(this).parent().hasClass("d3")) {
				// $('.threesixty').hide();
				pic.css("background-image", "url('"+href+"')").html('').removeClass('at3d');
			}

			else if ($(this).parent().hasClass("d3")) {
				// $('.threesixty').show();
				// pic.css("background-image", "none").html('<iframe src="'+href+'"></iframe>');
				pic.css("background-image", "none").html('<object width="100%" height="100%"><param name="movie" value="'+href+'"><embed src="'+href+'" width="100%" height="100%"></embed></object>').addClass('at3d');
			}

		}
		//end if active

	});
}
new cardGal();

//card popup
$('.maximize').on("click", function(e){
	e.preventDefault();
		var that = $(this),
			picID = $('.maximize .c-pic').attr('id'),
			navID = (picID).replace('-Pic', '-Nav'),
			href = $('#' + navID + ' .active').attr('href'),
			buildGal = $('#' + navID).html(),
			buildModal =  '<div class="w"><a href="#" class="x">&nbsp;</a><div class="c-pic pic" id="' + picID + '-pop" style="background-image:url(' + href + ')"><div class="threesixty car rotate"><div class="spinner"><span>0%</span></div><ol class="threesixty_images"></ol></div></div><ul class="c-gal c" id="' + navID + '-pop">' + buildGal + '</ul></div>';

		e.preventDefault();
		$('<div class="pop pop-card pop-bg-1" style="width:750px" id="popCard" />').html(buildModal).bPopup(
			{
				closeClass:'x',
				modalColor: '#415562',
				opacity:0.98,
				position: ["auto",50],
				onClose: function() { $(this).remove(); }
			}
		);
		new cardGal();

		new initRotate();

		// if( $('#popCard .d3').children().hasClass('active') ){
			// $('#popCard .threesixty').show();
		// }


});

/*** card gallery end */

/*** card tabs */

if( $('#cardTabs').length){
	function loadCardTabsMake() {
		$('#cardTabs .tab.hide_it2').each(function() {
			$(this).removeClass('hide_it2').css('width', '');
			if (!$(this).hasClass('active')) {
				$(this).hide();
			}
		});
	}
	function loadCardTabs() {
		if (typeof google != "undefined") {
			$('#cardTabs .tab').each(function() {
				$(this).width($(this).width());
			});
		}
		$('#cardTabs').easytabs({
			updateHash: false,
			collapsedByDefault: false
		});
		if (typeof google != "undefined") {
			$('#cardTabs .tab:not(.active)').addClass('hide_it2').show();
			google.maps.event.addDomListener(window, 'load', loadCardTabsMake);
		}
	}
	// if (typeof google != "undefined") {
		// google.maps.event.addDomListener(window, 'load', loadCardTabs);
	// } else {
		// loadCardTabs();
	// }
	loadCardTabs();
}

/*** card tabs end */

/*** rating start */

if( $('.stars').length){
	$('.stars').each(function() {
		var score = $(this).attr('-data-score');
		$(this).raty({
			hints: ['1', '2', '3', '4', '5'],
			score: parseInt(score),
			readOnly: ($(this).attr('-data-canvote') == 1 ? false : true),
			path: window.SITE_TEMPLATE_PATH+'/i/d/rate',
			noRatedMsg: "Этот товар еще никто не оценивал"
		});
	});

}

/*** rating end */

/*** catalogue view switch */

$('#catControl a').on("click", function(e){
	e.preventDefault();
	if (!$(this).hasClass("active")) {

		var that = $(this),
			holder = $('.cat-types');

		$('#catControl a').removeClass('active');
		that.addClass('active');

		if (that.hasClass("tldr")) {
			holder.removeClass('with').addClass('without')
		}
		else {
			holder.removeClass('without').addClass('with')
		}

	}

});

/*** catalogue view switch end */


/*** catalogue filter */

	//switch filter view
	$('#filterSwitcher').on('click', 'a', function(e) {
		e.preventDefault();

		if (!$(this).parent().hasClass("active")) {

			var that = $(this),
				li = that.parent();
				//holder = $('.cat-types');

			$('#filterSwitcher li').removeClass('active');
			li.addClass('active');

		}

	});


if( $('.hider').length){



	//hide or show items in filter
	$('.hider').on('click', function(e) {

		e.preventDefault();

		var that = $(this),
			item = that.parent().parent(),
			hideme = item.find('.hideme');

		if (that.hasClass("minus")) {
			that.removeClass('minus').addClass('plus');
			if(that.parent().is(".cont")) that.text('Показать все');
			hideme.slideUp(200);
			item.removeClass('open');
		}
		else if (that.hasClass("plus")) {
			that.removeClass('plus').addClass('minus');
			if(that.parent().is(".cont")) that.text('Свернуть');
			hideme.slideDown(200);
			item.addClass('open');

		}

	});
}

/*** catalogue filter end */


/*** catalogue filter result view switch */

$(document).on("click", '#resultsControl a', function(e){
	e.preventDefault();
	if (!$(this).hasClass("active")) {

		var that = $(this);

		$('#resultsControl a').removeClass('active');
		that.addClass('active');

 		if (that.hasClass("tiles")) {
			document.cookie = "catalog-result-type=line";
			$('.results-line').fadeIn(300);
			$('.results-block').fadeOut(250);
		}
		else {
			document.cookie = "catalog-result-type=block";
			$('.results-block').fadeIn(250);
			$('.results-line').fadeOut(250);
		}

	}

});

/*** catalogue view switch end */

/*** remove blocks in Favorites, Compare */

function favMidCount(){
	var its = $(".favorites .item").length;
	$("#favMidCount").text(its);
}

$('.b-x').on("click", function(e){
	e.preventDefault();
	$(this).parent().remove();
	new favMidCount();
});

if( $('.favorites').length){
	//favorites block count

	new favMidCount();
}

/*** remove blocks in Favorites, Compare end */

/*** remove rows from basket */

if ($('.td-x .x').length || $('#linkForUpdateBasket').length) {
	$('body').on("click", '.td-x .x', function(e){
		// e.preventDefault();
		var that = $(this),
			parent = that.parent().parent('tr');

		//delete separator after
		if( parent.next().length){
			parent.next().remove();
		}
		//delete separator before
		else {
			parent.prev().remove();
		}
		//delet row
		parent.remove();

		setTimeout(function() {
			BX.onCustomEvent('OnBasketChange');
			if ($('#ORDER_FORM').length) {
				submitForm();
			}
		}, 1000);

	});
}

/*** remove rows from basket end */



/***  basket - +  */

if ($(".amt-bt").length || $('#linkForUpdateBasket').length) {
	$("body").on("click", ".amt-bt", function() {

	  var $button = $(this);
	  var oldValue = $button.parent().find(".bsk-amt").val();

	  if ($button.text() == "+") {
		  var newVal = parseFloat(oldValue) + 1;
		} else {
	   // Don't allow decrementing below zero
		if (oldValue > 1) {
		  var newVal = parseFloat(oldValue) - 1;
		} else {
		  newVal = 1;
		}
	  }
		if (oldValue != newVal) {
			$button.parent().find(".bsk-amt").val(newVal).trigger('change');
		}

	});
}

// $(".bsk-amt").on("keyup", function() {
	// var $button = $(this);
	// $button.parent().find(".bsk-amt").trigger('change');
// });

/***  basket - +  end */

/*** order switch */

$('body').on("click", ".order-switch a", function(e){
	e.preventDefault();
	if (!$(this).hasClass("active")) {

		var that = $(this),
			holder = that.parent().parent();

		that.siblings().removeClass('active');
		that.addClass('active');

		var switch_is = that.parents('.order-switch');
		if (switch_is.is('#switchYurFiz')) {
			var cur_id = $('label', that).attr('for');
			$('#'+cur_id).attr('checked', true).trigger('click');
		} else if (switch_is.is('#switchDost')) {
			//samo
			if (that.is("#samo")) {
				// $("#basketDelAdr").slideUp(200);
				$("#basketDelAdr").hide();
				$("#dostTbl").hide();
				$("#samTbl").show();
				$('#samTbl input[type="radio"]').first().prop('checked', true).trigger('click').trigger('ifChecked');
			}
			//dost
			else if (that.is("#dost")) {
				// $("#basketDelAdr").slideDown(200);
				$("#basketDelAdr").show();
				$("#dostTbl").show();
				$("#samTbl").hide();
				$('#dostTbl input[type="radio"]').first().prop('checked', true).trigger('click').trigger('ifChecked');
			}
		}

	}

});

/*** order switch end */
/* remember active tab in contact info*/
$('body').on("click", "#switchYurFiz a", function(){
	var tabYurFiz = $(this).attr("name");
	localStorage.setItem("tabYurFiz", tabYurFiz);
	console.log(tabYurFiz)
});
var currTabYurFiz = localStorage.getItem("tabYurFiz");
$('a[name="' + currTabYurFiz + '"]').trigger("click");
console.log(currTabYurFiz)

$('body').on("click", "#switchDost a", function(){
	var tabDost = $(this).attr("name");
	localStorage.setItem("tabDost", tabDost);
	console.log(tabDost)
});

setTimeout(function() { var currTabDost = localStorage.getItem("tabDost");
$('a[name="' + currTabDost + '"]').trigger("click");
console.log(currTabDost) }, 4000);

;(function($){

    /**
     * Store scroll position for and set it after reload
     *
     * @return {boolean} [loacalStorage is available]
     */
    $.fn.scrollPosReaload = function(){
        if (localStorage) {
            var posReader = localStorage["posStorage"];
            if (posReader) {
                $(window).scrollTop(posReader);
                localStorage.removeItem("posStorage");
            }
            $(this).click(function(e) {
                localStorage["posStorage"] = $(window).scrollTop();
            });

            return true;
        }

        return false;
    }

    /* ================================================== */

    $(document).ready(function() {
        // Feel free to set it for any element who trigger the reload
        $('body #order_form_div').scrollPosReaload();
    });

}(jQuery));



/*** custom selects */

// catalogue filter result selects

$('#sortSelect,#sortSelect_bottom').ikSelect({
	autoWidth: false,
	ddFullWidth: false,
	customClass: 'sort-select'
});


$('#amountSelect,#amountSelect_bottom').ikSelect({
	autoWidth: false,
	ddFullWidth: false,
	customClass: 'amount-select'
});

//basket selects

$('#payMethodSelect').ikSelect({
	autoWidth: false,
	ddFullWidth: false,
	customClass: 'pay-method-select'
});

/*
$('#payMethodSelect').on('change', function() {
	//hide old/other tooltips
	$.powerTip.hide();

	//get text val
	var val = $(this).val(),
		text = $("#payMethodSelect option[value='"+ val +"']").text();
	//alert(text);

	//submit button text
	$('#payText').text(text);
	//tooltip body text
	$('#ttPayMethod .tt-dsc p').text(text);


	//tooltip
	$('.pay-method-select').powerTip({ manual: true , placement: 'e'});
	$('.pay-method-select').data('powertiptarget', 'ttPayMethod');

	$.powerTip.show($('.pay-method-select'));


});
*/



//gmap select
$('#map-select').ikSelect({
	autoWidth: false,
	ddFullWidth: false,
	customClass: 'map-select'
});

$('.budget-select').ikSelect({
	autoWidth: false,
	ddFullWidth: false,
	customClass: 'ik-30-lt'
});

//scrollbar in select
$('.ik_select_list_inner').mCustomScrollbar({
	theme:"map-select",
	scrollButtons:{ enable: true },
	autoDraggerLength: false
});

/*** custom selects end */

/***  basket tooltips */

//fake tooltips for phone and email if when matches already registered
if( $('.ttPhone').length){

	$('.ttPhone').on("click", function(e) {
		$('#ttPhone').css('display', 'block');

	});

 	$('.btn-tt-sms1').on("click", function(e) {
		e.preventDefault();
		$("#ttSms1").hide();
		$("#ttSms2").show();
	});
 	$('.btn-tt-sms2').on("click", function(e) {
		e.preventDefault();
		$('#ttPhone').css('display', 'none');
	});
}

if( $('.ttEmail').length){

	$('.ttEmail').on("click", function(e) {
		$('#ttEmail').css('display', 'block');

	});

 	$('.btn-tt-em1').on("click", function(e) {
		e.preventDefault();
		$("#ttEm1").hide();
		$("#ttEm2").show();
	});
 	$('.btn-tt-em2').on("click", function(e) {
		e.preventDefault();
		$('#ttEmail').css('display', 'none');
	});
}

/***  basket tooltips end */

/***  basket map show/hide */

$("body").on("click", ".sm-map-pop", function() {
	var rel = $(this).parent().parent().find('.relat .sm-map');
	var par = rel.parents('.relat');


	if (!par.hasClass('hide_it')) {

		rel.slideUp(200, function() {
			par.addClass('hide_it');
		});
	}
	else {
		par.removeClass('hide_it');
		rel.hide().slideDown(200);
	}

});
$("body").on("click", ".map-x", function() {
	var par = $(this).parents('.relat');
	var rel = par.find('.sm-map');
	rel.slideUp(200, function() {
		par.addClass('hide_it');
	});
});


/***  basket map show/hide end */


/*** scroll to top */

var $root = $('html, body');
$('.to-top a').click(function(e) {
	e.preventDefault();
    var href = $.attr(this, 'href');

    $root.animate({
        scrollTop: $(href).offset().top-50
    }, 500);
    return false;
});
/*** scroll to top end */


/*** range bar */

if( $('body').find('.slider-input').length){

	$( '.slider-input' ).each(function() {
		var $this = $(this);
		var rangeInputs = function(e) {
			var values = $this.val().split(',');
			$this.closest('.range').find($('.min-price')).val(values[0]);
			$this.closest('.range').find($('.max-price')).val(values[1]);
		}
		var	dragendRange =function(e){
			$("#set_filter").click();
		}

		var from_val = ($this.attr('-data-min'));
		var to_val = ($this.attr('-data-max'));
		var set_from_val = ($this.attr('-data-set-min'));
		var set_to_val = ($this.attr('-data-set-max'));
		console.log('init range');
		$this.jRange({
			from: from_val,
			to: to_val,
			step: 0.01,
			scale: [from_val, to_val],
			format: '%s',
			width: '100%',
			showLabels: false,
			showScale: false,
			isRange : true,
			onstatechange: function() {	new rangeInputs;},
			ondragend: 	function() {	new dragendRange;},
		}).jRange('setValue', set_from_val+','+set_to_val).trigger('change').parents('form').on('submit', function(e) {
			var min = ($this.closest('.range').find($('.min-price')).val());
			var max = ($this.closest('.range').find($('.max-price')).val());
			if (min < from_val) {
				$this.closest('.range').find($('.min-price')).val(from_val);
				min = from_val;
			}
			if (max > to_val) {
				$this.closest('.range').find($('.max-price')).val(to_val);
				max = to_val;
			}
			if (min > max) {
				$this.closest('.range').find($('.min-price')).val(max);
				min = max;
			}
		});
		// если в локальном хранилище уже есть данные о кликнутом фильтре, то центрируем его
		if(localStorage.getItem('label')) {
			var new_label = localStorage.getItem('label');

			$( ".item" ).each(function() {
				if ($(this).find('.slider-input').val() == new_label) {

					var elementClick = $(this).find('.bar');

					var height = outerHeight/2.5;
					var destination = $(elementClick).offset().top-height;
					jQuery("html:not(:animated),body:not(:animated)").animate({scrollTop: destination}, 0);
				}
			});
		};
	});

}


if( $('body').find('.slider-input-price').length){

	$( '.slider-input-price' ).each(function() {
		var $this = $(this);
		var rangeInputs = function(e) {
			var values = $this.val().split(',');
			$this.closest('.range').find($('.min-price')).val(values[0]);
			$this.closest('.range').find($('.max-price')).val(values[1]);
		}
		var	dragendRange =function(e){
			$("#set_filter").click();
		}

		var from_val = ($this.attr('-data-min'));
		var to_val = ($this.attr('-data-max'));
		var set_from_val = ($this.attr('-data-set-min'));
		var set_to_val = ($this.attr('-data-set-max'));
		console.log('init range');
		$this.jRange({
			from: from_val,
			to: to_val,
			step: 1,
			scale: [from_val, to_val],
			format: '%s',
			width: '100%',
			showLabels: false,
			showScale: false,
			isRange : true,
			onstatechange: function() {	new rangeInputs;},
			ondragend: 	function() {	new dragendRange;},
		}).jRange('setValue', set_from_val+','+set_to_val).trigger('change').parents('form').on('submit', function(e) {
			var min = ($this.closest('.range').find($('.min-price')).val());
			var max = ($this.closest('.range').find($('.max-price')).val());
			/*if (min < from_val) {
				$this.closest('.range').find($('.min-price')).val(from_val);
				min = from_val;
			}
			if (max > to_val) {
				$this.closest('.range').find($('.max-price')).val(to_val);
				max = to_val;
			}
			if (min > max) {
				$this.closest('.range').find($('.min-price')).val(max);
				min = max;
			}*/
		});
		// если в локальном хранилище уже есть данные о кликнутом фильтре, то центрируем его
		if(localStorage.getItem('label')) {
			var new_label = localStorage.getItem('label');
			$( ".item" ).each(function() {
				if ($(this).find('.slider-input-price').val() == new_label) {
					var elementClick = $(this).find('.bar');
					var height = outerHeight/2.5;
					var destination = $(elementClick).offset().top-height;
					jQuery("html:not(:animated),body:not(:animated)").animate({scrollTop: destination}, 0);
				}
			});
		};
	});

}

	//запись в local.storage значения slider для позиционироавния после перезагрузки
	$(".slider-input, .slider-input-price").change(function () {
		/*проверка инпута для центрирования*/
		var label = $(this).val();
		localStorage.setItem('label',label);
		return false;
	});


/*** range bar end */

/*** silders start */

	/** other deals slider start */

 	if( $('body #other').length){
		$('body #other').on('init', function(event, slick){
			//поиск элементов в обласити видимости экрана
			var $init = $('.slick-slider .slick-active .btn.btn-to-bsk');
			if ($init.length) {
			    $init.bind('inview', function(event, isInView) {
			          var $this = $(this);
			          if (isInView) {
			              $this.addClass('blink');
			          } else {
			              $this.removeClass('blink');
			          }
			    });
			}

			//Пробегающий блик на кнопке В корзину
			setRandomClass();
			setInterval(function () {
			    setRandomClass();
			}, 7000);

			function setRandomClass() {
	            var items_all = $('.slick-slider .slick-active').find(".btn.btn-to-bsk.blink");
				    items_all.removeClass("special");
				    var number_all = items_all.length;
				    var random_all = Math.floor((Math.random() * number_all));
				    setTimeout(function() {
				    	items_all.eq(random_all).addClass("special");
				    }, 100);
			}
		});
		$('body #other').on('afterChange', function(event, slick){
			//поиск элементов в обласити видимости экрана
			var $init = $('.slick-slider .slick-active .btn.btn-to-bsk');
			if ($init.length) {
			    $init.bind('inview', function(event, isInView) {
			          var $this = $(this);
			          if (isInView) {
			              $this.addClass('blink');
			          } else {
			              $this.removeClass('blink');
			          }
			    });
			}

			//Пробегающий блик на кнопке В корзину
			setRandomClass();
			setInterval(function () {
			    setRandomClass();
			}, 7000);

			function setRandomClass() {
	            var items_all = $('.slick-slider .slick-active').find(".btn.btn-to-bsk.blink");
				    items_all.removeClass("special");
				    var number_all = items_all.length;
				    var random_all = Math.floor((Math.random() * number_all));
				    setTimeout(function() {
				    	items_all.eq(random_all).addClass("special");
				    }, 100);
			}
		});
		$('body #other').slick({
			lazyLoad: 'ondemand',
			dots: true,
			infinite: false,
			speed: 1000,
			slidesToShow: 3,
			slidesToScroll: 3,
			variableWidth: true
		});
	}
	/** other deals slider end */

	/** other deals 2 slider start */

 	if( $('#other2').length){
		$('#other2').slick({
			dots: true,
			speed: 1000,
			slidesToShow: 3,
			slidesToScroll: 3,
			variableWidth: true
		});
	}
	/** other deals 2 slider end */

	/** spec offers in cat-brands start */

 	if( $('#specoff').length){
		$('#specoff').slick({
			dots: true,
			speed: 1000,
			slidesToShow: 3,
			slidesToScroll: 3,
			variableWidth: true
		});
	}
	/** spec offers in cat-brands end */

	/** news slider start */
	if( $('#news').length){
		$('#news').slick({
			dots: true,
			speed: 1000,
			slidesToShow: 4,
			slidesToScroll: 4,
			variableWidth: true
		});
	}
	/** news slider end */

	/** brands 4 catalogue slider start */
	if( $('#brandsSlider').length){
		$('#brandsSlider').slick({
			lazyLoad: 'ondemand',
			dots: false,
			speed: 1000,
			slidesToShow: 5,
			slidesToScroll: 5,
			variableWidth: true
		});
		var height = 0;
		$('#brandsSlider .item').each(function() {
			height = Math.max(height, $(this).height());
		});
		$('#brandsSlider').css('height', height+'px');
	}
	/** brands 4 catalogue slider end */

	/** hot deals slider start */
	if( $('#hotDeals').length){
		$('#hotDeals').slick({
			dots: false,
			speed: 1000,
			slidesToShow: 3,
			slidesToScroll: 3,
			variableWidth: true
		});
	}
	/** hot deals slider end */

	/** brand logos 4 catalogue slider start */
	if( $('#brandsLogoSlider').length){
		$('#brandsLogoSlider').slick({
			dots: false,
			speed: 1000,
			slidesToShow: 6,
			slidesToScroll: 6,
			variableWidth: true
		});
	}
	/** brands logos 4 catalogue slider end */

	/** porfolio slider display start */
 	if( $('#portDisplay').length){

		$('#portDisplay').slick({
		  speed: 700,
		  slidesToShow: 1,
		  slidesToScroll: 1,
		  arrows: true,
		  fade: true,
		//  variableWidth: true,
		  asNavFor: '#portNav'
		});
		$('#portNav').slick({
		  slidesToShow: 5,
		  slidesToScroll: 1,
		  speed: 700,
		  asNavFor: '#portDisplay',
		  focusOnSelect: true,
		  variableWidth: true,
		  centerMode:true,
		  centerPadding: 0
		});
	}
	/** porfolio slider nav end */

	/** infopage slider start */
	if( $('#infoSlider').length){
		$('#infoSlider').slick({
			dots: true,
			speed: 500,
			slidesToShow: 1,
			slidesToScroll: 1,
			fade: true,
			variableWidth: false,
			appendArrows: $('.info-slider-nav .w'),
			appendDots: $('.info-slider-nav .w'),
			prevArrow: '<a class="slick-prev-sm">Previous</a>',
			nextArrow: '<a class="slick-next-sm">Next</a>',
		});
	}
	/** infopage slider end */



	/** promo slider start */
	if( $('#promo').length){

		$('#promo').on('beforeChange', function(e, slickSlider, i, j){
			$('.pr-nav li').removeClass('slick-active');
			$('.pr-nav li').eq(j).addClass('slick-active');
		});


		$('#promo').slick({
			speed: 800,
			fade: true,
			slidesToShow: 1,
			slidesToScroll: 1,
			autoplay: true
		});

		$('.pr-nav li').on('click', function(e) {
			var n = $(this).index();
			$('#promo').slick('slickGoTo', parseInt(n));
			$(this).siblings().removeClass('slick-active');
			$(this).addClass('slick-active');
		});

	}

	$('.pr-nav li').eq(0).addClass('slick-active');
	/** promo slider end */

/*** sliders end */

	window.zoomf = function(onen_link) {
		var buildModal =  '<div class="w"><a href="#" class="x">&nbsp;</a><div class="c-pic pic" style="background-image:url(' + onen_link + ')"><div class="threesixty car rotate"><div class="spinner"><span>0%</span></div><ol class="threesixty_images"></ol></div></div></div>';

		$('<div class="pop pop-card pop-bg-1" style="width:750px" id="popCard" />').html(buildModal).bPopup(
			{
				closeClass:'x',
				modalColor: '#415562',
				opacity:0.98,
				position: ["auto",50],
				onClose: function() { $(this).remove(); }
			}
		);

	}

	if ($('#ORDER_FORM').length) {

		$('body').on('ifChecked', '.delivery_checks', function(e) {
			var el = $(this);
			$('#HIDE_DELIVERY_TYPE').val(el.attr('data-delivery_desc'));
			$('#HIDE_DELIVERY_NAME_CUR').val(el.attr('data-delivery'));
			$('#HIDE_LOCATION_ID').val(el.attr('data-selfdelivery'));
			$('#HIDE_DELIVERY_PRICE').val(el.attr('data-delivery_price'));
			$('#HIDE_CURRENT_REGION').val(window.CUR_REGION);
			$('#HIDE_CURRENT_CITY').val(window.CUR_CITY);
			$('#HIDE_CURRENT_NETWORK').val(window.CITY_NAME+" ["+window.CITY_CODE+"]");
			// submitForm();
		});
		$('.delivery_checks:checked').trigger('ifChecked');

		$('.legend_auto_fill').each(function() {
			$('body').on('change keyup', 'input[data-code="'+$(this).attr('data-field_code')+'"]', function() {
				var el = $('.legend_auto_fill[data-field_code="'+$(this).attr('data-code')+'"]').text($(this).val());
			})
		});

		$('body').on('change', '.yur-select[data-code="FIRM_TYPE"]', function() {
			var HIDE_FIRM_TYPE_ANOTHER = $('#HIDE_FIRM_TYPE_ANOTHER');
			if ($(this).val() == 'другое…' && $('#'+HIDE_FIRM_TYPE_ANOTHER.attr('name')).length == 0) {
				$(this).after('<input type="text" maxlength="250" size="40" value="'+HIDE_FIRM_TYPE_ANOTHER.val()+'" name="'+HIDE_FIRM_TYPE_ANOTHER.attr('name')+'" class="inp inp-drk inp-t inp-td2 additional_field required_field" id="'+HIDE_FIRM_TYPE_ANOTHER.attr('name')+'" data-code="HIDE_FIRM_TYPE_ANOTHER">');
			} else {
				$('#'+HIDE_FIRM_TYPE_ANOTHER.attr('name')).hide();
			}
		});
	}

	if( $('.ttSwitch3').length){
		$('.ttSwitch3').powerTip({ placement: 'nw', mouseOnToPopup: true });
		$('.ttSwitch3').data('powertiptarget', 'ttSwitch3');
	}

	$('#plusInstall').on('ifChecked ifUnchecked', function(event) {
		$('.btn-to-bsk[data-href_setup], #btnToBasket[data-href_setup]').each(function() {
			if (event.type == 'ifUnchecked') {
				$(this).attr('href', $(this).data('good_href'));
			} else {
				$(this).data('good_href', $(this).attr('href'));
				$(this).attr('href', $(this).attr('data-href_setup'));
			}
		});
	});

	/***  basket promo code show/hide */

	$(document).on("click", ".prm-hider", function() {

		if ($('.prm-hidden').is(':visible')) {
			$('.prm-hidden').slideUp(200);
		}
		else {
			$('.prm-hidden').slideDown(200);
		}

	});

	/***  basket promo code show/hide end */

	/*** personal account */

	$('#addCompUnhide').on("click", function(e) {
		$('#addComp').slideDown(200);
		$(this).css('display', 'none');
	});

	$('#addComp .lk-btn-cancel').on("click", function(e) {
		$('#addComp').slideUp(200);
		$('#addCompUnhide').css('display', 'block');
	});


	$('#addAdrUnhide').on("click", function(e) {
		$('#addAdr').slideDown(200);
		$(this).css('display', 'none');
	});

	$('#addAdr .lk-btn-cancel').on("click", function(e) {
		$('#addAdr').slideUp(200);
		$('#addAdrUnhide').css('display', 'block');
	});

	$('#addContactUnhide').on("click", function(e) {
		$('#addContact').slideDown(200);
		$(this).css('display', 'none');
	});

	$('#addContact .lk-btn-cancel').on("click", function(e) {
		$('#addContact').slideUp(200);
		$('#addContactUnhide').css('display', 'block');
	});

	//delivery method selects
	$('.yur-select').ikSelect({
		autoWidth: false,
		ddFullWidth: false,
		customClass: 'yur-select'
	});

	$("#adr_city").on("focus",function(){
		$("#popCity").addClass("return");
		$("#popCity.return .city-ul a").on("click",function(e){
			e.preventDefault();
			$("#addAdr input[name='adr[city]']").val($(this).parent().attr("-data-city-id"));
			$("#addAdr input[name='adr[city_text]']").val($(this).parent().attr("-data-city"));
			$("#popCity").removeClass("return");
			$("#popCity .x").click();
		});
		$(".city-pop").click();
	});



	$(".lk-btns-adr .lk-btn-add").on("click", function(){
		$("#addAdr input").removeClass("inp-inv");
		$("#addAdr .vfield").html("");

		$.post(
			"/lk_edit.php",
			{
				city: $("#addAdr input[name='adr[city]']").val(),
				city_text: $("#addAdr input[name='adr[city_text]']").val(),
				street: $("#addAdr input[name='adr[street]']").val(),
				house: $("#addAdr input[name='adr[house]']").val(),
				housing: $("#addAdr input[name='adr[housing]']").val(),
				build: $("#addAdr input[name='adr[build]']").val(),
				apartment: $("#addAdr input[name='adr[apartment]']").val(),
				action: "add_adr"
			},
			function(data){
				if(data.status=="ok"){
					$("#adrset").append("<div class=\"radioline\"><input name=\"compRadio2\" class=\"check-custom\" type=\"radio\" value=\""+data.ID+"\" id=\"adr"+data.ID+"\"><label for=\"adr"+data.ID+"\""+data.attr+">"+data.NAME+"</label></div>");
					$('.check-custom').iCheck({
						checkboxClass: 'icheckbox_minimal',
						radioClass: 'iradio_minimal',
						activeClass: 'active'
					});
					$('#addAdr').slideUp(200);
					$("#addAdr input").val("");
					$('#addAdrUnhide').css('display', 'block');
				}else{
					if(data.message!="") alert(data.message);
					if("error" in data){
						for (var item in data.error) {
							$("#addAdr input[name='comp["+item+"]']").addClass("inp-inv");
							$("#addAdr input[name='comp["+item+"]']").parent().find(".vfield").html(data.error[item]);
						}
					}
				}
			},
			"json"
		);
	});
	$(".lk-btns-comp .lk-btn-add").on("click", function(){
		$("#addComp input").removeClass("inp-inv");
		$("#addComp .vfield").html("");

		$.post(
			"/lk_edit.php",
			{
				org_type: $("#addComp select[name='comp[org_type]'] :selected").val(),
				org_type_text: $("#addComp select[name='comp[org_type]'] :selected").text(),
				org_name: $("#addComp input[name='comp[org_name]']").val(),
				inn: $("#addComp input[name='comp[inn]']").val(),
				kpp: $("#addComp input[name='comp[kpp]']").val(),
				ks: $("#addComp input[name='comp[ks]']").val(),
				bank: $("#addComp input[name='comp[bank]']").val(),
				bik: $("#addComp input[name='comp[bik]']").val(),
				rs: $("#addComp input[name='comp[rs]']").val(),
				ur_adr: $("#addComp input[name='comp[ur_adr]']").val(),
				action: "add_comp"
			},
			function(data){
				if(data.status=="ok"){
					$("#compset").append("<div class=\"radioline\"><input name=\"compRadio\" class=\"check-custom\" type=\"radio\" value=\""+data.ID+"\" id=\"comp"+data.ID+"\"><label for=\"comp"+data.ID+"\""+data.attr+">"+data.NAME+"</label></div>");
					$('.check-custom').iCheck({
						checkboxClass: 'icheckbox_minimal',
						radioClass: 'iradio_minimal',
						activeClass: 'active'
					});
					$('#addComp').slideUp(200);
					$('#addCompUnhide').css('display', 'block');
				}else{
					if(data.message!="") alert(data.message);
					if("error" in data){
						for (var item in data.error) {
							$("#addComp input[name='comp["+item+"]']").addClass("inp-inv");
							$("#addComp input[name='comp["+item+"]']").parent().find(".vfield").html(data.error[item]);
						}
					}
				}
			},
			"json"
		);
	});
	$(".lk-btns-contact .lk-btn-add").on("click", function(){
		$("#addContact input").removeClass("inp-inv");
		$("#addContact .vfield").html("");

		$.post(
			"/lk_edit.php",
			{
				fio: $("#addContact input[name='contact[fio]']").val(),
				email: $("#addContact input[name='contact[email]']").val(),
				phone: $("#addContact input[name='contact[phone]']").val(),
				action: "add_contact"
			},
			function(data){
				if(data.status=="ok"){
				console.log("3");
					$("#contactset").append("<div class=\"radioline\"><input name=\"compRadio1\" class=\"check-custom\" type=\"radio\" value=\""+data.ID+"\" id=\"adr"+data.ID+"\"><label for=\"adr"+data.ID+"\""+data.attr+">"+data.NAME+"</label></div>");
					$('.check-custom').iCheck({
						checkboxClass: 'icheckbox_minimal',
						radioClass: 'iradio_minimal',
						activeClass: 'active'
					});
					$('#addContact').slideUp(200);
					$("#addContact input").val("");
					$('#addContactUnhide').css('display', 'block');
				}else{
				console.log("2");
					if(data.message!="") alert(data.message);
					if("error" in data){
					console.log("1");
						for (var item in data.error) {
							console.log($("#addContact input[name='contact["+item+"]']"));
							$("#addContact input[name='contact["+item+"]']").addClass("inp-inv");
							$("#addContact input[name='contact["+item+"]']").parent().find(".vfield").html(data.error[item]);
						}
					}
				}
			},
			"json"
		);
	});

	$("#adrset .iradio_minimal").on("click",function(){
		$.post(
			"/lk_edit.php",
			{
				ID: $("input[name=compRadio2]:checked").val(),
				action: "default_adr"
			},
			function(data){
				if(data.status=="ok"){

				}else{
					alert(data.message);
				}
			},
			"json"
		);
	});
	$("#compset .iradio_minimal").on("click",function(){
		$.post(
			"/lk_edit.php",
			{
				ID: $("input[name=compRadio]:checked").val(),
				action: "default_comp"
			},
			function(data){
				if(data.status=="ok"){

				}else{
					alert(data.message);
				}
			},
			"json"
		);
	});
	$("#contactset .iradio_minimal").on("click",function(){
		$.post(
			"/lk_edit.php",
			{
				ID: $("input[name=compRadio1]:checked").val(),
				action: "default_contact"
			},
			function(data){
				if(data.status=="ok"){

				}else{
					alert(data.message);
				}
			},
			"json"
		);
	});
	$("input[name=compRadio]").on("change", function(){
		$.post(
			"/lk_edit.php",
			{
				ID: $("input[name=compRadio]:checked").val(),
				action: "default_comp"
			},
			function(data){
				if(data.status=="ok"){

				}else{
					alert(data.message);
				}
			},
			"json"
		);
	});
	$("input[name=compRadio1]").on("change", function(){
		$.post(
			"/lk_edit.php",
			{
				ID: $("input[name=compRadio1]:checked").val(),
				action: "default_contact"
			},
			function(data){
				if(data.status=="ok"){

				}else{
					alert(data.message);
				}
			},
			"json"
		);
	});
	$("input[name=compRadio2]").on("change", function(){
		$.post(
			"/lk_edit.php",
			{
				ID: $("input[name=compRadio2]:checked").val(),
				action: "default_adr"
			},
			function(data){
				if(data.status=="ok"){

				}else{
					alert(data.message);
				}
			},
			"json"
		);
	});
	/*** personal account end */

	//open pop
	$(document).on('click', '#address-sel', function() {
		var pop = $(this).parent().find('.city-sel-pop');
		if(pop!=undefined){
			if (pop.is(':visible')) {
				pop.slideUp(0);
			}
			else {
				pop.slideDown(0);
			}
		}
	});

	$(document).on('click', '#address-sel-pop a', function() {
		document.cookie = "address-sel="+$(this).attr("-data-id");

		window.location=$("#popCity li[-data-city-id="+$(this).attr("-city-id")+"] a").attr("href");
	});

	//open pop
	$(document).on('click', '#company-sel', function() {
		var pop = $(this).parent().find('.city-sel-pop');
		if(pop!=undefined){
			if (pop.is(':visible')) {
				pop.slideUp(0);
			}
			else {
				pop.slideDown(0);
			}
		}
	});

	$(document).on('click', '#company-sel-pop a', function() {
		document.cookie = "company-sel="+$(this).attr("-data-id");

		window.location.reload();
	});

	//open pop
	$(document).on('click', '#contact-sel', function() {
		var pop = $(this).parent().find('.city-sel-pop');
		if(pop!=undefined){
			if (pop.is(':visible')) {
				pop.slideUp(0);
			}
			else {
				pop.slideDown(0);
			}
		}
	});

	$(document).on('click', '#contact-sel-pop a', function() {
		document.cookie = "contact-sel="+$(this).attr("-data-id");

		window.location.reload();
	});

	$(document).on('click', '#map-city-select', function(e) {
		e.preventDefault();
		$(".city-pop").click();
	});

	$(".bx-filter .checkset input").on('ifChanged', function(event){
		$("#set_filter").click();
	});

	$(".bx-filter input[type=submit]").on('click', function(event){
		$(this).append("<input type='hidden' name='"+$(this).attr("name")+"' value='"+$(this).val()+"' id='hiddenForAjaxSubmit' />");
	});

	/*$(document).on("submit",".smartfilter",function(e){
		e.preventDefault();
		$.get(
			$(this).attr("action"),
			$(this).serialize(),
			function(data){
				//console.log(data)
				$(".bx-filter #hiddenForAjaxSubmit").remove();
				htmlObj=$.parseHTML( data );
				//console.log($(htmlObj).find("#catalogList"));
				if( $(htmlObj).find("#catalogList") ) $("#catalogList").html($(htmlObj).find("#catalogList").html());

				$('#sortSelect,#sortSelect_bottom').ikSelect({
					autoWidth: false,
					ddFullWidth: false,
					customClass: 'sort-select'
				});

				$('#amountSelect,#amountSelect_bottom').ikSelect({
					autoWidth: false,
					ddFullWidth: false,
					customClass: 'amount-select'
				});

				$('.checkset input, .check-custom').iCheck({
					checkboxClass: 'icheckbox_minimal',
					radioClass: 'iradio_minimal',
					activeClass: 'active'
				});

				if (!$('#resultsControl a').hasClass('active')) {
					$('#resultsControl a').first().addClass('active')
				}
				//	console.log("1");
			},
			"html"
		);
	});
*/
	$('input[name=USER_PERSONAL_PHONE]').mask("+7(999)999-99-99");
});
function print_block(id){
	pr = document.getElementById(id).innerHTML;
	newWin=window.open('','printWindow','Toolbar=0,Location=0,Directories=0,Status=0,Menubar=0,Scrollbars=0,Resizable=0');
	newWin.document.open();
	newWin.document.write(pr);
	newWin.print();
	newWin.document.close();
}
