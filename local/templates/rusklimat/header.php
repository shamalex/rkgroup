<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
IncludeTemplateLangFile(__FILE__);

// scripts
// jQuery
$APPLICATION->AddHeadScript(SITE_TEMPLATE_PATH.'/j/jquery-1.11.2.min.js');
// <!-- 'smart' sropdown menu -->
$APPLICATION->AddHeadScript(SITE_TEMPLATE_PATH.'/j/jquery.menu-aim.js');
// <!-- search autocomplete -->
$APPLICATION->AddHeadScript(SITE_TEMPLATE_PATH.'/j/jquery.autocomplete.min.js');
// <!-- custom scrollbar -->
$APPLICATION->AddHeadScript(SITE_TEMPLATE_PATH.'/j/jquery.mCustomScrollbar.min.js');
// <!-- custom select -->
$APPLICATION->AddHeadScript(SITE_TEMPLATE_PATH.'/j/jquery.ikSelect.min.js');
// <!-- custom checkboxes -->
$APPLICATION->AddHeadScript(SITE_TEMPLATE_PATH.'/j/jquery.icheck.min.js');
// <!-- popups -->
$APPLICATION->AddHeadScript(SITE_TEMPLATE_PATH.'/j/jquery.bpopup.min.js');
// <!-- timer -->
$APPLICATION->AddHeadScript(SITE_TEMPLATE_PATH.'/j/jquery.countdown.min.js');
// <!-- tabs for card and solutions -->
$APPLICATION->AddHeadScript(SITE_TEMPLATE_PATH.'/j/jquery.easytabs.min.js');
// <!-- tooltips -->
$APPLICATION->AddHeadScript(SITE_TEMPLATE_PATH.'/j/jquery.powertip.js');
// <!-- star rating -->
$APPLICATION->AddHeadScript(SITE_TEMPLATE_PATH.'/j/jquery.raty.js');
// <!-- range bar for cataloque filter -->
$APPLICATION->AddHeadScript(SITE_TEMPLATE_PATH.'/j/jquery.range.min.js');
//cookie
// $APPLICATION->AddHeadScript(SITE_TEMPLATE_PATH.'/j/jquery.cookie.js');
// <!-- carousels -->
$APPLICATION->AddHeadScript(SITE_TEMPLATE_PATH.'/j/slick.min.js');
// <!-- formstyler -->
$APPLICATION->AddHeadScript(SITE_TEMPLATE_PATH.'/j/jquery.formstyler.js');
// <!-- mask -->
$APPLICATION->AddHeadScript(SITE_TEMPLATE_PATH.'/j/jquery.maskedinput.js');
// <!-- ajax form -->
$APPLICATION->AddHeadScript(SITE_TEMPLATE_PATH.'/j/jquery.form.min.js');
// <!-- main js stuff -->
$APPLICATION->AddHeadScript(SITE_TEMPLATE_PATH.'/j/j.js');
// <!-- validate callback form -->
//$APPLICATION->AddHeadScript(SITE_TEMPLATE_PATH.'/j/callback.js');
// <!-- validate one-click form (card) -->
$APPLICATION->AddHeadScript(SITE_TEMPLATE_PATH.'/j/oneclick.js');
// <!-- google map -->
// $APPLICATION->AddHeadScript(SITE_TEMPLATE_PATH.'/j/gmap.js');
$APPLICATION->AddHeadScript(SITE_TEMPLATE_PATH.'/j/tooltipster.bundle.min.js');
// site js stuff
$APPLICATION->AddHeadScript(SITE_TEMPLATE_PATH.'/j/site.js');


$APPLICATION->AddHeadString('<link href="'.SITE_TEMPLATE_PATH.'/l/tooltipster-sideTip-light.min.css";  type="text/css" rel="stylesheet" />',true);
$APPLICATION->AddHeadString('<link href="'.SITE_TEMPLATE_PATH.'/l/tooltipster.bundle.min.css";  type="text/css" rel="stylesheet" />',true);


// strings
// $APPLICATION->AddHeadString('<script src="https://maps.googleapis.com/maps/api/js?sensor=false"></script>', true, 'BEFORE_CSS');

$APPLICATION->AddHeadString('<meta name="viewport" content="width=1280">');
$APPLICATION->AddHeadString('<link href="/favicon.ico?'.filemtime($_SERVER['DOCUMENT_ROOT'].'/favicon.ico').'" rel="icon">');

// css
// $APPLICATION->SetAdditionalCSS(SITE_TEMPLATE_PATH.'/l/base.css');

// code
$CUR_PAGE = $APPLICATION->GetCurPage(false);
$RIGHT_CONTENT_COL = array();

// bitrix js
CJSCore::Init(array('ajax', 'window'));

?><!DOCTYPE html>
<html>
<head>
<?
/*
N: кто вписал сюда вот эту ерунду???? признавайтесь!!! это все подключается выше через $APPLICATION->AddHeadScript

<script type="text/javascript" src="<?=SITE_TEMPLATE_PATH?>/j/j.js"></script>
<script type="text/javascript" src="<?=SITE_TEMPLATE_PATH?>/j/jquery.bpopup.min.js"></script>
<script type="text/javascript" src="<?=SITE_TEMPLATE_PATH?>/j/jquery.countdown.min.js"></script>
<script type="text/javascript" src="<?=SITE_TEMPLATE_PATH?>/j/jquery.easytabs.min.js"></script>
<script type="text/javascript" src="<?=SITE_TEMPLATE_PATH?>/j/jquery.powertip.js"></script>
<script type="text/javascript" src="<?=SITE_TEMPLATE_PATH?>/j/jquery.raty.js"></script>
<script type="text/javascript" src="<?=SITE_TEMPLATE_PATH?>/j/jquery.range.min.js"></script>
<script type="text/javascript" src="<?=SITE_TEMPLATE_PATH?>/j/slick.min.js"></script>
<script type="text/javascript" src="<?=SITE_TEMPLATE_PATH?>/j/jquery.formstyler.js"></script>
<script type="text/javascript" src="<?=SITE_TEMPLATE_PATH?>/j/callback.js"></script>
<script type="text/javascript" src="<?=SITE_TEMPLATE_PATH?>/j/oneclick.js"></script>
<script type="text/javascript" src="<?=SITE_TEMPLATE_PATH?>/j/site.js"></script>
*/
?>
<script>
window.SITE_TEMPLATE_PATH = "<?= SITE_TEMPLATE_PATH ?>";
window.HTTP_HOST = "<?= $_SERVER['HTTP_HOST'] ?>";
window.SERVER_NAME = "<?= $_SERVER['SERVER_NAME'] ?>";
<?
$COOKIE_NAME = $_SERVER['HTTP_HOST'];
$COOKIE_NAME = preg_replace('/:\d+$/ui', '', $COOKIE_NAME);
if (preg_replace('/[\d\.]+/ui', '', $COOKIE_NAME) != '') {
	$COOKIE_NAME = '.'.$COOKIE_NAME;
}

global $arGeoData;

?>
window.COOKIE_NAME = "<?= $COOKIE_NAME ?>";
window.COOKIE_PREFIX = "<?= COption::GetOptionString("main", "cookie_name", "BITRIX_SM") ?>_";
window.CUR_REGION = "<?= $arGeoData['CUR_CITY']['REGION'] ?>";
window.CUR_CITY_ID = "<?= $arGeoData['CUR_CITY']['ID'] ?>";
window.CUR_CITY = "<?= $arGeoData['CUR_CITY']['NAME'] ?>";
window.CITY_NAME = "<?= $arGeoData['CUR_CITY']['CITY_NAME'] ?>";
window.CITY_CODE = "<?= $arGeoData['CUR_CITY']['CITY_CODE'] ?>";
</script>
<title><?$APPLICATION->ShowTitle('title')?></title>
<?$APPLICATION->ShowHead();?>
<!--[if lt IE 9]><script src="<?= SITE_TEMPLATE_PATH ?>/j/IE9.js?<?= filemtime($_SERVER['DOCUMENT_ROOT'].SITE_TEMPLATE_PATH.'/j/IE9.js') ?>"></script><![endif]-->
</head>
<body class="<?= !empty($USER) && $USER->IsAdmin() ? "admin_user" : "" ?>">

<?$APPLICATION->ShowPanel()?>

<? if(CheckServerIPs()) :?>
	<?$APPLICATION->IncludeFile("include/counters.php", Array(), Array("MODE"=>"html", "SHOW_BORDER"=>false));?>
<? endif; ?>


<!-- header section -->
<header class="header">

	<!-- functions menu -->
	<div class="mn-func-w">
		<div class="mn-func c">
			<a class="city city-pop" href="#select_city"><span class="city_name_here"><?= $arGeoData['CUR_CITY']['NAME'] ?></span></a>
			<?$_GET["arGeoData"]=$arGeoData['CUR_CITY']['CITY_CODE'];?>
			<?$APPLICATION->IncludeComponent(
				"bitrix:menu", 
				"top", 
				array(
					"ROOT_MENU_TYPE" => "top",
					"MENU_CACHE_TYPE" => "A",
					"MENU_CACHE_TIME" => "36000000",
					"MENU_CACHE_USE_GROUPS" => "N",
					"MENU_THEME" => "site",
					"CACHE_SELECTED_ITEMS" => "N",
					"MENU_CACHE_GET_VARS" => array(
						0 => "arGeoData",
					),
					"MAX_LEVEL" => "2",
					"CHILD_MENU_TYPE" => "top_inner",
					"USE_EXT" => "N",
					"DELAY" => "N",
					"ALLOW_MULTI_SELECT" => "N",
					"COMPONENT_TEMPLATE" => "top"
				),
				false
			);?>
			<?unset($_GET["arGeoData"]);?>
			<?$APPLICATION->IncludeComponent("bitrix:menu", "empty", Array(
				"ROOT_MENU_TYPE" => "top_inner",	// Тип меню для первого уровня
				"MENU_CACHE_TYPE" => "A",	// Тип кеширования
				"MENU_CACHE_TIME" => "36000000",	// Время кеширования (сек.)
				"MENU_CACHE_USE_GROUPS" => "N",	// Учитывать права доступа
				"MENU_THEME" => "site",
				"CACHE_SELECTED_ITEMS" => "N",
				"MENU_CACHE_GET_VARS" => "",	// Значимые переменные запроса
				"MAX_LEVEL" => "1",	// Уровень вложенности меню
				"CHILD_MENU_TYPE" => "",	// Тип меню для остальных уровней
				"USE_EXT" => "N",	// Подключать файлы с именами вида .тип_меню.menu_ext.php
				"DELAY" => "N",	// Откладывать выполнение шаблона меню
				"ALLOW_MULTI_SELECT" => "N",	// Разрешить несколько активных пунктов одновременно
				"COMPONENT_TEMPLATE" => ".default"
				),
				false
			);?>
			<?global $USER;
			if($USER->GetID()){?>
				<a href="<?=$APPLICATION->GetCurPageParam("logout=yes", array("login","logout","register","forgot_password","change_password"))?>" class="mn-func-logout">Выйти</a>
				<a href="/personal/" class="mn-func-acc  mn-func-logged"><?=$USER->GetFullName()?></a>
			<?}else{?>
				<a href="/personal/" class="mn-func-acc">Личный кабинет</a>
			<?}?>

		</div>
	</div>
	<!-- / functions menu end -->

	<!-- actual header -->
	<div class="hd с">

		<?$APPLICATION->IncludeFile("include/header_logo.php", Array(), Array("MODE"=>"html"));?>

		<div class="hd-phone">
			<?
			$phone = $arGeoData['CUR_CITY']['PHONE'];
			$phone = preg_replace('/(\d+[\-\s])(\d+)([\-\s]\d+)$/ui', '\1<span class="red">\2</span>\3', $phone);
			?>
			<div class="phn"><?= $phone ?></div>
			<div class="hrs">09:00-20:00 ежедневно</div>
			<!--<a class="hrs" href="/news/2015/20151229/" style="color: #EE2D24;">режим работы в праздничные дни</a>-->
		</div>

		<a href="#callback" class="callback callback-pop"><span>Заказать<br/>обратный звонок</span></a>

		<!-- hd-srch -->
		<div class="hd-srch">
			<form class="srch-form c" action="/search/">
				<input type="hidden" name="how" value="r" />
				<input class="srch-field" name="q" id="hdSearchAutocomplete" type="text" />
				<input type="submit" class="srch-btn" value="" />
			</form>
			<!-- autocomplete is dynamically generated, hd.less > .autocomplete-suggestions -->
		</div>
		<!-- / hd-srch end -->

		<?$APPLICATION->IncludeComponent("bitrix:sale.basket.basket.line", "header_static", Array(
				"COMPONENT_TEMPLATE" => ".default",
				"PATH_TO_BASKET" => SITE_DIR."personal/cart/",	// Страница корзины
				"PATH_TO_PERSONAL" => SITE_DIR."personal/",	// Страница персонального раздела
				"PATH_TO_PROFILE" => SITE_DIR."personal/",	// Страница профиля
				"PATH_TO_REGISTER" => SITE_DIR."login/",	// Страница регистрации
				"POSITION_FIXED" => "N",	// Отображать корзину поверх шаблона
				"SHOW_AUTHOR" => "N",	// Добавить возможность авторизации
				"SHOW_EMPTY_VALUES" => "Y",	// Выводить нулевые значения в пустой корзине
				"SHOW_NUM_PRODUCTS" => "Y",	// Показывать количество товаров
				"SHOW_PERSONAL_LINK" => "N",	// Отображать персональный раздел
				"SHOW_PRODUCTS" => "Y",	// Показывать список товаров
				"SHOW_TOTAL_PRICE" => "Y",	// Показывать общую сумму по товарам
			),
			false
		);?>

	</div>
	<!-- / actual header end -->

	<!-- city popup -->
	<div class="pop pop-city pop-bg-1" id="popCity">
		<!-- city popup wrapper -->
		<div class="w">
			<a href="#" class="x">&nbsp;</a>
			<p class="supttl">Ваш город</p>
			<div class="ttl-p1 city_name_here" id="cityCurrent"><?= $arGeoData['CUR_CITY']['NAME'] ?></div>

			<noindex>
			<!-- city-container -->
			<div class="city-container c">

				<!-- region -->
				<div class="region" id="regions_block">
					<div class="w">
						<div class="ttl-p2">Регион</div>
						<div class="scroll">
							<ul class="reg-ul ul-orange">
								<? $was = 0; ?>
								<? foreach($arGeoData['CITIES'] as $region => $e): ?>
									<li<?= $was++ == 0 ? ' class="active"' : '' ?> -data-region="<?= $region ?>"><a href="#region"><?= $region ?></a></li>
								<? endforeach; ?>
							</ul>
						</div>
					</div>
					<!-- / region > .w end -->
				</div>
				<!-- / region end -->

				<!-- cities begin -->
				<div class="cities" id="cities_block">
					<div class="w">
						<div class="ttl-p2">Населенный пункт</div>

						<!-- city-slide -->
						<div class="city-slide c">

							<?
							$was1 = false;
							foreach($arGeoData['CITIES'] as $region => $cities) {
								$cnt = 0;
								$was = 1;
								$one = [];
								$two = [];
								$lim = ceil(count($cities)/2);
								foreach($cities as $city => $city_id) {
									$cnt++;
									if ($cnt <= $lim) {
										$one[] = [$city_id, $city];
									} else {
										$two[] = [$city_id, $city];
									}
								}
								?>
								<div class="scroll" style="display: <?= ($was1 ? 'none' : 'block') ?>;" -data-region="<?= $region ?>">
									<!-- column 1 -->
									<ul class="city-ul ul-orange">
										<? foreach($one as $city): ?>
											<li<?= $was++ == 0 ? ' class="active"' : '' ?> -data-city="<?= $city[1] ?>" -data-city-id="<?= $city[0] ?>"><a href="<?= nfGetCurPageParam('CITY='.$city[0], ['CITY']) ?>"><?= $city[1] ?></a></li>
										<? endforeach; ?>
									</ul>
									<!--/ column 1 end -->
									<!-- column 2 -->
									<ul class="city-ul ul-orange">
										<? foreach($two as $city): ?>
											<li<?= $was++ == 0 ? ' class="active"' : '' ?> -data-city="<?= $city[1] ?>" -data-city-id="<?= $city[0] ?>"><a href="<?= nfGetCurPageParam('CITY='.$city[0], ['CITY']) ?>"><?= $city[1] ?></a></li>
										<? endforeach; ?>
									</ul>
									<!--/ column 2 end -->
								</div>
								<!-- scroll end -->
								<?
								$was1 = true;
							}
							?>



						</div>
						<!--/ city-slide end -->
					</div>
					<!-- / cities > .w end -->
				</div>
				<!-- / cities end -->
			</div>
			<!-- / city-container end -->
			</noindex>
		</div>
		<!-- / city popup > .w end -->
	</div>
	<!-- / city popup end -->

	<div class="popup-callbacks"></div>
	<div class="popup-feedbacks"></div>
	<?if($arGeoData["BASKET_CHANGED"] && strpos($_SERVER["REQUEST_URI"],"/personal/cart/")===0){?><div class="popup-changebasket"></div><?}?>

</header>
<!-- / header section end -->

<!-- actual nav section -->
<nav class="nav">
	<!-- catalogue menu with simple links -->
	<ul class="mn-cat">
		<!-- simple link -->
		<li class="mn-cat-all"><a href="/catalog/">Полный<br/>каталог</a></li>
		<!-- / simple link -->
	</ul>
	<!-- / catalogue menu with simple links end -->

	<? if($_REQUEST['SHOW_STATIC_MENU'] == 'Y'): ?>
		<?$APPLICATION->IncludeFile("include/menu_catalogue.php", Array(), Array("MODE"=>"html"));?>
	<? else: ?>
		<?
		$arrMenuFilter = [];

		if (!empty($arGeoData['CUR_CITY']['PRICE_ID'])) {
			$arrMenuFilter['>CATALOG_PRICE_'.$arGeoData['CUR_CITY']['PRICE_ID']] = '0';
		} else {
			$arrMenuFilter['<CATALOG_PRICE_1'] = '0';
		}
		?>
		<?$APPLICATION->IncludeComponent(
			"bitrix:catalog.section.list",
			"menu_catalogue",
			Array(
				"ADD_SECTIONS_CHAIN" => "N",
				"CACHE_GROUPS" => "N",
				"CACHE_TIME" => "36000000",
				"CACHE_TYPE" => "A",
				"COMPONENT_TEMPLATE" => "menu_catalogue",
				"COUNT_ELEMENTS" => "N",
				"IBLOCK_ID" => "17",
				"IBLOCK_TYPE" => "catalog",
				"SECTION_CODE" => "",
				"SECTION_FIELDS" => array("CODE","NAME","SORT","PICTURE",""),
				"SECTION_ID" => "",
				"SECTION_URL" => "",
				"SECTION_USER_FIELDS" => array("UF_REAL_CAT_LINK","UF_MENU_COL",""),
				"SHOW_PARENT_NAME" => "Y",
				"TOP_DEPTH" => "4",
				"VIEW_MODE" => "LIST",
				"IBLOCK_ID_LINK" => "8",
				"FILTER" => $arrMenuFilter,
				"SHOW_CNT" => $_REQUEST['SHOW_MENU_CNT'] == 'Y',
			)
		);?>
	<? endif; ?>

	<!-- catalogue menu simple links  -->
	<ul class="mn-cat">

		<!-- simple link -->
		<li class="mn-cat-proj"><a href="/service/">Монтаж<br/>и проекты</a></li>
		<!-- / simple link -->

		<!-- simple link -->
		<li class="mn-cat-deal"><a style="" href="/special/">Акции</a></li>
		<!-- / simple link -->

	</ul>
	<!-- / catalogue menu with simple links end -->
</nav>
<!-- / actual nav section end -->

<? if($CUR_PAGE == '/'): ?>
	<?$APPLICATION->IncludeFile("include/promo.php", Array(), Array("MODE"=>"php"));?>
<? endif; ?>

<!-- midsection + main -->
<section class="mid <?$APPLICATION->ShowProperty("MAIN_CLASS")?>">
	<?$APPLICATION->IncludeComponent("bitrix:breadcrumb", "", array(
		"START_FROM" => "0",
		"PATH" => "",
		"SITE_ID" => "-"
		),
		false,
		Array('HIDE_ICONS' => 'Y')
	);?>
	<? $APPLICATION->AddBufferContent('ShowContentWrapHeader'); ?>
