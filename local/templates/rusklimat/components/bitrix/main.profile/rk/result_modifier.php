<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
global $USER;
//p($arResult["arUser"]);
$default=false;
$rsAddress=CIBlockElement::GetList(array(), array("IBLOCK_ID"=>30,"PROPERTY_USER"=>$USER->GetID()), false, false, array("ID","NAME","PROPERTY_city","PROPERTY_street","PROPERTY_house","PROPERTY_housing","PROPERTY_build","PROPERTY_apartment","PROPERTY_USER","PROPERTY_default"));
while ($arFields=$rsAddress->GetNext()) {
	if($arFields["PROPERTY_DEFAULT_VALUE"]=="Y") $default=true;
	$arResult["ADDRESS"][]=$arFields;
}
if(count($arResult["ADDRESS"])>0 && !$default)
	$arResult["ADDRESS"][0]["PROPERTY_DEFAULT_VALUE"]="Y";

$default=false;
$rsCompany=CIBlockElement::GetList(array(), array("IBLOCK_ID"=>31,"PROPERTY_USER"=>$USER->GetID()), false, false, array("ID","NAME","PROPERTY_org_type","PROPERTY_org_name","PROPERTY_bik","PROPERTY_rs","PROPERTY_bank","PROPERTY_ks","PROPERTY_USER","PROPERTY_default"));
while ($arFields=$rsCompany->GetNext()) {
	if($arFields["PROPERTY_DEFAULT_VALUE"]=="Y") $default=true;
	$arResult["COMPANY"][]=$arFields;
}
if(count($arResult["COMPANY"])>0 && !$default)
	$arResult["COMPANY"][0]["PROPERTY_DEFAULT_VALUE"]="Y";

$property_enums = CIBlockPropertyEnum::GetList(Array("DEF"=>"DESC", "SORT"=>"ASC"), Array("IBLOCK_ID"=>31, "CODE"=>"org_type"));
while($enum_fields = $property_enums->GetNext())
{
	$arResult["COMPANY_PARAMS"]["org_type"][$enum_fields["ID"]]=$enum_fields["VALUE"];
}

$default=false;
$rsContact=CIBlockElement::GetList(array(), array("IBLOCK_ID"=>33,"PROPERTY_USER"=>$USER->GetID()), false, false, array("ID","NAME","PROPERTY_fio","PROPERTY_email","PROPERTY_phone","PROPERTY_USER","PROPERTY_default"));
while ($arFields=$rsContact->GetNext()) {
	if($arFields["PROPERTY_DEFAULT_VALUE"]=="Y") $default=true;
	$arResult["CONTACTS"][]=$arFields;
}
if(count($arResult["CONTACTS"])==0 && $arResult["arUser"]["NAME"] && $arResult["arUser"]["EMAIL"] && $arResult["arUser"]["PERSONAL_PHONE"]){
	$arLoadArray=array(
		"NAME"=>$arResult["arUser"]["NAME"]." (".$arResult["arUser"]["EMAIL"].", ".$arResult["arUser"]["PERSONAL_PHONE"].")",
		"IBLOCK_ID"=>33,
		"ACTIVE"=>"Y",
		"PROPERTY_VALUES"=>array(
			"fio"=>$arResult["arUser"]["NAME"],
			"email"=>$arResult["arUser"]["EMAIL"],
			"phone"=>$arResult["arUser"]["PERSONAL_PHONE"],
			"USER"=>$USER->GetID(),
			"default"=>"Y"
		),
	);
				
	$el=new CIBlockElement;
	$res = $el->Add($arLoadArray);
	
	$rsContact=CIBlockElement::GetList(array(), array("IBLOCK_ID"=>33,"PROPERTY_USER"=>$USER->GetID()), false, false, array("ID","NAME","PROPERTY_fio","PROPERTY_email","PROPERTY_phone","PROPERTY_USER","PROPERTY_default"));
	while ($arFields=$rsContact->GetNext()) {
		if($arFields["PROPERTY_DEFAULT_VALUE"]=="Y") $default=true;
		$arResult["CONTACTS"][]=$arFields;
	}	
}
if(count($arResult["CONTACTS"])>0 && !$default)
	$arResult["CONTACTS"][0]["PROPERTY_DEFAULT_VALUE"]="Y";

$arFilterSubscribe = Array("IBLOCK_ID"=>22, 'NAME'=>$arResult["arUser"]["EMAIL"]);
$arSelectSubscribe = Array("ID", "NAME");
$arLimitSubscribe = ['nTopCount'=>1];
$resSubscribe = CIBlockElement::GetList(Array(), $arFilterSubscribe, false, $arLimitSubscribe, $arSelectSubscribe);  
if ($arFieldsSubscribe = $resSubscribe->Fetch()){
	$arResult['SUBSCRIBE'] = "Y";
}

?>