<?
/**
 * @global CMain $APPLICATION
 * @param array $arParams
 * @param array $arResult
 */
if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true)
	die();
?>
<?//p($arResult);?>


<p class="ttl">Логин пользователя</p>
<p class="dsc"><?=$arResult["arUser"]['LOGIN']?></p>
<? if(!empty($arResult["arUser"]['LAST_NAME'])): ?>
	<p class="ttl">Фамилия</p>
	<p class="dsc"><?=$arResult["arUser"]['LAST_NAME']?></p>
<? endif; ?>
<? if(!empty($arResult["arUser"]['NAME'])): ?>
	<p class="ttl">Имя</p>
	<p class="dsc"><?=$arResult["arUser"]['NAME']?></p>
<? endif; ?>
<? if(!empty($arResult["arUser"]['SECOND_NAME'])): ?>
	<p class="ttl">Отчество</p>
	<p class="dsc"><?=$arResult["arUser"]['SECOND_NAME']?></p>
<? endif; ?>

<?
/*
<p class="ttl">Организационно правовая форма</p>
<p class="dsc"><?= $arResult["arUser"]['ID'] ?></p>
*/
?>
<? if(!empty($arResult["arUser"]['PERSONAL_PHONE'])): ?>
	<p class="ttl">Телефон</p>
	<p class="dsc"><?= $arResult["arUser"]['PERSONAL_PHONE'] ?></p>
<? endif; ?>
<?if(!empty($arResult["arUser"]['DELIVERY_ADDRESS'])){?>
<p class="ttl">адрес доставки</p>
<p class="dsc"><?= $arResult["arUser"]['DELIVERY_ADDRESS'] ?></p>
<?}?>
