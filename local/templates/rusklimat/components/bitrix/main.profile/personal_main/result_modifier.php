<?
if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true)	die();

$arResult["arUser"]["DELIVERY_ADDRESS"]="";

$rsAddress=CIBlockElement::GetList(array("ID"=>"DESC"), array("IBLOCK_ID"=>30,"PROPERTY_USER"=>$USER->GetID()), false, array("nTopCount"=>1), array("ID","NAME","PROPERTY_city","PROPERTY_street","PROPERTY_house","PROPERTY_housing","PROPERTY_build","PROPERTY_apartment","PROPERTY_USER","PROPERTY_default"));
if ($arFields=$rsAddress->GetNext()) {
	$arResult["arUser"]["DELIVERY_ADDRESS"]=$arFields["NAME"];
}
if(!$arResult["arUser"]["DELIVERY_ADDRESS"]){
	CModule::IncludeModule("sale");
	$db_sales = CSaleOrderUserProps::GetList(
		array("DATE_UPDATE" => "DESC"),
		array("USER_ID" => $arResult["arUser"]['ID']),
		false,
		array("nTopCount"=>1)
	);

	while ($ar_sales = $db_sales->Fetch())
	{
		$prop=array();
		$db_propVals = CSaleOrderUserPropsValue::GetList(array("ID" => "ASC"), Array("USER_PROPS_ID"=>$ar_sales["ID"]));
		while ($arPropVals = $db_propVals->Fetch())
		{
			$prop[$arPropVals["PROP_CODE"]]=$arPropVals["VALUE"];
		}
		//if($prop["HIDE_CURRENT_REGION"]) $arResult["arUser"]["DELIVERY_ADDRESS"].=$prop["HIDE_CURRENT_REGION"].", ";
		if($prop["HIDE_CURRENT_CITY"]) $arResult["arUser"]["DELIVERY_ADDRESS"].=$prop["HIDE_CURRENT_CITY"].", ";
		if($prop["HIDE_DELIVERY_D_ADDR"]) $arResult["arUser"]["DELIVERY_ADDRESS"].=$prop["HIDE_DELIVERY_D_ADDR"].", ";
		if($prop["HIDE_DELIVERY_D_HOUSE"]) $arResult["arUser"]["DELIVERY_ADDRESS"].=$prop["HIDE_DELIVERY_D_HOUSE"].", ";
		if($prop["HIDE_DELIVERY_D_CORP"]) $arResult["arUser"]["DELIVERY_ADDRESS"].="корп. ".$prop["HIDE_DELIVERY_D_CORP"].", ";
		if($prop["HIDE_DELIVERY_D_BUILD"]) $arResult["arUser"]["DELIVERY_ADDRESS"].="стр. ".$prop["HIDE_DELIVERY_D_BUILD"].", ";
		if($prop["HIDE_DELIVERY_D_ROOM"]) $arResult["arUser"]["DELIVERY_ADDRESS"].=$prop["HIDE_DELIVERY_D_ROOM"].", ";
		$arResult["arUser"]["DELIVERY_ADDRESS"]=trim($arResult["arUser"]["DELIVERY_ADDRESS"],", ");
	}
}