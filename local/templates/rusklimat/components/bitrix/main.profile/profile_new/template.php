<?
/**
 * @global CMain $APPLICATION
 * @param array $arParams
 * @param array $arResult
 */
if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true)
	die();
?>

<script type="text/javascript" src="<?=SITE_TEMPLATE_PATH?>/j/lk.js"></script>

<? echo "<pre>";    echo "</pre>"; ?>

<!-- basket roll -->
<div class="roll lk c">
	<h1 class="ttl1">изменение данных</h1>
	
	<form method="post" name="form1" action="<?=$arResult["FORM_TARGET"]?>" enctype="multipart/form-data" id="personal_edit_form">
		<?=$arResult["BX_SESSION_CHECK"]?>
		<input type="hidden" name="lang" value="<?=LANG?>" />
		<input type="hidden" name="ID" value=<?=$arResult["ID"]?> />
		<div class="brd clmn-w c">
		
			<?ShowError($arResult["strProfileError"]);?>
			<?
			if ($arResult['DATA_SAVED'] == 'Y')
				ShowNote(GetMessage('PROFILE_DATA_SAVED'));
			?>
		
			<!-- left column holder -->
			<div class="clmn">
				<h2 class="ttl-lk">Основные данные</h2>
				<div class="frm">

					
					<label class="ttl">Организационно правовая форма</label>
					
					<div class="dsc">
						<select class="yur-select ik-30-lt" name="yur-select">
							<option value="1">Форма собственности 1</option>
							<option value="1">Форма собственности 2</option>
							<option value="1">Форма собственности 3</option>
						</select>
					</div>
				
					
					<label class="ttl">ФИО</label>
					<div class="dsc">
						<input class="inp inp-drk inp-t inp-lk" name="NAME" maxlength="255" value="<?=$arResult["arUser"]["NAME"]?>" type="text" required pattern="^.{2,100}$" />
						<div class="vfield" style="visibility:hidden">Пояснение ошибки валидации поля</div>
					</div>
					
					<label class="ttl">Телефон</label></td>
					<div class="dsc">
						<input class="inp inp-drk inp-t inp-lk" name="PERSONAL_PHONE" maxlength="255" value="<?=$arResult["arUser"]["PERSONAL_PHONE"]?>" type="text" required pattern="[\d]+" />
						<div class="vfield" style="visibility:hidden">Пояснение ошибки валидации поля</div>
					</div>
					
					<label class="ttl">Email</label></td>
					<div class="dsc">
						<input class="inp inp-drk inp-t inp-lk" name="EMAIL" maxlength="50" value="<? echo $arResult["arUser"]["EMAIL"]?>" type="email" required id="userEmailHere" />
						<input name="LOGIN" value="<? echo $arResult["arUser"]["LOGIN"]?>" maxlength="50" type="hidden" required id="userLoginHere" />
						<div class="vfield" style="visibility:hidden">Пояснение ошибки валидации поля</div>
					</div>

				</div>
				<!-- / frm end -->
				
				
				<h2 class="ttl-lk">Адрес доставки</h2>
				
				<fieldset class="radioset twolines adrset">
				 <? foreach ($arResult['arUser']['UF_DEL_ADDRESS'] as $k => $v) {?>
				 	
				 
					<div class="radioline">
						<input name="compRadio2" class="check-custom" type="radio" value="rad2<?=$k?>" id="rad2<?=$k?>" <? if ($k==0) {?>
							checked
						<?}?>>
						<label for="rad2<?=$k?>"><?=$v?></label>
					</div>	
					<?}?>
					</div>
				</fieldset>
				<!-- / radioset end -->
				
				<div class="lk-btns lk-btns-adr">
					<div class="btn btn-30 btn-b lk-btn-new" id="addAdrUnhide">Указать новый адрес</div>
				</div>
				<!-- / lk-btns end -->
				
				<div id="addAdr" style="display:none">
					<div class="frm lk-sp">

						<label class="ttl">Город, населенный пункт</label>
						
						<div class="dsc">
							<input class="inp inp-drk inp-t inp-lk" type="text"/>
						</div>
						
						<label class="ttl">Улица</label>
						<div class="dsc">
							<input class="inp inp-drk inp-t inp-lk" type="text"/>
						</div>
						<table class="tbl-lk-sm">
							<tr>
								<td class="td1">
									<label class="ttl">Дом</label>
									<div class="dsc">
										<input class="inp inp-drk inp-t inp-lk-sm" type="text"/>
									</div>
								</td>
								<td class="td1">
									<label class="ttl">Корпус</label>
									<div class="dsc">
										<input class="inp inp-drk inp-t inp-lk-sm" type="text"/>
									</div>
								</td>
								<td class="td1">
									<label class="ttl">Подъезд</label>
									<div class="dsc">
										<input class="inp inp-drk inp-t inp-lk-sm" type="text"/>
									</div>
								</td>
								<td class="td1">
									<label class="ttl">Кв./офис</label>
									<div class="dsc">
										<input class="inp inp-drk inp-t inp-lk-sm" type="text"/>
									</div>
								</td>
							</tr>
						</table>
					</div>
					<!-- / frm end -->
					<div class="lk-btns lk-btns-adr">
						<div class="btn btn-30 lk-btn-add">добавить адрес</div>
						<div class="btn btn-30 btn-g lk-btn-cancel">отменить</div>
					</div>
					<!-- / lk-btns end -->
				
				</div>
				<!-- / addAdr end-->
		
				
				
				<h2 class="ttl-lk">Настройки</h2>
				
				<fieldset class="checkset inline lk-set c">
					<div class="checkline">
						<input type="checkbox" id="checknews"><label for="checknews">Подписаться на новости</label>
					</div>
					<div class="checkline">
						<input type="checkbox" id="checkrss"><label for="checkrss">Рассылка RSS</label>
					</div>
				</fieldset>
		
				
			
			<!-- / left column holder end -->
			
			<!-- right column holder -->
			<div class="clmn">
			
				
				<h2 class="ttl-lk">Данные компании для<br/>формирования счета</h2>
				
				<fieldset class="radioset twolines adrset">
					<div class="radioline">
						<input name="compRadio" class="check-custom" type="radio" value="rad1" id="rad1" checked>
						<label for="">ООО “НПХ ТПЗ КМПП Сбербанк России” Банк Сбербанк 434242342524/4324</label>
					</div>	
					<div class="radioline">
						<input name="compRadio" class="check-custom" type="radio" value="rad2" id="rad2">
						<label for="">Адрес 2</label>
					</div>
					<div class="radioline">
						<input name="compRadio" class="check-custom" type="radio" value="rad2" id="rad3">
						<label for="">Адрес 3</label>
					</div>
				</fieldset>
				<!-- / radioset end -->
				
				<div class="lk-btns lk-btns-comp">
					<div class="btn btn-30 btn-b lk-btn-new" id="addCompUnhide">Указать новую компанию</div>
				</div>
				<!-- / lk-btns end -->
				
				<div id="addComp" style="display:none">
					<div class="frm lk-sp">
					
						<label class="ttl">Название организации</label>
						<div class="dsc">
							<input class="inp inp-drk inp-t inp-lk" value="пример ошибки валидации" type="text"/>
						</div>
						
						<label class="ttl">БИК</label></td>
						<div class="dsc">
							<input class="inp inp-drk inp-t inp-lk" type="text" />
						</div>
						
						<label class="ttl">Расчетный счет</label></td>
						<div class="dsc">
							<input class="inp inp-drk inp-t inp-lk" type="text" />
						</div>
						
						<label class="ttl">Наименование банка</label></td>
						<div class="dsc">
							<input class="inp inp-drk inp-t inp-lk" type="text" />
						</div>
						
						<label class="ttl">Кор. счет</label></td>
						<div class="dsc">
							<input class="inp inp-drk inp-t inp-lk" type="text" />
						</div>
					</div>
					<!-- / frm end -->
					<div class="lk-btns lk-btns-comp">
						<div class="btn btn-30 lk-btn-add">добавить компанию</div>
						<div class="btn btn-30 btn-g lk-btn-cancel">отменить</div>
					</div>
					<!-- / lk-btns end -->
				</div>
				<!-- / addComp end -->
				
				
				
				<h2 class="ttl-lk">Изменение пароля</h2>
				
				<div class="frm">
					
					
					<label class="ttl">Текущий пароль</label>
					<div class="dsc">
						<input class="inp inp-drk inp-t inp-lk" value="пример ошибки валидации" type="text"/>
					</div>
					
					
					<label class="ttl">Новый пароль</label></td>
					<div class="dsc">
						<input class="inp inp-drk inp-t inp-lk" type="password" name="NEW_PASSWORD" maxlength="50" value="" autocomplete="off" />
					</div>
					
					<label class="ttl">Повторите пароль</label></td>
					<div class="dsc">
						<input class="inp inp-drk inp-t inp-lk" type="password" name="NEW_PASSWORD_CONFIRM" maxlength="50" value="" autocomplete="off" />
					</div>

				</div>
				<!-- / frm end -->
				
			</div>
			<!-- / right column holder end -->
		</div>
		<!-- / brd end -->
		
		<button class="btn lk-btn-save" name="save" value="<?=(($arResult["ID"]>0) ? GetMessage("MAIN_SAVE") : GetMessage("MAIN_ADD"))?>" type="submit">Сохранить изменения</button>
	</form>
</div>
<!-- / lk roll end -->



