<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
	<div class="popup-ordercancel"></div>
	<div class="popup-ordermessage"></div>
<?if(!empty($arResult['ERRORS']['FATAL'])):?>

	<?foreach($arResult['ERRORS']['FATAL'] as $error):?>
		<?=ShowError($error)?>
	<?endforeach?>

	<?$component = $this->__component;?>
	<?if($arParams['AUTH_FORM_IN_TEMPLATE'] && isset($arResult['ERRORS']['FATAL'][$component::E_NOT_AUTHORIZED])):?>
		<?$APPLICATION->AuthForm('', false, false, 'N', false);?>
	<?endif?>

<?else:?>
<?//p($arResult['ORDERS']);?>
	<?if(!empty($arResult['ORDERS'])):?>
		<table class="tbl tbl-hover tbl-lt tbl-lk-history">
				<tr>
					<th class="col1">Номер заказа</th>
					<th class="col2">Дата и время заказа</th>
					<th class="col3">Товаров</th>
					<th class="col4">Сумма заказа</th>
					<th class="col5">Статус</th>
					<th class="col6">Действие</th>
				</tr>
			<?foreach($arResult['ORDERS'] as $order){?>
					<?//p($order);
					$cnt = 0;
					foreach ($order["BASKET_ITEMS"] as $item) {
						$cnt += $item['QUANTITY'];
					}
					?>
					<tr class="sp">
						<td colspan="5">&nbsp;</td>
					</tr>
					<tr>
						<td>
							<a href="<?=$order["ORDER"]["URL_TO_DETAIL"]?>"><?=$order["ORDER"]["ACCOUNT_NUMBER"]?></a>
						</td>
						<td>
							<?=$order["ORDER"]["DATE_INSERT_FORMATED"];?>
						</td>
						<td><?= $cnt ?></td>
						<td>
							<?=$order["ORDER"]["FORMATED_PRICE"]?>
						</td>
						<td><span class="st-grey"><?=$arResult["INFO"]["STATUS"][$order["ORDER"]["STATUS_ID"]]["NAME"] ?></span></td>
						<td>
							<a href="<?=$order["ORDER"]["URL_TO_DETAIL"]?>">Просмотр&nbsp;заказа</a>
							<?if($order["ORDER"]["STATUS_ID"]=="N" || $order["ORDER"]["STATUS_ID"]=="A"){
								if(!$order["ORDER"]["SEND_CANCEL"]){?>
								<br /><a href="javascript:void(0)" class="orderSendCancel" data-id="<?=$order["ORDER"]["ACCOUNT_NUMBER"]?>">Запросить отмену</a>
								<?}else{?>
								<br /><span>отправлена отмена</span>
								<?}?>
							<?}?>
							<br /><a href="javascript:void(0)" class="orderSendMessage" data-id="<?=$order["ORDER"]["ACCOUNT_NUMBER"]?>">Задать вопрос</a>
							<?if($order["ORDER"]["SEND_MESSAGE"]>0){?>
								<br /><span>отправлено запросов: <?=$order["ORDER"]["SEND_MESSAGE"]?></span>
							<?}?>
						</td>
					</tr>
			
			<?}?>
		</table>
		<?/*foreach($arResult["ORDER_BY_STATUS"] as $key => $group):?>
			<?
			if (empty($group)) {
				continue;
			}
			?>
		
			<!-- tbl-lk-history -->
			<table class="tbl tbl-hover tbl-lt tbl-lk-history">
				<tr>
					<th class="col1">Номер заказа</th>
					<th class="col2">Дата заказа</th>
					<th class="col3">Товаров</th>
					<th class="col4">Сумма заказа</th>
					<th class="col5">Статус</th>
					<th class="col6">Действие</th>
				</tr>
				<?foreach($group as $k => $order):?>
					<?//p($order);
					$cnt = 0;
					foreach ($order["BASKET_ITEMS"] as $item) {
						$cnt += $item['QUANTITY'];
					}
					?>
					<tr class="sp">
						<td colspan="5">&nbsp;</td>
					</tr>
					<tr>
						<td>
							<a href="<?=$order["ORDER"]["URL_TO_DETAIL"]?>"><?=$order["ORDER"]["ACCOUNT_NUMBER"]?></a>
						</td>
						<td>
							<?=$order["ORDER"]["DATE_INSERT_FORMATED"];?>
						</td>
						<td><?= $cnt ?></td>
						<td>
							<?=$order["ORDER"]["FORMATED_PRICE"]?>
						</td>
						<td><span class="st-grey"><?=$arResult["INFO"]["STATUS"][$key]["NAME"] ?></span></td>
						<td>
							<a href="<?=$order["ORDER"]["URL_TO_DETAIL"]?>">Просмотр&nbsp;заказа</a>
						</td>
					</tr>
				<?endforeach?>
			</table>
			<!-- / tbl-lk-history end -->

		<?endforeach*/?>

		<?if(strlen($arResult['NAV_STRING'])):?>
			<?=$arResult['NAV_STRING']?>
		<?endif?>

	<?else:?>
		<?=GetMessage('SPOL_NO_ORDERS')?>
	<?endif?>

<?endif?>