<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
$PRICE_CODE = $arParams['PRICE_CODE'][0];
$PRICE_CODE_NODISCOUNT = $arParams['PRICE_CODE'][1];

$FILTER_IDS = $GLOBALS[$arParams['FILTER_NAME']]['ID'];

$ID_TO_KEY = [];
foreach ($arResult['ITEMS'] as $key => $arItem) {
	$ID_TO_KEY[$arItem['ID']] = $key;
}

?>

<!-- roll -->
<div class="roll">
	<h1 class="ttl1 heart">Избранное (<span id="favMidCount"></span>)</h1>
	<div class="favorites c">

		<? foreach ($FILTER_IDS as $k => $FILTER_ID): ?>
			<?
			$arItem = $arResult['ITEMS'][$ID_TO_KEY[intval($FILTER_ID)]];
			if (empty($arItem)) {
				continue;
			}
			$pic = false;
			$params = false;
			if (!empty($arItem['PRODUCT_PREVIEW']['SRC'])) {
				$pic = $arItem['PRODUCT_PREVIEW']['SRC'];
			}
			?>
			<!-- item w/stars -->
			<div class="item b-base">
				<div class="hide_it"><input type="checkbox" class="addFav check-custom" id="addFav_<?= $arItem['ID'] ?>" -data-id="<?= $arItem['ID'] ?>"></div>
				<label for="addFav_<?= $arItem['ID'] ?>" class="b-x"></label>
				<div class="w c">
					<a href="<?= $arItem['DETAIL_PAGE_URL'] ?>" class="lnk">
						<div class="pic"<? if($pic): ?> style="background-image:url('<?= $pic ?>');"<? endif; ?>></div>
						<?if(count($arItem["LABELS"])>0){?>
						<div class="stamps c">
							<?foreach($arItem["LABELS"] as $i=>$label){?>
								<?/*if($i%2==0 && $i!=0){?><div class="clear"></div><?}*/?>
								<div class="ico" style="background-image: url('<?=$label["PREVIEW_PICTURE"]["SRC"]?>');<?if($label["PROPERTY_COLOR_VALUE"]){?>color: #<?=trim($label["PROPERTY_COLOR_VALUE"],"#")?>;<?}?>"><?=$label["PROPERTY_TEXT_VALUE"]?></div>
							<?}?>
							<?/*<div class="clear"></div>*/?>
						</div>
						<?}?>

						<div class="ttl"><?= !empty($arItem['DISPLAY_PROPERTIES']['item_name']['DISPLAY_VALUE']) ? $arItem['DISPLAY_PROPERTIES']['item_name']['DISPLAY_VALUE'] : $arItem['NAME'] ?></div>
						<?/*<div class="dsc">Electrolux EACS/I - 13 HO/N3 серии Orlando DC Inverter система</div>*/?>
					</a>
					<div class="rating c">
						<div class="stars" -data-score="<?= intval($arItem['TOTAL_RATE']) ?>"></div>
						<a href="<?= $arItem['DETAIL_PAGE_URL'] ?>#tabRevw" class="rev">Отзывов: <?= intval($arItem['REVIEWS']) ?></a>
					</div>
					<?if($arItem['PRICES'][$PRICE_CODE]['PRINT_VALUE']){?>
					<div class="prc">
						<div class="curr<? if(empty($arItem['PRICES'][$PRICE_CODE_NODISCOUNT]['PRINT_VALUE'])): ?> curr-only<? endif; ?>"><?= !empty($arItem['PRICES'][$PRICE_CODE]['PRINT_VALUE']) ? $arItem['PRICES'][$PRICE_CODE]['PRINT_VALUE'] : ' <span>руб.</span>' ?></div>
						<? if(!empty($arItem['PRICES'][$PRICE_CODE_NODISCOUNT]['PRINT_VALUE'])): ?>
							<div class="past">
								<?
								$diff = $arItem['PRICES'][$PRICE_CODE_NODISCOUNT]['VALUE'] - $arItem['PRICES'][$PRICE_CODE]['VALUE'];
								$diff = ceil($diff);
								$diff = abs($diff);
								?>
								<span class="old"><?= number_format($arItem['PRICES'][$PRICE_CODE_NODISCOUNT]['VALUE'], 0, '.', ' ') ?></span> <span class="profit"><?= number_format($diff, 0, '.', ' ') ?> выгода</span>
							</div>
						<? endif; ?>
					</div>
					<a class="btn btn-to-bsk" href="<?= $arItem['ADD_URL'] ?>" id="add_to_basket_<?= $arItem['ID'] ?>">Купить</a>
					<?}else{?>
					<div class="prc">
						Извините, товар отсутствует в продаже
					</div>	
					<?}?>
					<?
					$cats = [];
					foreach($arItem['SECTION']['PATH'] as $arSection) {
						$cats[] = $arSection['NAME'];
					}
					?>
					<script>
						$('#add_to_basket_<?= $arItem['ID'] ?>').on('click', function() {
							window.dataLayer = window.dataLayer || [];
							window.dataLayer.push({
								'event': 'addToCart',
								"ecommerce": {
									"currencyCode": "RUB",
									"add": {
										"products": [
											{
												"id": "<?= $arItem['DISPLAY_PROPERTIES']['CODE']['DISPLAY_VALUE'] ?>",
												"name" : "<?= !empty($arItem['DISPLAY_PROPERTIES']['item_name']['DISPLAY_VALUE']) ? $arItem['DISPLAY_PROPERTIES']['item_name']['DISPLAY_VALUE'] : $arItem['NAME'] ?>",
												"price": <?= number_format($arItem['PRICES'][$PRICE_CODE]['VALUE'], 2, '.', '') ?>,
												"brand": "<?= $arItem['DISPLAY_PROPERTIES']['EL_BRAND']['LINK_ELEMENT_VALUE'][$arItem['DISPLAY_PROPERTIES']['EL_BRAND']['VALUE']]['NAME'] ?>",
												"category": "<?= implode('/', $cats) ?>",
												"quantity": 1
											}
										]
									}
								}
							});
						});
					</script>

				</div>
			</div>
			<!-- / item w/stars -->
		<? endforeach; ?>

	</div>
	<!-- / favorites end -->
	<?/*<div class="btn btn-more btn-g">Показать еще</div>*/?>
</div>
<!-- / roll end -->
