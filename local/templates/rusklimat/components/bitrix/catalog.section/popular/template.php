<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
$PRICE_CODE = $arParams['PRICE_CODE'][0];
$PRICE_CODE_NODISCOUNT = $arParams['PRICE_CODE'][1];

?><? if (!empty($arResult['ITEMS'])): ?>
	<!-- other deals roll -->
	<div class="roll">
		<h2 class="ttl-r"><?if(strpos($_SERVER["REQUEST_URI"],"/personal/cart/")===0){?>на что еще стоит обратить внимание?<?}else{?>популярные товары<?}?></h2>
		<!-- other deals -->
		<div class="other-deals c">
			<!-- slides -->
			<div class="slides c popular_items" id="other">

				<? foreach ($arResult['ITEMS'] as $key => $arItem): ?>
					<?
					$pic = false;
					$params = false;
					if (!empty($arItem['PRODUCT_PREVIEW']['SRC'])) {
						$pic = $arItem['PRODUCT_PREVIEW']['SRC'];
					}
					?>
					<!-- item -->
					<div class="item b-base">
						<div class="w c">

							<a href="<?= $arItem['DETAIL_PAGE_URL'] ?>" class="lnk">
								<div class="ttl"><?= !empty($arItem['DISPLAY_PROPERTIES']['item_name']['DISPLAY_VALUE']) ? $arItem['DISPLAY_PROPERTIES']['item_name']['DISPLAY_VALUE'] : $arItem['NAME'] ?></div>
								<div class="pic" style="text-align:center;"<? /*if($pic): ?> style="background-image:url('<?= $pic ?>');"<? endif;*/ ?>>
									<? if($pic): ?><img style="height:100%; width:auto;" data-lazy="<?= $pic ?>"/><? endif; ?>
								</div>
								<?if(count($arItem["LABELS"])>0){?>
								<div class="stamps c">
									<?foreach($arItem["LABELS"] as $i=>$label){?>
										<?/*if($i%2==0 && $i!=0){?><div class="clear"></div><?}*/?>
										<div class="ico" style="background-image: url('<?=$label["PREVIEW_PICTURE"]["SRC"]?>');<?if($label["PROPERTY_COLOR_VALUE"]){?>color: #<?=trim($label["PROPERTY_COLOR_VALUE"],"#")?>;<?}?>"><?=$label["PROPERTY_TEXT_VALUE"]?></div>
									<?}?>
									<?/*<div class="clear"></div>*/?>
								</div>
								<?}?>
							</a>
							<div class="prc">
								<div class="curr<? if(empty($arItem['PRICES'][$PRICE_CODE_NODISCOUNT]['PRINT_VALUE'])): ?> curr-only<? endif; ?>"><?= !empty($arItem['PRICES'][$PRICE_CODE]['PRINT_VALUE']) ? $arItem['PRICES'][$PRICE_CODE]['PRINT_VALUE'] : ' <span>руб.</span>' ?></div>
								<? if(!empty($arItem['PRICES'][$PRICE_CODE_NODISCOUNT]['PRINT_VALUE'])): ?>
									<div class="past">
										<?
										$diff = $arItem['PRICES'][$PRICE_CODE_NODISCOUNT]['VALUE'] - $arItem['PRICES'][$PRICE_CODE]['VALUE'];
										$diff = ceil($diff);
										$diff = abs($diff);
										?>
										<span class="old"><?= number_format($arItem['PRICES'][$PRICE_CODE_NODISCOUNT]['VALUE'], 0, '.', ' ') ?></span> <span class="profit"><?= number_format($diff, 0, '.', ' ') ?> выгода</span>
									</div>
								<? endif; ?>
							</div>
							<a class="btn btn-to-bsk" href="<?= $arItem['ADD_URL'] ?>" id="add_to_basket_<?= $arItem['ID'] ?>">Купить</a>

							<?
							$cats = [];
							foreach($arItem['SECTION']['PATH'] as $arSection) {
								$cats[] = $arSection['NAME'];
							}
							?>
							<script>
								$('#add_to_basket_<?= $arItem['ID'] ?>').on('click', function() {
									window.dataLayer = window.dataLayer || [];
									window.dataLayer.push({
										'event': 'addToCart',
										"ecommerce": {
											"currencyCode": "RUB",
											"add": {
												"products": [
													{
														"id": "<?= $arItem['DISPLAY_PROPERTIES']['CODE']['DISPLAY_VALUE'] ?>",
														"name" : "<?= !empty($arItem['DISPLAY_PROPERTIES']['item_name']['DISPLAY_VALUE']) ? $arItem['DISPLAY_PROPERTIES']['item_name']['DISPLAY_VALUE'] : $arItem['NAME'] ?>",
														"price": <?= number_format($arItem['PRICES'][$PRICE_CODE]['VALUE'], 2, '.', '') ?>,
														"brand": "<?= $arItem['DISPLAY_PROPERTIES']['EL_BRAND']['LINK_ELEMENT_VALUE'][$arItem['DISPLAY_PROPERTIES']['EL_BRAND']['VALUE']]['NAME'] ?>",
														"category": "<?= implode('/', $cats) ?>",
														"quantity": 1
													}
												]
											}
										}
									});
								});
							</script>

						</div>
					</div>
					<!-- / item -->
				<? endforeach; ?>

			</div>
			<!-- / slides end -->
		</div>
		<!-- / other deals end -->
	</div>
	<!-- / other deals roll end -->
<? endif; ?>
