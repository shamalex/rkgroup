<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);

CModule::IncludeModule('iblock');

$arSort = [$arParams['SORT_BY1'] => $arParams['SORT_ORDER1'], $arParams['SORT_BY2'] => $arParams['SORT_ORDER2']];
$arFilter = Array("IBLOCK_ID"=>$arParams['IBLOCK_ID']);
$arLimit = ['nTopCount'=>1];
$arSelect = ['ID', 'CODE'];
$res = CIBlockElement::GetList($arSort, $arFilter, false, $arLimit, $arSelect);  
if ($arFields = $res->Fetch()) {
	$arResult['VARIABLES']['ELEMENT_CODE'] = $arFields['CODE'];
}

include($_SERVER["DOCUMENT_ROOT"].$templateFolder.'/detail.php');
?>
