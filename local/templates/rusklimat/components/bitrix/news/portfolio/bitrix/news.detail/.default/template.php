<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>

<!-- roll -->
<div class="roll portofolio">
	<h1 class="ttl1"><?=$arResult["NAME"]?></h1>

	<? if(!empty($arResult["DISPLAY_PROPERTIES"]['IMAGES']['FILE_VALUE'])): ?>
		<?
		$files = $arResult["DISPLAY_PROPERTIES"]['IMAGES']['FILE_VALUE'];
		while(count($files) <= 5) {
			$files = array_merge($files, $arResult["DISPLAY_PROPERTIES"]['IMAGES']['FILE_VALUE']);
		}
		?>
		<!-- port-display -->
		<div class="port-display">
			<!-- items slides -->
			<div class="items slides" id="portDisplay">
			
				<? foreach($files as $file): ?>
					<!-- item -->
					<div class="item" style="background-image:url('<?= $file['SRC'] ?>');"></div>
					<!-- / item -->
				<? endforeach; ?>
				
			</div>
			<!-- / items slides end -->
		</div>
		<!-- / port-display end -->
		
		<!-- port-nav -->
		<div class="port-nav">
			<!-- items slides -->
			<div class="items slides c" id="portNav">
				<? foreach($files as $file): ?>
					<div class="item" style="background-image:url('<?= $file['SRC'] ?>');"></div>
				<? endforeach; ?>
			</div>
			<!-- / items slides end -->			
		</div>
		<!-- / port-nav end -->
	<? endif; ?>

</div>
<!-- / roll end -->

<!-- port-info -->
<div class="roll port-info c">
	
	<!-- cln-r -->
	<div class="col-r">
	
		<? if(!empty($arResult["DISPLAY_PROPERTIES"]['SQARE']['DISPLAY_VALUE'])): ?>
			<?
			$sqare = $arResult["DISPLAY_PROPERTIES"]['SQARE']['DISPLAY_VALUE'];
			$sqare = preg_replace('/[^\d]+/ui', '', $sqare);
			$sqare = number_format($sqare, 0, '', ' ');
			?>
			<!-- / cln-s-->
			<div class="cln-s">
				<div class="ttl"><?= $sqare ?> м²</div>
				<div class="dsc">Площадь объекта</div>
			</div>
			<!-- / cln-s-->
		<? endif; ?>
		
		<? if(!empty($arResult["DISPLAY_PROPERTIES"]['PRICE']['DISPLAY_VALUE'])): ?>
			<?
			$price = $arResult["DISPLAY_PROPERTIES"]['PRICE']['DISPLAY_VALUE'];
			$price = preg_replace('/[^\d]+/ui', '', $price);
			$price = number_format($price, 0, '', ' ');
			?>
			<!-- / cln-s-->
			<div class="cln-s">
				<div class="ttl"><?= $price ?> руб.</div>
				<div class="dsc">Итоговая стоимость проекта</div>
			</div>
			<!-- / cln-s-->
		<? endif; ?>
	
	</div>
	<!-- / cln-r -->

	<!-- cln-l -->
	<div class="col-l txt-block">
		<? if(!empty($arResult["DISPLAY_PROPERTIES"]['TITLE1']['DISPLAY_VALUE'])): ?>
			<h2 class="ttl2"><?= $arResult["DISPLAY_PROPERTIES"]['TITLE1']['DISPLAY_VALUE'] ?></h2>
		<? endif; ?>
		<?= $arResult["DISPLAY_PROPERTIES"]['DESC1']['DISPLAY_VALUE'] ?>
	</div>
	<!-- / cln-l-->

</div>
<!-- / port-info end -->

<?/* if(!empty($arResult["DISPLAY_PROPERTIES"]['CATS']['ELEMENTS']) && !empty($arResult["DISPLAY_PROPERTIES"]['BRANDS']['LINK_ELEMENT_VALUE']) && count($arResult["DISPLAY_PROPERTIES"]['CATS']['ELEMENTS']) == count($arResult["DISPLAY_PROPERTIES"]['BRANDS']['LINK_ELEMENT_VALUE'])): ?>
	<?
	$propID = $arParams['PROP_ID'];
	?>
	<!-- pop-cat roll -->
	<div class="roll pop-cat">
		<? if(!empty($arResult["DISPLAY_PROPERTIES"]['TITLE2']['DISPLAY_VALUE'])): ?>
			<h2 class="ttl2"><?= $arResult["DISPLAY_PROPERTIES"]['TITLE2']['DISPLAY_VALUE'] ?></h2>
		<? endif; ?>
		<? if(!empty($arResult["DISPLAY_PROPERTIES"]['DESC2']['DISPLAY_VALUE'])): ?>
			<div class="txt-block">
				<?= $arResult["DISPLAY_PROPERTIES"]['DESC2']['DISPLAY_VALUE'] ?>
			</div>
		<? endif; ?>
		
		<? if(!empty($arResult["DISPLAY_PROPERTIES"]['BRANDS']['DISPLAY_VALUE'])): ?>
			<!-- items -->
			<div class="items c">
				
				<?
				$iCat = 0;
				?>
				<? foreach($arResult["DISPLAY_PROPERTIES"]['BRANDS']['LINK_ELEMENT_VALUE'] as $arBrand): ?>
					<?
					$arCat = $arResult["DISPLAY_PROPERTIES"]['CATS']['ELEMENTS'][$iCat];
					$iCat++;
					$key = $arBrand['ID'];
					$htmlKey = htmlspecialcharsbx($key);
					$keyCrc = abs(crc32($htmlKey));
					?>
					<!-- item -->
					<a href="<?= $arCat['SECTION_PAGE_URL'] ?>?arrFilter_<?= $propID ?>_<?= $keyCrc ?>=Y&set_filter=Показать" class="item c-block">
						<div class="pic"<? if(!empty($arBrand['PREVIEW_PICTURE']['SRC'])): ?> style="background-image:url('<?= $arBrand['PREVIEW_PICTURE']['SRC'] ?>');"<? endif; ?>></div>
						<h3 class="ttl"><?= $arCat['NAME'] ?></h3>
					</a>
					<!-- / item -->
				<? endforeach; ?>

			</div>
			<!-- / items -->
		<? endif; ?>
	</div>	
	<!-- / pop-cat roll end -->
<? endif; */?>
<?if(!empty($arResult["DISPLAY_PROPERTIES"]['BRANDS']['LINK_ELEMENT_VALUE'])){?>
	<?
	$propID = $arParams['PROP_ID'];
	?>
	<!-- pop-cat roll -->
	<div class="roll pop-cat">
		<? if(!empty($arResult["DISPLAY_PROPERTIES"]['TITLE2']['DISPLAY_VALUE'])): ?>
			<h2 class="ttl2"><?= $arResult["DISPLAY_PROPERTIES"]['TITLE2']['DISPLAY_VALUE'] ?></h2>
		<? endif; ?>
		<? if(!empty($arResult["DISPLAY_PROPERTIES"]['DESC2']['DISPLAY_VALUE'])): ?>
			<div class="txt-block">
				<?= $arResult["DISPLAY_PROPERTIES"]['DESC2']['DISPLAY_VALUE'] ?>
			</div>
		<? endif; ?>
		
		<? if(!empty($arResult["DISPLAY_PROPERTIES"]['BRANDS']['DISPLAY_VALUE'])): ?>
			<!-- items -->
			<div class="items c">
				<? foreach($arResult["DISPLAY_PROPERTIES"]['BRANDS']['LINK_ELEMENT_VALUE'] as $arBrand): ?>
					<?
					
					$key = $arBrand['ID'];
					$htmlKey = htmlspecialcharsbx($key);
					$keyCrc = abs(crc32($htmlKey));
					?>
					<!-- item -->
					<div class="item c-block">
						<a href="/catalog/brands/<?=$arBrand["CODE"]?>/" >
							<div class="pic"<? if(!empty($arBrand['PREVIEW_PICTURE']['SRC'])){?> style="background-image:url('<?= $arBrand['PREVIEW_PICTURE']['SRC'] ?>');"<?}?>></div>
						</a>
						<div class="ttl">
							<?foreach($arBrand["PROPERTY_PORTFOLIO_LINKS_VALUE"] as $key=>$link){?>
							<a href="<?=$link?>" style="display: block;">
								<?= $arBrand["PROPERTY_PORTFOLIO_LINKS_DESCRIPTION"][$key] ?>
							</a>
							<?}?>
						</div>
					</div>
					<!-- / item -->
				<? endforeach; ?>

			</div>
			<!-- / items -->
		<? endif; ?>
	</div>	
	<!-- / pop-cat roll end -->
<?}?>
