<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

if (!empty($arResult["DISPLAY_PROPERTIES"]['CATS']['VALUE'])) {
	CModule::IncludeModule('iblock');

	$arSort = false;
	$arFilter = Array("IBLOCK_ID"=>$arResult["DISPLAY_PROPERTIES"]['CATS']['LINK_IBLOCK_ID'], "ID"=>$arResult["DISPLAY_PROPERTIES"]['CATS']['VALUE']);
	$arSelect = ['ID', 'CODE', 'NAME', 'SECTION_PAGE_URL', 'PICTURE'];
	$res = CIBlockSection::GetList(Array(), $arFilter, false, $arSelect);  
	while ($arFields = $res->GetNext()) {
		if (!empty($arFields['PICTURE'])) {
			$arFields['PICTURE'] = CFile::GetFileArray($arFields['PICTURE']);
		}
		$arResult["DISPLAY_PROPERTIES"]['CATS']['ELEMENTS'][] = $arFields;
	}
}
if (!empty($arResult["DISPLAY_PROPERTIES"]['BRANDS']['VALUE'])) {
	CModule::IncludeModule('iblock');

	$arSort = false;
	$arFilter = Array("IBLOCK_ID"=>$arResult["DISPLAY_PROPERTIES"]['BRANDS']['LINK_IBLOCK_ID'], "ID"=>$arResult["DISPLAY_PROPERTIES"]['BRANDS']['VALUE']);
	$arSelect = ['ID', 'CODE', 'NAME', 'DETAIL_PAGE_URL', 'PREVIEW_PICTURE', "PROPERTY_portfolio_links"];
	$res = CIBlockElement::GetList(Array(), $arFilter, false, false, $arSelect);  
	while ($arFields = $res->GetNext()) {
		if (!empty($arFields['PREVIEW_PICTURE'])) {
			$arFields['PREVIEW_PICTURE'] = CFile::GetFileArray($arFields['PREVIEW_PICTURE']);
		}
		$arResult["DISPLAY_PROPERTIES"]['BRANDS']['LINK_ELEMENT_VALUE'][$arFields['ID']] = array_merge($arResult["DISPLAY_PROPERTIES"]['BRANDS']['LINK_ELEMENT_VALUE'][$arFields['ID']], $arFields);
	}
}