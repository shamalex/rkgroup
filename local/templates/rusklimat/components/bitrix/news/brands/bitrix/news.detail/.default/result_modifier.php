<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

// p($arParams);
// p($arResult);

$arSort = Array('NAME'=>'ASC', 'ID'=>'DESC');
$arFilter = Array("IBLOCK_ID"=>$arResult['IBLOCK_ID'], 'ACTIVE'=>'Y');
$arSelect = Array("ID", "NAME", "DETAIL_PAGE_URL");
$res = CIBlockElement::GetList($arSort, $arFilter, false, false, $arSelect);  
while($arFields = $res->GetNext()) {  
	$arResult['ALL_BRANDS'][] = $arFields;
}

if(!empty($arResult["ALL_BRANDS"])) {
	
	$elementsIDS = [];
	
	foreach($arResult["ALL_BRANDS"] as $arItem) {
		$elementsIDS[] = $arItem['ID'];
	}


	$CNT = [];
	$arFilter = Array("IBLOCK_ID"=>$arParams['CATALOG_IBLOCK_ID'], "INCLUDE_SUBSECTIONS"=>"Y", "PROPERTY_EL_BRAND"=>$elementsIDS);
	if (!empty($arParams['FILTER'])) {
		$arFilter = array_merge($arFilter, $arParams['FILTER']);
	}
	$arSelect = Array("ID", "PROPERTY_EL_BRAND");
	$res = CIBlockElement::GetList(Array(), $arFilter, false, false, $arSelect);
	while($arFields = $res->Fetch()) {
		$CNT[$arFields['PROPERTY_EL_BRAND_VALUE']]++;
	}
	
	foreach($arResult["ALL_BRANDS"] as $i=>$arItem) {
		if (!empty(intval($CNT[$arItem['ID']]))) {
			$arResult["ALL_BRANDS"][$i]['ELEMENT_CNT'] = intval($CNT[$arItem['ID']]);
		} else {
			unset($arResult["ALL_BRANDS"][$i]);
		}
	}
	$arResult["ALL_BRANDS"] = array_values($arResult["ALL_BRANDS"]);
	
}