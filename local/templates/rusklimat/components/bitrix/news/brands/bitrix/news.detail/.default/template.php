<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
$half_brands = ceil(count($arResult['ALL_BRANDS'])/2);
?>

<!-- cat-brands -->
<div class="cat-brands roll c">
	<h1 class="ttl1">О товарах бренда <?= $arResult["NAME"] ?></h1>

	<!-- col-l -->
	<div class="col-l">
<?/*		<!-- v-gr -->
		<div class="v-gr c">
			<ul class="brand-list">
				<? foreach($arResult['ALL_BRANDS'] as $i=>$brand): ?>
					<? if($i==$half_brands): ?>
						</ul>
						<ul class="brand-list">
					<? endif; ?>
					<li><a href="<?= $brand['DETAIL_PAGE_URL'] ?>"><?= $brand['NAME'] ?></a></li>
				<? endforeach; ?>
			</ul>
		</div>
		<!-- / v-gr end -->
*/?>
		<?if($arResult["PROPERTIES"]["BANNER"]["VALUE"]){
		$renderImage = CFile::ResizeImageGet($arResult["PROPERTIES"]["BANNER"]["VALUE"], Array("width" => 356, "height" => 320));?>
		<div class="v-gr">
			<a href="<?=$arResult["PROPERTIES"]["BANNER"]["DESCRIPTION"]?>" class="pic-offer" style="background-image:url('<?=$renderImage['src']?>');"></a>
		</div>
		<?}?>

<?/*		<!-- v-gr -->
		<div class="v-gr">
			<h2 class="ttl2">Требуется помощь?</h2>

			<a class="item b-hlp callback-pop" href="#">
				<div class="w">
					<div class="ttl">Закажите звонок!</div>
					<p class="dsc">Удобная функция заказа<br/>обратного звонка.</p>
					<div class="ico ico-1">&nbsp;</div>
				</div>
			</a>
			<a class="item b-hlp" href="tel:88007771977">
				<div class="w">
					<div class="ttl">8 800 777-19-77</div>
					<p class="dsc">Помогаем по любым<br/>вопросам продажи и сервиса.</p>
					<div class="ico ico-2">&nbsp;</div>
				</div>
			</a>
		</div>
		<!-- / v-gr end -->
*/?>
		<?
		/*
		<!-- v-gr -->
		<div class="v-gr">
			<h2 class="ttl2">Горячее предложение</h2>
			<!-- item w/timer -->
			<div class="item b-base">
				<div class="w c">

					<div class="timer">
						<div class="sum">Успей сэкономить 80 рублей</div>
						<div class="time" data-countdown="2015/08/08">
							<!-- <b>11</b> <span>дн.</span> <b>11</b> <span>ч.</span> <b>11</b> <span>мин.</span> <b class="sec">11</b> <span>сек.</span> -->
						</div>
					</div>

					<a class="lnk" href="card.html">
						<div class="ttl">Инверторная сплит система система система система</div>
						<div class="pic" style="background-image:url('i/blocks/hot1.png');">
							<div class="ico-hot">Успей<br/>купить</div>
						</div>
						<div class="dsc">Electrolux EACS/I - 13 HO/N3 серии Orlando DC Inverter система</div>
					</a>

					<div class="prc">
						<div class="curr">18 990 руб.</div>
						<div class="past">
							<span class="old">89 490</span> <span class="profit">1 666 выгода</span>
						</div>
					</div>
					<div class="btn btn-to-bsk">Купить</div>

				</div>
			</div>
			<!-- / item w/timer -->
		</div>
		<!-- / v-gr end -->
		*/
		?>

	</div>
	<!-- / col-l end-->

	<!-- col-r -->
	<div <?/*class="col-r"*/?>>
		<div class="cat-brand-logo" id="thisIsTop"<?if($arParams["DISPLAY_PICTURE"]!="N" && is_array($arResult["DETAIL_PICTURE"])):?> style="background-image:url('<?=$arResult["DETAIL_PICTURE"]["SRC"]?>');"<?endif?>></div>

		<?$APPLICATION->IncludeComponent("bitrix:catalog.section.list", "brand_sections", Array(
			"ADD_SECTIONS_CHAIN" => "N",	// Включать раздел в цепочку навигации
				"CACHE_GROUPS" => "N",	// Учитывать права доступа
				"CACHE_TIME" => "36000000",	// Время кеширования (сек.)
				"CACHE_TYPE" => "A",	// Тип кеширования
				"COMPONENT_TEMPLATE" => ".default",
				"COUNT_ELEMENTS" => "N",	// Показывать количество элементов в разделе
				"IBLOCK_ID" => "8",	// Инфоблок
				"IBLOCK_TYPE" => "catalog",	// Тип инфоблока
				"SECTION_CODE" => "",	// Код раздела
				"SECTION_FIELDS" => "",	// Поля разделов
				"SECTION_ID" => "",	// ID раздела
				"SECTION_URL" => "",	// URL, ведущий на страницу с содержимым раздела
				"SECTION_USER_FIELDS" => "",	// Свойства разделов
				"SHOW_PARENT_NAME" => "Y",	// Показывать название раздела
				"TOP_DEPTH" => "",	// Максимальная отображаемая глубина разделов
				"TOP_DEPTH_LIMIT" => "2",	// Максимальная отображаемая глубина разделов
				"VIEW_MODE" => "LIST",	// Вид списка подразделов
				"BRAND_ID" => $arResult["ID"],
				"PROP_ID" => 52,
				"FILTER" => $arParams['FILTER'],
			),
			false
		);?>

		<div class="intro txt-block">
			<?if(strlen($arResult["DETAIL_TEXT"])>0):?>
				<?echo $arResult["DETAIL_TEXT"];?>
			<?else:?>
				<?echo $arResult["PREVIEW_TEXT"];?>
			<?endif?>
		</div>
		<?global $filterBrand;
		$filterBrand=array("PROPERTY_BRAND"=>$arResult["ID"]);?>
		<?$APPLICATION->IncludeComponent("bitrix:news.list", "news_brand", Array(
			"ACTIVE_DATE_FORMAT" => "d.m.Y",	// Формат показа даты
				"ADD_SECTIONS_CHAIN" => "N",	// Включать раздел в цепочку навигации
				"AJAX_MODE" => "N",	// Включить режим AJAX
				"AJAX_OPTION_ADDITIONAL" => "",	// Дополнительный идентификатор
				"AJAX_OPTION_HISTORY" => "N",	// Включить эмуляцию навигации браузера
				"AJAX_OPTION_JUMP" => "N",	// Включить прокрутку к началу компонента
				"AJAX_OPTION_STYLE" => "N",	// Включить подгрузку стилей
				"CACHE_FILTER" => "Y",	// Кешировать при установленном фильтре
				"CACHE_GROUPS" => "N",	// Учитывать права доступа
				"CACHE_TIME" => "36000000",	// Время кеширования (сек.)
				"CACHE_TYPE" => "A",	// Тип кеширования
				"CHECK_DATES" => "N",	// Показывать только активные на данный момент элементы
				"COMPONENT_TEMPLATE" => ".default",
				"DETAIL_URL" => "",	// URL страницы детального просмотра (по умолчанию - из настроек инфоблока)
				"DISPLAY_BOTTOM_PAGER" => "N",	// Выводить под списком
				"DISPLAY_DATE" => "Y",	// Выводить дату элемента
				"DISPLAY_NAME" => "Y",	// Выводить название элемента
				"DISPLAY_PICTURE" => "Y",	// Выводить изображение для анонса
				"DISPLAY_PREVIEW_TEXT" => "N",	// Выводить текст анонса
				"DISPLAY_TOP_PAGER" => "N",	// Выводить над списком
				"FIELD_CODE" => "",	// Поля
				"FILTER_NAME" => "filterBrand",	// Фильтр
				"HIDE_LINK_WHEN_NO_DETAIL" => "N",	// Скрывать ссылку, если нет детального описания
				"IBLOCK_ID" => "6",	// Код информационного блока
				"IBLOCK_TYPE" => "news",	// Тип информационного блока (используется только для проверки)
				"INCLUDE_IBLOCK_INTO_CHAIN" => "N",	// Включать инфоблок в цепочку навигации
				"INCLUDE_SUBSECTIONS" => "Y",	// Показывать элементы подразделов раздела
				"MESSAGE_404" => "",	// Сообщение для показа (по умолчанию из компонента)
				"NEWS_COUNT" => "3",	// Количество новостей на странице
				"PAGER_BASE_LINK_ENABLE" => "N",	// Включить обработку ссылок
				"PAGER_DESC_NUMBERING" => "N",	// Использовать обратную навигацию
				"PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",	// Время кеширования страниц для обратной навигации
				"PAGER_SHOW_ALL" => "N",	// Показывать ссылку "Все"
				"PAGER_SHOW_ALWAYS" => "N",	// Выводить всегда
				"PAGER_TEMPLATE" => ".default",	// Шаблон постраничной навигации
				"PAGER_TITLE" => "",	// Название категорий
				"PARENT_SECTION" => "",	// ID раздела
				"PARENT_SECTION_CODE" => "",	// Код раздела
				"PREVIEW_TRUNCATE_LEN" => "",	// Максимальная длина анонса для вывода (только для типа текст)
				"PROPERTY_CODE" => "",	// Свойства
				"SET_BROWSER_TITLE" => "N",	// Устанавливать заголовок окна браузера
				"SET_LAST_MODIFIED" => "N",	// Устанавливать в заголовках ответа время модификации страницы
				"SET_META_DESCRIPTION" => "N",	// Устанавливать описание страницы
				"SET_META_KEYWORDS" => "N",	// Устанавливать ключевые слова страницы
				"SET_STATUS_404" => "N",	// Устанавливать статус 404
				"SET_TITLE" => "N",	// Устанавливать заголовок страницы
				"SHOW_404" => "N",	// Показ специальной страницы
				"SORT_BY1" => "ACTIVE_FROM",	// Поле для первой сортировки новостей
				"SORT_BY2" => "SORT",	// Поле для второй сортировки новостей
				"SORT_ORDER1" => "DESC",	// Направление для первой сортировки новостей
				"SORT_ORDER2" => "ASC",	// Направление для второй сортировки новостей
			),
			false
		);?>
		
		
		<? echo"<pre>";print_r($arResult);echo"</pre>";
		//echo"<pre>";print_r($arItem);echo"</pre>";
		?>
		<?
		/*
		<div class="brand-news">
			<h2 class="ttl2">Новости по бренду</h2>
			<div class="items c">

				<!-- item -->
				<a href="#" class="item">
					<div class="ttl">Всё о пользе газовых инфракрасных обогревателей Ballu</div>
					<div class="date">21.03.2014</div>
				</a>
				<!-- / item -->
				<!-- item -->
				<a href="#" class="item">
					<div class="ttl">Встречайте увлажнители BALLU Hello Kitty: профессиональную серию  профессиональную серию </div>
					<div class="date">15.03.2014</div>
				</a>
				<!-- / item -->
				<!-- item -->
				<a href="#" class="item">
					<div class="ttl">Водонагреватели Electrolux Formax. Новые возможности управления комфортом!</div>
					<div class="date">02.03.2014</div>
				</a>
				<!-- / item -->

			</div>
			<!-- / items end -->
		</div>
		<!-- / cat-news end -->
		*/
		?>


	</div>
	<!-- / col-r end -->
</div>
<!-- / cat-brands roll end -->
