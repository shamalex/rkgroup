<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();

/* $ID_TO_ROOT = [];
$arSort = array('depth_level' => 'asc');
$arFilter = Array("IBLOCK_ID"=>$arParams['IBLOCK_ID'], '>DEPTH_LEVEL' => '1');
$arSelect = Array("ID", "IBLOCK_SECTION_ID", "DEPTH_LEVEL");
$res = CIBlockSection::GetList($arSort, $arFilter, false, $arSelect);
while($arFields = $res->Fetch()) {
	$ID_TO_ROOT[$arFields['ID']] = $arFields['IBLOCK_SECTION_ID'];
}

$IDS = [];
foreach($arResult['SECTIONS'] as $arSection) {
	$IDS[] = $arSection['ID'];
}

$CNT = [];
$arFilter = Array("IBLOCK_ID"=>$arParams['IBLOCK_ID'], "SECTION_ID"=>$IDS, "INCLUDE_SUBSECTIONS"=>"Y");
if (!empty($arParams['FILTER'])) {
	$arFilter = array_merge($arFilter, $arParams['FILTER']);
}
$arSelect = Array("ID", "SECTION_ID", "IBLOCK_SECTION_ID");
$res = CIBlockElement::GetList(Array(), $arFilter, false, false, $arSelect);
while($arFields = $res->Fetch()) {
	$CNT[$arFields['IBLOCK_SECTION_ID']]++;
}

foreach($CNT as $ID => $CNT_NUM) {
	if (!empty($ID_TO_ROOT[$ID])) {
		$CAT_ID = $ID;
		while($ROOT_ID = $ID_TO_ROOT[$CAT_ID]) {
			$CNT[$ROOT_ID] += $CNT_NUM;
			$CAT_ID = $ID_TO_ROOT[$CAT_ID];
		}
	}
}

foreach($arResult['SECTIONS'] as $i=>$arSection) {
	$arResult['SECTIONS'][$i]['ELEMENT_CNT'] = intval($CNT[$arSection['ID']]);
	$arResult['SECTIONS'][$i]['SECTION_PAGE_URL'] = preg_replace('/^\/catalog/ui', '', $arSection['SECTION_PAGE_URL']);
}
 */

if(!empty($arResult["ITEMS"])) {
	
	$elementsIDS = [];
	
	foreach($arResult["ITEMS"] as $arItem) {
		$elementsIDS[] = $arItem['ID'];
	}


	$CNT = [];
	$arFilter = Array("IBLOCK_ID"=>$arParams['CATALOG_IBLOCK_ID'], "INCLUDE_SUBSECTIONS"=>"Y", "PROPERTY_EL_BRAND"=>$elementsIDS);
	if (!empty($arParams['FILTER'])) {
		$arFilter = array_merge($arFilter, $arParams['FILTER']);
	}
	$arSelect = Array("ID", "PROPERTY_EL_BRAND");
	$res = CIBlockElement::GetList(Array(), $arFilter, false, false, $arSelect);
	while($arFields = $res->Fetch()) {
		$CNT[$arFields['PROPERTY_EL_BRAND_VALUE']]++;
	}
	
	foreach($arResult["ITEMS"] as $i=>$arItem) {
		$arResult["ITEMS"][$i]['ELEMENT_CNT'] = intval($CNT[$arItem['ID']]);
	}
	
}