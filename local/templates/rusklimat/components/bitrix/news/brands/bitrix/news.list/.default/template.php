<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>
<!-- cat-brands -->
<div class="cat-brands roll c">
	<h1 class="ttl1">Торговые марки</h1>

	<div class="brands-all c">
		<?foreach($arResult["ITEMS"] as $arItem):?>
			<?
			if (empty($arItem['ELEMENT_CNT'])) {
				continue;
			}
			$this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_EDIT"));
			$this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_DELETE"), array("CONFIRM" => GetMessage('CT_BNL_ELEMENT_DELETE_CONFIRM')));
			$max_len = 170;
			$preview = strip_tags($arItem["PREVIEW_TEXT"]);
			if (strlen($preview) > $max_len) {
				$preview = cropStr($preview, $max_len);
				$preview .= '...';
			}
			?>
			<!-- item -->
			<a href="<?=$arItem["DETAIL_PAGE_URL"]?>" class="item c-block" id="<?=$this->GetEditAreaId($arItem['ID']);?>">
				<div class="pic"<?if($arParams["DISPLAY_PICTURE"]!="N" && is_array($arItem["PREVIEW_PICTURE"])):?> style="background-image:url('<?=$arItem["PREVIEW_PICTURE"]["SRC"]?>');"<? endif; ?>></div>
				<div class="dsc"><?echo $preview;?></div>
			</a>
			<!-- / item -->
		<?endforeach;?>
	</div>
	<!-- / brands-all end -->

</div>
<!-- / cat-brands roll end -->
