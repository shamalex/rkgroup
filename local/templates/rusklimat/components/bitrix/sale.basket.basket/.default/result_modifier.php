<?

$ID_TO_CATALOG = [];
foreach($arResult["GRID"]["ROWS"] as &$arItem) {
	$ID_TO_CATALOG[$arItem['PRODUCT_ID']] = $arItem['ID'];
	// $db_res = CSaleBasket::GetPropsList(
		// array(
				// "SORT" => "ASC",
				// "NAME" => "ASC"
			// ),
		// array("ID" => $arItem['ID'])
	// );
	// while ($ar_res = $db_res->Fetch()) {
		// $arItem['PROPS'][] = $ar_res;
	// }
}

if (!empty($ID_TO_CATALOG)) {
	$arFilter = Array("IBLOCK_ID"=>8, 'ID'=>array_keys($ID_TO_CATALOG));
	$arSelect = Array("ID", "NAME", "CODE", "XML_ID", "IBLOCK_SECTION_ID", "PROPERTY_FULL_NAME", "PROPERTY_SM_PICTURE1", "PROPERTY_ITEM_NAME", "PROPERTY_CODE", "PROPERTY_NS_CODE", "PROPERTY_EL_BRAND.NAME");
	$res = CIBlockElement::GetList(Array(), $arFilter, false, false, $arSelect);  
	while($arFields = $res->Fetch()) {
		if (!empty($arFields['PROPERTY_SM_PICTURE1_VALUE'])) {
			$arFields['PROPERTY_SM_PICTURE1_VALUE'] = CFile::GetFileArray($arFields['PROPERTY_SM_PICTURE1_VALUE']);
		}
		$arFields['CATALOG_ID'] = $arFields['ID'];
		$ID = $arFields['ID'];
		
		$arSection = array();
		$arSection["ID"] = $arFields['IBLOCK_SECTION_ID'];
		$arSection["PATH"] = array();
		if (!empty($arSection["ID"])) {
			$rsPath = CIBlockSection::GetNavChain($arFilter["IBLOCK_ID"], $arSection["ID"]);
			while($arPath = $rsPath->GetNext()) {
				$arSection["PATH"][] = $arPath;
			}
		}
		$arFields['SECTION'] = $arSection;
		
		$arResult["GRID"]["ROWS"][$ID_TO_CATALOG[$ID]] = array_merge($arFields, $arResult["GRID"]["ROWS"][$ID_TO_CATALOG[$arFields['ID']]]);
	}
}


