<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
use Bitrix\Sale\DiscountCouponsManager;

if (!empty($arResult["ERROR_MESSAGE"]))
	ShowError($arResult["ERROR_MESSAGE"]);

$bDelayColumn  = false;
$bDeleteColumn = false;
$bWeightColumn = false;
$bPropsColumn  = false;
$bPriceType    = false;

if ($normalCount > 0):
?><!-- basket roll -->
<div class="<? if($arParams['HIDE_BUTTONS'] != 'Y'): ?>roll basket c<? endif; ?>" id="basket_items_list">
	<h1 class="ttl1">Корзина</h1>

	<div class="brd tbl-w tbl-basket-w bx_ordercart_order_table_container">
		<!-- tbl-basket -->
		<table class="tbl tbl-hover tbl-lt tbl-basket" id="basket_items">
			<tr>
				<th class="col1">Изображение</th>
				<th class="col2">Наименование товара</th>
				<th class="col3">Цена</th>
				<th class="col4">Количество</th>
				<th class="col5" colspan="2">Итого</th>
			</tr>
			<?
			foreach ($arResult["GRID"]["HEADERS"] as $id => $arHeader) {
				$arHeader["name"] = (isset($arHeader["name"]) ? (string)$arHeader["name"] : '');
				if ($arHeader["name"] == '') {
					$arHeader["name"] = GetMessage("SALE_".$arHeader["id"]);
				}
				$arHeaders[] = $arHeader["id"];
			}
			foreach ($arResult["GRID"]["ROWS"] as $k => $arItem):

				// p($arItem);

				if ($arItem["DELAY"] == "N" && $arItem["CAN_BUY"] == "Y"):
				?>
				<tr class="sp">
					<td colspan="6">&nbsp;</td>
				</tr>
				<tr class="not_sp" id="<?=$arItem["ID"]?>">
					<td class="td-pic">
						<a href="<?=$arItem["DETAIL_PAGE_URL"] ?>" class="pic" <? if(!empty($arItem['PROPERTY_SM_PICTURE1_VALUE']['SRC'])): ?> style="background-image:url('<?=$arItem['PROPERTY_SM_PICTURE1_VALUE']['SRC']?>');" <? endif; ?>></a>
					</td>
					<td class="td-name">
						<?if (strlen($arItem["DETAIL_PAGE_URL"]) > 0):?><a href="<?=$arItem["DETAIL_PAGE_URL"] ?>"><?endif;?>
							<?=$arItem["PROPERTY_ITEM_NAME_VALUE"]?>
						<?if (strlen($arItem["DETAIL_PAGE_URL"]) > 0):?></a><?endif;?>
						<? //p($arItem) ?>
					</td>
					<td class="td-price">
						<div class="current_price" id="current_price_<?=$arItem["ID"]?>">
							<?=$arItem["PRICE_FORMATED"]?>
						</div>
					</td>
					<td class="td-status">
						<div class="amt">
							<?
							$ratio = isset($arItem["MEASURE_RATIO"]) ? $arItem["MEASURE_RATIO"] : 0;
							$max = isset($arItem["AVAILABLE_QUANTITY"]) ? "max=\"".$arItem["AVAILABLE_QUANTITY"]."\"" : "";
							$useFloatQuantity = ($arParams["QUANTITY_FLOAT"] == "Y") ? true : false;
							$useFloatQuantityJS = ($useFloatQuantity ? "true" : "false");
							?>
							<input
								type="text"
								size="3"
								id="QUANTITY_INPUT_<?=$arItem["ID"]?>"
								name="QUANTITY_INPUT_<?=$arItem["ID"]?>"
								size="2"
								maxlength="18"
								min="0"
								<?=$max?>
								step="<?=$ratio?>"
								style="max-width: 50px"
								class="inp inp-lt bsk-amt"
								value="<?=$arItem["QUANTITY"]?>"
								onchange="updateQuantity('QUANTITY_INPUT_<?=$arItem["ID"]?>', '<?=$arItem["ID"]?>', <?=$ratio?>, <?=$useFloatQuantityJS?>)"
							>
							<div class="amt-bt amt-hi">+</div>
							<div class="amt-bt amt-lo">&ndash;</div>
						</div>
						<input type="hidden" id="QUANTITY_<?=$arItem['ID']?>" name="QUANTITY_<?=$arItem['ID']?>" value="<?=$arItem["QUANTITY"]?>" />
					</td>
					<td class="td-price"><div id="sum_<?=$arItem["ID"]?>"><?=$arItem["SUM"]?></div></td>
					<td class="td-x">
						<a href="<?=str_replace("#ID#", $arItem["ID"], $arUrls["delete"])?>" class="x" title="<?=GetMessage("SALE_DELETE")?>" style="display: block;" id="delete_item_<?= $arItem["ID"] ?>"></a>
						<script>
							<?
							$cats = [];
							foreach($arItem['SECTION']['PATH'] as $arSection) {
								$cats[] = $arSection['NAME'];
							}
							?>
							$('#delete_item_<?= $arItem["ID"] ?>').on('click', function() {
								window.dataLayer = window.dataLayer || [];
								window.dataLayer.push({
									'event': 'removeFromCart',
									"ecommerce": {
										"remove": {
											"products": [
												{
													"id": "<?=$arItem["PROPERTY_CODE_VALUE"]?>",
													"name": "<?=$arItem["PROPERTY_ITEM_NAME_VALUE"]?>",
													"price": <?= number_format($arItem['PRICE'], 2, '.', '') ?>,
													"brand": "<?=$arItem["PROPERTY_EL_BRAND_NAME"]?>",
													"category": "<?= implode('/', $cats) ?>",
													"quantity": parseInt($('#QUANTITY_<?=$arItem['ID']?>').val())
												}
											]
										}
									}
								});
							});
						</script>
					</td>
				</tr>
				<?
				endif;
			endforeach;
			?>
		</table>
		<!-- / tbl-basket end -->
		<div class="basket-sum">Сумма: <strong id="bskSum"><span id="allSum_FORMATED"><?=str_replace(" ", "&nbsp;", $arResult["allSum_FORMATED"])?></span></strong></div>
		<div class="basket-prm c">
			<div class="prm-holder">
				<p class="prm-hider"><span>Ввести промокод</span></p>
				<div class="prm-hidden" style="display: none;"  id="coupons_block">
					<input type="text" class="inp inp-t inp-green bsk-prm" name="COUPON" id="coupon" value="" onchange="enterCoupon();DisChangeTopPrice();">
					<?
					if (!empty($arResult['COUPON_LIST']))
					{
						foreach ($arResult['COUPON_LIST'] as $oneCoupon)
						{

							$couponClass = 'disabled';
							switch ($oneCoupon['STATUS'])
							{
								case DiscountCouponsManager::STATUS_NOT_FOUND:
								case DiscountCouponsManager::STATUS_FREEZE:
									$couponClass = 'bad';
									break;
								case DiscountCouponsManager::STATUS_APPLYED:
									$couponClass = 'good';
									break;
							}

							$oneCoupon['COUPON_DEL']=$oneCoupon['COUPON'];
							if(strpos($oneCoupon['COUPON'],"#ccf#")==strlen($oneCoupon['COUPON'])-5){
								$oneCoupon['COUPON']=str_replace("#ccf#","",$oneCoupon['COUPON']);
								$oneCoupon['CHECK_CODE_TEXT']=" не действителен в Вашем городе";
							}
							?><p class="bx_ordercart_coupon <? echo $couponClass; ?>"><input disabled readonly type="hidden" name="OLD_COUPON[]" value="<?=htmlspecialcharsbx($oneCoupon['COUPON']);?>"><span>Промокод "<?=($oneCoupon['DISCOUNT_NAME'])?$oneCoupon['DISCOUNT_NAME']:$oneCoupon['COUPON']?>" <?
							if (isset($oneCoupon['CHECK_CODE_TEXT']))
							{
								echo (is_array($oneCoupon['CHECK_CODE_TEXT']) ? implode(' ', $oneCoupon['CHECK_CODE_TEXT']) : $oneCoupon['CHECK_CODE_TEXT']);
							}
							?></span><span class="action <? echo $couponClass; ?>" data-coupon="<? echo htmlspecialcharsbx($oneCoupon['COUPON_DEL']); ?>"></span></p><?
						}
						unset($couponClass, $oneCoupon);
					}
					?>
					<?/*<p class="hint hint-green">Промокод “Скидка 5%” применен</p>*/?>
				</div>
			</div>
		</div>
	</div>
	<!-- / brd tbl-basket-w end -->

	<? if($arParams['HIDE_BUTTONS'] != 'Y'): ?>
		<div class="bsk-helpers">
			<a class="lnk bsk-callback callback-pop" href="#"><span>Заказать обратный<br>звонок по этой корзине</span></a>
			<a class="lnk bsk-print" href="#print" onclick="window.print(); return false;"><span>Распечатать<br>эту корзину</span></a>
		</div>
		<!-- / bsk-helpers end -->

		<a href="javascript:void(0)" onclick="checkOut();" class="checkout btn btn-order btn-50" id="btnOrder"><?=GetMessage("SALE_ORDER")?></a>
	<? endif; ?>

	<input type="hidden" id="column_headers" value="<?=CUtil::JSEscape(implode($arHeaders, ","))?>" />
	<input type="hidden" id="offers_props" value="<?=CUtil::JSEscape(implode($arParams["OFFERS_PROPS"], ","))?>" />
	<input type="hidden" id="action_var" value="<?=CUtil::JSEscape($arParams["ACTION_VARIABLE"])?>" />
	<input type="hidden" id="quantity_float" value="<?=$arParams["QUANTITY_FLOAT"]?>" />
	<input type="hidden" id="count_discount_4_all_quantity" value="<?=($arParams["COUNT_DISCOUNT_4_ALL_QUANTITY"] == "Y") ? "Y" : "N"?>" />
	<input type="hidden" id="price_vat_show_value" value="<?=($arParams["PRICE_VAT_SHOW_VALUE"] == "Y") ? "Y" : "N"?>" />
	<input type="hidden" id="hide_coupon" value="<?=($arParams["HIDE_COUPON"] == "Y") ? "Y" : "N"?>" />
	<input type="hidden" id="use_prepayment" value="<?=($arParams["USE_PREPAYMENT"] == "Y") ? "Y" : "N"?>" />

	<?
	/*
	<div class="bx_ordercart_order_pay">

		<div class="bx_ordercart_order_pay_right">
			<table class="bx_ordercart_order_sum">
				<?if ($bWeightColumn && floatval($arResult['allWeight']) > 0):?>
					<tr>
						<td class="custom_t1"><?=GetMessage("SALE_TOTAL_WEIGHT")?></td>
						<td class="custom_t2" id="allWeight_FORMATED"><?=$arResult["allWeight_FORMATED"]?>
						</td>
					</tr>
				<?endif;?>
				<?if ($arParams["PRICE_VAT_SHOW_VALUE"] == "Y"):?>
					<tr>
						<td><?echo GetMessage('SALE_VAT_EXCLUDED')?></td>
						<td id="allSum_wVAT_FORMATED"><?=$arResult["allSum_wVAT_FORMATED"]?></td>
					</tr>
					<?if (floatval($arResult["DISCOUNT_PRICE_ALL"]) > 0):?>
						<tr>
							<td class="custom_t1"></td>
							<td class="custom_t2" style="text-decoration:line-through; color:#828282;" id="PRICE_WITHOUT_DISCOUNT">
								<?=$arResult["PRICE_WITHOUT_DISCOUNT"]?>
							</td>
						</tr>
					<?endif;?>
					<?
					if (floatval($arResult['allVATSum']) > 0):
						?>
						<tr>
							<td><?echo GetMessage('SALE_VAT')?></td>
							<td id="allVATSum_FORMATED"><?=$arResult["allVATSum_FORMATED"]?></td>
						</tr>
						<?
					endif;
					?>
				<?endif;?>
					<tr>
						<td class="fwb"><?=GetMessage("SALE_TOTAL")?></td>
						<td class="fwb" id="allSum_FORMATED"><?=str_replace(" ", "&nbsp;", $arResult["allSum_FORMATED"])?></td>
					</tr>


			</table>
			<div style="clear:both;"></div>
		</div>
		<div style="clear:both;"></div>
		<div class="bx_ordercart_order_pay_center">

			<?if ($arParams["USE_PREPAYMENT"] == "Y" && strlen($arResult["PREPAY_BUTTON"]) > 0):?>
				<?=$arResult["PREPAY_BUTTON"]?>
				<span><?=GetMessage("SALE_OR")?></span>
			<?endif;?>

			<a href="javascript:void(0)" onclick="checkOut();" class="checkout"><?=GetMessage("SALE_ORDER")?></a>
		</div>
	</div>
	*/
	?>



</div>
<!-- / basket roll end -->

<? if($arParams['HIDE_BUTTONS'] != 'Y'): ?>
	<script>
		window.dataLayer = window.dataLayer || [];
		window.dataLayer.push({
			'event': 'checkout',
			'ecommerce': {
				'checkout': {
					'actionField': {'step': 1}
				}
			}
		});
	</script>
<? endif; ?>

<?
else:
?>
<?
include($_SERVER["DOCUMENT_ROOT"].$templateFolder."/empty_basket.php");
?>
<?
endif;
?>
