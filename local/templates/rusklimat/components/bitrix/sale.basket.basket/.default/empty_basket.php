<?
if (!empty($arParams['EMPTY_MOVE_TO'])) {
	if($_REQUEST['is_ajax_post'] != 'Y') {
		LocalRedirect($arParams['EMPTY_MOVE_TO']);
	} else {
		$APPLICATION->RestartBuffer();
		?>
		<script>
			window.top.location = '<?= $arParams['EMPTY_MOVE_TO'] ?>';
		</script>
		<?
		die();
	}
}
?>

<!-- roll -->
<div class="roll basket txt-block">
	<h1 class="ttl1">Корзина</h1>
	<a href="<?= $_SERVER['REQUEST_URI'] ?>" id="linkForUpdateBasket"></a>

	<div class="empty c">
		<div class="ico basket-empty"></div>
		<div class="dsc">
			<h2 class="ttl">В вашей корзине пока еще пусто!</h2>
			<p>Чтобы найти нужный товар, воспользуйтесь <a href="/catalog/"><strong>каталогом</strong></a> нашей продукции. <br />И не забудьте просмотреть действующие <a href="/special/"><strong>акции</strong></a>, которые помогут приобрести товар с максимальной выгодой.</p>
			<p>Удачных покупок!</p>
		</div>
	</div>
	<!-- / empty end -->
</div>
<!-- / roll end -->

<style>
	#orderBlock {
		display: none;
	}
</style>
