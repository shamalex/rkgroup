<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
//p($arParams);
$templateData = array(
	'TEMPLATE_THEME' => $this->GetFolder().'/themes/'.$arParams['TEMPLATE_THEME'].'/colors.css',
	'TEMPLATE_CLASS' => 'bx-'.$arParams['TEMPLATE_THEME']
);

// if (isset($templateData['TEMPLATE_THEME']))
// {
	// $this->addExternalCss($templateData['TEMPLATE_THEME']);
// }
// $this->addExternalCss("/bitrix/css/main/bootstrap.css");
// $this->addExternalCss("/bitrix/css/main/font-awesome.css");
//p($arResult['PROPS_GROUPS']);
//p($arResult['ITEMS']);
?>
<style type="text/css">
	.count0{
		display: flex;
	}
</style>
<? if(!empty($arResult["ITEMS"])): ?>
	<?
	$hasFilterProps = false;
	foreach($arResult["ITEMS"] as $key=>$arItem) {
		if(isset($arItem["PRICE"])) {
			if ($arItem["VALUES"]["MAX"]["VALUE"] - $arItem["VALUES"]["MIN"]["VALUE"] > 0) {
				$hasFilterProps = true;
				break;
			}
		} else {
			if (!empty($arItem["VALUES"])) {
				$hasFilterProps = true;
				break;
			}
		}
	}
	?>
	<? if($hasFilterProps): ?>
		<div class="filter c bx-filter <?=$templateData["TEMPLATE_CLASS"]?>">
			<!-- filter .w -->
			<div class="w">

				<form name="<?echo $arResult["FILTER_NAME"]."_form"?>" action="<?echo $arResult["FORM_ACTION"]?>" method="get" class="smartfilter">
					<!-- filter btns -->
					<div class="btns c">
						<input
							class="btn btn-30 btn-g btn-f-clear"
							type="submit"
							id="del_filter1"
							name="del_filter"
							value="<?=GetMessage("CT_BCSF_DEL_FILTER")?>"
						/>
						<?
						$GLOBALS['SET_FILTER_LABEL'] = GetMessage("CT_BCSF_SET_FILTER");
						?>
					</div><br>

					<?foreach($arResult["HIDDEN"] as $arItem):?>
						<input type="hidden" name="<?echo $arItem["CONTROL_NAME"]?>" id="<?echo $arItem["CONTROL_ID"]?>" value="<?echo $arItem["HTML_VALUE"]?>" />
					<?endforeach;?>

					<?
					foreach($arResult["ITEMS"] as $key=>$arItem)//prices
						{
							/**/
							if($arItem["CODE"] == 'GROUP'&&count($arItem["VALUES"])>1){
								//p($arItem);
								?>
								<div class="item <?if ($arItem["DISPLAY_EXPANDED"]== "Y"):?>open<?endif?>">
							<!-- ttl -->
							<div class="ttl c">
							<span><?=$arItem['NAME']?></span>
									<?
								/*	$GLOBALS['FILTER_BRANDS'] = $arItem["VALUES"];*/
									?>
									<div class="ico que-sm que" style="display: none;"></div>

							</div>
							<!-- / ttl -->

							<!-- content -->
							<fieldset class="checkset">
							<?
								$arCur = current($arItem["VALUES"]);

										?>
										<div class="cont">
										<?$i=0;?>
										<?foreach($arItem["VALUES"] as $val => $ar):?>
											<?if($i==6){?>

											<div class="hideme" <?if ($arItem["DISPLAY_EXPANDED"] != "Y"):?>style="display: none;"<?endif?>>
											<?}?>


											<div class="checkline count<?=$ar["ELEMENT_COUNT"]?>" title="<?=$ar["VALUE"];?>">
												<input
													type="checkbox"
													value="<? echo $ar["HTML_VALUE"] ?>"
													name="<? echo $ar["CONTROL_NAME"] ?>"
													id="<? echo $ar["CONTROL_ID"] ?>"
													<? echo $ar["CHECKED"]? 'checked="checked"': '' ?>
													onclick="smartFilter.click(this)"
												/><label data-role="label_<?=$ar["CONTROL_ID"]?>" class="bx-filter-param-label <? echo $ar["DISABLED"] ? 'disabled': '' ?>" for="<? echo $ar["CONTROL_ID"] ?>"><?=$arResult['SECTIONS'][$ar["VALUE"]]['NAME'];?><?

													?> (<span data-role="count_<?=$ar["CONTROL_ID"]?>"><? echo $ar["ELEMENT_COUNT"]; ?></span>)<?
												?></label>
											</div>
											<?$i++;?>
										<?endforeach;?>
										<?if($i>6){?>
										</div><div class="hider plus">Показать все</div><?}?>
										<?/*foreach ($arItem['VALUES'] as $key => $ar) {
											?>

											<div>
												<a href="<?=$arResult['SECTIONS'][$ar['VALUE']]['DETAIL_PAGE_URL']?>"><?=$arResult['SECTIONS'][$ar['VALUE']]['NAME']?></a>
											</div>
											<?
										}*/?>
										</div>

							</fieldset>
							</div>
								<?
							}
						}
					foreach($arResult["ITEMS"] as $key=>$arItem)//prices
						{
							$key = $arItem["ENCODED_ID"];
							if(isset($arItem["PRICE"])):
								//p($arItem);
								if (!isset($arItem["VALUES"]["MIN"]["VALUE"]) || !isset($arItem["VALUES"]["MAX"]["VALUE"]) || ($arItem["VALUES"]["MAX"]["VALUE"] - $arItem["VALUES"]["MIN"]["VALUE"] <= 0))
									continue;
								if ($arItem["CODE"] != $arParams['CUR_MAIN_CITY_CODE'])
									continue;
								//p($arItem);
								$precision = 2;
								if (Bitrix\Main\Loader::includeModule("currency"))
								{
									$res = CCurrencyLang::GetFormatDescription($arItem["VALUES"]["MIN"]["CURRENCY"]);
									$precision = $res['DECIMALS'];
								}
								$cur_min = ($arItem["VALUES"]["MIN"]["HTML_VALUE"]);
								if (empty($cur_min)) {
									$cur_min = ($arItem["VALUES"]["MIN"]["VALUE"]);
									$arItem["VALUES"]["MIN"]["HTML_VALUE"] = $cur_min;
								}
								$cur_max = ($arItem["VALUES"]["MAX"]["HTML_VALUE"]);
								if (empty($cur_max)) {
									$cur_max = ($arItem["VALUES"]["MAX"]["VALUE"]);
									$arItem["VALUES"]["MAX"]["HTML_VALUE"] = $cur_max;
								}
								?>
								<!-- item -->
								<div class="item open">
									<!-- ttl -->
									<div class="ttl">
										<span>цена, руб.</span>
										<div class="ico que-sm que" style="display: none;"></div>

									</div>
									<!-- / ttl -->
									<!-- content -->
									<div class="cont hideme">
										<!-- range -->
										<div class="range">
											<fieldset class="inputset blocke c">
												<div class="inputline inp-ot">
													<label for="inputLow">от</label>
													<input
														class="min-price inp inp-100 inp-drk"
														type="text"
														name="<?echo $arItem["VALUES"]["MIN"]["CONTROL_NAME"]?>"
														placeholder="<?echo $arItem["VALUES"]["MIN"]["HTML_VALUE"]?>"
														id="inputLow"
														size="5"

													/>
												</div>
												<div class="inputline inp-do">
													<label for="inputLow">до</label>
													<input
														class="max-price inp inp-100 inp-drk"
														type="text"
														name="<?echo $arItem["VALUES"]["MAX"]["CONTROL_NAME"]?>"
														placeholder="<?echo $arItem["VALUES"]["MAX"]["HTML_VALUE"]?>"
														id="inputHigh"
														size="5"

													/>
												</div>
											</fieldset>
											<!-- range bar -->
											<div class="bar c">
												<?
												$cur_min = ($arItem["VALUES"]["MIN"]["HTML_VALUE"]);
												if (empty($cur_min)) {
													$cur_min = ($arItem["VALUES"]["MIN"]["VALUE"]);
												}
												$cur_max = ($arItem["VALUES"]["MAX"]["HTML_VALUE"]);
												if (empty($cur_max)) {
													$cur_max = ($arItem["VALUES"]["MAX"]["VALUE"]);
												}
												?>
												<input type="hidden" class="slider-input-price" value="<?= ($arItem["VALUES"]["MIN"]["VALUE"]).','.($arItem["VALUES"]["MAX"]["VALUE"]) ?>" -data-min="<?= ($arItem["VALUES"]["MIN"]["VALUE"]) ?>" -data-max="<?= ($arItem["VALUES"]["MAX"]["VALUE"]) ?>" -data-set-min="<?= $cur_min ?>" -data-set-max="<?= $cur_max ?>" />
											</div>
											<!-- / range bar end-->
										</div>
										<!-- / range end -->
									</div>
									<!-- / content -->
								</div>
								<!-- / item -->
								<?
								$arJsParams = array(
									"leftSlider" => 'left_slider_'.$key,
									"rightSlider" => 'right_slider_'.$key,
									"tracker" => "drag_tracker_".$key,
									"trackerWrap" => "drag_track_".$key,
									"minInputId" => $arItem["VALUES"]["MIN"]["CONTROL_ID"],
									"maxInputId" => $arItem["VALUES"]["MAX"]["CONTROL_ID"],
									"minPrice" => $arItem["VALUES"]["MIN"]["VALUE"],
									"maxPrice" => $arItem["VALUES"]["MAX"]["VALUE"],
									"curMinPrice" => $arItem["VALUES"]["MIN"]["HTML_VALUE"],
									"curMaxPrice" => $arItem["VALUES"]["MAX"]["HTML_VALUE"],
									"fltMinPrice" => ($arItem["VALUES"]["MIN"]["FILTERED_VALUE"]) ? $arItem["VALUES"]["MIN"]["FILTERED_VALUE"] : $arItem["VALUES"]["MIN"]["VALUE"] ,
									"fltMaxPrice" => ($arItem["VALUES"]["MAX"]["FILTERED_VALUE"]) ? $arItem["VALUES"]["MAX"]["FILTERED_VALUE"] : $arItem["VALUES"]["MAX"]["VALUE"],
									"precision" => $precision,
									"colorUnavailableActive" => 'colorUnavailableActive_'.$key,
									"colorAvailableActive" => 'colorAvailableActive_'.$key,
									"colorAvailableInactive" => 'colorAvailableInactive_'.$key,
								);
								?>
								<script type="text/javascript">
									BX.ready(function(){
										window['trackBar<?=$key?>'] = new BX.Iblock.SmartFilter(<?=CUtil::PhpToJSObject($arJsParams)?>);
									});
								</script>
							<?endif;

						}
					?>

					<?
					foreach($arResult["ITEMS"] as $key=>$arItem)//prices
						{
							/**/
							if($arItem["CODE"] == 'filter_00060'&&count($arItem["VALUES"])>0){
							//p($arItem);
								?>
								<div class="item <?if ($arItem["DISPLAY_EXPANDED"]== "Y"):?>open<?endif?>">
							<!-- ttl -->
							<div class="ttl c">
							<span>Потребительский класс</span>
									<?
								/*	$GLOBALS['FILTER_BRANDS'] = $arItem["VALUES"];*/
									?>
									<div class="ico que-sm que" style="display: none;"></div>

							</div>
							<!-- / ttl -->

							<!-- content -->
							<fieldset class="checkset">
							<?
								$arCur = current($arItem["VALUES"]);

										?>
										<div class="cont">
										<?$i=0;?>
										<?foreach($arItem["VALUES"] as $val => $ar):?>
											<?if($i==6){?>

											<div class="hideme" <?if ($arItem["DISPLAY_EXPANDED"] != "Y"):?>style="display: none;"<?endif?>>
											<?}?>


											<div class="checkline count<?=$ar["ELEMENT_COUNT"]?>" title="<?=$ar["VALUE"];?>">
												<input
													type="checkbox"
													value="<? echo $ar["HTML_VALUE"] ?>"
													name="<? echo $ar["CONTROL_NAME"] ?>"
													id="<? echo $ar["CONTROL_ID"] ?>"
													<? echo $ar["CHECKED"]? 'checked="checked"': '' ?>
													onclick="smartFilter.click(this)"
												/><label data-role="label_<?=$ar["CONTROL_ID"]?>" class="bx-filter-param-label <? echo $ar["DISABLED"] ? 'disabled': '' ?>" for="<? echo $ar["CONTROL_ID"] ?>"><?=$ar["VALUE"];?><?

													?> (<span data-role="count_<?=$ar["CONTROL_ID"]?>"><? echo $ar["ELEMENT_COUNT"]; ?></span>)<?
												?></label>
											</div>
											<?$i++;?>
										<?endforeach;?>
										<?if($i>6){?>
										</div><div class="hider plus">Показать все</div><?}?>
										</div>

							</fieldset>
							</div>
								<?
							}
						}
						foreach($arResult["ITEMS"] as $key=>$arItem)//prices
						{
							if($arItem["CODE"] == 'EL_BRAND'){
								//p($arItem);
								?>
								<div class="item <?if ($arItem["DISPLAY_EXPANDED"]== "Y"):?>open<?endif?>">
							<!-- ttl -->
							<div class="ttl c">
							<span>Производители</span>
									<?
									$GLOBALS['FILTER_BRANDS'] = $arItem["VALUES"];
									?>
									<div class="ico que-sm que" style="display: none;"></div>

							</div>
							<!-- / ttl -->

							<!-- content -->
							<fieldset class="checkset">
							<?
								$arCur = current($arItem["VALUES"]);

										?>
										<div class="cont">
										<?$i=0;?>
										<?foreach($arItem["VALUES"] as $val => $ar):?>
											<?if($i==6){?>

											<div class="hideme" <?if ($arItem["DISPLAY_EXPANDED"] != "Y"):?>style="display: none;"<?endif?>>
											<?}?>


											<div class="checkline count<?=$ar["ELEMENT_COUNT"]?>" title="<?=$ar["VALUE"];?>">
												<input
													type="checkbox"
													value="<? echo $ar["HTML_VALUE"] ?>"
													name="<? echo $ar["CONTROL_NAME"] ?>"
													id="<? echo $ar["CONTROL_ID"] ?>"
													<? echo $ar["CHECKED"]? 'checked="checked"': '' ?>
													onclick="smartFilter.click(this)"
												/><label data-role="label_<?=$ar["CONTROL_ID"]?>" class="bx-filter-param-label <? echo $ar["DISABLED"] ? 'disabled': '' ?>" for="<? echo $ar["CONTROL_ID"] ?>"><?=$ar["VALUE"];?><?

													?> (<span data-role="count_<?=$ar["CONTROL_ID"]?>"><? echo $ar["ELEMENT_COUNT"]; ?></span>)<?
												?></label>
											</div>
											<?$i++?>
										<?endforeach;?>
										<?if($i>6){?>
										</div><div class="hider plus">Показать все</div><?}?>
										</div>

							</fieldset>
							</div>
								<?
							}
						}
					//not prices
foreach ($arResult['PROPS_GROUPS'] as $k => $v) {

					//echo "<h3>".$v['NAME']."</h1>";
					foreach($v['FILTER_VAL'] as $key=>$arItem)
					{
						if(
							empty($arItem["VALUES"])
							|| isset($arItem["PRICE"])
						)
							continue;

						if (
							$arItem["DISPLAY_TYPE"] == "A"
							&& (
								$arItem["VALUES"]["MAX"]["VALUE"] - $arItem["VALUES"]["MIN"]["VALUE"] <= 0
							)
						) {
							continue;
						}
						//p($arItem);
						// break;
						?>
						<div class="item <?if ($arItem["DISPLAY_EXPANDED"]== "Y"):?>open<?endif?>">
							<!-- ttl -->
							<div class="ttl c">
								<? if($arItem["CODE"] == 'EL_BRAND'): ?>
									<span>Производители</span>
									<?
									$GLOBALS['FILTER_BRANDS'] = $arItem["VALUES"];
									?>
								<? else: ?>
									<span><?=$arItem["NAME"]?> <?=($arItem['HINT']!='')?'('.$arItem['HINT'].')':'';?></span>
								<? endif; ?>
								<div class="ico que-sm que" style="display: none;"></div>
								<?/*<div class="hider <?if ($arItem["DISPLAY_EXPANDED"]== "Y"):?>minus<?else:?>plus<?endif?>"></div>*/?>
							</div>
							<!-- / ttl -->

							<!-- content -->
							<fieldset class="checkset">
								<?
								$arCur = current($arItem["VALUES"]);
								switch ($arItem["DISPLAY_TYPE"])
								{
									case "A"://NUMBERS_WITH_SLIDER
									//p($arItem);
									//$key = $arItem["ENCODED_ID"];
									$arItem["VALUES"]["MIN"]["VALUE"]=number_format($arItem["VALUES"]["MIN"]["VALUE"],2,'.','');
									if($arItem["VALUES"]["MIN"]["HTML_VALUE"]!=''){
										$arItem["VALUES"]["MIN"]["HTML_VALUE"]=number_format($arItem["VALUES"]["MIN"]["HTML_VALUE"],2,'.','');
									}

									$arItem["VALUES"]["MAX"]["VALUE"]=number_format($arItem["VALUES"]["MAX"]["VALUE"],2,'.','');
									if($arItem["VALUES"]["MAX"]["HTML_VALUE"]!=''){
										$arItem["VALUES"]["MAX"]["HTML_VALUE"]=number_format($arItem["VALUES"]["MAX"]["HTML_VALUE"],2,'.','');
									}
									$cur_max=number_format($cur_max,2,'.','');
										$cur_min = ($arItem["VALUES"]["MIN"]["HTML_VALUE"]);
									if (empty($cur_min)) {
										$cur_min = ($arItem["VALUES"]["MIN"]["VALUE"]);
										$arItem["VALUES"]["MIN"]["HTML_VALUE"] = $cur_min;
									}
									$cur_max = ($arItem["VALUES"]["MAX"]["HTML_VALUE"]);
									if (empty($cur_max)) {
										$cur_max = ($arItem["VALUES"]["MAX"]["VALUE"]);
										$arItem["VALUES"]["MAX"]["HTML_VALUE"] = $cur_max;
									}

									/*p($cur_max);
									p($cur_min);*/
									?>
										<div class="cont hideme">
										<!-- range -->
										<div class="range">
											<fieldset class="inputset blocke c">
												<div class="inputline inp-ot">
													<label for="inputLow">от</label>
													<input
														class="min-price inp inp-100 inp-drk"
														type="text"
														name="<?echo $arItem["VALUES"]["MIN"]["CONTROL_NAME"]?>"
														id="inputLow"
														placeholder="<?echo $cur_min?>"
														size="5"

													/>
												</div>
												<div class="inputline inp-do">
													<label for="inputLow">до</label>
													<input
														class="max-price inp inp-100 inp-drk"
														type="text"
														name="<?echo $arItem["VALUES"]["MAX"]["CONTROL_NAME"]?>"
														id="inputHigh"
														placeholder="<?echo $cur_max?>"
														size="5"

													/>
												</div>
											</fieldset>
											<!-- range bar -->
											<div class="bar c">
												<?
												$cur_min = ($arItem["VALUES"]["MIN"]["HTML_VALUE"]);
												if (empty($cur_min)) {
													$cur_min = ($arItem["VALUES"]["MIN"]["VALUE"]);
												}
												$cur_max = ($arItem["VALUES"]["MAX"]["HTML_VALUE"]);
												if (empty($cur_max)) {
													$cur_max = ($arItem["VALUES"]["MAX"]["VALUE"]);
												}
												?>
												<input type="hidden" class="slider-input" value="<?=$arItem["VALUES"]["MIN"]["VALUE"].','.$arItem["VALUES"]["MAX"]["VALUE"]?>" -data-min="<?=$arItem["VALUES"]["MIN"]["VALUE"] ?>" -data-max="<?= $arItem["VALUES"]["MAX"]["VALUE"] ?>" -data-set-min="<?=$cur_min ?>" -data-set-max="<?=$cur_max?>" />
											</div>
											<!-- / range bar end-->
										</div>
										<!-- / range end -->
									</div>
										<?
										$arJsParams = array(
											"leftSlider" => 'left_slider_'.$key,
											"rightSlider" => 'right_slider_'.$key,
											"tracker" => "drag_tracker_".$key,
											"trackerWrap" => "drag_track_".$key,
											"minInputId" => $arItem["VALUES"]["MIN"]["CONTROL_ID"],
											"maxInputId" => $arItem["VALUES"]["MAX"]["CONTROL_ID"],
											"minPrice" => $arItem["VALUES"]["MIN"]["VALUE"],
											"maxPrice" => $arItem["VALUES"]["MAX"]["VALUE"],
											"curMinPrice" => $arItem["VALUES"]["MIN"]["HTML_VALUE"],
											"curMaxPrice" => $arItem["VALUES"]["MAX"]["HTML_VALUE"],
											"fltMinPrice" => intval($arItem["VALUES"]["MIN"]["FILTERED_VALUE"]) ? $arItem["VALUES"]["MIN"]["FILTERED_VALUE"] : $arItem["VALUES"]["MIN"]["VALUE"] ,
											"fltMaxPrice" => intval($arItem["VALUES"]["MAX"]["FILTERED_VALUE"]) ? $arItem["VALUES"]["MAX"]["FILTERED_VALUE"] : $arItem["VALUES"]["MAX"]["VALUE"],
											"precision" => $arItem["DECIMALS"]? $arItem["DECIMALS"]: 0,
											"colorUnavailableActive" => 'colorUnavailableActive_'.$key,
											"colorAvailableActive" => 'colorAvailableActive_'.$key,
											"colorAvailableInactive" => 'colorAvailableInactive_'.$key,
										);
										?>
										<script type="text/javascript">
											BX.ready(function(){
												window['trackBar<?=$key?>'] = new BX.Iblock.SmartFilter(<?=CUtil::PhpToJSObject($arJsParams)?>);
											});
										</script>
										<?
										break;
									case "B"://NUMBERS
										?>
										<div class="col-xs-6 bx-filter-parameters-box-container-block bx-left">
											<i class="bx-ft-sub"><?=GetMessage("CT_BCSF_FILTER_FROM")?></i>
											<div class="bx-filter-input-container">
												<input
													class="min-price"
													type="text"
													name="<?echo $arItem["VALUES"]["MIN"]["CONTROL_NAME"]?>"
													id="<?echo $arItem["VALUES"]["MIN"]["CONTROL_ID"]?>"
													value="<?echo $arItem["VALUES"]["MIN"]["HTML_VALUE"]?>"
													size="5"
													onkeyup="smartFilter.keyup(this)"
													/>
											</div>
										</div>
										<div class="col-xs-6 bx-filter-parameters-box-container-block bx-right">
											<i class="bx-ft-sub"><?=GetMessage("CT_BCSF_FILTER_TO")?></i>
											<div class="bx-filter-input-container">
												<input
													class="max-price"
													type="text"
													name="<?echo $arItem["VALUES"]["MAX"]["CONTROL_NAME"]?>"
													id="<?echo $arItem["VALUES"]["MAX"]["CONTROL_ID"]?>"
													value="<?echo $arItem["VALUES"]["MAX"]["HTML_VALUE"]?>"
													size="5"
													onkeyup="smartFilter.keyup(this)"
													/>
											</div>
										</div>
										<?
										break;
									case "G"://CHECKBOXES_WITH_PICTURES
										?>
										<div class="bx-filter-param-btn-inline">
										<?foreach ($arItem["VALUES"] as $val => $ar):?>
											<input
												style="display: none"
												type="checkbox"
												name="<?=$ar["CONTROL_NAME"]?>"
												id="<?=$ar["CONTROL_ID"]?>"
												value="<?=$ar["HTML_VALUE"]?>"
												<? echo $ar["CHECKED"]? 'checked="checked"': '' ?>
											/>
											<?
											$class = "";
											if ($ar["CHECKED"])
												$class.= " bx-active";
											if ($ar["DISABLED"])
												$class.= " disabled";
											?>
											<label for="<?=$ar["CONTROL_ID"]?>" data-role="label_<?=$ar["CONTROL_ID"]?>" class="bx-filter-param-label <?=$class?>" onclick="smartFilter.keyup(BX('<?=CUtil::JSEscape($ar["CONTROL_ID"])?>')); BX.toggleClass(this, 'bx-active');">
												<span class="bx-filter-param-btn bx-color-sl">
													<?if (isset($ar["FILE"]) && !empty($ar["FILE"]["SRC"])):?>
													<span class="bx-filter-btn-color-icon" style="background-image:url('<?=$ar["FILE"]["SRC"]?>');"></span>
													<?endif?>
												</span>
											</label>
										<?endforeach?>
										</div>
										<?
										break;
									case "H"://CHECKBOXES_WITH_PICTURES_AND_LABELS
										?>
										<div class="bx-filter-param-btn-block">
										<?foreach ($arItem["VALUES"] as $val => $ar):?>
											<input
												style="display: none"
												type="checkbox"
												name="<?=$ar["CONTROL_NAME"]?>"
												id="<?=$ar["CONTROL_ID"]?>"
												value="<?=$ar["HTML_VALUE"]?>"
												<? echo $ar["CHECKED"]? 'checked="checked"': '' ?>
											/>
											<?
											$class = "";
											if ($ar["CHECKED"])
												$class.= " bx-active";
											if ($ar["DISABLED"])
												$class.= " disabled";
											?>
											<label for="<?=$ar["CONTROL_ID"]?>" data-role="label_<?=$ar["CONTROL_ID"]?>" class="bx-filter-param-label<?=$class?> count<?=$ar["ELEMENT_COUNT"]?>" onclick="smartFilter.keyup(BX('<?=CUtil::JSEscape($ar["CONTROL_ID"])?>')); BX.toggleClass(this, 'bx-active');">
												<span class="bx-filter-param-btn bx-color-sl">
													<?if (isset($ar["FILE"]) && !empty($ar["FILE"]["SRC"])):?>
														<span class="bx-filter-btn-color-icon" style="background-image:url('<?=$ar["FILE"]["SRC"]?>');"></span>
													<?endif?>
												</span>
												<span class="bx-filter-param-text" title="<?=$ar["VALUE"];?>"><?=$ar["VALUE"];?><?
												if ($arParams["DISPLAY_ELEMENT_COUNT"] !== "N" && isset($ar["ELEMENT_COUNT"])):
													?> (<span data-role="count_<?=$ar["CONTROL_ID"]?>"><? echo $ar["ELEMENT_COUNT"]; ?></span>)<?
												endif;?></span>
											</label>
										<?endforeach?>
										</div>
										<?
										break;
									case "P"://DROPDOWN
										$checkedItemExist = false;
										?>
										<div class="bx-filter-select-container">
											<div class="bx-filter-select-block" onclick="smartFilter.showDropDownPopup(this, '<?=CUtil::JSEscape($key)?>')">
												<div class="bx-filter-select-text" data-role="currentOption">
													<?
													foreach ($arItem["VALUES"] as $val => $ar)
													{
														if ($ar["CHECKED"])
														{
															echo $ar["VALUE"];
															$checkedItemExist = true;
														}
													}
													if (!$checkedItemExist)
													{
														echo GetMessage("CT_BCSF_FILTER_ALL");
													}
													?>
												</div>
												<div class="bx-filter-select-arrow"></div>
												<input
													style="display: none"
													type="radio"
													name="<?=$arCur["CONTROL_NAME_ALT"]?>"
													id="<? echo "all_".$arCur["CONTROL_ID"] ?>"
													value=""
												/>
												<?foreach ($arItem["VALUES"] as $val => $ar):?>
													<input
														style="display: none"
														type="radio"
														name="<?=$ar["CONTROL_NAME_ALT"]?>"
														id="<?=$ar["CONTROL_ID"]?>"
														value="<? echo $ar["HTML_VALUE_ALT"] ?>"
														<? echo $ar["CHECKED"]? 'checked="checked"': '' ?>
													/>
												<?endforeach?>
												<div class="bx-filter-select-popup" data-role="dropdownContent" style="display: none;">
													<ul>
														<li>
															<label for="<?="all_".$arCur["CONTROL_ID"]?>" class="bx-filter-param-label" data-role="label_<?="all_".$arCur["CONTROL_ID"]?>" onclick="smartFilter.selectDropDownItem(this, '<?=CUtil::JSEscape("all_".$arCur["CONTROL_ID"])?>')">
																<? echo GetMessage("CT_BCSF_FILTER_ALL"); ?>
															</label>
														</li>
													<?
													foreach ($arItem["VALUES"] as $val => $ar):
														$class = "";
														if ($ar["CHECKED"])
															$class.= " selected";
														if ($ar["DISABLED"])
															$class.= " disabled";
													?>
														<li>
															<label for="<?=$ar["CONTROL_ID"]?>" class="bx-filter-param-label<?=$class?>" data-role="label_<?=$ar["CONTROL_ID"]?>" onclick="smartFilter.selectDropDownItem(this, '<?=CUtil::JSEscape($ar["CONTROL_ID"])?>')"><?=$ar["VALUE"]?></label>
														</li>
													<?endforeach?>
													</ul>
												</div>
											</div>
										</div>
										<?
										break;
									case "R"://DROPDOWN_WITH_PICTURES_AND_LABELS
										?>
										<div class="bx-filter-select-container">
											<div class="bx-filter-select-block" onclick="smartFilter.showDropDownPopup(this, '<?=CUtil::JSEscape($key)?>')">
												<div class="bx-filter-select-text fix" data-role="currentOption">
													<?
													$checkedItemExist = false;
													foreach ($arItem["VALUES"] as $val => $ar):
														if ($ar["CHECKED"])
														{
														?>
															<?if (isset($ar["FILE"]) && !empty($ar["FILE"]["SRC"])):?>
																<span class="bx-filter-btn-color-icon" style="background-image:url('<?=$ar["FILE"]["SRC"]?>');"></span>
															<?endif?>
															<span class="bx-filter-param-text">
																<?=$ar["VALUE"]?>
															</span>
														<?
															$checkedItemExist = true;
														}
													endforeach;
													if (!$checkedItemExist)
													{
														?><span class="bx-filter-btn-color-icon all"></span> <?
														echo GetMessage("CT_BCSF_FILTER_ALL");
													}
													?>
												</div>
												<div class="bx-filter-select-arrow"></div>
												<input
													style="display: none"
													type="radio"
													name="<?=$arCur["CONTROL_NAME_ALT"]?>"
													id="<? echo "all_".$arCur["CONTROL_ID"] ?>"
													value=""
												/>
												<?foreach ($arItem["VALUES"] as $val => $ar):?>
													<input
														style="display: none"
														type="radio"
														name="<?=$ar["CONTROL_NAME_ALT"]?>"
														id="<?=$ar["CONTROL_ID"]?>"
														value="<?=$ar["HTML_VALUE_ALT"]?>"
														<? echo $ar["CHECKED"]? 'checked="checked"': '' ?>
													/>
												<?endforeach?>
												<div class="bx-filter-select-popup" data-role="dropdownContent" style="display: none">
													<ul>
														<li style="border-bottom: 1px solid #e5e5e5;padding-bottom: 5px;margin-bottom: 5px;">
															<label for="<?="all_".$arCur["CONTROL_ID"]?>" class="bx-filter-param-label" data-role="label_<?="all_".$arCur["CONTROL_ID"]?>" onclick="smartFilter.selectDropDownItem(this, '<?=CUtil::JSEscape("all_".$arCur["CONTROL_ID"])?>')">
																<span class="bx-filter-btn-color-icon all"></span>
																<? echo GetMessage("CT_BCSF_FILTER_ALL"); ?>
															</label>
														</li>
													<?
													foreach ($arItem["VALUES"] as $val => $ar):
														$class = "";
														if ($ar["CHECKED"])
															$class.= " selected";
														if ($ar["DISABLED"])
															$class.= " disabled";
													?>
														<li>
															<label for="<?=$ar["CONTROL_ID"]?>" data-role="label_<?=$ar["CONTROL_ID"]?>" class="bx-filter-param-label<?=$class?>" onclick="smartFilter.selectDropDownItem(this, '<?=CUtil::JSEscape($ar["CONTROL_ID"])?>')">
																<?if (isset($ar["FILE"]) && !empty($ar["FILE"]["SRC"])):?>
																	<span class="bx-filter-btn-color-icon" style="background-image:url('<?=$ar["FILE"]["SRC"]?>');"></span>
																<?endif?>
																<span class="bx-filter-param-text">
																	<?=$ar["VALUE"]?>
																</span>
															</label>
														</li>
													<?endforeach?>
													</ul>
												</div>
											</div>
										</div>
										<?
										break;
									case "K"://RADIO_BUTTONS
										?>
										<div class="radio">
											<label class="bx-filter-param-label" for="<? echo "all_".$arCur["CONTROL_ID"] ?>">
												<span class="bx-filter-input-checkbox">
													<input
														type="radio"
														value=""
														name="<? echo $arCur["CONTROL_NAME_ALT"] ?>"
														id="<? echo "all_".$arCur["CONTROL_ID"] ?>"
														onclick="smartFilter.click(this)"
													/>
													<span class="bx-filter-param-text"><? echo GetMessage("CT_BCSF_FILTER_ALL"); ?></span>
												</span>
											</label>
										</div>
										<?foreach($arItem["VALUES"] as $val => $ar):?>
											<div class="radio count<?=$ar["ELEMENT_COUNT"]?>">
												<label data-role="label_<?=$ar["CONTROL_ID"]?>" class="bx-filter-param-label" for="<? echo $ar["CONTROL_ID"] ?>">
													<span class="bx-filter-input-checkbox <? echo $ar["DISABLED"] ? 'disabled': '' ?>">
														<input
															type="radio"
															value="<? echo $ar["HTML_VALUE_ALT"] ?>"
															name="<? echo $ar["CONTROL_NAME_ALT"] ?>"
															id="<? echo $ar["CONTROL_ID"] ?>"
															<? echo $ar["CHECKED"]? 'checked="checked"': '' ?>
															onclick="smartFilter.click(this)"
														/>
														<span class="bx-filter-param-text" title="<?=$ar["VALUE"];?>"><?=$ar["VALUE"];?><?
														if ($arParams["DISPLAY_ELEMENT_COUNT"] !== "N" && isset($ar["ELEMENT_COUNT"])):
															?> (<span data-role="count_<?=$ar["CONTROL_ID"]?>"><? echo $ar["ELEMENT_COUNT"]; ?></span>)<?
														endif;?></span>
													</span>
												</label>
											</div>
										<?endforeach;?>
										<?
										break;
									case "U"://CALENDAR
										?>
										<div class="bx-filter-parameters-box-container-block"><div class="bx-filter-input-container bx-filter-calendar-container">
											<?$APPLICATION->IncludeComponent(
												'bitrix:main.calendar',
												'',
												array(
													'FORM_NAME' => $arResult["FILTER_NAME"]."_form",
													'SHOW_INPUT' => 'Y',
													'INPUT_ADDITIONAL_ATTR' => 'class="calendar" placeholder="'.FormatDate("SHORT", $arItem["VALUES"]["MIN"]["VALUE"]).'" onkeyup="smartFilter.keyup(this)" onchange="smartFilter.keyup(this)"',
													'INPUT_NAME' => $arItem["VALUES"]["MIN"]["CONTROL_NAME"],
													'INPUT_VALUE' => $arItem["VALUES"]["MIN"]["HTML_VALUE"],
													'SHOW_TIME' => 'N',
													'HIDE_TIMEBAR' => 'Y',
												),
												null,
												array('HIDE_ICONS' => 'Y')
											);?>
										</div></div>
										<div class="bx-filter-parameters-box-container-block"><div class="bx-filter-input-container bx-filter-calendar-container">
											<?$APPLICATION->IncludeComponent(
												'bitrix:main.calendar',
												'',
												array(
													'FORM_NAME' => $arResult["FILTER_NAME"]."_form",
													'SHOW_INPUT' => 'Y',
													'INPUT_ADDITIONAL_ATTR' => 'class="calendar" placeholder="'.FormatDate("SHORT", $arItem["VALUES"]["MAX"]["VALUE"]).'" onkeyup="smartFilter.keyup(this)" onchange="smartFilter.keyup(this)"',
													'INPUT_NAME' => $arItem["VALUES"]["MAX"]["CONTROL_NAME"],
													'INPUT_VALUE' => $arItem["VALUES"]["MAX"]["HTML_VALUE"],
													'SHOW_TIME' => 'N',
													'HIDE_TIMEBAR' => 'Y',
												),
												null,
												array('HIDE_ICONS' => 'Y')
											);?>
										</div></div>
										<?
										break;
									default://CHECKBOXES
										?>
										<div class="cont">
										<?$i=0;?>
										<?foreach($arItem["VALUES"] as $val => $ar):?>
											<?if($i==6){?>

											<div class="hideme" <?if ($arItem["DISPLAY_EXPANDED"] != "Y"):?>style="display: none;"<?endif?>>
											<?}?>


											<div class="checkline count<?=$ar["ELEMENT_COUNT"]?>" title="<?=$ar["VALUE"];?>">
												<input
													type="checkbox"
													value="<? echo $ar["HTML_VALUE"] ?>"
													name="<? echo $ar["CONTROL_NAME"] ?>"
													id="<? echo $ar["CONTROL_ID"] ?>"
													<? echo $ar["CHECKED"]? 'checked="checked"': '' ?>
													onclick="smartFilter.click(this)"
												/><label data-role="label_<?=$ar["CONTROL_ID"]?>" class="bx-filter-param-label <? echo $ar["DISABLED"] ? 'disabled': '' ?>" for="<? echo $ar["CONTROL_ID"] ?>"><?=$ar["VALUE"];?><?
												if ($arParams["DISPLAY_ELEMENT_COUNT"] !== "N" && isset($ar["ELEMENT_COUNT"])):
													?> (<span data-role="count_<?=$ar["CONTROL_ID"]?>"><? echo $ar["ELEMENT_COUNT"]; ?></span>)<?
												endif;?></label>
											</div>
											<?$i++;?>
										<?endforeach;?>
										<?if($i>6){?>
										</div><div class="hider plus">Показать все</div><?}?>
										</div>
								<?
								}
								?>
							</fieldset>
							<!-- / content -->

						</div>
					<?
					}
				}
					?>

					<!-- item -->
					<div class="item" style="display: none;">
						<!-- ttl -->
						<div class="ttl">
							<span>наличие</span>
							<div class="ico que-sm que"></div>
							<?/*<div class="hider plus"></div>*/?>
						</div>
						<!-- / ttl -->
						<!-- content -->
						<div class="cont hideme">

							<fieldset class="checkset blocke c">
								<div class="checkline">
									<input type="checkbox" id="check1n"><label for="check1n">в наличии</label>
								</div>
								<div class="checkline">
									<input type="checkbox" id="check2n"><label for="check2n">нету</label>
								</div>
							</fieldset>
						</div>
						<!-- / content -->
					</div>
					<!-- / item -->
					<div style="display: none;">
					<input
						class="btn btn-30 btn-f-show"
						type="submit"
						id="set_filter"
						name="set_filter"
						value="<?=GetMessage("CT_BCSF_SET_FILTER")?>"
					/></div>
					<!-- filter btns -->
					<div class="btns c">

						<input
							class="btn btn-30 btn-g btn-f-clear"
							type="submit"
							id="del_filter"
							name="del_filter"
							value="<?=GetMessage("CT_BCSF_DEL_FILTER")?>"
						/>
						<?
						$GLOBALS['SET_FILTER_LABEL'] = GetMessage("CT_BCSF_SET_FILTER");
						?>
					</div>
					<!-- / filter btns -->

				</form>
			</div>
			<!-- / filter .w end -->
		</div>
		<script>
			var smartFilter = new JCSmartFilter('<?echo CUtil::JSEscape($arResult["FORM_ACTION"])?>', '<?=CUtil::JSEscape($arParams["FILTER_VIEW_MODE"])?>', <?=CUtil::PhpToJSObject($arResult["JS_FILTER_PARAMS"])?>);
		</script>
	<? endif; ?>
<? endif; ?>
