<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

if (isset($arParams["TEMPLATE_THEME"]) && !empty($arParams["TEMPLATE_THEME"]))
{
	$arAvailableThemes = array();
	$dir = trim(preg_replace("'[\\\\/]+'", "/", dirname(__FILE__)."/themes/"));
	if (is_dir($dir) && $directory = opendir($dir))
	{
		while (($file = readdir($directory)) !== false)
		{
			if ($file != "." && $file != ".." && is_dir($dir.$file))
				$arAvailableThemes[] = $file;
		}
		closedir($directory);
	}

	if ($arParams["TEMPLATE_THEME"] == "site")
	{
		$solution = COption::GetOptionString("main", "wizard_solution", "", SITE_ID);
		if ($solution == "eshop")
		{
			$templateId = COption::GetOptionString("main", "wizard_template_id", "eshop_bootstrap", SITE_ID);
			$templateId = (preg_match("/^eshop_adapt/", $templateId)) ? "eshop_adapt" : $templateId;
			$theme = COption::GetOptionString("main", "wizard_".$templateId."_theme_id", "blue", SITE_ID);
			$arParams["TEMPLATE_THEME"] = (in_array($theme, $arAvailableThemes)) ? $theme : "blue";
		}
	}
	else
	{
		$arParams["TEMPLATE_THEME"] = (in_array($arParams["TEMPLATE_THEME"], $arAvailableThemes)) ? $arParams["TEMPLATE_THEME"] : "blue";
	}
}
else
{
	$arParams["TEMPLATE_THEME"] = "blue";
}

$arParams["FILTER_VIEW_MODE"] = (isset($arParams["FILTER_VIEW_MODE"]) && toUpper($arParams["FILTER_VIEW_MODE"]) == "HORIZONTAL") ? "HORIZONTAL" : "VERTICAL";
$arParams["POPUP_POSITION"] = (isset($arParams["POPUP_POSITION"]) && in_array($arParams["POPUP_POSITION"], array("left", "right"))) ? $arParams["POPUP_POSITION"] : "left";
$arResult['PROPS_GROUPS']=[];
$arSort = array('SORT'=>'ASC');
$arFilter = Array("IBLOCK_ID"=>42);
$arSelect = ['ID','NAME','SORT','PROPERTY_PROPS','XML_ID'];
$res = CIBlockElement::GetList($arSort, $arFilter, false, false, $arSelect);
while($arElement = $res->Fetch()) {
	
	$arResult['PROPS_GROUPS'][]=$arElement;
}
$prop_list = [];
$prop_xml=[];
$properties = CIBlockProperty::GetList(Array("sort"=>"asc", "name"=>"asc"), Array("IBLOCK_ID"=>8));
while ($prop_fields = $properties->GetNext()) {
	if ($prop_fields["SORT"] < 100) {
		continue;
	}
	if(in_array($prop_fields['ID'],$arResult['PROPERTY_ID_LIST'])){
		$prop_xml[$prop_fields['XML_ID']]=$prop_fields['ID'];
		$prop_xml2[$prop_fields['XML_ID']]=$prop_fields['CODE'];
		$prop_xml_hint[$prop_fields['XML_ID']]=$prop_fields['HINT'];
	}
	
}
/*p($prop_xml_hint);*/
//p($arResult['PROPERTY_ID_LIST']);
//p($arParams);
CModule::IncludeModule('iblock');
						
$properties=[];
$arFilter1 = Array("IBLOCK_ID"=>43,'PROPERTY_FILTER_CATS'=>$arParams['SECTION_ID']);
$arSelect1 = Array("ID", "NAME", "CODE", "XML_ID");
$res = CIBlockElement::GetList(Array('SORT'=>'ASC'), $arFilter1, false, false, $arSelect1);  
while($arFields = $res->Fetch()) {  
	$properties[]=$arFields['CODE'];
}
$arResult['SECTIONS']=array();
if(count($properties)==0){
	$rsParentSection = CIBlockSection::GetByID($arParams['SECTION_ID']);
	if ($arCurSection = $rsParentSection->GetNext())
	{
		$IDs=[];
		//p($arCurSection);
		$arFilter2 = Array("IBLOCK_ID"=>$arParams['IBLOCK_ID'], '>LEFT_MARGIN' => $arCurSection['LEFT_MARGIN'],'<RIGHT_MARGIN' => $arCurSection['RIGHT_MARGIN'],'>DEPTH_LEVEL' => $arCurSection['DEPTH_LEVEL']);
		$arSelect2 = Array("ID","NAME","SECTION_PAGE_URL" ,"IBLOCK_SECTION_ID", "DEPTH_LEVEL");
		$res = CIBlockSection::GetList($arSort, $arFilter2, false, $arSelect2);
		while($arFields=$res->GetNext()){
			//p($arFields);
			$IDs[]=$arFields['ID'];
			$arResult['SECTIONS'][$arFields['ID']]['NAME']=$arFields['NAME'];
			$arResult['SECTIONS'][$arFields['ID']]['DETAIL_PAGE_URL']=$arFields['SECTION_PAGE_URL'];
		}
		//p($IDs);
		$arFilter1 = Array("IBLOCK_ID"=>43,'PROPERTY_FILTER_CATS'=>$IDs);
		$arSelect1 = Array("ID", "NAME", "CODE", "XML_ID");
		$res = CIBlockElement::GetList(Array('SORT'=>'ASC'), $arFilter1, false, false, $arSelect1);  
		while($arFields = $res->Fetch()) {  
			$properties[]=$arFields['CODE'];
		}
	}
}
//p($properties);
$arDisableProp=array(
	'410bf94d-d687-11e6-bef2-ac162d7b6f40',
	'103a479e-6104-11e5-b5fc-ac162d7b6f40'
	);
foreach ($arResult['PROPS_GROUPS'] as $k => $v) {
	$whas=false;
	foreach ($v['PROPERTY_PROPS_VALUE'] as $k2 => $v2) {
		if($prop_xml[$v2]!=''&&!in_array($v2,$arDisableProp)){
			if(empty($arResult['ITEMS'][$prop_xml[$v2]]["VALUES"])){
				unset($arResult['PROPS_GROUPS'][$k]['PROPERTY_PROPS_VALUE'][$k2]);	
			}else{
				if ($arResult['ITEMS'][$prop_xml[$v2]]["DISPLAY_TYPE"] == "A"&& ($arResult['ITEMS'][$prop_xml[$v2]]["VALUES"]["MAX"]["VALUE"] - $arResult['ITEMS'][$prop_xml[$v2]]["VALUES"]["MIN"]["VALUE"] <= 0)) {
					unset($arResult['PROPS_GROUPS'][$k]['PROPERTY_PROPS_VALUE'][$k2]);
				}else{
					if(in_array($prop_xml2[$v2],$properties)){
						$arItem=$arResult['ITEMS'][$prop_xml[$v2]];
						$arItem['XML_ID']=$v2;
						$arItem['HINT']=$prop_xml_hint[$v2];
						$arResult['PROPS_GROUPS'][$k]['FILTER_VAL'][]=$arItem;
						$whas=true;			
					}else{
						unset($arResult['PROPS_GROUPS'][$k]['PROPERTY_PROPS_VALUE'][$k2]);	
					}
					
				}
				
			}
						
			
			
		}else{
			unset($arResult['PROPS_GROUPS'][$k]['PROPERTY_PROPS_VALUE'][$k2]);
		}
	}
	if(!$whas){
		unset($arResult['PROPS_GROUPS'][$k]);
	}
}
//p($arResult['PROPS_GROUPS']);
