function viewPlace(id){
	$(".contacts .item").removeClass("active");
	$("#"+id).addClass("active");
	$("#"+id).prependTo($(".contacts"));
}

if (!window.BX_GMapAddPlacemark)
{

	var iconBase = window.SITE_TEMPLATE_PATH+'/i/d/'; //путь к
	window.icons = {
		// магаз
		store: {
			icon: iconBase + 'i-map-mrk-blue.png'
		},
		//  склад
		warehouse: {
			icon: iconBase + 'i-map-mrk-blue.png'
		},
		// хз что
		torg: {
			icon: iconBase + 'i-map-mrk-blue.png'
		}
	};

	window.BX_GMapAddPlacemarkCnt = 0;

	window.BX_GMapAddPlacemark = function(arPlacemark, map_id)
	{
		var map = GLOBAL_arMapObjects[map_id];

		if (null == map)
			return false;

		if(!arPlacemark.LAT || !arPlacemark.LON)
			return false;

		var icon = 'store';
		if (window.BX_GMapAddPlacemarkCnt == 1) {
			icon = 'warehouse';
		} else if (window.BX_GMapAddPlacemarkCnt == 2) {
			icon = 'torg';
		}
		window.BX_GMapAddPlacemarkCnt++;
		if (window.BX_GMapAddPlacemarkCnt == 3) {
			window.BX_GMapAddPlacemarkCnt = 0;
		}
		var obPlacemark = new google.maps.Marker({
			'position': new google.maps.LatLng(arPlacemark.LAT, arPlacemark.LON),
			'icon': icons[icon].icon,
			'map': map
		});
		obPlacemark.addListener('click', function() {
			viewPlace(arPlacemark.ID);
		});

		if (BX.type.isNotEmptyString(arPlacemark.TEXT))
		{
			obPlacemark.infowin = new google.maps.InfoWindow({
				content: arPlacemark.TEXT.replace(/\n/g, '<br />')
			});

			google.maps.event.addListener(obPlacemark, 'click', function() {
				if (null != window['__bx_google_infowin_opened_' + map_id])
					window['__bx_google_infowin_opened_' + map_id].close();

				this.infowin.open(this.map, this);
				window['__bx_google_infowin_opened_' + map_id] = this.infowin;
			});
		}

		return obPlacemark;
	}
}

if (null == window.BXWaitForMap_view)
{
	function BXWaitForMap_view(map_id)
	{
		if (null == window.GLOBAL_arMapObjects) {
			return;
		}

		if (window.GLOBAL_arMapObjects[map_id]) {
			window['BX_SetPlacemarks_' + map_id]();
		} else {
			setTimeout('BXWaitForMap_view(\'' + map_id + '\')', 300);
		}
	}
}
