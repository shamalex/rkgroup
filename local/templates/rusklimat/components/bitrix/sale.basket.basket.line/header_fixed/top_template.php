<?if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED!==true) die();?><?
/*
<div class="bx-hdr-profile">
	<?if ($arParams['SHOW_AUTHOR'] == 'Y'):?>
		<div class="bx-basket-block">
			<i class="fa fa-user"></i>
			<?if ($USER->IsAuthorized()):
				$name = trim($USER->GetFullName());
				if (! $name)
					$name = trim($USER->GetLogin());
				if (strlen($name) > 15)
					$name = substr($name, 0, 12).'...';
				?>
				<a href="<?=$arParams['PATH_TO_PROFILE']?>"><?=htmlspecialcharsbx($name)?></a>
				&nbsp;
				<a href="?logout=yes"><?=GetMessage('TSB1_LOGOUT')?></a>
			<?else:?>
				<a href="<?=$arParams['PATH_TO_REGISTER']?>?login=yes"><?=GetMessage('TSB1_LOGIN')?></a>
				&nbsp;
				<a href="<?=$arParams['PATH_TO_REGISTER']?>?register=yes"><?=GetMessage('TSB1_REGISTER')?></a>
			<?endif?>
		</div>
	<?endif?>

	<div class="bx-basket-block">
		<i class="fa fa-shopping-cart"></i>
		<a href="<?=$arParams['PATH_TO_BASKET']?>"><?=GetMessage('TSB1_CART')?></a>
		<?if ($arParams['SHOW_NUM_PRODUCTS'] == 'Y' && ($arResult['NUM_PRODUCTS'] > 0 || $arParams['SHOW_EMPTY_VALUES'] == 'Y')):?>
			<?=$arResult['NUM_PRODUCTS'].' '.$arResult['PRODUCT(S)']?>
		<?endif?>
		<?if ($arParams['SHOW_TOTAL_PRICE'] == 'Y'):?>
			<br <?if ($arParams['POSITION_FIXED'] == 'Y'):?>class="hidden-xs"<?endif?>/>
			<span>
				<?=GetMessage('TSB1_TOTAL_PRICE')?>
				<?if ($arResult['NUM_PRODUCTS'] > 0 || $arParams['SHOW_EMPTY_VALUES'] == 'Y'):?>
					<strong><?=$arResult['TOTAL_PRICE']?></strong>
				<?endif?>
			</span>
		<?endif?>
		<?if ($arParams['SHOW_PERSONAL_LINK'] == 'Y'):?>
			<br>
			<span class="icon_info"></span>
			<a href="<?=$arParams['PATH_TO_PERSONAL']?>"><?=GetMessage('TSB1_PERSONAL')?></a>
		<?endif?>
	</div>
</div>
*/
// p($arResult);
$arResult['TOTAL_PRICE'] = str_replace(' руб.', '', $arResult['TOTAL_PRICE']);
$arResult['NUM_PRODUCTS'] = 0;
$arResult['FP']=0;
$disc = 0;$pr = 0;
foreach($arResult['CATEGORIES']['READY'] as $arItem) {
	$arResult['NUM_PRODUCTS'] += $arItem['QUANTITY'];
	$disc+=$arItem['DISCOUNT_PRICE'];
	$pr+=floatval(str_replace(" ", "", $arItem['FULL_PRICE']));
}
$arResult['FP']= (integer)($pr-$disc);
$productS = BasketNumberWordEndings($arResult['NUM_PRODUCTS']);
$arResult["PRODUCT(S)"] = GetMessage("TSB1_PRODUCT") . $productS;
?>

<?//p($arResult);?>
<div class="bsk item<?= ($arResult['NUM_PRODUCTS'] > 0 ? '' : ' empty') ?>" data-full="<?= ($arResult['NUM_PRODUCTS'] > 0 ? '1' : '0') ?>">
	<div class="i-w">
	
		<a class="lnk 222" href="<?=$arParams['PATH_TO_BASKET']?>"><span>Корзина</span></a>
		
		<div class="inf">
			<span class="count bskCount"><?=$arResult['NUM_PRODUCTS']?></span>
			<span class="txt bskTxt"><?= $arResult['PRODUCT(S)'] ?>, </span>
			<span class="bskSum"><?//=$arResult['TOTAL_PRICE']?><?=number_format($arResult['FP'], 0, '.', ' ');?><span class="rbl">a</span></span>
		</div>

		<a class="btn fix-bsk-btn<?= ($arResult['NUM_PRODUCTS'] > 0 ? '' : ' btn-disabled') ?>" id="FixedBtn" href="<?=$arParams['PATH_TO_BASKET']?>">Оформить</a>
	
		<div id="bskHint" class="ttp"><div class="w">Товар добавлен в корзину!</div></div>
	
	</div>
	<script>
		$(window).trigger('scroll');
	</script>
</div>