<?if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED!==true) die();?>
<?global $BASKET_ITEMS,$arGeoData;

foreach($arResult['CATEGORIES']['READY'] as $arItem) {
	$BASKET_ITEMS[$arItem['PRODUCT_ID']] = $arItem;
}

if($arGeoData["PREV_CITY_ID"]){
	SendDataToExternalServer("BASKET", array());
	if($_SESSION["BASKET_ITEMS"] != $BASKET_ITEMS){
		$arGeoData["BASKET_CHANGED"] = true;
	}
}

$_SESSION["BASKET_ITEMS"] = $BASKET_ITEMS;
?>