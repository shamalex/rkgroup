<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>

	<div class="col-l txt-block">
	
			<h1 class="ttl1"><?=$arResult['NAME']?></h1>
			<?=$arResult['~PREVIEW_TEXT']?>
			<??>
			<?if(count($arResult['PROPERTIES']['IMAGES']['VALUE'])>0){?>
			<!-- info slider -->
			<div class="info-slider c">
				<!-- info slides -->
				<div class="slides c" id="infoSlider">
					<?foreach ($arResult['PROPERTIES']['IMAGES']['VALUE'] as $k => $v) {
						$v=CFile::GetFileArray($v);
						?>
						<div class="item">
							<div class="pic" style="background-image:url('<?=$v['SRC']?>');"></div>
							<div class="dsc">
								<?if($arResult['PROPERTIES']['IMAGES']['DESCRIPTION'][$k]!=''){
									$arResult['PROPERTIES']['IMAGES']['DESCRIPTION'][$k]=explode("##",$arResult['PROPERTIES']['IMAGES']['DESCRIPTION'][$k]);?>
									<div class="ttl"><?=$arResult['PROPERTIES']['IMAGES']['DESCRIPTION'][$k][0]?></div>
									<?if($arResult['PROPERTIES']['IMAGES']['DESCRIPTION'][$k][1]){?>
									<div class="txt">
										<p><?=$arResult['PROPERTIES']['IMAGES']['DESCRIPTION'][$k][1]?></p>
									</div>
									<?}?>
								<?}?>
								
							</div>
						</div>	
					<?}?>
					
					
					
					
					
				</div>
				<!-- / info slides end -->
				<div class="info-slider-nav"><div class="w"></div></div>
			</div>
			<!-- / info slider end -->
			<?}?>

			<?=$arResult['~DETAIL_TEXT']?>

		</div>

<?$this->SetViewTarget("service_banner");?>
<?if($arResult["PROPERTIES"]["BANNER"]["VALUE"]){
$renderImage = CFile::ResizeImageGet($arResult["PROPERTIES"]["BANNER"]["VALUE"], Array("width" => 356, "height" => 320));?>
<div class="v-gr">
	<a href="<?=$arResult["PROPERTIES"]["BANNER"]["DESCRIPTION"]?>" class="pic-offer" style="background-image:url('<?=$renderImage['src']?>');"></a>
</div>		
<?}?>
<?$this->EndViewTarget();?>