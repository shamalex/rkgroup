<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>

<!-- roll -->
<div class="roll">
	<h1 class="ttl1"><?= $arResult['NAME'] ?></h1>
	
	<!-- articles -->
	<div class="articles">

		<? foreach($arResult["ITEMS"] as $arItem): ?>
			<?
				$this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_EDIT"));
				$this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_DELETE"), array("CONFIRM" => GetMessage('CT_BNL_ELEMENT_DELETE_CONFIRM')));
			?>
			<!-- item -->
			<div class="item b-art" id="<?=$this->GetEditAreaId($arItem['ID']);?>">
				<div class="w c">
					<? if($arParams["DISPLAY_PICTURE"]!="N" && is_array($arItem["PREVIEW_PICTURE"])): ?>
						<a href="<?=$arItem["DETAIL_PAGE_URL"]?>" class="pic" style="background-image:url('<?= $arItem["PREVIEW_PICTURE"]["SRC"] ?>');"></a>
					<? endif; ?>
					<div class="dsc">
						<div class="date"><?= $arItem["DISPLAY_ACTIVE_FROM"] ?></div>
						<div class="ttl"><a href="<?=$arItem["DETAIL_PAGE_URL"]?>"><?= $arItem["NAME"] ?></a></div>
						<div class="inf">
							<p><?= $arItem["PREVIEW_TEXT"] ?></p>
						</div>
						<div class="lnk">
							<a href="<?=$arItem["DETAIL_PAGE_URL"]?>">Подробнее</a>
						</div>

					</div>
					<!-- dsc end -->
				</div>
				<!-- / item .w -->
			</div>
			<!-- / item -->
		<? endforeach; ?>
		
		<?if($arParams["DISPLAY_BOTTOM_PAGER"]):?>
			<br /><?=$arResult["NAV_STRING"]?>
		<?endif;?>
		
	</div>
	<!-- / articles end -->

	
</div>
<!-- / roll end -->
