<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?><!-- info roll-->
<div class="roll">
	<h1 class="ttl1"><?=$APPLICATION->GetTitle()?></h1>
	<div class="c">
		<?
		foreach ($arResult['SECTIONS'] as &$arSection):
			$this->AddEditAction($arSection['ID'], $arSection['EDIT_LINK'], $strSectionEdit);
			$this->AddDeleteAction($arSection['ID'], $arSection['DELETE_LINK'], $strSectionDelete, $arSectionDeleteParams);
			?>
			<!-- item-->
			<div class="item b-art-sq" id="<?=$this->GetEditAreaId($arSection['ID']);?>">
				<div class="pic" style="background-image:url('<?= SITE_TEMPLATE_PATH ?>/i/blocks/info2.png');">
					<a href="<?= $arSection['SECTION_PAGE_URL'] ?>"><div class="all"><span><?= $arSection['ELEMENT_CNT'] ?> <?= getNumEnding($arSection['ELEMENT_CNT'], array('статья', 'статьи', 'статей')) ?></span></div></a>
				</div>
				<div class="w">
					<h2 class="ttl"><a href="<?= $arSection['SECTION_PAGE_URL'] ?>"><?= $arSection['NAME'] ?></a></h2>
					<? if(!empty($arSection['ELEMENTS'])): ?>
						<ul class="ul">
							<? foreach($arSection['ELEMENTS'] as $arElement): ?>
								<li><a href="<?= $arElement['DETAIL_PAGE_URL'] ?>"><?= $arElement['NAME'] ?></a></li>
							<? endforeach; ?>
						</ul>
					<? endif; ?>
				</div>
			</div>
			<!-- / item -->
		<? endforeach; ?>
	</div>
	<!-- с -->
</div>
<!-- / info roll end -->