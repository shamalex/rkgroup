<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
use Bitrix\Main\Loader;
use Bitrix\Main\ModuleManager;

$this->setFrameMode(true);

if (!isset($arParams['FILTER_VIEW_MODE']) || (string)$arParams['FILTER_VIEW_MODE'] == '')
	$arParams['FILTER_VIEW_MODE'] = 'VERTICAL';
$arParams['USE_FILTER'] = (isset($arParams['USE_FILTER']) && $arParams['USE_FILTER'] == 'Y' ? 'Y' : 'N');

if ($arParams['USE_FILTER'] == 'Y')
{
	$arFilter = array(
		"IBLOCK_ID" => $arParams["IBLOCK_ID"],
		"ACTIVE" => "Y",
		"GLOBAL_ACTIVE" => "Y",
	);
	if (0 < intval($arResult["VARIABLES"]["SECTION_ID"]))
	{
		$arFilter["ID"] = $arResult["VARIABLES"]["SECTION_ID"];
	}
	elseif ('' != $arResult["VARIABLES"]["SECTION_CODE"])
	{
		$arFilter["=CODE"] = $arResult["VARIABLES"]["SECTION_CODE"];
	}

	$obCache = new CPHPCache();
	if ($obCache->InitCache(36000, serialize($arFilter), "/iblock/catalog"))
	{
		$arCurSection = $obCache->GetVars();
	}
	elseif ($obCache->StartDataCache())
	{
		$arCurSection = array();
		if (Loader::includeModule("iblock"))
		{
			$dbRes = CIBlockSection::GetList(array(), $arFilter, false, array("ID"));

			if(defined("BX_COMP_MANAGED_CACHE"))
			{
				global $CACHE_MANAGER;
				$CACHE_MANAGER->StartTagCache("/iblock/catalog");

				if ($arCurSection = $dbRes->Fetch())
				{
					$CACHE_MANAGER->RegisterTag("iblock_id_".$arParams["IBLOCK_ID"]);
				}
				$CACHE_MANAGER->EndTagCache();
			}
			else
			{
				if(!$arCurSection = $dbRes->Fetch())
					$arCurSection = array();
			}
		}
		$obCache->EndDataCache($arCurSection);
	}
	if (!isset($arCurSection))
	{
		$arCurSection = array();
	}
	?><?$APPLICATION->IncludeComponent(
		"bitrix:catalog.smart.filter",
		"",
		array(
			"IBLOCK_TYPE" => $arParams["IBLOCK_TYPE"],
			"IBLOCK_ID" => $arParams["IBLOCK_ID"],
			"SECTION_ID" => $arCurSection['ID'],
			"FILTER_NAME" => $arParams["FILTER_NAME"],
			"PRICE_CODE" => $arParams["PRICE_CODE"],
			"CACHE_TYPE" => $arParams["CACHE_TYPE"],
			"CACHE_TIME" => $arParams["CACHE_TIME"],
			"CACHE_GROUPS" => $arParams["CACHE_GROUPS"],
			"SAVE_IN_SESSION" => "N",
			"FILTER_VIEW_MODE" => $arParams["FILTER_VIEW_MODE"],
			"XML_EXPORT" => "Y",
			"SECTION_TITLE" => "NAME",
			"SECTION_DESCRIPTION" => "DESCRIPTION",
			'HIDE_NOT_AVAILABLE' => $arParams["HIDE_NOT_AVAILABLE"],
			"TEMPLATE_THEME" => $arParams["TEMPLATE_THEME"],
			'CONVERT_CURRENCY' => $arParams['CONVERT_CURRENCY'],
			'CURRENCY_ID' => $arParams['CURRENCY_ID'],
		),
		$component,
		array('HIDE_ICONS' => 'Y')
	);?><?
}
?>


<?
if($arParams["USE_COMPARE"]=="Y")
{
	?><?$APPLICATION->IncludeComponent(
		"bitrix:catalog.compare.list",
		"",
		array(
			"IBLOCK_TYPE" => $arParams["IBLOCK_TYPE"],
			"IBLOCK_ID" => $arParams["IBLOCK_ID"],
			"NAME" => $arParams["COMPARE_NAME"],
			"DETAIL_URL" => $arResult["FOLDER"].$arResult["URL_TEMPLATES"]["element"],
			"COMPARE_URL" => $arResult["FOLDER"].$arResult["URL_TEMPLATES"]["compare"],
			"ACTION_VARIABLE" => $arParams["ACTION_VARIABLE"],
			"PRODUCT_ID_VARIABLE" => $arParams["PRODUCT_ID_VARIABLE"],
			'POSITION_FIXED' => isset($arParams['COMPARE_POSITION_FIXED']) ? $arParams['COMPARE_POSITION_FIXED'] : '',
			'POSITION' => isset($arParams['COMPARE_POSITION']) ? $arParams['COMPARE_POSITION'] : ''
		),
		$component,
		array("HIDE_ICONS" => "Y")
	);?><?
}

if (isset($arParams['USE_COMMON_SETTINGS_BASKET_POPUP']) && $arParams['USE_COMMON_SETTINGS_BASKET_POPUP'] == 'Y')
{
	$basketAction = (isset($arParams['COMMON_ADD_TO_BASKET_ACTION']) ? $arParams['COMMON_ADD_TO_BASKET_ACTION'] : '');
}
else
{
	$basketAction = (isset($arParams['SECTION_ADD_TO_BASKET_ACTION']) ? $arParams['SECTION_ADD_TO_BASKET_ACTION'] : '');
}
$intSectionID = 0;
?>

<?
// p($arParams);
// p($arResult);

$arSelect = ['NAME', 'ID'];
$rsSections = CIBlockSection::GetList(array(), array('ID' => $arResult['VARIABLES']['SECTION_ID']), false, $arSelect);
if ($arSection = $rsSections->GetNext()) {
	
	$arSectionsParams = [];
	$arSelect = Array("ID", "NAME", "CODE", "XML_ID", "PROPERTY_OWNER_SECTION", "PROPERTY_PARAM", "PROPERTY_PARAM.NAME", "PROPERTY_PARAM.PROPERTY_TYPE", "PROPERTY_PARAM.PROPERTY_SITE_CODE", "PROPERTY_VALUE", "PROPERTY_VALUE_LINK", "PROPERTY_VALUE_LINK.PROPERTY_NAME_FULL", "PROPERTY_VALUE_LINK.PROPERTY_FILE", "PROPERTY_VALUE_LINK.PROPERTY_FILEDATA", "PROPERTY_VALUE_LINK.PROPERTY_FILEEXT");
	$arFilter = Array("IBLOCK_ID"=>19, "PROPERTY_OWNER_SECTION" => $arSection['ID']);  
	$res = CIBlockElement::GetList(Array("PROPERTY_VALUE_LINK" => "ASC", "PROPERTY_PARAM.NAME" => "ASC"), $arFilter, false, false, $arSelect);  
	while($arFields = $res->GetNext()) { 
		if (!empty($arFields['PROPERTY_VALUE_LINK_PROPERTY_FILE_VALUE'])) {
			$arFields['PROPERTY_VALUE_LINK_PROPERTY_FILE_VALUE'] = CFile::GetFileArray($arFields['PROPERTY_VALUE_LINK_PROPERTY_FILE_VALUE']);
		}
		if (!isset($arSectionsParams[$arFields['PROPERTY_PARAM_NAME']])) {
			$arSectionsParams[$arFields['PROPERTY_PARAM_NAME']] = $arFields;
		}
	}
	// p($arSectionsParams);
	
}

?>


<div class="roll">
	<h1 class="ttl1"><?= $arSection['NAME'] ?></h1>
	<? if(!empty($arSectionsParams['описание категории']['~PROPERTY_VALUE_LINK_PROPERTY_FILEDATA_VALUE']['TEXT'])): ?>
		<div class="txt-block">
			<p><?= $arSectionsParams['описание категории']['~PROPERTY_VALUE_LINK_PROPERTY_FILEDATA_VALUE']['TEXT'] ?></p>
		</div>
	<? endif; ?>
	
	<?$APPLICATION->IncludeComponent(
		"bitrix:catalog.section.list",
		"inner",
		array(
			"IBLOCK_TYPE" => $arParams["IBLOCK_TYPE"],
			"IBLOCK_ID" => $arParams["IBLOCK_ID"],
			"SECTION_ID" => $arResult["VARIABLES"]["SECTION_ID"],
			"SECTION_CODE" => $arResult["VARIABLES"]["SECTION_CODE"],
			"CACHE_TYPE" => $arParams["CACHE_TYPE"],
			"CACHE_TIME" => $arParams["CACHE_TIME"],
			"CACHE_GROUPS" => $arParams["CACHE_GROUPS"],
			"COUNT_ELEMENTS" => $arParams["SECTION_COUNT_ELEMENTS"],
			"TOP_DEPTH" => $arParams["SECTION_TOP_DEPTH"],
			"SECTION_URL" => $arResult["FOLDER"].$arResult["URL_TEMPLATES"]["section"],
			"VIEW_MODE" => $arParams["SECTIONS_VIEW_MODE"],
			"SHOW_PARENT_NAME" => $arParams["SECTIONS_SHOW_PARENT_NAME"],
			"HIDE_SECTION_NAME" => (isset($arParams["SECTIONS_HIDE_SECTION_NAME"]) ? $arParams["SECTIONS_HIDE_SECTION_NAME"] : "N"),
			"ADD_SECTIONS_CHAIN" => (isset($arParams["ADD_SECTIONS_CHAIN"]) ? $arParams["ADD_SECTIONS_CHAIN"] : '')
		),
		$component,
		array("HIDE_ICONS" => "Y")
	);?>

	<!-- cat-cln roll -->
	<div class="cat-cln c roll">

		<!-- col-l -->
		<div class="col-l">
			
			<!-- filter switcher -->
			<div class="filter-switcher" id="filterSwitcher">
				<ul class="c">
					<li class="active"><a href="#" class="switch">выбор кондиционера (пока не работает)</a><div class="arr"></div></li>
				<!--	<li>/</li>
					<li><a href="#" class="switch">готовые решения</a><div class="arr"></div></li> -->
				</ul>
			</div>
			<!-- / filter switcher end -->
			
			<!-- filter -->
			<div class="filter c">
				<!-- filter .w -->
				<div class="w">
					
					<!-- item -->
					<div class="item open">
						<!-- ttl -->
						<div class="ttl">
							<span>цена, руб.</span>
							<div class="ico que-sm que"></div>
							<div class="hider minus"></div>
						</div>
						<!-- / ttl -->
						<!-- content -->
						<div class="cont hideme">
							<!-- range -->
							<div class="range">
								<fieldset class="inputset blocke c">
									<div class="inputline inp-ot">
										<label for="inputLow">от</label><input class="inp inp-100 inp-drk" type="text" id="inputLow">
									</div>								
									<div class="inputline inp-do">
										<label for="inputHigh">до</label><input class="inp inp-100 inp-drk" type="text" id="inputHigh">
									</div>
								</fieldset>
								<!-- range bar -->
								<div class="bar c">
									<input type="hidden" class="slider-input" value="23" />
								</div>
								<!-- / range bar end-->
							</div>
							<!-- / range end -->
							
							<fieldset class="checkset blocke c">
								<div class="checkline">
									<input type="checkbox" id="check1b"><label for="check1b">Эконом</label>
								</div>
								<div class="checkline">
									<input type="checkbox" id="check2b"><label for="check2b">Престиж</label>
								</div>
								<div class="checkline">
									<input type="checkbox" id="check3b"><label for="check3b">Комфорт</label>
								</div>
								<div class="checkline">
									<input type="checkbox" id="check4b"><label for="check4b">Люкс</label>
								</div>
							</fieldset>
						</div>
						<!-- / content -->
					</div>
					<!-- / item -->
					
					<!-- item -->
					<div class="item open">
						<!-- ttl -->
						<div class="ttl">
							<span>производители</span>
							<div class="ico que-sm que"></div>
							<div class="hider minus"></div>
						</div>
						<!-- / ttl -->
						<!-- content -->
						<div class="cont hideme">
							<fieldset class="checkset">
								<div class="checkline">
									<input type="checkbox" id="check1"><label for="check1">Ballu (29)</label>
								</div>
								<div class="checkline">
									<input type="checkbox" id="check2"><label for="check2">Electrolux (295)</label>
								</div>
								<div class="checkline">
									<input type="checkbox" id="check3"><label for="check3">Zanussi (429)</label>
								</div>
								<div class="checkline">
									<input type="checkbox" id="check4"><label for="check4">Hitachi  (2)</label>
								</div>
								<div class="checkline">
									<input type="checkbox" id="check5"><label for="check5">Mitsubishi Electric (85)</label>
								</div>
								<div class="checkline">
									<input type="checkbox" id="check6"><label for="check6">Biasi (4)</label>
								</div>
							</fieldset>
						</div>
						<!-- / content -->
					</div>
					<!-- / item -->
					
					<!-- item -->
					<div class="item">
						<!-- ttl -->
						<div class="ttl">
							<span>площадь</span>
							<div class="ico que-sm que"></div>
							<div class="hider plus"></div>
						</div>
						<!-- / ttl -->
						<!-- content -->
						<div class="cont hideme" style="display:none;">
							<fieldset class="checkset">
								<div class="checkline">
									<input type="checkbox" id="check1s" checked><label for="check1s">Ильича</label>
								</div>
								<div class="checkline">
									<input type="checkbox" id="check2s"><label for="check2s">Красная</label>
								</div>
								<div class="checkline">
									<input type="checkbox" id="check3s" checked><label for="check3s">Нормальная</label>
								</div>
							</fieldset>
						</div>
						<!-- / content -->
					</div>
					<!-- / item -->
					
					<!-- item -->
					<div class="item">
						<!-- ttl -->
						<div class="ttl">
							<span>наличие</span>
							<div class="ico que-sm que"></div>
							<div class="hider plus"></div>
						</div>
						<!-- / ttl -->
						<!-- content -->
						<div class="cont hideme" style="display:none">
						
							<fieldset class="checkset blocke c">
								<div class="checkline">
									<input type="checkbox" id="check1n"><label for="check1n">в наличии</label>
								</div>
								<div class="checkline">
									<input type="checkbox" id="check2n"><label for="check2n">нету</label>
								</div>
							</fieldset>
						</div>
						<!-- / content -->
					</div>
					<!-- / item -->
					
					<!-- filter btns -->
					<div class="btns c">
						<div class="btn btn-30 btn-f-show">Показать</div>
						<div class="btn btn-30 btn-g btn-f-clear">Сбросить</div>
					</div>
					<!-- / filter btns -->
				</div>
				<!-- / filter .w end -->
			</div>
			<!-- / filter end -->
			
			<!-- info-block -->
			<div class="info-block">
				<h3 class="ttl2">Помощь при выборе</h3>
				<ul class="list art-list">
					<li><a href="#">Как выбрать кондиционер?</a></li>
					<li><a href="#">Правила монтажа кондиционеров даже при открытых дверях и частично окнах!</a></li>
					<li><a href="#">Что делать?</a></li>
					<li><a href="#">Что делать? Как выбрать кондиционер? Что делать? Как выбрать кондиционер? Что делать? Как выбрать кондиционер? </a></li>
				</ul>
			</div>
			<!-- / info-block end-->
			
			<!-- info-block -->
			<div class="info-block">
				<h3 class="ttl2">Услуги по кондиционированию</h3>
				<ul class="list srv-list">
					<li><a href="#">Услуга 1</a></li>
					<li><a href="#">Услуга 2 по кондиционированию по кондиционированию</a></li>
					<li><a href="#">Услуга 3 по кондиционированию по кондиционированию по кондиционированию по кондиционированию</a></li>
					<li><a href="#">Услуга 4</a></li>
				</ul>
			</div>
			<!-- / info-block end-->
			
			<!-- info-block -->
			<div class="info-block">
				<h3 class="ttl2">Последний отзыв</h3>
				<div class="info-review">
					<div class="stars"></div>
					<div>
						<span class="name"><a href="#">Константин Константинопольский</a></span>
						<span class="city">Ярославль</span>
					</div>
					<div class="dsc">
						<p>Отличный кондиционер, пользуюсь 3 года, масса достоинств - мобильность, неплохо охлаждает помещение, даже при открытых дверях и частично окнах!</p>
					</div>
				</div>
			</div>
			<!-- / info-block end-->
			
			<!-- info-block -->
			<div class="info-block">
				<h3 class="ttl2">При покупке кондиционера также интересуются</h3>
				<ul class="list item-list">
					<li><a href="#" style="background-image:url('<?= SITE_TEMPLATE_PATH ?>/i/blocks/item-list1.png');">Кондиционеры</a></li>
					<li><a href="#" style="background-image:url('<?= SITE_TEMPLATE_PATH ?>/i/blocks/item-list2.png');">Аксессуары для кондиционеров</a></li>
					<li><a href="#" style="background-image:url('<?= SITE_TEMPLATE_PATH ?>/i/blocks/item-list3.png');">Товары для кондиционеров для монтажа кондиционеров</a></li>
				</ul>
			</div>
			<!-- / info-block end-->

		</div>
		<!-- / col-l end -->
		
		<!-- col-r -->
		<div class="col-r">

			<!-- results switcher -->
			<div class="results-switcher brd c">
				
				<div class="sort">
					<div class="ttl">Сортировать по</div>
					<select class="sort-select" id="sortSelect" name="sortSelect">
						<option value="1">цене</option>
						<option value="2">не цене</option>
						<option value="3">названию</option>
						<option value="4">цвету</option>
						<option value="5">random</option>
					</select>
				</div>
				
				<div class="amount">
					<div class="ttl">Показывать по</div>
					<select class="amount-select" id="amountSelect" name="amount-select">
						<option value="1">10</option>
						<option value="2">20</option>
						<option value="3">30</option>
						<option value="4">40</option>
						<option value="5">50</option>
						<option value="6">60</option>
					</select>
				</div>
				
				<div class="btns" id="resultsControl">
					<a href="#" class="switch tiles active"></a><a href="#" class="switch blocks"></a>
				</div>
			</div>
			<!-- / results switcher end -->
			
			<? $intSectionID = $APPLICATION->IncludeComponent(
				"bitrix:catalog.section",
				"",
				array(
					"IBLOCK_TYPE" => $arParams["IBLOCK_TYPE"],
					"IBLOCK_ID" => $arParams["IBLOCK_ID"],
					"ELEMENT_SORT_FIELD" => $arParams["ELEMENT_SORT_FIELD"],
					"ELEMENT_SORT_ORDER" => $arParams["ELEMENT_SORT_ORDER"],
					"ELEMENT_SORT_FIELD2" => $arParams["ELEMENT_SORT_FIELD2"],
					"ELEMENT_SORT_ORDER2" => $arParams["ELEMENT_SORT_ORDER2"],
					"PROPERTY_CODE" => $arParams["LIST_PROPERTY_CODE"],
					"META_KEYWORDS" => $arParams["LIST_META_KEYWORDS"],
					"META_DESCRIPTION" => $arParams["LIST_META_DESCRIPTION"],
					"BROWSER_TITLE" => $arParams["LIST_BROWSER_TITLE"],
					"INCLUDE_SUBSECTIONS" => $arParams["INCLUDE_SUBSECTIONS"],
					"BASKET_URL" => $arParams["BASKET_URL"],
					"ACTION_VARIABLE" => $arParams["ACTION_VARIABLE"],
					"PRODUCT_ID_VARIABLE" => $arParams["PRODUCT_ID_VARIABLE"],
					"SECTION_ID_VARIABLE" => $arParams["SECTION_ID_VARIABLE"],
					"PRODUCT_QUANTITY_VARIABLE" => $arParams["PRODUCT_QUANTITY_VARIABLE"],
					"PRODUCT_PROPS_VARIABLE" => $arParams["PRODUCT_PROPS_VARIABLE"],
					"FILTER_NAME" => $arParams["FILTER_NAME"],
					"CACHE_TYPE" => $arParams["CACHE_TYPE"],
					"CACHE_TIME" => $arParams["CACHE_TIME"],
					"CACHE_FILTER" => $arParams["CACHE_FILTER"],
					"CACHE_GROUPS" => $arParams["CACHE_GROUPS"],
					"SET_TITLE" => $arParams["SET_TITLE"],
					"SET_STATUS_404" => $arParams["SET_STATUS_404"],
					"DISPLAY_COMPARE" => $arParams["USE_COMPARE"],
					"PAGE_ELEMENT_COUNT" => $arParams["PAGE_ELEMENT_COUNT"],
					"LINE_ELEMENT_COUNT" => $arParams["LINE_ELEMENT_COUNT"],
					"PRICE_CODE" => $arParams["PRICE_CODE"],
					"USE_PRICE_COUNT" => $arParams["USE_PRICE_COUNT"],
					"SHOW_PRICE_COUNT" => $arParams["SHOW_PRICE_COUNT"],

					"PRICE_VAT_INCLUDE" => $arParams["PRICE_VAT_INCLUDE"],
					"USE_PRODUCT_QUANTITY" => $arParams['USE_PRODUCT_QUANTITY'],
					"ADD_PROPERTIES_TO_BASKET" => (isset($arParams["ADD_PROPERTIES_TO_BASKET"]) ? $arParams["ADD_PROPERTIES_TO_BASKET"] : ''),
					"PARTIAL_PRODUCT_PROPERTIES" => (isset($arParams["PARTIAL_PRODUCT_PROPERTIES"]) ? $arParams["PARTIAL_PRODUCT_PROPERTIES"] : ''),
					"PRODUCT_PROPERTIES" => $arParams["PRODUCT_PROPERTIES"],

					"DISPLAY_TOP_PAGER" => $arParams["DISPLAY_TOP_PAGER"],
					"DISPLAY_BOTTOM_PAGER" => $arParams["DISPLAY_BOTTOM_PAGER"],
					"PAGER_TITLE" => $arParams["PAGER_TITLE"],
					"PAGER_SHOW_ALWAYS" => $arParams["PAGER_SHOW_ALWAYS"],
					"PAGER_TEMPLATE" => $arParams["PAGER_TEMPLATE"],
					"PAGER_DESC_NUMBERING" => $arParams["PAGER_DESC_NUMBERING"],
					"PAGER_DESC_NUMBERING_CACHE_TIME" => $arParams["PAGER_DESC_NUMBERING_CACHE_TIME"],
					"PAGER_SHOW_ALL" => $arParams["PAGER_SHOW_ALL"],

					"OFFERS_CART_PROPERTIES" => $arParams["OFFERS_CART_PROPERTIES"],
					"OFFERS_FIELD_CODE" => $arParams["LIST_OFFERS_FIELD_CODE"],
					"OFFERS_PROPERTY_CODE" => $arParams["LIST_OFFERS_PROPERTY_CODE"],
					"OFFERS_SORT_FIELD" => $arParams["OFFERS_SORT_FIELD"],
					"OFFERS_SORT_ORDER" => $arParams["OFFERS_SORT_ORDER"],
					"OFFERS_SORT_FIELD2" => $arParams["OFFERS_SORT_FIELD2"],
					"OFFERS_SORT_ORDER2" => $arParams["OFFERS_SORT_ORDER2"],
					"OFFERS_LIMIT" => $arParams["LIST_OFFERS_LIMIT"],

					"SECTION_ID" => $arResult["VARIABLES"]["SECTION_ID"],
					"SECTION_CODE" => $arResult["VARIABLES"]["SECTION_CODE"],
					"SECTION_URL" => $arResult["FOLDER"].$arResult["URL_TEMPLATES"]["section"],
					"DETAIL_URL" => $arResult["FOLDER"].$arResult["URL_TEMPLATES"]["element"],
					'CONVERT_CURRENCY' => $arParams['CONVERT_CURRENCY'],
					'CURRENCY_ID' => $arParams['CURRENCY_ID'],
					'HIDE_NOT_AVAILABLE' => $arParams["HIDE_NOT_AVAILABLE"],

					'LABEL_PROP' => $arParams['LABEL_PROP'],
					'ADD_PICT_PROP' => $arParams['ADD_PICT_PROP'],
					'PRODUCT_DISPLAY_MODE' => $arParams['PRODUCT_DISPLAY_MODE'],

					'OFFER_ADD_PICT_PROP' => $arParams['OFFER_ADD_PICT_PROP'],
					'OFFER_TREE_PROPS' => $arParams['OFFER_TREE_PROPS'],
					'PRODUCT_SUBSCRIPTION' => $arParams['PRODUCT_SUBSCRIPTION'],
					'SHOW_DISCOUNT_PERCENT' => $arParams['SHOW_DISCOUNT_PERCENT'],
					'SHOW_OLD_PRICE' => $arParams['SHOW_OLD_PRICE'],
					'MESS_BTN_BUY' => $arParams['MESS_BTN_BUY'],
					'MESS_BTN_ADD_TO_BASKET' => $arParams['MESS_BTN_ADD_TO_BASKET'],
					'MESS_BTN_SUBSCRIBE' => $arParams['MESS_BTN_SUBSCRIBE'],
					'MESS_BTN_DETAIL' => $arParams['MESS_BTN_DETAIL'],
					'MESS_NOT_AVAILABLE' => $arParams['MESS_NOT_AVAILABLE'],

					'TEMPLATE_THEME' => (isset($arParams['TEMPLATE_THEME']) ? $arParams['TEMPLATE_THEME'] : ''),
					"ADD_SECTIONS_CHAIN" => "N",
					'ADD_TO_BASKET_ACTION' => $basketAction,
					'SHOW_CLOSE_POPUP' => isset($arParams['COMMON_SHOW_CLOSE_POPUP']) ? $arParams['COMMON_SHOW_CLOSE_POPUP'] : '',
					'COMPARE_PATH' => $arResult['FOLDER'].$arResult['URL_TEMPLATES']['compare']
				),
				$component
			); ?>
			<?
			$GLOBALS['CATALOG_CURRENT_SECTION_ID'] = $intSectionID;
			unset($basketAction);
			?>

			
		</div>
		<!-- / col-r end-->
	</div>
	<!-- / cat-cln end-->
</div>
<!-- / roll end -->



<!-- brand logos slider roll -->
<div class="roll">
	<h2 class="ttl-r">кондиционеры по производителям (пока не работает)</h2>
	<!-- brands logos slider -->	
	<div class="brands-logos-slider c">
		<!-- news slides -->
		<div class="slides c" id="brandsLogoSlider">
	 
			<!-- item -->
			<a class="item" href="#">
				<div class="ttl">Кондиционеры Ballu название в 3 строки максимум</div>
				<div class="pic" style="background-image:url('<?= SITE_TEMPLATE_PATH ?>/i/blocks/logo-ballu.png');"></div>
			</a>
			<!-- / item -->
			<!-- item -->
			<a class="item" href="#">
				<div class="ttl">Кондиционеры не Ballu</div>
				<div class="pic" style="background-image:url('<?= SITE_TEMPLATE_PATH ?>/i/blocks/logo-elec.png');"></div>
			</a>
			<!-- / item -->
			<!-- item -->
			<a class="item" href="#">
				<div class="ttl">Кондиционеры Zanus</div>
				<div class="pic" style="background-image:url('<?= SITE_TEMPLATE_PATH ?>/i/blocks/logo-zanus.png');"></div>
			</a>
			<!-- / item -->
			<!-- item -->
			<a class="item" href="#">
				<div class="ttl">Кондиционеры самое длинное название  здесь например</div>
				<div class="pic" style="background-image:url('<?= SITE_TEMPLATE_PATH ?>/i/blocks/cred1.png');"></div>
			</a>
			<!-- / item -->
			<!-- item -->
			<a class="item" href="#">
				<div class="ttl">Кондиционеры Yandex</div>
				<div class="pic" style="background-image:url('<?= SITE_TEMPLATE_PATH ?>/i/blocks/ya.jpg');"></div>
			</a>
			<!-- / item -->
			<!-- item -->
			<a class="item" href="#">
				<div class="ttl">Кондиционеры Ballu</div>
				<div class="pic" style="background-image:url('<?= SITE_TEMPLATE_PATH ?>/i/blocks/logo-ballu.png');"></div>
			</a>
			<!-- / item -->
			
			<!-- 6 items -->
			
			<!-- item -->
			<a class="item" href="#">
				<div class="ttl">Кондиционеры Ballu название в 3 строки максимум</div>
				<div class="pic" style="background-image:url('<?= SITE_TEMPLATE_PATH ?>/i/blocks/logo-ballu.png');"></div>
			</a>
			<!-- / item -->
			<!-- item -->
			<a class="item" href="#">
				<div class="ttl">Кондиционеры не Ballu</div>
				<div class="pic" style="background-image:url('<?= SITE_TEMPLATE_PATH ?>/i/blocks/logo-elec.png');"></div>
			</a>
			<!-- / item -->
			<!-- item -->
			<a class="item" href="#">
				<div class="ttl">Кондиционеры Zanussi и прочее</div>
				<div class="pic" style="background-image:url('<?= SITE_TEMPLATE_PATH ?>/i/blocks/logo-zanus.png');"></div>
			</a>
			<!-- / item -->
			<!-- item -->
			<a class="item" href="#">
				<div class="ttl">Кондиционеры самое длинное название здесь например</div>
				<div class="pic" style="background-image:url('<?= SITE_TEMPLATE_PATH ?>/i/blocks/cred1.png');"></div>
			</a>
			<!-- / item -->
			<!-- item -->
			<a class="item" href="#">
				<div class="ttl">Кондиционеры Yandex две строки заголовок</div>
				<div class="pic" style="background-image:url('<?= SITE_TEMPLATE_PATH ?>/i/blocks/ya.jpg');"></div>
			</a>
			<!-- / item -->
			<!-- item -->
			<a class="item" href="#">
				<div class="ttl">Кондиционеры Ballu</div>
				<div class="pic" style="background-image:url('<?= SITE_TEMPLATE_PATH ?>/i/blocks/logo-ballu.png');"></div>
			</a>
			<!-- / item -->
	
		</div>
		<!-- / slides end -->
	</div>
	<!-- / brands logos slider end -->
	
	
</div>
<!-- / brand logos slider roll end -->


<!-- about us -->
<div class="txt-block roll">
	<h2 class="ttl-r">ДОПОЛНИТЕЛЬНО О кондиционерах (где брать эту информацию???)</h2>
	<p>Торгово-Инженерный Департамент Русклимат – структурное подразделение холдинга, основные задачи которого, связаны с реализацией комплексных решений, продажей всего ассортимента оборудования и сопутствующих услуг конечным потребителям. Основная специализация - продажа, проектирование, монтаж и сервисное обслуживание систем вентиляции, кондиционирования, отопления.</p>
	<p>В состав департамента входит отдел продаж, транспортный отдел, проектный отдел, служба менеджеров проектов, инженерная, монтажная и сервисная службы. Нашими специалистами реализовано более 50000 объектов различного направления, начиная от бытовых кондиционеров и заканчивая сложными системами центрального кондиционирования, вентиляции, отопления и водоснабжения.</p>
	<p>Департамент обладает всеми необходимыми лицензиями и допусками СРО и является активным членом специализированных обществ и партнерств.</p>
</div>
<!-- about us end -->