<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);

/*
$arViewModeList = $arResult['VIEW_MODE_LIST'];

$arViewStyles = array(
	'LIST' => array(
		'CONT' => 'bx_sitemap',
		'TITLE' => 'bx_sitemap_title',
		'LIST' => 'bx_sitemap_ul',
	),
	'LINE' => array(
		'CONT' => 'bx_catalog_line',
		'TITLE' => 'bx_catalog_line_category_title',
		'LIST' => 'bx_catalog_line_ul',
		'EMPTY_IMG' => $this->GetFolder().'/images/line-empty.png'
	),
	'TEXT' => array(
		'CONT' => 'bx_catalog_text',
		'TITLE' => 'bx_catalog_text_category_title',
		'LIST' => 'bx_catalog_text_ul'
	),
	'TILE' => array(
		'CONT' => 'bx_catalog_tile',
		'TITLE' => 'bx_catalog_tile_category_title',
		'LIST' => 'bx_catalog_tile_ul',
		'EMPTY_IMG' => $this->GetFolder().'/images/tile-empty.png'
	)
);
$arCurView = $arViewStyles[$arParams['VIEW_MODE']];

$strSectionEdit = CIBlock::GetArrayByID($arParams["IBLOCK_ID"], "SECTION_EDIT");
$strSectionDelete = CIBlock::GetArrayByID($arParams["IBLOCK_ID"], "SECTION_DELETE");
$arSectionDeleteParams = array("CONFIRM" => GetMessage('CT_BCSL_ELEMENT_DELETE_CONFIRM'));

?><div class="<? echo $arCurView['CONT']; ?>"><?
if ('Y' == $arParams['SHOW_PARENT_NAME'] && 0 < $arResult['SECTION']['ID'])
{
	$this->AddEditAction($arResult['SECTION']['ID'], $arResult['SECTION']['EDIT_LINK'], $strSectionEdit);
	$this->AddDeleteAction($arResult['SECTION']['ID'], $arResult['SECTION']['DELETE_LINK'], $strSectionDelete, $arSectionDeleteParams);

	?><h1
		class="<? echo $arCurView['TITLE']; ?>"
		id="<? echo $this->GetEditAreaId($arResult['SECTION']['ID']); ?>"
	><a href="<? echo $arResult['SECTION']['SECTION_PAGE_URL']; ?>"><?
		echo (
			isset($arResult['SECTION']["IPROPERTY_VALUES"]["SECTION_PAGE_TITLE"]) && $arResult['SECTION']["IPROPERTY_VALUES"]["SECTION_PAGE_TITLE"] != ""
			? $arResult['SECTION']["IPROPERTY_VALUES"]["SECTION_PAGE_TITLE"]
			: $arResult['SECTION']['NAME']
		);
	?></a></h1><?
}
if (0 < $arResult["SECTIONS_COUNT"])
{
?>
<ul class="<? echo $arCurView['LIST']; ?>">
<?
	switch ($arParams['VIEW_MODE'])
	{
		case 'LINE':
			foreach ($arResult['SECTIONS'] as &$arSection)
			{
				$this->AddEditAction($arSection['ID'], $arSection['EDIT_LINK'], $strSectionEdit);
				$this->AddDeleteAction($arSection['ID'], $arSection['DELETE_LINK'], $strSectionDelete, $arSectionDeleteParams);

				if (false === $arSection['PICTURE'])
					$arSection['PICTURE'] = array(
						'SRC' => $arCurView['EMPTY_IMG'],
						'ALT' => (
							'' != $arSection["IPROPERTY_VALUES"]["SECTION_PICTURE_FILE_ALT"]
							? $arSection["IPROPERTY_VALUES"]["SECTION_PICTURE_FILE_ALT"]
							: $arSection["NAME"]
						),
						'TITLE' => (
							'' != $arSection["IPROPERTY_VALUES"]["SECTION_PICTURE_FILE_TITLE"]
							? $arSection["IPROPERTY_VALUES"]["SECTION_PICTURE_FILE_TITLE"]
							: $arSection["NAME"]
						)
					);
				?><li id="<? echo $this->GetEditAreaId($arSection['ID']); ?>">
				<a
					href="<? echo $arSection['SECTION_PAGE_URL']; ?>"
					class="bx_catalog_line_img"
					style="background-image: url(<? echo $arSection['PICTURE']['SRC']; ?>);"
					title="<? echo $arSection['PICTURE']['TITLE']; ?>"
				></a>
				<h2 class="bx_catalog_line_title"><a href="<? echo $arSection['SECTION_PAGE_URL']; ?>"><? echo $arSection['NAME']; ?></a><?
				if ($arParams["COUNT_ELEMENTS"])
				{
					?> <span>(<? echo $arSection['ELEMENT_CNT']; ?>)</span><?
				}
				?></h2><?
				if ('' != $arSection['DESCRIPTION'])
				{
					?><p class="bx_catalog_line_description"><? echo $arSection['DESCRIPTION']; ?></p><?
				}
				?><div style="clear: both;"></div>
				</li><?
			}
			unset($arSection);
			break;
		case 'TEXT':
			foreach ($arResult['SECTIONS'] as &$arSection)
			{
				$this->AddEditAction($arSection['ID'], $arSection['EDIT_LINK'], $strSectionEdit);
				$this->AddDeleteAction($arSection['ID'], $arSection['DELETE_LINK'], $strSectionDelete, $arSectionDeleteParams);

				?><li id="<? echo $this->GetEditAreaId($arSection['ID']); ?>"><h2 class="bx_catalog_text_title"><a href="<? echo $arSection['SECTION_PAGE_URL']; ?>"><? echo $arSection['NAME']; ?></a><?
				if ($arParams["COUNT_ELEMENTS"])
				{
					?> <span>(<? echo $arSection['ELEMENT_CNT']; ?>)</span><?
				}
				?></h2></li><?
			}
			unset($arSection);
			break;
		case 'TILE':
			foreach ($arResult['SECTIONS'] as &$arSection)
			{
				$this->AddEditAction($arSection['ID'], $arSection['EDIT_LINK'], $strSectionEdit);
				$this->AddDeleteAction($arSection['ID'], $arSection['DELETE_LINK'], $strSectionDelete, $arSectionDeleteParams);

				if (false === $arSection['PICTURE'])
					$arSection['PICTURE'] = array(
						'SRC' => $arCurView['EMPTY_IMG'],
						'ALT' => (
							'' != $arSection["IPROPERTY_VALUES"]["SECTION_PICTURE_FILE_ALT"]
							? $arSection["IPROPERTY_VALUES"]["SECTION_PICTURE_FILE_ALT"]
							: $arSection["NAME"]
						),
						'TITLE' => (
							'' != $arSection["IPROPERTY_VALUES"]["SECTION_PICTURE_FILE_TITLE"]
							? $arSection["IPROPERTY_VALUES"]["SECTION_PICTURE_FILE_TITLE"]
							: $arSection["NAME"]
						)
					);
				?><li id="<? echo $this->GetEditAreaId($arSection['ID']); ?>">
				<a
					href="<? echo $arSection['SECTION_PAGE_URL']; ?>"
					class="bx_catalog_tile_img"
					style="background-image:url(<? echo $arSection['PICTURE']['SRC']; ?>);"
					title="<? echo $arSection['PICTURE']['TITLE']; ?>"
					> </a><?
				if ('Y' != $arParams['HIDE_SECTION_NAME'])
				{
					?><h2 class="bx_catalog_tile_title"><a href="<? echo $arSection['SECTION_PAGE_URL']; ?>"><? echo $arSection['NAME']; ?></a><?
					if ($arParams["COUNT_ELEMENTS"])
					{
						?> <span>(<? echo $arSection['ELEMENT_CNT']; ?>)</span><?
					}
				?></h2><?
				}
				?></li><?
			}
			unset($arSection);
			break;
		case 'LIST':
			$intCurrentDepth = 1;
			$boolFirst = true;
			foreach ($arResult['SECTIONS'] as &$arSection)
			{
				$this->AddEditAction($arSection['ID'], $arSection['EDIT_LINK'], $strSectionEdit);
				$this->AddDeleteAction($arSection['ID'], $arSection['DELETE_LINK'], $strSectionDelete, $arSectionDeleteParams);

				if ($intCurrentDepth < $arSection['RELATIVE_DEPTH_LEVEL'])
				{
					if (0 < $intCurrentDepth)
						echo "\n",str_repeat("\t", $arSection['RELATIVE_DEPTH_LEVEL']),'<ul>';
				}
				elseif ($intCurrentDepth == $arSection['RELATIVE_DEPTH_LEVEL'])
				{
					if (!$boolFirst)
						echo '</li>';
				}
				else
				{
					while ($intCurrentDepth > $arSection['RELATIVE_DEPTH_LEVEL'])
					{
						echo '</li>',"\n",str_repeat("\t", $intCurrentDepth),'</ul>',"\n",str_repeat("\t", $intCurrentDepth-1);
						$intCurrentDepth--;
					}
					echo str_repeat("\t", $intCurrentDepth-1),'</li>';
				}

				echo (!$boolFirst ? "\n" : ''),str_repeat("\t", $arSection['RELATIVE_DEPTH_LEVEL']);
				?><li id="<?=$this->GetEditAreaId($arSection['ID']);?>"><h2 class="bx_sitemap_li_title"><a href="<? echo $arSection["SECTION_PAGE_URL"]; ?>"><? echo $arSection["NAME"];?><?
				if ($arParams["COUNT_ELEMENTS"])
				{
					?> <span>(<? echo $arSection["ELEMENT_CNT"]; ?>)</span><?
				}
				?></a></h2><?

				$intCurrentDepth = $arSection['RELATIVE_DEPTH_LEVEL'];
				$boolFirst = false;
			}
			unset($arSection);
			while ($intCurrentDepth > 1)
			{
				echo '</li>',"\n",str_repeat("\t", $intCurrentDepth),'</ul>',"\n",str_repeat("\t", $intCurrentDepth-1);
				$intCurrentDepth--;
			}
			if ($intCurrentDepth > 0)
			{
				echo '</li>',"\n";
			}
			break;
	}
?>
</ul>
<?
	echo ('LINE' != $arParams['VIEW_MODE'] ? '<div style="clear: both;"></div>' : '');
}
?></div>
<?
*/

// p($arResult);
// p($arResult['SECTIONS'][0]);
$arRootSections = [];
$arSectionsIds = [];
foreach($arResult['SECTIONS'] as $arSection) {
	$arSectionsIds[] = $arSection['ID'];
	if ($arSection['DEPTH_LEVEL'] == 1 && $arSection['ELEMENT_CNT'] > 0) {
		$arRootSections[$arSection['ID']] = $arSection;
	}
	if ($arSection['DEPTH_LEVEL'] == 2 && $arSection['ELEMENT_CNT'] > 0) {
		$arRootSections[$arSection['IBLOCK_SECTION_ID']]['SECTIONS'][$arSection['ID']] = $arSection;
	}
}

$arSectionsParams = [];
$arSelect = Array("ID", "NAME", "CODE", "XML_ID", "PROPERTY_OWNER_SECTION", "PROPERTY_PARAM", "PROPERTY_PARAM.NAME", "PROPERTY_PARAM.PROPERTY_TYPE", "PROPERTY_PARAM.PROPERTY_SITE_CODE", "PROPERTY_VALUE", "PROPERTY_VALUE_LINK", "PROPERTY_VALUE_LINK.PROPERTY_NAME_FULL", "PROPERTY_VALUE_LINK.PROPERTY_FILE", "PROPERTY_VALUE_LINK.PROPERTY_FILEDATA", "PROPERTY_VALUE_LINK.PROPERTY_FILEEXT");
$arFilter = Array("IBLOCK_ID"=>19, "PROPERTY_OWNER_SECTION" => $arSectionsIds);  
$res = CIBlockElement::GetList(Array("PROPERTY_VALUE_LINK" => "ASC", "PROPERTY_PARAM.NAME" => "ASC"), $arFilter, false, false, $arSelect);  
while($arFields = $res->GetNext()) { 
	if (!empty($arFields['PROPERTY_VALUE_LINK_PROPERTY_FILE_VALUE'])) {
		$arFields['PROPERTY_VALUE_LINK_PROPERTY_FILE_VALUE'] = CFile::GetFileArray($arFields['PROPERTY_VALUE_LINK_PROPERTY_FILE_VALUE']);
	}
	if (!isset($arSectionsParams[$arFields['PROPERTY_OWNER_SECTION_VALUE']][$arFields['PROPERTY_PARAM_NAME']])) {
		$arSectionsParams[$arFields['PROPERTY_OWNER_SECTION_VALUE']][$arFields['PROPERTY_PARAM_NAME']] = $arFields;
	}
}
// p($arSectionsParams[8983]);

?>


<!-- cat ctrl -->
<div id="thisIsTop" class="cat-ctrl brd c">

	<ul>
		<li><a href="#sbCond">Кондиционирование</a></li>
		<li>/</li>
		<li><a href="#sbHeat">Отопление</a></li>
		<li>/</li>
		<li><a href="#sbVent">Вентиляция</a></li>
		<li>/</li>
		<li><a href="#sbWater">Водоснабжение</a></li>
	</ul>

	<div class="btns" id="catControl">
		<a href="#" class="switch tldr active">кратко</a><a href="#" class="switch moar">подробно</a>
	</div>
	
</div>
<!-- / cat ctrl end -->

<!-- cat types -->
<div class="cat-types without">

<!-- type -->
	<h2 class="ttl2 c"><span>Кондиционирование и все остальное (откуда берутся эти категории???)</span> <div style="background-image:url('<?= SITE_TEMPLATE_PATH ?>/i/blocks/podcategor-ttl-1.png');" class="ico"></div></h2>
	<!-- items -->
	<div class="items c">
	
		<? foreach($arRootSections as $arSection): ?>
			<?
			$pic = false;
			if (isset($arSectionsParams[$arSection['ID']])) {
				$params = $arSectionsParams[$arSection['ID']];
				if (isset($params['изображение категории']) && !empty($params['изображение категории']['PROPERTY_VALUE_LINK_PROPERTY_FILE_VALUE']['SRC'])) {
					$pic = $params['изображение категории']['PROPERTY_VALUE_LINK_PROPERTY_FILE_VALUE']['SRC'];
				}
			}
			?>
			<!-- item -->
			<div class="item">
				<a href="<?= $arSection['SECTION_PAGE_URL'] ?>" class="lnk">
					<div class="pic"<? if($pic): ?> style="background-image:url('<?= $pic ?>');"<? endif; ?>></div>
					<h3 class="ttl"><?= $arSection['NAME'] ?></h3>
				</a>
				<ul class="list">
					<? foreach($arSection['SECTIONS'] as $arChildSection): ?>
						<li><a href="<?= $arChildSection['SECTION_PAGE_URL'] ?>"><?= $arChildSection['NAME'] ?></a><span>от 100 руб. до 100 000 руб.</span></li>
					<? endforeach; ?>
				</ul>
			</div>
			<!-- / item -->
		<? endforeach; ?>
		
	</div>
	<!-- / items -->
	<p class="to-top"><a href="#thisIsTop">Наверх</a></p>
	
<!-- type -->

</div>
<!-- / cat types end -->