<?
$MESS["CP_BCT_TPL_ROTATE_TIMER"] = "Diapositiva avanzada después, sec. (0 - no cambian de diapositivas)";
$MESS["ROTATE_TIMER_TIP"] = "Especifica el tiempo que una diapositiva aparece en la pantalla. Este parámetro se usa cuando son diapositivas avanzadas.";
?>