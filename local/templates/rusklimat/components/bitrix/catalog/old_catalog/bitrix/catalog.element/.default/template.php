<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);


/* $templateLibrary = array('popup');
$currencyList = '';
if (!empty($arResult['CURRENCIES']))
{
	$templateLibrary[] = 'currency';
	$currencyList = CUtil::PhpToJSObject($arResult['CURRENCIES'], false, true, true);
}
$templateData = array(
	'TEMPLATE_THEME' => $this->GetFolder().'/themes/'.$arParams['TEMPLATE_THEME'].'/style.css',
	'TEMPLATE_CLASS' => 'bx_'.$arParams['TEMPLATE_THEME'],
	'TEMPLATE_LIBRARY' => $templateLibrary,
	'CURRENCIES' => $currencyList
);
unset($currencyList, $templateLibrary);

$strMainID = $this->GetEditAreaId($arResult['ID']);
$arItemIDs = array(
	'ID' => $strMainID,
	'PICT' => $strMainID.'_pict',
	'DISCOUNT_PICT_ID' => $strMainID.'_dsc_pict',
	'STICKER_ID' => $strMainID.'_sticker',
	'BIG_SLIDER_ID' => $strMainID.'_big_slider',
	'BIG_IMG_CONT_ID' => $strMainID.'_bigimg_cont',
	'SLIDER_CONT_ID' => $strMainID.'_slider_cont',
	'SLIDER_LIST' => $strMainID.'_slider_list',
	'SLIDER_LEFT' => $strMainID.'_slider_left',
	'SLIDER_RIGHT' => $strMainID.'_slider_right',
	'OLD_PRICE' => $strMainID.'_old_price',
	'PRICE' => $strMainID.'_price',
	'DISCOUNT_PRICE' => $strMainID.'_price_discount',
	'SLIDER_CONT_OF_ID' => $strMainID.'_slider_cont_',
	'SLIDER_LIST_OF_ID' => $strMainID.'_slider_list_',
	'SLIDER_LEFT_OF_ID' => $strMainID.'_slider_left_',
	'SLIDER_RIGHT_OF_ID' => $strMainID.'_slider_right_',
	'QUANTITY' => $strMainID.'_quantity',
	'QUANTITY_DOWN' => $strMainID.'_quant_down',
	'QUANTITY_UP' => $strMainID.'_quant_up',
	'QUANTITY_MEASURE' => $strMainID.'_quant_measure',
	'QUANTITY_LIMIT' => $strMainID.'_quant_limit',
	'BASIS_PRICE' => $strMainID.'_basis_price',
	'BUY_LINK' => $strMainID.'_buy_link',
	'ADD_BASKET_LINK' => $strMainID.'_add_basket_link',
	'BASKET_ACTIONS' => $strMainID.'_basket_actions',
	'NOT_AVAILABLE_MESS' => $strMainID.'_not_avail',
	'COMPARE_LINK' => $strMainID.'_compare_link',
	'PROP' => $strMainID.'_prop_',
	'PROP_DIV' => $strMainID.'_skudiv',
	'DISPLAY_PROP_DIV' => $strMainID.'_sku_prop',
	'OFFER_GROUP' => $strMainID.'_set_group_',
	'BASKET_PROP_DIV' => $strMainID.'_basket_prop',
);
$strObName = 'ob'.preg_replace("/[^a-zA-Z0-9_]/", "x", $strMainID);
$templateData['JS_OBJ'] = $strObName;

$strTitle = (
	isset($arResult["IPROPERTY_VALUES"]["ELEMENT_DETAIL_PICTURE_FILE_TITLE"]) && $arResult["IPROPERTY_VALUES"]["ELEMENT_DETAIL_PICTURE_FILE_TITLE"] != ''
	? $arResult["IPROPERTY_VALUES"]["ELEMENT_DETAIL_PICTURE_FILE_TITLE"]
	: $arResult['NAME']
);
$strAlt = (
	isset($arResult["IPROPERTY_VALUES"]["ELEMENT_DETAIL_PICTURE_FILE_ALT"]) && $arResult["IPROPERTY_VALUES"]["ELEMENT_DETAIL_PICTURE_FILE_ALT"] != ''
	? $arResult["IPROPERTY_VALUES"]["ELEMENT_DETAIL_PICTURE_FILE_ALT"]
	: $arResult['NAME']
);
?><div class="bx_item_detail <? echo $templateData['TEMPLATE_CLASS']; ?>" id="<? echo $arItemIDs['ID']; ?>">
<?
if ('Y' == $arParams['DISPLAY_NAME'])
{
?>
<div class="bx_item_title"><h1><span><?
	echo (
		isset($arResult["IPROPERTY_VALUES"]["ELEMENT_PAGE_TITLE"]) && $arResult["IPROPERTY_VALUES"]["ELEMENT_PAGE_TITLE"] != ''
		? $arResult["IPROPERTY_VALUES"]["ELEMENT_PAGE_TITLE"]
		: $arResult["NAME"]
	); ?>
</span></h1></div>
<?
}
reset($arResult['MORE_PHOTO']);
$arFirstPhoto = current($arResult['MORE_PHOTO']);
?>
	<div class="bx_item_container">
		<div class="bx_lt">
<div class="bx_item_slider" id="<? echo $arItemIDs['BIG_SLIDER_ID']; ?>">
	<div class="bx_bigimages" id="<? echo $arItemIDs['BIG_IMG_CONT_ID']; ?>">
	<div class="bx_bigimages_imgcontainer">
	<span class="bx_bigimages_aligner"><img id="<? echo $arItemIDs['PICT']; ?>" src="<? echo $arFirstPhoto['SRC']; ?>" alt="<? echo $strAlt; ?>" title="<? echo $strTitle; ?>"></span>
<?
if ('Y' == $arParams['SHOW_DISCOUNT_PERCENT'])
{
	if (!isset($arResult['OFFERS']) || empty($arResult['OFFERS']))
	{
		if (0 < $arResult['MIN_PRICE']['DISCOUNT_DIFF'])
		{
?>
	<div class="bx_stick_disc right bottom" id="<? echo $arItemIDs['DISCOUNT_PICT_ID'] ?>"><? echo -$arResult['MIN_PRICE']['DISCOUNT_DIFF_PERCENT']; ?>%</div>
<?
		}
	}
	else
	{
?>
	<div class="bx_stick_disc right bottom" id="<? echo $arItemIDs['DISCOUNT_PICT_ID'] ?>" style="display: none;"></div>
<?
	}
}
if ($arResult['LABEL'])
{
?>
	<div class="bx_stick average left top" id="<? echo $arItemIDs['STICKER_ID'] ?>" title="<? echo $arResult['LABEL_VALUE']; ?>"><? echo $arResult['LABEL_VALUE']; ?></div>
<?
}
?>
	</div>
	</div>
<?
if ($arResult['SHOW_SLIDER'])
{
	if (!isset($arResult['OFFERS']) || empty($arResult['OFFERS']))
	{
		if (5 < $arResult['MORE_PHOTO_COUNT'])
		{
			$strClass = 'bx_slider_conteiner full';
			$strOneWidth = (100/$arResult['MORE_PHOTO_COUNT']).'%';
			$strWidth = (20*$arResult['MORE_PHOTO_COUNT']).'%';
			$strSlideStyle = '';
		}
		else
		{
			$strClass = 'bx_slider_conteiner';
			$strOneWidth = '20%';
			$strWidth = '100%';
			$strSlideStyle = 'display: none;';
		}
?>
	<div class="<? echo $strClass; ?>" id="<? echo $arItemIDs['SLIDER_CONT_ID']; ?>">
	<div class="bx_slider_scroller_container">
	<div class="bx_slide">
	<ul style="width: <? echo $strWidth; ?>;" id="<? echo $arItemIDs['SLIDER_LIST']; ?>">
<?
		foreach ($arResult['MORE_PHOTO'] as &$arOnePhoto)
		{
?>
	<li data-value="<? echo $arOnePhoto['ID']; ?>" style="width: <? echo $strOneWidth; ?>; padding-top: <? echo $strOneWidth; ?>;"><span class="cnt"><span class="cnt_item" style="background-image:url('<? echo $arOnePhoto['SRC']; ?>');"></span></span></li>
<?
		}
		unset($arOnePhoto);
?>
	</ul>
	</div>
	<div class="bx_slide_left" id="<? echo $arItemIDs['SLIDER_LEFT']; ?>" style="<? echo $strSlideStyle; ?>"></div>
	<div class="bx_slide_right" id="<? echo $arItemIDs['SLIDER_RIGHT']; ?>" style="<? echo $strSlideStyle; ?>"></div>
	</div>
	</div>
<?
	}
	else
	{
		foreach ($arResult['OFFERS'] as $key => $arOneOffer)
		{
			if (!isset($arOneOffer['MORE_PHOTO_COUNT']) || 0 >= $arOneOffer['MORE_PHOTO_COUNT'])
				continue;
			$strVisible = ($key == $arResult['OFFERS_SELECTED'] ? '' : 'none');
			if (5 < $arOneOffer['MORE_PHOTO_COUNT'])
			{
				$strClass = 'bx_slider_conteiner full';
				$strOneWidth = (100/$arOneOffer['MORE_PHOTO_COUNT']).'%';
				$strWidth = (20*$arOneOffer['MORE_PHOTO_COUNT']).'%';
				$strSlideStyle = '';
			}
			else
			{
				$strClass = 'bx_slider_conteiner';
				$strOneWidth = '20%';
				$strWidth = '100%';
				$strSlideStyle = 'display: none;';
			}
?>
	<div class="<? echo $strClass; ?>" id="<? echo $arItemIDs['SLIDER_CONT_OF_ID'].$arOneOffer['ID']; ?>" style="display: <? echo $strVisible; ?>;">
	<div class="bx_slider_scroller_container">
	<div class="bx_slide">
	<ul style="width: <? echo $strWidth; ?>;" id="<? echo $arItemIDs['SLIDER_LIST_OF_ID'].$arOneOffer['ID']; ?>">
<?
			foreach ($arOneOffer['MORE_PHOTO'] as &$arOnePhoto)
			{
?>
	<li data-value="<? echo $arOneOffer['ID'].'_'.$arOnePhoto['ID']; ?>" style="width: <? echo $strOneWidth; ?>; padding-top: <? echo $strOneWidth; ?>"><span class="cnt"><span class="cnt_item" style="background-image:url('<? echo $arOnePhoto['SRC']; ?>');"></span></span></li>
<?
			}
			unset($arOnePhoto);
?>
	</ul>
	</div>
	<div class="bx_slide_left" id="<? echo $arItemIDs['SLIDER_LEFT_OF_ID'].$arOneOffer['ID'] ?>" style="<? echo $strSlideStyle; ?>" data-value="<? echo $arOneOffer['ID']; ?>"></div>
	<div class="bx_slide_right" id="<? echo $arItemIDs['SLIDER_RIGHT_OF_ID'].$arOneOffer['ID'] ?>" style="<? echo $strSlideStyle; ?>" data-value="<? echo $arOneOffer['ID']; ?>"></div>
	</div>
	</div>
<?
		}
	}
}
?>
</div>
		</div>
		<div class="bx_rt">
<?
$useBrands = ('Y' == $arParams['BRAND_USE']);
$useVoteRating = ('Y' == $arParams['USE_VOTE_RATING']);
if ($useBrands || $useVoteRating)
{
?>
	<div class="bx_optionblock">
<?
	if ($useVoteRating)
	{
		?><?$APPLICATION->IncludeComponent(
			"bitrix:iblock.vote",
			"stars",
			array(
				"IBLOCK_TYPE" => $arParams['IBLOCK_TYPE'],
				"IBLOCK_ID" => $arParams['IBLOCK_ID'],
				"ELEMENT_ID" => $arResult['ID'],
				"ELEMENT_CODE" => "",
				"MAX_VOTE" => "5",
				"VOTE_NAMES" => array("1", "2", "3", "4", "5"),
				"SET_STATUS_404" => "N",
				"DISPLAY_AS_RATING" => $arParams['VOTE_DISPLAY_AS_RATING'],
				"CACHE_TYPE" => $arParams['CACHE_TYPE'],
				"CACHE_TIME" => $arParams['CACHE_TIME']
			),
			$component,
			array("HIDE_ICONS" => "Y")
		);?><?
	}
	if ($useBrands)
	{
		?><?$APPLICATION->IncludeComponent("bitrix:catalog.brandblock", ".default", array(
			"IBLOCK_TYPE" => $arParams['IBLOCK_TYPE'],
			"IBLOCK_ID" => $arParams['IBLOCK_ID'],
			"ELEMENT_ID" => $arResult['ID'],
			"ELEMENT_CODE" => "",
			"PROP_CODE" => $arParams['BRAND_PROP_CODE'],
			"CACHE_TYPE" => $arParams['CACHE_TYPE'],
			"CACHE_TIME" => $arParams['CACHE_TIME'],
			"CACHE_GROUPS" => $arParams['CACHE_GROUPS'],
			"WIDTH" => "",
			"HEIGHT" => ""
			),
			$component,
			array("HIDE_ICONS" => "Y")
		);?><?
	}
?>
	</div>
<?
}
unset($useVoteRating, $useBrands);
?>
<div class="item_price">
<?
$minPrice = (isset($arResult['RATIO_PRICE']) ? $arResult['RATIO_PRICE'] : $arResult['MIN_PRICE']);
$boolDiscountShow = (0 < $minPrice['DISCOUNT_DIFF']);
?>
	<div class="item_old_price" id="<? echo $arItemIDs['OLD_PRICE']; ?>" style="display: <? echo ($boolDiscountShow ? '' : 'none'); ?>"><? echo ($boolDiscountShow ? $minPrice['PRINT_VALUE'] : ''); ?></div>
	<div class="item_current_price" id="<? echo $arItemIDs['PRICE']; ?>"><? echo $minPrice['PRINT_DISCOUNT_VALUE']; ?></div>
	<div class="item_economy_price" id="<? echo $arItemIDs['DISCOUNT_PRICE']; ?>" style="display: <? echo ($boolDiscountShow ? '' : 'none'); ?>"><? echo ($boolDiscountShow ? GetMessage('CT_BCE_CATALOG_ECONOMY_INFO', array('#ECONOMY#' => $minPrice['PRINT_DISCOUNT_DIFF'])) : ''); ?></div>
</div>
<?
unset($minPrice);
if (!empty($arResult['DISPLAY_PROPERTIES']) || $arResult['SHOW_OFFERS_PROPS'])
{
?>
<div class="item_info_section">
<?
	if (!empty($arResult['DISPLAY_PROPERTIES']))
	{
?>
	<dl>
<?
		foreach ($arResult['DISPLAY_PROPERTIES'] as &$arOneProp)
		{
?>
		<dt><? echo $arOneProp['NAME']; ?></dt><dd><?
			echo (
				is_array($arOneProp['DISPLAY_VALUE'])
				? implode(' / ', $arOneProp['DISPLAY_VALUE'])
				: $arOneProp['DISPLAY_VALUE']
			); ?></dd><?
		}
		unset($arOneProp);
?>
	</dl>
<?
	}
	if ($arResult['SHOW_OFFERS_PROPS'])
	{
?>
	<dl id="<? echo $arItemIDs['DISPLAY_PROP_DIV'] ?>" style="display: none;"></dl>
<?
	}
?>
</div>
<?
}
if ('' != $arResult['PREVIEW_TEXT'])
{
	if (
		'S' == $arParams['DISPLAY_PREVIEW_TEXT_MODE']
		|| ('E' == $arParams['DISPLAY_PREVIEW_TEXT_MODE'] && '' == $arResult['DETAIL_TEXT'])
	)
	{
?>
<div class="item_info_section">
<?
		echo ('html' == $arResult['PREVIEW_TEXT_TYPE'] ? $arResult['PREVIEW_TEXT'] : '<p>'.$arResult['PREVIEW_TEXT'].'</p>');
?>
</div>
<?
	}
}
if (isset($arResult['OFFERS']) && !empty($arResult['OFFERS']) && !empty($arResult['OFFERS_PROP']))
{
	$arSkuProps = array();
?>
<div class="item_info_section" style="padding-right:150px;" id="<? echo $arItemIDs['PROP_DIV']; ?>">
<?
	foreach ($arResult['SKU_PROPS'] as &$arProp)
	{
		if (!isset($arResult['OFFERS_PROP'][$arProp['CODE']]))
			continue;
		$arSkuProps[] = array(
			'ID' => $arProp['ID'],
			'SHOW_MODE' => $arProp['SHOW_MODE'],
			'VALUES_COUNT' => $arProp['VALUES_COUNT']
		);
		if ('TEXT' == $arProp['SHOW_MODE'])
		{
			if (5 < $arProp['VALUES_COUNT'])
			{
				$strClass = 'bx_item_detail_size full';
				$strOneWidth = (100/$arProp['VALUES_COUNT']).'%';
				$strWidth = (20*$arProp['VALUES_COUNT']).'%';
				$strSlideStyle = '';
			}
			else
			{
				$strClass = 'bx_item_detail_size';
				$strOneWidth = '20%';
				$strWidth = '100%';
				$strSlideStyle = 'display: none;';
			}
?>
	<div class="<? echo $strClass; ?>" id="<? echo $arItemIDs['PROP'].$arProp['ID']; ?>_cont">
		<span class="bx_item_section_name_gray"><? echo htmlspecialcharsex($arProp['NAME']); ?></span>
		<div class="bx_size_scroller_container"><div class="bx_size">
			<ul id="<? echo $arItemIDs['PROP'].$arProp['ID']; ?>_list" style="width: <? echo $strWidth; ?>;margin-left:0%;">
<?
			foreach ($arProp['VALUES'] as $arOneValue)
			{
				$arOneValue['NAME'] = htmlspecialcharsbx($arOneValue['NAME']);
?>
<li data-treevalue="<? echo $arProp['ID'].'_'.$arOneValue['ID']; ?>" data-onevalue="<? echo $arOneValue['ID']; ?>" style="width: <? echo $strOneWidth; ?>; display: none;">
<i title="<? echo $arOneValue['NAME']; ?>"></i><span class="cnt" title="<? echo $arOneValue['NAME']; ?>"><? echo $arOneValue['NAME']; ?></span></li>
<?
			}
?>
			</ul>
			</div>
			<div class="bx_slide_left" style="<? echo $strSlideStyle; ?>" id="<? echo $arItemIDs['PROP'].$arProp['ID']; ?>_left" data-treevalue="<? echo $arProp['ID']; ?>"></div>
			<div class="bx_slide_right" style="<? echo $strSlideStyle; ?>" id="<? echo $arItemIDs['PROP'].$arProp['ID']; ?>_right" data-treevalue="<? echo $arProp['ID']; ?>"></div>
		</div>
	</div>
<?
		}
		elseif ('PICT' == $arProp['SHOW_MODE'])
		{
			if (5 < $arProp['VALUES_COUNT'])
			{
				$strClass = 'bx_item_detail_scu full';
				$strOneWidth = (100/$arProp['VALUES_COUNT']).'%';
				$strWidth = (20*$arProp['VALUES_COUNT']).'%';
				$strSlideStyle = '';
			}
			else
			{
				$strClass = 'bx_item_detail_scu';
				$strOneWidth = '20%';
				$strWidth = '100%';
				$strSlideStyle = 'display: none;';
			}
?>
	<div class="<? echo $strClass; ?>" id="<? echo $arItemIDs['PROP'].$arProp['ID']; ?>_cont">
		<span class="bx_item_section_name_gray"><? echo htmlspecialcharsex($arProp['NAME']); ?></span>
		<div class="bx_scu_scroller_container"><div class="bx_scu">
			<ul id="<? echo $arItemIDs['PROP'].$arProp['ID']; ?>_list" style="width: <? echo $strWidth; ?>;margin-left:0%;">
<?
			foreach ($arProp['VALUES'] as $arOneValue)
			{
				$arOneValue['NAME'] = htmlspecialcharsbx($arOneValue['NAME']);
?>
<li data-treevalue="<? echo $arProp['ID'].'_'.$arOneValue['ID'] ?>" data-onevalue="<? echo $arOneValue['ID']; ?>" style="width: <? echo $strOneWidth; ?>; padding-top: <? echo $strOneWidth; ?>; display: none;" >
<i title="<? echo $arOneValue['NAME']; ?>"></i>
<span class="cnt"><span class="cnt_item" style="background-image:url('<? echo $arOneValue['PICT']['SRC']; ?>');" title="<? echo $arOneValue['NAME']; ?>"></span></span></li>
<?
			}
?>
			</ul>
			</div>
			<div class="bx_slide_left" style="<? echo $strSlideStyle; ?>" id="<? echo $arItemIDs['PROP'].$arProp['ID']; ?>_left" data-treevalue="<? echo $arProp['ID']; ?>"></div>
			<div class="bx_slide_right" style="<? echo $strSlideStyle; ?>" id="<? echo $arItemIDs['PROP'].$arProp['ID']; ?>_right" data-treevalue="<? echo $arProp['ID']; ?>"></div>
		</div>
	</div>
<?
		}
	}
	unset($arProp);
?>
</div>
<?
}
?>
<div class="item_info_section">
<?
if (isset($arResult['OFFERS']) && !empty($arResult['OFFERS']))
{
	$canBuy = $arResult['OFFERS'][$arResult['OFFERS_SELECTED']]['CAN_BUY'];
}
else
{
	$canBuy = $arResult['CAN_BUY'];
}
$buyBtnMessage = ($arParams['MESS_BTN_BUY'] != '' ? $arParams['MESS_BTN_BUY'] : GetMessage('CT_BCE_CATALOG_BUY'));
$addToBasketBtnMessage = ($arParams['MESS_BTN_ADD_TO_BASKET'] != '' ? $arParams['MESS_BTN_ADD_TO_BASKET'] : GetMessage('CT_BCE_CATALOG_ADD'));
$notAvailableMessage = ($arParams['MESS_NOT_AVAILABLE'] != '' ? $arParams['MESS_NOT_AVAILABLE'] : GetMessageJS('CT_BCE_CATALOG_NOT_AVAILABLE'));
$showBuyBtn = in_array('BUY', $arParams['ADD_TO_BASKET_ACTION']);
$showAddBtn = in_array('ADD', $arParams['ADD_TO_BASKET_ACTION']);

$showSubscribeBtn = false;
$compareBtnMessage = ($arParams['MESS_BTN_COMPARE'] != '' ? $arParams['MESS_BTN_COMPARE'] : GetMessage('CT_BCE_CATALOG_COMPARE'));

if ($arParams['USE_PRODUCT_QUANTITY'] == 'Y')
{
	if ($arParams['SHOW_BASIS_PRICE'] == 'Y')
	{
		$basisPriceInfo = array(
			'#PRICE#' => $arResult['MIN_BASIS_PRICE']['PRINT_DISCOUNT_VALUE'],
			'#MEASURE#' => (isset($arResult['CATALOG_MEASURE_NAME']) ? $arResult['CATALOG_MEASURE_NAME'] : '')
		);
?>
		<p id="<? echo $arItemIDs['BASIS_PRICE']; ?>" class="item_section_name_gray"><? echo GetMessage('CT_BCE_CATALOG_MESS_BASIS_PRICE', $basisPriceInfo); ?></p>
<?
	}
?>
	<span class="item_section_name_gray"><? echo GetMessage('CATALOG_QUANTITY'); ?></span>
	<div class="item_buttons vam">
		<span class="item_buttons_counter_block">
			<a href="javascript:void(0)" class="bx_bt_button_type_2 bx_small bx_fwb" id="<? echo $arItemIDs['QUANTITY_DOWN']; ?>">-</a>
			<input id="<? echo $arItemIDs['QUANTITY']; ?>" type="text" class="tac transparent_input" value="<? echo (isset($arResult['OFFERS']) && !empty($arResult['OFFERS'])
					? 1
					: $arResult['CATALOG_MEASURE_RATIO']
				); ?>">
			<a href="javascript:void(0)" class="bx_bt_button_type_2 bx_small bx_fwb" id="<? echo $arItemIDs['QUANTITY_UP']; ?>">+</a>
			<span class="bx_cnt_desc" id="<? echo $arItemIDs['QUANTITY_MEASURE']; ?>"><? echo (isset($arResult['CATALOG_MEASURE_NAME']) ? $arResult['CATALOG_MEASURE_NAME'] : ''); ?></span>
		</span>
		<span class="item_buttons_counter_block" id="<? echo $arItemIDs['BASKET_ACTIONS']; ?>" style="display: <? echo ($canBuy ? '' : 'none'); ?>;">
<?
	if ($showBuyBtn)
	{
?>
			<a href="javascript:void(0);" class="bx_big bx_bt_button bx_cart" id="<? echo $arItemIDs['BUY_LINK']; ?>"><span></span><? echo $buyBtnMessage; ?></a>
<?
	}
	if ($showAddBtn)
	{
?>
			<a href="javascript:void(0);" class="bx_big bx_bt_button bx_cart" id="<? echo $arItemIDs['ADD_BASKET_LINK']; ?>"><span></span><? echo $addToBasketBtnMessage; ?></a>
<?
	}
?>
		</span>
		<span id="<? echo $arItemIDs['NOT_AVAILABLE_MESS']; ?>" class="bx_notavailable" style="display: <? echo (!$canBuy ? '' : 'none'); ?>;"><? echo $notAvailableMessage; ?></span>
<?
	if ($arParams['DISPLAY_COMPARE'] || $showSubscribeBtn)
	{
?>
		<span class="item_buttons_counter_block">
<?
		if ($arParams['DISPLAY_COMPARE'])
		{
?>
			<a href="javascript:void(0);" class="bx_big bx_bt_button_type_2 bx_cart" id="<? echo $arItemIDs['COMPARE_LINK']; ?>"><? echo $compareBtnMessage; ?></a>
<?
		}
		if ($showSubscribeBtn)
		{

		}
?>
		</span>
<?
	}
?>
	</div>
<?
	if ('Y' == $arParams['SHOW_MAX_QUANTITY'])
	{
		if (isset($arResult['OFFERS']) && !empty($arResult['OFFERS']))
		{
?>
	<p id="<? echo $arItemIDs['QUANTITY_LIMIT']; ?>" style="display: none;"><? echo GetMessage('OSTATOK'); ?>: <span></span></p>
<?
		}
		else
		{
			if ('Y' == $arResult['CATALOG_QUANTITY_TRACE'] && 'N' == $arResult['CATALOG_CAN_BUY_ZERO'])
			{
?>
	<p id="<? echo $arItemIDs['QUANTITY_LIMIT']; ?>"><? echo GetMessage('OSTATOK'); ?>: <span><? echo $arResult['CATALOG_QUANTITY']; ?></span></p>
<?
			}
		}
	}
}
else
{
?>
	<div class="item_buttons vam">
		<span class="item_buttons_counter_block" id="<? echo $arItemIDs['BASKET_ACTIONS']; ?>" style="display: <? echo ($canBuy ? '' : 'none'); ?>;">
<?
	if ($showBuyBtn)
	{
?>
			<a href="javascript:void(0);" class="bx_big bx_bt_button bx_cart" id="<? echo $arItemIDs['BUY_LINK']; ?>"><span></span><? echo $buyBtnMessage; ?></a>
<?
	}
	if ($showAddBtn)
	{
?>
		<a href="javascript:void(0);" class="bx_big bx_bt_button bx_cart" id="<? echo $arItemIDs['ADD_BASKET_LINK']; ?>"><span></span><? echo $addToBasketBtnMessage; ?></a>
<?
	}
?>
		</span>
		<span id="<? echo $arItemIDs['NOT_AVAILABLE_MESS']; ?>" class="bx_notavailable" style="display: <? echo (!$canBuy ? '' : 'none'); ?>;"><? echo $notAvailableMessage; ?></span>
<?
	if ($arParams['DISPLAY_COMPARE'] || $showSubscribeBtn)
	{
		?>
		<span class="item_buttons_counter_block">
	<?
	if ($arParams['DISPLAY_COMPARE'])
	{
		?>
		<a href="javascript:void(0);" class="bx_big bx_bt_button_type_2 bx_cart" id="<? echo $arItemIDs['COMPARE_LINK']; ?>"><? echo $compareBtnMessage; ?></a>
	<?
	}
	if ($showSubscribeBtn)
	{

	}
?>
		</span>
<?
	}
?>
	</div>
<?
}
unset($showAddBtn, $showBuyBtn);
?>
</div>
			<div class="clb"></div>
		</div>

		<div class="bx_md">
<div class="item_info_section">
<?
if (isset($arResult['OFFERS']) && !empty($arResult['OFFERS']))
{
	if ($arResult['OFFER_GROUP'])
	{
		foreach ($arResult['OFFER_GROUP_VALUES'] as $offerID)
		{
?>
	<span id="<? echo $arItemIDs['OFFER_GROUP'].$offerID; ?>" style="display: none;">
<?$APPLICATION->IncludeComponent("bitrix:catalog.set.constructor",
	".default",
	array(
		"IBLOCK_ID" => $arResult["OFFERS_IBLOCK"],
		"ELEMENT_ID" => $offerID,
		"PRICE_CODE" => $arParams["PRICE_CODE"],
		"BASKET_URL" => $arParams["BASKET_URL"],
		"OFFERS_CART_PROPERTIES" => $arParams["OFFERS_CART_PROPERTIES"],
		"CACHE_TYPE" => $arParams["CACHE_TYPE"],
		"CACHE_TIME" => $arParams["CACHE_TIME"],
		"CACHE_GROUPS" => $arParams["CACHE_GROUPS"],
		"TEMPLATE_THEME" => $arParams['~TEMPLATE_THEME'],
		"CONVERT_CURRENCY" => $arParams['CONVERT_CURRENCY'],
		"CURRENCY_ID" => $arParams["CURRENCY_ID"]
	),
	$component,
	array("HIDE_ICONS" => "Y")
);?><?
?>
	</span>
<?
		}
	}
}
else
{
	if ($arResult['MODULES']['catalog'] && $arResult['OFFER_GROUP'])
	{
?><?$APPLICATION->IncludeComponent("bitrix:catalog.set.constructor",
	".default",
	array(
		"IBLOCK_ID" => $arParams["IBLOCK_ID"],
		"ELEMENT_ID" => $arResult["ID"],
		"PRICE_CODE" => $arParams["PRICE_CODE"],
		"BASKET_URL" => $arParams["BASKET_URL"],
		"CACHE_TYPE" => $arParams["CACHE_TYPE"],
		"CACHE_TIME" => $arParams["CACHE_TIME"],
		"CACHE_GROUPS" => $arParams["CACHE_GROUPS"],
		"TEMPLATE_THEME" => $arParams['~TEMPLATE_THEME'],
		"CONVERT_CURRENCY" => $arParams['CONVERT_CURRENCY'],
		"CURRENCY_ID" => $arParams["CURRENCY_ID"]
	),
	$component,
	array("HIDE_ICONS" => "Y")
);?><?
	}
}
?>
</div>
		</div>
		<div class="bx_rb">
<div class="item_info_section">
<?
if ('' != $arResult['DETAIL_TEXT'])
{
?>
	<div class="bx_item_description">
		<div class="bx_item_section_name_gray" style="border-bottom: 1px solid #f2f2f2;"><? echo GetMessage('FULL_DESCRIPTION'); ?></div>
<?
	if ('html' == $arResult['DETAIL_TEXT_TYPE'])
	{
		echo $arResult['DETAIL_TEXT'];
	}
	else
	{
		?><p><? echo $arResult['DETAIL_TEXT']; ?></p><?
	}
?>
	</div>
<?
}
?>
</div>
		</div>
		<div class="bx_lb">
<div class="tac ovh">
</div>
<div class="tab-section-container">
<?
if ('Y' == $arParams['USE_COMMENTS'])
{
?>
<?$APPLICATION->IncludeComponent(
	"bitrix:catalog.comments",
	"",
	array(
		"ELEMENT_ID" => $arResult['ID'],
		"ELEMENT_CODE" => "",
		"IBLOCK_ID" => $arParams['IBLOCK_ID'],
		"SHOW_DEACTIVATED" => $arParams['SHOW_DEACTIVATED'],
		"URL_TO_COMMENT" => "",
		"WIDTH" => "",
		"COMMENTS_COUNT" => "5",
		"BLOG_USE" => $arParams['BLOG_USE'],
		"FB_USE" => $arParams['FB_USE'],
		"FB_APP_ID" => $arParams['FB_APP_ID'],
		"VK_USE" => $arParams['VK_USE'],
		"VK_API_ID" => $arParams['VK_API_ID'],
		"CACHE_TYPE" => $arParams['CACHE_TYPE'],
		"CACHE_TIME" => $arParams['CACHE_TIME'],
		'CACHE_GROUPS' => $arParams['CACHE_GROUPS'],
		"BLOG_TITLE" => "",
		"BLOG_URL" => $arParams['BLOG_URL'],
		"PATH_TO_SMILE" => "",
		"EMAIL_NOTIFY" => $arParams['BLOG_EMAIL_NOTIFY'],
		"AJAX_POST" => "Y",
		"SHOW_SPAM" => "Y",
		"SHOW_RATING" => "N",
		"FB_TITLE" => "",
		"FB_USER_ADMIN_ID" => "",
		"FB_COLORSCHEME" => "light",
		"FB_ORDER_BY" => "reverse_time",
		"VK_TITLE" => "",
		"TEMPLATE_THEME" => $arParams['~TEMPLATE_THEME']
	),
	$component,
	array("HIDE_ICONS" => "Y")
);?>
<?
}
?>
</div>
		</div>
			<div style="clear: both;"></div>
	</div>
	<div class="clb"></div>
</div><?
if (isset($arResult['OFFERS']) && !empty($arResult['OFFERS']))
{
	foreach ($arResult['JS_OFFERS'] as &$arOneJS)
	{
		if ($arOneJS['PRICE']['DISCOUNT_VALUE'] != $arOneJS['PRICE']['VALUE'])
		{
			$arOneJS['PRICE']['DISCOUNT_DIFF_PERCENT'] = -$arOneJS['PRICE']['DISCOUNT_DIFF_PERCENT'];
			$arOneJS['BASIS_PRICE']['DISCOUNT_DIFF_PERCENT'] = -$arOneJS['BASIS_PRICE']['DISCOUNT_DIFF_PERCENT'];
		}
		$strProps = '';
		if ($arResult['SHOW_OFFERS_PROPS'])
		{
			if (!empty($arOneJS['DISPLAY_PROPERTIES']))
			{
				foreach ($arOneJS['DISPLAY_PROPERTIES'] as $arOneProp)
				{
					$strProps .= '<dt>'.$arOneProp['NAME'].'</dt><dd>'.(
						is_array($arOneProp['VALUE'])
						? implode(' / ', $arOneProp['VALUE'])
						: $arOneProp['VALUE']
					).'</dd>';
				}
			}
		}
		$arOneJS['DISPLAY_PROPERTIES'] = $strProps;
	}
	if (isset($arOneJS))
		unset($arOneJS);
	$arJSParams = array(
		'CONFIG' => array(
			'USE_CATALOG' => $arResult['CATALOG'],
			'SHOW_QUANTITY' => $arParams['USE_PRODUCT_QUANTITY'],
			'SHOW_PRICE' => true,
			'SHOW_DISCOUNT_PERCENT' => ($arParams['SHOW_DISCOUNT_PERCENT'] == 'Y'),
			'SHOW_OLD_PRICE' => ($arParams['SHOW_OLD_PRICE'] == 'Y'),
			'DISPLAY_COMPARE' => $arParams['DISPLAY_COMPARE'],
			'SHOW_SKU_PROPS' => $arResult['SHOW_OFFERS_PROPS'],
			'OFFER_GROUP' => $arResult['OFFER_GROUP'],
			'MAIN_PICTURE_MODE' => $arParams['DETAIL_PICTURE_MODE'],
			'SHOW_BASIS_PRICE' => ($arParams['SHOW_BASIS_PRICE'] == 'Y'),
			'ADD_TO_BASKET_ACTION' => $arParams['ADD_TO_BASKET_ACTION'],
			'SHOW_CLOSE_POPUP' => ($arParams['SHOW_CLOSE_POPUP'] == 'Y')
		),
		'PRODUCT_TYPE' => $arResult['CATALOG_TYPE'],
		'VISUAL' => array(
			'ID' => $arItemIDs['ID'],
		),
		'DEFAULT_PICTURE' => array(
			'PREVIEW_PICTURE' => $arResult['DEFAULT_PICTURE'],
			'DETAIL_PICTURE' => $arResult['DEFAULT_PICTURE']
		),
		'PRODUCT' => array(
			'ID' => $arResult['ID'],
			'NAME' => $arResult['~NAME']
		),
		'BASKET' => array(
			'QUANTITY' => $arParams['PRODUCT_QUANTITY_VARIABLE'],
			'BASKET_URL' => $arParams['BASKET_URL'],
			'SKU_PROPS' => $arResult['OFFERS_PROP_CODES'],
			'ADD_URL_TEMPLATE' => $arResult['~ADD_URL_TEMPLATE'],
			'BUY_URL_TEMPLATE' => $arResult['~BUY_URL_TEMPLATE']
		),
		'OFFERS' => $arResult['JS_OFFERS'],
		'OFFER_SELECTED' => $arResult['OFFERS_SELECTED'],
		'TREE_PROPS' => $arSkuProps
	);
	if ($arParams['DISPLAY_COMPARE'])
	{
		$arJSParams['COMPARE'] = array(
			'COMPARE_URL_TEMPLATE' => $arResult['~COMPARE_URL_TEMPLATE'],
			'COMPARE_PATH' => $arParams['COMPARE_PATH']
		);
	}
}
else
{
	$emptyProductProperties = empty($arResult['PRODUCT_PROPERTIES']);
	if ('Y' == $arParams['ADD_PROPERTIES_TO_BASKET'] && !$emptyProductProperties)
	{
?>
<div id="<? echo $arItemIDs['BASKET_PROP_DIV']; ?>" style="display: none;">
<?
		if (!empty($arResult['PRODUCT_PROPERTIES_FILL']))
		{
			foreach ($arResult['PRODUCT_PROPERTIES_FILL'] as $propID => $propInfo)
			{
?>
	<input type="hidden" name="<? echo $arParams['PRODUCT_PROPS_VARIABLE']; ?>[<? echo $propID; ?>]" value="<? echo htmlspecialcharsbx($propInfo['ID']); ?>">
<?
				if (isset($arResult['PRODUCT_PROPERTIES'][$propID]))
					unset($arResult['PRODUCT_PROPERTIES'][$propID]);
			}
		}
		$emptyProductProperties = empty($arResult['PRODUCT_PROPERTIES']);
		if (!$emptyProductProperties)
		{
?>
	<table>
<?
			foreach ($arResult['PRODUCT_PROPERTIES'] as $propID => $propInfo)
			{
?>
	<tr><td><? echo $arResult['PROPERTIES'][$propID]['NAME']; ?></td>
	<td>
<?
				if(
					'L' == $arResult['PROPERTIES'][$propID]['PROPERTY_TYPE']
					&& 'C' == $arResult['PROPERTIES'][$propID]['LIST_TYPE']
				)
				{
					foreach($propInfo['VALUES'] as $valueID => $value)
					{
						?><label><input type="radio" name="<? echo $arParams['PRODUCT_PROPS_VARIABLE']; ?>[<? echo $propID; ?>]" value="<? echo $valueID; ?>" <? echo ($valueID == $propInfo['SELECTED'] ? '"checked"' : ''); ?>><? echo $value; ?></label><br><?
					}
				}
				else
				{
					?><select name="<? echo $arParams['PRODUCT_PROPS_VARIABLE']; ?>[<? echo $propID; ?>]"><?
					foreach($propInfo['VALUES'] as $valueID => $value)
					{
						?><option value="<? echo $valueID; ?>" <? echo ($valueID == $propInfo['SELECTED'] ? '"selected"' : ''); ?>><? echo $value; ?></option><?
					}
					?></select><?
				}
?>
	</td></tr>
<?
			}
?>
	</table>
<?
		}
?>
</div>
<?
	}
	if ($arResult['MIN_PRICE']['DISCOUNT_VALUE'] != $arResult['MIN_PRICE']['VALUE'])
	{
		$arResult['MIN_PRICE']['DISCOUNT_DIFF_PERCENT'] = -$arResult['MIN_PRICE']['DISCOUNT_DIFF_PERCENT'];
		$arResult['MIN_BASIS_PRICE']['DISCOUNT_DIFF_PERCENT'] = -$arResult['MIN_BASIS_PRICE']['DISCOUNT_DIFF_PERCENT'];
	}
	$arJSParams = array(
		'CONFIG' => array(
			'USE_CATALOG' => $arResult['CATALOG'],
			'SHOW_QUANTITY' => $arParams['USE_PRODUCT_QUANTITY'],
			'SHOW_PRICE' => (isset($arResult['MIN_PRICE']) && !empty($arResult['MIN_PRICE']) && is_array($arResult['MIN_PRICE'])),
			'SHOW_DISCOUNT_PERCENT' => ($arParams['SHOW_DISCOUNT_PERCENT'] == 'Y'),
			'SHOW_OLD_PRICE' => ($arParams['SHOW_OLD_PRICE'] == 'Y'),
			'DISPLAY_COMPARE' => $arParams['DISPLAY_COMPARE'],
			'MAIN_PICTURE_MODE' => $arParams['DETAIL_PICTURE_MODE'],
			'SHOW_BASIS_PRICE' => ($arParams['SHOW_BASIS_PRICE'] == 'Y'),
			'ADD_TO_BASKET_ACTION' => $arParams['ADD_TO_BASKET_ACTION'],
			'SHOW_CLOSE_POPUP' => ($arParams['SHOW_CLOSE_POPUP'] == 'Y')
		),
		'VISUAL' => array(
			'ID' => $arItemIDs['ID'],
		),
		'PRODUCT_TYPE' => $arResult['CATALOG_TYPE'],
		'PRODUCT' => array(
			'ID' => $arResult['ID'],
			'PICT' => $arFirstPhoto,
			'NAME' => $arResult['~NAME'],
			'SUBSCRIPTION' => true,
			'PRICE' => $arResult['MIN_PRICE'],
			'BASIS_PRICE' => $arResult['MIN_BASIS_PRICE'],
			'SLIDER_COUNT' => $arResult['MORE_PHOTO_COUNT'],
			'SLIDER' => $arResult['MORE_PHOTO'],
			'CAN_BUY' => $arResult['CAN_BUY'],
			'CHECK_QUANTITY' => $arResult['CHECK_QUANTITY'],
			'QUANTITY_FLOAT' => is_double($arResult['CATALOG_MEASURE_RATIO']),
			'MAX_QUANTITY' => $arResult['CATALOG_QUANTITY'],
			'STEP_QUANTITY' => $arResult['CATALOG_MEASURE_RATIO'],
		),
		'BASKET' => array(
			'ADD_PROPS' => ($arParams['ADD_PROPERTIES_TO_BASKET'] == 'Y'),
			'QUANTITY' => $arParams['PRODUCT_QUANTITY_VARIABLE'],
			'PROPS' => $arParams['PRODUCT_PROPS_VARIABLE'],
			'EMPTY_PROPS' => $emptyProductProperties,
			'BASKET_URL' => $arParams['BASKET_URL'],
			'ADD_URL_TEMPLATE' => $arResult['~ADD_URL_TEMPLATE'],
			'BUY_URL_TEMPLATE' => $arResult['~BUY_URL_TEMPLATE']
		)
	);
	if ($arParams['DISPLAY_COMPARE'])
	{
		$arJSParams['COMPARE'] = array(
			'COMPARE_URL_TEMPLATE' => $arResult['~COMPARE_URL_TEMPLATE'],
			'COMPARE_PATH' => $arParams['COMPARE_PATH']
		);
	}
	unset($emptyProductProperties);
}
?>
<script type="text/javascript">
var <? echo $strObName; ?> = new JCCatalogElement(<? echo CUtil::PhpToJSObject($arJSParams, false, true); ?>);
BX.message({
	ECONOMY_INFO_MESSAGE: '<? echo GetMessageJS('CT_BCE_CATALOG_ECONOMY_INFO'); ?>',
	BASIS_PRICE_MESSAGE: '<? echo GetMessageJS('CT_BCE_CATALOG_MESS_BASIS_PRICE') ?>',
	TITLE_ERROR: '<? echo GetMessageJS('CT_BCE_CATALOG_TITLE_ERROR') ?>',
	TITLE_BASKET_PROPS: '<? echo GetMessageJS('CT_BCE_CATALOG_TITLE_BASKET_PROPS') ?>',
	BASKET_UNKNOWN_ERROR: '<? echo GetMessageJS('CT_BCE_CATALOG_BASKET_UNKNOWN_ERROR') ?>',
	BTN_SEND_PROPS: '<? echo GetMessageJS('CT_BCE_CATALOG_BTN_SEND_PROPS'); ?>',
	BTN_MESSAGE_BASKET_REDIRECT: '<? echo GetMessageJS('CT_BCE_CATALOG_BTN_MESSAGE_BASKET_REDIRECT') ?>',
	BTN_MESSAGE_CLOSE: '<? echo GetMessageJS('CT_BCE_CATALOG_BTN_MESSAGE_CLOSE'); ?>',
	BTN_MESSAGE_CLOSE_POPUP: '<? echo GetMessageJS('CT_BCE_CATALOG_BTN_MESSAGE_CLOSE_POPUP'); ?>',
	TITLE_SUCCESSFUL: '<? echo GetMessageJS('CT_BCE_CATALOG_ADD_TO_BASKET_OK'); ?>',
	COMPARE_MESSAGE_OK: '<? echo GetMessageJS('CT_BCE_CATALOG_MESS_COMPARE_OK') ?>',
	COMPARE_UNKNOWN_ERROR: '<? echo GetMessageJS('CT_BCE_CATALOG_MESS_COMPARE_UNKNOWN_ERROR') ?>',
	COMPARE_TITLE: '<? echo GetMessageJS('CT_BCE_CATALOG_MESS_COMPARE_TITLE') ?>',
	BTN_MESSAGE_COMPARE_REDIRECT: '<? echo GetMessageJS('CT_BCE_CATALOG_BTN_MESSAGE_COMPARE_REDIRECT') ?>',
	SITE_ID: '<? echo SITE_ID; ?>'
});
</script> */
?>

<?

// p($arResult);

if (!empty($arResult['ID'])) {
	$arSelect = Array("ID", "NAME", "CODE", "XML_ID", "PROPERTY_OWNER", "PROPERTY_PARAM", "PROPERTY_PARAM.NAME", "PROPERTY_PARAM.PROPERTY_TYPE", "PROPERTY_PARAM.PROPERTY_SITE_CODE", "PROPERTY_VALUE", "PROPERTY_VALUE_LINK", "PROPERTY_VALUE_LINK.PROPERTY_NAME_FULL", "PROPERTY_VALUE_LINK.PROPERTY_FILE", "PROPERTY_VALUE_LINK.PROPERTY_FILEDATA", "PROPERTY_VALUE_LINK.PROPERTY_FILEEXT");
	$arFilter = Array("IBLOCK_ID"=>19, "PROPERTY_OWNER" => $arResult['ID']);
	$res = CIBlockElement::GetList(Array("PROPERTY_VALUE_LINK" => "ASC", "PROPERTY_PARAM.NAME" => "ASC"), $arFilter, false, false, $arSelect);
	while($arFields = $res->GetNext()) {
		if (!empty($arFields['PROPERTY_VALUE_LINK_PROPERTY_FILE_VALUE'])) {
			$arFields['PROPERTY_VALUE_LINK_PROPERTY_FILE_VALUE'] = CFile::GetFileArray($arFields['PROPERTY_VALUE_LINK_PROPERTY_FILE_VALUE']);
		}
		if (!isset($arItemsParams[$arFields['PROPERTY_PARAM_NAME']])) {
			$arItemsParams[$arFields['PROPERTY_PARAM_NAME']] = $arFields;
		}
	}
	// p($arItemsParams);

	$pic = false;
	$pic1 = false;
	if (isset($arItemsParams['Малое изображение 1']) && !empty($arItemsParams['Малое изображение 1']['PROPERTY_VALUE_LINK_PROPERTY_FILE_VALUE']['SRC'])) {
		$pic = $arItemsParams['Малое изображение 1']['PROPERTY_VALUE_LINK_PROPERTY_FILE_VALUE']['SRC'];
	}
	if (isset($arItemsParams['Малое изображение 2']) && !empty($arItemsParams['Малое изображение 2']['PROPERTY_VALUE_LINK_PROPERTY_FILE_VALUE']['SRC'])) {
		$pic01 = $arItemsParams['Малое изображение 2']['PROPERTY_VALUE_LINK_PROPERTY_FILE_VALUE']['SRC'];
	}
	if (isset($arItemsParams['Большое изображение 1']) && !empty($arItemsParams['Большое изображение 1']['PROPERTY_VALUE_LINK_PROPERTY_FILE_VALUE']['SRC'])) {
		$pic1 = $arItemsParams['Большое изображение 1']['PROPERTY_VALUE_LINK_PROPERTY_FILE_VALUE']['SRC'];
	}
	if (isset($arItemsParams['Большое изображение 2']) && !empty($arItemsParams['Большое изображение 2']['PROPERTY_VALUE_LINK_PROPERTY_FILE_VALUE']['SRC'])) {
		$pic001 = $arItemsParams['Большое изображение 2']['PROPERTY_VALUE_LINK_PROPERTY_FILE_VALUE']['SRC'];
	}
}

$badProps = [];
$badProps[] = 'SEO description';
$badProps[] = 'SEO keywords';
$badProps[] = 'SEO title';



?>

<!-- card -->
<div class="card c">
	<!-- left column holder -->
	<div class="col-l">
		<!-- left column w -->
		<div class="w brd c c-pic-h">
			<!-- row 1 -->
			<div class="row-1 c">
				<h1 class="ttl"><?= $arResult['NAME'] ?></h1>
				<!--<a href="#" class="brand-logo pic" style="background-image:url('<?= SITE_TEMPLATE_PATH ?>/i/blocks/logo-ballu.png');"></a>-->
				<a href="#" class="brand-logo pic"><?= strip_tags($arResult['DISPLAY_PROPERTIES']['BRAND']['DISPLAY_VALUE']) ?></a>
			</div>
			<!-- / row 1 end -->

			<!-- row 2 -->
			<div class="row-2 c">
				<div class="f-l">
					<div class="rate">
						<div class="stars"></div>
						<a href="#" class="lnk">Отзывов: 0</a>
					</div>
					<div class="article">код товара: <span><?= $arResult['XML_ID'] == $arResult['ID'] ? "" : $arResult['XML_ID'] ?></span></div>
				</div>
				<!--
				<div class="chars">
					<div class="item brd">
						<div class="w c">
							<span class="nmb">40 м²</span>
							<span class="dsc">площадь<br/>помещения</span>
						</div>
					</div>
					<div class="item brd">
						<div class="w c">
							<span class="nmb">56°</span>
							<span class="dsc">температурный<br/>режим</span>
						</div>
					</div>
				</div>
				-->
			</div>
			<!-- / rowr 2 end -->

			<!-- pic container -->
			<a href="#" class="c-pic-container maximize">

				<!--
				<div class="icos">
					<div class="ico">
						<span>Тонкая<br/>очистка<br/>воздуха</span>
					</div>
					<div class="ico tw-l">
						<span>удобное<br/>управление</span>
					</div>
				</div>-->
				<div class="c-pic pic" id="cardGal-Pic"<? if(!empty($pic1)): ?> style="background-image:url('<?= $pic1 ?>');"<? endif; ?>>
				</div>

			</a>
			<!-- pic container end -->

			<!-- pic nav -->
			<ul class="c-gal c" id="cardGal-Nav">
				<li <? if(!empty($pic)): ?> style="background-image:url('<?= $pic ?>'); background-size: contain; background-repeat: no-repeat; background-position: center;"<? endif; ?>><? if(!empty($pic1)): ?><a class="active" href="<?= $pic1 ?>"></a><? endif; ?></li>
				<? if(!empty($pic01)): ?>
					<li <? if(!empty($pic01)): ?> style="background-image:url('<?= $pic01 ?>'); background-size: contain; background-repeat: no-repeat; background-position: center;"<? endif; ?>><? if(!empty($pic001)): ?><a href="<?= $pic001 ?>"></a><? endif; ?></li>
				<? endif; ?>
			</ul>
			<!-- pic nav end -->

		</div>
		<!-- / left column w end -->

		<!-- tabs -->
		<div class="tabs" id="cardTabs">
			<!-- tabs labels -->
			<ul class="tabs-ul c">
				<li class="li-tab-desc"><a href="#tabDesc" data-tab-id="tabDesc"><span><h2>Описание</h2></span></a></li>
				<li class="li-tab-char"><a href="#tabChar" data-tab-id="tabChar"><span><h2>Характеристики</h2></span></a></li>
				<li class="li-tab-inst"><a href="#tabInst" data-tab-id="tabInst"><span><h3>Инструкции</h3></span></a></li>
				<li class="li-tab-revw"><a href="#tabRevw" data-tab-id="tabRevw"><span><h2>Отзывы (0)</h2></span></a></li>
				<li class="li-tab-avai li-two-lines"><a href="#tabAvai" data-tab-id="tabAvai"><span><h3>Доставка и<br>оплата</span></h3></a></li>
			</ul>
			<!-- / tabs labels end -->

			<!-- tabs content w -->
			<div class="w brd c">

				<!-- description -->
				<div class="tab tab-desc" id="tabDesc">
					<? if (isset($arItemsParams['html описания товара']) && !empty($arItemsParams['html описания товара']['~PROPERTY_VALUE_LINK_PROPERTY_FILEDATA_VALUE']['TEXT'])): ?>
						<?= $arItemsParams['html описания товара']['~PROPERTY_VALUE_LINK_PROPERTY_FILEDATA_VALUE']['TEXT'] ?>
					<? endif; ?>
				</div>
				<!-- / description end -->

				<!-- characteristics -->
				<div class="tab tab-char" id="tabChar">
					<!-- tbl-char -->
					<table class="tbl tbl-char">
						<?
						$was = false;
						?>
						<? foreach($arItemsParams as $param): ?>
							<?
							if (in_array($param['PROPERTY_PARAM_NAME'], $badProps) || !empty($param['PROPERTY_VALUE_LINK_VALUE'])) {
								continue;
							}
							?>
							<? if($was): ?>
								<tr class="sp">
									<td colspan="2">&nbsp;</td>
								</tr>
							<? else: ?>
								<?
								$was = true;
								?>
							<? endif; ?>
							<tr>
								<td><?= $param['PROPERTY_PARAM_NAME'] ?></td>
								<td style="width:160px"><?= strlen($param['PROPERTY_VALUE_VALUE']) > 0 ? $param['PROPERTY_VALUE_VALUE'] : "[ пустое значение ]" ?></td>
							</tr>
						<? endforeach; ?>
					</table>
					<!-- / tbl-char end -->

				</div>
				<!-- / characteristics end -->

				<!-- instructions -->
				<div class="tab tab-inst" id="tabInst">
					<?
					/*
					<!-- link -->
					<div class="item">
						<p class="lnk"><a href="#" target="_blank">Руководство по эксплуатации мобидьных кондиционеров Ballu BPHS серии Platinum</a></p>
						<div class="dsc c"><div class="type">PDF</div>2.3 MB</div>
					</div>
					<!-- / link -->
					<!-- link -->
					<div class="item">
						<p class="lnk"><a href="#" target="_blank">Руководство по эксплуатации моби Ballu BPHS серии Platinum</a></p>
						<div class="dsc c"><div class="type">PDF</div>2.3 MB</div>
					</div>
					<!-- / link -->
					<!-- link -->
					<div class="item">
						<p class="lnk"><a href="#" target="_blank">Руководство о по эксплуатации мобидьных кондиционерово по эксплуатации мобидьных кондиционеров Ballu BPHS серии Platinum</a></p>
						<div class="dsc c"><div class="type">PDF</div>2.3 MB</div>
					</div>
					<!-- / link -->
					<!-- link -->
					<div class="item">
						<p class="lnk"><a href="#" target="_blank">Руководство по ии Platinum</a></p>
						<div class="dsc c"><div class="type">PDF</div>2.3 MB</div>
					</div>
					<!-- / link -->
					<!-- link -->
					<div class="item">
						<p class="lnk"><a href="#" target="_blank">Руководство по эксплуатации мобидьных кондиционеров Ballu BPHS серии Platinum</a></p>
						<div class="dsc c"><div class="type">PDF</div>2.3 MB</div>
					</div>
					<!-- / link -->
					*/
					?>
				</div>
				<!-- / instructions end -->

				<!-- reviews -->
				<div class="tab tab-revw" id="tabRevw">
					<!-- add review -->
					<div class="revw-add c">
						<div class="btn btn-30 btn-revw-add">Написать отзыв</div>
						<!--<div class="count"><span>12</span> человек уже оценили этот товар по достоинству, народ рекомендует!</div>-->
					</div>
					<!-- / add review end -->

					<?
					/*
					<!-- item -->
					<div class="item">
						<div class="ttl c">
							<div class="stars"></div>
							<div class="name">Константин Константинопольский</div>
							<div class="city">Ярославль</div>
						</div>
						<div class="dsc">
							<p>Отличный кондиционер, пользуюсь 3 года, масса достоинств - мобильность, неплохо охлаждает помещение, даже при открытых дверях и частично окнах!</p>
							<p>Неплохо охлаждает помещение, даже при открытых дверях и частично окнах!</p>
						</div>
					</div>
					<!-- / item -->
					<!-- item -->
					<div class="item">
						<div class="ttl c">
							<div class="stars"></div>
							<div class="name">Евгений Ломакин</div>
							<div class="city">Астрахань</div>
						</div>
						<div class="dsc">
							<p>Еще из недостатков - предполагаю, что в случае необходимости ремонта могут быть проблемы (не слишком известный бренд, мало точек для сервиса даже в Москве, не говоря уже о других точках</p>
						</div>
					</div>
					<!-- / item -->
					<!-- item -->
					<div class="item">
						<div class="ttl c">
							<div class="stars"></div>
							<div class="name">Вероника Столетова</div>
							<div class="city">Сургут</div>
						</div>
						<div class="dsc">
							<p>Отличный кондиционер, пользуюсь 3 года, масса достоинств - мобильность, неплохо охлаждает помещение, даже при открытых дверях и частично окнах!</p>
						</div>
					</div>
					<!-- / item -->
					<!-- item -->
					<div class="item">
						<div class="ttl c">
							<div class="stars"></div>
							<div class="name">Константин Константинопольский</div>
							<div class="city">Ярославль</div>
						</div>
						<div class="dsc">
							<p>Отличный кондиционер, пользуюсь 3 года, масса достоинств - мобильность, неплохо охлаждает помещение, даже при открытых дверях и частично окнах!</p>
							<p>Неплохо охлаждает помещение, даже при открытых дверях и частично окнах!</p>
						</div>
					</div>
					<!-- / item -->

					<div class="btn btn-more btn-g btn-30">Показать еще</div>
					*/
					?>

				</div>
				<!-- / reviews end -->

				<!-- availability -->
				<div class="tab tab-avai" id="tabAvai">

					<!-- tbl-avai -->
					<table class="tbl tbl-avai">
						<tr>
							<th>Варианты доставки</th>
							<th style="width:120px">Дата доставки</th>
							<th style="width:160px">Стоимость доставки</th>
						</tr>
						<tr class="sp">
							<td colspan="3">&nbsp;</td>
						</tr>
						<tr>
							<td>Доставка Русклимат</td>
							<td>2 мая</td>
							<td>бесплатно</td>
						</tr>
						<tr class="sp">
							<td colspan="3">&nbsp;</td>
						</tr>
						<tr>
							<td>Срочная доставка DPD В течении дня с 10:00 до 22:00</td>
							<td>завтра, 17 апреля</td>
							<td>100500 руб.</td>
						</tr>
					</table>
					<!-- / tbl-avai end -->

					<!-- gmap -->
					<div class="map">
						<!-- gmap header -->
						<div class="map-hd c">
							<div class="map-city l">
								<div class="map-city-ttl">Ваш город</div>
								<select name="" id="map-select">
									<option value="">Населенный</option>
									<option value="">Пункт</option>
									<option value="">Александровск-Сахалинский</option>
									<option value="">Населенный</option>
									<option value="">Электрозаводск</option>
									<option value="">Населенный</option>
									<option value="">Населенный</option>
									<option value="">Пункт</option>
									<option value="">Александровск-Сахалинский</option>
									<option value="">Населенный</option>
									<option value="">Электрозаводск</option>
									<option value="">Населенный</option>
								</select>
							</div>
						</div>
						<!-- / gmap header end -->

						<!-- gmap holder (gmap.js) -->
						<div  id="map" class="map-container map-inner"></div>
						<!-- / gmap holder end -->

					</div>
					<!-- / gmap -->

				</div>
				<!-- / availability end -->

			</div>
			<!-- / tabs content w end-->
		</div>
		<!-- / tabs end -->

	</div>
	<!-- / left column holder end -->

	<!-- right column holder -->
	<div class="col-r">
		<!-- control -->
		<div class="ctrl">
			<!-- control w -->
			<div class="w">

				<!-- actions -->
				<div class="actions c">
					<fieldset class="checkset actionset">
						<input type="checkbox" id="addComp"><label for="addComp">Сравнить</label>
						<input type="checkbox" id="addFav"><label for="addFav">В избранное</label>
					</fieldset>
					<a href="#" class="print" title="Распечатать страницу" onclick="window.print();">Распечатать</a>
				</div>
				<!-- / actions end -->

				<!-- prices -->
					<div class="prices">
						<div class="price">0 руб.</div>
						<!--<div class="price-old">0 руб.</div>-->
				</div>
				<!-- / prices end -->

				<!-- timer -->
				<div class="timer">
					<div class="sum">Успей сэкономить 1500 рублей</div>
					<div class="time" data-countdown="2015/07/08">
					<!-- <b>11</b> <span>дн.</span> <b>11</b> <span>ч.</span> <b>11</b> <span>мин.</span> <b class="sec">11</b> <span>сек.</span> -->
					</div>
				</div>
				<!-- / timer end -->

				<!-- install -->
				<div class="install">

					<fieldset class="checkset">
						<div class="checkline">
							<input type="checkbox" id="plusInstall"><label for="plusInstall">Хочу заказать с установкой </label>
							<a href="#" title="ШТО???" class="que que-sm">?</a>
						</div>
					</fieldset>

				</div>
				<!-- / install end -->

				<!-- buy btns -->
				<div class="buy-btns">
					<div class="btn btn-to-bsk btn-50" id="btnToBasket"><span>Купить</span></div>
					<div class="btn btn-1-click btn-30 one-click-pop" id="btn1ClickBuy">купить в 1 клик</div>
					<div class="credit-pop btn btn-credit btn-30" id="btnCreditBuy">в кредит за 5 000 руб./мес.</div>
				</div>
				<!-- / buy btns end -->


				<table class="del-tbl">
					<tr>
						<td class="td1"><a class="del-instock city-pop" href="#"><span>Москва</span></a></td>
						<td class="td2">в наличии</td>
					</tr>
					<tr>
						<td class="td1"><span>Доставка</span><br/>21 апреля и позже</td>
						<td class="td2">300 руб.</td>
					</tr>
					<tr>
						<td class="td1"><a class="del-self self-pop" href="#"><span>Самовывоз</span></a><br/>Сегодня, 13 января</td>
						<td class="td2">бесплатно</td>
					</tr>
					<tr>
						<td class="td1"><span>Срочная доставка</span><br/>Позавчера, 15 января</td>
						<td class="td2">1220 руб.</td>
					</tr>
					<tr>
						<td class="td1">5% скидка при оплате online</td>
						<td class="td2">
							<div class="visa-master"></div>
						</td>
					</tr>
				</table>

				<div class="extras">

					<a href="#" class="pic item" style="background-image:url('<?= SITE_TEMPLATE_PATH ?>/i/blocks/card-offer.png');"></a>

					<div class="warning item">
						<p class="ttl">Описание состояния</p>
						<p class="dsc">потертости и мелкие царапины, упаковка в нормальном состоянии, комплект полный</p>
					</div>

				</div>


			</div>
			<!-- / control w end -->
		</div>
		<!-- / control end -->


		<div class="items hlp">
			<a class="item b-hlp callback-pop" href="#">
				<div class="w">
					<div class="ttl">Закажите звонок!</div>
					<p class="dsc">Удобная функция заказа<br/>обратного звонка.</p>
					<div class="ico ico-1">&nbsp;</div>
				</div>
			</a>
			<a class="item b-hlp" href="#">
				<div class="w">
					<div class="ttl">+7 495 777-19-77</div>
					<p class="dsc">Помогаем по любым<br/>вопросам продажи и сервиса.</p>
					<div class="ico ico-2">&nbsp;</div>
				</div>
			</a>
			<a class="item b-hlp" href="mailto:info@rusklimat.ru">
				<div class="w">
					<div class="ttl">info@rusklimat.ru</div>
					<p class="dsc">Не стесняйтесь<br/>написать нам письмо.</p>
					<div class="ico ico-3">&nbsp;</div>
				</div>
			</a>
		</div>


	</div>
	<!-- / right column holder end -->

</div>
<!-- / card end -->

<!-- series roll -->
<div class="roll">
	<h2 class="ttl-r">Все товары серии ballu i green (пока не работает)</h2>
	<div class="brd tbl-w">

		<table class="tbl tbl-series tbl-hover">
			<tr>
				<th class="col1">Фото</th>
				<th class="col2">Наименование</th>
				<th class="col3">Мощность<br/>(охлаждение)</th>
				<th class="col4">Площадь<br/>обслуживания</th>
				<th class="col5">Цена</th>
				<th class="col6">&nbsp;</th>
				<th class="col7">&nbsp;</th>
			</tr>
			<tr class="sp">
				<td colspan="7">&nbsp;</td>
			</tr>
			<tr>
				<td>
					<a href="#" class="pic" style="background-image:url('<?= SITE_TEMPLATE_PATH ?>/i/blocks/series2.png');"></a>
				</td>
				<td>
					<a href="#">Orlando DC Inverter</a>
				</td>
				<td>2.2 кВт</td>
				<td>40 м2</td>
				<td>25 990 руб.</td>
				<td>
					<fieldset class="checkset compareset">
						<input type="checkbox" class="compCheck" id="addComp4"><label for="addComp4">Сравнить</label>
					</fieldset>
				</td>
				<td>
					<div class="btn btn-30 btn-to-bsk">Купить</div>
				</td>
			</tr>
			<tr class="sp">
				<td colspan="7">&nbsp;</td>
			</tr>
			<tr class="td-hi">
				<td>
					<a href="#" class="pic" style="background-image:url('<?= SITE_TEMPLATE_PATH ?>/i/blocks/series1.png');"></a>
				</td>
				<td>
					<a href="#">EACS/l-11HO/N3 <br/>Orlando DC Inverter</a>
				</td>
				<td>2.2 кВт</td>
				<td>40 м2</td>
				<td>25 990 руб.</td>
				<td>
					<fieldset class="checkset compareset">
						<input type="checkbox" id="addComp3"><label for="addComp3">Сравнить</label>
					</fieldset>
				</td>
				<td>
					<div class="btn btn-30 btn-to-bsk">Уже в корзине</div>
				</td>
			</tr>
			<tr class="sp">
				<td colspan="7">&nbsp;</td>
			</tr>
			<tr>
				<td>
					<a href="#" class="pic" style="background-image:url('<?= SITE_TEMPLATE_PATH ?>/i/blocks/series2.png');"></a>
				</td>
				<td>
					<a href="#">Orlando DC Inverter</a>
				</td>
				<td>2.2 кВт</td>
				<td>40 м2</td>
				<td>25 990 руб.</td>
				<td>
					<fieldset class="checkset compareset">
						<input type="checkbox" id="addComp2"><label for="addComp2">Сравнить</label>
					</fieldset>
				</td>
				<td>
					<div class="btn btn-30 btn-to-bsk">Купить</div>
				</td>
			</tr>
		</table>

	</div>
</div>
<!-- / series roll end-->


<!-- other deals roll -->
<div class="roll">
	<h2 class="ttl-r">Требует вашего внимания (пока не работает)</h2>
	<!-- other deals -->
	<div class="other-deals c">
		<!-- slides -->
		<div class="slides c" id="other">

			<!-- item w/stamps -->
			<div class="item b-base">
				<div class="w c">

					<a href="card.html" class="lnk">
						<div class="ttl">Orlando сплит система система система система</div>
						<div class="pic" style="background-image:url('<?= SITE_TEMPLATE_PATH ?>/i/blocks/hot1.png');"></div>
						<div class="stamps c">
							<div class="ico ico-1">Новейшее покрытие</div>
							<div class="ico ico-2">5 лет гарантии</div>
						</div>
						<div class="dsc">Electrolux EACS/I - 13 HO/N3 серии Orlando DC Inverter система</div>
					</a>
					<div class="prc">
						<div class="curr curr-only">17 234 руб.</div>
					</div>
					<div class="btn btn-to-bsk">Купить</div>

				</div>
			</div>
			<!-- / item w/stamps -->

			<!-- item w/stamps -->
			<div class="item b-base">
				<div class="w c">

					<a href="card.html" class="lnk">
						<div class="ttl">Инверторная сплит система и еще что-то</div>
						<div class="pic" style="background-image:url('<?= SITE_TEMPLATE_PATH ?>/i/blocks/hot2.png');"></div>
						<div class="stamps c">
							<div class="ico ico-3">Ограниченная серия</div>
						</div>
						<div class="dsc">Inverter система</div>
					</a>
					<div class="prc">
						<div class="curr curr-only">690 руб.</div>
					</div>
					<div class="btn btn-to-bsk">Купить</div>

				</div>
			</div>
			<!-- / item w/stamps -->

			<!-- item w/stamps -->
			<div class="item b-base">
				<div class="w c">

					<a href="card.html" class="lnk">
						<div class="ttl">Hello World</div>
						<div class="pic" style="background-image:url('<?= SITE_TEMPLATE_PATH ?>/i/blocks/hot3.png');"></div>
						<div class="stamps c">
							<div class="ico ico-5">Лидер<br/>продаж</div>
							<div class="ico ico-1 ico-r">Новейшее<br/>покрытие</div>
						</div>
						<div class="dsc">Electrolux EACS/I - 13 HO/N3 серии Orlando DC Inverter система</div>
					</a>
					<div class="prc">
						<div class="curr curr-only">66 666 руб.</div>
					</div>
					<div class="btn btn-to-bsk">Купить</div>

				</div>
			</div>
			<!-- / item w/stamps -->

			<!-- 3 items end -->

			<!-- item w/stamps -->
			<div class="item b-base">
				<div class="w c">

					<a href="card.html" class="lnk">
						<div class="ttl">BBBBИнверторная сплит</div>
						<div class="pic" style="background-image:url('<?= SITE_TEMPLATE_PATH ?>/i/blocks/hot2.png');"></div>
						<div class="stamps c">
							<div class="ico ico-1">Новейшее<br/>покрытие</div>
							<div class="ico ico-2 ico-r">5 лет гарантии</div>
						</div>
						<div class="dsc">Electrolux EACS/Iter система</div>
					</a>
					<div class="prc">
						<div class="curr curr-only">881 690 руб.</div>
					</div>
					<div class="btn btn-to-bsk">Купить</div>

				</div>
			</div>
			<!-- / item w/stamps -->

			<!-- item w/stamps -->
			<div class="item b-base">
				<div class="w c">

					<a href="card.html" class="lnk">
						<div class="ttl">CCCИнверторная сплит система и еще что-то</div>
						<div class="pic" style="background-image:url('<?= SITE_TEMPLATE_PATH ?>/i/blocks/hot2.png');"></div>
						<div class="stamps c">
							<div class="ico ico-3">Ограниченная серия</div>
						</div>
						<div class="dsc">Orlando DC Inverter система</div>
					</a>
					<div class="prc">
						<div class="curr curr-only">1 690 руб.</div>
					</div>
					<div class="btn btn-to-bsk">Купить</div>

				</div>
			</div>
			<!-- / item w/stamps -->

			<!-- item w/stamps -->
			<div class="item b-base">
				<div class="w c">

					<a href="card.html" class="lnk">
						<div class="ttl">DDDDDИнверторная сплит система система система система</div>
						<div class="pic" style="background-image:url('<?= SITE_TEMPLATE_PATH ?>/i/blocks/hot1.png');"></div>
						<div class="stamps c">
							<div class="ico ico-5">Лидер<br/>продаж</div>
							<div class="ico ico-1 ico-r">Новейшее<br/>покрытие</div>
						</div>
						<div class="dsc">Electrolux EACS/I - 13 HO/N3 серии Orlando DC Inverter система</div>
					</a>
					<div class="prc">
						<div class="curr curr-only">8 880 руб.</div>
					</div>
					<div class="btn btn-to-bsk">Купить</div>

				</div>
			</div>
			<!-- / item w/stamps -->







		</div>
		<!-- / slides end -->
	</div>
	<!-- / other deals end -->
</div>
<!-- / other deals roll end -->


<!-- help block roll -->
<div class="roll">
	<h3 class="ttl-r">ТРЕБУЕТСЯ ПОМОЩЬ?</h3>
	<div class="help c">
		<a class="item b-hlp callback-pop" href="#">
			<div class="w">
				<div class="ttl">Закажите звонок!</div>
				<p class="dsc">Удобная функция заказа<br/>обратного звонка.</p>
				<div class="ico ico-1">&nbsp;</div>
			</div>
		</a>
		<a class="item b-hlp" href="#">
			<div class="w">
				<div class="ttl">+7 495 777-19-77</div>
				<p class="dsc">Помогаем по любым<br/>вопросам продажи и сервиса.</p>
				<div class="ico ico-2">&nbsp;</div>
			</div>
		</a>
		<a class="item b-hlp" href="mailto:info@rusklimat.ru">
			<div class="w">
				<div class="ttl">info@rusklimat.ru</div>
				<p class="dsc">Не стесняйтесь<br/>написать нам письмо.</p>
				<div class="ico ico-3">&nbsp;</div>
			</div>
		</a>
	</div>
</div>
<!-- / help block roll end -->

<!-- card popups -->

	<!-- credit popup, triggered by .credit-pop -->
	<div class="pop pop-credit pop-bg-1" id="popCredit">
		<div class="w">
			<a href="#" class="x">&nbsp;</a>
			<div class="ttl-p1">Купить в кредит просто!</div>
			<div class="ttl-p2">Всего 4 355 руб./мес.</div>
			<form action="#">
				<div class="items">
					<!-- item -->
					<div class="item brd c">
						<table>
							<tr>
								<td class="rad">
									<input name="credRadio" class="check-custom" type="radio" value="cred1" id="cred1" checked>
								</td>
								<td class="bank">
									<label class="pic" style="background-image:url('<?= SITE_TEMPLATE_PATH ?>/i/blocks/cred1.png');" for="cred1"></label>
								</td>
								<td class="dsc">
									<p>Сумма кредита до 200 000 руб.</p>
									<p>Быстрое решение без заполнения анкет.</p>
									<p>Без комиссий за открытие счета и досрочное погашение кредита.</p>
									<p>Точки погашения кредита по всей России.</p>
								</td>
							</tr>
						</table>
					</div>
					<!-- / item -->
					<!-- item -->
					<div class="item brd c">
						<table>
							<tr>
								<td class="rad">
									<input name="credRadio" class="check-custom" type="radio" value="cred2" id="cred2">
								</td>
								<td class="bank">
									<label class="pic" style="background-image:url('<?= SITE_TEMPLATE_PATH ?>/i/blocks/cred2.png');" for="cred2"></label>
								</td>
								<td class="dsc">
									<p>Сумма кредита до 200 000 руб.</p>
									<p>Быстрое решение без заполнения анкет.</p>
									<p>Без комиссий за открытие счета и досрочное погашение кредита.</p>
									<p>Точки погашения кредита по всей России.</p>
								</td>
							</tr>
						</table>
					</div>
					<!-- / item -->
					<!-- item -->
					<div class="item brd c">
						<table>
							<tr>
								<td class="rad">
									<input name="credRadio" class="check-custom" type="radio" value="cred3" id="cred3">
								</td>
								<td class="bank">
									<label class="pic" style="background-image:url('<?= SITE_TEMPLATE_PATH ?>/i/blocks/cred3.png');" for="cred3"></label>
								</td>
								<td class="dsc">
									<p>Сумма кредита до 200 000 руб.</p>
									<p>Быстрое решение без заполнения анкет.</p>
									<p>Без комиссий за открытие счета и досрочное погашение кредита.</p>
									<p>Точки погашения кредита по всей России.</p>
								</td>
							</tr>
						</table>
					</div>
					<!-- / item -->
					<!-- item -->
					<div class="item brd c">
						<table>
							<tr>
								<td class="rad">
									<input name="credRadio" class="check-custom" type="radio" value="cred4" id="cred4">
								</td>
								<td class="bank">
									<label class="pic" style="background-image:url('<?= SITE_TEMPLATE_PATH ?>/i/blocks/cred4.png');" for="cred4"></label>
								</td>
								<td class="dsc">
									<p>Сумма кредита до 200 000 руб.</p>
									<p>Быстрое решение без заполнения анкет.</p>
									<p>Без комиссий за открытие счета и досрочное погашение кредита.</p>
									<p>Точки погашения кредита по всей России.</p>
								</td>
							</tr>
						</table>
					</div>
					<!-- / item -->
				</div>
				<!-- / items end -->

				<div class="btns c">
					<input type="submit" class="btn credit-picked-btn" name="credit-picked-btn" value="купить в кредит" />
					<a class="lnk-consult" href="#"><span>Помощь консультанта</span></a>
				</div>

			</form>

		</div>
		<!-- / credit w end -->

		<!-- pop-ft -->
		<div class="pop-ft">
			<p>Для дополнительной информации обратитесь к менеджеру<br/>кредитного отдела 8 800 567 09 00, доб. 1117</p>
		</div>
		<!-- / pop-ft end -->

	</div>
	<!-- / credit popup end -->

	<!-- self popup, triggered by .self-pop -->
	<div class="pop pop-self pop-bg-1" id="popSelf">
		<div class="w">
			<a href="#" class="x">&nbsp;</a>
			<div class="ttl-p1">точки самовывоза</div>
			<form action="#">
				<div class="items">
					<!-- item -->
					<div class="item brd c">
						<table>
							<tr>
								<td class="rad">
									<input name="credRadio" class="check-custom" type="radio" value="self1" id="self1" checked>
								</td>
								<td class="adr">
									<label for="self1">
										<p><strong>Климатический центр Русклимат</strong></p>
										<p>Водный стадион, ул. Нарвская, д.21</p>
									</label>
								</td>
								<td class="dsc">
									<p><strong>График работы</strong></p>
									<p class="str">Пн - Вс: 9.00-16.00</p>
									<p><strong>парковка</strong></p>
									<p class="str">150 парковочных мест</p>
									<p><strong>Способы оплаты</strong></p>
									<p class="str">Наличный расчет, Visa, Master Card</p>
								</td>
							</tr>
						</table>
					</div>
					<!-- / item -->
					<!-- item -->
					<div class="item brd c">
						<table>
							<tr>
								<td class="rad">
									<input name="credRadio" class="check-custom" type="radio" value="self2" id="self2" checked>
								</td>
								<td class="adr">
									<label for="self2">
										<p><strong>Климатический центр Русклимат</strong></p>
										<p>Водный стадион, ул. Нарвская, д.21</p>
									</label>
								</td>
								<td class="dsc">
									<p><strong>График работы</strong></p>
									<p class="str">Пн - Вс: 9.00-16.00</p>
									<p><strong>парковка</strong></p>
									<p class="str">150 парковочных мест</p>
									<p><strong>Способы оплаты</strong></p>
									<p class="str">Наличный расчет, Visa, Master Card</p>
								</td>
							</tr>
						</table>
					</div>
					<!-- / item -->
					<!-- item -->
					<div class="item brd c">
						<table>
							<tr>
								<td class="rad">
									<input name="credRadio" class="check-custom" type="radio" value="self3" id="self3" checked>
								</td>
								<td class="adr">
									<label for="self3">
										<p><strong>Климатический центр Русклимат</strong></p>
										<p>Водный стадион, ул. Нарвская, д.21</p>
									</label>
								</td>
								<td class="dsc">
									<p><strong>График работы</strong></p>
									<p class="str">Пн - Вс: 9.00-16.00</p>
									<p><strong>парковка</strong></p>
									<p class="str">150 парковочных мест</p>
									<p><strong>Способы оплаты</strong></p>
									<p class="str">Наличный расчет, Visa, Master Card</p>
								</td>
							</tr>
						</table>
					</div>
					<!-- / item -->

				</div>
				<!-- / items end -->

				<div class="btns c">
					<input type="submit" class="btn self-picked-btn" name="self-picked-btn" value="Оформит заказ" />
					<a class="lnk-consult" href="#"><span>Помощь консультанта</span></a>
				</div>

			</form>

		</div>
		<!-- / self w end -->

	</div>
	<!-- / self popup end -->

	<!-- one click order popup, triggered by .one-click-pop -->
	<div class="pop pop-one-click pop-bg-1" id="popOneClick">
		<div class="w">
			<a href="#" class="x">&nbsp;</a>
			<div class="ttl-p1">Оставьте номер вашего телефона<br/>и мы вам перезвоним</div>
			<div class="call-form">
				<form action="#" autocomplete="off">
					<input type="text" class="call-field" id="oneClickPhone" placeholder="Ваш номер телефона" value="" />
					<div class="btn call-btn test-one-click-pop-ok" id="callClickBtn">Заказать обратный звонок</div>
				</form>
			</div>
		</div>
	</div>
	<!-- / one click order popup end -->

	<!-- one click order success popup, triggered on success -->
	<div class="pop pop-one-click pop-bg-1" id="popOneClickYes">
		<div class="w">
			<a href="#" class="x">&nbsp;</a>
			<div class="ttl-p2">Вы успешно заказали покупку<br/>товара в один клик!</div>
			<p class="dsc">Наши менеджеры свяжутся с вами в ближайшее время.</p>
			<p class="dsc"><a href="#" class="test-one-click-pop-fail">Тест: если неверно указан номер телефона.</a></p>
		</div>
	</div>
	<!-- / one click order success popup end -->

	<!-- one click order fail popup, triggered on fail -->
	<div class="pop pop-one-click pop-bg-2" id="popOneClickNo">
		<div class="w">
			<a href="#" class="x">&nbsp;</a>
			<div class="ttl-p2">Неверно указан номер телефона,<br/>пожалуйста, повторите ввод!</div>
		</div>
	</div>
	<!-- / one click order fail popup end -->






<!-- / card popups end -->
