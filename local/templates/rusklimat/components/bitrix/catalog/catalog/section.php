<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
use Bitrix\Main\Loader;
use Bitrix\Main\ModuleManager;

$this->setFrameMode(true);

$filter_name=$arParams["FILTER_NAME"];
global $$filter_name;
$inputFilter=$$filter_name;

if (!isset($arParams['FILTER_VIEW_MODE']) || (string)$arParams['FILTER_VIEW_MODE'] == '')
	$arParams['FILTER_VIEW_MODE'] = 'VERTICAL';
$arParams['USE_FILTER'] = (isset($arParams['USE_FILTER']) && $arParams['USE_FILTER'] == 'Y' ? 'Y' : 'N');

if ($arParams['USE_FILTER'] == 'Y')
{
	$arFilter = array(
		"IBLOCK_ID" => $arParams["IBLOCK_ID"],
		"ACTIVE" => "Y",
		"GLOBAL_ACTIVE" => "Y",
	);
	if (0 < intval($arResult["VARIABLES"]["SECTION_ID"]))
	{
		$arFilter["ID"] = $arResult["VARIABLES"]["SECTION_ID"];
	}
	elseif ('' != $arResult["VARIABLES"]["SECTION_CODE"])
	{
		$arFilter["=CODE"] = $arResult["VARIABLES"]["SECTION_CODE"];
	}

	$obCache = new CPHPCache();
	if ($obCache->InitCache(36000, serialize($arFilter), "/iblock/catalog"))
	{
		$arCurSection = $obCache->GetVars();
	}
	elseif ($obCache->StartDataCache())
	{
		$arCurSection = array();
		if (Loader::includeModule("iblock"))
		{
			$dbRes = CIBlockSection::GetList(array(), $arFilter, false, array("ID",'NAME','DESCRIPTION', "SECTION_PAGE_URL", "UF_LINK_SECTIONS", "IBLOCK_SECTION_ID",'LEFT_MARGIN','RIGHT_MARGIN','DEPTH_LEVEL'));

			if(defined("BX_COMP_MANAGED_CACHE"))
			{
				global $CACHE_MANAGER;
				$CACHE_MANAGER->StartTagCache("/iblock/catalog");

				// if ($arCurSection = $dbRes->Fetch())
				if ($arCurSection = $dbRes->GetNext())
				{

					$arSection = array();
					if (!empty($arCurSection["ID"])) {
						$rsPath = CIBlockSection::GetNavChain($arParams["IBLOCK_ID"], $arCurSection['ID'], ['ID', 'DEPTH_LEVEL']);
						while($arPath = $rsPath->GetNext()) {
							$arSection[] = $arPath;
						}
					}
					$arCurSection['SECTION'] = $arSection;
					unset($arSection);
					$CACHE_MANAGER->RegisterTag("iblock_id_".$arParams["IBLOCK_ID"]);
				}
				$CACHE_MANAGER->EndTagCache();
			}
			else
			{
				// if(!$arCurSection = $dbRes->Fetch())
				if(!$arCurSection = $dbRes->GetNext())
					$arCurSection = array();
			}
		}
		$obCache->EndDataCache($arCurSection);
	}
	if (!isset($arCurSection))
	{
		$arCurSection = array();
	}
	if(empty($arCurSection['ID'])) {

		$SELECT_CITY = false;

		$CUR_HOST = $_SERVER['HTTP_HOST'];
		$CUR_HOST = preg_replace('/^www\./ui', '', $CUR_HOST);

		$arFilter = Array("IBLOCK_ID"=>$arParams['GEO_DOMAINS_IBLOCK_ID']);
		$arLimit = Array('nTopCount'=>1);
		$arSelect = Array("ID", "NAME", "PROPERTY_CITY");
		$res = CIBlockElement::GetList(Array(), $arFilter, false, $arLimit, $arSelect);
		if ($arFields = $res->Fetch()) {
			$SELECT_CITY = $arFields['PROPERTY_CITY_VALUE'];
		}

		include($_SERVER['DOCUMENT_ROOT'].'/catalog/redirect_rules.php');	// $REDIRECTS

		$URL = !empty($_SERVER['REDIRECT_URL']) ? $_SERVER['REDIRECT_URL'] : $_SERVER['DOCUMENT_URI'];

		if (isset($REDIRECTS[$URL])) {
			if ($REDIRECTS[$URL][0] == 0) {
				// cat
				$arFilter = Array("UF_CAT_CODE" => $REDIRECTS[$URL][1], "ACTIVE"=>"");
				$items = GetIBlockSectionList($arParams["IBLOCK_ID"], false, Array("sort"=>"asc"), 1, $arFilter);
				if ($arItem = $items->GetNext()) {
					$SEND_URL = nfGetCurPageParam($SELECT_CITY ? 'CITY='.$SELECT_CITY : '', ['CITY'], false, $arItem['SECTION_PAGE_URL'].(!empty($_SERVER['QUERY_STRING']) ? '?'.$_SERVER['QUERY_STRING'] : ''));
					LocalRedirect($SEND_URL, false, '301 Moved Permanently');
				}
			} else {
				// item
				$arFilter = Array("IBLOCK_ID"=>$arParams["IBLOCK_ID"], "PROPERTY_CODE" => $REDIRECTS[$URL][1], "ACTIVE"=>"");
				$arSelectFields = ['ID', 'DETAIL_PAGE_URL', 'LIST_PAGE_URL'];
				$res = GetIBlockElementListEx($arParams["IBLOCK_TYPE"], $arParams["IBLOCK_ID"], false, false, 1, $arFilter, $arSelectFields, false);
				if ($arFields = $res->GetNext()) {
					$SEND_URL = nfGetCurPageParam($SELECT_CITY ? 'CITY='.$SELECT_CITY : '', ['CITY'], false, $arFields['DETAIL_PAGE_URL'].(!empty($_SERVER['QUERY_STRING']) ? '?'.$_SERVER['QUERY_STRING'] : ''));
					LocalRedirect($SEND_URL, false, '301 Moved Permanently');
				}
			}
		}
		unset($REDIRECTS);

		$URL = explode('/', $URL);

		$CLEAR_URL = [];
		foreach(array_reverse($URL) as $link) {
			if (!empty($link)) {
				$CLEAR_URL[] = $link;
			}
		}
		if ($CLEAR_URL[count($CLEAR_URL)-1] == 'catalog') {
			unset($CLEAR_URL[count($CLEAR_URL)-1]);

			// p('not html');

			$arFilter = Array("IBLOCK_ID"=>$arParams["IBLOCK_ID"], "PROPERTY_OLD_CODE1" => $CLEAR_URL[0]);
			// p($arFilter);
			$arSelectFields = ['ID', 'DETAIL_PAGE_URL', 'LIST_PAGE_URL'];
			$res = GetIBlockElementListEx($arParams["IBLOCK_TYPE"], $arParams["IBLOCK_ID"], false, false, 1, $arFilter, $arSelectFields, false);
			if ($arFields = $res->GetNext()) {
				// p('old code');
				// p($arFields);
				$SEND_URL = nfGetCurPageParam($SELECT_CITY ? 'CITY='.$SELECT_CITY : '', ['CITY'], false, $arFields['DETAIL_PAGE_URL'].(!empty($_SERVER['QUERY_STRING']) ? '?'.$_SERVER['QUERY_STRING'] : ''));
				LocalRedirect($SEND_URL, false, '301 Moved Permanently');
			} else {
				// p('new section');

				$arFilter = Array(
					// array(
						// "LOGIC" => "OR",
						// array("UF_CAT_OLD_URL" => $CLEAR_URL[0]),
						// array("UF_CAT_NEW_URL" => $CLEAR_URL[0]),
					// ),
					"UF_CAT_NEW_URL" => $CLEAR_URL[0],
				);
				$items = GetIBlockSectionList($arParams["IBLOCK_ID"], false, Array("sort"=>"asc"), 1, $arFilter);
				if ($arItem = $items->GetNext()) {
					// p($arItem);
					$SEND_URL = nfGetCurPageParam($SELECT_CITY ? 'CITY='.$SELECT_CITY : '', ['CITY'], false, $arItem['SECTION_PAGE_URL'].(!empty($_SERVER['QUERY_STRING']) ? '?'.$_SERVER['QUERY_STRING'] : ''));
					LocalRedirect($SEND_URL, false, '301 Moved Permanently');
				} else {

					// p('old section');

					$SEARCH_ARR = array_reverse($CLEAR_URL);
					$CUR_INDEX = 0;
					$arSections = [];

					foreach($SEARCH_ARR as $CUR_INDEX => $SEARCH_LINK) {
						$arSort = Array('depth_level' => 'asc', 'SORT' => 'asc', 'NAME' => 'asc', 'XML_ID' => 'asc');
						$arFilter = Array("IBLOCK_ID"=>$arParams["IBLOCK_ID"]);
						$arFilter["UF_CAT_OLD_URL"] = $SEARCH_LINK;
						$arFilter["DEPTH_LEVEL"] = $CUR_INDEX + 1;
						if ($CUR_INDEX != 0 && !empty($arSections[$CUR_INDEX-1])) {
							$arFilter["SECTION_ID"] = $arSections[$CUR_INDEX-1]['ID'];
						}
						$arSelect = Array("ID", "NAME", "CODE", "XML_ID", "SECTION_PAGE_URL", "IBLOCK_SECTION_ID", "DEPTH_LEVEL", "UF_CAT_OLD_URL", "UF_CAT_NEW_URL");
						$res = CIBlockSection::GetList(Array(), $arFilter, false, $arSelect);
						if ($arFields = $res->Fetch()) {
							$arSections[$CUR_INDEX] = $arFields;
						} else {
							break;
						}
					}

					// p($arSections);

					if (count($arSections) == count($SEARCH_ARR)) {
						$arFilter = Array(
							"ID" => $arSections[count($arSections)-1]['ID'],
						);
						$items = GetIBlockSectionList($arParams["IBLOCK_ID"], false, Array("sort"=>"asc"), 1, $arFilter);
						if ($arItem = $items->GetNext()) {
							// p($arItem);
							$SEND_URL = nfGetCurPageParam($SELECT_CITY ? 'CITY='.$SELECT_CITY : '', ['CITY'], false, $arItem['SECTION_PAGE_URL'].(!empty($_SERVER['QUERY_STRING']) ? '?'.$_SERVER['QUERY_STRING'] : ''));
							LocalRedirect($SEND_URL, false, '301 Moved Permanently');
						}
					}

				}

			}

		}

	}
	/* ?><?$APPLICATION->IncludeComponent(
		"bitrix:catalog.smart.filter",
		"",
		array(
			"IBLOCK_TYPE" => $arParams["IBLOCK_TYPE"],
			"IBLOCK_ID" => $arParams["IBLOCK_ID"],
			"SECTION_ID" => $arCurSection['ID'],
			"FILTER_NAME" => $arParams["FILTER_NAME"],
			"PRICE_CODE" => $arParams["PRICE_CODE"],
			"CACHE_TYPE" => $arParams["CACHE_TYPE"],
			"CACHE_TIME" => $arParams["CACHE_TIME"],
			"CACHE_GROUPS" => $arParams["CACHE_GROUPS"],
			"SAVE_IN_SESSION" => "N",
			"FILTER_VIEW_MODE" => $arParams["FILTER_VIEW_MODE"],
			"XML_EXPORT" => "Y",
			"SECTION_TITLE" => "NAME",
			"SECTION_DESCRIPTION" => "DESCRIPTION",
			'HIDE_NOT_AVAILABLE' => $arParams["HIDE_NOT_AVAILABLE"],
			"TEMPLATE_THEME" => $arParams["TEMPLATE_THEME"],
			'CONVERT_CURRENCY' => $arParams['CONVERT_CURRENCY'],
			'CURRENCY_ID' => $arParams['CURRENCY_ID'],
		),
		$component,
		array('HIDE_ICONS' => 'Y')
	);?><? */


	// $GLOBALS[$arParams["FILTER_NAME"]][">CATALOG_PRICE_1"] = "0";

}
?>


<?
if($arParams["USE_COMPARE"]=="Y")
{
/*
	?><?$APPLICATION->IncludeComponent(
		"bitrix:catalog.compare.list",
		"empty",
		array(
			"IBLOCK_TYPE" => $arParams["IBLOCK_TYPE"],
			"IBLOCK_ID" => $arParams["IBLOCK_ID"],
			"NAME" => $arParams["COMPARE_NAME"],
			"DETAIL_URL" => $arResult["FOLDER"].$arResult["URL_TEMPLATES"]["element"],
			"COMPARE_URL" => $arResult["FOLDER"].$arResult["URL_TEMPLATES"]["compare"],
			"ACTION_VARIABLE" => $arParams["ACTION_VARIABLE"],
			"PRODUCT_ID_VARIABLE" => $arParams["PRODUCT_ID_VARIABLE"],
			'POSITION_FIXED' => isset($arParams['COMPARE_POSITION_FIXED']) ? $arParams['COMPARE_POSITION_FIXED'] : '',
			'POSITION' => isset($arParams['COMPARE_POSITION']) ? $arParams['COMPARE_POSITION'] : ''
		),
		$component,
		array("HIDE_ICONS" => "Y")
	);?><?
*/
}

if (isset($arParams['USE_COMMON_SETTINGS_BASKET_POPUP']) && $arParams['USE_COMMON_SETTINGS_BASKET_POPUP'] == 'Y')
{
	$basketAction = (isset($arParams['COMMON_ADD_TO_BASKET_ACTION']) ? $arParams['COMMON_ADD_TO_BASKET_ACTION'] : '');
}
else
{
	$basketAction = (isset($arParams['SECTION_ADD_TO_BASKET_ACTION']) ? $arParams['SECTION_ADD_TO_BASKET_ACTION'] : '');
}
$intSectionID = 0;
?>

<?
$removeSetFilter = false;
$key_show_brand_id = false;
if (!empty($arResult['VARIABLES']['BRAND_CODE'])) {
	CModule::IncludeModule("iblock");

	$propID = 52;

	$arSort = [];
	$arFilter = ['IBLOCK_ID' => 9, 'CODE'=>$arResult['VARIABLES']['BRAND_CODE']];
	$arLimit = ['nTopCount'=>1];
	$arSelect = ['ID', 'NAME'];
	$res = CIBlockElement::GetList($arSort, $arFilter, false, $arLimit, $arSelect);
	if ($arFields = $res->Fetch()) {

		$key = $arFields['ID'];
		$htmlKey = htmlspecialcharsbx($key);
		$keyCrc = abs(crc32($htmlKey));

		$key_show_brand_id = 'arrFilter_'.$propID.'_'.$keyCrc;

		$_GET[$key_show_brand_id] = 'Y';
		if ($_GET['set_filter'] != 'Показать') {
			$removeSetFilter = true;
			$_GET['set_filter'] = 'Показать';
		}
	} else {
		LocalRedirect($arCurSection['SECTION_PAGE_URL']);
	}
}
$arResult["URL_TEMPLATES"]["section"] = str_replace('#BRAND_CODE#/', '', $arResult["URL_TEMPLATES"]["section"]);
$arResult["URL_TEMPLATES"]["element"] = str_replace('#BRAND_CODE#/', '', $arResult["URL_TEMPLATES"]["element"]);
//p($arCurSection);
?>

	<?$APPLICATION->IncludeComponent(
		"bitrix:catalog.section.list",
		"inner",
		array(
			"IBLOCK_TYPE" => $arParams["IBLOCK_TYPE"],
			"IBLOCK_ID" => $arParams["IBLOCK_ID"],
			"SECTION_ID" => $arResult["VARIABLES"]["SECTION_ID"],
			"SECTION_CODE" => $arResult["VARIABLES"]["SECTION_CODE"],
			"CACHE_TYPE" => $arParams["CACHE_TYPE"],
			"CACHE_TIME" => $arParams["CACHE_TIME"],
			"CACHE_GROUPS" => $arParams["CACHE_GROUPS"],
			"PAGE" => $_REQUEST['PAGEN_1'],
			"COUNT_ELEMENTS" => $arParams["SECTION_COUNT_ELEMENTS"],
			"TOP_DEPTH" => $arParams["SECTION_TOP_DEPTH"],
			"SECTION_URL" => $arResult["FOLDER"].$arResult["URL_TEMPLATES"]["section"],
			"VIEW_MODE" => $arParams["SECTIONS_VIEW_MODE"],
			"SHOW_PARENT_NAME" => $arParams["SECTIONS_SHOW_PARENT_NAME"],
			"HIDE_SECTION_NAME" => (isset($arParams["SECTIONS_HIDE_SECTION_NAME"]) ? $arParams["SECTIONS_HIDE_SECTION_NAME"] : "N"),
			"ADD_SECTIONS_CHAIN" => (isset($arParams["ADD_SECTIONS_CHAIN"]) ? $arParams["ADD_SECTIONS_CHAIN"] : ''),
			"FILTER" => $GLOBALS[$arParams['FILTER_NAME']],
		),
		$component,
		array("HIDE_ICONS" => "Y")
	);?>
<?switch ($arCurSection['DEPTH_LEVEL']) {
	case '1':
?>
	

<?		
		break;
	
	default:
?>
<div class="roll">



	<!-- cat-cln roll -->
	<div class="cat-cln c roll">
		<?$this->SetViewTarget("col-l");?>
		<!-- col-l -->
		<div class="col-l">

			<!-- filter switcher -->
			<div class="filter-switcher" id="filterSwitcher" style="display: none;">
				<ul class="c">
					<li class="active"><a href="#" class="switch">выбор кондиционера</a><div class="arr"></div></li>
				<!--	<li>/</li>
					<li><a href="#" class="switch">готовые решения</a><div class="arr"></div></li> -->
				</ul>
			</div>
			<!-- / filter switcher end -->

			<!-- filter -->
			<?//p($arParams);?>
			<? if(!empty($arCurSection['ID'])): ?>
				<?$APPLICATION->IncludeComponent(
					"bitrix:catalog.smart.filter",
					"",
					array(
						"IBLOCK_TYPE" => $arParams["IBLOCK_TYPE"],
						"IBLOCK_ID" => $arParams["IBLOCK_ID"],
						"SECTION_ID" => $arCurSection['ID'],
						"SECTION_CODE" => $arResult["VARIABLES"]["SECTION_CODE"],
						"FILTER_NAME" => $arParams["FILTER_NAME"],
						"PRICE_CODE" => $arParams["PRICE_CODE"],
						"CACHE_TYPE" => $arParams["CACHE_TYPE"],
						"CACHE_TIME" => $arParams["CACHE_TIME"],
						"CACHE_GROUPS" => $arParams["CACHE_GROUPS"],
						"SAVE_IN_SESSION" => "N",
						"FILTER_VIEW_MODE" => $arParams["FILTER_VIEW_MODE"],
						"XML_EXPORT" => "Y",
						"SECTION_TITLE" => "NAME",
						"SECTION_DESCRIPTION" => "DESCRIPTION",
						'HIDE_NOT_AVAILABLE' => $arParams["HIDE_NOT_AVAILABLE"],
						"TEMPLATE_THEME" => $arParams["TEMPLATE_THEME"],
						'CONVERT_CURRENCY' => $arParams['CONVERT_CURRENCY'],
						'CURRENCY_ID' => $arParams['CURRENCY_ID'],
						"CUR_MAIN_CITY_CODE" => $arParams['CUR_MAIN_CITY_CODE'],
						"COUNT_ELEMENTS" => $arParams["SECTION_COUNT_ELEMENTS"],
						"TOP_DEPTH" => $arParams["SECTION_TOP_DEPTH"],
						"SECTION_URL" => $arResult["FOLDER"].$arResult["URL_TEMPLATES"]["section"],
						"VIEW_MODE" => $arParams["SECTIONS_VIEW_MODE"],
						"SHOW_PARENT_NAME" => $arParams["SECTIONS_SHOW_PARENT_NAME"],
						"HIDE_SECTION_NAME" => (isset($arParams["SECTIONS_HIDE_SECTION_NAME"]) ? $arParams["SECTIONS_HIDE_SECTION_NAME"] : "N"),
						"ADD_SECTIONS_CHAIN" => (isset($arParams["ADD_SECTIONS_CHAIN"]) ? $arParams["ADD_SECTIONS_CHAIN"] : ''),
					),
					$component,
					array('HIDE_ICONS' => 'N')
				);?>
			<? endif; ?>

			<?

			if ($key_show_brand_id) {
				unset($_GET[$key_show_brand_id]);
			}
			if ($removeSetFilter) {
				unset($_GET['set_filter']);
			}
			?>

			<!-- / filter end -->

			<?$APPLICATION->IncludeFile("include/catalog-info-block.php", Array(), Array("MODE"=>"html"));?>
			<?$APPLICATION->IncludeFile("include/catalog-info-block2.php", Array(), Array("MODE"=>"html"));?>



			<?$APPLICATION->IncludeFile("include/catalog-info-block4.php", Array(), Array("MODE"=>"html"));?>

			<?
			$root_sections = [];
			foreach($arCurSection['SECTION'] as $arSection) {
				if ($arSection['ID'] == $arCurSection['ID']) {
					continue;
				}
				$root_sections[] = $arSection['ID'];
			}
			$root_sections = array_reverse($root_sections);

			$GLOBALS['arrContentFilter'] = [
				'PROPERTY_SECTION'=>$arCurSection['ID'],
			];
			?>
			<?$res=$APPLICATION->IncludeComponent(
				"bitrix:news.list",
				"info-block",
				array(
					"ACTIVE_DATE_FORMAT" => "d.m.Y",
					"ADD_SECTIONS_CHAIN" => "N",
					"AJAX_MODE" => "N",
					"AJAX_OPTION_ADDITIONAL" => "",
					"AJAX_OPTION_HISTORY" => "N",
					"AJAX_OPTION_JUMP" => "N",
					"AJAX_OPTION_STYLE" => "N",
					"CACHE_FILTER" => "Y",
					"CACHE_GROUPS" => "N",
					"CACHE_TIME" => "36000000",
					"CACHE_TYPE" => "A",
					"CHECK_DATES" => "Y",
					"COMPONENT_TEMPLATE" => "info-block",
					"DETAIL_URL" => "",
					"DISPLAY_BOTTOM_PAGER" => "N",
					"DISPLAY_DATE" => "N",
					"DISPLAY_NAME" => "N",
					"DISPLAY_PICTURE" => "N",
					"DISPLAY_PREVIEW_TEXT" => "Y",
					"DISPLAY_TOP_PAGER" => "N",
					"FIELD_CODE" => array(
						0 => "",
						1 => "",
					),
					"FILTER_NAME" => "arrContentFilter",
					"HIDE_LINK_WHEN_NO_DETAIL" => "N",
					"IBLOCK_ID" => "7",
					"IBLOCK_TYPE" => "articles",
					"INCLUDE_IBLOCK_INTO_CHAIN" => "N",
					"INCLUDE_SUBSECTIONS" => "N",
					"MESSAGE_404" => "",
					"NEWS_COUNT" => "9999",
					"PAGER_BASE_LINK_ENABLE" => "N",
					"PAGER_DESC_NUMBERING" => "N",
					"PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
					"PAGER_SHOW_ALL" => "N",
					"PAGER_SHOW_ALWAYS" => "N",
					"PAGER_TEMPLATE" => ".default",
					"PAGER_TITLE" => "",
					"PARENT_SECTION" => "",
					"PARENT_SECTION_CODE" => "",
					"PREVIEW_TRUNCATE_LEN" => "",
					"PROPERTY_CODE" => array(
						0 => "IN_DEEP",
						1 => "SECTION",
						2 => "",
					),
					"SET_BROWSER_TITLE" => "N",
					"SET_LAST_MODIFIED" => "N",
					"SET_META_DESCRIPTION" => "N",
					"SET_META_KEYWORDS" => "N",
					"SET_STATUS_404" => "N",
					"SET_TITLE" => "N",
					"SHOW_404" => "N",
					"SORT_BY1" => "ID",
					"SORT_BY2" => "NAME",
					"SORT_ORDER1" => "DESC",
					"SORT_ORDER2" => "ASC",
					"CATALOG_SECTION_ID" => $arCurSection['ID'],
					"CATALOG_SECTION_ROOT_IDS" => $root_sections,
				),
				false
			);?>
			<?if(count($res)==0 && $arCurSection['IBLOCK_SECTION_ID']){
			$GLOBALS['arrContentFilter'] = [
				'PROPERTY_SECTION'=>$arCurSection['IBLOCK_SECTION_ID'],
			];
			?>
			<?$res=$APPLICATION->IncludeComponent(
				"bitrix:news.list",
				"info-block",
				array(
					"ACTIVE_DATE_FORMAT" => "d.m.Y",
					"ADD_SECTIONS_CHAIN" => "N",
					"AJAX_MODE" => "N",
					"AJAX_OPTION_ADDITIONAL" => "",
					"AJAX_OPTION_HISTORY" => "N",
					"AJAX_OPTION_JUMP" => "N",
					"AJAX_OPTION_STYLE" => "N",
					"CACHE_FILTER" => "Y",
					"CACHE_GROUPS" => "N",
					"CACHE_TIME" => "36000000",
					"CACHE_TYPE" => "A",
					"CHECK_DATES" => "Y",
					"COMPONENT_TEMPLATE" => "info-block",
					"DETAIL_URL" => "",
					"DISPLAY_BOTTOM_PAGER" => "N",
					"DISPLAY_DATE" => "N",
					"DISPLAY_NAME" => "N",
					"DISPLAY_PICTURE" => "N",
					"DISPLAY_PREVIEW_TEXT" => "Y",
					"DISPLAY_TOP_PAGER" => "N",
					"FIELD_CODE" => array(
						0 => "",
						1 => "",
					),
					"FILTER_NAME" => "arrContentFilter",
					"HIDE_LINK_WHEN_NO_DETAIL" => "N",
					"IBLOCK_ID" => "7",
					"IBLOCK_TYPE" => "articles",
					"INCLUDE_IBLOCK_INTO_CHAIN" => "N",
					"INCLUDE_SUBSECTIONS" => "N",
					"MESSAGE_404" => "",
					"NEWS_COUNT" => "9999",
					"PAGER_BASE_LINK_ENABLE" => "N",
					"PAGER_DESC_NUMBERING" => "N",
					"PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
					"PAGER_SHOW_ALL" => "N",
					"PAGER_SHOW_ALWAYS" => "N",
					"PAGER_TEMPLATE" => ".default",
					"PAGER_TITLE" => "",
					"PARENT_SECTION" => "",
					"PARENT_SECTION_CODE" => "",
					"PREVIEW_TRUNCATE_LEN" => "",
					"PROPERTY_CODE" => array(
						0 => "IN_DEEP",
						1 => "SECTION",
						2 => "",
					),
					"SET_BROWSER_TITLE" => "N",
					"SET_LAST_MODIFIED" => "N",
					"SET_META_DESCRIPTION" => "N",
					"SET_META_KEYWORDS" => "N",
					"SET_STATUS_404" => "N",
					"SET_TITLE" => "N",
					"SHOW_404" => "N",
					"SORT_BY1" => "ID",
					"SORT_BY2" => "NAME",
					"SORT_ORDER1" => "DESC",
					"SORT_ORDER2" => "ASC",
					"CATALOG_SECTION_ID" => $arCurSection['ID'],
					"CATALOG_SECTION_ROOT_IDS" => $root_sections,
				),
				false
			);?>
			<?}?>
			<?$APPLICATION->IncludeComponent("sws:super.component", "catalog.services", Array(
				"IBLOCK_ID" => "27",
				"SECTIONS" => $arCurSection['SECTION']
				),
				false
				);?>
			<?/*$ELlist=[];
			$value = [];

			$res = CIBlockElement::GetList(array(), array("SECTION_ID"=>$arCurSection['ID'],"INCLUDE_SUBSECTIONS"=>"Y","IBLOCK_ID"=>$arParams["IBLOCK_ID"]), false, false, array("ID","CODE"));
			while($ar=$res->GetNext()){
				$ELlist[$ar["ID"]]=$ar["CODE"];
			}

			if(count($ELlist)>0){
				$arSort = array('PROPERTY_date'=>'DESC');
				$arFilter = Array("IBLOCK_ID"=>13, "PROPERTY_ITEM_LINK"=>array_keys($ELlist), "ACTIVE"=>"Y");
				$arSelect = false;

				$res = CIBlockElement::GetList($arSort, $arFilter, false, false, $arSelect);
				if($ob = $res->GetNextElement()) {
					$props = $ob->GetProperties();
					$value = [];
					foreach($props as $key=>$prop) {
						$value[$key] = $prop['VALUE'];
					}
					?>
					<div class="info-block">
						<h3 class="ttl2">Последний отзыв</h3>
						<div class="info-review">
							<div class="stars" -data-score="<?= intval($value['grade'])+3 ?>"></div>
							<div>
								<span class="name"><a href="/<?=$ELlist[$value["ITEM_LINK"]]?>/#tabRevw"><?= !empty($value['author']) ? $value['author'] : 'Аноним' ?></a></span>
								<?if($value['city']){?><div class="city"><?=$value['city']?></div><?}?>
							</div>
							<div class="dsc">
								<p><?= $value['text']['TEXT'] ?></p>
							</div>
						</div>
					</div>
				<?}
			}
			if(count($value)==0){
				$res = CIBlockElement::GetList(array(), array("SECTION_ID"=>$arCurSection['IBLOCK_SECTION_ID'],"INCLUDE_SUBSECTIONS"=>"Y","IBLOCK_ID"=>$arParams["IBLOCK_ID"]), false, false, array("ID","CODE"));
				while($ar=$res->GetNext()){
					$ELlist[$ar["ID"]]=$ar["CODE"];
				}

				if(count($ELlist)>0){
					$arSort = array('PROPERTY_date'=>'DESC');
					$arFilter = Array("IBLOCK_ID"=>13, "PROPERTY_ITEM_LINK"=>array_keys($ELlist), "ACTIVE"=>"Y");
					$arSelect = false;

					$res = CIBlockElement::GetList($arSort, $arFilter, false, false, $arSelect);
					if($ob = $res->GetNextElement()) {
						$props = $ob->GetProperties();
						$value = [];
						foreach($props as $key=>$prop) {
							$value[$key] = $prop['VALUE'];
						}
						?>
						<div class="info-block">
							<h3 class="ttl2">Последний отзыв</h3>
							<div class="info-review">
								<div class="stars" -data-score="<?= intval($value['grade'])+3 ?>"></div>
								<div>
									<span class="name"><a href="/<?=$ELlist[$value["ITEM_LINK"]]?>/#tabRevw"><?= !empty($value['author']) ? $value['author'] : 'Аноним' ?></a></span>
									<?if($value['city']){?><div class="city"><?=$value['city']?></div><?}?>
								</div>
								<div class="dsc">
									<p><?= $value['text']['TEXT'] ?></p>
								</div>
							</div>
						</div>
					<?}
				}
			}*/
			?>
			<?/*if(is_array($arCurSection["UF_LINK_SECTIONS"]) && count($arCurSection["UF_LINK_SECTIONS"])>0){?>
				<div class="info-block">
					<h3 class="ttl2">При покупке также интересуются</h3>
					<ul class="list item-list">
					<?$dbRes = CIBlockSection::GetList(array(), array("ID"=>$arCurSection["UF_LINK_SECTIONS"]), false, array("ID", "SECTION_PAGE_URL", "NAME", "PICTURE"));
					while($arRes=$dbRes->GetNext()){
						$file = CFile::ResizeImageGet($arRes['PICTURE'], array('width'=>35, 'height'=>25), BX_RESIZE_IMAGE_PROPORTIONAL, true);
						?>
						<li><a href="<?=$arRes["SECTION_PAGE_URL"]?>" style="background-image:url('<?=$file['src']?>');"><?=$arRes["NAME"]?></a></li>

					<?}?>
					</ul>
				</div>
			<?}*/
			?>
		</div>
		<!-- / col-l end -->
		<?$this->EndViewTarget();?>
		<?$APPLICATION->ShowViewContent("col-l");?>

		<!-- col-r -->
		<div class="col-r" id="catalogList">

			<?
			$SORT = [
				[$arParams["ELEMENT_SORT_FIELD"], $arParams["ELEMENT_SORT_ORDER"]],
				[$arParams["ELEMENT_SORT_FIELD2"], $arParams["ELEMENT_SORT_ORDER2"]],
			];
			if (empty($_REQUEST['SORT']) || $_REQUEST['SORT'] == 'POPULAR') {

				global $arGeoData;

				$SEE_PROP = preg_replace('/\-+/ui', '_', $arGeoData['CUR_CITY']['CITY_CODE']);
				$SEE_PROP = strtoupper('SEE_'.$SEE_PROP);

				$SORT = [
					// ['show_counter', 'desc'],
					['PROPERTY_VIEWS.PROPERTY_'.$SEE_PROP, 'desc,nulls'],
					[$arParams["ELEMENT_SORT_FIELD"], $arParams["ELEMENT_SORT_ORDER"]],
				];
			}elseif ($_REQUEST['SORT'] == 'PRICE_DESC') {
				$SORT = [
					[$arParams["ELEMENT_SORT_FIELD"], 'desc'],
					[$arParams["ELEMENT_SORT_FIELD2"], $arParams["ELEMENT_SORT_ORDER2"]],
				];
			} elseif ($_REQUEST['SORT'] == 'PRICE_ASC') {
				$SORT = [
					[$arParams["ELEMENT_SORT_FIELD"], 'asc'],
					[$arParams["ELEMENT_SORT_FIELD2"], $arParams["ELEMENT_SORT_ORDER2"]],
				];
			}
			$PAGE_ELEMENT_COUNT = $arParams["PAGE_ELEMENT_COUNT"];
			$CNT_AVAL = [/*10, */20, 50, 100/*"ALL"*/];
			$count=$APPLICATION->get_cookie("count");
			if (!empty($_REQUEST['COUNT'])) {
				if($_REQUEST['COUNT']!="ALL") $_REQUEST['COUNT'] = intval($_REQUEST['COUNT']);
				if (in_array($_REQUEST['COUNT'], $CNT_AVAL)) {
					$APPLICATION->set_cookie("count",$_REQUEST['COUNT']);
					if($_REQUEST['COUNT']!="ALL") $PAGE_ELEMENT_COUNT = $_REQUEST['COUNT'];
					else $PAGE_ELEMENT_COUNT = 100;
				}
			}elseif($count){
				if($count!="ALL") $PAGE_ELEMENT_COUNT = $count;
				else $PAGE_ELEMENT_COUNT = 100;
			}
			?>
			<?$this->SetViewTarget("results-switcher");?>
			<!-- results switcher -->
			<? if(!empty($arCurSection['ID'])): ?>
				<div class="results-switcher brd c">

					<div class="sort">
						<div class="ttl">Сортировать по</div>
						<select class="sort-select" id="sortSelect" name="sortSelect">
							<option value="<?= nfGetCurPageParam('SORT=POPULAR', ['SORT']) ?>"<?= ($_REQUEST['SORT'] == 'POPULAR' ? ' selected' : '') ?>>популярности</option>
							<option value="<?= nfGetCurPageParam('SORT=PRICE_ASC', ['SORT']) ?>"<?= ($_REQUEST['SORT'] == 'PRICE_ASC' ? ' selected' : '') ?>>возрастанию цены</option>
							<option value="<?= nfGetCurPageParam('SORT=PRICE_DESC', ['SORT']) ?>"<?= ($_REQUEST['SORT'] == 'PRICE_DESC' ? ' selected' : '') ?>>убыванию цены</option>
						</select>
					</div>

					<div class="amount">
						<div class="ttl">Показывать по</div>
						<select class="amount-select" id="amountSelect" name="amount-select">
							<?/*<option value="<?= nfGetCurPageParam('COUNT=10', ['COUNT']) ?>">10</option>*/?>
							<option value="<?= nfGetCurPageParam('COUNT=20', ['COUNT']) ?>"<?= ($PAGE_ELEMENT_COUNT == '20' ? ' selected' : '') ?>>20</option>
							<option value="<?= nfGetCurPageParam('COUNT=50', ['COUNT']) ?>"<?= ($PAGE_ELEMENT_COUNT == '50' ? ' selected' : '') ?>>50</option>
							<option value="<?= nfGetCurPageParam('COUNT=100', ['COUNT']) ?>"<?= ($PAGE_ELEMENT_COUNT == '100' ? ' selected' : '') ?>>100</option>
							<?/*<option value="<?= nfGetCurPageParam('COUNT=ALL', ['COUNT']) ?>"<?= ($PAGE_ELEMENT_COUNT == 100 ? ' selected' : '') ?>>все</option>*/?>
						</select>
					</div>

					<div class="btns" id="resultsControl">
						<a href="#" class="switch tiles <?if(isset($_COOKIE["catalog-result-type"]) && $_COOKIE["catalog-result-type"]=="line"){?>active<?}?>"></a><a href="#" class="switch blocks <?if($_COOKIE["catalog-result-type"]=="block"){?>active<?}?>"></a>
						<script>
							if (!$('#resultsControl a').hasClass('active')) {
								$('#resultsControl a').first().addClass('active')
							}
						</script>
					</div>
				</div>
			<? endif; ?>
			<!-- / results switcher end -->
			<?$this->EndViewTarget();?>
			<?$this->SetViewTarget("listing");
						CModule::IncludeModule('iblock');
						
						$properties=[];
						$arFilter1 = Array("IBLOCK_ID"=>43,'PROPERTY_TILE_CATS'=>$arCurSection['ID']);
						$arSelect1 = Array("ID", "NAME", "CODE", "XML_ID");
						$res = CIBlockElement::GetList(Array('SORT'=>'ASC'), $arFilter1, false, false, $arSelect1);  
						while($arFields = $res->Fetch()) {  
							$properties[]=$arFields['CODE'];
						}
						
						if(count($properties)==0){
							$IDs=[];
							//p($arCurSection);
							$arFilter2 = Array("IBLOCK_ID"=>$arParams['IBLOCK_ID'], '>LEFT_MARGIN' => $arCurSection['LEFT_MARGIN'],'<RIGHT_MARGIN' => $arCurSection['RIGHT_MARGIN'],'>DEPTH_LEVEL' => $arCurSection['DEPTH_LEVEL']);
							$arSelect2 = Array("ID", "IBLOCK_SECTION_ID", "DEPTH_LEVEL");
							$res = CIBlockSection::GetList($arSort, $arFilter2, false, $arSelect2);
							while($arFields=$res->GetNext()){
								//p($arFields);
								$IDs[]=$arFields['ID'];
							}
							//p($IDs);
							$arFilter1 = Array("IBLOCK_ID"=>43,'PROPERTY_TILE_CATS'=>$IDs);
							$arSelect1 = Array("ID", "NAME", "CODE", "XML_ID");
							$res = CIBlockElement::GetList(Array('SORT'=>'ASC'), $arFilter1, false, false, $arSelect1);  
							while($arFields = $res->Fetch()) {  
								$properties[]=$arFields['CODE'];
							}

						}
						//p($properties);
			?>
			<? $intSectionID = $APPLICATION->IncludeComponent(
				"bitrix:catalog.section",
				"",
				array(
					"IBLOCK_TYPE" => $arParams["IBLOCK_TYPE"],
					"IBLOCK_ID" => $arParams["IBLOCK_ID"],
					// "ELEMENT_SORT_FIELD" => $arParams["ELEMENT_SORT_FIELD"],
					// "ELEMENT_SORT_ORDER" => $arParams["ELEMENT_SORT_ORDER"],
					// "ELEMENT_SORT_FIELD2" => $arParams["ELEMENT_SORT_FIELD2"],
					// "ELEMENT_SORT_ORDER2" => $arParams["ELEMENT_SORT_ORDER2"],
					"ELEMENT_SORT_FIELD" => $SORT[0][0],
					"ELEMENT_SORT_ORDER" => $SORT[0][1],
					"ELEMENT_SORT_FIELD2" => $SORT[1][0],
					"ELEMENT_SORT_ORDER2" => $SORT[1][1],
					"PROPERTY_CODE" => $properties,
					"META_KEYWORDS" => $arParams["LIST_META_KEYWORDS"],
					"META_DESCRIPTION" => $arParams["LIST_META_DESCRIPTION"],
					"BROWSER_TITLE" => $arParams["LIST_BROWSER_TITLE"],
					"INCLUDE_SUBSECTIONS" => $arParams["INCLUDE_SUBSECTIONS"],
					"BASKET_URL" => $arParams["BASKET_URL"],
					"ACTION_VARIABLE" => $arParams["ACTION_VARIABLE"],
					"PRODUCT_ID_VARIABLE" => $arParams["PRODUCT_ID_VARIABLE"],
					"SECTION_ID_VARIABLE" => $arParams["SECTION_ID_VARIABLE"],
					"PRODUCT_QUANTITY_VARIABLE" => $arParams["PRODUCT_QUANTITY_VARIABLE"],
					"PRODUCT_PROPS_VARIABLE" => $arParams["PRODUCT_PROPS_VARIABLE"],
					"FILTER_NAME" => $arParams["FILTER_NAME"],
					"CACHE_TYPE" => $arParams["CACHE_TYPE"],
					"CACHE_TIME" => $arParams["CACHE_TIME"],
					"CACHE_FILTER" => $arParams["CACHE_FILTER"],
					"CACHE_GROUPS" => $arParams["CACHE_GROUPS"],
					"SET_TITLE" => $arParams["SET_TITLE"],
					"SET_STATUS_404" => $arParams["SET_STATUS_404"],
					"DISPLAY_COMPARE" => $arParams["USE_COMPARE"],
					// "PAGE_ELEMENT_COUNT" => $arParams["PAGE_ELEMENT_COUNT"],
					"PAGE_ELEMENT_COUNT" => $PAGE_ELEMENT_COUNT,
					"LINE_ELEMENT_COUNT" => $arParams["LINE_ELEMENT_COUNT"],
					"PRICE_CODE" => $arParams["PRICE_CODE"],
					"USE_PRICE_COUNT" => $arParams["USE_PRICE_COUNT"],
					"SHOW_PRICE_COUNT" => $arParams["SHOW_PRICE_COUNT"],

					"PRICE_VAT_INCLUDE" => $arParams["PRICE_VAT_INCLUDE"],
					"USE_PRODUCT_QUANTITY" => $arParams['USE_PRODUCT_QUANTITY'],
					"ADD_PROPERTIES_TO_BASKET" => (isset($arParams["ADD_PROPERTIES_TO_BASKET"]) ? $arParams["ADD_PROPERTIES_TO_BASKET"] : ''),
					"PARTIAL_PRODUCT_PROPERTIES" => (isset($arParams["PARTIAL_PRODUCT_PROPERTIES"]) ? $arParams["PARTIAL_PRODUCT_PROPERTIES"] : ''),
					"PRODUCT_PROPERTIES" => $arParams["PRODUCT_PROPERTIES"],

					"DISPLAY_TOP_PAGER" => $arParams["DISPLAY_TOP_PAGER"],
					"DISPLAY_BOTTOM_PAGER" => $arParams["DISPLAY_BOTTOM_PAGER"],
					"PAGER_TITLE" => $arParams["PAGER_TITLE"],
					"PAGER_SHOW_ALWAYS" => $arParams["PAGER_SHOW_ALWAYS"],
					"PAGER_TEMPLATE" => $arParams["PAGER_TEMPLATE"],
					"PAGER_DESC_NUMBERING" => $arParams["PAGER_DESC_NUMBERING"],
					"PAGER_DESC_NUMBERING_CACHE_TIME" => $arParams["PAGER_DESC_NUMBERING_CACHE_TIME"],
					"PAGER_SHOW_ALL" => $arParams["PAGER_SHOW_ALL"],

					"OFFERS_CART_PROPERTIES" => $arParams["OFFERS_CART_PROPERTIES"],
					"OFFERS_FIELD_CODE" => $arParams["LIST_OFFERS_FIELD_CODE"],
					"OFFERS_PROPERTY_CODE" => $arParams["LIST_OFFERS_PROPERTY_CODE"],
					"OFFERS_SORT_FIELD" => $arParams["OFFERS_SORT_FIELD"],
					"OFFERS_SORT_ORDER" => $arParams["OFFERS_SORT_ORDER"],
					"OFFERS_SORT_FIELD2" => $arParams["OFFERS_SORT_FIELD2"],
					"OFFERS_SORT_ORDER2" => $arParams["OFFERS_SORT_ORDER2"],
					"OFFERS_LIMIT" => $arParams["LIST_OFFERS_LIMIT"],

					"SECTION_ID" => $arResult["VARIABLES"]["SECTION_ID"],
					"SECTION_CODE" => $arResult["VARIABLES"]["SECTION_CODE"],
					"SECTION_URL" => $arResult["FOLDER"].$arResult["URL_TEMPLATES"]["section"],
					"DETAIL_URL" => $arResult["FOLDER"].$arResult["URL_TEMPLATES"]["element"],
					'CONVERT_CURRENCY' => $arParams['CONVERT_CURRENCY'],
					'CURRENCY_ID' => $arParams['CURRENCY_ID'],
					'HIDE_NOT_AVAILABLE' => $arParams["HIDE_NOT_AVAILABLE"],

					'LABEL_PROP' => $arParams['LABEL_PROP'],
					'ADD_PICT_PROP' => $arParams['ADD_PICT_PROP'],
					'PRODUCT_DISPLAY_MODE' => $arParams['PRODUCT_DISPLAY_MODE'],

					'OFFER_ADD_PICT_PROP' => $arParams['OFFER_ADD_PICT_PROP'],
					'OFFER_TREE_PROPS' => $arParams['OFFER_TREE_PROPS'],
					'PRODUCT_SUBSCRIPTION' => $arParams['PRODUCT_SUBSCRIPTION'],
					'SHOW_DISCOUNT_PERCENT' => $arParams['SHOW_DISCOUNT_PERCENT'],
					'SHOW_OLD_PRICE' => $arParams['SHOW_OLD_PRICE'],
					'MESS_BTN_BUY' => $arParams['MESS_BTN_BUY'],
					'MESS_BTN_ADD_TO_BASKET' => $arParams['MESS_BTN_ADD_TO_BASKET'],
					'MESS_BTN_SUBSCRIBE' => $arParams['MESS_BTN_SUBSCRIBE'],
					'MESS_BTN_DETAIL' => $arParams['MESS_BTN_DETAIL'],
					'MESS_NOT_AVAILABLE' => $arParams['MESS_NOT_AVAILABLE'],

					'TEMPLATE_THEME' => (isset($arParams['TEMPLATE_THEME']) ? $arParams['TEMPLATE_THEME'] : ''),
					"ADD_SECTIONS_CHAIN" => "N",
					'ADD_TO_BASKET_ACTION' => $basketAction,
					'SHOW_CLOSE_POPUP' => isset($arParams['COMMON_SHOW_CLOSE_POPUP']) ? $arParams['COMMON_SHOW_CLOSE_POPUP'] : '',
					'COMPARE_PATH' => $arResult['FOLDER'].$arResult['URL_TEMPLATES']['compare'],
					"CUR_MAIN_CITY_CODE" => $arParams['CUR_MAIN_CITY_CODE'],
					"CUR_MAIN_CITY_CODE2" => $arParams['CUR_MAIN_CITY_CODE2'],
					"QUANITY_PROP" => $arParams['QUANITY_PROP'],
					"QUANITY_PROP2" => $arParams['QUANITY_PROP2'],
				),
				$component
			); ?>
			<?$this->EndViewTarget();?>
			<?
			$filter_name=$arParams["FILTER_NAME"];
			global $$filter_name;
			//print_r($$filter_name);
			?>
			<?if(trim($APPLICATION->GetViewContent("listing"))){
				$APPLICATION->ShowViewContent("listing");
			}elseif(!is_array($$filter_name) || count($$filter_name)==0 || $inputFilter===$$filter_name){
				unset($APPLICATION->__view["col-l"]);?>
				<?$APPLICATION->IncludeFile("/catalog/messages/catalog-section-empty.php", Array(), Array("MODE"=>"html"));?>
			<?}else{
				$APPLICATION->ShowViewContent("results-switcher");?>
				<?$APPLICATION->IncludeFile("/catalog/messages/catalog-filter-empty.php", Array(), Array("MODE"=>"html"));?>
			<?}?>

			<?
			$GLOBALS['CATALOG_CURRENT_SECTION_ID'] = $intSectionID;
			unset($basketAction);
			?>


		</div>
		<!-- / col-r end-->
	</div>
	<!-- / cat-cln end-->
</div>
<!-- / roll end -->
<?if($APPLICATION->__view["col-l"]){?>
<?
$GLOBALS['arrBrandsFilter'] = [];
if (!empty($GLOBALS['FILTER_BRANDS'])) {
	$GLOBALS['arrBrandsFilter']['ID'] = array_keys($GLOBALS['FILTER_BRANDS']);
} else {
	$GLOBALS['arrBrandsFilter']['ID'] = [0];
}
?>

<?$APPLICATION->IncludeComponent("bitrix:news.list", "brands_list", Array(
		"ACTIVE_DATE_FORMAT" => "d.m.Y",	// Формат показа даты
		"ADD_SECTIONS_CHAIN" => "N",	// Включать раздел в цепочку навигации
		"AJAX_MODE" => "N",	// Включить режим AJAX
		"AJAX_OPTION_ADDITIONAL" => "",	// Дополнительный идентификатор
		"AJAX_OPTION_HISTORY" => "N",	// Включить эмуляцию навигации браузера
		"AJAX_OPTION_JUMP" => "N",	// Включить прокрутку к началу компонента
		"AJAX_OPTION_STYLE" => "N",	// Включить подгрузку стилей
		"CACHE_FILTER" => "Y",	// Кешировать при установленном фильтре
		"CACHE_GROUPS" => "N",	// Учитывать права доступа
		"CACHE_TIME" => "36000000",	// Время кеширования (сек.)
		"CACHE_TYPE" => "A",	// Тип кеширования
		"CHECK_DATES" => "Y",	// Показывать только активные на данный момент элементы
		"COMPONENT_TEMPLATE" => ".default",
		"DETAIL_URL" => "",	// URL страницы детального просмотра (по умолчанию - из настроек инфоблока)
		"DISPLAY_BOTTOM_PAGER" => "N",	// Выводить под списком
		"DISPLAY_DATE" => "N",	// Выводить дату элемента
		"DISPLAY_NAME" => "Y",	// Выводить название элемента
		"DISPLAY_PICTURE" => "Y",	// Выводить изображение для анонса
		"DISPLAY_PREVIEW_TEXT" => "N",	// Выводить текст анонса
		"DISPLAY_TOP_PAGER" => "N",	// Выводить над списком
		"FIELD_CODE" => "",	// Поля
		"FILTER_NAME" => "arrBrandsFilter",	// Фильтр
		"HIDE_LINK_WHEN_NO_DETAIL" => "N",	// Скрывать ссылку, если нет детального описания
		"IBLOCK_ID" => "9",	// Код информационного блока
		"IBLOCK_TYPE" => "catalog",	// Тип информационного блока (используется только для проверки)
		"INCLUDE_IBLOCK_INTO_CHAIN" => "N",	// Включать инфоблок в цепочку навигации
		"INCLUDE_SUBSECTIONS" => "Y",	// Показывать элементы подразделов раздела
		"MESSAGE_404" => "",	// Сообщение для показа (по умолчанию из компонента)
		"NEWS_COUNT" => "1000",	// Количество новостей на странице
		"PAGER_BASE_LINK_ENABLE" => "N",	// Включить обработку ссылок
		"PAGER_DESC_NUMBERING" => "N",	// Использовать обратную навигацию
		"PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",	// Время кеширования страниц для обратной навигации
		"PAGER_SHOW_ALL" => "N",	// Показывать ссылку "Все"
		"PAGER_SHOW_ALWAYS" => "N",	// Выводить всегда
		"PAGER_TEMPLATE" => ".default",	// Шаблон постраничной навигации
		"PAGER_TITLE" => "",	// Название категорий
		"PARENT_SECTION" => "",	// ID раздела
		"PARENT_SECTION_CODE" => "",	// Код раздела
		"PREVIEW_TRUNCATE_LEN" => "",	// Максимальная длина анонса для вывода (только для типа текст)
		"PROPERTY_CODE" => "",	// Свойства
		"SET_BROWSER_TITLE" => "N",	// Устанавливать заголовок окна браузера
		"SET_LAST_MODIFIED" => "N",	// Устанавливать в заголовках ответа время модификации страницы
		"SET_META_DESCRIPTION" => "N",	// Устанавливать описание страницы
		"SET_META_KEYWORDS" => "N",	// Устанавливать ключевые слова страницы
		"SET_STATUS_404" => "N",	// Устанавливать статус 404
		"SET_TITLE" => "N",	// Устанавливать заголовок страницы
		"SHOW_404" => "N",	// Показ специальной страницы
		"SORT_BY1" => "NAME",	// Поле для первой сортировки новостей
		"SORT_BY2" => "ID",	// Поле для второй сортировки новостей
		"SORT_ORDER1" => "ASC",	// Направление для первой сортировки новостей
		"SORT_ORDER2" => "DESC",	// Направление для второй сортировки новостей
		"BLOCK_TITLE" => $GLOBALS['CUR_SECTION_NAME'],
	),
	false
);?>
<?
$REMOVE_PARAMS = [];
$REMOVE_PARAMS[] = 'set_filter';
$REMOVE_PARAMS[] = 'del_filter';
foreach($GLOBALS['FILTER_BRANDS'] as $BRAND_ID => $BRAND_FILTER_DATA) {
	$REMOVE_PARAMS[] = $BRAND_FILTER_DATA['CONTROL_NAME'];
}
?>
<script>
	<? foreach($GLOBALS['FILTER_BRANDS'] as $BRAND_ID => $BRAND_FILTER_DATA): ?>
		<?
		/*
		$('#brandsLogoSlider #brand_filter_<?= $BRAND_ID ?>').attr('href', '<?= nfGetCurPageParam($BRAND_FILTER_DATA['CONTROL_NAME'].'='.$BRAND_FILTER_DATA['HTML_VALUE'].'&set_filter='.$GLOBALS['SET_FILTER_LABEL'], $REMOVE_PARAMS, false, $arCurSection['SECTION_PAGE_URL']) ?>');
		*/
		?>
		$('#brandsLogoSlider #brand_filter_<?= $BRAND_ID ?>').attr('href', '<?= $arCurSection['SECTION_PAGE_URL'].$BRAND_FILTER_DATA['URL_ID'].'/' ?>');
	<? endforeach; ?>
</script>


<?$APPLICATION->IncludeComponent(
	"bitrix:catalog.section.list",
	"inner_bottom",
	array(
		"IBLOCK_TYPE" => $arParams["IBLOCK_TYPE"],
		"IBLOCK_ID" => $arParams["IBLOCK_ID"],
		"SECTION_ID" => $arResult["VARIABLES"]["SECTION_ID"],
		"SECTION_CODE" => $arResult["VARIABLES"]["SECTION_CODE"],
		"CACHE_TYPE" => $arParams["CACHE_TYPE"],
		"CACHE_TIME" => $arParams["CACHE_TIME"],
		"CACHE_GROUPS" => $arParams["CACHE_GROUPS"],
		"PAGE" => $_REQUEST['PAGEN_1'],
		// "COUNT_ELEMENTS" => $arParams["SECTION_COUNT_ELEMENTS"],
		"COUNT_ELEMENTS" => 'N',
		// "TOP_DEPTH" => $arParams["SECTION_TOP_DEPTH"],
		"TOP_DEPTH" => 1,
		"SECTION_FIELDS" => ['UF_CAT_SEO_DESC'],
		"SECTION_URL" => $arResult["FOLDER"].$arResult["URL_TEMPLATES"]["section"],
		"VIEW_MODE" => $arParams["SECTIONS_VIEW_MODE"],
		"SHOW_PARENT_NAME" => $arParams["SECTIONS_SHOW_PARENT_NAME"],
		"HIDE_SECTION_NAME" => (isset($arParams["SECTIONS_HIDE_SECTION_NAME"]) ? $arParams["SECTIONS_HIDE_SECTION_NAME"] : "N"),
		"ADD_SECTIONS_CHAIN" => 'N'
	),
	$component,
	array("HIDE_ICONS" => "Y")
);?>
<?}?>


<?		
		break;
}?>
