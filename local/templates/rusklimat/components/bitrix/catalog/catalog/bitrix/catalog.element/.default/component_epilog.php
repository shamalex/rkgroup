<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $templateData */
/** @var @global CMain $APPLICATION */
/* use Bitrix\Main\Loader;
global $APPLICATION;
if (isset($templateData['TEMPLATE_THEME']))
{
	$APPLICATION->SetAdditionalCSS($templateData['TEMPLATE_THEME']);
}
if (isset($templateData['TEMPLATE_LIBRARY']) && !empty($templateData['TEMPLATE_LIBRARY']))
{
	$loadCurrency = false;
	if (!empty($templateData['CURRENCIES']))
		$loadCurrency = Loader::includeModule('currency');
	CJSCore::Init($templateData['TEMPLATE_LIBRARY']);
	if ($loadCurrency)
	{
	?>
	<script type="text/javascript">
		BX.Currency.setCurrencies(<? echo $templateData['CURRENCIES']; ?>);
	</script>
<?
	}
}
if (isset($templateData['JS_OBJ']))
{
?><script type="text/javascript">
BX.ready(BX.defer(function(){
	if (!!window.<? echo $templateData['JS_OBJ']; ?>)
	{
		window.<? echo $templateData['JS_OBJ']; ?>.allowViewedCount(true);
	}
}));
</script><?
} */
/*echo "<pre>";print_r($uri);echo "</pre>";
die();*/
// p($arResult);
global $APPLICATION;
// $APPLICATION->AddChainItem(!empty($arResult['PROPERTIES']['item_name']['VALUE']) ? $arResult['PROPERTIES']['item_name']['VALUE'] : $arResult['NAME'], '');
// $GLOBALS['ADD_NAME_TO_CHAIN'] = !empty($arResult['PROPERTIES']['item_name']['VALUE']) ? $arResult['PROPERTIES']['item_name']['VALUE'] : $arResult['NAME'];
if ($arParams['ADD_SECTIONS_CHAIN'] != 'Y' && $arParams['ADD_SECTIONS_CHAIN'] != 1) {
	foreach($arResult['SECTION']['PATH'] as $arSection) {
		$link = '/'.$arSection['CODE'].'/';
		$APPLICATION->AddChainItem($arSection['NAME'], $link);
	}
	$APPLICATION->AddChainItem(!empty($arResult['PROPERTIES']['item_name']['VALUE']) ? $arResult['PROPERTIES']['item_name']['VALUE'] : $arResult['NAME'], '');
} else {
	CModule::IncludeModule('iblock');
	$arSort = array();
	$arFilter = Array("IBLOCK_ID"=>$arParams['IBLOCK_ID'], "ID"=>$arResult['ID']);
	$arLimit = ['nTopCount'=>1];
	$arSelect = ['ID', 'DETAIL_PAGE_URL'];
	$res = CIBlockElement::GetList($arSort, $arFilter, false, $arLimit, $arSelect);
	if ($arElement = $res->GetNext()) {
		/*LocalRedirect($arElement['DETAIL_PAGE_URL'], false, '301 Moved Permanently');*/
	}
}
$GLOBALS['ORDER_WITH_SETUP'] = $arResult['PROPERTIES']['ORDER_WITH_SETUP']['VALUE'];


// views

CModule::IncludeModule('iblock');

$VIEWS_IBLOCK_ID = 25;
if (empty($arResult['PROPERTIES']['VIEWS']['VALUE'])) {

	$el = new CIBlockElement;

	$arLoadProductArray = Array(
		"IBLOCK_SECTION_ID" => false,
		"IBLOCK_ID"      => $VIEWS_IBLOCK_ID,
		"NAME"           => $arResult['ID'],
		"CODE"           => '',
		"XML_ID"         => $arResult['ID'],
		"ACTIVE"         => 'Y',
		"SORT"    => '1',
		"PROPERTY_VALUES"    => [],
	);

	if($PRODUCT_ID = $el->Add($arLoadProductArray, false, false, false)) {
		// p("New ID: ".$PRODUCT_ID);
		CIBlockElement::SetPropertyValuesEx($arResult['ID'], $arParams['IBLOCK_ID'], array('VIEWS' => $PRODUCT_ID));
		$arResult['PROPERTIES']['VIEWS']['VALUE'] = $PRODUCT_ID;
	} else {
		// p("Error add: ".$el->LAST_ERROR);
	}
}

global $arGeoData;

$SEE_PROP = preg_replace('/\-+/ui', '_', $arGeoData['CUR_CITY']['CITY_CODE']);
$SEE_PROP = strtoupper('SEE_'.$SEE_PROP);

$arSort = array();
$arFilter = Array("IBLOCK_ID"=>$VIEWS_IBLOCK_ID, "ID"=>$arResult['PROPERTIES']['VIEWS']['VALUE']);
$arLimit = ['nTopCount'=>1];
$arSelect = ['ID', 'PROPERTY_'.$SEE_PROP];
$res = CIBlockElement::GetList($arSort, $arFilter, false, $arLimit, $arSelect);
if ($arElement = $res->Fetch()) {
	$views = intval($arElement['PROPERTY_'.$SEE_PROP.'_VALUE']);
	$views++;
	CIBlockElement::SetPropertyValuesEx($arElement['ID'], $VIEWS_IBLOCK_ID, array($SEE_PROP => $views));
} else {
	CIBlockElement::SetPropertyValuesEx($arResult['ID'], $arParams['IBLOCK_ID'], array('VIEWS' => ''));
}

$strMainID = $this->GetEditAreaId($arResult['ID']);
$arItemIDs = array(
	'ID' => $strMainID,
	'ADD_BASKET_LINK' => $strMainID.'_add_basket_link',
);
