<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();

/* 
$arViewModeList = array('LIST', 'LINE', 'TEXT', 'TILE');

$arDefaultParams = array(
	'VIEW_MODE' => 'LIST',
	'SHOW_PARENT_NAME' => 'Y',
	'HIDE_SECTION_NAME' => 'N'
);

$arParams = array_merge($arDefaultParams, $arParams);

if (!in_array($arParams['VIEW_MODE'], $arViewModeList))
	$arParams['VIEW_MODE'] = 'LIST';
if ('N' != $arParams['SHOW_PARENT_NAME'])
	$arParams['SHOW_PARENT_NAME'] = 'Y';
if ('Y' != $arParams['HIDE_SECTION_NAME'])
	$arParams['HIDE_SECTION_NAME'] = 'N';

$arResult['VIEW_MODE_LIST'] = $arViewModeList;

if (0 < $arResult['SECTIONS_COUNT'])
{
	if ('LIST' != $arParams['VIEW_MODE'])
	{
		$boolClear = false;
		$arNewSections = array();
		foreach ($arResult['SECTIONS'] as &$arOneSection)
		{
			if (1 < $arOneSection['RELATIVE_DEPTH_LEVEL'])
			{
				$boolClear = true;
				continue;
			}
			$arNewSections[] = $arOneSection;
		}
		unset($arOneSection);
		if ($boolClear)
		{
			$arResult['SECTIONS'] = $arNewSections;
			$arResult['SECTIONS_COUNT'] = count($arNewSections);
		}
		unset($arNewSections);
	}
}

if (0 < $arResult['SECTIONS_COUNT'])
{
	$boolPicture = false;
	$boolDescr = false;
	$arSelect = array('ID');
	$arMap = array();
	if ('LINE' == $arParams['VIEW_MODE'] || 'TILE' == $arParams['VIEW_MODE'])
	{
		reset($arResult['SECTIONS']);
		$arCurrent = current($arResult['SECTIONS']);
		if (!isset($arCurrent['PICTURE']))
		{
			$boolPicture = true;
			$arSelect[] = 'PICTURE';
		}
		if ('LINE' == $arParams['VIEW_MODE'] && !array_key_exists('DESCRIPTION', $arCurrent))
		{
			$boolDescr = true;
			$arSelect[] = 'DESCRIPTION';
			$arSelect[] = 'DESCRIPTION_TYPE';
		}
	}
	if ($boolPicture || $boolDescr)
	{
		foreach ($arResult['SECTIONS'] as $key => $arSection)
		{
			$arMap[$arSection['ID']] = $key;
		}
		$rsSections = CIBlockSection::GetList(array(), array('ID' => array_keys($arMap)), false, $arSelect);
		while ($arSection = $rsSections->GetNext())
		{
			if (!isset($arMap[$arSection['ID']]))
				continue;
			$key = $arMap[$arSection['ID']];
			if ($boolPicture)
			{
				$arSection['PICTURE'] = intval($arSection['PICTURE']);
				$arSection['PICTURE'] = (0 < $arSection['PICTURE'] ? CFile::GetFileArray($arSection['PICTURE']) : false);
				$arResult['SECTIONS'][$key]['PICTURE'] = $arSection['PICTURE'];
				$arResult['SECTIONS'][$key]['~PICTURE'] = $arSection['~PICTURE'];
			}
			if ($boolDescr)
			{
				$arResult['SECTIONS'][$key]['DESCRIPTION'] = $arSection['DESCRIPTION'];
				$arResult['SECTIONS'][$key]['~DESCRIPTION'] = $arSection['~DESCRIPTION'];
				$arResult['SECTIONS'][$key]['DESCRIPTION_TYPE'] = $arSection['DESCRIPTION_TYPE'];
				$arResult['SECTIONS'][$key]['~DESCRIPTION_TYPE'] = $arSection['~DESCRIPTION_TYPE'];
			}
		}
	}
}
 */
 
$ID_TO_ROOT = [];
$arSort = array('depth_level' => 'asc');
$arFilter = Array("IBLOCK_ID"=>$arParams['IBLOCK_ID'], '>DEPTH_LEVEL' => '1');
$arSelect = Array("ID", "IBLOCK_SECTION_ID", "DEPTH_LEVEL");
$res = CIBlockSection::GetList($arSort, $arFilter, false, $arSelect);
while($arFields = $res->Fetch()) {
	$ID_TO_ROOT[$arFields['ID']] = $arFields['IBLOCK_SECTION_ID'];
}

$IDS = [];
$DEPTH_LEVEL = [];
foreach($arResult['SECTIONS'] as $arSection) {
	$IDS[] = $arSection['ID'];
	$DEPTH_LEVEL[$arSection['ID']] = $arSection['DEPTH_LEVEL'];
}
global $arGeoData;
$PRICE_CODE = $arGeoData['CUR_CITY']['PRICE_ID'];
$PRICE_CODE_NODISCOUNT = $arGeoData['CUR_CITY']['PRICE_NODISCOUNT_ID'];
$CNT = [];
$min_price = [];
$max_price = [];
$arFilter = Array("IBLOCK_ID"=>$arParams['IBLOCK_ID'], "SECTION_ID"=>$IDS, "INCLUDE_SUBSECTIONS"=>"Y");
if (!empty($arParams['FILTER'])) {
	$arFilter = array_merge($arFilter, $arParams['FILTER']);
}

$SECTIONS_FOR_PRICE_DIAP=[];
$arSelect = Array("ID", "SECTION_ID", "IBLOCK_SECTION_ID", "CATALOG_PRICE_".$PRICE_CODE);
$res = CIBlockElement::GetList(Array(), $arFilter, false, false, $arSelect);
while($arFields = $res->Fetch()) {
	$CNT[$arFields['IBLOCK_SECTION_ID']]++;

	if($SECTIONS_FOR_PRICE_DIAP[$arFields['IBLOCK_SECTION_ID']])$section_id=$SECTIONS_FOR_PRICE_DIAP[$arFields['IBLOCK_SECTION_ID']];
	else{
		if(!isset($DEPTH_LEVEL[$arFields['IBLOCK_SECTION_ID']]) || $DEPTH_LEVEL[$arFields['IBLOCK_SECTION_ID']]>2){
			$resNav=CIBlockSection::GetNavChain($arParams['IBLOCK_ID'],$arFields['IBLOCK_SECTION_ID']);
			while($elNav=$resNav->GetNext()){
				if($elNav["DEPTH_LEVEL"]==2){
					$section_id=$elNav['ID'];
					$SECTIONS_FOR_PRICE_DIAP[$arFields['IBLOCK_SECTION_ID']]=$elNav['ID'];
				}
			}
		}else{
			$SECTIONS_FOR_PRICE_DIAP[$arFields['IBLOCK_SECTION_ID']]=$arFields['IBLOCK_SECTION_ID'];
			$section_id=$arFields['IBLOCK_SECTION_ID'];
		}
	}
	if(!$min_price[$section_id] || $min_price[$section_id]>intval($arFields["CATALOG_PRICE_".$PRICE_CODE])) $min_price[$section_id]=intval($arFields["CATALOG_PRICE_".$PRICE_CODE]);
	if(!$max_price[$section_id] || $max_price[$section_id]<intval($arFields["CATALOG_PRICE_".$PRICE_CODE])) $max_price[$section_id]=intval($arFields["CATALOG_PRICE_".$PRICE_CODE]);
}

foreach($CNT as $ID => $CNT_NUM) {
	if (!empty($ID_TO_ROOT[$ID])) {
		$CAT_ID = $ID;
		while($ROOT_ID = $ID_TO_ROOT[$CAT_ID]) {
			$CNT[$ROOT_ID] += $CNT_NUM;
			$CAT_ID = $ID_TO_ROOT[$CAT_ID];
		}
	}
}

foreach($arResult['SECTIONS'] as $i=>$arSection) {
	$arResult['SECTIONS'][$i]['ELEMENT_CNT'] = intval($CNT[$arSection['ID']]);
	/*$arResult['SECTIONS'][$i]['SECTION_PAGE_URL'] = preg_replace('/^\/catalog/ui', '', $arSection['SECTION_PAGE_URL']);*/
	$arResult['SECTIONS'][$i]['MIN_PRICE'] = $min_price[$arSection['ID']];
	$arResult['SECTIONS'][$i]['MAX_PRICE'] = $max_price[$arSection['ID']];
	
}
