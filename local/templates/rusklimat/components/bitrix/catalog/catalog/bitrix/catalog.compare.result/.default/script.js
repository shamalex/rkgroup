BX.namespace("BX.Iblock.Catalog");

BX.Iblock.Catalog.CompareClass = (function()
{
	var CompareClass = function(wrapObjId)
	{
		this.wrapObjId = wrapObjId;
	};

	CompareClass.prototype.MakeAjaxAction = function(url)
	{
		BX.showWait(BX(this.wrapObjId));
		BX.ajax.post(
			url,
			{
				ajax_action: 'Y'
			},
			BX.proxy(function(result)
			{
				BX.closeWait();
				BX(this.wrapObjId).innerHTML = result;
				// BX.onCustomEvent('OnCompareChange');
				
				$('.checkset input, .check-custom').iCheck({
					checkboxClass: 'icheckbox_minimal',
					radioClass: 'iradio_minimal',
					activeClass: 'active'
				});
				if( $('.stars').length){
					$('.stars').each(function() {
						var score = $(this).attr('-data-score');
						$(this).raty({
							hints: ['1', '2', '3', '4', '5'],
							score: parseInt(score),
							readOnly: ($(this).attr('-data-canvote') == 1 ? false : true),
							path: window.SITE_TEMPLATE_PATH+'/i/d/rate'
						});
					});
					
				}
			}, this)
		);
	};

	return CompareClass;
})();