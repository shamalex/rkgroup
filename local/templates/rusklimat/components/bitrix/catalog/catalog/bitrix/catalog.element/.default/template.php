﻿<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);

global $arGeoData;

$strMainID = $this->GetEditAreaId($arResult['ID']);
$arItemIDs = array(
	'ID' => $strMainID,
	'PICT' => $strMainID.'_pict',
	'DISCOUNT_PICT_ID' => $strMainID.'_dsc_pict',
	'STICKER_ID' => $strMainID.'_sticker',
	'BIG_SLIDER_ID' => $strMainID.'_big_slider',
	'BIG_IMG_CONT_ID' => $strMainID.'_bigimg_cont',
	'SLIDER_CONT_ID' => $strMainID.'_slider_cont',
	'SLIDER_LIST' => $strMainID.'_slider_list',
	'SLIDER_LEFT' => $strMainID.'_slider_left',
	'SLIDER_RIGHT' => $strMainID.'_slider_right',
	'OLD_PRICE' => $strMainID.'_old_price',
	'PRICE' => $strMainID.'_price',
	'DISCOUNT_PRICE' => $strMainID.'_price_discount',
	'SLIDER_CONT_OF_ID' => $strMainID.'_slider_cont_',
	'SLIDER_LIST_OF_ID' => $strMainID.'_slider_list_',
	'SLIDER_LEFT_OF_ID' => $strMainID.'_slider_left_',
	'SLIDER_RIGHT_OF_ID' => $strMainID.'_slider_right_',
	'QUANTITY' => $strMainID.'_quantity',
	'QUANTITY_DOWN' => $strMainID.'_quant_down',
	'QUANTITY_UP' => $strMainID.'_quant_up',
	'QUANTITY_MEASURE' => $strMainID.'_quant_measure',
	'QUANTITY_LIMIT' => $strMainID.'_quant_limit',
	'BASIS_PRICE' => $strMainID.'_basis_price',
	'BUY_LINK' => $strMainID.'_buy_link',
	'ADD_BASKET_LINK' => $strMainID.'_add_basket_link',
	'BASKET_ACTIONS' => $strMainID.'_basket_actions',
	'NOT_AVAILABLE_MESS' => $strMainID.'_not_avail',
	'COMPARE_LINK' => $strMainID.'_compare_link',
	'PROP' => $strMainID.'_prop_',
	'PROP_DIV' => $strMainID.'_skudiv',
	'DISPLAY_PROP_DIV' => $strMainID.'_sku_prop',
	'OFFER_GROUP' => $strMainID.'_set_group_',
	'BASKET_PROP_DIV' => $strMainID.'_basket_prop',
);

$PRICE_CODE = $arParams['PRICE_CODE'][0];
$PRICE_CODE_NODISCOUNT = $arParams['PRICE_CODE'][1];


// p($arResult);

$pics = [];
$pdfs = [];
foreach($arResult['PROPERTIES'] as $PROPERTY) {
	if (!empty($PROPERTY['VALUE'])) {
		if (substr($PROPERTY['CODE'], 0, 7) == 'picture') {
			$pic = CFile::GetFileArray($PROPERTY['VALUE']);
			$pic['CODE'] = $PROPERTY['CODE'];
			$pics[] = $pic;
		} elseif (substr($PROPERTY['CODE'], 0, 4) == 'pdf_') {
			//p($PROPERTY);
			//$pdf = CFile::MakeFileArray($PROPERTY['VALUE']);
			$pdf = CFile::GetFileArray($PROPERTY['VALUE']);
			//p($pdf);
			$pdf['PROP'] = $PROPERTY;
			// if (!isset($pdfs[$pdf['PROP']['VALUE']])) {
				// $pdfs[$pdf['PROP']['VALUE']] = $pdf;
			// }
			$pdfs[] = $pdf;
		}
	}
}
// p($pics);
// p($pdfs);

$badProps = [
	'item_name',
];


// delivery

// p($arResult['GEO']);

$IS_BIG_SIZE = !empty($arResult['DISPLAY_PROPERTIES']['IS_BIG_SIZE']['VALUE']);
$ITEM_PRICE = $arResult['PRICES'][$PRICE_CODE]['VALUE'];
//p($arResult['GEO']);
$DELIVERY_PRICE = false;
$DELIVERY_TIME = false;
$DELIVERY_TIME_DAYS = 0;
$DELIVERY_TIME_DAYS_REAL = 0;
$DELIVERY_TIME_VAL = false;
$DELIVERY2_TIME_DAYS = 0;
$DELIVERY2_TIME_DAYS_REAL = 0;
$DELIVERY2_TIME_VAL = false;
if (!$IS_BIG_SIZE) {
	if ($ITEM_PRICE >= 1000 && $ITEM_PRICE <= 2999) {
		$DELIVERY_PRICE = $arResult['GEO']['PROPERTIES']['tarif_1']['VALUE'];
	} elseif ($ITEM_PRICE >= 3000 && $ITEM_PRICE <= 5999) {
		$DELIVERY_PRICE = $arResult['GEO']['PROPERTIES']['tarif_2']['VALUE'];
	} elseif ($ITEM_PRICE >= 6000 && $ITEM_PRICE <= 8999) {
		$DELIVERY_PRICE = $arResult['GEO']['PROPERTIES']['tarif_3']['VALUE'];
	} elseif ($ITEM_PRICE >= 9000) {
		$DELIVERY_PRICE = $arResult['GEO']['PROPERTIES']['tarif_4']['VALUE'];
	}
} else {
	if ($ITEM_PRICE >= 1000 && $ITEM_PRICE <= 2999) {
		$DELIVERY_PRICE = $arResult['GEO']['PROPERTIES']['tarif_5']['VALUE'];
	} elseif ($ITEM_PRICE >= 3000 && $ITEM_PRICE <= 5999) {
		$DELIVERY_PRICE = $arResult['GEO']['PROPERTIES']['tarif_6']['VALUE'];
	} elseif ($ITEM_PRICE >= 6000 && $ITEM_PRICE <= 8999) {
		$DELIVERY_PRICE = $arResult['GEO']['PROPERTIES']['tarif_7']['VALUE'];
	} elseif ($ITEM_PRICE >= 9000) {
		$DELIVERY_PRICE = $arResult['GEO']['PROPERTIES']['tarif_8']['VALUE'];
	}
}
if ($DELIVERY_PRICE == 'нет') {
	$DELIVERY_PRICE = false;
}
if (!empty($arResult['DISPLAY_PROPERTIES'][$arParams['QUANITY_PROP']]['VALUE'])) {
	$DELIVERY_TIME_DAYS = $arResult['GEO']['PROPERTIES']['shipping_1']['VALUE'];
	$DELIVERY2_TIME_DAYS = $arResult['GEO']['PROPERTIES']['samov_1']['VALUE'];
} elseif (!empty($arResult['DISPLAY_PROPERTIES'][$arParams['QUANITY_PROP2']]['VALUE'])) {
	$DELIVERY_TIME_DAYS = $arResult['GEO']['PROPERTIES']['shipping_2']['VALUE'];
	$DELIVERY2_TIME_DAYS = $arResult['GEO']['PROPERTIES']['samov_2']['VALUE'];
}

if (!empty($DELIVERY_TIME_DAYS)) {
	if ($DELIVERY_TIME_DAYS == 'сегодня раб/врем') {
		$DELIVERY_TIME_DAYS = 0;
	} elseif ($DELIVERY_TIME_DAYS == 'к осн фил') {
		$DELIVERY_TIME_DAYS = $arResult['GEO']['PROPERTIES']['shipping_2']['VALUE'];
	} elseif ($DELIVERY_TIME_DAYS == 'нет') {
		$DELIVERY_TIME_DAYS = false;
		$DELIVERY_TIME_VAL = '';
	}
	if ($DELIVERY_TIME_DAYS !== false) {
		$one_day = 86400;
		$cur_time = time();
		$today = $cur_time;
		$days_left = $DELIVERY_TIME_DAYS;

		while ($days_left > 0) {
			$today += $one_day;
			$dow = intval(date('w', $today));
			if ($dow != 0 && $dow != 6) {
				$days_left -= 1;
			}
			$DELIVERY_TIME_DAYS_REAL += 1;
		}
		$DELIVERY_TIME_VAL = date('j '.GetMonthForDelivery($today), $today);
		// if ($DELIVERY_TIME_DAYS_REAL == 0) {
			// $DELIVERY_TIME_VAL = 'Сегодня, '.$DELIVERY_TIME_VAL;
		// } elseif ($DELIVERY_TIME_DAYS_REAL == 1) {
			// $DELIVERY_TIME_VAL = 'Завтра, '.$DELIVERY_TIME_VAL;
		// }
	}
}
if (!empty($DELIVERY2_TIME_DAYS)) {
	if ($DELIVERY2_TIME_DAYS == 'сегодня раб/врем') {
		$DELIVERY2_TIME_DAYS = 0;
	} elseif ($DELIVERY2_TIME_DAYS == 'к осн фил') {
		$DELIVERY2_TIME_DAYS = $arResult['GEO']['PROPERTIES']['shipping_2']['VALUE'];
	} elseif ($DELIVERY2_TIME_DAYS == 'нет') {
		$DELIVERY2_TIME_DAYS = false;
		$DELIVERY2_TIME_VAL = '';
	}
	if ($DELIVERY2_TIME_DAYS !== false) {
		$one_day = 86400;
		$cur_time = time();
		$today = $cur_time;
		$days_left = $DELIVERY2_TIME_DAYS;

		while ($days_left > 0) {
			$today += $one_day;
			$dow = intval(date('w', $today));
			if ($dow != 0 && $dow != 6) {
				$days_left -= 1;
			}
			$DELIVERY2_TIME_DAYS_REAL += 1;
		}
		$DELIVERY2_TIME_VAL = date('j '.GetMonthForDelivery($today), $today);
		if ($DELIVERY2_TIME_DAYS_REAL == 0) {
			$DELIVERY2_TIME_VAL = 'Сегодня, '.$DELIVERY2_TIME_VAL;
		} elseif ($DELIVERY2_TIME_DAYS_REAL == 1) {
			$DELIVERY2_TIME_VAL = 'Завтра, '.$DELIVERY2_TIME_VAL;
		}
	}
}

// p($arParams['QUANITY_PROP']);
// p($arResult['DISPLAY_PROPERTIES'][$arParams['QUANITY_PROP']]['VALUE']);
// p($arParams['QUANITY_PROP2']);
// p($arResult['DISPLAY_PROPERTIES'][$arParams['QUANITY_PROP2']]['VALUE']);

// p($arResult['GEO']);

?>
<div class="popup-kp"></div>

<!-- card -->
<div class="card c">
	<!-- left column holder -->
	<div class="col-l">
		<!-- left column w -->
		<div class="w brd c c-pic-h">
			<!-- row 1 -->
			<div class="row-1 c">
				<h1 class="ttl"><?= !empty($arResult['DISPLAY_PROPERTIES']['item_name']['DISPLAY_VALUE']) ? $arResult['DISPLAY_PROPERTIES']['item_name']['DISPLAY_VALUE'] : $arResult['NAME'] ?></h1>
				<? if(!empty($arResult['DISPLAY_PROPERTIES']['EL_BRAND']['VALUE'])): ?>
					<? if(!empty($arResult['DISPLAY_PROPERTIES']['EL_BRAND']['LINK_ELEMENT_VALUE'][$arResult['DISPLAY_PROPERTIES']['EL_BRAND']['VALUE']]['PREVIEW_PICTURE']['SRC'])): ?>
						<a href="<?= $arResult['DISPLAY_PROPERTIES']['EL_BRAND']['LINK_ELEMENT_VALUE'][$arResult['DISPLAY_PROPERTIES']['EL_BRAND']['VALUE']]['DETAIL_PAGE_URL'] ?>" class="brand-logo pic" style="background-image:url('<?= $arResult['DISPLAY_PROPERTIES']['EL_BRAND']['LINK_ELEMENT_VALUE'][$arResult['DISPLAY_PROPERTIES']['EL_BRAND']['VALUE']]['PREVIEW_PICTURE']['SRC'] ?>');"></a>
					<? else: ?>
						<a href="<?= $arResult['DISPLAY_PROPERTIES']['EL_BRAND']['LINK_ELEMENT_VALUE'][$arResult['DISPLAY_PROPERTIES']['EL_BRAND']['VALUE']]['DETAIL_PAGE_URL'] ?>" class="brand-logo pic"><?= $arResult['DISPLAY_PROPERTIES']['EL_BRAND']['LINK_ELEMENT_VALUE'][$arResult['DISPLAY_PROPERTIES']['EL_BRAND']['VALUE']]['NAME'] ?></a>
					<? endif; ?>
				<? endif; ?>
			</div>
			<!-- / row 1 end -->

			<!-- row 2 -->
			<div class="row-2 c">
				<div class="f-l">
					<div class="rate">
						<div class="stars" -data-score="<?= $arResult['TOTAL_RATE'] ?>"></div>
						<a href="#tabRevw" class="lnk" id="move_to_review_tab">Отзывов: <?= count($arResult['REVIEWS']) ?></a>
						<script>
							$(function() {
								$('#move_to_review_tab').click(function(e) {
									$('.li-tab-revw > a').trigger('click');
									$('html, body').animate({ scrollTop: $('#cardTabs').offset().top - 60 }, 500);
								});
							});
						</script>
					</div>
					<? if(!empty($arResult['DISPLAY_PROPERTIES']['CODE']['DISPLAY_VALUE'])): ?>
						<div class="article">код товара: <span><?= $arResult['DISPLAY_PROPERTIES']['CODE']['DISPLAY_VALUE'] ?></span></div>
					<? endif; ?>
				</div>
				<!--
				<div class="chars">
					<div class="item brd">
						<div class="w c">
							<span class="nmb">40 м²</span>
							<span class="dsc">площадь<br/>помещения</span>
						</div>
					</div>
					<div class="item brd">
						<div class="w c">
							<span class="nmb">56°</span>
							<span class="dsc">температурный<br/>режим</span>
						</div>
					</div>
				</div>
				-->
			</div>
			<!-- / rowr 2 end -->

			<? if(!empty($pics)): ?>
				<!-- pic container -->
				<a href="#" class="c-pic-container maximize">

					<?if(count($arResult['LABELS'])>0){
						//p($arResult['LABELS']);
					?>
					<div class="icos">
						<?foreach($arResult['LABELS'] as $k => $label){?>
						<div id="label-<?=$k?>">
							<?=$label['VALUE']?>
						</div>
						<?}?>
					</div>
					<?}?>
					<div class="c-pic pic" id="cardGal-Pic"<? if(!empty($pics)): ?> style="background-image:url('<?= $pics[0]['SRC'] ?>');"<? endif; ?>>
					</div>

				</a>
				<!-- pic container end -->

				<noindex>
					<div class="print_only main"><img src="<?= $pics[0]['SRC'] ?>" alt="image"></div>
					<div class="print_only">
						<div>
							<? foreach($pics as $i=>$pic): ?>
								<img src="<?= $pic['SRC'] ?>" alt="image">
							<? endforeach; ?>
						</div>
					</div>
				</noindex>

				<!-- pic nav -->
				<ul class="c-gal c" id="cardGal-Nav">
					<? if(!empty($pics)): ?>
						<? foreach($pics as $i=>$pic): ?>
							<? if($pic['CONTENT_TYPE'] == 'application/octet-stream'): ?>
								<li class="d3"><a <?= $i == 0 ? 'class="active"' : '' ?> href="<?= $pic['SRC'] ?>"></a></li>
							<? else: ?>
								<li data-image="<?= $pic['SRC'] ?>" style="background-image:url('<?= $pic['SRC'] ?>'); background-size: contain; background-repeat: no-repeat; background-position: center;"><a <?= $i == 0 ? 'class="active"' : '' ?> href="<?= $pic['SRC'] ?>"></a></li>
							<? endif; ?>
						<? endforeach; ?>
					<? else: ?>
						<li></li>
					<? endif; ?>
				</ul>
				<!-- pic nav end -->
			<? endif; ?>

		</div>
		<!-- / left column w end -->

		<?
		$paramsTab = false;
		foreach($arResult['DISPLAY_PROPERTIES'] as $DISPLAY_PROPERTY) {
			if (in_array($DISPLAY_PROPERTY['CODE'], $badProps) || $DISPLAY_PROPERTY['SORT'] < 100 || in_array(substr($DISPLAY_PROPERTY['CODE'], 0, 4), ['pdf_', 'seo_']) || $DISPLAY_PROPERTY['USER_TYPE'] == 'HTML') {
				continue;
			}
			$paramsTab = true;
			break;
		}
		?>
<?
if(CFile::GetPath($arResult['PROPERTIES']['html_ru_new']['VALUE'])!=''){
	$HTML_DESCR=file_get_contents($_SERVER['DOCUMENT_ROOT'].CFile::GetPath($arResult['PROPERTIES']['html_ru_new']['VALUE']));
}
?>
<?//$HTML_DESCR=gzinflate(substr(file_get_contents($_SERVER['DOCUMENT_ROOT'].CFile::GetPath($arResult['PROPERTIES']['html_ru_new']['VALUE'])), 10));?>
<?//=$_SERVER['DOCUMENT_ROOT'].CFile::GetPath($arResult['PROPERTIES']['html_ru_new']['VALUE']);?>
		<!-- tabs -->
		<div class="tabs" id="cardTabs">
			<!-- tabs labels -->
			<ul class="tabs-ul c">
				
				<? if (!empty($HTML_DESCR)): ?>
					<li class="li-tab-desc"><a href="#tabDesc" data-tab-id="tabDesc"><span><h2>Описание</h2></span></a></li>
				<? endif; ?>
				<? if($paramsTab): ?>
					<li class="li-tab-char"><a href="#tabChar" data-tab-id="tabChar"><span><h2>Характеристики</h2></span></a></li>
				<? endif; ?>
				<li class="li-tab-revw"><a href="#tabRevw" data-tab-id="tabRevw"><span><h2>Отзывы (<?= count($arResult['REVIEWS']) ?>)</h2></span></a></li>
				<? /* if(!empty($arResult['DISPLAY_PROPERTIES'][$arParams['QUANITY_PROP']]['VALUE']) || !empty($arResult['DISPLAY_PROPERTIES'][$arParams['QUANITY_PROP2']]['VALUE'])): */ ?>
				<? if(!empty($arResult['PRICES'][$PRICE_CODE]['VALUE']) && (($DELIVERY_PRICE !== false && !empty($DELIVERY_TIME_VAL)) || ($DELIVERY2_TIME_VAL !== false))): ?>
					<li class="li-tab-avai li-two-lines"><a href="#tabAvai" data-tab-id="tabAvai"><span><h3>Условия<br />самовывоза / доставки</span></h3></a></li>
				<? endif; ?>
			</ul>
			<!-- / tabs labels end -->

			<!-- tabs content w -->
			<div class="w brd c">

				<? if (!empty($HTML_DESCR)): ?>
					<!-- description -->
					<div class="tab tab-desc" id="tabDesc">
						<?=iconv('CP1251','UTF-8',$HTML_DESCR)?>
					</div>
					<!-- / description end -->
				<? endif; ?>

				<? if($paramsTab): ?>
					<!-- characteristics -->
					<div class="tab tab-char" id="tabChar">
						<? if(!empty($pdfs)): ?>
							<!-- instructions -->
								<? foreach($pdfs as $pdf): ?>
									<!-- link -->
									<div class="inst-item">
										<div class="dsc c">
											<img title="pdf файл" class="pdf-icon" src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAACAAAAAgCAYAAABzenr0AAAF6UlEQVRYR62XaWxUVRTHf/fNm5l22k6pdhAKCKECJUVrsEVSNxAXorRNQRJi4hKMftAoYkz0C2qMfjAuEUlcENSoQSwg0CFRE3FBxMIQKctIQShNWbqxONPpdLrMu+beaWs79rUT8CaTTt97c+/vnPM/yxMMXWZ1dfVGIcSSpOuX9W9PT8/mxYsXLwPidhuIpBtZfr8/vGjRoss6MPlHzc3N7N69e/PSpUttIZIBcv1+f5sC+KP2IIZhjA4iBEg58JxlWcy+sYgtW7awZMkSmpqaqKmp2blq1ar7gsFgd/KGyQBj/X5/iwI4HDyKwzECgBBI08QIhZGZHrASEPG4xfWFM9m2bRt33DGPnJwxGiIQCDRUVFRMB3oGQ9gCBOuO4RjBA9JpkrV6nba+fcXjiN7eBIBlUVgwQwPcu3AhkfYIPl+uhti7d29DZWXlEAhbgGPH/xoxBNLlJGPNenpuvZmeokLoA1AhmDF9Glu3bqWyspLOWIxYZ0x7QmkiEAicLi8vz+/3hC3AXyfrRwd4bz2OYB3htW8jurq0BxTAtPyp1NbWcurUKYTSiA5NXGtix44dlJWVjQFC6rotQP2phpFFaJo4q7/HtXE7kU1rEZ0xfZCUkqysTHKvvnpYAfcBXAO0jgjQ0Ng4KoCjNoj7jTV0fPMJoiM6cKCCUJ/By5KSqZMn93tgdIDTZ86ODOByYa5Zh+v9T+nctR151RilQNu0VaGZNHFC6gBnm5sx+uL3n13V9QwP7tIyel9/AccHn9Nd9RF0dA6pCckemDBuXOoALa1tAwL6D4DTidjsx9i1h/j6dzFWvgRCYL35MnR3g/a+THjEsjSUCsk1Y32pA5y/cGF4AGV9ZgbiurnIXdthnA/cbsRNd0O2Fzm/NMHbG4db5sDc2fq7AlDCTFmEFy9dGh7A5YQqP/LLTXD/XYi2CxBqh2lTIX8yfPY18pc9sLgMzjUjNq2Frm4NcFVOTuoAoVB4KICy3OVENrdilT+M8dqLiLnF4M1MWKxUr1xvOiDSgTxwWLtflM7RRUoBZGd7Uwdoj0T+BVAlOc1N/JW3kME6jNtLMZ56VFs2uBENaEXB9pfxQRrIysxMHSAajf4L4HbTtXwFjoqF9K7+mLSft0EsZqv44XJRecDj8aQO0KXU3L9cLqJF8xE5Y/B8uwHVB0bKebti4Ha5Ugfo7WsuejOHA+tME45JeUhpXdbhahvTNEcH2L7zh5byOxfYVrQrvVH9404qFtxlX4rrPNktM0pKqAsEECNNREr1dpVyGEppWRSUlHAsEKAgGrIHOJo7vqVgzhwagsFEL4jHkbEYagPdWlUVdDgQ6enEL17URwlhgNNEuFw69YY87zARbheqS0wpLKRu3z5mnm8aBaCkhHMnT2oA2RFlfOOJAZuiG6v4+5mV5LWeHWJntGozoedf0Jkx/nT9wL14YyOt8+/R41tefr72bEoA58+c0RbL9gi59XW0jp2Ac/Zscr7zc2nBQnJ2fsffDyyjJ7Afw+vF+8nHyFCI0CPL8Z1toHXcJERGBqLPA1IIcidOTB0g3JZoRjISwfvnQcJTCxDZWWQdCNDx4ENkbPiC6GNPEK89pAdSo2gWGZ+tJ3TtdWQ3niA8ZToiw6NDpj6qSHp9vtQBusLhhMgiEdwH9tG7aQtmRRm0txMrnUfascN0P/Ek8uAh7XZxww241n1AbPos0o4fGQiB+l38ndU6ld1eb4oAxcVYPX3Tc0cHxu+7kE8+DWlpyJoAxHsx9u5GvrMaeaJe9wPx3LOIfQGsl17F+KMGq6gEPP0eMLUxhtNJ3f79KWiguFirX6/OTvj1RyidB570RI3vvzZYhl9VwfsfJt4PfvsJbrsT0tOHJqTDkQKAL69F5aseJNRSf5U3VCwHNxg1BauBQ421KlSmmXhG1Yfk5/sxDCMRgrZz9ml40Jd3xAW+YbvclZZBIeiGtqK2c7PspuJsVS8ANVMnj+xXeryeGoALQIPde4ET8ADu/+M0mz3UG4ya4bXK/wGltwNO6mSzSgAAAABJRU5ErkJggg==" alt="PDF">
											<a href="<?= $pdf['SRC']?>" target="_blank"><?= !empty($pdf['PROP']['DESCRIPTION']) ? $pdf['PROP']['DESCRIPTION'] : $pdf['PROP']['NAME'] ?></a>  [ <?= ConvertBytes($pdf['FILE_SIZE']) ?> ]
										</div>
									</div>
									<!-- / link -->
								<? endforeach; ?>
							<!-- / instructions end -->
						<? endif; ?>
						<!-- tbl-char -->
						<table class="tbl tbl-char">
							<?foreach ($arResult['PROPS_GROUPS'] as $k => $v) {
	$whas=false;
	$text='<tr class="sp">
										<td colspan="2">&nbsp;</td>
									</tr><tr>
										<td colspan="2" style="text-align:center;font-weight:bold;">'.$v['NAME'].'</td>
									</tr>';
	$text.='<tr class="sp">
										<td colspan="2">&nbsp;</td>
									</tr>';
	foreach ($v['PROPERTY_PROPS_VALUE'] as $k2 => $v2) {
		if($arResult['PROPS_XML_IDS'][$v2]['VALUE']!=''){
			//p($arResult['PROPS_XML_IDS'][$v2]);
			if($whas){
				$text.='<tr class="sp">
										<td colspan="2">&nbsp;</td>
									</tr>';
			}else{
				$whas=true;	
			}
			$text.='<tr>
									<td>'.$arResult['PROPS_XML_IDS'][$v2]['NAME'].'</td>
									<td style="width:160px">';
			$text.=strlen($arResult['PROPS_XML_IDS'][$v2]['DISPLAY_VALUE']) > 0 ? $arResult['PROPS_XML_IDS'][$v2]['DISPLAY_VALUE'].(!empty($arResult['PROPS_XML_IDS'][$v2]['HINT']) ? ' '.$arResult['PROPS_XML_IDS'][$v2]['HINT'] : '') : "[ пустое значение ]";
			$text.='</td>
								</tr>';

		}
		
	}
	if($whas){
			echo $text;
		}
}?>
						</table>
						<!-- / tbl-char end -->

					</div>
					<!-- / characteristics end -->
				<? endif; ?>

				<!-- reviews -->
				<div class="tab tab-revw" id="tabRevw">

					<!-- add review -->
					<div class="revw-add c">
						<div class="btn btn-30 btn-revw-add">Написать отзыв</div>
						<!--<div class="count"><span>12</span> человек уже оценили этот товар по достоинству, народ рекомендует!</div>-->
						<div class="form-revw-add">
							<div class="result"></div>
							<form>
								<label class="ttl">Оценка</label>
								<div class="dsc">
									<div class="stars" -data-score="0" -data-canvote="1"></div>
								</div>
								<label class="ttl">ФИО</label>
								<div class="dsc">
									<input class="inp inp-drk inp-t inp-lk" name="NAME" maxlength="255" value="<?=$USER->GetFullName();?>" type="text" />
								</div>

								<label class="ttl">Текст отзыва</label>
								<div class="dsc">
									<textarea name="COMMENT" class="txtarea txtarea-drk txtarea-lk order-txt"></textarea>
								</div>
								<input type="hidden" name="product" value="<?=$arResult["ID"]?>" />
								<input type="hidden" name="city" value="<?=$GLOBALS['arGeoData']['CUR_CITY']['NAME']?>" />
								<div class="btn btn-30 btn-revw-send">Отправить отзыв</div>
							</form>
						</div>
					</div>
					<!-- / add review end -->

					<? foreach($arResult['REVIEWS'] as $arReview): ?>
						<!-- item -->
						<div class="item">
							<div class="ttl c">
								<div class="stars" -data-score="<?= intval($arReview['grade'])+3 ?>"></div>
								<div class="name"><?= !empty($arReview['author']) ? $arReview['author'] : 'Аноним' ?></div>
								<?if($arReview['city']){?><div class="city"><?=$arReview['city']?></div><?}?>
							</div>
							<?if($arReview['item_yandex_id']){?>
							<div class="ya-each">
								<a class="ya-each-lnk" target="_blank" href="https://market.yandex.ru/product/<?= $arReview['item_yandex_id'] ?>/reviews?track=tabs">
									<span class="ya-each-ltr">Я</span>ндекс.Маркет
								</a>
							</div>
							<?}?>
							<div class="dsc">
								<p><?= $arReview['text']['TEXT'] ?></p>
								<? if(!empty($arReview['pro']['TEXT'])): ?>
									<p><b>Достоинства:</b> <?= $arReview['pro']['TEXT'] ?></p>
								<? endif; ?>
								<? if(!empty($arReview['contra']['TEXT'])): ?>
									<p><b>Недостатки:</b> <?= $arReview['contra']['TEXT'] ?></p>
								<? endif; ?>
							</div>
						</div>
						<!-- / item -->
					<? endforeach; ?>

					<? if(!empty($arResult['REVIEWS']) && $arResult['REVIEWS'][0]['item_yandex_id']): ?>
						<a href="https://market.yandex.ru/product/<?= $arResult['REVIEWS'][0]['item_yandex_id'] ?>/reviews?track=tabs" target="_blank" class="yam-link" onclick="yaCounter1451879.reachGoal('YA_OTZYV'); return true;"><div class="btn btn-30 btn-revw-ya">Все отзывы на Яндекс.Маркете</div></a>
					<? endif; ?>

				</div>
				<!-- / reviews end -->


				<? /* if(!empty($arResult['DISPLAY_PROPERTIES'][$arParams['QUANITY_PROP']]['VALUE']) || !empty($arResult['DISPLAY_PROPERTIES'][$arParams['QUANITY_PROP2']]['VALUE'])): */ ?>
				<? if(!empty($arResult['PRICES'][$PRICE_CODE]['VALUE']) && (($DELIVERY_PRICE !== false && !empty($DELIVERY_TIME_VAL)) || ($DELIVERY2_TIME_VAL !== false))): ?>
					<!-- availability -->
					<div class="tab tab-avai" id="tabAvai">
						<?

						$MAIN_CITY = $arResult['GEO']['PROPERTIES']['network_name']['VALUE'];
						$MAIN_CITY = str_replace('Русклимат ', '', $MAIN_CITY);
						?>
						<!-- tbl-avai -->

						<? if($DELIVERY_PRICE !== false && !empty($DELIVERY_TIME_VAL)): ?>
							<p class="ttl2">Доставка товара по г. <?= $arResult['GEO']['NAME'] ?></p>
							<table class="tbl tbl-avai">
								<tr>
									<th>Варианты доставки</th>
									<th style="width:120px">Дата доставки</th>
									<th style="width:160px">Стоимость доставки</th>
								</tr>
								<tr class="sp">
									<td colspan="3">&nbsp;</td>
								</tr>
								<? if($DELIVERY_TIME_VAL != ''): ?>
									<tr>
										<td>Доставка Русклимат</td>
										<td><?= $DELIVERY_TIME_VAL ?></td>
										<td><?= ($DELIVERY_PRICE == 0 ? 'бесплатно' : number_format($DELIVERY_PRICE, 0, '.', ' ').' руб.') ?></td>
									</tr>
								<? endif; ?>

							</table>
						<? endif; ?>
						<? if($DELIVERY2_TIME_VAL !== false && !empty($arResult['LOCATIONS'])): ?>
							<div><br/></div>
							<p class="ttl2">Самовывоз в г. <?= $MAIN_CITY ?></p>
							<table class="tbl tbl-avai">
								<tr>
									<th>Варианты самовывоза</th>
									<th style="width:120px">Дата самовывоза</th>
									<th style="width:160px">Стоимость самовывоза</th>
								</tr>
								<? foreach($arResult['LOCATIONS'] as $i=>$arLocation): ?>
									<? if($i >= 0): ?>
										<tr class="sp">
											<td colspan="3">&nbsp;</td>
										</tr>
									<? endif; ?>
									<tr>
										<td><b><?= $arLocation['NAME'] ?> </b><br><?= $MAIN_CITY ?>, <?= $arLocation['PROPERTIES']['address']['VALUE'] ?></td>
										<td><?= !empty($DELIVERY2_TIME_VAL) ? $DELIVERY2_TIME_VAL : '&nbsp;' ?></td>
										<td><?= !empty($DELIVERY2_TIME_VAL) ? 'бесплатно' : '&nbsp;' ?></td>
									</tr>
								<? endforeach; ?>
							</table>
						<? endif; ?>
						<!-- / tbl-avai end -->

						<? if($DELIVERY2_TIME_VAL !== false): ?>
							<!-- gmap -->
							<div class="map">
								<!-- gmap header -->
								<div class="map-hd c">
									<div class="map-city l">
										<div class="map-city-ttl">Ваш город</div>
										<div class="ik_select map-select" id="map-city-select" style="position: relative;"><div class="ik_select_link map-select-link ik_select_link_novalue"><div class="ik_select_link_text"><?=$arGeoData['CUR_CITY']['NAME']?></div></div></div>
										<?/*
										<select name="" id="map-select">
											<option value="">Населенный</option>
											<option value="">Пункт</option>
											<option value="">Александровск-Сахалинский</option>
											<option value="">Населенный</option>
											<option value="">Электрозаводск</option>
											<option value="">Населенный</option>
											<option value="">Населенный</option>
											<option value="">Пункт</option>
											<option value="">Александровск-Сахалинский</option>
											<option value="">Населенный</option>
											<option value="">Электрозаводск</option>
											<option value="">Населенный</option>
										</select>
										*/?>
									</div>
								</div>
								<!-- / gmap header end -->

								<!-- gmap holder (gmap.js) -->
								<!--<div id="map" class="map-container map-inner"></div>-->
								<?= $arParams['~CATALOG_STORE_LIST_CART'] ?>
								<!-- / gmap holder end -->

							</div>
							<!-- / gmap -->
						<? endif; ?>

					</div>
					<!-- / availability end -->
				<? endif; ?>

			</div>
			<!-- / tabs content w end-->
		</div>
		<!-- / tabs end -->

	</div>
	<!-- / left column holder end -->

	<!-- right column holder -->
	<div class="col-r">
		<!-- control -->
		<div class="ctrl">
			<!-- control w -->
			<div class="w">

				<!-- actions -->
				<div class="actions c">
					<noindex>
					<fieldset class="checkset actionset">
						<?
						$href = str_replace('#ID#', $arResult['ID'], $arResult['COMPARE_URL_TEMPLATE']).'&ajax_action=Y';
						$href2 = str_replace([$arParams['ACTION_VARIABLE'].'=ADD_TO_COMPARE_LIST', $arParams['PRODUCT_ID_VARIABLE'].'='], ['action_footer=DELETE_FROM_COMPARE_LIST', 'add_id_footer='], $href);
						?>
						<input type="checkbox" class="addComp" id="addComp_<?= $arResult['ID'] ?>" -data-href="<?= $href ?>" -data-href2="<?= $href2 ?>"><label for="addComp_<?= $arResult['ID'] ?>">Сравнить</label>
						<span><input type="checkbox" class="addFav" id="addFav_<?= $arResult['ID'] ?>" -data-id="<?= $arResult['ID'] ?>"><label for="addFav_<?= $arResult['ID'] ?>">В избранное</label></span>
					</fieldset>
					</noindex>
					<a href="#" class="print" title="Распечатать страницу" rel="nofollow" onclick="window.print();">Распечатать</a>
				</div>
				<!-- / actions end -->

				<!-- prices -->
				<div class="prices">
					<div class="price"><?= $arResult['PRICES'][$PRICE_CODE]['PRINT_VALUE'] ?></div>
					<? if(!empty($arResult['PRICES'][$PRICE_CODE_NODISCOUNT]['PRINT_VALUE'])): ?>
						<div class="price-old"><?= $arResult['PRICES'][$PRICE_CODE_NODISCOUNT]['PRINT_VALUE'] ?></div>
					<? endif; ?>
				</div>
				<!-- / prices end -->

				<? if(!empty($arResult['TIME_LIMITS'])): ?>
					<!-- timer -->
					<div class="timer">
						<? if(!empty($arResult['PRICES'][$PRICE_CODE_NODISCOUNT]['PRINT_VALUE'])): ?>
							<?
							$diff = $arResult['PRICES'][$PRICE_CODE]['VALUE'] - $arResult['PRICES'][$PRICE_CODE_NODISCOUNT]['VALUE'];
							$diff = ceil($diff);
							$diff = abs($diff);
							?>
							<div class="sum">Успей сэкономить <?= number_format($diff, 0, '.', ' ') ?> <?= getNumEnding($diff, ['рубль', 'рубля', 'рублей']) ?></div>
						<? else: ?>
							<div class="sum">Успей купить</div>
						<? endif; ?>
						<div class="time" data-countdown="<?= $arResult['TIME_LIMITS'] ?>"></div>
					</div>
					<!-- / timer end -->
				<? endif; ?>

				<? if($arResult['DISPLAY_PROPERTIES']['ORDER_WITH_SETUP']['VALUE'] == 1): ?>
					<!-- install -->
					<div class="install">
						<fieldset class="checkset">
							<div class="checkline">
								<input type="checkbox" id="plusInstall" name="ORDER_WITH_SETUP" /><label for="plusInstall">Хочу заказать с установкой </label>
								<a href="#" class="que que-sm ttSwitch3">?</a>
								<div id="ttSwitch3" style="display:none">
									<div class="tt tt-switch tt-c">
										<?$APPLICATION->IncludeFile("include/ttSwitch3.php", Array(), Array("MODE"=>"html", "SHOW_BORDER"=>false));?>
									</div>
								</div>
							</div>
						</fieldset>
					</div>
					<!-- / install end -->
				<? endif; ?>

				<? if(!empty($arResult['PRICES'][$PRICE_CODE]['VALUE'])): ?>
					<!-- buy btns -->
					<div class="buy-btns">
						<?php /* проверка. если товар уже в корзине*/?>
						<?if($arResult["CAN_BUY"]):?>
							<?if (CModule::IncludeModule("sale"))
							{
								$arBasketItems = array();

								$dbBasketItems = CSaleBasket::GetList(
								array(),
								array(
									"FUSER_ID" => CSaleBasket::GetBasketUserID(),
									"LID" => SITE_ID,
									"PRODUCT_ID" => $arResult["ID"],
									"DELAY" => "N",
									"ORDER_ID" => "NULL"
									),
								false,
								false,
								array("QUANTITY")
								);

								$i = 0;
								while ($arItems = $dbBasketItems->Fetch()):?>
									<script>$(document).ready(function(){$("#<? echo $arItemIDs['ADD_BASKET_LINK']; ?>").attr("href", "#").addClass("in_basket").text("Уже в корзине");});</script>
								<?endwhile;?>
							<?}?>
						<?endif;?>
						<?php /* конец проверки, если товар уже в корзине*/?>
						<?/*<a class="btn btn-to-bsk btn-50" id="btnToBasket" href="<?= $arResult['ADD_URL'] ?>"><span>Купить</span></a>*/?>
						<a class="btn btn-to-bsk btn-50 bx_big bx_bt_button bx_cart" id="<? echo $arItemIDs['ADD_BASKET_LINK']; ?>" href="<?= $arResult['ADD_URL'] ?>" data-href_setup="<?= $arResult['ADD_URL'] ?>&ORDER_WITH_SETUP=Y"><span>Купить</span></a>
						<?
						$cats = [];
						foreach($arResult['SECTION']['PATH'] as $arSection) {
							$cats[] = $arSection['NAME'];
						}
						?>
						<script>
							$('#<? echo $arItemIDs['ADD_BASKET_LINK']; ?>').on('click', function() {
								window.dataLayer = window.dataLayer || [];
								window.dataLayer.push({
									'event': 'addToCart',
									"ecommerce": {
										"currencyCode": "RUB",
										"add": {
											"products": [
												{
													"id": "<?= $arResult['DISPLAY_PROPERTIES']['CODE']['DISPLAY_VALUE'] ?>",
													"name" : "<?= !empty($arResult['DISPLAY_PROPERTIES']['item_name']['DISPLAY_VALUE']) ? $arResult['DISPLAY_PROPERTIES']['item_name']['DISPLAY_VALUE'] : $arResult['NAME'] ?>",
													"price": <?= number_format($arResult['PRICES'][$PRICE_CODE]['VALUE'], 2, '.', '') ?>,
													"brand": "<?= $arResult['DISPLAY_PROPERTIES']['EL_BRAND']['LINK_ELEMENT_VALUE'][$arResult['DISPLAY_PROPERTIES']['EL_BRAND']['VALUE']]['NAME'] ?>",
													"category": "<?= implode('/', $cats) ?>",
													"quantity": 1
												}
											]
										}
									}
								});
							});
						</script>
						<div class="btn btn-1-click btn-30 one-click-pop" id="btn1ClickBuy">купить в 1 клик</div>
						<a class="credit-pop btn btn-credit btn-30" id="btnCreditBuy" style="display: none;">в кредит за 5 000 руб./мес.</a>
					</div>
					<!-- / buy btns end -->

					<table class="del-tbl">
						<tr>
							<td class="td1"><a class="del-instock city-pop" href="#"><span class="city_name_here"><?= $arResult['GEO']['NAME'] ?></span></a></td>
							<td class="td2">
								<? if(!empty($arResult['DISPLAY_PROPERTIES'][$arParams['QUANITY_PROP']]['VALUE'])): ?>
									в наличии
								<? elseif(!empty($arResult['DISPLAY_PROPERTIES'][$arParams['QUANITY_PROP2']]['VALUE'])): ?>
									под заказ
								<? else: ?>
									нет в наличии
								<? endif; ?>
							</td>
						</tr>
						<? if($DELIVERY_PRICE !== false && !empty($DELIVERY_TIME_VAL)): ?>
							<tr>
								<td class="td1"><span>Доставка</span><br/><?= $DELIVERY_TIME_VAL ?> и позже</td>
								<td class="td2"><?= ($DELIVERY_PRICE == 0 ? 'бесплатно' : number_format($DELIVERY_PRICE, 0, '.', ' ').' руб.') ?></td>
							</tr>
						<? endif; ?>
						<? if(!empty($DELIVERY2_TIME_VAL)): ?>
							<tr>
								<td class="td1">
								<a class="del-self1 self-pop1" href="javascript: void(0)" onClick="$('.li-tab-avai a').click();$('html, body').animate({scrollTop: $('#cardTabs').offset().top}, 2000);"><span>Самовывоз</span></a><br/><?= $DELIVERY2_TIME_VAL ?></td>
								<td class="td2">бесплатно</td>
							</tr>
						<? endif; ?>
						<?
						/*
						<tr>
							<td class="td1"><span>Срочная доставка</span><br/>Сегодня, 19 ноября</td>
							<td class="td2">1220 руб.</td>
						</tr>
						<tr>
							<td class="td1">5% скидка при оплате online</td>
							<td class="td2">
								<div class="visa-master"></div>
							</td>
						</tr>
						*/
						?>
					</table>
					<script>
						$(function() {
							$('#move_to_delivery_tab').click(function(e) {
								$('.li-tab-avai > a').trigger('click');
								$('html, body').animate({ scrollTop: $('#cardTabs').offset().top - 60 }, 500);
							});
						});
					</script>

				<? endif; ?>

				<div class="extras" style="display: none;">

					<a href="#" class="pic item" style="background-image:url('<?= SITE_TEMPLATE_PATH ?>/i/blocks/card-offer.png');"></a>

					<div class="warning item">
						<p class="ttl">Описание состояния</p>
						<p class="dsc">потертости и мелкие царапины, упаковка в нормальном состоянии, комплект полный</p>
					</div>

				</div>


			</div>
			<!-- / control w end -->
		</div>
		<!-- / control end -->


		<div class="items hlp">
			<a class="item b-hlp kp-pop">
				<div class="w">
					<div class="ttl">Закажите<br>коммерческое<br>предложение</div>
					<p class="dsc"></p>
					<div class="ico ico-4">&nbsp;</div>
				</div>
			</a>
			<a class="item b-hlp callback-pop" href="#">
				<div class="w">
					<div class="ttl">Закажите звонок!</div>
					<p class="dsc">Удобная функция заказа<br/>обратного звонка.</p>
					<div class="ico ico-1">&nbsp;</div>
				</div>
			</a>
			<a class="item b-hlp" href="tel:<?= $GLOBALS['arGeoData']['CUR_CITY']['PHONE_NUM'] ?>">
				<div class="w">
					<div class="ttl"><?= $GLOBALS['arGeoData']['CUR_CITY']['PHONE'] ?></div>
					<p class="dsc">Помогаем по любым<br/>вопросам продажи и сервиса.</p>
					<div class="ico ico-2">&nbsp;</div>
				</div>
			</a>
			<a class="item b-hlp" href="mailto:online@rusklimat.ru">
				<div class="w">
					<div class="ttl">online@rusklimat.ru</div>
					<p class="dsc">Не стесняйтесь<br/>написать нам письмо.</p>
					<div class="ico ico-3">&nbsp;</div>
				</div>
			</a>
		</div>


	</div>
	<!-- / right column holder end -->

</div>
<!-- / card end -->

<?
$cats = [];
foreach($arResult['SECTION']['PATH'] as $arSection) {
	$cats[] = $arSection['NAME'];
}
?>
<script>
	window.dataLayer = window.dataLayer || [];
	window.dataLayer.push({
		"ecommerce": {
			"currencyCode": "RUB",
			"detail": {
				"products": [
					{
						"id": "<?= $arResult['DISPLAY_PROPERTIES']['CODE']['DISPLAY_VALUE'] ?>",
						"name" : "<?= !empty($arResult['DISPLAY_PROPERTIES']['item_name']['DISPLAY_VALUE']) ? $arResult['DISPLAY_PROPERTIES']['item_name']['DISPLAY_VALUE'] : $arResult['NAME'] ?>",
						"price": <?= number_format($arResult['PRICES'][$PRICE_CODE]['VALUE'], 2, '.', '') ?>,
						"brand": "<?= $arResult['DISPLAY_PROPERTIES']['EL_BRAND']['LINK_ELEMENT_VALUE'][$arResult['DISPLAY_PROPERTIES']['EL_BRAND']['VALUE']]['NAME'] ?>",
						"category": "<?= implode('/', $cats) ?>"
					}
				]
			}
		}
	});
</script>
