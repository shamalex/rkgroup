<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>

<? if(!empty($arResult['SECTION']) && !empty($arResult['SECTION']['~UF_CAT_SEO_DESC'])): ?>
	<!-- about us -->
	<div class="txt-block roll">
		<? if(empty($_REQUEST['PAGEN_1'])): ?>
			<h2 class="ttl-r">ДОПОЛНИТЕЛЬНО О <?= $arResult['SECTION']['NAME'] ?></h2>
			<?= $arResult['SECTION']['~UF_CAT_SEO_DESC'] ?>
		<? endif; ?>
	</div>
	<!-- about us end -->
<? endif; ?>
