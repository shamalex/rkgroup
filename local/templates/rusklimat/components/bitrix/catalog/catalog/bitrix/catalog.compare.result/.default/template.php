<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */

$isAjax = ($_SERVER['REQUEST_METHOD'] == 'POST' && isset($_POST["ajax_action"]) && $_POST["ajax_action"] == "Y");
$arCompareElements=$_SESSION[$arParams["NAME"]][$arParams["IBLOCK_ID"]]["ITEMS"];
?>
<div class="bx_compare" id="bx_catalog_compare_block">
<?
if ($isAjax)
{
	$APPLICATION->RestartBuffer();
}
?>

<div class="comp-holder c">

	<table class="comp-tbl">
	<tr class="">
		<th class="td1">

		<!-- comp switcher -->
		<div class="comp-switcher" id="filterSwitcher1">
			<ul class="c">
				<li<?= !$arResult["DIFFERENT"] ? ' class="active"' : '' ?>><a href="<? echo $arResult['COMPARE_URL_TEMPLATE'].'DIFFERENT=N'; ?>" class="switch">Все параметры</a><div class="arr"></div></li>
				<li>/</li>
				<li<?= $arResult["DIFFERENT"] ? ' class="active"' : '' ?>><a href="<? echo $arResult['COMPARE_URL_TEMPLATE'].'DIFFERENT=Y'; ?>" class="switch">Различающиеся</a><div class="arr"></div></li>
			</ul>
		</div>
		<!-- / comp switcher end -->

		</th>
		<? foreach($arCompareElements as $key=>$val): 
			$arElement=$arResult["ITEMS"][$key];?>
			<th class="it">
				<!-- item w/stars -->
				<div class="item b-comp">
					<a onclick="CatalogCompareObj.MakeAjaxAction('<?=CUtil::JSEscape($arElement['~DELETE_URL'])?>');" href="#" title="<?=GetMessage("CATALOG_REMOVE_PRODUCT")?>" class="b-x"></a>
					<div class="w c">
						<a href="<?= $arElement["DETAIL_PAGE_URL"] ?>" class="lnk">
							<div class="pic"<? if(!empty($arElement["DISPLAY_PROPERTIES"]["picture1"]['FILE_VALUE']['SRC'])): ?> style="background-image:url('<?=$arElement["DISPLAY_PROPERTIES"]["picture1"]['FILE_VALUE']['SRC']?>');"<? endif; ?>></div>
							<div class="ttl"><?= $arElement['NAME'] ?></div>
							<?/*<div class="dsc"></div>*/?>
						</a>
						<div class="rating c">
							<div class="stars" -data-score="<?= intval($arElement['TOTAL_RATE']) ?>"></div>
							<a href="<?= $arElement['DETAIL_PAGE_URL'] ?>#tabRevw" class="lnk">Отзывов: <?= intval($arElement['REVIEWS']) ?></a>
						</div>
						<? if(!empty($arElement["PRICES"][$arParams['PRICE_CODE'][0]]['PRINT_VALUE'])){ ?>
							<div class="prc">
								<? if(!empty($arElement["PRICES"][$arParams['PRICE_CODE'][0]]['PRINT_VALUE'])): ?>
									<div class="curr"><?= $arElement["PRICES"][$arParams['PRICE_CODE'][0]]['PRINT_VALUE'] ?></div>
								<? endif; ?>
								<? if(!empty($arElement["PRICES"][$arParams['PRICE_CODE'][1]]['PRINT_VALUE'])): ?>
									<div class="old"><?= preg_replace('/[^\d\s]+/ui', '', $arElement["PRICES"][$arParams['PRICE_CODE'][1]]['PRINT_VALUE']) ?></div>
								<? endif; ?>
							</div>
						
							<a href="<?= $arElement["ADD_URL"] ?>" class="btn btn-to-bsk">Купить</a>
						<? }else{?>
							<div class="away">Извините, товар отсутствует в продаже</div>							
						<?} ?>
						<div class="fav">
							<fieldset class="checkset">
								<div class="checkline">
									<input type="checkbox" class="addFav" id="addFav_<?= $arElement['ID'] ?>" -data-id="<?= $arElement['ID'] ?>"><label for="addFav_<?= $arElement['ID'] ?>">В избранное</label>
								</div>
							</fieldset>
						</div>
						<div class="article">Код товара: <?=$arElement["DISPLAY_PROPERTIES"]["CODE"]["DISPLAY_VALUE"]?></div>
					</div>
				</div>
				<!-- / item w/stars -->
			</th>
		<? endforeach; ?>

	</tr>

	<!-- tbl body -->
	<tbody>

		<?
		if (!empty($arResult["SHOW_PROPERTIES"]))
		{
			foreach ($arResult["SHOW_PROPERTIES"] as $code => $arProperty)
			{
				$showRow = true;
				if ($arResult['DIFFERENT'])
				{
					$arCompare = array();
					foreach($arCompareElements as $key=>$val){
						$arElement=$arResult["ITEMS"][$key];
						$arPropertyValue = $arElement["DISPLAY_PROPERTIES"][$code]["VALUE"];
						if (is_array($arPropertyValue))
						{
							sort($arPropertyValue);
							$arPropertyValue = implode(" / ", $arPropertyValue);
						}
						$arCompare[] = $arPropertyValue;
					}
					unset($arElement);
					$showRow = (count(array_unique($arCompare)) > 1);
				}

				if ($showRow)
				{
					if ($code == "picture1" || $code == "item_name" || strpos($code, 'COUNT_') === 0 || $code == "NS_CODE") {
						continue;
					}
					?>
					<tr class="sp">
						<td colspan="4">&nbsp;</td>
					</tr>
					<tr>
						<td class="td1"><?=$arProperty["NAME"]?></td>
						<?foreach($arCompareElements as $key=>$val){
							$arElement=$arResult["ITEMS"][$key];
							?>
							<td>
								<?=(is_array($arElement["DISPLAY_PROPERTIES"][$code]["DISPLAY_VALUE"])? implode("/ ", $arElement["DISPLAY_PROPERTIES"][$code]["DISPLAY_VALUE"]): $arElement["DISPLAY_PROPERTIES"][$code]["DISPLAY_VALUE"])?>
							</td>
							<?
						}
						unset($arElement);
						?>
					</tr>
				<?
				}
			}
		}
		?>
		<tr class="sp">
			<td colspan="4">&nbsp;</td>
		</tr>
		<?if (!$arResult['DIFFERENT']){?>
		<tr class="">
			<th class="td1">

			</th>
			<? foreach($arCompareElements as $key=>$val): 
				$arElement=$arResult["ITEMS"][$key]; ?>
				<th class="it">
					<!-- item w/stars -->
					<div class="item b-comp">
						<a onclick="CatalogCompareObj.MakeAjaxAction('<?=CUtil::JSEscape($arElement['~DELETE_URL'])?>');" href="#" title="<?=GetMessage("CATALOG_REMOVE_PRODUCT")?>" class="b-x"></a>
						<div class="w c">
							<a href="<?= $arElement["DETAIL_PAGE_URL"] ?>" class="lnk">
								<div class="pic"<? if(!empty($arElement["DISPLAY_PROPERTIES"]["picture1"]['FILE_VALUE']['SRC'])): ?> style="background-image:url('<?=$arElement["DISPLAY_PROPERTIES"]["picture1"]['FILE_VALUE']['SRC']?>');"<? endif; ?>></div>
								<div class="ttl"><?= $arElement['NAME'] ?></div>

							</a>
							<div class="rating c">
								<div class="stars" -data-score="<?= intval($arElement['TOTAL_RATE']) ?>"></div>
								<a href="<?= $arElement['DETAIL_PAGE_URL'] ?>#tabRevw" class="lnk">Отзывов: <?= intval($arElement['REVIEWS']) ?></a>
							</div>
							<? if(!empty($arElement["PRICES"][$arParams['PRICE_CODE'][0]]['PRINT_VALUE'])){ ?>
								<div class="prc">
									<? if(!empty($arElement["PRICES"][$arParams['PRICE_CODE'][0]]['PRINT_VALUE'])): ?>
										<div class="curr"><?= $arElement["PRICES"][$arParams['PRICE_CODE'][0]]['PRINT_VALUE'] ?></div>
									<? endif; ?>
									<? if(!empty($arElement["PRICES"][$arParams['PRICE_CODE'][1]]['PRINT_VALUE'])): ?>
										<div class="old"><?= preg_replace('/[^\d\s]+/ui', '', $arElement["PRICES"][$arParams['PRICE_CODE'][1]]['PRINT_VALUE']) ?></div>
									<? endif; ?>
								</div>
							
								<a href="<?= $arElement["ADD_URL"] ?>" class="btn btn-to-bsk">Купить</a>
							<? }else{?>
								<div class="away">Извините, товар отсутствует в продаже</div>							
							<?} ?>
							<div class="fav">
								<fieldset class="checkset">
									<div class="checkline">
										<input type="checkbox" class="addFav" id="addFav2_<?= $arElement['ID'] ?>" -data-id="<?= $arElement['ID'] ?>"><label for="addFav2_<?= $arElement['ID'] ?>">В избранное</label>
									</div>
								</fieldset>
							</div>
							<div class="article">Код товара: <?=$arElement["DISPLAY_PROPERTIES"]["NS_CODE"]["DISPLAY_VALUE"]?></div>
						</div>
					</div>
					<!-- / item w/stars -->
				</th>
			<? endforeach; ?>

		</tr>
		<?}?>
	</tbody>
	<!-- / tbl body end -->

	</table>
	<!-- / comp-tbl end -->
</div>
<!-- / comp holder end -->


<?
if ($isAjax)
{
	die();
}
?>
</div>
<script type="text/javascript">
	var CatalogCompareObj = new BX.Iblock.Catalog.CompareClass("bx_catalog_compare_block");
</script>
