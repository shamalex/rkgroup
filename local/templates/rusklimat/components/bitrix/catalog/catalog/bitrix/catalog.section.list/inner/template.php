<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);

$arRootSections = [];
foreach($arResult['SECTIONS'] as $arSection) {
	if ($arSection['IBLOCK_SECTION_ID'] == $arParams['SECTION_ID'] && $arSection['ELEMENT_CNT'] > 0) {
		$arRootSections[$arSection['ID']] = $arSection;
	}
}

?>

<div class="cat roll">
<? if(!empty($arResult['SECTION'])): ?>
	<h1 class="ttl1"><?= $arResult['SECTION']['NAME'] ?></h1>
	<? if(!empty($arResult['SECTION']['DESCRIPTION'])): ?>
		<div class="txt-block qq">
			<p><? if(empty($_REQUEST['PAGEN_1'])): ?> <?= $arResult['SECTION']["DESCRIPTION"]; ?><? endif; ?></p>
		</div>
		<? endif; ?>
<? endif; ?>
</div>
<div class="roll c">
<? if(!empty($arRootSections)&&$arResult['SECTION']['DEPTH_LEVEL']==1): ?>

	<h2 class="ttl2">Выберите <?= $arResult['SECTION']['NAME'] ?> по типу</h2>
	<!-- brands slider -->
<div class="cat-types without">
		<!-- news slides -->
		<div class="items c">

			<? foreach($arRootSections as $arSection): ?>
				<?
				$pic = false;
				if (!empty($arSection['PICTURE']['SRC'])) {
					$pic = $arSection['PICTURE']['SRC'];
				}
				?>
				<!-- item -->
				<a class="item" href="<?= $arSection['SECTION_PAGE_URL'] ?>">
					<div class="pic" style="text-align:center;"<? /*if($pic): ?> style="background-image:url('<?= $pic ?>');"<? endif;*/ ?>>
						<? if($pic): ?><img style="height:100%; width:auto;" data-lazy="<?= $pic ?>"/><? endif; ?>
					</div>
					<h2 class="ttl"><?= $arSection['NAME'] ?></h2>
				</a>
				<!-- / item -->
			<? endforeach; ?>

		</div>
		<!-- / slides end -->
</div>
	<!-- / brands slider end -->
</div>
<? endif; ?>
