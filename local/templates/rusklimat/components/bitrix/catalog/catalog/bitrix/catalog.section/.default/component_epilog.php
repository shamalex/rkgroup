<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $templateData */
/** @var @global CMain $APPLICATION */
use Bitrix\Main\Loader;
global $APPLICATION;
/* if (isset($templateData['TEMPLATE_THEME']))
{
	$APPLICATION->SetAdditionalCSS($templateData['TEMPLATE_THEME']);
}
if (isset($templateData['TEMPLATE_LIBRARY']) && !empty($templateData['TEMPLATE_LIBRARY']))
{
	$loadCurrency = false;
	if (!empty($templateData['CURRENCIES']))
		$loadCurrency = Loader::includeModule('currency');
	CJSCore::Init($templateData['TEMPLATE_LIBRARY']);
	if ($loadCurrency)
	{
	?>
	<script type="text/javascript">
		BX.Currency.setCurrencies(<? echo $templateData['CURRENCIES']; ?>);
	</script>
<?
	}
} */


// views

CModule::IncludeModule('iblock');

$VIEWS_IBLOCK_ID = 25;

$arFilter = array('IBLOCK_ID' => $arParams['IBLOCK_ID'], 'ID' => $arResult['ID']);
$rsSections = CIBlockSection::GetList([], $arFilter, false, ['ID', 'UF_SECTION_VIEWS'], ['nTopCount'=>1]);
if ($arSection = $rsSections->Fetch()) {
	if (empty($arSection['UF_SECTION_VIEWS'])) {

		$el = new CIBlockElement;

		$arLoadProductArray = Array(
			"IBLOCK_SECTION_ID" => false,
			"IBLOCK_ID"      => $VIEWS_IBLOCK_ID,
			"NAME"           => $arResult['ID'],
			"CODE"           => '',
			"XML_ID"         => $arResult['ID'],
			"ACTIVE"         => 'Y',
			"SORT"    => '2',
			"PROPERTY_VALUES"    => [],
		);

		if($PRODUCT_ID = $el->Add($arLoadProductArray, false, false, false)) {
			// p("New ID: ".$PRODUCT_ID);
			// CIBlockElement::SetPropertyValuesEx($arResult['ID'], $arParams['IBLOCK_ID'], array('VIEWS' => $PRODUCT_ID));

			global $USER_FIELD_MANAGER;
			$USER_FIELD_MANAGER->Update( 'IBLOCK_'.$arParams['IBLOCK_ID'].'_SECTION', $arResult['ID'], array('UF_SECTION_VIEWS'  => $PRODUCT_ID) );

			$arSection['UF_SECTION_VIEWS'] = $PRODUCT_ID;
		} else {
			// p("Error add: ".$el->LAST_ERROR);
		}
	}

	global $arGeoData;

	$SEE_PROP = preg_replace('/\-+/ui', '_', $arGeoData['CUR_CITY']['CITY_CODE']);
	$SEE_PROP = strtoupper('SEE_'.$SEE_PROP);

	$arSort = array();
	$arFilter = Array("IBLOCK_ID"=>$VIEWS_IBLOCK_ID, "ID"=>$arSection['UF_SECTION_VIEWS']);
	$arLimit = ['nTopCount'=>1];
	$arSelect = ['ID', 'PROPERTY_'.$SEE_PROP];
	$res = CIBlockElement::GetList($arSort, $arFilter, false, $arLimit, $arSelect);
	if ($arElement = $res->Fetch()) {
		$views = intval($arElement['PROPERTY_'.$SEE_PROP.'_VALUE']);
		$views++;
		CIBlockElement::SetPropertyValuesEx($arElement['ID'], $VIEWS_IBLOCK_ID, array($SEE_PROP => $views));
	} else {
		global $USER_FIELD_MANAGER;
		$USER_FIELD_MANAGER->Update( 'IBLOCK_'.$arParams['IBLOCK_ID'].'_SECTION', $arResult['ID'], array('UF_SECTION_VIEWS'  => '') );
	}
}


CModule::IncludeModule('sale');
$arProductsInBasket = array();
$resBasket = CSaleBasket::GetList(
	array(),
	array(
	"FUSER_ID" => CSaleBasket::GetBasketUserID(),
	"LID" => SITE_ID,
	"ORDER_ID" => "NULL"
	)
);
while ($arBasketProd = $resBasket->GetNext()) {
	$arProductsInBasket[] = '#add_to_basket_'.$arBasketProd['PRODUCT_ID'];
	$arProductsInBasket2[] = '#add_to_basket2_'.$arBasketProd['PRODUCT_ID'];
}

echo '<script>jQuery(document).ready(function(){jQuery("'.implode(', ', $arProductsInBasket).'").attr("href", "#").addClass("in_basket").text("Уже в корзине");});</script>';
echo '<script>jQuery(document).ready(function(){jQuery("'.implode(', ', $arProductsInBasket2).'").attr("href", "#").addClass("in_basket").text("Уже в корзине");});</script>';
