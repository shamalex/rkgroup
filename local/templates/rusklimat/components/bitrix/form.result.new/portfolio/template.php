<?
if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
?>
<?//p($arResult);?>
<!-- budget roll -->
<div class="roll" id="formActionLink">
	<h1 class="ttl2">Оставить заявку</h1>
	
	<?/*if ($arResult["isFormErrors"] == "Y"):?><div class="form_errors"><?=$arResult["FORM_ERRORS_TEXT"];?></div><?endif;*/?>

	<? if(!empty($arResult["FORM_NOTE"])): ?>
		<div class="form_note"><?=$arResult["FORM_NOTE"]?></div>
	<? endif; ?>
	
	<?if ($arResult["isFormNote"] != "Y"): ?>
		<!-- budget -->
		<div class="budget c">
			<?=$arResult["FORM_HEADER"]?>
			<?
			foreach ($arResult["QUESTIONS"] as $FIELD_SID => $arQuestion)
			{
				if ($arQuestion['STRUCTURE'][0]['FIELD_TYPE'] == 'hidden')
				{
					echo $arQuestion["HTML_CODE"];
				}
				else
				{
					/* 
					?>
						<tr>
							<td>
								<?=$arQuestion["CAPTION"]?><?if ($arQuestion["REQUIRED"] == "Y"):?><?=$arResult["REQUIRED_SIGN"];?><?endif;?>
								<?=$arQuestion["IS_INPUT_CAPTION_IMAGE"] == "Y" ? "<br />".$arQuestion["IMAGE"]["HTML_CODE"] : ""?>
							</td>
							<td><?=$arQuestion["HTML_CODE"]?></td>
						</tr>
					<?
					 */
				}
			} //endwhile
			?>
			<?= $arResult["QUESTIONS"]["SIMPLE_QUESTION_140"]["HTML_CODE"] ?>
			<?= $arResult["QUESTIONS"]["SIMPLE_QUESTION_306"]["HTML_CODE"] ?>
			<?= $arResult["QUESTIONS"]["SIMPLE_QUESTION_629"]["HTML_CODE"] ?>
			<div class="cols c">
				<!-- col-l -->
				<div class="col-l">
					<div class="w">
						<div class="b-brd">
							<div class="ttl"><?= $arResult["QUESTIONS"]["SIMPLE_QUESTION_625"]["CAPTION"] ?></div>
							<?= $arResult["QUESTIONS"]["SIMPLE_QUESTION_625"]["HTML_CODE"] ?><div class="vfield"><?=$arResult["FORM_ERRORS"]["SIMPLE_QUESTION_625"]?></div>
							<div class="ttl"><?= $arResult["QUESTIONS"]["SIMPLE_QUESTION_303"]["CAPTION"] ?></div>
							<?= $arResult["QUESTIONS"]["SIMPLE_QUESTION_303"]["HTML_CODE"] ?><div class="vfield"><?=$arResult["FORM_ERRORS"]["SIMPLE_QUESTION_303"]?></div>
							<div class="ttl"><?= $arResult["QUESTIONS"]["SIMPLE_QUESTION_400"]["CAPTION"] ?></div>
							<?= $arResult["QUESTIONS"]["SIMPLE_QUESTION_400"]["HTML_CODE"] ?><div class="vfield"><?=$arResult["FORM_ERRORS"]["SIMPLE_QUESTION_400"]?></div>
							<div class="ttl"><?= $arResult["QUESTIONS"]["SIMPLE_QUESTION_542"]["CAPTION"] ?></div>
							<?= $arResult["QUESTIONS"]["SIMPLE_QUESTION_542"]["HTML_CODE"] ?><div class="vfield"><?=$arResult["FORM_ERRORS"]["SIMPLE_QUESTION_542"]?></div>
						</div>
						<!-- / b-brd end -->
					</div>
					<!-- / col-l .w end -->
				</div>
				<!-- / col-l end -->
				
				<!-- col-r -->
				<div class="col-r">
					<div class="w">
						<div class="ttl"><?= $arResult["QUESTIONS"]["SIMPLE_QUESTION_361"]["CAPTION"] ?></div>
						<fieldset class="checkset inline<?=isset($arResult["FORM_ERRORS"]["SIMPLE_QUESTION_361"])?" inp-inv":""?>">
							<?= $arResult["QUESTIONS"]["SIMPLE_QUESTION_361"]["HTML_CODE"] ?>
						</fieldset>
						<div class="vfield"><?=$arResult["FORM_ERRORS"]["SIMPLE_QUESTION_361"]?></div>
						<div class="ttl"><?= $arResult["QUESTIONS"]["SIMPLE_QUESTION_997"]["CAPTION"] ?></div>
						<fieldset class="checkset inline<?=isset($arResult["FORM_ERRORS"]["SIMPLE_QUESTION_997"])?" inp-inv":""?>">
							<?= $arResult["QUESTIONS"]["SIMPLE_QUESTION_997"]["HTML_CODE"] ?>
						</fieldset>
						<div class="vfield"><?=$arResult["FORM_ERRORS"]["SIMPLE_QUESTION_997"]?></div>
						<div class="ttl"><?= $arResult["QUESTIONS"]["SIMPLE_QUESTION_644"]["CAPTION"] ?></div>
						<?= $arResult["QUESTIONS"]["SIMPLE_QUESTION_644"]["HTML_CODE"] ?><div class="vfield"><?=$arResult["FORM_ERRORS"]["SIMPLE_QUESTION_644"]?></div>
				
					</div>
					<!-- / col-r .w end -->
				</div>
				<!-- / col-r end -->
			</div>
			<!-- / cols end -->
			<input name="subj" type="text" value="" />
			<button <?=(intval($arResult["F_RIGHT"]) < 10 ? "disabled=\"disabled\"" : "");?> type="submit" class="btn btn-30 btn-b budget-sub-btn" id="budgetSubBtn" name="web_form_submit" value="<?=htmlspecialcharsbx(strlen(trim($arResult["arForm"]["BUTTON"])) <= 0 ? GetMessage("FORM_ADD") : $arResult["arForm"]["BUTTON"]);?>"><?=htmlspecialcharsbx(strlen(trim($arResult["arForm"]["BUTTON"])) <= 0 ? GetMessage("FORM_ADD") : $arResult["arForm"]["BUTTON"]);?></button>
			<?=$arResult["FORM_FOOTER"]?>
		</div>
		<!-- / budget end -->
	<? endif; ?>
</div>
<!-- / budget roll end -->
