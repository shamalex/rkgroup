<?
if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

global $arGeoData;

$arResult["FORM_HEADER"] = preg_replace('/action="([^"]+)"/ui', 'action="\1#formActionLink"', $arResult["FORM_HEADER"]);

$arReplaces = [];
$arReplaces['SIMPLE_QUESTION_140'] = [
	['/type="text"/ui'],
	['type="hidden"'],
];
$arReplaces['SIMPLE_QUESTION_306'] = [
	['/type="text"/ui'],
	['type="hidden"'],
];
$arReplaces['SIMPLE_QUESTION_629'] = [
	['/type="text"/ui'],
	['type="hidden"'],
];
if(!isset($arResult["FORM_ERRORS"]["SIMPLE_QUESTION_625"])){
	$arReplaces['SIMPLE_QUESTION_625'] = [
		['/class="inputselect"/ui', '/select selected/ui'],
		['class="inputselect budget-select ik-30-lt" id="budgetSelect"', 'select class="inputselect budget-select ik-30-lt" id="budgetSelect"'],
	];
}else{
	$arReplaces['SIMPLE_QUESTION_625'] = [
		['/class="inputselect"/ui', '/select selected/ui'],
		['class="inputselect budget-select ik-30-lt inp-inv" id="budgetSelect"', 'select class="inputselect budget-select ik-30-lt inp-inv" id="budgetSelect"'],
	];
}
if(!isset($arResult["FORM_ERRORS"]["SIMPLE_QUESTION_303"])){
	$arReplaces['SIMPLE_QUESTION_303'] = [
		['/class="inputselect"/ui', '/select selected/ui'],
		['class="inputselect budget-select ik-30-lt" id="budgetSelect2"', 'select class="inputselect budget-select ik-30-lt" id="budgetSelect2"'],
	];
}else{
	$arReplaces['SIMPLE_QUESTION_303'] = [
		['/class="inputselect"/ui', '/select selected/ui'],
		['class="inputselect budget-select ik-30-lt inp-inv" id="budgetSelect2"', 'select class="inputselect budget-select ik-30-lt inp-inv" id="budgetSelect2"'],
	];
}
if(!isset($arResult["FORM_ERRORS"]["SIMPLE_QUESTION_400"])){
	$arReplaces['SIMPLE_QUESTION_400'] = [
		['/class="inputtext"/ui'],
		['class="inputtext inp inp-lt"'],
	];
}else{
	$arReplaces['SIMPLE_QUESTION_400'] = [
		['/class="inputtext"/ui'],
		['class="inputtext inp inp-lt inp-inv"'],
	];
}
if(!isset($arResult["FORM_ERRORS"]["SIMPLE_QUESTION_542"])){
	$arReplaces['SIMPLE_QUESTION_542'] = [
		['/class="inputtext"/ui'],
		['class="inputtext inp inp-lt"'],
	];
}else{
	$arReplaces['SIMPLE_QUESTION_542'] = [
		['/class="inputtext"/ui'],
		['class="inputtext inp inp-lt inp-inv"'],
	];
}
$arReplaces['SIMPLE_QUESTION_361'] = [
	['/<br[^>]*>/ui', '/<input/ui', '/<\/label>/ui'],
	[' ', '<div class="checkline"><input', '</label></div>'],
];
$arReplaces['SIMPLE_QUESTION_997'] = [
	['/<br[^>]*>/ui', '/<input/ui', '/<\/label>/ui'],
	[' ', '<div class="checkline"><input', '</label></div>'],
];
if(!isset($arResult["FORM_ERRORS"]["SIMPLE_QUESTION_644"])){
	$arReplaces['SIMPLE_QUESTION_644'] = [
		['/class="inputtextarea"/ui', '/cols="[\d]+"/ui', '/rows="[\d]+"/ui'],
		['class="inputtextarea txtarea txtarea-lt budget-txt"', '', ''],
	];
}else{
	$arReplaces['SIMPLE_QUESTION_644'] = [
		['/class="inputtextarea"/ui', '/cols="[\d]+"/ui', '/rows="[\d]+"/ui'],
		['class="inputtextarea txtarea txtarea-lt budget-txt inp-inv"', '', ''],
	];
}

foreach($arReplaces as $code => $repArr) {
	$arResult["QUESTIONS"][$code]["HTML_CODE"] = preg_replace($repArr[0], $repArr[1], $arResult["QUESTIONS"][$code]["HTML_CODE"]);
}