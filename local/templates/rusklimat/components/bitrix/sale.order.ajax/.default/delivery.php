<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
global $arGeoData, $USER;
if($_COOKIE["address-sel"] && isset($arResult["DELIVERY_ADDRESS_LIST"][$_COOKIE["address-sel"]]) && $USER->IsAuthorized()){
	$GLOBALS['FIELDS_CODES']['HIDE_DELIVERY_D_ADDR']['VALUE']=$arResult["DELIVERY_ADDRESS_LIST"][$_COOKIE["address-sel"]]["PROPERTY_STREET_VALUE"];
	$GLOBALS['FIELDS_CODES']['HIDE_DELIVERY_D_HOUSE']['VALUE']=$arResult["DELIVERY_ADDRESS_LIST"][$_COOKIE["address-sel"]]["PROPERTY_HOUSE_VALUE"];
	$GLOBALS['FIELDS_CODES']['HIDE_DELIVERY_D_CORP']['VALUE']=$arResult["DELIVERY_ADDRESS_LIST"][$_COOKIE["address-sel"]]["PROPERTY_HOUSING_VALUE"];
	$GLOBALS['FIELDS_CODES']['HIDE_DELIVERY_D_BUILD']['VALUE']=$arResult["DELIVERY_ADDRESS_LIST"][$_COOKIE["address-sel"]]["PROPERTY_BUILD_VALUE"];
	$GLOBALS['FIELDS_CODES']['HIDE_DELIVERY_D_ROOM']['VALUE']=$arResult["DELIVERY_ADDRESS_LIST"][$_COOKIE["address-sel"]]["PROPERTY_APARTMENT_VALUE"];
	$arResult["SELECTED_ADDRESS"]=$arResult["DELIVERY_ADDRESS_LIST"][$_COOKIE["address-sel"]]["NAME"];

}
?>

<script type="text/javascript">
	function fShowStore(id, showImages, formWidth, siteId)
	{
		var strUrl = '<?=$templateFolder?>' + '/map.php';
		var strUrlPost = 'delivery=' + id + '&showImages=' + showImages + '&siteId=' + siteId;

		var storeForm = new BX.CDialog({
					'title': '<?=GetMessage('SOA_ORDER_GIVE')?>',
					head: '',
					'content_url': strUrl,
					'content_post': strUrlPost,
					'width': formWidth,
					'height':450,
					'resizable':false,
					'draggable':false
				});

		var button = [
				{
					title: '<?=GetMessage('SOA_POPUP_SAVE')?>',
					id: 'crmOk',
					'action': function ()
					{
						GetBuyerStore();
						BX.WindowManager.Get().Close();
					}
				},
				BX.CDialog.btnCancel
			];
		storeForm.ClearButtons();
		storeForm.SetButtons(button);
		storeForm.Show();
	}

	function GetBuyerStore()
	{
		BX('BUYER_STORE').value = BX('POPUP_STORE_ID').value;
		//BX('ORDER_DESCRIPTION').value = '<?=GetMessage("SOA_ORDER_GIVE_TITLE")?>: '+BX('POPUP_STORE_NAME').value;
		BX('store_desc').innerHTML = BX('POPUP_STORE_NAME').value;
		BX.show(BX('select_store'));
	}

	function showExtraParamsDialog(deliveryId)
	{
		var strUrl = '<?=$templateFolder?>' + '/delivery_extra_params.php';
		var formName = 'extra_params_form';
		var strUrlPost = 'deliveryId=' + deliveryId + '&formName=' + formName;

		if(window.BX.SaleDeliveryExtraParams)
		{
			for(var i in window.BX.SaleDeliveryExtraParams)
			{
				strUrlPost += '&'+encodeURI(i)+'='+encodeURI(window.BX.SaleDeliveryExtraParams[i]);
			}
		}

		var paramsDialog = new BX.CDialog({
			'title': '<?=GetMessage('SOA_ORDER_DELIVERY_EXTRA_PARAMS')?>',
			head: '',
			'content_url': strUrl,
			'content_post': strUrlPost,
			'width': 500,
			'height':200,
			'resizable':true,
			'draggable':false
		});

		var button = [
			{
				title: '<?=GetMessage('SOA_POPUP_SAVE')?>',
				id: 'saleDeliveryExtraParamsOk',
				'action': function ()
				{
					insertParamsToForm(deliveryId, formName);
					BX.WindowManager.Get().Close();
				}
			},
			BX.CDialog.btnCancel
		];

		paramsDialog.ClearButtons();
		paramsDialog.SetButtons(button);
		//paramsDialog.adjustSizeEx();
		paramsDialog.Show();
	}

	function insertParamsToForm(deliveryId, paramsFormName)
	{
		var orderForm = BX("ORDER_FORM"),
			paramsForm = BX(paramsFormName);
			wrapDivId = deliveryId + "_extra_params";

		var wrapDiv = BX(wrapDivId);
		window.BX.SaleDeliveryExtraParams = {};

		if(wrapDiv)
			wrapDiv.parentNode.removeChild(wrapDiv);

		wrapDiv = BX.create('div', {props: { id: wrapDivId}});

		for(var i = paramsForm.elements.length-1; i >= 0; i--)
		{
			var input = BX.create('input', {
				props: {
					type: 'hidden',
					name: 'DELIVERY_EXTRA['+deliveryId+']['+paramsForm.elements[i].name+']',
					value: paramsForm.elements[i].value
					}
				}
			);

			window.BX.SaleDeliveryExtraParams[paramsForm.elements[i].name] = paramsForm.elements[i].value;

			wrapDiv.appendChild(input);
		}

		orderForm.appendChild(wrapDiv);

		BX.onCustomEvent('onSaleDeliveryGetExtraParams',[window.BX.SaleDeliveryExtraParams]);
	}

	BX.addCustomEvent('onDeliveryExtraServiceValueChange', function(){ submitForm(); });

</script>

<input type="hidden" name="BUYER_STORE" id="BUYER_STORE" value="<?=$arResult["BUYER_STORE"]?>" />
<input type="hidden" name="" id="CUR_DELIVERY_ID" value="" />

<? if(!empty($arResult["DELIVERY"])): ?>
	<div class="order-item">

		<?
		$curSelected = false;
		foreach ($arResult["DELIVERY"] as $delivery_id => $arDelivery) {
			if ($arDelivery["CHECKED"]=="Y") {
				$curSelected = $arDelivery["DESCRIPTION"];
				break;
			}
		}
		?>

		<!-- order-controls delivery -->
		<div class="order-controls">

			<!-- order-switch -->
			<div class="order-switch" id="switchDost">
				<div class="w c">
					<a href="#"<?= $curSelected == 'samo' ? '  class="active"' : '' ?> name="samo" id="samo"><span>Я заберу самостоятельно</span></a>
					<a href="#"<?= $curSelected == 'dost' ? '  class="active"' : '' ?> name="dost" id="dost"><span>Я закажу доставку</span></a>
				</div>
			</div>
			<!-- / order-switch end -->

			<!-- tooltip -->
			<a href="#" class="que ttSwitch2">?</a>
			<div id="ttSwitch2" style="display:none">
				<div class="tt tt-switch tt-c">
					<?$APPLICATION->IncludeFile("include/ttSwitch2.php", Array(), Array("MODE"=>"html", "SHOW_BORDER"=>false));?>
				</div>
			</div>
			<!-- / tooltip end -->

		</div>
		<!-- / order-controls delivery end -->

		<!-- dostTbl -->
		<div id="dostTbl"<?= $curSelected != 'dost' ? ' style="display:none"' : '' ?>>

			<h2 class="ttl-o">Условия доставки товара</h2>
			<?global $USER;
			if($USER->IsAuthorized()){?>
			<table class="tbl-order tbl-del-city">
				<tr>
					<td class="td1">
						<p class="ttl">Мои адреса</p>
					</td>
					<td class="td2">

						<!-- city-sel-holder -->
						<div class="city-sel-holder">

							<div class="city-sel" id="address-sel"><?=$arResult["SELECTED_ADDRESS"]?></div>
							<div class="hint">Выберите один из адресов,указанных Вами ранее!</div>

							<!-- city-sel-pop -->
							<div class="city-sel-pop" id="address-sel-pop">
								<!-- city-sel-pop .w-->
								<div class="w">

									<!-- #citySel1 -->
									<div id="citySel1">
										<div class="city-sel-list c">

											<!-- column 1 -->
											<ul class="ul-orange">
												<?foreach($arResult["DELIVERY_ADDRESS_LIST"] as $id=>$address){?>
												<li<?if($_COOKIE["address-sel"]==$id){?> class="active"<?}?>><a href="#" -data-id="<?=$id?>" -city-id="<?=$address["PROPERTY_CITY_VALUE"]?>"><?=$address["NAME"]?></a></li>
												<?}?>
											</ul>
											<!--/ column 1 end -->

										</div>
										<!-- / city-sel-list end -->

									</div>
									<!-- / #citySel1 -->

								</div>
								<!-- / city-sel-pop > .w end-->
							</div>
							<!-- / city-sel-pop end -->


						</div>
						<!-- / city-sel-holder end -->

					</td>
				</tr>
			</table>
			<?}?>


			<table class="tbl-order tbl-del-city">

				<tr>
					<td class="td1">
						<p class="ttl">Город</p>
					</td>
					<td class="td2">

						<!-- city-sel-holder -->
						<div class="city-sel-holder">

							<a class="city-sel city-pop city_name_here" href="#select_city"><?= $arGeoData['CUR_CITY']['NAME'] ?></a>
							<div class="hint">Выберите ваш город. Для разных городов условия доставки могут отличаться!</div>
							<?/*<div class="vfield">Выберите ваш город. Для разных городов условия доставки могут отличаться!</div>*/?>

						</div>
						<!-- / city-sel-holder end -->

					</td>
				</tr>
			</table>

			<table class="tbl tbl-del-options">
				<tr>
					<th colspan="2">Вариант получения</th>
					<th>Дата</th>
					<th>Стоимость</th>
				</tr>
				<?
				CModule::IncludeModule('sale');
				CModule::IncludeModule('catalog');
				$was_active = false;
				foreach ($arResult["DELIVERY"] as $delivery_id => $arDelivery)
				{
					?>
					<? if($arDelivery["DESCRIPTION"] == 'dost'): ?>
						<?
						if ($arDelivery["CHECKED"]=="Y") {
							$was_active = true;
						}
						?>
						<tr class="sp">
							<td colspan="3">&nbsp;</td>
						</tr>
						<tr>
							<td class="td1">
								<input type="radio"
									id="ID_DELIVERY_ID_<?= $arDelivery["ID"] ?>"
									name="<?=htmlspecialcharsbx($arDelivery["FIELD_NAME"])?>"
									value="<?= $arDelivery["ID"] ?>"<?if ($arDelivery["CHECKED"]=="Y") echo " checked";?>
									class="check-custom delivery_checks"
									data-delivery="<?=htmlspecialcharsbx($arDelivery["NAME"])?>"
									data-selfdelivery=""
									data-delivery_price="<?= !empty($arDelivery["PRICE_FORMATED"]) ? $arDelivery["PRICE_FORMATED"] : 0 ?>"
									data-delivery_desc="<?= $arDelivery["DESCRIPTION"] ?>"
									/>
							</td>
							<td class="td2">
								<p><label for="ID_DELIVERY_ID_<?=$arDelivery["ID"]?>"><?= htmlspecialcharsbx($arDelivery["NAME"])?></label></p>
								<? if($arDelivery["CHECKED"]=="Y" && !empty($arDelivery["PERIOD_SELECTS"])): ?>
									<div class="del-method-hidden" style="display:block">
										<p class="hint">Выберите день доставки:</p>
										<select class="del-method-select ik-30-lt" name="<?= $GLOBALS['FIELDS_CODES']['HIDE_DELIVERY_TIME']['FIELD_NAME'] ?>">
											<? foreach($arDelivery["PERIOD_SELECTS"] as $period_date): ?>
												<option value="<?= $period_date ?>"<?= $GLOBALS['FIELDS_CODES']['HIDE_DELIVERY_TIME']['VALUE'] == $period_date ? ' selected' : '' ?>><?= $period_date ?></option>
											<? endforeach; ?>
										</select>
									</div>
								<? endif; ?>
							</td>
							<td>
								<? if(!empty($arDelivery["PERIOD_TEXT"])): ?>
									<?= $arDelivery["PERIOD_TEXT"] ?>
								<? else: ?>
									&nbsp;
								<? endif; ?>
							</td>
							<td>
								<? if($arDelivery["PRICE"] == 0): ?>
									бесплатно
								<? else: ?>
									<?=(strlen($arDelivery["PRICE_FORMATED"]) > 0 ? $arDelivery["PRICE_FORMATED"] : '&nbsp;')?>
								<? endif; ?>
							</td>
						</tr>
					<? endif; ?>
					<?
				}
				?>
			</table>

		</div>
		<!-- / dostTbl -->

		<!-- samTbl -->
		<div id="samTbl"<?= $curSelected != 'samo' ? ' style="display:none"' : '' ?>>
			<h2 class="ttl-o">Условия самовывоза товара</h2>

			<table class="tbl-order tbl-del-city">

				<tr>
					<td class="td1">
						<p class="ttl">Город</p>
					</td>
					<td class="td2">

						<!-- city-sel-holder -->
						<div class="city-sel-holder">

							<a class="city-sel city-pop city_name_here" href="#select_city"><?= $arGeoData['CUR_CITY']['NAME'] ?></a>
							<div class="hint">Выберите ваш город. Для разных городов условия доставки могут отличаться!</div>
							<?/*<div class="vfield">Выберите ваш город. Для разных городов условия доставки могут отличаться!</div>*/?>

						</div>
						<!-- / city-sel-holder end -->

					</td>
				</tr>
			</table>

			<table class="tbl tbl-del-options">
				<tr>
					<th colspan="2">Вариант получения</th>
					<th>Дата</th>
					<th>Стоимость</th>
				</tr>
				<?
				CModule::IncludeModule('sale');
				CModule::IncludeModule('catalog');
				$MAIN_CITY = $arResult['GEO']['PROPERTIES']['network_name']['VALUE'];
				$MAIN_CITY = str_replace('Русклимат ', '', $MAIN_CITY);
				foreach ($arResult["DELIVERY"] as $delivery_id => $arDelivery)
				{
					?>
					<? if($arDelivery["DESCRIPTION"] == 'samo'): ?>
						<? foreach($arResult['LOCATIONS'] as $i=>$arLocation): ?>
							<tr class="sp">
								<td colspan="3">&nbsp;</td>
							</tr>
							<tr>
								<td class="td1">
									<?
									$checked = false;
									if (!empty($_REQUEST[$arDelivery["FIELD_NAME"]])) {
										if ($_REQUEST[$arDelivery["FIELD_NAME"]] == $arDelivery["ID"].'_'.$arLocation["ID"]) {
											$checked = true;
										}
									} elseif($i == 0) {
										$checked = true;
									}
									?>
									<input type="radio"
										id="ID_DELIVERY_ID_<?= $arDelivery["ID"] ?>_<?= $arLocation["ID"] ?>"
										name="<?=htmlspecialcharsbx($arDelivery["FIELD_NAME"])?>"
										value="<?= $arDelivery["ID"] ?>_<?= $arLocation["ID"] ?>"<?if ($checked) echo " checked";?>
										class="check-custom delivery_checks"
										data-delivery="<?=htmlspecialcharsbx($arDelivery["NAME"])?>"
										data-selfdelivery="<?= $arLocation['NAME'] ?> (<?= $MAIN_CITY ?>, <?= $arLocation['PROPERTIES']['address']['VALUE'] ?>)"
										data-delivery_price="<?= !empty($arDelivery["PRICE_FORMATED"]) ? $arDelivery["PRICE_FORMATED"] : 0 ?>"
										data-delivery_desc="<?= $arDelivery["DESCRIPTION"] ?>"
										/>
								</td>
								<td class="td2">
									<p><label for="ID_DELIVERY_ID_<?=$arDelivery["ID"]?>_<?= $arLocation["ID"] ?>"><b><?= $arLocation['NAME'] ?> </b></label><br><span class="sm-map-pop"><?= $MAIN_CITY ?>, <?= $arLocation['PROPERTIES']['address']['VALUE'] ?></span></p>

									<div class="relat hide_it">
										<div class="sm-map">
											<!-- gmap -->
											<div class="map">
												<div class="map-x">
												</div>
												<!-- gmap holder (gmap.js) -->
												<div id="map_<?=$arDelivery["ID"]?>_<?= $arLocation["ID"] ?>" class="map-container map-inner">
													<?
													$arPlacemarks = [];
													$gps = explode(',', $arLocation['PROPERTIES']['place']['VALUE']);
													if (!empty($gps)) {
														$gpsN=substr(doubleval($gps[0]),0,15);
														$gpsS=substr(doubleval($gps[1]),0,15);
														$arPlacemarks[]=array("LON"=>$gpsS,"LAT"=>$gpsN,"TEXT"=>$arLocation["PROPERTIES"]['address']['VALUE']);
													}
													if (!empty($arPlacemarks)) {
														?>
														<script>
															if (window.top.$) {
																window.google = window.top.google;
																window.SITE_TEMPLATE_PATH = window.top.SITE_TEMPLATE_PATH;
															}
														</script>
														<?
														$APPLICATION->IncludeComponent(
															"bitrix:map.google.view",
															".default",
															array(
																"INIT_MAP_TYPE" => "ROADMAP",
																"MAP_DATA" => serialize(array("google_lat"=>$gpsN,"google_lon"=>$gpsS,"google_scale"=>11,"PLACEMARKS"=>$arPlacemarks)),
																"MAP_WIDTH" => "260",
																"MAP_HEIGHT" => "330",
																"CONTROLS" => array(
																	// 0 => "SMALL_ZOOM_CONTROL",
																	// 1 => "SCALELINE",
																),
																"OPTIONS" => array(
																	0 => "ENABLE_DBLCLICK_ZOOM",
																	1 => "ENABLE_DRAGGING",
																	2 => "ENABLE_KEYBOARD",
																	3 => "ENABLE_SCROLL_ZOOM",
																),
																"MAP_ID" => "",
																"COMPONENT_TEMPLATE" => ".default"
															),
															false
														);
													}
													?>
												</div>
												<!-- / gmap holder end -->
											</div>
											<!-- / gmap -->
										</div>
									</div>
									<? if($checked && !empty($arDelivery["PERIOD_SELECTS"])): ?>
										<div class="del-method-hidden" style="display:block">
											<p class="hint">Выберите день доставки:</p>
											<select class="del-method-select ik-30-lt" name="<?= $GLOBALS['FIELDS_CODES']['HIDE_DELIVERY_TIME']['FIELD_NAME'] ?>">
												<? foreach($arDelivery["PERIOD_SELECTS"] as $period_date): ?>
													<option value="<?= $period_date ?>"<?= $GLOBALS['FIELDS_CODES']['HIDE_DELIVERY_TIME']['VALUE'] == $period_date ? ' selected' : '' ?>><?= $period_date ?></option>
												<? endforeach; ?>
											</select>
										</div>
									<? endif; ?>
								</td>
								<td>
									<? if(!empty($arDelivery["PERIOD_TEXT"])): ?>
										<?= $arDelivery["PERIOD_TEXT"] ?>
									<? else: ?>
										&nbsp;
									<? endif; ?>
								</td>
								<td>
									<? if($arDelivery["PRICE"] == 0): ?>
										бесплатно
									<? else: ?>
										<?=(strlen($arDelivery["PRICE_FORMATED"]) > 0 ? $arDelivery["PRICE_FORMATED"] : '&nbsp;')?>
									<? endif; ?>
								</td>
							</tr>
						<? endforeach; ?>
					<? endif; ?>
					<?
				}
				?>
			</table>

		</div>
		<!-- / samTbl -->

	</div>
	<!-- / order-item end -->
	<div class="order-item" id="basketDelAdr"<?= $curSelected != 'dost' ? ' style="display:none"' : '' ?>>
		<h2 class="ttl-o">Адрес доставки</h2>

		<table class="tbl-order tbl-del-adr tbl-order-v">

			<tr>
				<td class="td1"><p class="ttl">Улица</p></td>
				<td class="td2" colspan="3">
					<input class="inp inp-drk inp-t inp-adr" type="text" name="<?= $GLOBALS['FIELDS_CODES']['HIDE_DELIVERY_D_ADDR']['FIELD_NAME'] ?>" value="<?= $GLOBALS['FIELDS_CODES']['HIDE_DELIVERY_D_ADDR']['VALUE'] ?>" />
					<?/*<div class="hint">Пояснение к полю</div>
					<div class="vfield">Пояснение ошибки валидации поля</div>*/?>
				</td>
			</tr>
			<tr>
				<td class="td1"><p class="ttl">Дом</p></td>
				<td class="td2">
					<input class="inp inp-drk inp-t" type="text" name="<?= $GLOBALS['FIELDS_CODES']['HIDE_DELIVERY_D_HOUSE']['FIELD_NAME'] ?>" value="<?= $GLOBALS['FIELDS_CODES']['HIDE_DELIVERY_D_HOUSE']['VALUE'] ?>" />
					<?/*<div class="hint">Пояснение к полю</div>
					<div class="vfield">Пояснение ошибки валидации поля</div>*/?>
				</td>
				<td class="td1"><p class="ttl">Корпус</p></td>
				<td class="td2">
					<input class="inp inp-drk inp-t" type="text" name="<?= $GLOBALS['FIELDS_CODES']['HIDE_DELIVERY_D_CORP']['FIELD_NAME'] ?>" value="<?= $GLOBALS['FIELDS_CODES']['HIDE_DELIVERY_D_CORP']['VALUE'] ?>" />
					<?/*<div class="hint">Пояснение к полю</div>
					<div class="vfield">Пояснение ошибки валидации поля</div>*/?>
				</td>
			</tr>
			<tr>
				<td class="td1"><p class="ttl">Строение</p></td>
				<td class="td2">
					<input class="inp inp-drk inp-t" type="text" name="<?= $GLOBALS['FIELDS_CODES']['HIDE_DELIVERY_D_BUILD']['FIELD_NAME'] ?>" value="<?= $GLOBALS['FIELDS_CODES']['HIDE_DELIVERY_D_BUILD']['VALUE'] ?>" />
					<?/*<div class="hint">Пояснение к полю</div>
					<div class="vfield">Пояснение ошибки валидации поля</div>*/?>
				</td>
				<td class="td1"><p class="ttl">Квартира / офис</p></td>
				<td class="td2">
					<input class="inp inp-drk inp-t" type="text" name="<?= $GLOBALS['FIELDS_CODES']['HIDE_DELIVERY_D_ROOM']['FIELD_NAME'] ?>" value="<?= $GLOBALS['FIELDS_CODES']['HIDE_DELIVERY_D_ROOM']['VALUE'] ?>" />
					<?/*<div class="hint">Пояснение к полю</div>
					<div class="vfield">Пояснение ошибки валидации поля</div>*/?>
				</td>
			</tr>

		</table>
	</div>
	<!-- / order-item end -->
<? else: ?>
	<script>
		window.top.needUpdateOfDelivery = 1;
	</script>
<? endif; ?>
