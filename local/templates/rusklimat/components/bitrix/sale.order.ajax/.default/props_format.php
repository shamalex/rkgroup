<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<?


if (!function_exists("showFilePropertyField"))
{
	function showFilePropertyField($name, $property_fields, $values, $max_file_size_show=50000)
	{
		$res = "";

		if (!is_array($values) || empty($values))
			$values = array(
				"n0" => 0,
			);

		if ($property_fields["MULTIPLE"] == "N")
		{
			$res = "<label for=\"\"><input type=\"file\" size=\"".$max_file_size_show."\" value=\"".$property_fields["VALUE"]."\" name=\"".$name."[0]\" id=\"".$name."[0]\"></label>";
			$res .= "<script> setTimeout(function() {(window.top.$ || $)('#ORDER_FORM input[type=\"file\"]').styler({filePlaceholder: 'Файл не приложен', fileBrowse: 'Приложить файл'});}, window.top.$ ? 50 : 0); </script>";
		}
		else
		{
			$res = '
			<script type="text/javascript">
				function addControl(item)
				{
					var current_name = item.id.split("[")[0],
						current_id = item.id.split("[")[1].replace("[", "").replace("]", ""),
						next_id = parseInt(current_id) + 1;

					var newInput = document.createElement("input");
					newInput.type = "file";
					newInput.name = current_name + "[" + next_id + "]";
					newInput.id = current_name + "[" + next_id + "]";
					newInput.onchange = function() { addControl(this); };

					var br = document.createElement("br");
					var br2 = document.createElement("br");

					BX(item.id).parentNode.appendChild(br);
					BX(item.id).parentNode.appendChild(br2);
					BX(item.id).parentNode.appendChild(newInput);
				}
			</script>
			';

			$res .= "<label for=\"\"><input type=\"file\" size=\"".$max_file_size_show."\" value=\"".$property_fields["VALUE"]."\" name=\"".$name."[0]\" id=\"".$name."[0]\"></label>";
			$res .= "<br/><br/>";
			$res .= "<label for=\"\"><input type=\"file\" size=\"".$max_file_size_show."\" value=\"".$property_fields["VALUE"]."\" name=\"".$name."[1]\" id=\"".$name."[1]\" onChange=\"javascript:addControl(this);\"></label>";
		}

		return $res;
	}
}

if (!function_exists("PrintPropsForm"))
{
	function PrintPropsForm($arSource = array(), $locationTemplate = ".default", $FIELDS_ERRORS = [], $PERSON_TYPE_ID = 1, $company_list, $contact_list)
	{
		
		$arLabels = [];
/*		if ($PERSON_TYPE_ID != 1) {*/
			foreach ($arSource as $arProperties) {
				$arLabels[$arProperties["GROUP_NAME"]] = 0;
			}
/*		}*/
		// p($arLabels);
		
		if (!empty($arSource))
		{
			?>
				<?

				$DO_NOT_SHOW_SCRIPT = false;
				foreach ($arSource as $arProperties)
				{
					$GLOBALS['FIELDS_CODES'][$arProperties["CODE"]] = $arProperties;
					if (strpos($arProperties['CODE'], 'HIDE_') === 0) {
						?>
						
						<input type="hidden" name="<?=$arProperties["FIELD_NAME"]?>" id="<?=$arProperties["CODE"]?>" value="<?=$arProperties["VALUE"]?>" />
						<?
						continue;
					}
					?>
					<span data-property-id-row="<?=intval(intval($arProperties["ID"]))?>" class="<?= (!empty($FIELDS_ERRORS[$arProperties["NAME"]]) ? 'error_here' : '') ?>">

						<?
						/*
						<div class="bx_block r1x3 pt8">
							<?=$arProperties["NAME"]?>
							<?if ($arProperties["REQUIED_FORMATED"]=="Y"):?>
								<span class="bx_sof_req">*</span>
							<?endif?>
						</div>
						*/
						?>

						<?
						
						if (isset($arLabels[$arProperties["GROUP_NAME"]]) && $arLabels[$arProperties["GROUP_NAME"]] == 0 ) {
							$arLabels[$arProperties["GROUP_NAME"]] = 1;
							?>
							<?if ($PERSON_TYPE_ID != 1) {?>
							<tr>
								<td class="td1" colspan="2">
									<h3 class="ttl2-o" style="margin-bottom: 5px;">
										<? if($arProperties["GROUP_NAME"] == 'Личные данные'): ?>
											Контактное лицо
										<? elseif($arProperties["GROUP_NAME"] == 'Данные компании'): ?>
											<br />
											Юридическое лицо
										<? endif; ?>
									</h3>
								</td>
							</tr>
							<?}?>
							<?global $USER;
							if($arProperties["GROUP_NAME"] == 'Данные компании' && $USER->IsAuthorized()){?>
							<tr>
								<td class="td1">
									<p class="ttl">Данные компании</p>
								</td>
								<td class="td2">
								
									<!-- city-sel-holder -->
									<div class="city-sel-holder">
										
										<div class="city-sel" id="company-sel"><?=$arResult["SELECTED_COMPANY"]?></div>
										<div class="hint">Выберите одну из компаний, указанных Вами ранее!</div>

										<!-- city-sel-pop -->
										<div class="city-sel-pop" id="company-sel-pop">
											<!-- city-sel-pop .w-->
											<div class="w">
											
												<!-- #citySel1 -->
												<div id="citySel1">
													<div class="city-sel-list c">
												
														<!-- column 1 -->
														<ul class="ul-orange">														
															<?foreach($company_list as $id=>$comp){?>
															<li<?if($_COOKIE["address-sel"]==$id){?> class="active"<?}?>><a href="#" -data-id="<?=$id?>"><?=$comp["NAME"]?></a></li>
															<?}?>												
														</ul>
														<!--/ column 1 end -->
												
													</div>
													<!-- / city-sel-list end -->
													
												</div>
												<!-- / #citySel1 -->
									
											</div>
											<!-- / city-sel-pop > .w end-->
										</div>
										<!-- / city-sel-pop end -->

										
									</div>
									<!-- / city-sel-holder end -->
								
								</td>
							</tr>
							<?}elseif($arProperties["GROUP_NAME"] == 'Личные данные' && $USER->IsAuthorized()){?>
							<tr>
								<td class="td1">
									<p class="ttl">Контактное лицо</p>
								</td>
								<td class="td2">
								
									<!-- city-sel-holder -->
									<div class="city-sel-holder">
										
										<div class="city-sel" id="contact-sel"><?=$arResult["SELECTED_CONTACT"]?></div>
										<div class="hint">Выберите одно из контактных лиц, указанных Вами ранее!</div>

										<!-- city-sel-pop -->
										<div class="city-sel-pop" id="contact-sel-pop">
											<!-- city-sel-pop .w-->
											<div class="w">
											
												<!-- #citySel1 -->
												<div id="citySel0">
													<div class="city-sel-list c">
												
														<!-- column 1 -->
														<ul class="ul-orange">														
															<?foreach($contact_list as $id=>$contact){?>
															<li<?if($_COOKIE["contact-sel"]==$id){?> class="active"<?}?>><a href="#" -data-id="<?=$id?>"><?=$contact["NAME"]?></a></li>
															<?}?>												
														</ul>
														<!--/ column 1 end -->
												
													</div>
													<!-- / city-sel-list end -->
													
												</div>
												<!-- / #citySel1 -->
									
											</div>
											<!-- / city-sel-pop > .w end-->
										</div>
										<!-- / city-sel-pop end -->

										
									</div>
									<!-- / city-sel-holder end -->
								
								</td>
							</tr>
							<?}?>							
							<?
						}
						
						if ($arProperties["TYPE"] == "CHECKBOX")
						{
							if ($arProperties["NAME"] == "Я ознакомлен и согласен с условиями продажи товаров") {
								
								$DO_NOT_SHOW_SCRIPT = true;
								?>
								<input type="hidden" name="<?=$arProperties["FIELD_NAME"]?>" value="">
								<input type="checkbox" name="<?=$arProperties["FIELD_NAME"]?>" id="<?=$arProperties["FIELD_NAME"]?>" value="Y" class="test <?= (!empty($FIELDS_ERRORS[$arProperties["NAME"]]) ? 'inp-inv' : '') ?><?= ($arProperties["REQUIED"] == 'Y' ? ' required_field' : '') ?>" data-code="<?= $arProperties["CODE"] ?>" checked>
								<?
							} else {
								?>
								<div class="bx_block r1x3 pt8">
									<input type="hidden" name="<?=$arProperties["FIELD_NAME"]?>" value="">
									<input type="checkbox" name="<?=$arProperties["FIELD_NAME"]?>" id="<?=$arProperties["FIELD_NAME"]?>" value="Y" class="<?= ($arProperties["REQUIED"] == 'Y' ? ' required_field' : '') ?>" data-code="<?= $arProperties["CODE"] ?>" <?if ($arProperties["CHECKED"]=="Y") echo " checked";?>>
									<?if (strlen(trim($arProperties["DESCRIPTION"])) > 0):?>
										<div class="bx_description"><?=$arProperties["DESCRIPTION"]?></div>
									<?endif?>
								</div>
								<?
							}
						}
						elseif ($arProperties["TYPE"] == "TEXT")
						{
							?>
							<tr>
								<td class="td1"><p class="ttl"><?=$arProperties["NAME"]?></p></td>
								<td class="td2">
									<div class="relat">
										<?
										$classes = [
											'PHONE' => 'ttPhone',
											'EMAIL' => 'ttEmail',
										];
										?>
										<input type="text" maxlength="250" size="<?=$arProperties["SIZE1"]?>" value="<?=$arProperties["VALUE"]?>" name="<?=$arProperties["FIELD_NAME"]?>" class="inp inp-drk inp-t inp-td2<?= (!empty($classes[$arProperties["CODE"]]) ? ' '.$classes[$arProperties["CODE"]] : '') ?><?= (!empty($FIELDS_ERRORS[$arProperties["NAME"]]) ? ' inp-inv' : '') ?><?= ($arProperties["REQUIED"] == 'Y' ? ' required_field' : '') ?>" id="<?=$arProperties["FIELD_NAME"]?>" data-code="<?= $arProperties["CODE"] ?>" <?if($arProperties["CODE"]=="PHONE"){?>pattern="\+7 ?\(\d\d\d\) ?\d\d\d-\d\d-\d\d" placeholder="+7 (###) ###-##-##" oninvalid="setCustomValidity('');$(this).parent().find('.vfield').html('Некорректный телефон');" oninput="setCustomValidity('');$(this).parent().find('.vfield').html('');" data-mask="+7 (000) 000-00-00"<?}?> />
										<? if(!empty($arProperties["DESCRIPTION"])): ?>
											<div class="hint"><?= $arProperties["DESCRIPTION"] ?></div>
										<? endif; ?>
										<? if(!empty($FIELDS_ERRORS[$arProperties["NAME"]])): ?>
											<div class="vfield"><?= $FIELDS_ERRORS[$arProperties["NAME"]] ?></div>
										<? endif; ?>

										<?/*global $USER;
										if($arProperties["CODE"]=="PHONE" && !$USER->IsAuthorized()){?>
										<div id="ttPhone" class="e permatip" style="display:none">
											<div class="tt tt-check">
												<div class="tt-ico tt-msg"></div>
												<div class="tt-dsc">
													
													<!-- #ttSms1 -->
													<div id="codelink" style="display:none">
														<p>Похоже, что вы к нам уже заходили!<br>Для продолжения получите SMS код и введите его здесь! </p>
														<div class="btn btn-30 btn-b btn-tt-sms1">Получить SMS код</div>
													</div>
													<!-- / #ttSms1 end -->
													
													<!-- #ttSms2 -->
													<div id="coderesult" style="display:none">
														<p></p>
													</div>
													<!-- / #ttSms2 end -->

													<!-- #ttSms2 -->
													<div id="codeform" style="display:none">
														<p>Мы отправили Вам SMS с кодом!</p>
														<p>Введите код</p>
														<div class="c">
															<input class="inp inp-drk inp-tt-sms" type="text" id="SMS_CODE">
															<div class="btn btn-30 btn-b btn-tt-sms2">ввести sms код</div>
														</div>
													</div>
													<!-- / #ttSms2 end -->
													
												</div>
												<!-- / tt-dsc end -->
											</div>
											<!-- / tt-check end -->
										</div>	
										<?}*/?>
									</div>
									<!-- / rel -->								
								</td>
							</tr>
							<?
						}
						elseif ($arProperties["TYPE"] == "SELECT")
						{
							?>
							<tr>
								<td class="td1"><p class="ttl"><?=$arProperties["NAME"]?></p></td>
								<td class="td2">
									<select name="<?=$arProperties["FIELD_NAME"]?>" id="<?=$arProperties["FIELD_NAME"]?>" size="<?=$arProperties["SIZE1"]?>" class="yur-select ik-30-lt<?= (!empty($FIELDS_ERRORS[$arProperties["NAME"]]) ? ' inp-inv' : '') ?><?= ($arProperties["REQUIED"] == 'Y' ? ' required_field' : '') ?>" name="yur-select" data-code="<?= $arProperties["CODE"] ?>">
										<?foreach($arProperties["VARIANTS"] as $arVariants):?>
											<option value="<?=$arVariants["VALUE"]?>"<?=$arVariants["SELECTED"] == "Y" ? " selected" : ''?>><?=$arVariants["NAME"]?></option>
										<?endforeach?>
									</select>
									<? if(!empty($arProperties["DESCRIPTION"])): ?>
										<div class="hint"><?= $arProperties["DESCRIPTION"] ?></div>
									<? endif; ?>
									<? if(!empty($FIELDS_ERRORS[$arProperties["NAME"]])): ?>
										<div class="vfield"><?= $FIELDS_ERRORS[$arProperties["NAME"]] ?></div>
									<? endif; ?>
									<script>
										setTimeout(function() {
											(window.top.$ || $)('.yur-select').ikSelect({
												autoWidth: false,
												ddFullWidth: false,
												customClass: 'yur-select'
											});
										}, window.top.$ ? 50 : 0);
									</script>
								</td>
							</tr>
							<?
						}
						elseif ($arProperties["TYPE"] == "MULTISELECT")
						{
							?>
							<div class="bx_block r3x1">
								<select multiple name="<?=$arProperties["FIELD_NAME"]?>" id="<?=$arProperties["FIELD_NAME"]?>" size="<?=$arProperties["SIZE1"]?>" class="<?= ($arProperties["REQUIED"] == 'Y' ? ' required_field' : '') ?>" data-code="<?= $arProperties["CODE"] ?>">
									<?foreach($arProperties["VARIANTS"] as $arVariants):?>
										<option value="<?=$arVariants["VALUE"]?>"<?=$arVariants["SELECTED"] == "Y" ? " selected" : ''?>><?=$arVariants["NAME"]?></option>
									<?endforeach?>
								</select>
								<?if (strlen(trim($arProperties["DESCRIPTION"])) > 0):?>
									<div class="bx_description"><?=$arProperties["DESCRIPTION"]?></div>
								<?endif?>
							</div>
							<?
						}
						elseif ($arProperties["TYPE"] == "TEXTAREA")
						{
							$rows = ($arProperties["SIZE2"] > 10) ? 4 : $arProperties["SIZE2"];
							?>
							<tr>
								<td class="td1"><p class="ttl"><?=$arProperties["NAME"]?></p></td>
								<td class="td2">
									<textarea rows="<?=$rows?>" cols="<?=$arProperties["SIZE1"]?>" name="<?=$arProperties["FIELD_NAME"]?>" id="<?=$arProperties["FIELD_NAME"]?>" class="txtarea txtarea-drk order-txt yur-txt<?= (!empty($FIELDS_ERRORS[$arProperties["NAME"]]) ? ' inp-inv' : '') ?><?= ($arProperties["REQUIED"] == 'Y' ? ' required_field' : '') ?>"<? /* if(!empty($arProperties["DESCRIPTION"])): ?> placeholder="<?= $arProperties["DESCRIPTION"] ?>"<? endif; */ ?> data-code="<?= $arProperties["CODE"] ?>"><?=$arProperties["VALUE"]?></textarea>
									<? if(!empty($arProperties["DESCRIPTION"])): ?>
										<div class="hint"><?= $arProperties["DESCRIPTION"] ?></div>
									<? endif; ?>
									<? if(!empty($FIELDS_ERRORS[$arProperties["NAME"]])): ?>
										<div class="vfield"><?= $FIELDS_ERRORS[$arProperties["NAME"]] ?></div>
									<? endif; ?>
								</td>
							</tr>
							<?
						}
						elseif ($arProperties["TYPE"] == "LOCATION")
						{
							?>
							<div class="bx_block r3x1" style="display: none;">
								<?
								// p($arProperties);
								$value = 129;
								// if (is_array($arProperties["VARIANTS"]) && count($arProperties["VARIANTS"]) > 0)
								// {
									// foreach ($arProperties["VARIANTS"] as $arVariant)
									// {
										// if ($arVariant["SELECTED"] == "Y")
										// {
											// $value = $arVariant["ID"];
											// break;
										// }
									// }
								// }

								// here we can get '' or 'popup'
								// map them, if needed
								if(CSaleLocation::isLocationProMigrated())
								{
									$locationTemplateP = $locationTemplate == 'popup' ? 'search' : 'steps';
									$locationTemplateP = $_REQUEST['PERMANENT_MODE_STEPS'] == 1 ? 'steps' : $locationTemplateP; // force to "steps"
								}
								?>

								<?if($locationTemplateP == 'steps'):?>
									<input type="hidden" id="LOCATION_ALT_PROP_DISPLAY_MANUAL[<?=intval($arProperties["ID"])?>]" name="LOCATION_ALT_PROP_DISPLAY_MANUAL[<?=intval($arProperties["ID"])?>]" value="<?=($_REQUEST['LOCATION_ALT_PROP_DISPLAY_MANUAL'][intval($arProperties["ID"])] ? '1' : '0')?>" />
								<?endif?>

								<?CSaleLocation::proxySaleAjaxLocationsComponent(array(
									"AJAX_CALL" => "N",
									"COUNTRY_INPUT_NAME" => "COUNTRY",
									"REGION_INPUT_NAME" => "REGION",
									"CITY_INPUT_NAME" => $arProperties["FIELD_NAME"],
									"CITY_OUT_LOCATION" => "Y",
									"LOCATION_VALUE" => $value,
									"ORDER_PROPS_ID" => $arProperties["ID"],
									"ONCITYCHANGE" => ($arProperties["IS_LOCATION"] == "Y" || $arProperties["IS_LOCATION4TAX"] == "Y") ? "submitForm()" : "",
									"SIZE1" => $arProperties["SIZE1"],
								),
									array(
										"ID" => $value,
										"CODE" => "",
										"SHOW_DEFAULT_LOCATIONS" => "Y",

										// function called on each location change caused by user or by program
										// it may be replaced with global component dispatch mechanism coming soon
										"JS_CALLBACK" => "submitFormProxy",

										// function window.BX.locationsDeferred['X'] will be created and lately called on each form re-draw.
										// it may be removed when sale.order.ajax will use real ajax form posting with BX.ProcessHTML() and other stuff instead of just simple iframe transfer
										"JS_CONTROL_DEFERRED_INIT" => intval($arProperties["ID"]),

										// an instance of this control will be placed to window.BX.locationSelectors['X'] and lately will be available from everywhere
										// it may be replaced with global component dispatch mechanism coming soon
										"JS_CONTROL_GLOBAL_ID" => intval($arProperties["ID"]),

										"DISABLE_KEYBOARD_INPUT" => "Y",
										"PRECACHE_LAST_LEVEL" => "Y",
										"PRESELECT_TREE_TRUNK" => "Y",
										"SUPPRESS_ERRORS" => "Y"
									),
									$locationTemplateP,
									true,
									'location-block-wrapper'
								)?>

								<?if (strlen(trim($arProperties["DESCRIPTION"])) > 0):?>
									<div class="bx_description"><?=$arProperties["DESCRIPTION"]?></div>
								<?endif?>
							<?
						}
						elseif ($arProperties["TYPE"] == "RADIO")
						{
							?>
							<div class="bx_block r3x1">
								<?
								if (is_array($arProperties["VARIANTS"]))
								{
									foreach($arProperties["VARIANTS"] as $arVariants):
									?>
										<input
											type="radio"
											name="<?=$arProperties["FIELD_NAME"]?>"
											id="<?=$arProperties["FIELD_NAME"]?>_<?=$arVariants["VALUE"]?>"
											value="<?=$arVariants["VALUE"]?>" <?if($arVariants["CHECKED"] == "Y") echo " checked";?> />

										<label for="<?=$arProperties["FIELD_NAME"]?>_<?=$arVariants["VALUE"]?>"><?=$arVariants["NAME"]?></label></br>
									<?
									endforeach;
								}
								?>
								<?if (strlen(trim($arProperties["DESCRIPTION"])) > 0):?>
									<div class="bx_description"><?=$arProperties["DESCRIPTION"]?></div>
								<?endif?>
							</div>
							<?
						}
						elseif ($arProperties["TYPE"] == "FILE")
						{
							if ($arProperties["NAME"] == "Приложение") {
								echo showFilePropertyField("ORDER_PROP_".$arProperties["ID"], $arProperties, $arProperties["VALUE"], $arProperties["SIZE1"]);
							} else {
								?>
								<div class="bx_block r3x1">
									<?=showFilePropertyField("ORDER_PROP_".$arProperties["ID"], $arProperties, $arProperties["VALUE"], $arProperties["SIZE1"])?>
									<?if (strlen(trim($arProperties["DESCRIPTION"])) > 0):?>
										<div class="bx_description"><?=$arProperties["DESCRIPTION"]?></div>
									<?endif?>
								</div>
								<?
							}
						}
						elseif ($arProperties["TYPE"] == "DATE")
						{
							?>
							<div>
								<?
								global $APPLICATION;

								$APPLICATION->IncludeComponent('bitrix:main.calendar', '', array(
									'SHOW_INPUT' => 'Y',
									'INPUT_NAME' => "ORDER_PROP_".$arProperties["ID"],
									'INPUT_VALUE' => $arProperties["VALUE"],
									'SHOW_TIME' => 'N'
								), null, array('HIDE_ICONS' => 'N'));
								?>
								<?if (strlen(trim($arProperties["DESCRIPTION"])) > 0):?>
									<div class="bx_description"><?=$arProperties["DESCRIPTION"]?></div>
								<?endif?>
							</div>
							<?
						}
						?>
					</span>

					<?if(CSaleLocation::isLocationProEnabled() && !$DO_NOT_SHOW_SCRIPT):?>

					<?
					$propertyAttributes = array(
						'type' => $arProperties["TYPE"],
						'valueSource' => $arProperties['SOURCE'] == 'DEFAULT' ? 'default' : 'form' // value taken from property DEFAULT_VALUE or it`s a user-typed value?
					);

					if(intval($arProperties['IS_ALTERNATE_LOCATION_FOR']))
						$propertyAttributes['isAltLocationFor'] = intval($arProperties['IS_ALTERNATE_LOCATION_FOR']);

					if(intval($arProperties['CAN_HAVE_ALTERNATE_LOCATION']))
						$propertyAttributes['altLocationPropId'] = intval($arProperties['CAN_HAVE_ALTERNATE_LOCATION']);

					if($arProperties['IS_ZIP'] == 'Y')
						$propertyAttributes['isZip'] = true;
					?>

						<script>

							<?// add property info to have client-side control on it?>
							(window.top.BX || BX).saleOrderAjax.addPropertyDesc(<?=CUtil::PhpToJSObject(array(
									'id' => intval($arProperties["ID"]),
									'attributes' => $propertyAttributes
								))?>);

						</script>
					<?endif?>
					<?
				}
				?>
			<?
		}
	}
}
?>
