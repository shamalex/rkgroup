<?
global $arGeoData;

CModule::IncludeModule("iblock");
$resGD=CIBlockElement::GetList(array(),array("IBLOCK_ID"=>32,"PROPERTY_network"=>$arGeoData["CUR_CITY"]["CITY_CODE"]),false,false,array("ID","PROPERTY_network","PROPERTY_bank","PROPERTY_url","PROPERTY_login","PROPERTY_password","PROPERTY_stage2"));
if(!$arGD=$resGD->GetNext()){
	foreach($arResult["PAY_SYSTEM"] as $key=>$arPaySystem)
	{
		if($arPaySystem["PSA_ACTION_FILE"]=="/bitrix/php_interface/include/sale_payment/online"){
			unset($arResult["PAY_SYSTEM"][$key]);
			break;
		};
	}
}

if (!empty($arResult["ORDER"]["ID"])) {
	
	$basket = array();

	$dbBasket = CSaleBasket::GetList(
		array("ID" => "ASC", "NAME" => "ASC"),
		array("ORDER_ID" => $arResult["ORDER"]["ID"]),
		false,
		false,
		array("ID", "DETAIL_PAGE_URL", "NAME", "NOTES", "QUANTITY", "PRICE",
			"CURRENCY", "PRODUCT_ID", "DISCOUNT_PRICE", "WEIGHT", "CATALOG_XML_ID",
			"VAT_RATE", "PRODUCT_XML_ID", "TYPE", "SET_PARENT_ID", "MEASURE_CODE", "MEASURE_NAME", "MODULE"
		)
	);
	while ($arItem = $dbBasket->Fetch())
	{
		if (CSaleBasketHelper::isSetItem($arItem))
			continue;

		// adjust some sale params
		$arItem["PRICE_VAT_VALUE"] = (($arItem["PRICE"] / ($arItem["VAT_RATE"] +1)) * $arItem["VAT_RATE"]);
		$arItem["WEIGHT"] = doubleval($arItem["WEIGHT"]);

		$basket[$arItem['ID']] = $arItem;
	}
	
	$ID_TO_CATALOG = [];
	foreach($basket as $arItem) {
		$ID_TO_CATALOG[$arItem['PRODUCT_ID']] = $arItem['ID'];
	}

	if (!empty($ID_TO_CATALOG)) {
		$arFilter = Array("IBLOCK_ID"=>8, 'ID'=>array_keys($ID_TO_CATALOG));
		$arSelect = Array("ID", "NAME", "CODE", "XML_ID", "IBLOCK_SECTION_ID", "PROPERTY_FULL_NAME", "PROPERTY_ITEM_NAME", "PROPERTY_CODE", "PROPERTY_NS_CODE", "PROPERTY_EL_BRAND.NAME");
		$res = CIBlockElement::GetList(Array(), $arFilter, false, false, $arSelect);  
		while($arFields = $res->Fetch()) {
			$arFields['CATALOG_ID'] = $arFields['ID'];
			$ID = $arFields['ID'];
			
			$arSection = array();
			$arSection["ID"] = $arFields['IBLOCK_SECTION_ID'];
			$arSection["PATH"] = array();
			if (!empty($arSection["ID"])) {
				$rsPath = CIBlockSection::GetNavChain($arFilter["IBLOCK_ID"], $arSection["ID"]);
				// $rsPath->SetUrlTemplates("", $arParams["SECTION_URL"]);
				while($arPath = $rsPath->GetNext()) {
					// $ipropValues = new \Bitrix\Iblock\InheritedProperty\SectionValues($arFilter["IBLOCK_ID"], $arPath["ID"]);
					// $arPath["IPROPERTY_VALUES"] = $ipropValues->getValues();
					$arSection["PATH"][] = $arPath;
				}
			}
			$arFields['SECTION'] = $arSection;
			
			$basket[$ID_TO_CATALOG[$ID]] = array_merge($arFields, $basket[$ID_TO_CATALOG[$ID]]);
		}
	}
	
	$arResult["ORDER"]["BASKET"] = $basket;
	
} else {
	
	$arResult['GEO'] = [];


	$arSort = array();
	$arFilter = Array("IBLOCK_ID"=>15, "ID"=>$arGeoData['CUR_CITY']['ID']);
	$arSelect = false;
	$res = CIBlockElement::GetList($arSort, $arFilter, false, false, $arSelect);  
	if ($ob = $res->GetNextElement()) {
		$arResult['GEO'] = $ob->GetFields();
		$arResult['GEO']['PROPERTIES'] = $ob->GetProperties();
	}
	
	
	
	if ($arResult['GEO']['PROPERTIES']['predoplata_required']['VALUE'] == 'да') {
		foreach($arResult["PAY_SYSTEM"] as $i => $arPaySystem) {
			if ($arPaySystem['PSA_ID'] == 10) {
				unset($arResult["PAY_SYSTEM"][$i]);
			}
		}
	}
	

	$QUANITY_PROP = preg_replace('/\-+/ui', '_', $arGeoData['CUR_CITY']['CITY_CODE1']);
	$QUANITY_PROP = strtoupper('COUNT_'.$QUANITY_PROP);
	$QUANITY_PROP2 = preg_replace('/\-+/ui', '_', $arGeoData['CUR_CITY']['CITY_CODE2']);
	$QUANITY_PROP2 = strtoupper('COUNT_'.$QUANITY_PROP2);
	
	$arResult['QUANITY_PROP'] = $QUANITY_PROP;
	$arResult['QUANITY_PROP2'] = $QUANITY_PROP2;
	
	$ID_TO_CATALOG = [];
	foreach($arResult['BASKET_ITEMS'] as $i=>$arItem) {
		$ID_TO_CATALOG[$arItem['PRODUCT_ID']] = $i;
	}

	if (!empty($ID_TO_CATALOG)) {
		$arFilter = Array("IBLOCK_ID"=>8, 'ID'=>array_keys($ID_TO_CATALOG));
		$arSelect = Array("ID", "PROPERTY_".$QUANITY_PROP, "PROPERTY_".$QUANITY_PROP2, "PROPERTY_IS_BIG_SIZE");
		$res = CIBlockElement::GetList(Array(), $arFilter, false, false, $arSelect);  
		while($arFields = $res->Fetch()) {
			
			$ID = $arFields['ID'];
			
			$arResult['BASKET_ITEMS'][$ID_TO_CATALOG[$ID]] = array_merge($arResult['BASKET_ITEMS'][$ID_TO_CATALOG[$ID]], $arFields);
			
		}
	}
	
	$arResult['LOCATIONS'] = [];

	$arSort = array('SORT'=>'ASC', 'NAME'=>'ASC');
	$arFilter = Array("IBLOCK_ID"=>16, "ACTIVE"=>"Y", "CODE"=>$arGeoData['CUR_CITY']['CITY_CODE']);
	$arSelect = false;
	$res = CIBlockElement::GetList($arSort, $arFilter, false, false, $arSelect);  
	while ($ob = $res->GetNextElement()) {
		$arFields = $ob->GetFields();
		$arFields['PROPERTIES'] = $ob->GetProperties();
		$arResult['LOCATIONS'][] = $arFields;
	}
	
}

$rsAddress=CIBlockElement::GetList(array(), array("IBLOCK_ID"=>30,"PROPERTY_USER"=>$USER->GetID()), false, false, array("ID","NAME","PROPERTY_city","PROPERTY_street","PROPERTY_house","PROPERTY_housing","PROPERTY_build","PROPERTY_apartment","PROPERTY_USER","PROPERTY_default"));
while ($arFields=$rsAddress->GetNext()) {
	if($arFields["PROPERTY_DEFAULT_VALUE"]=="Y") $default=true;
	$arResult["DELIVERY_ADDRESS_LIST"][$arFields["ID"]]=$arFields;
}

if($_COOKIE["address-sel"] && $arResult["DELIVERY_ADDRESS_LIST"][$_COOKIE["address-sel"]]["PROPERTY_CITY_VALUE"]!=$arGeoData['CUR_CITY']['ID']){
	setcookie ("address-sel", "", time() - 3600);
	unset($_COOKIE["address-sel"]);
}

$rsCompany=CIBlockElement::GetList(array(), array("IBLOCK_ID"=>31,"PROPERTY_USER"=>$USER->GetID()), false, false, array("ID","NAME","PROPERTY_org_type","PROPERTY_org_name","PROPERTY_inn","PROPERTY_kpp","PROPERTY_bik","PROPERTY_rs","PROPERTY_bank","PROPERTY_ks","PROPERTY_ur_adr","PROPERTY_USER","PROPERTY_default"));
while ($arFields=$rsCompany->GetNext()) {
	if($arFields["PROPERTY_DEFAULT_VALUE"]=="Y") $default=true;
	$arResult["COMPANY_LIST"][$arFields["ID"]]=$arFields;
}

$org_type=[];
$property_enums = CIBlockPropertyEnum::GetList(Array("DEF"=>"DESC", "SORT"=>"ASC"), Array("IBLOCK_ID"=>31, "CODE"=>"org_type"));
while($enum_fields = $property_enums->GetNext())
{
	$org_type[$enum_fields["ID"]]=$enum_fields["VALUE"];
}

$rsContact=CIBlockElement::GetList(array(), array("IBLOCK_ID"=>33,"PROPERTY_USER"=>$USER->GetID()), false, false, array("ID","NAME","PROPERTY_fio","PROPERTY_phone","PROPERTY_email","PROPERTY_USER","PROPERTY_default"));
while ($arFields=$rsContact->GetNext()) {
	if($arFields["PROPERTY_DEFAULT_VALUE"]=="Y") $default=true;
	$arResult["CONTACT_LIST"][$arFields["ID"]]=$arFields;
}

$db_vars = CSaleOrderPropsVariant::GetList(
	array("SORT" => "ASC"),
	array("ORDER_PROPS_ID" => 25)
);
while ($vars = $db_vars->Fetch())
{
	$id=array_search($vars["NAME"],$org_type);
	if(is_numeric($id))
		$arResult["org_types"][$id]=$vars["VALUE"];
}


