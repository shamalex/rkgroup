<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<div class="brd order-w" id="orderBlock">

	<!-- order -->
	<div class="order">
		
		<div class="txt-block">
			<?
			if (!empty($arResult["ORDER"]))
			{
				?>
				<h2 class="ttl-o"><?=GetMessage("SOA_TEMPL_ORDER_COMPLETE")?></h2>
				<table class="sale_order_full_table">
					<tr>
						<td>
							<?= GetMessage("SOA_TEMPL_ORDER_SUC", Array("#ORDER_DATE#" => $arResult["ORDER"]["DATE_INSERT"], "#ORDER_ID#" => $arResult["ORDER"]["ACCOUNT_NUMBER"]))?>
							<br /><br />
							<?= GetMessage("SOA_TEMPL_ORDER_SUC1", Array("#LINK#" => $arParams["PATH_TO_PERSONAL"])) ?>
						</td>
					</tr>
				</table>
				<?
				if (!empty($arResult["PAY_SYSTEM"]))
				{
					?>
					<br /><br />

					<table class="sale_order_full_table">
						<tr>
							<td class="ps_logo">
								<div class="pay_name"><?=GetMessage("SOA_TEMPL_PAY")?></div>
								<?=CFile::ShowImage($arResult["PAY_SYSTEM"]["LOGOTIP"], 100, 100, "border=0", "", false);?>
								<div class="paysystem_name"><?= $arResult["PAY_SYSTEM"]["NAME"] ?></div><br>
							</td>
						</tr>
						<?
						if (strlen($arResult["PAY_SYSTEM"]["ACTION_FILE"]) > 0)
						{
							?>
							<tr>
								<td>
									<?
									if ($arResult["PAY_SYSTEM"]["NEW_WINDOW"] == "Y")
									{
										?>
										<script language="JavaScript">
											window.open('<?=$arParams["PATH_TO_PAYMENT"]?>?ORDER_ID=<?=urlencode(urlencode($arResult["ORDER"]["ACCOUNT_NUMBER"]))?>&PAYMENT_ID=<?=$arResult['ORDER']["PAYMENT_ID"]?>');
										</script>
										<?= GetMessage("SOA_TEMPL_PAY_LINK", Array("#LINK#" => $arParams["PATH_TO_PAYMENT"]."?ORDER_ID=".urlencode(urlencode($arResult["ORDER"]["ACCOUNT_NUMBER"]))."&PAYMENT_ID=".$arResult['ORDER']["PAYMENT_ID"]))?>
										<?
										if (CSalePdf::isPdfAvailable() && CSalePaySystemsHelper::isPSActionAffordPdf($arResult['PAY_SYSTEM']['ACTION_FILE']))
										{
											?><br />
											<?= GetMessage("SOA_TEMPL_PAY_PDF", Array("#LINK#" => $arParams["PATH_TO_PAYMENT"]."?ORDER_ID=".urlencode(urlencode($arResult["ORDER"]["ACCOUNT_NUMBER"]))."&pdf=1&DOWNLOAD=Y")) ?>
											<?
										}
									}
									else
									{
										if (strlen($arResult["PAY_SYSTEM"]["PATH_TO_ACTION"])>0)
										{
											try
											{
												include($arResult["PAY_SYSTEM"]["PATH_TO_ACTION"]);
											}
											catch(\Bitrix\Main\SystemException $e)
											{
												if($e->getCode() == CSalePaySystemAction::GET_PARAM_VALUE)
													$message = GetMessage("SOA_TEMPL_ORDER_PS_ERROR");
												else
													$message = $e->getMessage();

												echo '<span style="color:red;">'.$message.'</span>';
											}
										}
									}
									?>
								</td>
							</tr>
							<?
						}
						?>
					</table>
					<?
				}
			}
			else
			{
				?>
				<b><?=GetMessage("SOA_TEMPL_ERROR_ORDER")?></b><br /><br />

				<table class="sale_order_full_table">
					<tr>
						<td>
							<?=GetMessage("SOA_TEMPL_ERROR_ORDER_LOST", Array("#ORDER_ID#" => $arResult["ACCOUNT_NUMBER"]))?>
							<?=GetMessage("SOA_TEMPL_ERROR_ORDER_LOST1")?>
						</td>
					</tr>
				</table>
				<?
			}
			?>
		</div>
	</div>
</div>

<script>
	window.dataLayer = window.dataLayer || [];
	window.dataLayer.push({
		"event": "ecpc",
		"ecommerce": {
			"currencyCode": "RUB",
			"purchase": {
				"actionField": {
					"id" : "<?= $arResult["ORDER"]["ID"] ?>",
					"affiliation" : "Интернет-магазин Русклимат",
					"revenue" : <?= number_format($arResult["ORDER"]['PRICE'], 2, '.', '') ?>
				},
				"products": [
					<?
					$cnt = count($arResult["ORDER"]["BASKET"]);
					$now = 1;
					?>
					<? foreach($arResult["ORDER"]["BASKET"] as $arBasketItem): ?>
						<?
						$cats = [];
						foreach($arBasketItem['SECTION']['PATH'] as $arSection) {
							$cats[] = $arSection['NAME'];
						}
						?>
						{
							"id": "<?= $arBasketItem['PROPERTY_CODE_VALUE'] ?>",
							"name": "<?= $arBasketItem['PROPERTY_ITEM_NAME_VALUE'] ?>",
							"price": <?= number_format($arBasketItem['PRICE'], 2, '.', '') ?>,
							"brand": "<?= $arBasketItem['PROPERTY_EL_BRAND_NAME'] ?>",
							"category": "<?= implode('/', $cats) ?>",
							"quantity": <?= $arBasketItem['QUANTITY'] ?>
						}<?= $now != $cnt ? "," : "" ?>
						<?
							$now++;
						?>
						
					<? endforeach ?>
				]
			}
		}
	});
</script>
