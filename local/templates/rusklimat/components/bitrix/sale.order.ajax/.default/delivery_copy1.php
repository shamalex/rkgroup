<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
global $arGeoData;
?>

<script type="text/javascript">
	function fShowStore(id, showImages, formWidth, siteId)
	{
		var strUrl = '<?=$templateFolder?>' + '/map.php';
		var strUrlPost = 'delivery=' + id + '&showImages=' + showImages + '&siteId=' + siteId;

		var storeForm = new BX.CDialog({
					'title': '<?=GetMessage('SOA_ORDER_GIVE')?>',
					head: '',
					'content_url': strUrl,
					'content_post': strUrlPost,
					'width': formWidth,
					'height':450,
					'resizable':false,
					'draggable':false
				});

		var button = [
				{
					title: '<?=GetMessage('SOA_POPUP_SAVE')?>',
					id: 'crmOk',
					'action': function ()
					{
						GetBuyerStore();
						BX.WindowManager.Get().Close();
					}
				},
				BX.CDialog.btnCancel
			];
		storeForm.ClearButtons();
		storeForm.SetButtons(button);
		storeForm.Show();
	}

	function GetBuyerStore()
	{
		BX('BUYER_STORE').value = BX('POPUP_STORE_ID').value;
		//BX('ORDER_DESCRIPTION').value = '<?=GetMessage("SOA_ORDER_GIVE_TITLE")?>: '+BX('POPUP_STORE_NAME').value;
		BX('store_desc').innerHTML = BX('POPUP_STORE_NAME').value;
		BX.show(BX('select_store'));
	}

	function showExtraParamsDialog(deliveryId)
	{
		var strUrl = '<?=$templateFolder?>' + '/delivery_extra_params.php';
		var formName = 'extra_params_form';
		var strUrlPost = 'deliveryId=' + deliveryId + '&formName=' + formName;

		if(window.BX.SaleDeliveryExtraParams)
		{
			for(var i in window.BX.SaleDeliveryExtraParams)
			{
				strUrlPost += '&'+encodeURI(i)+'='+encodeURI(window.BX.SaleDeliveryExtraParams[i]);
			}
		}

		var paramsDialog = new BX.CDialog({
			'title': '<?=GetMessage('SOA_ORDER_DELIVERY_EXTRA_PARAMS')?>',
			head: '',
			'content_url': strUrl,
			'content_post': strUrlPost,
			'width': 500,
			'height':200,
			'resizable':true,
			'draggable':false
		});

		var button = [
			{
				title: '<?=GetMessage('SOA_POPUP_SAVE')?>',
				id: 'saleDeliveryExtraParamsOk',
				'action': function ()
				{
					insertParamsToForm(deliveryId, formName);
					BX.WindowManager.Get().Close();
				}
			},
			BX.CDialog.btnCancel
		];

		paramsDialog.ClearButtons();
		paramsDialog.SetButtons(button);
		//paramsDialog.adjustSizeEx();
		paramsDialog.Show();
	}

	function insertParamsToForm(deliveryId, paramsFormName)
	{
		var orderForm = BX("ORDER_FORM"),
			paramsForm = BX(paramsFormName);
			wrapDivId = deliveryId + "_extra_params";

		var wrapDiv = BX(wrapDivId);
		window.BX.SaleDeliveryExtraParams = {};

		if(wrapDiv)
			wrapDiv.parentNode.removeChild(wrapDiv);

		wrapDiv = BX.create('div', {props: { id: wrapDivId}});

		for(var i = paramsForm.elements.length-1; i >= 0; i--)
		{
			var input = BX.create('input', {
				props: {
					type: 'hidden',
					name: 'DELIVERY_EXTRA['+deliveryId+']['+paramsForm.elements[i].name+']',
					value: paramsForm.elements[i].value
					}
				}
			);

			window.BX.SaleDeliveryExtraParams[paramsForm.elements[i].name] = paramsForm.elements[i].value;

			wrapDiv.appendChild(input);
		}

		orderForm.appendChild(wrapDiv);

		BX.onCustomEvent('onSaleDeliveryGetExtraParams',[window.BX.SaleDeliveryExtraParams]);
	}

	BX.addCustomEvent('onDeliveryExtraServiceValueChange', function(){ submitForm(); });

</script>

<input type="hidden" name="BUYER_STORE" id="BUYER_STORE" value="<?=$arResult["BUYER_STORE"]?>" />
<input type="hidden" name="" id="CUR_DELIVERY_ID" value="" />

<? if(!empty($arResult["DELIVERY"])): ?>
	<div class="order-item">
	
		<!-- order-controls delivery -->
		<div class="order-controls">
		
			<!-- order-switch -->
			<div class="order-switch" id="switchDost">
				<div class="w c">
					<a href="#" class="active" id="samo"><span>Я заберу самостоятельно</span></a>
					<a href="#" id="dost"><span>Я закажу доставку</span></a>
				</div>
			</div>
			<!-- / order-switch end -->
			
			<!-- tooltip -->
			<a href="#" class="que ttSwitch2">?</a>
			<div id="ttSwitch2" style="display:none">
				<div class="tt tt-switch tt-c">
					<?$APPLICATION->IncludeFile("include/ttSwitch2.php", Array(), Array("MODE"=>"html", "SHOW_BORDER"=>false));?>
				</div>
			</div>
			<!-- / tooltip end -->
			<script>
				setTimeout(function() {
					if( (window.top.$ || $)('.ttSwitch2').length){
						(window.top.$ || $)('.ttSwitch2').powerTip({ placement: 'nw', mouseOnToPopup: true });
						(window.top.$ || $)('.ttSwitch2').data('powertiptarget', 'ttSwitch2');
					}
				}, window.top.$ ? 50 : 0);
			</script>
			
		</div>
		<!-- / order-controls delivery end -->
		
		<!-- dostTbl -->
		<div id="dostTbl" style="display:none">

			<h2 class="ttl-o">Условия доставки товара</h2>

			<table class="tbl-order tbl-del-city">
			
				<tr>
					<td class="td1">
						<p class="ttl">Город</p>
					</td>
					<td class="td2">
					
						<!-- city-sel-holder -->
						<div class="city-sel-holder">
						
							<a class="city-sel city-pop city_name_here" href="#select_city"><?= $arGeoData['CUR_CITY']['NAME'] ?></a>
							<div class="hint">Выберите ваш город. Для разных городов условия доставки могут отличаться!</div>
							<?/*<div class="vfield">Выберите ваш город. Для разных городов условия доставки могут отличаться!</div>*/?>
							
						</div>
						<!-- / city-sel-holder end -->
					
					</td>
				</tr>
			</table>

			<table class="tbl tbl-del-options">
				<tr>
					<th colspan="2">Варианты доставки</th>
					<th>Дата доставки</th>
					<th>Стоимость доставки</th>
				</tr>
				<tr class="sp">
					<td colspan="4">&nbsp;</td>
				</tr>
				<tr>
					<td class="td1"><input name="delRadio" class="check-custom" type="radio" value="rad1" id="rad1" checked></td>
					<td class="td2">
						<p>Доставка Русклимат</p>
					
						<div class="del-method-hidden">
							<p class="hint">Выберите день доставки:</p>
						
							<select class="del-method-select ik-30-lt" name="del-method-select">
								<option value="1">17 апреля, завтра</option>
								<option value="2">7 апреля, завтра</option>
								<option value="3">6 апреля, завтра</option>
							</select>
						</div>
					
					</td>
					<td>2 мая</td>
					<td>бесплатно</td>
				</tr>
				<tr class="sp">
					<td colspan="4">&nbsp;</td>
				</tr>
				<tr>
					<td class="td1"><input name="delRadio" class="check-custom" type="radio" value="rad2" id="rad2"></td>
					<td class="td2">
						<p>Срочная доставка DPD В течении дня с 10:00 до 22:00</p>
						
						<div class="del-method-hidden">
							<p class="hint">Выберите день доставки:</p>
							<select class="del-method-select ik-30-lt" name="del-method-select">
								<option value="1">17 апреля, завтра</option>
								<option value="2">7 апреля, завтра</option>
								<option value="3">6 апреля, завтра</option>
							</select>
						</div>
						
					</td>
					<td>завтра, 17 апреля</td>
					<td>100500 руб.</td>
				</tr>
				<tr class="sp">
					<td colspan="4">&nbsp;</td>
				</tr>
				<tr>
					<td class="td1"><input name="delRadio" class="check-custom" type="radio" value="rad3" id="rad3"></td>
					<td class="td2">
						<p>Срочная доставка DPD В течении дня с 10:00 до 22:00</p>
					
						<div class="del-method-hidden">
							<p class="hint">Выберите день доставки:</p>
							<select class="del-method-select ik-30-lt" name="del-method-select">
								<option value="1">17 апреля, завтра</option>
								<option value="2">7 апреля, завтра</option>
								<option value="3">6 апреля, завтра</option>
								<option value="6">1 апреля, завтра</option>
							</select>
						</div>
					</td>
					<td>завтра, 17 апреля</td>
					<td>100500 руб.</td>
				</tr>
			</table>
			
		</div>
		<!-- / dostTbl -->
		
		<!-- samTbl -->
		<div id="samTbl">
			<h2 class="ttl-o">Условия самовывоза товара</h2>			

			<table class="tbl-order tbl-del-city">
			
				<tr>
					<td class="td1">
						<p class="ttl">Город</p>
					</td>
					<td class="td2">
					
						<!-- city-sel-holder -->
						<div class="city-sel-holder">
						
							<a class="city-sel city-pop city_name_here" href="#select_city"><?= $arGeoData['CUR_CITY']['NAME'] ?></a>
							<div class="hint">Выберите ваш город. Для разных городов условия доставки могут отличаться!</div>
							<?/*<div class="vfield">Выберите ваш город. Для разных городов условия доставки могут отличаться!</div>*/?>
							
						</div>
						<!-- / city-sel-holder end -->
					
					</td>
				</tr>
			</table>

	
	
	
			<table class="tbl tbl-del-options">
				<tr>
					<th colspan="2">Варианты самовывоза</th>
					<th>Дата самовывоза</th>
					<th>Стоимость самовывоза</th>
				</tr>
				<tr class="sp">
					<td colspan="4">&nbsp;</td>
				</tr>
				<tr>
					<td class="td1"><input name="delRadio" class="check-custom" type="radio" value="rad1" id="rad1" checked></td>
					<td class="td2">
						<p>Магазин Русклимат, <span class="sm-map-pop">его адрес и т.д.</span></p>
						<div class="relat">
							<div class="sm-map">
								<!-- gmap -->
								<div class="map">
									<div class="map-x">
									</div>
									<!-- gmap holder (gmap.js) -->
									<div id="map" class="map-container map-inner"></div>
									<!-- / gmap holder end -->
								</div>
								<!-- / gmap -->
							</div>
						</div>
					
						<div class="del-method-hidden" style="display:block">
							<p class="hint">Выберите день доставки:</p>
						
							<select class="del-method-select ik-30-lt" name="del-method-select">
								<option value="1">17 апреля, завтра</option>
								<option value="2">7 апреля, завтра</option>
								<option value="3">6 апреля, завтра</option>
							</select>
						</div>
					
					</td>
					<td>2 мая</td>
					<td>бесплатно</td>
				</tr>
				<tr class="sp">
					<td colspan="4">&nbsp;</td>
				</tr>
				<tr>
					<td class="td1"><input name="delRadio" class="check-custom" type="radio" value="rad2" id="rad2"></td>
					<td class="td2">
						<p>Магазин Русклимат, его адрес и т.д.</p>
						
						<div class="del-method-hidden">
							<p class="hint">Выберите день доставки:</p>
							<select class="del-method-select ik-30-lt" name="del-method-select">
								<option value="1">17 апреля, завтра</option>
								<option value="2">7 апреля, завтра</option>
								<option value="3">6 апреля, завтра</option>
							</select>
						</div>
						
					</td>
					<td>завтра, 17 апреля</td>
					<td>100500 руб.</td>
				</tr>
				<tr class="sp">
					<td colspan="4">&nbsp;</td>
				</tr>
				<tr>
					<td class="td1"><input name="delRadio" class="check-custom" type="radio" value="rad3" id="rad3"></td>
					<td class="td2">
						<p>Магазин Русклимат, его адрес и т.д.</p>
					
						<div class="del-method-hidden">
							<p class="hint">Выберите день доставки:</p>
							<select class="del-method-select ik-30-lt" name="del-method-select">
								<option value="1">17 апреля, завтра</option>
								<option value="2">7 апреля, завтра</option>
								<option value="3">6 апреля, завтра</option>
								<option value="6">1 апреля, завтра</option>
							</select>
						</div>
					</td>
					<td>завтра, 17 апреля</td>
					<td>100500 руб.</td>
				</tr>
			</table>
			
		</div>
		<!-- / samTbl -->
		
		<!-- samTbl -->
		<div id="samTbl123" style="display: none;">
			<h2 class="ttl-o">Выбор способа получения товара</h2>
			
			<table class="tbl tbl-del-options">
				<tr>
					<th colspan="2">Вариант получения</th>
					<th>Дата</th>
					<th>Стоимость</th>
				</tr>
				<?
				CModule::IncludeModule('sale');
				CModule::IncludeModule('catalog');
				foreach ($arResult["DELIVERY"] as $delivery_id => $arDelivery)
				{
					/* 
					if($arDelivery["ISNEEDEXTRAINFO"] == "Y")
						$extraParams = "showExtraParamsDialog('".$delivery_id."');";
					else
						$extraParams = "";

					if (count($arDelivery["STORE"]) > 0)
						$clickHandler = "onClick = \"fShowStore('".$arDelivery["ID"]."','".$arParams["SHOW_STORES_IMAGES"]."','700','".SITE_ID."')\";";
					else
						$clickHandler = "onClick = \"BX('ID_DELIVERY_ID_".$arDelivery["ID"]."').checked=true;".$extraParams."submitForm();\"";
					 */
					
					// delivery
					/* 
					if (count($arDelivery["STORE"]) > 0) {
						$arStore = array();
						$arStoreId = array();
						$dbDelivery = CSaleDelivery::GetList(
							array("SORT"=>"ASC"),
							array("ID" => $arDelivery["ID"]),
							false,
							false,
							array("ID", "STORE")
						);
						$arDeliveryCur = $dbDelivery->Fetch();
						
						// p($arDeliveryCur);

						if (count($arDeliveryCur) > 0 && strlen($arDeliveryCur["STORE"]) > 0)
						{
							$arStoreInfo = unserialize($arDeliveryCur["STORE"]);
							foreach ($arStoreInfo as $val)
								$arStoreId[$val] = $val;
						}

						$arStoreLocation = array("yandex_scale" => 11, "PLACEMARKS" => array());

						$dbList = CCatalogStore::GetList(
							array("SORT" => "ASC", "ID" => "DESC"),
							array("ACTIVE" => "Y", "ID" => $arStoreId, "ISSUING_CENTER" => "Y", "+SITE_ID" => SITE_ID),
							false,
							false,
							array("ID", "SORT", "TITLE", "ADDRESS", "DESCRIPTION", "IMAGE_ID", "PHONE", "SCHEDULE", "GPS_N", "GPS_S", "SITE_ID", "ISSUING_CENTER", "EMAIL")
						);
						while ($arStoreTmp = $dbList->Fetch())
						{
							$arStore[$arStoreTmp["ID"]] = $arStoreTmp;

							if (intval($arStoreTmp["IMAGE_ID"]) > 0)
							{
								$arImage = CFile::GetFileArray($arStoreTmp["IMAGE_ID"]);
								$imgValue = CFile::ShowImage($arImage, 115, 115, "border=0", "", false);
								$arStore[$arStoreTmp["ID"]]["IMAGE"] = $imgValue;
								$arStore[$arStoreTmp["ID"]]["IMAGE_URL"] = $arImage["SRC"];
							}

							if (floatval($arStoreLocation["yandex_lat"]) <= 0)
								$arStoreLocation["yandex_lat"] = $arStoreTmp["GPS_N"];

							if (floatval($arStoreLocation["yandex_lon"]) <= 0)
								$arStoreLocation["yandex_lon"] = $arStoreTmp["GPS_S"];

							$arLocationTmp = array();
							$arLocationTmp["ID"] = $arStoreTmp["ID"];
							if (strlen($arStoreTmp["GPS_N"]) > 0)
								$arLocationTmp["LAT"] = $arStoreTmp["GPS_N"];
							if (strlen($arStoreTmp["GPS_S"]) > 0)
								$arLocationTmp["LON"] = $arStoreTmp["GPS_S"];
							if (strlen($arStoreTmp["TITLE"]) > 0)
								$arLocationTmp["TEXT"] = $arStoreTmp["TITLE"]."\r\n".$arStoreTmp["DESCRIPTION"];

							$arStoreLocation["PLACEMARKS"][] = $arLocationTmp;
						}
					
						// p($arStore);
					}
					 */
					?>
					<? /* if (count($arDelivery["STORE"]) > 0): ?>
						<?
						$first = 0;
						?>
						<? foreach ($arStore as $arCurStore): ?>
							<tr class="sp">
								<td colspan="3">&nbsp;</td>
							</tr>
							<tr>
								<td class="td1">
									<input type="radio"
										id="ID_DELIVERY_ID_<?= $arDelivery["ID"] ?>_<?= $arCurStore["ID"] ?>"
										name="<?=htmlspecialcharsbx($arDelivery["FIELD_NAME"])?>"
										value="<?= $arDelivery["ID"] ?>_<?= $arCurStore["ID"] ?>" -data-delivery="<?= $arDelivery["ID"] ?>" -data-store="<?= $arCurStore["ID"] ?>" <?if ($arDelivery["CHECKED"]=="Y" && ($arResult["BUYER_STORE"]==$arCurStore["ID"] || ($first++ == 0 && $arResult["BUYER_STORE"] == 0))) echo " checked";?>
										class="check-custom check-store"
										/>
								</td>
								<td class="td2">
									<p><label for="ID_DELIVERY_ID_<?=$arDelivery["ID"]?>_<?= $arCurStore["ID"] ?>"><b><?= htmlspecialcharsbx($arCurStore["TITLE"])?></b> <br><?= htmlspecialcharsbx($arCurStore["ADDRESS"])?></label></p>
								</td>
								<td>
								<? if($arDelivery["PRICE"] == 0): ?>
									бесплатно
								<? else: ?>
									<?=(strlen($arDelivery["PRICE_FORMATED"]) > 0 ? $arDelivery["PRICE_FORMATED"] : number_format($arDelivery["PRICE"], 2, ',', ' '))?>
								<? endif; ?>
								</td>
							</tr>
						<? endforeach; ?>
					<? else: */ ?>
					<?
					
					$MAIN_CITY = $arResult['GEO']['PROPERTIES']['network_name']['VALUE'];
					$MAIN_CITY = str_replace('Русклимат ', '', $MAIN_CITY);
					?>
					<? if($arDelivery["ID"] == 9): ?>
						<? foreach($arResult['LOCATIONS'] as $i=>$arLocation): ?>
							<tr class="sp">
								<td colspan="3">&nbsp;</td>
							</tr>
							<tr>
								<td class="td1">
									<?
									p($arDelivery);
									$checked = false;
									if (!empty($_REQUEST[$arDelivery["FIELD_NAME"]])) {
										if ($_REQUEST[$arDelivery["FIELD_NAME"]] == $arDelivery["ID"].'_'.$arLocation["ID"]) {
											$checked = true;
										}
									} elseif($i == 0) {
										$checked = true;
									}
									/*---*/
									?>
									<input type="radio"
										id="ID_DELIVERY_ID_<?= $arDelivery["ID"] ?>_<?= $arLocation["ID"] ?>"
										name="<?=htmlspecialcharsbx($arDelivery["FIELD_NAME"])?>"
										value="<?= $arDelivery["ID"] ?>_<?= $arLocation["ID"] ?>"<?if ($checked) echo " checked";?>
										class="check-custom delivery_checks"
										data-delivery="<?=htmlspecialcharsbx($arDelivery["NAME"])?>"
										data-selfdelivery="<?= $arLocation['NAME'] ?> (<?= $MAIN_CITY ?>, <?= $arLocation['PROPERTIES']['address']['VALUE'] ?>)"
										data-delivery_price="<?= !empty($arDelivery["PRICE_FORMATED"]) ? $arDelivery["PRICE_FORMATED"] : 0 ?>"
										data-delivery_desc="<?= $arDelivery["DESCRIPTION"] ?>"
										/>
								</td>
								<td class="td2">
									<p><label for="ID_DELIVERY_ID_<?=$arDelivery["ID"]?>_<?= $arLocation["ID"] ?>"><b><?= $arLocation['NAME'] ?> </b><br><?= $MAIN_CITY ?>, <?= $arLocation['PROPERTIES']['address']['VALUE'] ?></label></p>
								</td>
								<td>
									<? if(!empty($arDelivery["PERIOD_TEXT"])): ?>
										<?= $arDelivery["PERIOD_TEXT"] ?>
									<? else: ?>
										&nbsp;
									<? endif; ?>
								</td>
								<td>
									<? if($arDelivery["PRICE"] == 0): ?>
										бесплатно
									<? else: ?>
										<?=(strlen($arDelivery["PRICE_FORMATED"]) > 0 ? $arDelivery["PRICE_FORMATED"] : '&nbsp;')?>
									<? endif; ?>
								</td>
							</tr>
						<? endforeach; ?>
					<? else: ?>
						<tr class="sp">
							<td colspan="3">&nbsp;</td>
						</tr>
						<tr>
							<td class="td1">
								<input type="radio"
									id="ID_DELIVERY_ID_<?= $arDelivery["ID"] ?>"
									name="<?=htmlspecialcharsbx($arDelivery["FIELD_NAME"])?>"
									value="<?= $arDelivery["ID"] ?>"<?if ($arDelivery["CHECKED"]=="Y") echo " checked";?>
									class="check-custom delivery_checks"
									data-delivery="<?=htmlspecialcharsbx($arDelivery["NAME"])?>"
									data-selfdelivery=""
									data-delivery_price="<?= !empty($arDelivery["PRICE_FORMATED"]) ? $arDelivery["PRICE_FORMATED"] : 0 ?>"
									/>
							</td>
							<td class="td2">
								<p><label for="ID_DELIVERY_ID_<?=$arDelivery["ID"]?>"><?= htmlspecialcharsbx($arDelivery["NAME"])?></label></p>
							</td>
							<td>
								<? if(!empty($arDelivery["PERIOD_TEXT"])): ?>
									<?= $arDelivery["PERIOD_TEXT"] ?>
								<? else: ?>
									&nbsp;
								<? endif; ?>
							</td>
							<td>
								<? if($arDelivery["PRICE"] == 0): ?>
									бесплатно
								<? else: ?>
									<?=(strlen($arDelivery["PRICE_FORMATED"]) > 0 ? $arDelivery["PRICE_FORMATED"] : '&nbsp;')?>
								<? endif; ?>
							</td>
						</tr>
					<? endif; ?>
					<?
				}
				?>
			</table>
		</div>
		<!-- / samTbl -->

	</div>
	<!-- / order-item end -->
	
	<div class="order-item" id="basketDelAdr" style="display:none">
		<h2 class="ttl-o">Адрес доставки</h2>
		
		<table class="tbl-order tbl-del-adr tbl-order-v">
			
			<tr>
				<td class="td1"><p class="ttl">Улица</p></td>
				<td class="td2" colspan="3">
					<input class="inp inp-drk inp-t inp-adr" type="text"/>
					<div class="hint">Пояснение к полю</div>
					<div class="vfield">Пояснение ошибки валидации поля</div>
				</td>
			</tr>
			<tr>
				<td class="td1"><p class="ttl">Дом</p></td>
				<td class="td2">
					<input class="inp inp-drk inp-t" type="text"/>
					<div class="hint">Пояснение к полю</div>
					<div class="vfield">Пояснение ошибки валидации поля</div>
				</td>
				<td class="td1"><p class="ttl">Корпус</p></td>
				<td class="td2">
					<input class="inp inp-drk inp-t" type="text"/>
					<div class="hint">Пояснение к полю</div>
					<div class="vfield">Пояснение ошибки валидации поля</div>
				</td>
			</tr>
			<tr>
				<td class="td1"><p class="ttl">Строение</p></td>
				<td class="td2">
					<input class="inp inp-drk inp-t" type="text"/>
					<div class="hint">Пояснение к полю</div>
					<div class="vfield">Пояснение ошибки валидации поля</div>
				</td>
				<td class="td1"><p class="ttl">Квартира / офис</p></td>
				<td class="td2">
					<input class="inp inp-drk inp-t" type="text"/>
					<div class="hint">Пояснение к полю</div>
					<div class="vfield">Пояснение ошибки валидации поля</div>
				</td>
			</tr>
		
		</table>
	</div>
	<!-- / order-item end -->
<? endif; ?>
