<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

if($USER->IsAuthorized() || $arParams["ALLOW_AUTO_REGISTER"] == "Y")
{
	if($arResult["USER_VALS"]["CONFIRM_ORDER"] == "Y" || $arResult["NEED_REDIRECT"] == "Y")
	{
		if(strlen($arResult["REDIRECT_URL"]) > 0)
		{
			$APPLICATION->RestartBuffer();
			?>
			<script type="text/javascript">
				window.top.location.href='<?=CUtil::JSEscape($arResult["REDIRECT_URL"])?>';
			</script>
			<?
			die();
		}

	}
}

$GLOBALS['FIELDS_CODES'] = [];

// $APPLICATION->SetAdditionalCSS($templateFolder."/style_cart.css");
// $APPLICATION->SetAdditionalCSS($templateFolder."/style.css");

// p($arResult);
// p($arResult['BASKET_ITEMS']);


/*
$IS_BIG_SIZE = false;
foreach($arResult['BASKET_ITEMS'] as $i => $arItem) {
	if (!empty($arItem['PROPERTY_IS_BIG_SIZE_VALUE'])) {
		$IS_BIG_SIZE = true;
		break;
	}
}
$ITEM_PRICE = $arResult['ORDER_PRICE'];

$DELIVERY_PRICE = false;
$DELIVERY_TIME = false;
if (!$IS_BIG_SIZE) {
	if ($ITEM_PRICE >= 1000 && $ITEM_PRICE <= 2999) {
		$DELIVERY_PRICE = $arResult['GEO']['PROPERTIES']['tarif_1']['VALUE'];
	} elseif ($ITEM_PRICE >= 3000 && $ITEM_PRICE <= 5999) {
		$DELIVERY_PRICE = $arResult['GEO']['PROPERTIES']['tarif_2']['VALUE'];
	} elseif ($ITEM_PRICE >= 6000 && $ITEM_PRICE <= 8999) {
		$DELIVERY_PRICE = $arResult['GEO']['PROPERTIES']['tarif_3']['VALUE'];
	} elseif ($ITEM_PRICE >= 9000) {
		$DELIVERY_PRICE = $arResult['GEO']['PROPERTIES']['tarif_4']['VALUE'];
	}
} else {
	if ($ITEM_PRICE >= 1000 && $ITEM_PRICE <= 2999) {
		$DELIVERY_PRICE = $arResult['GEO']['PROPERTIES']['tarif_5']['VALUE'];
	} elseif ($ITEM_PRICE >= 3000 && $ITEM_PRICE <= 5999) {
		$DELIVERY_PRICE = $arResult['GEO']['PROPERTIES']['tarif_6']['VALUE'];
	} elseif ($ITEM_PRICE >= 6000 && $ITEM_PRICE <= 8999) {
		$DELIVERY_PRICE = $arResult['GEO']['PROPERTIES']['tarif_7']['VALUE'];
	} elseif ($ITEM_PRICE >= 9000) {
		$DELIVERY_PRICE = $arResult['GEO']['PROPERTIES']['tarif_8']['VALUE'];
	}
}
if ($DELIVERY_PRICE == 'нет') {
	$DELIVERY_PRICE = false;
}
*/

$IS_BIG_SIZE = false;
$DELIVERY_PRICE = 0;
foreach($arResult['BASKET_ITEMS'] as $i => $arItem) {
//p($arItem['PRICE']);
//p($arItem['PROPERTY_IS_BIG_SIZE_VALUE']);
//p($arResult['GEO']['PROPERTIES']);
	if (!empty($arItem['PROPERTY_IS_BIG_SIZE_VALUE'])) {
		$IS_BIG_SIZE = true;
	}
	$ITEM_PRICE = $arItem['PRICE'];
	if (!$IS_BIG_SIZE) {
		if ($ITEM_PRICE >= 1000 && $ITEM_PRICE <= 2999) {
			if($arResult['GEO']['PROPERTIES']['tarif_1']['VALUE']>$DELIVERY_PRICE)
				$DELIVERY_PRICE = $arResult['GEO']['PROPERTIES']['tarif_1']['VALUE'];
		} elseif ($ITEM_PRICE >= 3000 && $ITEM_PRICE <= 5999) {
			if($arResult['GEO']['PROPERTIES']['tarif_2']['VALUE']>$DELIVERY_PRICE)
				$DELIVERY_PRICE = $arResult['GEO']['PROPERTIES']['tarif_2']['VALUE'];
		} elseif ($ITEM_PRICE >= 6000 && $ITEM_PRICE <= 8999) {
			if($arResult['GEO']['PROPERTIES']['tarif_3']['VALUE']>$DELIVERY_PRICE)
				$DELIVERY_PRICE = $arResult['GEO']['PROPERTIES']['tarif_3']['VALUE'];
		} elseif ($ITEM_PRICE >= 9000) {
			if($arResult['GEO']['PROPERTIES']['tarif_4']['VALUE']>$DELIVERY_PRICE)
				$DELIVERY_PRICE = $arResult['GEO']['PROPERTIES']['tarif_4']['VALUE'];
		}
	} else {
		if ($ITEM_PRICE >= 1000 && $ITEM_PRICE <= 2999) {
			if($arResult['GEO']['PROPERTIES']['tarif_5']['VALUE']>$DELIVERY_PRICE)
				$DELIVERY_PRICE = $arResult['GEO']['PROPERTIES']['tarif_5']['VALUE'];
		} elseif ($ITEM_PRICE >= 3000 && $ITEM_PRICE <= 5999) {
			if($arResult['GEO']['PROPERTIES']['tarif_6']['VALUE']>$DELIVERY_PRICE)
				$DELIVERY_PRICE = $arResult['GEO']['PROPERTIES']['tarif_6']['VALUE'];
		} elseif ($ITEM_PRICE >= 6000 && $ITEM_PRICE <= 8999) {
			if($arResult['GEO']['PROPERTIES']['tarif_7']['VALUE']>$DELIVERY_PRICE)
				$DELIVERY_PRICE = $arResult['GEO']['PROPERTIES']['tarif_7']['VALUE'];
		} elseif ($ITEM_PRICE >= 9000) {
			if($arResult['GEO']['PROPERTIES']['tarif_8']['VALUE']>$DELIVERY_PRICE)
				$DELIVERY_PRICE = $arResult['GEO']['PROPERTIES']['tarif_8']['VALUE'];
		}
	}
	if ($DELIVERY_PRICE == 'нет') {
		$DELIVERY_PRICE = 0;
	}	
}

$DELIVERY_TIME = false;

if ($DELIVERY_PRICE == 0) {
	$DELIVERY_PRICE = false;
}


$DELIVERY_TIME_DAYS = 0;
$DELIVERY_TIME_DAYS_REAL = 0;
$DELIVERY_TIME_VAL = false;
$DELIVERY_TIME_VALS = [];
$DELIVERY2_TIME_DAYS = 0;
$DELIVERY2_TIME_DAYS_REAL = 0;
$DELIVERY2_TIME_VAL = false;
$DELIVERY2_TIME_VALS = [];
foreach($arResult['BASKET_ITEMS'] as $i => $arItem) {
	
	$DELIVERY_TIME_DAYS_CUR = 0;
	$DELIVERY2_TIME_DAYS_CUR = 0;
	
	if (!empty($arItem['PROPERTY_'.$arResult['QUANITY_PROP'].'_VALUE'])) {
		$DELIVERY_TIME_DAYS_CUR = $arResult['GEO']['PROPERTIES']['shipping_1']['VALUE'];
		$DELIVERY2_TIME_DAYS_CUR = $arResult['GEO']['PROPERTIES']['samov_1']['VALUE'];
	} elseif (!empty($arItem['PROPERTY_'.$arResult['QUANITY_PROP2'].'_VALUE'])) {
		$DELIVERY_TIME_DAYS_CUR = $arResult['GEO']['PROPERTIES']['shipping_2']['VALUE'];
		$DELIVERY2_TIME_DAYS_CUR = $arResult['GEO']['PROPERTIES']['samov_2']['VALUE'];
	}
	if (!empty($DELIVERY_TIME_DAYS_CUR)) {
		if ($DELIVERY_TIME_DAYS_CUR == 'сегодня раб/врем') {
			$DELIVERY_TIME_DAYS_CUR = 0;
		} elseif ($DELIVERY_TIME_DAYS_CUR == 'к осн фил') {
			$DELIVERY_TIME_DAYS_CUR = $arResult['GEO']['PROPERTIES']['shipping_2']['VALUE'];
		} elseif ($DELIVERY_TIME_DAYS_CUR == 'нет') {
			$DELIVERY_TIME_DAYS_CUR = false;
		}
		
	}
	if (!empty($DELIVERY2_TIME_DAYS_CUR)) {
		if ($DELIVERY2_TIME_DAYS_CUR == 'сегодня раб/врем') {
			$DELIVERY2_TIME_DAYS_CUR = 0;
		} elseif ($DELIVERY2_TIME_DAYS_CUR == 'к осн фил') {
			$DELIVERY2_TIME_DAYS_CUR = $arResult['GEO']['PROPERTIES']['shipping_2']['VALUE'];
		} elseif ($DELIVERY2_TIME_DAYS_CUR == 'нет') {
			$DELIVERY2_TIME_DAYS_CUR = false;
		}
	}
	if ($DELIVERY_TIME_DAYS === false || $DELIVERY2_TIME_DAYS === false) {
		if ($DELIVERY_TIME_DAYS === false) {
			$DELIVERY_TIME_VAL = '';
		}
		if ($DELIVERY_TIME_DAYS === false) {
			$DELIVERY2_TIME_VAL = '';
		}
		break;
	} else {
		if ($DELIVERY_TIME_DAYS == 0) {
			$DELIVERY_TIME_DAYS = $DELIVERY_TIME_DAYS_CUR;
		}
		$DELIVERY_TIME_DAYS = max($DELIVERY_TIME_DAYS, $DELIVERY_TIME_DAYS_CUR);
		if ($DELIVERY2_TIME_DAYS == 0) {
			$DELIVERY2_TIME_DAYS = $DELIVERY2_TIME_DAYS_CUR;
		}
		$DELIVERY2_TIME_DAYS = max($DELIVERY2_TIME_DAYS, $DELIVERY2_TIME_DAYS_CUR);
	}
}
// p($DELIVERY_TIME_DAYS);
// p($DELIVERY2_TIME_DAYS);

$DORPDOWN_DAYS_SELECT_ITEMS = 3;
if ($DELIVERY_TIME_DAYS !== false) {
	$one_day = 86400;
	$cur_time = time();
	$today = $cur_time;
	$days_left = $DELIVERY_TIME_DAYS;
	
	while ($days_left > 0) {
		$today += $one_day;
		$dow = intval(date('w', $today));
		if ($dow != 0 && $dow != 6) {
			$days_left -= 1;
		}
		$DELIVERY_TIME_DAYS_REAL += 1;
	}
	$days_cntr = 0;
	$DELIVERY_TIME_DAYS_REAL_tmp = $DELIVERY_TIME_DAYS_REAL;
	while (count($DELIVERY_TIME_VALS) < $DORPDOWN_DAYS_SELECT_ITEMS) {
		$today_tmp = $today + $one_day * $days_cntr;
		$dow = intval(date('w', $today_tmp));
		if ($dow != 0 && $dow != 6) {
			$DELIVERY_TIME_VAL = date('j '.GetMonthForDelivery($today_tmp), $today_tmp);
			if ($DELIVERY_TIME_DAYS_REAL_tmp == 0) {
				$DELIVERY_TIME_VAL = 'Сегодня, '.$DELIVERY_TIME_VAL;
			} elseif ($DELIVERY_TIME_DAYS_REAL_tmp == 1) {
				$DELIVERY_TIME_VAL = 'Завтра, '.$DELIVERY_TIME_VAL;
			}
			$DELIVERY_TIME_VALS[] = $DELIVERY_TIME_VAL;
		}
		$days_cntr++;
		$DELIVERY_TIME_DAYS_REAL_tmp++;
	}
	$DELIVERY_TIME_VAL = $DELIVERY_TIME_VALS[0];
	unset($DELIVERY_TIME_DAYS_REAL_tmp);
}
if ($DELIVERY2_TIME_DAYS !== false) {
	$one_day = 86400;
	$cur_time = time();
	$today = $cur_time;
	$days_left = $DELIVERY2_TIME_DAYS;
	
	while ($days_left > 0) {
		$today += $one_day;
		$dow = intval(date('w', $today));
		if ($dow != 0 && $dow != 6) {
			$days_left -= 1;
		}
		$DELIVERY2_TIME_DAYS_REAL += 1;
	}
	$days_cntr = 0;
	$DELIVERY2_TIME_DAYS_REAL_tmp = $DELIVERY2_TIME_DAYS_REAL;
	while (count($DELIVERY2_TIME_VALS) < $DORPDOWN_DAYS_SELECT_ITEMS) {
		$today_tmp = $today + $one_day * $days_cntr;
		$dow = intval(date('w', $today_tmp));
		if ($dow != 0 && $dow != 6) {
			$DELIVERY2_TIME_VAL = date('j '.GetMonthForDelivery($today_tmp), $today_tmp);
			if ($DELIVERY2_TIME_DAYS_REAL_tmp == 0) {
				$DELIVERY2_TIME_VAL = 'Сегодня, '.$DELIVERY2_TIME_VAL;
			} elseif ($DELIVERY2_TIME_DAYS_REAL_tmp == 1) {
				$DELIVERY2_TIME_VAL = 'Завтра, '.$DELIVERY2_TIME_VAL;
			}
			$DELIVERY2_TIME_VALS[] = $DELIVERY2_TIME_VAL;
		}
		$days_cntr++;
		$DELIVERY2_TIME_DAYS_REAL_tmp++;
	}
	$DELIVERY2_TIME_VAL = $DELIVERY2_TIME_VALS[0];
	unset($DELIVERY2_TIME_DAYS_REAL_tmp);
}

foreach ($arResult["DELIVERY"] as $delivery_id => $arDelivery) {
	if ($delivery_id == 9) {
		if(!empty($DELIVERY2_TIME_VAL)) {
			$arResult["DELIVERY"][$delivery_id]["PERIOD_TEXT"] = $DELIVERY2_TIME_VAL;
			$arResult["DELIVERY"][$delivery_id]["PERIOD_SELECTS"] = $DELIVERY2_TIME_VALS;
			$arResult["DELIVERY"][$delivery_id]["PRICE"] = 0;
			$arResult["DELIVERY"][$delivery_id]["PRICE_FORMATED"] = 0;
		} else {
			// unset($arResult["DELIVERY"][$delivery_id]);
		}
	} elseif ($delivery_id == 10) {	// доставка
		if($DELIVERY_PRICE !== false && !empty($DELIVERY_TIME_VAL)) {
			$arResult["DELIVERY"][$delivery_id]["PERIOD_TEXT"] = $DELIVERY_TIME_VAL;
			$arResult["DELIVERY"][$delivery_id]["PERIOD_SELECTS"] = $DELIVERY_TIME_VALS;
			$arResult["DELIVERY"][$delivery_id]["PRICE"] = $DELIVERY_PRICE;
			$arResult["DELIVERY"][$delivery_id]["PRICE_FORMATED"] = number_format($DELIVERY_PRICE, 0, ',', ' ').' руб.';
		} else {
			// unset($arResult["DELIVERY"][$delivery_id]);
		}
	}
	// p($arResult["DELIVERY"][$delivery_id]);
}

?>

<a name="order_form"></a>

<div id="order_form_div" class="order-checkout">
<NOSCRIPT>
	<div class="errortext"><?=GetMessage("SOA_NO_JS")?></div>
</NOSCRIPT>

<?
if (!function_exists("getColumnName"))
{
	function getColumnName($arHeader)
	{
		return (strlen($arHeader["name"]) > 0) ? $arHeader["name"] : GetMessage("SALE_".$arHeader["id"]);
	}
}

if (!function_exists("cmpBySort"))
{
	function cmpBySort($array1, $array2)
	{
		if (!isset($array1["SORT"]) || !isset($array2["SORT"]))
			return -1;

		if ($array1["SORT"] > $array2["SORT"])
			return 1;

		if ($array1["SORT"] < $array2["SORT"])
			return -1;

		if ($array1["SORT"] == $array2["SORT"])
			return 0;
	}
}
?>

<div class="bx_order_make">
	<?
	if(!$USER->IsAuthorized() && $arParams["ALLOW_AUTO_REGISTER"] == "N")
	{
		if(!empty($arResult["ERROR"]))
		{
			foreach($arResult["ERROR"] as $v)
				echo ShowError($v);
		}
		elseif(!empty($arResult["OK_MESSAGE"]))
		{
			foreach($arResult["OK_MESSAGE"] as $v)
				echo ShowNote($v);
		}

		include($_SERVER["DOCUMENT_ROOT"].$templateFolder."/auth.php");
	}
	else
	{
		if($arResult["USER_VALS"]["CONFIRM_ORDER"] == "Y" || $arResult["NEED_REDIRECT"] == "Y")
		{
			if(strlen($arResult["REDIRECT_URL"]) == 0)
			{
				include($_SERVER["DOCUMENT_ROOT"].$templateFolder."/confirm.php");
			}
		}
		else
		{
			?>
			<script type="text/javascript">

			<?if(CSaleLocation::isLocationProEnabled()):?>

				<?
				// spike: for children of cities we place this prompt
				$city = \Bitrix\Sale\Location\TypeTable::getList(array('filter' => array('=CODE' => 'CITY'), 'select' => array('ID')))->fetch();
				?>

				BX.saleOrderAjax.init(<?=CUtil::PhpToJSObject(array(
					'source' => $this->__component->getPath().'/get.php',
					'cityTypeId' => intval($city['ID']),
					'messages' => array(
						'otherLocation' => '--- '.GetMessage('SOA_OTHER_LOCATION'),
						'moreInfoLocation' => '--- '.GetMessage('SOA_NOT_SELECTED_ALT'), // spike: for children of cities we place this prompt
						'notFoundPrompt' => '<div class="-bx-popup-special-prompt">'.GetMessage('SOA_LOCATION_NOT_FOUND').'.<br />'.GetMessage('SOA_LOCATION_NOT_FOUND_PROMPT', array(
							'#ANCHOR#' => '<a href="javascript:void(0)" class="-bx-popup-set-mode-add-loc">',
							'#ANCHOR_END#' => '</a>'
						)).'</div>'
					)
				))?>);

			<?endif?>
	
			function customizePaySelect()
			{
				$('#payMethodSelect').ikSelect({
					autoWidth: false,
					ddFullWidth: false,
					customClass: 'pay-method-select'
				});
				$('.checkset input, .check-custom').iCheck({
					checkboxClass: 'icheckbox_minimal',
					radioClass: 'iradio_minimal',
					activeClass: 'active'
				});
				$('#samTbl, #dostTbl').on('ifChecked', 'input[type="radio"]', function() {
					$('#CUR_DELIVERY_ID').attr('name', '');
					$('#BUYER_STORE').val('');
					submitForm();
				});
				$('#CUR_DELIVERY_ID').attr('name', '');
				$('#BUYER_STORE').val('');
				
				if( $('.ttSwitch2').length){
					$('.ttSwitch2').powerTip({ placement: 'nw', mouseOnToPopup: true });
					$('.ttSwitch2').data('powertiptarget', 'ttSwitch2');
				}
				
				//delivery method selects
				$('.del-method-select').ikSelect({
					autoWidth: false,
					ddFullWidth: false,
					customClass: 'ik-green del-method-select'
				});
				$('.yur-select').ikSelect({
					autoWidth: false,
					ddFullWidth: false,
					customClass: 'yur-select',
					ddMaxHeight: 400
				}).trigger('change');
				
				if (typeof window.top.needUpdateOfDelivery !== 'undefined' && window.top.needUpdateOfDelivery && window.top.needUpdateOfDelivery == 1) {
					window.top.needUpdateOfDelivery++;
					setTimeout(function() {
						window.top.changePaySystem();
					}, 0);
				}
				
			}

			var BXFormPosting = false;
			function submitForm(val)
			{
				if (BXFormPosting === true)
					return true;

				BXFormPosting = true;
				if(val != 'Y')
					BX('confirmorder').value = 'N';

				var orderForm = BX('ORDER_FORM');
				BX.showWait();

				<?if(CSaleLocation::isLocationProEnabled()):?>
					BX.saleOrderAjax.cleanUp();
				<?endif?>

				BX.ajax.submit(orderForm, ajaxResult);

				return true;
			}

			function ajaxResult(res)
			{
				var orderForm = BX('ORDER_FORM');
				try
				{
					// if json came, it obviously a successfull order submit

					var json = JSON.parse(res);
					BX.closeWait();

					if (json.error)
					{
						BXFormPosting = false;
						return;
					}
					else if (json.redirect)
					{
						window.top.location.href = json.redirect;
					}
				}
				catch (e)
				{
					// json parse failed, so it is a simple chunk of html

					BXFormPosting = false;
					BX('order_form_content').innerHTML = res;

					<?if(CSaleLocation::isLocationProEnabled()):?>
						BX.saleOrderAjax.initDeferredControl();
					<?endif?>
				}

				BX.closeWait();
				BX.onCustomEvent(orderForm, 'onAjaxSuccess');
				
				customizePaySelect();
			}

			function SetContact(profileId)
			{
				BX("profile_change").value = "Y";
				submitForm();
			}
			</script>
			<?if($_POST["is_ajax_post"] != "Y")
			{
				?><form action="<?=$APPLICATION->GetCurPage();?>" method="POST" name="ORDER_FORM" id="ORDER_FORM" enctype="multipart/form-data">
				<?=bitrix_sessid_post()?>
				<div id="order_form_content">
				<?
			}
			else
			{
				$APPLICATION->RestartBuffer();
			}

			if($_REQUEST['PERMANENT_MODE_STEPS'] == 1)
			{
				?>
				<input type="hidden" name="PERMANENT_MODE_STEPS" value="1" />
				<?
			}

			$arResult["FIELDS_ERRORS"] = [];
			if(!empty($arResult["ERROR"]) && $arResult["USER_VALS"]["FINAL_STEP"] == "Y")
			{
				$show_errors = [];
				
				foreach($arResult["ERROR"] as $v) {
					$matches = [];
					preg_match('/Заполните поле \"([^\"]+)\"/ui', $v, $matches);
					if (!empty($matches) && !empty($matches[1])) {
						$arResult["FIELDS_ERRORS"][$matches[1]] = $matches[0];
					} else {
						// echo ShowError($v);
						$show_errors[] = ShowError($v);
					}
				}
				if (!empty($show_errors)) {
					// echo '<div style="padding-top: 50px;"></div>';
					foreach($show_errors as $v) {
						echo $v;
					}
					?>
					<script type="text/javascript">
						top.BX.scrollToNode(top.BX('ORDER_FORM'));
						top.$('html, body').scrollTop(top.$('body').scrollTop() - 60);
					</script>
					<?
				}
				
			}
			
			?>
			<div class="brd order-w" id="orderBlock">

				<!-- order -->
				<div class="order">
					<?
					
					include($_SERVER["DOCUMENT_ROOT"].$templateFolder."/person_type.php");		// Тип покупателя
					include($_SERVER["DOCUMENT_ROOT"].$templateFolder."/props.php");
					if ($arParams["DELIVERY_TO_PAYSYSTEM"] == "p2d")
					{
						include($_SERVER["DOCUMENT_ROOT"].$templateFolder."/paysystem.php");
						include($_SERVER["DOCUMENT_ROOT"].$templateFolder."/delivery.php");
					}
					else
					{
						include($_SERVER["DOCUMENT_ROOT"].$templateFolder."/delivery.php");
						include($_SERVER["DOCUMENT_ROOT"].$templateFolder."/paysystem.php");
					}

					include($_SERVER["DOCUMENT_ROOT"].$templateFolder."/related_props.php");	// Свойства, связанные с оплатой и доставкой

					include($_SERVER["DOCUMENT_ROOT"].$templateFolder."/summary.php");	// Товары в заказе и цена, комментарий
					if(strlen($arResult["PREPAY_ADIT_FIELDS"]) > 0)
						echo $arResult["PREPAY_ADIT_FIELDS"];
					?>
					
					<script>
						window.dataLayer = window.dataLayer || [];
						window.dataLayer.push({
							'event': 'checkout',
							'ecommerce': {
								'checkout': {
									'actionField': {'step': 2}
								}
							}
						});
					</script>
					
				</div>
				<!-- / order end -->
			</div>
			<!-- / brd order-w end -->
			
			<script>
				if (typeof customizePaySelect != 'undefined') {
					customizePaySelect();
				}
				<? if(!empty($arResult["FIELDS_ERRORS"])): ?>
					top.window.setTimeout(function() {
						top.$(function() {
							var first_err = top.$('.inp-inv:first');
							if (first_err.length) {
								var err_id = first_err.attr('id');
								if (!err_id) {
									err_id = 'first_error_here';
									first_err.attr('id', err_id);
								}
								top.BX.scrollToNode(top.BX(err_id));
								top.$('html, body').scrollTop(top.$('body').scrollTop() - 60);
							}
						});
					}, 0);
				<? endif; ?>
			</script>

			<?if($_POST["is_ajax_post"] != "Y")
			{
				?>
					</div>
					<input type="hidden" name="confirmorder" id="confirmorder" value="Y">
					<input type="hidden" name="profile_change" id="profile_change" value="N">
					<input type="hidden" name="is_ajax_post" id="is_ajax_post" value="Y">
					<input type="hidden" name="json" value="Y">
					<?/*<div class="bx_ordercart_order_pay_center"><a href="javascript:void();" onclick="submitForm('Y'); return false;" id="ORDER_CONFIRM_BUTTON" class="checkout"><?=GetMessage("SOA_TEMPL_BUTTON")?></a></div>*/?>
				</form>
				<?
				if($arParams["DELIVERY_NO_AJAX"] == "N")
				{
					?>
					<div style="display:none;"><?$APPLICATION->IncludeComponent("bitrix:sale.ajax.delivery.calculator", "", array(), null, array('HIDE_ICONS' => 'Y')); ?></div>
					<?
				}
			}
			else
			{
				?>
				<script type="text/javascript">
					top.BX('confirmorder').value = 'Y';
					top.BX('profile_change').value = 'N';
				</script>
				<?
				die();
			}
		}
	}
	?>
	</div>
</div>

<?if(CSaleLocation::isLocationProEnabled()):?>

	<div style="display: none">
		<?// we need to have all styles for sale.location.selector.steps, but RestartBuffer() cuts off document head with styles in it?>
		<?$APPLICATION->IncludeComponent(
			"bitrix:sale.location.selector.steps", 
			".default", 
			array(
			),
			false
		);?>
		<?$APPLICATION->IncludeComponent(
			"bitrix:sale.location.selector.search", 
			".default", 
			array(
			),
			false
		);?>
	</div>

<?endif?>