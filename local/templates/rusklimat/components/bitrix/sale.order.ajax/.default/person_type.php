<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<?
if(count($arResult["PERSON_TYPE"]) > 1)
{
	?>

	<!-- order-controls fizik/yurik -->
	<div class="order-controls">

		<!-- order-switch -->
		<div class="order-switch" id="switchYurFiz">
			<div class="w c">
				<?foreach($arResult["PERSON_TYPE"] as $v):?>
					<? if($v["NAME"] == 'Физическое лицо'): ?>
						<a href="#"<? if($v["CHECKED"]=="Y"):?> class="active"<? endif; ?> name="fizik" id="fizik"><label for="PERSON_TYPE_<?=$v["ID"]?>"><span>Покупаю как частное лицо</span></label></a>
					<? else: ?>
						<a href="#"<? if($v["CHECKED"]=="Y"):?> class="active"<? endif; ?> name="yurik" id="yurik"><label for="PERSON_TYPE_<?=$v["ID"]?>"><span>Покупаю от имени организации</span></label></a>
					<? endif; ?>
					<div class="hide_it"><input type="radio" id="PERSON_TYPE_<?=$v["ID"]?>" name="PERSON_TYPE" value="<?=$v["ID"]?>"<?if ($v["CHECKED"]=="Y") echo " checked=\"checked\"";?> onClick="submitForm()"></div>
				<?endforeach;?>
			</div>
		</div>
		<!-- / order-switch end -->

		<!-- tooltip -->
		<a href="#" class="que ttSwitch1">?</a>
		<div id="ttSwitch1" style="display:none">
			<div class="tt tt-switch tt-c">
				<?$APPLICATION->IncludeFile("include/ttSwitch1.php", Array(), Array("MODE"=>"html", "SHOW_BORDER"=>false));?>
			</div>
		</div>
		<!-- / tooltip end -->
		<script>
			setTimeout(function() {
				if( (window.top.$ || $)('.ttSwitch1').length){
					(window.top.$ || $)('.ttSwitch1').powerTip({ placement: 'nw', mouseOnToPopup: true });
					(window.top.$ || $)('.ttSwitch1').data('powertiptarget', 'ttSwitch1');
				}
			}, window.top.$ ? 50 : 0);
		</script>

	</div>
	<!-- / order-controls fizik/yurik end -->

	<input type="hidden" name="PERSON_TYPE_OLD" value="<?=$arResult["USER_VALS"]["PERSON_TYPE_ID"]?>" />
	<?
}
else
{
	if(IntVal($arResult["USER_VALS"]["PERSON_TYPE_ID"]) > 0)
	{
		//for IE 8, problems with input hidden after ajax
		?>
		<span style="display:none;">
			<input type="text" name="PERSON_TYPE" value="<?=IntVal($arResult["USER_VALS"]["PERSON_TYPE_ID"])?>" />
			<input type="text" name="PERSON_TYPE_OLD" value="<?=IntVal($arResult["USER_VALS"]["PERSON_TYPE_ID"])?>" />
		</span>
		<?
	}
	else
	{
		foreach($arResult["PERSON_TYPE"] as $v)
		{
			?>
			<input type="hidden" id="PERSON_TYPE" name="PERSON_TYPE" value="<?=$v["ID"]?>" />
			<input type="hidden" name="PERSON_TYPE_OLD" value="<?=$v["ID"]?>" />
			<?
		}
	}
}
?>
