<?
if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

$ClientID = 'navigation_'.$arResult['NavNum'];

if(!$arResult["NavShowAlways"])
{
	if ($arResult["NavRecordCount"] == 0 || ($arResult["NavPageCount"] == 1 && $arResult["NavShowAll"] == false))
		return;
}
?>
<?
/*
<div class="btn btn-more btn-g">Показать еще</div>

<div class="paginator">
	<a href="#" class="pages-lt">&lt;</a>
	<ul class="pages">
		<li class="active"><a href="#">1</a></li>
		<li><a href="#">2</a></li>
		<li><a href="#">3</a></li>
		<li class="page-more"><a href="#">...</a></li>
		<li><a href="#">14</a></li>
		<li><a href="#">15</a></li>
	</ul>
	<a href="#" class="pages-rt">&gt;</a>
</div>
*/
?>

<div class="paginator">
<?
$strNavQueryString = ($arResult["NavQueryString"] != "" ? $arResult["NavQueryString"]."&amp;" : "");
$strNavQueryStringFull = ($arResult["NavQueryString"] != "" ? "?".$arResult["NavQueryString"] : "");

// to show always first and last pages
$arResult["nStartPage"] = 1;
$arResult["nEndPage"] = $arResult["NavPageCount"];

$sPrevHref = '';
if ($arResult["NavPageNomer"] > 1)
{
	$bPrevDisabled = false;

	if ($arResult["bSavePage"] || $arResult["NavPageNomer"] > 2)
	{
		$sPrevHref = $arResult["sUrlPath"].'?'.$strNavQueryString.'PAGEN_'.$arResult["NavNum"].'='.($arResult["NavPageNomer"]-1);
	}
	else
	{
		$sPrevHref = $arResult["sUrlPath"].$strNavQueryStringFull;
	}
}
else
{
	$bPrevDisabled = true;
}

$sNextHref = '';
if ($arResult["NavPageNomer"] < $arResult["NavPageCount"])
{
	$bNextDisabled = false;
	$sNextHref = $arResult["sUrlPath"].'?'.$strNavQueryString.'PAGEN_'.$arResult["NavNum"].'='.($arResult["NavPageNomer"]+1);
}
else
{
	$bNextDisabled = true;
}

$nStartPage = $arResult["nStartPage"];
$points1href = [];
$points2href = [];
do
{
	if ($arResult["nStartPage"] <= 2 || $arResult["nEndPage"]-$arResult["nStartPage"] <= 1 || abs($arResult['nStartPage']-$arResult["NavPageNomer"])<=2)
	{

	}
	else
	{
		if ($arResult['nStartPage']-$arResult["NavPageNomer"]<=2) {
			$points1href[] = $arResult["nStartPage"];
		} else {
			$points2href[] = $arResult["nStartPage"];
		}
	}
	$arResult["nStartPage"]++;
} while($arResult["nStartPage"] <= $arResult["nEndPage"]);
$arResult["nStartPage"] = $nStartPage;

if (!empty($points1href)) {
	$points1href = floor(array_sum($points1href)/count($points1href));
} else {
	$points1href = false;
}
if (!empty($points2href)) {
	$points2href = floor(array_sum($points2href)/count($points2href));
} else {
	$points2href = false;
}

?>

	<?if ($bPrevDisabled):?>
		<a class="pages-lt disabled" title="<?=GetMessage("nav_prev")?>">&lt;</a>
	<?else:?>
		<a rel="prev" href="<?=$sPrevHref;?>" class="pages-lt" id="<?=$ClientID?>_previous_page" title="<?=GetMessage("nav_prev")?>">&lt;</a>
	<?endif;?>

	<ul class="pages navigation-pages">
		<?
		$bFirst = true;
		$bPoints = false;
		do
		{
			if ($arResult["nStartPage"] <= 2 || $arResult["nEndPage"]-$arResult["nStartPage"] <= 1 || abs($arResult['nStartPage']-$arResult["NavPageNomer"])<=2)
			{

				if ($arResult["nStartPage"] == $arResult["NavPageNomer"]):
		?>
				<li class="active"><a href="<?=$arResult["sUrlPath"]?><?=$strNavQueryStringFull?>"><?=$arResult["nStartPage"]?></a></li>
		<?
				elseif($arResult["nStartPage"] == 1 && $arResult["bSavePage"] == false):
		?>
				<li><a href="<?=$arResult["sUrlPath"]?><?=$strNavQueryStringFull?>"><?=$arResult["nStartPage"]?></a></li>
		<?
				else:
		?>
				<li><a href="<?=$arResult["sUrlPath"]?>?<?=$strNavQueryString?>PAGEN_<?=$arResult["NavNum"]?>=<?=$arResult["nStartPage"]?>"><?=$arResult["nStartPage"]?></a></li>
		<?
				endif;
				$bFirst = false;
				$bPoints = true;
			}
			else
			{
				if ($bPoints)
				{
					?>
						<li class="page-more"><a href="<?=$arResult["sUrlPath"]?>?<?=$strNavQueryString?>PAGEN_<?=$arResult["NavNum"]?>=<?= $points1href ? $points1href : $points2href ?>">...</a></li>
					<?
					$bPoints = false;
					if ($points1href) {
						$points1href = false;
					}
				}
			}
			$arResult["nStartPage"]++;
		} while($arResult["nStartPage"] <= $arResult["nEndPage"]);

		?>
	</ul>

	<?if ($bNextDisabled):?>
		<a class="pages-rt disabled" title="<?=GetMessage("nav_prev")?>">&lt;</a>
	<?else:?>
		<a rel="next" href="<?=$sNextHref;?>" class="pages-rt" id="<?=$ClientID?>_next_page" title="<?=GetMessage("nav_next")?>">&gt;</a>
	<?endif;?>

</div>
<?CJSCore::Init();?>
<script type="text/javascript">
	BX.bind(document, "keydown", function (event) {

		event = event || window.event;
		if (!event.ctrlKey)
			return;

		var target = event.target || event.srcElement;
		if (target && target.nodeName && (target.nodeName.toUpperCase() == "INPUT" || target.nodeName.toUpperCase() == "TEXTAREA"))
			return;

		var key = (event.keyCode ? event.keyCode : (event.which ? event.which : null));
		if (!key)
			return;

		var link = null;
		if (key == 39)
			link = BX('<?=$ClientID?>_next_page');
		else if (key == 37)
			link = BX('<?=$ClientID?>_previous_page');

		if (link && link.href)
			document.location = link.href;
	});
</script>
