<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */

$itemCount = count($arResult);
$isAjax = (isset($_REQUEST["ajax_action"]) && $_REQUEST["ajax_action"] == "Y");
$idCompareCount = 'compareList'.$this->randString();
$obCompare = 'ob'.$idCompareCount;
$idCompareTable = $idCompareCount.'_tbl';
$idCompareRow = $idCompareCount.'_row_';
$idCompareAll = $idCompareCount.'_count';
$style = ($itemCount == 0 ? ' style="display: none;"' : '');
?>
<div id="<? echo $idCompareCount; ?>" class="bx_catalog-compare-list l"><?
if ($isAjax)
{
	$APPLICATION->RestartBuffer();
}
$frame = $this->createFrame($idCompareCount)->begin('');

$productS = BasketNumberWordEndings($itemCount);
$productS_txt = GetMessage("TSB1_PRODUCT") . $productS;

?>
<?
/*
<div class="bx_catalog_compare_count"><?
if ($itemCount > 0)
{
	?><p><? echo GetMessage('CP_BCCL_TPL_MESS_COMPARE_COUNT'); ?>&nbsp;<span id="<? echo $idCompareAll; ?>"><? echo $itemCount; ?></span></p>
	<p class="compare-redirect"><a href="<? echo $arParams["COMPARE_URL"]; ?>"><? echo GetMessage('CP_BCCL_TPL_MESS_COMPARE_PAGE'); ?></a></p><?
}
?></div>
*/
?>
<div class="comp item <?= $itemCount > 0 ? '' : 'empty' ?>" data-full="<?= $itemCount > 0 ? '1' : '0' ?>">
	<div class="i-w">
	
		<a class="lnk" href="<?= $arParams["COMPARE_URL"]; ?>"><span>Сравнение</span></a>
		
		<div class="inf">
			<span class="count compCount" id="<? echo $idCompareAll; ?>"><?= $itemCount; ?></span>
			<span class="txt compTxt"><?= $productS_txt ?></span>
		</div>
		
		<div id="compHint" class="ttp"><div class="w">Товар добавлен к сравнению!</div></div>
		<div id="compHint2" class="ttp"><div class="w">Товар удален из сравнения!</div></div>
	
	</div>
	<script>
		$(window).trigger('scroll');
		<? foreach($arResult as $arItem): ?>
			$('#addComp_<?= $arItem['ID'] ?>, #addComp2_<?= $arItem['ID'] ?>').attr('checked', true).trigger('refresh');
		<? endforeach; ?>
	</script>
</div>
<?
$frame->end();
if ($isAjax)
{
	die();
}
$currentPath = CHTTP::urlDeleteParams(
	$APPLICATION->GetCurPageParam(),
	array(
		$arParams['PRODUCT_ID_VARIABLE'],
		$arParams['ACTION_VARIABLE'],
		'ajax_action'
	),
	array("delete_system_params" => true)
);

$jsParams = array(
	'VISUAL' => array(
		'ID' => $idCompareCount,
	),
	'AJAX' => array(
		'url' => $currentPath,
		'params' => array(
			'ajax_action' => 'Y'
		),
		'templates' => array(
			'delete' => (strpos($currentPath, '?') === false ? '?' : '&').$arParams['ACTION_VARIABLE'].'=DELETE_FROM_COMPARE_LIST&'.$arParams['PRODUCT_ID_VARIABLE'].'='
		)
	),
	'POSITION' => array(
		'fixed' => $arParams['POSITION_FIXED'] == 'Y',
		'align' => array(
			'vertical' => $arParams['POSITION'][0],
			'horizontal' => $arParams['POSITION'][1]
		)
	)
);
?></div>
<script type="text/javascript">
var <? echo $obCompare; ?> = new JCCatalogCompareList(<? echo CUtil::PhpToJSObject($jsParams, false, true); ?>)
</script>