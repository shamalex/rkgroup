<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);

//p($arResult);
?>

<!-- info roll -->
<div class="roll">
	<h2 class="ttl-r">Масса полезной информации</h2>

	<!-- info item container end -->	
	<div class="info c">		
		<?foreach($arResult["SECTIONS"] as $key=>$arSection){
			if($key>=8) break;
			if(in_array($key,array(0,2,5,7))){?>
				<a href="<?=$arSection["SECTION_PAGE_URL"]?>" class="item item-link">
					<div class="pic" style="background-image:url('<?=$arSection["PICTURE"]["SRC"]?>');">
						<div class="all"><span><?= $arSection['ELEMENT_CNT'] ?> <?= getNumEnding($arSection['ELEMENT_CNT'], array('статья', 'статьи', 'статей')) ?></span></div>
					</div>
					<div class="w"> 
						<h2 class="ttl"><?=$arSection["NAME"]?></h2>
						<p class="dsc"><?=$arSection["DESCRIPTION"]?></p>
						<?/*foreach($arSection["ELEMENTS"] as $element){?>
						<p class="dsc"><?=$element["NAME"]?></p>
						<?}*/?>
					</div>
				</a>
			<?}else{?>
				<div class="item item-list">
					<div class="w">
						<h2 class="ttl"><a href="<?=$arSection["SECTION_PAGE_URL"]?>"><?=$arSection["NAME"]?></a></h2>
						<div class="all"><a href="<?=$arSection["SECTION_PAGE_URL"]?>"><?= $arSection['ELEMENT_CNT'] ?> <?= getNumEnding($arSection['ELEMENT_CNT'], array('статья', 'статьи', 'статей')) ?></a></div>
						<ul class="ul">
							<?foreach($arSection["ELEMENTS"] as $element){?>
							<li><a href="<?=$element["DETAIL_PAGE_URL"]?>"><?=$element["NAME"]?></a></li>
							<?}?>
						</ul>
					</div>
				</div>
			<?}
		}?>
	
	</div>
	<!-- / info item container end -->	
</div>
<!-- / info roll end -->	