<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();

foreach($arResult["SECTIONS"] as $key=>$arSection){
	if($key>=8) break;
	if(in_array($key,array(0,2,5,7))){
		$limit=1;
	}else{
		$limit=3;
	}
	
	$resEl=CIBlockElement::GetList(array("SORT"=>"ASC"),array("SECTION_ID"=>$arSection["ID"]),false,array('nPageSize'=>$limit),array("ID","NAME","DETAIL_PAGE_URL"));
	while($arEl=$resEl->GetNext()){
		$arResult["SECTIONS"][$key]["ELEMENTS"][]=$arEl;
	}
}