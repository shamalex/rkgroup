<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();

$arResult['MENU'] = [];

$arRealCatIds = [];
foreach ($arResult['SECTIONS'] as $key => $arSection) {
	if (!empty($arSection['UF_REAL_CAT_LINK'])) {
		$arRealCatIds[$arSection['UF_REAL_CAT_LINK']][] = $key;
	}
}

$IDS = [];
$ID_TO_ROOT = [];
$arSelect = ['ID', "IBLOCK_SECTION_ID", "DEPTH_LEVEL", 'SECTION_PAGE_URL'];
$rsSections = CIBlockSection::GetList(array(), array("IBLOCK_ID"=>$arParams['IBLOCK_ID_LINK']), false, $arSelect);
while ($arSection = $rsSections->GetNext()) {
	if (isset($arRealCatIds[$arSection['ID']])) {
		foreach($arRealCatIds[$arSection['ID']] as $key) {
			$arResult['SECTIONS'][$key]['UF_REAL_CAT_LINK'] = $arSection['SECTION_PAGE_URL'];
		}
		$IDS[] = $arSection['ID'];
	}
	if ($arSection['DEPTH_LEVEL'] > 1) {
		$ID_TO_ROOT[$arSection['ID']] = $arSection['IBLOCK_SECTION_ID'];
	}
}

$CNT = [];
$arFilter = Array("IBLOCK_ID"=>$arParams['IBLOCK_ID_LINK'], "SECTION_ID"=>$IDS, "INCLUDE_SUBSECTIONS"=>"Y");
if (!empty($arParams['FILTER'])) {
	$arFilter = array_merge($arFilter, $arParams['FILTER']);
}
$arSelect = Array("ID", "SECTION_ID", "IBLOCK_SECTION_ID");
$res = CIBlockElement::GetList(Array(), $arFilter, false, false, $arSelect);
while($arFields = $res->Fetch()) {
	$CNT[$arFields['IBLOCK_SECTION_ID']]++;
}

foreach($CNT as $ID => $CNT_NUM) {
	if (!empty($ID_TO_ROOT[$ID])) {
		$CAT_ID = $ID;
		while($ROOT_ID = $ID_TO_ROOT[$CAT_ID]) {
			$CNT[$ROOT_ID] += $CNT_NUM;
			$CAT_ID = $ID_TO_ROOT[$CAT_ID];
		}
	}
}



$arLinks = [];
foreach ($arResult['SECTIONS'] as $key => $arSection) {
	$arSection['ELEMENT_CNT'] = intval($CNT[$arSection['~UF_REAL_CAT_LINK']]);
	if ($arParams['SHOW_CNT'] != 'Y' && !empty($arSection['~UF_REAL_CAT_LINK']) && $arSection['ELEMENT_CNT'] == 0) {
		continue;
	}
	$arLinks[$arSection['ID']] = &$arResult['SECTIONS'][$key];
	if (empty($arSection['UF_REAL_CAT_LINK'])) {
		$arSection['UF_REAL_CAT_LINK'] = '#';
	}
	if ($arSection['DEPTH_LEVEL'] == 1) {
		$arResult['MENU'][$arSection['ID']] = [
			'NAME' => $arSection['~NAME'],
			'DEPTH_LEVEL' => $arSection['DEPTH_LEVEL'],
			'CODE' => !empty($arSection['~CODE']) ? $arSection['~CODE'] : 'empty_'.$arSection['ID'],
			'PICTURE' => $arSection['PICTURE']['SRC'],
			'LINK' => $arSection['UF_REAL_CAT_LINK'],
			'CNT' => $arSection['ELEMENT_CNT'],
			'CHILDREN' => [],
		];
		$arLinks[$arSection['ID']] = &$arResult['MENU'][$arSection['ID']];
	} elseif(!empty($arLinks[$arSection['IBLOCK_SECTION_ID']])) {
		if ($arLinks[$arSection['IBLOCK_SECTION_ID']]['DEPTH_LEVEL'] == 1) {
			$arLinks[$arSection['IBLOCK_SECTION_ID']]['CNT'] += $arSection['ELEMENT_CNT'];
		}
		$arLinks[$arSection['IBLOCK_SECTION_ID']]['CHILDREN'][$arSection['ID']] = [
			'NAME' => $arSection['~NAME'].($arParams['SHOW_CNT'] == 'Y' ? ' ('.$arSection['ELEMENT_CNT'].')' : ''),
			'DEPTH_LEVEL' => $arSection['DEPTH_LEVEL'],
			'LINK' => $arSection['UF_REAL_CAT_LINK'],
			'COL' => $arSection['UF_MENU_COL'],
			'CNT' => $arSection['ELEMENT_CNT'],
			'CHILDREN' => [],
		];
		$arLinks[$arSection['ID']] = &$arLinks[$arSection['IBLOCK_SECTION_ID']]['CHILDREN'][$arSection['ID']];
	}
}
