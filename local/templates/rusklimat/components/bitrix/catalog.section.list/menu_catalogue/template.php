<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>
<ul class="mn-cat" id="menuCatalogue">
	<? foreach($arResult['MENU'] as $arMenu): ?>
		<? if($arMenu['CNT'] > 0 || $arParams['SHOW_CNT'] == 'Y'): ?>
			<li class="mn-cat-<?= $arMenu['CODE'] ?>" data-submenu-id="submenu-<?= $arMenu['CODE'] ?>">
				<a href="<?= $arMenu['LINK'] ?>"<? if(!empty($arMenu['PICTURE'])): ?> style="background-image: url('<?= $arMenu['PICTURE'] ?>'); background-position: center top;"<? endif; ?>><?= $arMenu['NAME'] ?></a>
				<div class="lvl2" id="submenu-<?= $arMenu['CODE'] ?>">
					<div class="shd">
						<div class="w c">
							<?
							$col = false;
							?>
							<div class="item">
								<? foreach($arMenu['CHILDREN'] as $arMenu2): ?>
									<?
									if ($col !== false && $col != $arMenu2['COL']) {
										?>
										</div>
										<div class="item">
										<?
									}
									$col = $arMenu2['COL'];
									?>
									<div class="ttl"><? if(trim($arMenu2['NAME']) != '' && $arMenu2['NAME'] != '-'): ?><a href="<?= $arMenu2['LINK'] ?>"><?= $arMenu2['NAME'] ?></a><? else: ?>&nbsp;<? endif; ?></div>
									<? if(!empty($arMenu2['CHILDREN'])): ?>
										<? foreach($arMenu2['CHILDREN'] as $arMenu3): ?>
											<div class="ttl2"><a href="<?= $arMenu3['LINK'] ?>"><?= $arMenu3['NAME'] ?></a></div>
											<? if(!empty($arMenu3['CHILDREN'])): ?>
												<ul class="list">
													<? foreach($arMenu3['CHILDREN'] as $arMenu4): ?>
														<li><a href="<?= $arMenu4['LINK'] ?>"><?= $arMenu4['NAME'] ?></a></li>
													<? endforeach; ?>
												</ul>
											<? endif; ?>
										<? endforeach; ?>
									<? endif; ?>
								<? endforeach; ?>
							</div>
						</div>

						<div class="w b">
							<div class="submenu-first">
								<a href="javascript:void(0);">Значение</a>
								<a href="javascript:void(0);">Значение второе</a>
								<a href="javascript:void(0);">Значение</a>
								<a href="javascript:void(0);">Значение второе</a>
								<a href="javascript:void(0);">Значение</a>
								<a href="javascript:void(0);">Значение</a>
								<a href="javascript:void(0);">Значение</a>
								<a href="javascript:void(0);">Значение</a>
								<a href="javascript:void(0);">Значение второе</a>
								<a href="javascript:void(0);">Значение второе</a>
							</div>
							<div class="submenu-second">
								<a href="javascript:void(0);">заявка на расчет</a>
								<span></span>
								<a href="javascript:void(0);">переход на услугу</a>
								<span></span>
								<a href="javascript:void(0);">переход на услугу</a>
								<span></span>
								<a href="javascript:void(0);">переход на услугу еще</a>
							</div>
						</div>
					</div>
				</div>
			</li>
		<? endif; ?>
	<? endforeach; ?>
</ul>
