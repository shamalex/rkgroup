<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<?//p($arResult);?>
	<div class="popup-ordercancel"></div>
	<div class="popup-ordermessage"></div>
<?if(!empty($arResult['ERRORS']['FATAL'])):?>

	<?foreach($arResult['ERRORS']['FATAL'] as $error):?>
		<?=ShowError($error)?>
	<?endforeach?>

	<?$component = $this->__component;?>
	<?if($arParams['AUTH_FORM_IN_TEMPLATE'] && isset($arResult['ERRORS']['FATAL'][$component::E_NOT_AUTHORIZED])):?>
		<?$APPLICATION->AuthForm('', false, false, 'N', false);?>
	<?endif?>

<?else:?>

	<?if(!empty($arResult['ERRORS']['NONFATAL'])):?>

		<?foreach($arResult['ERRORS']['NONFATAL'] as $error):?>
			<?=ShowError($error)?>
		<?endforeach?>

	<?endif?>
	
	<!-- basket roll -->
	<div class="roll lk c">
		<h1 class="ttl1">заказ #<?=$arResult["ID"]?></h1>
		<h2 class="ttl2">Информация о заказе</h2>
		<?if($arResult["STATUS_ID"]=="N" || $arResult["STATUS_ID"]=="A"){
			if(!$arResult["SEND_CANCEL"]){?>
			<a href="javascript:void(0)" class="orderSendCancel" data-id="<?=$arResult["ID"]?>">Запросить отмену</a><br />
			<?}else{?>
			<span>По заказу отправлена отмена</span><br />
			<?}?>
		<?}?>
		<a href="javascript:void(0)" class="orderSendMessage" data-id="<?=$arResult["ID"]?>">Задать вопрос</a><br />
		<?if($arResult["SEND_MESSAGE"]>0){?>
			<span>По заказу отправлено запросов: <?=$arResult["SEND_MESSAGE"]?></span><br />
		<?}?>
	
		<div class="lk-print">
			<table>
				<tr>
					<td>
						<p class="ttl">ФИО</p>
						<?
						$FIO = $arResult["USER_NAME"].' '.$arResult["USER_LAST_NAME"];
						foreach($arResult['ORDER_PROPS'] as $arProp) {
							if ($arProp['CODE'] == 'FIO') {
								if (strlen($arProp['VALUE']) > 0) {
									$FIO = $arProp['VALUE'];
								}
								break;
							}
						}
						?>
						<p class="dsc"><?=$FIO?></p>
					</td>
					<td>
						<p class="ttl">Способ оплаты</p>
						<?foreach ($arResult['PAYMENT'] as $payment):?>
							<?if(intval($payment["PAY_SYSTEM_ID"])):?>
								<p class="dsc">
									<?if ($payment['PAY_SYSTEM']):?>
										<?=$payment["PAY_SYSTEM"]["NAME"]?>
									<?else:?>
										<?=$payment["PAY_SYSTEM_NAME"];?>
									<?endif;?>
								</p>
							<?endif;?>
						<?endforeach;?>
					</td>
					<td>
						<?
						/*
						<p class="ttl">Дата доставки</p>
						<p class="dsc">25.11.2014</p>
						*/
						?>
						<?
						$cnt = 0;
						foreach($arResult["BASKET"] as $prod) {
							$cnt += $prod["QUANTITY"];
						}
						?>
						<p class="ttl">всего товаров</p>
						<p class="dsc"><?= $cnt ?></p>
					</td>
				</tr>
				<tr>
					<td>
						<p class="ttl">Покупатель</p>
						<p class="dsc"><?=$arResult["PERSON_TYPE"]["NAME"]?></p>
					</td>
					<td>
						<p class="ttl">Способ доставки</p>
						<?foreach ($arResult['SHIPMENT'] as $shipment):?>
							<?if (intval($shipment["DELIVERY_ID"])):?>
								<p class="dsc"><?=$shipment["DELIVERY"]["NAME"]?></p>
							<?endif;?>
						<?endforeach;?>
						
					</td>
					<td>
						<p class="ttl">Итого</p>
						<p class="dsc"><?=$arResult["PRICE_FORMATED"]?></p>
					</td>
				</tr>
				<tr>
					<td>
						&nbsp;
					</td>
					<td>					
						<?if($arResult["DELIVERY_ADDRESS"] && $shipment["DELIVERY_ID"]==10){?>
						<p class="ttl">адрес доставки</p>
						<p class="dsc"><?=$arResult["DELIVERY_ADDRESS"]?></p>
						<?}?>
						&nbsp;
					</td>
					<td>
						<div class="btn btn-30 btn-b btn-lk-print" onclick="window.print();">Распечатать</div>
					</td>
				</tr>
			</table>
		</div>
		<!-- lk-print -->
		
		<?if (isset($arResult["BASKET"])):?>
			<div class="brd tbl-w tbl-basket-w">
				<!-- tbl-basket -->
				<table class="tbl tbl-hover tbl-lt tbl-basket tbl-lk">
					<tr>
						<th class="col1">Изображение</th>
						<th class="col2">Наименование товара</th>
						<th class="col3">Цена</th>
						<th class="col4 td-amt">Количество</th>
						<th class="col5">Итого</th>
					</tr>
					<?foreach($arResult["BASKET"] as $prod):?>
						<tr class="sp">
							<td colspan="6">&nbsp;</td>
						</tr>
						<tr>
							<td class="td-pic">
								<? if(!empty($prod['PROPERTY_SM_PICTURE1_VALUE']['SRC'])): ?>
									<a href="<?=$prod["DETAIL_PAGE_URL"]?>" class="pic" style="background-image:url('<?= $prod['PROPERTY_SM_PICTURE1_VALUE']['SRC'] ?>');"></a>
								<? else: ?>
									&nbsp;
								<? endif; ?>
							</td>
							<td class="td-name">
								<a href="<?=$prod["DETAIL_PAGE_URL"]?>"><?=htmlspecialcharsEx($prod["NAME"])?></a>
							</td>
							<td class="td-price"><?=$prod["PRICE_FORMATED"]?></td>
							<td class="td-amt">
								<?=$prod["QUANTITY"]?>
							</td>
							<td class="td-price"><?=SaleFormatCurrency($prod["PRICE"]*$prod["QUANTITY"], $prod["CURRENCY"]);?></td>
						</tr>
					<?endforeach?>
				</table>
				<!-- / tbl-basket end -->
				
				<!-- basket tbl sum -->
				<?if($arResult["PRICE_DELIVERY"]>0){?><div class="order-delivery">Стоимость доставки: <strong><span id="bskDel"><?=$arResult["PRICE_DELIVERY_FORMATED"]?></span></strong></div><?}?>
				<div class="basket-sum">Сумма: <strong><span id="bskSum"><?=$arResult["PRICE_FORMATED"]?></span></strong></div>
				<!-- / basket tbl sum end -->
				
			</div>
			<!-- / brd tbl-basket-w end -->
		<?endif;?>
			
		<h2 class="ttl2">История заказа</h2>

		<div class="brd tbl-w">
			<!-- tbl-lk-history -->
			<table class="tbl tbl-hover tbl-lt tbl-lk-history">
				<tr>
					<th class="col1">Дата</th>
					<th class="col2">Статус</th>
					<?/*<th class="col3">Комментарий менеджера</th>*/?>
				</tr>
				<?foreach($arResult["ORDER_HISTORY"] as $key=>$val){?>
				<tr class="sp">
					<td colspan="2">&nbsp;</td>
				</tr>
				<tr>
					<td><?=$val["DATE"]?></td>
					<td><span class="st-grey"><?=$val["STATUS"]?></span></td>
					<?/*<td class="manager-cmt">Оставьте свой отзыв о компании и сделайте наш сервис лучше! <a href="#">Оставить отзыв</a></td>*/?>
				</tr>
				<?}?>
			</table>
			<!-- / tbl-lk-history end -->
		</div>
		<!-- / brd end -->

		
	</div>
	<!-- / lk roll end -->

<?endif?>