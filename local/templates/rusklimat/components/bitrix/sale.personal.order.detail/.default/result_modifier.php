<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

$cp = $this->__component;
if (is_object($cp))
{
	CModule::IncludeModule('iblock');

	if(empty($arResult['ERRORS']['FATAL']))
	{

		$hasDiscount = false;
		$hasProps = false;
		$productSum = 0;
		$basketRefs = array();

		$noPict = array(
			'SRC' => $this->GetFolder().'/images/no_photo.png'
		);

		if(is_readable($nPictFile = $_SERVER['DOCUMENT_ROOT'].$noPict['SRC']))
		{
			$noPictSize = getimagesize($nPictFile);
			$noPict['WIDTH'] = $noPictSize[0];
			$noPict['HEIGHT'] = $noPictSize[1];
		}
		if (isset($arResult["BASKET"]))
		{
			/* foreach ($arResult["BASKET"] as $k => &$prod)
			{
				if (floatval($prod['DISCOUNT_PRICE']))
					$hasDiscount = true;
				// move iblock props (if any) to basket props to have some kind of consistency
				if (isset($prod['IBLOCK_ID']))
				{
					$iblock = $prod['IBLOCK_ID'];
					if (isset($prod['PARENT']))
						$parentIblock = $prod['PARENT']['IBLOCK_ID'];
					foreach ($arParams['CUSTOM_SELECT_PROPS'] as $prop)
					{
						$key = $prop.'_VALUE';
						if (isset($prod[$key]))
						{
							// in the different iblocks we can have different properties under the same code
							if (isset($arResult['PROPERTY_DESCRIPTION'][$iblock][$prop]))
								$realProp = $arResult['PROPERTY_DESCRIPTION'][$iblock][$prop];
							elseif (isset($arResult['PROPERTY_DESCRIPTION'][$parentIblock][$prop]))
								$realProp = $arResult['PROPERTY_DESCRIPTION'][$parentIblock][$prop];
							if (!empty($realProp))
								$prod['PROPS'][] = array(
									'NAME' => $realProp['NAME'],
									'VALUE' => htmlspecialcharsEx($prod[$key])
								);
						}
					}
				}
				// if we have props, show "properties" column
				if (!empty($prod['PROPS']))
					$hasProps = true;
				$productSum += $prod['PRICE'] * $prod['QUANTITY'];
				$basketRefs[$prod['PRODUCT_ID']][] =& $arResult["BASKET"][$k];
				if (!isset($prod['PICTURE']))
					$prod['PICTURE'] = $noPict;
			} */
			
			CModule::IncludeModule('iblock');
			
			$ID_TO_CATALOG = [];
			foreach($arResult["BASKET"] as $k => $prod) {
				$ID_TO_CATALOG[$prod['PRODUCT_ID']] = $prod['ID'];
			}

			if (!empty($ID_TO_CATALOG)) {
				
				$arFilter = Array("IBLOCK_ID"=>8, 'ID'=>array_keys($ID_TO_CATALOG));
				$arSelect = Array("ID", "NAME", "CODE", "XML_ID", "IBLOCK_SECTION_ID", "PROPERTY_FULL_NAME", "PROPERTY_SM_PICTURE1", "PROPERTY_ITEM_NAME", "PROPERTY_CODE", "PROPERTY_NS_CODE", "PROPERTY_EL_BRAND.NAME");
				$res = CIBlockElement::GetList(Array(), $arFilter, false, false, $arSelect);  
				while($arFields = $res->Fetch()) {
					if (!empty($arFields['PROPERTY_SM_PICTURE1_VALUE'])) {
						$arFields['PROPERTY_SM_PICTURE1_VALUE'] = CFile::GetFileArray($arFields['PROPERTY_SM_PICTURE1_VALUE']);
					}
					$arFields['CATALOG_ID'] = $arFields['ID'];
					$ID = $arFields['ID'];
					
					// $arSection = array();
					// $arSection["ID"] = $arFields['IBLOCK_SECTION_ID'];
					// $arSection["PATH"] = array();
					// if (!empty($arSection["ID"])) {
						// $rsPath = CIBlockSection::GetNavChain($arFilter["IBLOCK_ID"], $arSection["ID"]);
						// while($arPath = $rsPath->GetNext()) {
							// $arSection["PATH"][] = $arPath;
						// }
					// }
					// $arFields['SECTION'] = $arSection;
					
					$arResult["BASKET"][$ID_TO_CATALOG[$ID]] = array_merge($arFields, $arResult["BASKET"][$ID_TO_CATALOG[$arFields['ID']]]);
				}
			}
			
		}

		$arResult['HAS_DISCOUNT'] = $hasDiscount;
		$arResult['HAS_PROPS'] = $hasProps;

		$arResult['PRODUCT_SUM_FORMATTED'] = SaleFormatCurrency($productSum, $arResult['CURRENCY']);

		if($img = intval($arResult["DELIVERY"]["STORE_LIST"][$arResult['STORE_ID']]['IMAGE_ID']))
		{

			$pict = CFile::ResizeImageGet($img, array(
				'width' => 150,
				'height' => 90
			), BX_RESIZE_IMAGE_PROPORTIONAL_ALT, true);

			if(strlen($pict['src']))
				$pict = array_change_key_case($pict, CASE_UPPER);

			$arResult["DELIVERY"]["STORE_LIST"][$arResult['STORE_ID']]['IMAGE'] = $pict;
		}

	}
}
$codes_for_addr=array(
	"HIDE_CURRENT_CITY",
	"HIDE_DELIVERY_D_ADDR",
	"HIDE_DELIVERY_D_HOUSE",
	"HIDE_DELIVERY_D_CORP",
	"HIDE_DELIVERY_D_BUILD",
	"HIDE_DELIVERY_D_ROOM",
);
$prop=array();
foreach($arResult["ORDER_PROPS"] as $props){
	if(in_array($props["CODE"],$codes_for_addr))
		$prop[$props["CODE"]]=$props["VALUE"];
}
if($prop["HIDE_CURRENT_CITY"]) $arResult["DELIVERY_ADDRESS"].=$prop["HIDE_CURRENT_CITY"].", ";
if($prop["HIDE_DELIVERY_D_ADDR"]) $arResult["DELIVERY_ADDRESS"].=$prop["HIDE_DELIVERY_D_ADDR"].", ";
if($prop["HIDE_DELIVERY_D_HOUSE"]) $arResult["DELIVERY_ADDRESS"].=$prop["HIDE_DELIVERY_D_HOUSE"].", ";
if($prop["HIDE_DELIVERY_D_CORP"]) $arResult["DELIVERY_ADDRESS"].="корп. ".$prop["HIDE_DELIVERY_D_CORP"].", ";
if($prop["HIDE_DELIVERY_D_BUILD"]) $arResult["DELIVERY_ADDRESS"].="стр. ".$prop["HIDE_DELIVERY_D_BUILD"].", ";
if($prop["HIDE_DELIVERY_D_ROOM"]) $arResult["DELIVERY_ADDRESS"].=$prop["HIDE_DELIVERY_D_ROOM"].", ";
$arResult["DELIVERY_ADDRESS"]=trim($arResult["DELIVERY_ADDRESS"],", ");

$status_list = [];
$obStatus=CSaleStatus::GetList();
while($arStatus=$obStatus->GetNext()){
	$status_list[$arStatus["ID"]] = $arStatus["NAME"];
}


//p($status_list);
$res=CSaleOrderChange::GetList(array("ID"=>"DESC"),array("ORDER_ID"=>$arResult["ID"],"ENTITY"=>"ORDER"));
while($ar=$res->GetNext()){
	$dt=explode(" ",$ar["DATE_CREATE"]);
	if($ar["TYPE"]=="ORDER_ADDED")
		$arResult["ORDER_HISTORY"][]=array("DATE"=>$dt[0],"STATUS"=>$status_list["N"]);
	else{
		$data=unserialize($ar["~DATA"]);
		if($data["STATUS_ID"] && $status_list[$data["STATUS_ID"]])
			$arResult["ORDER_HISTORY"][]=array("DATE"=>$dt[0],"STATUS"=>$status_list[$data["STATUS_ID"]]);
	}
}

$arResult["SEND_CANCEL"]=CIBlockElement::GetList(array(),array('IBLOCK_ID' => 36, "PROPERTY_order"=>$arResult["ID"]),array(),false,array('ID', 'NAME')); 
$arResult["SEND_MESSAGE"]=CIBlockElement::GetList(array(),array('IBLOCK_ID' => 37, "PROPERTY_order"=>$arResult["ID"]),array(),false,array('ID', 'NAME')); 
?>