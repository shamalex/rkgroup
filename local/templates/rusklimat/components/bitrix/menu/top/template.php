<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>

<? if (!empty($arResult)): ?>
	<?//p($arResult);
	$arMenu = array();
	foreach($arResult as $arItem) {
		if($arParams["MAX_LEVEL"] == 1 && $arItem["DEPTH_LEVEL"] > 1) {
			continue;
		}
		$arItem['TARGET'] = false;
		if (strpos($arItem["LINK"], 'http:') === 0 || strpos($arItem["LINK"], 'https:') === 0) {
			$arItem['TARGET'] = '_blank';
		}
		if ($arItem["DEPTH_LEVEL"] == 1) {
			$arItem['CHILDREN'] = array();
			$arMenu[] = $arItem;
		} else {
			$cnt = count($arMenu) - 1;
			$arMenu[$cnt]['CHILDREN'][] = $arItem;
		}
	}
	?>
	<ul class="mn-func-ul c">
		<? foreach($arMenu as $arItem): ?>
			<? if(count($arItem["CHILDREN"]) == 0): ?>
				<li><a href="<?=$arItem["LINK"]?>"<?= ($arItem['TARGET'] ? ' target="'.$arItem['TARGET'].'"' : '') ?>><?=$arItem["TEXT"]?></a></li>
			<? else: ?>
				<li class="mn-drop">
					<? if($arItem["LINK"] == ""): ?>
						<span><?=$arItem["TEXT"]?></span>
					<? else: ?>
						<span><a href="<?=$arItem["LINK"]?>"<?= ($arItem['TARGET'] ? ' target="'.$arItem['TARGET'].'"' : '') ?>><?=$arItem["TEXT"]?></a></span>
					<? endif; ?>
					<ul class="lvl2">
						<? foreach($arItem["CHILDREN"] as $arChild): ?>
							<li><a href="<?=$arChild["LINK"]?>"<?= ($arChild['TARGET'] ? ' target="'.$arChild['TARGET'].'"' : '') ?>><?=$arChild["TEXT"]?></a></li>
						<? endforeach; ?>
					</ul>
				</li>
			<? endif; ?>
		<? endforeach; ?>
	</ul>
<? endif; ?>