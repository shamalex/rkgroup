<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>

<? if (!empty($arResult)): ?>
	<?
	$arMenu = array();
	$curCol = 0;
	foreach($arResult as $arItem) {
		if($arParams["MAX_LEVEL"] == 1 && $arItem["DEPTH_LEVEL"] > 1) {
			continue;
		}
		if ($arItem["TEXT"] == '--') {
			$curCol++;
			continue;
		}
		$arItem['TARGET'] = false;
		if (strpos($arItem["LINK"], 'http:') === 0 || strpos($arItem["LINK"], 'https:') === 0) {
			$arItem['TARGET'] = '_blank';
		}
		if (!isset($arMenu[$curCol])) {
			$arMenu[$curCol] = array(
				'TITLE' => false,
				'ITEMS' => array(),
			);
		}
		if (!$arMenu[$curCol]['TITLE']) {
			$arMenu[$curCol]['TITLE'] = $arItem["TEXT"];
		} else {
			$arMenu[$curCol]['ITEMS'][] = $arItem;
		}
	}
	?>
	<!-- ft-mn -->
	<div class="ft-mn">
		<? foreach($arMenu as $arColumn): ?>
			<div class="clmn">
				<h3 class="ttl"><?= $arColumn['TITLE'] ?></h3>
				<ul class="ul">
					<? foreach($arColumn['ITEMS'] as $arItem): ?>
						<li><a href="<?=$arItem["LINK"]?>"<?= ($arItem['TARGET'] ? ' target="'.$arItem['TARGET'].'"' : '') ?>><?=$arItem["TEXT"]?></a></li>
					<? endforeach; ?>
				</ul>
			</div>
		<? endforeach; ?>
	</div>
	<!-- /ft-mn end -->
	
<? endif; ?>