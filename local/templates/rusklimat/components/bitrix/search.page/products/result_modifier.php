<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
global $arGeoData;
/* get from catalog/index.php */
$QUANITY_PROP = preg_replace('/\-+/ui', '_', $arGeoData['CUR_CITY']['CITY_CODE1']);
$QUANITY_PROP = strtoupper('COUNT_'.$QUANITY_PROP);
$QUANITY_PROP2 = preg_replace('/\-+/ui', '_', $arGeoData['CUR_CITY']['CITY_CODE2']);
$QUANITY_PROP2 = strtoupper('COUNT_'.$QUANITY_PROP2);

if(is_array($arResult['SEARCH'])){
	$PRODUCT_IDs = array();
	foreach($arResult['SEARCH'] as $key => &$item){
		$price_old = GetCatalogProductPrice($item['ITEM_ID'], $arGeoData["CUR_CITY"]["PRICE_NODISCOUNT_ID"]);
		$price_now = GetCatalogProductPrice($item['ITEM_ID'], $arGeoData["CUR_CITY"]["PRICE_ID"]);
		$PRODUCT_IDs[$key] = $item['ITEM_ID'];
		$item['PRICE_VALUE'] = $price_now;
		$item['OLD_PRICE_VALUE'] = $price_old;
	}
	$prodRes = CIBlockElement::GetList(array(),
		array(
			"IBLOCK_ID" => 8,
			"=ID" => $PRODUCT_IDs,
			"ACTIVE"=>"Y"
		) , false, false, array("ID", "PROPERTY_sm_picture1","PROPERTY_sm_picture2","PROPERTY_sm_picture3","PROPERTY_sm_picture4", "PROPERTY_".$QUANITY_PROP, "PROPERTY_".$QUANITY_PROP2) 
	);
	while($prod = $prodRes->GetNext()){
		for($i=1;$i<=10;$i++){
			if(($imgId = $prod['PROPERTY_SM_PICTURE'.$i.'_VALUE']))
				break;
		}
		if(false!==($PROD_KEY = array_search($prod['ID'], $PRODUCT_IDs))){
			$arResult['SEARCH'][$PROD_KEY]['IS_PRODUCT'] = true;
			$arResult['SEARCH'][$PROD_KEY]['IMG'] = CFile::GetPath($imgId);
			
			$quantity = 0;
			$arResult['SEARCH'][$PROD_KEY]['QUANITY'] =& $quantity;
			if($prod['PROPERTY_'.$QUANITY_PROP2.'_VALUE'])
				$quantity = 1;
			if($prod['PROPERTY_'.$QUANITY_PROP.'_VALUE'])
				$quantity = 2;
			unset($quantity);
		}
		

	}
}

?>