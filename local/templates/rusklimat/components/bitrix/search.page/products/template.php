<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
?>
<!-- srch-res -->
<div class="srch-res">
	<form class="srch-form c" action="" method="get">
	<div class="srch-holder">
		<input class="srch-field srch-res-inp" placeholder="например: Мобильный кондиционер Ballu"  name="q" value="<?=$arResult["REQUEST"]["QUERY"]?>" id="searchAutocompleteResults" type="text"/>
		<input type="hidden" name="tags" value="<?echo $arResult["REQUEST"]["TAGS"]?>" />
		<input type="hidden" name="how" value="<?echo $arResult["REQUEST"]["HOW"]=="d"? "d": "r"?>" />
		<input type="submit" class="srch-btn" name="srch-btn" value="" />
		<!-- autocomplete is dynamically generated, hd.less > .autocomplete-suggestions -->
	</div>
	<fieldset class="checkset inline">
		<div class="checkline">
			<input type="checkbox" id="srchInName" checked><label for="srchInName">искать фрагмент текста в наименованиях товаров</label>
		</div>
		<div class="checkline">
			<input type="checkbox" id="srchInDesc" checked><label for="srchInDesc">искать фрагмент текста в описаниях и характеристиках</label>
		</div>
	</fieldset>
	</form>
</div>
<!-- / srch-res end -->
<?if($arResult && $arResult["NAV_RESULT"]):?>
<h2 class="ttl2">Совпадений по запросу <span class="marker">“<?=$arResult["REQUEST"]["QUERY"]?>”</span> <?echo GetMessage("CT_BSP_FOUND")?>: <span class="srch-res-nmbr" id="srchResNmbr"><?echo $arResult["NAV_RESULT"]->SelectedRowsCount()?></span></h2>
<? endif; ?>
<div class="brd tbl-w">
	<!-- tbl-srch-res -->
	<?if($arResult["REQUEST"]["QUERY"] === false):?>
	<?elseif($arResult["ERROR_CODE"]!=0):?>
		<p><?=GetMessage("CT_BSP_ERROR")?></p>
		<?ShowError($arResult["ERROR_TEXT"]);?>
		<p><?=GetMessage("CT_BSP_CORRECT_AND_CONTINUE")?></p>
	<?elseif(count($arResult["SEARCH"])>0):?>
		<table class="tbl tbl-hover tbl-lt tbl-srch-res">
			<tr>
				<th class="col1">Изображение</th>
				<th class="col2">Наименование товара</th>
				<th class="col3">Цена</th>
				<th class="col4">Наличие</th>
				<th class="col5">совпадение</th>
			</tr>
			<tr class="sp">
				<td colspan="5">&nbsp;</td>
			</tr>
			<?foreach($arResult["SEARCH"] as $arItem):?>
			<? if(!$arItem['IS_PRODUCT']):?>
			<tr>
				<td colspan="4">
					<a href="<?echo $arItem["URL"]?>"><?echo $arItem["TITLE_FORMATED"]?></a>
				</td>
				<td class="td-where">
					<?if ($arItem["TITLE_FORMATED"] == $arItem["TITLE"]): ?>
						<span class="marker">в содержимом</span>
					<? else : ?>
						&nbsp;
					<? endif; ?>
				</td>
			</tr>
			<? else :?>
			<tr>
				<td class="td-pic">
					<? if($arItem['IMG']): ?>
					<a href="<?echo $arItem["URL"]?>" class="pic" style="background-image:url('<?echo $arItem["IMG"]?>');"></a>
					<?/*  else :  */?>
					<? endif; ?>
				</td>
				<td class="td-name">
					<a href="<?echo $arItem["URL"]?>"><?echo $arItem["TITLE_FORMATED"]?></a>
				</td>
				<td class="td-price">
					<? if($arItem['PRICE_VALUE'] || $arItem['OLD_PRICE_VALUE']): ?>
						<? if($arItem['PRICE_VALUE']):?>
							<span class="curr"><?echo number_format($arItem["PRICE_VALUE"]["PRICE"]);?> руб.</span>
						<? endif; ?>
						<? if($arItem['OLD_PRICE_VALUE']):?>
							<span class="old"><?echo number_format($arItem["OLD_PRICE_VALUE"]["PRICE"]);?> руб.</span>
						<? endif; ?>
						<? if($arItem['PRICE_VALUE'] && $arItem['OLD_PRICE_VALUE']): ?>
							<br /><span class="profit"><?echo number_format($arItem["OLD_PRICE_VALUE"]["PRICE"]-$arItem["PRICE_VALUE"]["PRICE"]);?> экономии</span>
						<? endif; ?>
					<? else : ?>
					<? endif; ?>
				</td>
				<td class="td-status">
					<?if($arItem['QUANITY'] === 2):?>
						<div class="status st-ok">в наличии</div>
					<?elseif($arItem['QUANITY'] === 1):?>
						<div class="status st-no">под заказ</div>
					<?elseif($arItem['QUANITY'] === 0):?>
						<div class="status st-no st-no-text"></div>
					<?endif;?>
				</td>
				<td class="td-where">
					<?if ($arItem["TITLE_FORMATED"] == $arItem["TITLE"]): ?>
						<span class="marker">в содержимом</span>
					<? else : ?>
						&nbsp;
					<? endif; ?>
				</td>
			</tr>
			<? endif; ?>
			<tr class="sp">
				<td colspan="5">&nbsp;</td>
			</tr>
			<?endforeach;?>
		</table>
	<?else:?>
		<?$APPLICATION->IncludeFile("include/search_error.php", Array(), Array("MODE"=>"html"));?>
	<?endif;?>

	<!-- / tbl-srch-res end -->

</div>
<!-- / brd end -->
