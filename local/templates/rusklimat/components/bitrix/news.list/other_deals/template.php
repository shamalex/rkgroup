<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<?ob_start();?>
<?
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>

<? if(!empty($arResult["ITEMS"])): ?>
	<!-- other deals roll -->
	<div class="roll">
		<h2 class="ttl-r"><?= !empty($GLOBALS['OTHER_DEALS_ROLL_NAME']) ? $GLOBALS['OTHER_DEALS_ROLL_NAME'] : "Популярные товары" ?></h2>
		<!-- other deals -->
		<div class="other-deals c">
			<!-- slides -->
			<div class="slides c" id="<?= !empty($GLOBALS['OTHER_DEALS_ROLL_ID']) ? $GLOBALS['OTHER_DEALS_ROLL_ID'] : "other" ?>">

				<?foreach($arResult["ITEMS"] as $arItem):?>
					<?
					$this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_EDIT"));
					$this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_DELETE"), array("CONFIRM" => GetMessage('CT_BNL_ELEMENT_DELETE_CONFIRM')));
					?>

					<!-- item w/stamps -->
					<div class="item b-base" id="<?=$this->GetEditAreaId($arItem['ID']);?>">
						<div class="w c">

							<a class="lnk" href="<?= $arItem["DISPLAY_PROPERTIES"]["ITEM"]["LINK_ELEMENT_VALUE"]["DETAIL_PAGE_URL"] ?>">
								<div class="ttl"><?= !empty($arItem["DISPLAY_PROPERTIES"]["TEXT_TOP"]["DISPLAY_VALUE"]) ? $arItem["DISPLAY_PROPERTIES"]["TEXT_TOP"]["DISPLAY_VALUE"] : $arItem["DISPLAY_PROPERTIES"]["ITEM"]["LINK_ELEMENT_VALUE"]["NAME"] ?></div>
								<div class="pic"<? if(!empty($arItem["DISPLAY_PROPERTIES"]["ITEM"]["LINK_ELEMENT_VALUE"]["PROPERTY_PICTURE1_VALUE"])): ?> style="background-image:url('<?= $arItem["DISPLAY_PROPERTIES"]["ITEM"]["LINK_ELEMENT_VALUE"]["PROPERTY_PICTURE1_VALUE"] ?>');"<? endif; ?>></div>
								<div class="stamps c">
									<?/*= $arItem["DISPLAY_PROPERTIES"]["CHECKS"]["DISPLAY_VALUE"] */?>
									<?foreach($arItem["LABELS"] as $i=>$label){?>
										<div class="ico" style="background-image: url('<?=$label["PREVIEW_PICTURE"]["SRC"]?>');<?if($label["PROPERTY_COLOR_VALUE"]){?>color: #<?=trim($label["PROPERTY_COLOR_VALUE"],"#")?>;<?}?>"><?=$label["PROPERTY_TEXT_VALUE"]?></div>
									<?}?>
									<div class="clear"></div>
								</div>
								<div class="dsc"><?= !empty($arItem["DISPLAY_PROPERTIES"]["TEXT_BOTTOM"]["DISPLAY_VALUE"]) ? $arItem["DISPLAY_PROPERTIES"]["TEXT_BOTTOM"]["DISPLAY_VALUE"] : '' ?></div>
							</a>
							<div class="prc">
								<div class="curr curr-only"><?= $arItem["DISPLAY_PROPERTIES"]["ITEM"]["LINK_ELEMENT_VALUE"]["PRICES"][$arParams['GEO_DATA']['CITY_CODE']]["PRINT_VALUE"] ?></div>
							</div>
							
							#ADD_BTN_<?=$arItem["DISPLAY_PROPERTIES"]["ITEM"]["LINK_ELEMENT_VALUE"]["ID"]?>#

							<?
							$cats = [];
							foreach($arItem["DISPLAY_PROPERTIES"]["ITEM"]["LINK_ELEMENT_VALUE"]['PATH'] as $arSection) {
								$cats[] = $arSection['NAME'];
							}
							?>
							<script>
								$('#other_deal_<?= $arItem["ID"] ?>_add').on('click', function() {
									window.dataLayer = window.dataLayer || [];
									window.dataLayer.push({
										'event': 'addToCart',
										"ecommerce": {
											"currencyCode": "RUB",
											"add": {
												"products": [
													{
														"id": "<?= $arItem["ID"] ?>",
														"name" : "<?= $arItem["NAME"] ?>",
														"price": <?= number_format($arItem["DISPLAY_PROPERTIES"]["ITEM"]["LINK_ELEMENT_VALUE"]["PRICES"][$arParams['GEO_DATA']['CITY_CODE']]["VALUE"], 2, '.', '') ?>,
														"brand": "<?= $arItem["DISPLAY_PROPERTIES"]["ITEM"]["LINK_ELEMENT_VALUE"]["PROPERTY_EL_BRAND_NAME"] ?>",
														"category": "<?= implode('/', $cats) ?>",
														"quantity": 1
													}
												]
											}
										}
									});
								});
							</script>

						</div>
					</div>
					<!-- / item w/stamps -->
				<?endforeach;?>

			</div>
			<!-- / slides end -->
		</div>
		<!-- / other deals end -->
	</div>
	<!-- / other deals roll end -->
<? endif; ?>
<?
$this->__component->arResult["CACHED_TPL"] = @ob_get_contents();
ob_get_clean();
?>