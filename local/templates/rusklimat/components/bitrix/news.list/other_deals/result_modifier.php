<?

CModule::IncludeModule('iblock');
CModule::IncludeModule('catalog');

$LABELS = [];
$arSort = array('SORT'=>'ASC', 'NAME'=>'ASC');
$arFilter = Array("IBLOCK_ID"=>26, "ACTIVE"=>"Y");
$arSelect = array("ID","NAME","PREVIEW_PICTURE","PROPERTY_text", "PROPERTY_color");
$res = CIBlockElement::GetList($arSort, $arFilter, false, false, $arSelect);  
while ($arFields = $res->GetNext()) {
	if($arFields["PREVIEW_PICTURE"]){
		$arFields["PREVIEW_PICTURE"]=CFile::GetByID($arFields["PREVIEW_PICTURE"])->GetNext();
		$arFields["PREVIEW_PICTURE"]["SRC"]=CFile::GetPath($arFields["PREVIEW_PICTURE"]["ID"]);
		$LABELS[$arFields["ID"]] = $arFields;
	}
}

foreach($arResult["ITEMS"] as $i=>$arItem) {
	
	$arItem["LABELS"]=[];
	
	$arItem["DISPLAY_ACTIVE_TO"] = CIBlockFormatProperties::DateFormat($arParams["ACTIVE_DATE_FORMAT"], MakeTimeStamp($arItem["DATE_ACTIVE_TO"], CSite::GetDateFormat()));
	
	$arFilter = Array("IBLOCK_ID"=>$arItem["DISPLAY_PROPERTIES"]["ITEM"]["LINK_IBLOCK_ID"], "ID"=>$arItem["DISPLAY_PROPERTIES"]["ITEM"]["VALUE"]);
	$arSelect = Array("NAME", "DETAIL_PAGE_URL", "IBLOCK_SECTION_ID", "PROPERTY_picture1", "PROPERTY_EL_BRAND.NAME", "CATALOG_GROUP_".$arParams['GEO_DATA']['PRICE_ID'], "CATALOG_GROUP_".$arParams['GEO_DATA']['PRICE_NODISCOUNT_ID'], "PROPERTY_labels");
	$arLimit = Array('nTopCount'=>1);
	$res = CIBlockElement::GetList(Array(), $arFilter, false, $arLimit, $arSelect);  
	if ($obElement = $res->GetNextElement()) {
		
		$arFields = $obElement->GetFields();
		
		if (!empty($arFields['PROPERTY_PICTURE1_VALUE'])) {
			// $arFields['PROPERTY_PICTURE1_VALUE'] = CFile::GetFileArray($arFields['PROPERTY_PICTURE1_VALUE']);
			$arFields['PROPERTY_PICTURE1_VALUE'] = CFile::ResizeImageGet($arFields['PROPERTY_PICTURE1_VALUE'], array('width'=>296, 'height'=>170), BX_RESIZE_IMAGE_PROPORTIONAL, false);
			$arFields['PROPERTY_PICTURE1_VALUE'] = $arFields['PROPERTY_PICTURE1_VALUE']['src'];
		}
		
		$arFields["PATH"] = array();
		$rsPath = CIBlockSection::GetNavChain($arResult["IBLOCK_ID"], $arFields["IBLOCK_SECTION_ID"]);
		// $rsPath->SetUrlTemplates("", $arParams["SECTION_URL"]);
		while($arPath = $rsPath->GetNext())
		{
			$ipropValues = new \Bitrix\Iblock\InheritedProperty\SectionValues($arParams["IBLOCK_ID"], $arPath["ID"]);
			$arPath["IPROPERTY_VALUES"] = $ipropValues->getValues();
			$arFields["PATH"][] = $arPath;
		}
		
		$arResultPrices = CIBlockPriceTools::GetCatalogPrices($arItem["DISPLAY_PROPERTIES"]["ITEM"]["LINK_IBLOCK_ID"], [$arParams['GEO_DATA']['CITY_CODE'], $arParams['GEO_DATA']['CITY_CODE'].'_NODISCOUNT']);
		$arConvertParams = ['CONVERT_CURRENCY'=>'N', 'CURRENCY_ID'=>''];
		$arFields["PRICES"] = CIBlockPriceTools::GetItemPrices($arItem["DISPLAY_PROPERTIES"]["ITEM"]["LINK_IBLOCK_ID"], $arResultPrices, $arFields, false, $arConvertParams);
		
		$arItem["DISPLAY_PROPERTIES"]["ITEM"]["LINK_ELEMENT_VALUE"] = $arFields;

		// p($arFields);
		if(!empty($arFields["PROPERTY_LABELS_VALUE"]) && count($LABELS)>0){
			foreach($arFields["PROPERTY_LABELS_VALUE"] as $label){
				if(isset($LABELS[$label])){
					$arItem["LABELS"][] = $LABELS[$label];
				}
			}
		}	
	}
	
	$arResult["ITEMS"][$i] = $arItem;
	
	
}
$cp = $this->__component; // объект компонента
 
if (is_object($cp))
{
   // добавим в arResult компонента два поля - MY_TITLE и IS_OBJECT
   $cp->arResult['arResult'] = $arResult;
    
   $cp->SetResultCacheKeys(array('arResult'));
   // сохраним их в копии arResult, с которой работает шаблон, если модуль main меньше 10.0
   if (!isset($arResult['arResult']))
   {
      $arResult['arResult'] = $cp->arResult['arResult'];
   }
}
$this->__component->SetResultCacheKeys(array("CACHED_TPL"));?>