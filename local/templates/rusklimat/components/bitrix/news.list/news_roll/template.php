<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>
<?//echo "<pre>";print_r($arResult);echo "</pre>";?>
<?//echo "<pre>";print_r($arParams);echo "</pre>";?>
<?if(count($arResult["ITEMS"])>0){?>
<!-- news roll -->
<div class="roll">
	<h2 class="ttl-r">Интересные новости</h2>
	<!-- news -->	
	<div class="news c">
		<!-- news slides -->
		<div class="slides c" id="news">
			<?foreach($arResult["ITEMS"] as $arItem){?>
				<?if($arItem["DETAIL_TEXT"]){?><a class="item" href="<?=$arItem["DETAIL_PAGE_URL"]?>"><?}else{?><div class="item"><?}?>
					
					<div class="pic" style="background-image:url('<?=$arItem["PREVIEW_PICTURE"]["SRC"]?>');"></div>
					<div class="w">
						<h2 class="ttl"><?=$arItem["NAME"]?></h2>
						<p class="date"><?=$arItem["ACTIVE_FROM"]?></p>
					</div>
				<?if($arItem["DETAIL_TEXT"]){?></a><?}else{?></div><?}?>
			<?}?>
		</div>
	</div>
	<!-- / news end -->
</div>
<!-- / news block end -->
<?}?>