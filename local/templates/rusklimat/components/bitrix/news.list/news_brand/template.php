<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>
<?//echo "<pre>";print_r($arResult);echo "</pre>";?>
<?//echo "<pre>";print_r($arParams);echo "</pre>";?>
<?if(count($arResult["ITEMS"])>0){?>
<div class="brand-news">
	<h2 class="ttl2">Новости по бренду</h2>
	<div class="items c">
		<?foreach($arResult["ITEMS"] as $arItem){?>
			<?if($arItem["DETAIL_TEXT"]){?><a href="<?=$arItem["DETAIL_PAGE_URL"]?>" class="item"><?}else{?><div class="item"><?}?>
				<div class="ttl"><?=$arItem["NAME"]?></div>
				<div class="date"><?=$arItem["ACTIVE_FROM"]?></div>
			<?if($arItem["DETAIL_TEXT"]){?></a><?}else{?></div><?}?>
		<?}?>	
	</div>
	<!-- / items end -->
</div>
<!-- / cat-news end -->
<?}?>