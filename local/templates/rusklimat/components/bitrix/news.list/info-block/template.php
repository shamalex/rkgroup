<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>
<?if(count($arResult["ITEMS"])>0){?>
<div class="info-block">
	<h3 class="ttl2">Полезная информация</h3>
	<ul class="list item-list">
		<?foreach($arResult["ITEMS"] as $arItem){
		$file = CFile::ResizeImageGet($arItem['PREVIEW_PICTURE']["ID"], array('width'=>35, 'height'=>25), BX_RESIZE_IMAGE_PROPORTIONAL, true);
		?>
		<li><a href="<?=$arItem["DETAIL_PAGE_URL"]?>" style="background-image:url('<?=$file['src']?>');"><?=$arItem["NAME"]?></a></li>
		<?}?>
	</ul>
</div>
<?}?>
