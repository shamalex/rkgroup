<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>
<? if(!empty($arResult["ITEMS"])): ?>
	<!-- promo -->
	<section class="promo c">
		<!-- slides -->
		<div class="slides c" id="promo">
			<?foreach($arResult["ITEMS"] as $arItem):?>
				<a class="item" style="background-image:url('<?=$arItem["DISPLAY_PROPERTIES"]["MAIN_BANNER"]["FILE_VALUE"]["SRC"]?>');" href="<?=$arItem["DETAIL_PAGE_URL"]?>"></a>
			<?endforeach;?>
		</div>
		<!-- / slides end -->
		
		<!-- slides nav -->
		<ul class="pr-nav c" id="promo-nav">
			<?foreach($arResult["ITEMS"] as $arItem):?>
			<li class="<?=$arItem["DISPLAY_PROPERTIES"]["PREVEW_BG"]["VALUE_XML_ID"]?>"><span class="i-pr-arr">&nbsp;</span><?= $arItem['NAME'] ?></li>
			<?endforeach;?>
		</ul>
		<!-- / slides nav end -->
	</section>
	<!-- / promo end -->
<? endif; ?>
