<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>

<? if(!empty($arResult["ITEMS"])): ?>
	<!-- roll -->
	<div class="roll pop-cat">
		<h2 class="ttl-r">популярные товарные категории</h2>
		<!-- items -->
		<div class="items c">
			<?foreach($arResult["ITEMS"] as $arItem):?>
				<!-- item -->
				<a href="<?= $arItem['CAT']['SECTION_PAGE_URL'] ?>" class="item c-block">
					<div class="pic"<? if(!empty($arItem['CAT']['PICTURE']['SRC'])): ?> style="background-image:url('<?= $arItem['CAT']['PICTURE']['SRC'] ?>');"<? endif; ?>></div>
					<h3 class="ttl"><?= $arItem['CAT']['NAME'] ?></h3>
				</a>
				<!-- / item -->
			<?endforeach;?>
		</div>
		<!-- / items -->
	</div>	
	<!-- / roll end -->
<? endif; ?>
