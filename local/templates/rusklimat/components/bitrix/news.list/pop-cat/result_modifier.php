<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

CModule::IncludeModule('iblock');

$CATALOG_IBLOCK_ID = 8;

$IDS = [];

foreach($arResult["ITEMS"] as $i=>$arItem) {
	$ID = intval($arItem['EXTERNAL_ID']);
	$IDS[$ID] = $i;
}

if (!empty($IDS)) {

	$arFilter = array('IBLOCK_ID' => $CATALOG_IBLOCK_ID, 'ID' => array_keys($IDS));
	$rsSections = CIBlockSection::GetList(false, $arFilter, false, ['ID', 'NAME', 'SECTION_PAGE_URL', 'PICTURE'], ['nTopCount'=>4]);
	while ($arSection = $rsSections->GetNext()) {
		if (!empty($arSection['PICTURE'])) {
			$arSection['PICTURE'] = CFile::GetFileArray($arSection['PICTURE']);
		}
		$arResult["ITEMS"][$IDS[$arSection['ID']]]['CAT'] = $arSection;
	}
}

foreach($arResult["ITEMS"] as $i=>$arItem) {
	if (empty($arItem['CAT'])) {
		unset($arResult["ITEMS"][$i]);
	}
}