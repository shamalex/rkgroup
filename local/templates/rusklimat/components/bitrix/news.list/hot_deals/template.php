<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);

?>

<? if(!empty($arResult["ITEMS"])): ?>
	<?if($arParams["NEWS_COUNT"]>1){?>
	<!-- hot deals block -->
	<div class="roll">
		<h2 class="ttl-r"><?= !empty($GLOBALS['HOT_DEALS_ROLL_NAME']) ? $GLOBALS['HOT_DEALS_ROLL_NAME'] : "Успей купить" ?></h2>
		<div class="hot-deals c">

			<!-- slides -->
			<div class="slides c" id="<?= !empty($GLOBALS['HOT_DEALS_BLOCK_ID']) ? $GLOBALS['HOT_DEALS_BLOCK_ID'] : 'specoff' ?>">
	<?}?>
				<?foreach($arResult["ITEMS"] as $arItem):?>
					<?
					//p($arItem["DISPLAY_PROPERTIES"]);
					$this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_EDIT"));
					$this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_DELETE"), array("CONFIRM" => GetMessage('CT_BNL_ELEMENT_DELETE_CONFIRM')));
					?>
					<!-- item w/timer -->
					<div class="item b-base" id="<?=$this->GetEditAreaId($arItem['ID']);?>">
						<div class="w c">

							<div class="timer">
								<? if(!empty($arItem["DISPLAY_PROPERTIES"]["ITEM"]["LINK_ELEMENT_VALUE"]["PRICES"][$arParams['GEO_DATA']['PRICEBUSH_ID'].'_NODISCOUNT']["PRINT_VALUE"])): ?>
									<?
									$diff = $arItem["DISPLAY_PROPERTIES"]["ITEM"]["LINK_ELEMENT_VALUE"]["PRICES"][$arParams['GEO_DATA']['PRICEBUSH_ID']]["VALUE"] - $arItem["DISPLAY_PROPERTIES"]["ITEM"]["LINK_ELEMENT_VALUE"]["PRICES"][$arParams['GEO_DATA']['PRICEBUSH_ID'].'_NODISCOUNT']["VALUE"];
									$diff = ceil($diff);
									$diff = abs($diff);
									?>
									<div class="sum">Успей сэкономить <?= number_format($diff, 0, '.', ' ') ?> <?= getNumEnding($diff, ['рубль', 'рубля', 'рублей']) ?></div>
								<? else: ?>
									<div class="sum">Успей купить</div>
								<? endif; ?>
									<div class="time" data-countdown="<?echo $arItem["DISPLAY_ACTIVE_TO"]?>"></div>
								</div>

							<a class="lnk" href="<?= $arItem["DISPLAY_PROPERTIES"]["ITEM"]["LINK_ELEMENT_VALUE"]["DETAIL_PAGE_URL"] ?>">
								<div class="ttl"><?= !empty($arItem["DISPLAY_PROPERTIES"]["TEXT_TOP"]["DISPLAY_VALUE"]) ? $arItem["DISPLAY_PROPERTIES"]["TEXT_TOP"]["DISPLAY_VALUE"] : $arItem["DISPLAY_PROPERTIES"]["ITEM"]["LINK_ELEMENT_VALUE"]["NAME"] ?></div>
									<div class="pic"<? if(!empty($arItem["DISPLAY_PROPERTIES"]["ITEM"]["LINK_ELEMENT_VALUE"]["PROPERTY_PICTURE1_VALUE"])): ?> style="background-image:url('<?= $arItem["DISPLAY_PROPERTIES"]["ITEM"]["LINK_ELEMENT_VALUE"]["PROPERTY_PICTURE1_VALUE"] ?>');"<? endif; ?>>
										<div class="ico-hot">Успей<br/>купить</div>
									</div>
									<div class="dsc"><?= !empty($arItem["DISPLAY_PROPERTIES"]["TEXT_BOTTOM"]["DISPLAY_VALUE"]) ? $arItem["DISPLAY_PROPERTIES"]["TEXT_BOTTOM"]["DISPLAY_VALUE"] : '' ?></div>
								</a>

							<div class="prc">
								<div class="curr"><?= $arItem["DISPLAY_PROPERTIES"]["ITEM"]["LINK_ELEMENT_VALUE"]["PRICES"][$arParams['GEO_DATA']['PRICEBUSH_ID']]["PRINT_VALUE"] ?></div>
								<? if(!empty($arItem["DISPLAY_PROPERTIES"]["ITEM"]["LINK_ELEMENT_VALUE"]["PRICES"][$arParams['GEO_DATA']['PRICEBUSH_ID'].'_NODISCOUNT']["PRINT_VALUE"])): ?>
									<div class="past">
										<span class="old"><?= number_format($arItem["DISPLAY_PROPERTIES"]["ITEM"]["LINK_ELEMENT_VALUE"]["PRICES"][$arParams['GEO_DATA']['PRICEBUSH_ID'].'_NODISCOUNT']["VALUE"], 0, '.', ' ') ?></span> <span class="profit"><?= number_format($diff, 0, '.', ' ') ?> выгода</span>
									</div>
								<? endif; ?>
							</div>
							<a class="btn btn-to-bsk" href="<?= $arItem["DISPLAY_PROPERTIES"]["ITEM"]["LINK_ELEMENT_VALUE"]["DETAIL_PAGE_URL"] ?>?action=ADD2BASKET&add_id=<?= $arItem["DISPLAY_PROPERTIES"]["ITEM"]["LINK_ELEMENT_VALUE"]["ID"] ?>" id="hot_deal_<?= $arItem["ID"] ?>_add">Купить</a>

							<?
							$cats = [];
							foreach($arItem["DISPLAY_PROPERTIES"]["ITEM"]["LINK_ELEMENT_VALUE"]['PATH'] as $arSection) {
								$cats[] = $arSection['NAME'];
							}
							?>
							<script>
								$('#hot_deal_<?= $arItem["ID"] ?>_add').on('click', function() {
									window.dataLayer = window.dataLayer || [];
									window.dataLayer.push({
										'event': 'addToCart',
										"ecommerce": {
											"currencyCode": "RUB",
											"add": {
												"products": [
													{
														"id": "<?= $arItem["ID"] ?>",
														"name" : "<?= $arItem["NAME"] ?>",
														"price": <?= number_format($arItem["DISPLAY_PROPERTIES"]["ITEM"]["LINK_ELEMENT_VALUE"]["PRICES"][$arParams['GEO_DATA']['PRICEBUSH_ID']]["VALUE"], 2, '.', '') ?>,
														"brand": "<?= $arItem["DISPLAY_PROPERTIES"]["ITEM"]["LINK_ELEMENT_VALUE"]["PROPERTY_EL_BRAND_NAME"] ?>",
														"category": "<?= implode('/', $cats) ?>",
														"quantity": 1
													}
												]
											}
										}
									});
								});
							</script>

						</div>
					</div>
					<!-- / item w/timer -->
				<?endforeach;?>
	<?if($arParams["NEWS_COUNT"]>1){?>
			</div>
			<!-- slides -->
		</div>
		<!-- / hot deals item container end -->
	</div>
	<!-- / hot deals roll end -->
	<?}?>
<? endif; ?>
