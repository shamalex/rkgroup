<?

CModule::IncludeModule('iblock');
CModule::IncludeModule('catalog');

foreach($arResult["ITEMS"] as $i=>$arItem) {
	
	$arItem["DISPLAY_ACTIVE_TO"] = CIBlockFormatProperties::DateFormat($arParams["ACTIVE_DATE_FORMAT"], MakeTimeStamp($arItem["DATE_ACTIVE_TO"], CSite::GetDateFormat()));
	
	$arFilter = Array("IBLOCK_ID"=>$arItem["DISPLAY_PROPERTIES"]["ITEM"]["LINK_IBLOCK_ID"], "ID"=>$arItem["DISPLAY_PROPERTIES"]["ITEM"]["VALUE"]);
	$arSelect = Array("NAME", "DETAIL_PAGE_URL", "IBLOCK_SECTION_ID", "PROPERTY_picture1_ru_new", "PROPERTY_EL_BRAND.NAME", "CATALOG_GROUP_".$arParams['GEO_DATA']['PRICE_ID'], "CATALOG_GROUP_".$arParams['GEO_DATA']['PRICE_NODISCOUNT_ID']);
	$arLimit = Array('nTopCount'=>1);
	$res = CIBlockElement::GetList(Array(), $arFilter, false, $arLimit, $arSelect);  
	if ($obElement = $res->GetNextElement()) {
		
		$arFields = $obElement->GetFields();
		
		if (!empty($arFields['PROPERTY_PICTURE1_RU_NEW_VALUE'])) {
			// $arFields['PROPERTY_PICTURE1_VALUE'] = CFile::GetFileArray($arFields['PROPERTY_PICTURE1_VALUE']);
			$arFields['PROPERTY_PICTURE1_VALUE'] = CFile::ResizeImageGet($arFields['PROPERTY_PICTURE1_RU_NEW_VALUE'], array('width'=>296, 'height'=>170), BX_RESIZE_IMAGE_PROPORTIONAL, false);
			$arFields['PROPERTY_PICTURE1_VALUE'] = $arFields['PROPERTY_PICTURE1_VALUE']['src'];
		}
		
		$arFields["PATH"] = array();
		$rsPath = CIBlockSection::GetNavChain($arResult["IBLOCK_ID"], $arFields["IBLOCK_SECTION_ID"]);
		// $rsPath->SetUrlTemplates("", $arParams["SECTION_URL"]);
		while($arPath = $rsPath->GetNext())
		{
			$ipropValues = new \Bitrix\Iblock\InheritedProperty\SectionValues($arParams["IBLOCK_ID"], $arPath["ID"]);
			$arPath["IPROPERTY_VALUES"] = $ipropValues->getValues();
			$arFields["PATH"][] = $arPath;
		}
		
		$arResultPrices = CIBlockPriceTools::GetCatalogPrices($arItem["DISPLAY_PROPERTIES"]["ITEM"]["LINK_IBLOCK_ID"], [$arParams['GEO_DATA']['PRICEBUSH_ID'], $arParams['GEO_DATA']['PRICEBUSH_ID'].'_NODISCOUNT']);
		$arConvertParams = ['CONVERT_CURRENCY'=>'N', 'CURRENCY_ID'=>''];
		$arFields["PRICES"] = CIBlockPriceTools::GetItemPrices($arItem["DISPLAY_PROPERTIES"]["ITEM"]["LINK_IBLOCK_ID"], $arResultPrices, $arFields, false, $arConvertParams);
		
		$arItem["DISPLAY_PROPERTIES"]["ITEM"]["LINK_ELEMENT_VALUE"] = $arFields;
	}
	
	
	
	$arResult["ITEMS"][$i] = $arItem;
	
	
}