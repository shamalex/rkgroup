<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);?>

<?if(strlen($arResult["ERROR_MESSAGE"])>0)
	ShowError($arResult["ERROR_MESSAGE"]);
?>
<?$arPlacemarks=array();?>
<?if(is_array($arResult["ITEMS"]) && !empty($arResult["ITEMS"])):?>
	<?foreach($arResult["ITEMS"] as $arItem):?>
		<?foreach($arItem["PROPERTIES"] as $pid=>$arProperty):?>
			<?
			if($pid=='place' && !empty($arProperty['VALUE']))
			{
				$gps = explode(',', $arProperty['VALUE']);
				$gpsN=substr(doubleval($gps[0]),0,15);
				$gpsS=substr(doubleval($gps[1]),0,15);
				$arPlacemarks[]=array("LON"=>$gpsS,"LAT"=>$gpsN,"TEXT"=>$arItem["PROPERTIES"]['address']['VALUE'],"ID"=>$this->GetEditAreaId($arItem['ID']));
			}
			?>
		<?endforeach;?>
	<?endforeach;?>
<?endif;?>
<? if(!empty($arPlacemarks)): ?>
	<?
	$APPLICATION->IncludeComponent(
		"bitrix:map.google.view",
		".default",
		array(
			"INIT_MAP_TYPE" => "ROADMAP",
			"MAP_DATA" => serialize(array("google_lat"=>$gpsN,"google_lon"=>$gpsS,"google_scale"=>11,"PLACEMARKS"=>$arPlacemarks)),
			"MAP_WIDTH" => "1110",
			"MAP_HEIGHT" => "430",
			"CONTROLS" => array(
				0 => "SMALL_ZOOM_CONTROL",
				//1 => "SCALELINE",
			),
			"OPTIONS" => array(
				0 => "ENABLE_DBLCLICK_ZOOM",
				1 => "ENABLE_DRAGGING",
				//2 => "ENABLE_KEYBOARD",
				//3 => "ENABLE_SCROLL_ZOOM",
			),
			"MAP_ID" => "",
			"COMPONENT_TEMPLATE" => ".default"
		),
		false
	);
	?>
<? endif; ?>

<?
$MAIN_CITY = $arResult['GEO']['PROPERTIES']['network_name']['VALUE'];
$MAIN_CITY = str_replace('Русклимат ', '', $MAIN_CITY);
?>
<?$APPLICATION->IncludeFile(SITE_TEMPLATE_PATH."/include/shops.php", Array(), Array("MODE"=>"php"));?>
<!-- contacts -->
<div class="contacts">

	<?
	/*
	<!-- item -->
	<div class="item">
		<!-- item .w-->
		<div class="w c">
			<div class="pic" style="background-image:url('/bitrix/templates/rusklimat/i/blocks/contact3.png');"></div>
			<div class="dsc">
				<h2 class="ttl c"><div class="ico ico-1"></div>Интернет-магазин "Русклимат"</h2>
				<div class="inf">
					<!-- <p>...</p> -->
				</div>
				<table class="tbl-contact">
					<tr>
						<td class="col-1">
							&nbsp;
						</td>
						<td class="col-2">
							<p class="tl">График работы</p>
							<p>ежедневно: 9:00 – 20:00</p>
						</td>
						<td>
							<p class="tl">Телефон для заказов</p>
							<p>8 (800) 777-19-77,<br>звонок бесплатный </p>
						</td>
					</tr>
					<tr>
						<td class="col-1">
							<p class="tl">E-mail</p>
							<p><a itemprop="email" href="mailto:online@rusklimat.ru">online@rusklimat.ru</a></p>
						</td>
						<td class="col-2">
							<p class="tl">Способы оплаты</p>
							<p>наличные, перевод <br>для физических и юридических лиц</p>
						</td>
						<td>
							&nbsp;
						</td>
					</tr>
				</table>
			</div>
			<!-- dsc end -->
		</div>
		<!-- / item .w -->
	</div>
	<!-- / item -->
	*/
	?>

	<?
	$icons = ['ico-1', 'ico-3', 'ico-2'];
	?>
	<?foreach($arResult["ITEMS"] as $i=>$arItem):?>
	<?
	$this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_EDIT"));
	$this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_DELETE"), array("CONFIRM" => GetMessage('CT_BNL_ELEMENT_DELETE_CONFIRM')));
	?>
	<!-- item -->
	<div class="item" id="<?=$this->GetEditAreaId($arItem['ID']);?>">
		<!-- item .w-->
		<div class="w c">
			<?/*<div class="pic" style="background-image:url('/bitrix/templates/rusklimat/i/blocks/contact3.png');"></div>*/?>
			<div class="dsc">
				<?
				$icon = $icons[$i%count($icons)];
				?>
				<h2 class="ttl c"><?/*<div class="ico <?= $icon ?>"></div>*/?><?= $arItem['NAME'] ?></h2>
				<div class="inf">
					<!-- <p>...</p> -->
				</div>
				<table class="tbl-contact">
					<tr>
						<td class="col-1">
							<? if(!empty($arItem['DISPLAY_PROPERTIES']['address']['VALUE'])): ?>
								<p class="tl">Адрес</p>
								<p><?= $MAIN_CITY?>, <?= $arItem['DISPLAY_PROPERTIES']['address']['VALUE'] ?></p>
							<? else: ?>
								&nbsp;
							<? endif; ?>
						</td>
						<td class="col-2">
							<p class="tl">Телефон для заказов</p>
							<p>8 (800) 777-19-77,<br>звонок бесплатный </p>
						</td>
						<td>
							<p class="tl">E-mail</p>
							<p><a itemprop="email" href="mailto:online@rusklimat.ru">online@rusklimat.ru</a></p>
						</td>
					</tr>
				</table>
			</div>
			<!-- dsc end -->
		</div>
		<!-- / item .w -->
	</div>
	<!-- / item -->
	<?endforeach;?>
</div>
<!-- / contacts end -->
