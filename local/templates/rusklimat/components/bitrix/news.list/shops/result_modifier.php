<?

$arResult['GEO'] = [];
global $arGeoData;

$arSort = array();
$arFilter = Array("IBLOCK_ID"=>15, "ID"=>$arGeoData['CUR_CITY']['ID']);
$arSelect = false;
$res = CIBlockElement::GetList($arSort, $arFilter, false, false, $arSelect);  
if ($ob = $res->GetNextElement()) {
	$arResult['GEO'] = $ob->GetFields();
	$arResult['GEO']['PROPERTIES'] = $ob->GetProperties();
}