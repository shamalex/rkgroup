<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>


<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<?if(strlen($arResult["ERROR_MESSAGE"])>0)
	ShowError($arResult["ERROR_MESSAGE"]);
?>
<?$arPlacemarks=array();?>
<?if(is_array($arResult["ITEMS"]) && !empty($arResult["ITEMS"])):?>
	<?foreach($arResult["ITEMS"] as $arItem):?>
		<?foreach($arItem["PROPERTIES"] as $pid=>$arProperty):?>
			<?
			if($pid=='place' && !empty($arProperty['VALUE']))
			{
				$gps = explode(',', $arProperty['VALUE']);
				$gpsN=substr(doubleval($gps[0]),0,15);
				$gpsS=substr(doubleval($gps[1]),0,15);
				$arPlacemarks[]=array("LON"=>$gpsS,"LAT"=>$gpsN,"TEXT"=>$arItem["PROPERTIES"]['address']['VALUE']);
			}
			?>
		<?endforeach;?>
	<?endforeach;?>
<?endif;?>
<? if(!empty($arPlacemarks)): ?>
	<?
	$APPLICATION->IncludeComponent(
		"bitrix:map.google.view",
		".default",
		array(
			"INIT_MAP_TYPE" => "ROADMAP",
			"MAP_DATA" => serialize(array("google_lat"=>$gpsN,"google_lon"=>$gpsS,"google_scale"=>11,"PLACEMARKS"=>$arPlacemarks)),
			"MAP_WIDTH" => "672",
			"MAP_HEIGHT" => "430",
			"CONTROLS" => array(
				0 => "SMALL_ZOOM_CONTROL",
				//1 => "SCALELINE",
			),
			"OPTIONS" => array(
				0 => "ENABLE_DBLCLICK_ZOOM",
				1 => "ENABLE_DRAGGING",
				//2 => "ENABLE_KEYBOARD",
			),
			"MAP_ID" => "",
			"COMPONENT_TEMPLATE" => ".default"
		),
		false
	);
	?>
<? endif; ?>
