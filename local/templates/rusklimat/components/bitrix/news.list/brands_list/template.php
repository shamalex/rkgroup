<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>
<? if(!empty($arResult["ITEMS"])): ?>
	<!-- brand logos slider roll -->
	<div class="roll">
		<h2 class="ttl-r"><?= $arParams['BLOCK_TITLE'] ?> по производителям</h2>
		<!-- brands logos slider -->	
		<div class="brands-logos-slider c">
			<!-- news slides -->
			<div class="slides c" id="brandsLogoSlider">
		 
				<?foreach($arResult["ITEMS"] as $arItem):?>
					<!-- item -->
					<a class="item brand_filter_link" href="#" id="brand_filter_<?= $arItem["ID"]?>">
						<?/*<div class="ttl"><?echo $arItem["NAME"]?></div>*/?>
						<div class="pic" <? if(is_array($arItem["PREVIEW_PICTURE"])): ?> style="background-image:url('<?= $arItem["PREVIEW_PICTURE"]["SRC"] ?>');" <? endif; ?>></div>
					</a>
					<!-- / item -->
				<?endforeach;?>
			</div>
			<!-- / slides end -->
		</div>
		<!-- / brands logos slider end -->
		
		
	</div>
	<!-- / brand logos slider roll end -->
<? endif; ?>
