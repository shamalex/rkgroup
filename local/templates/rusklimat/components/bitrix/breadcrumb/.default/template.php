<?php
if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

/**
 * @global CMain $APPLICATION
 */

global $APPLICATION;

//delayed function must return a string
if(empty($arResult))
	return "";

$strReturn = '';

$HIDE_BREADCRUMS = $APPLICATION->GetProperty("HIDE_BREADCRUMS", "N") == 'Y';

if (!$HIDE_BREADCRUMS) {
	$strReturn .= '<!-- breadcrubms -->';
	$strReturn .= "\n";
	$strReturn .= '<div class="breadcrumbs c">';
	$strReturn .= '<ul>';

	$itemSize = count($arResult);
	for($index = 0; $index < $itemSize; $index++)
	{
		$title = htmlspecialcharsex($arResult[$index]["TITLE"]);
		
		if ($title == '') {
			continue;
		}

		$nextRef = ($index < $itemSize-2 && $arResult[$index+1]["LINK"] <> ""? ' itemref="bx_breadcrumb_'.($index+1).'"' : '');
		$child = ($index > 0? ' itemprop="child"' : '');

		if($arResult[$index]["LINK"] <> "" && $index != $itemSize-1)
		{
			$strReturn .= '<li class="bx-breadcrumb-item" id="bx_breadcrumb_'.$index.'" itemscope="" itemtype="http://data-vocabulary.org/Breadcrumb"'.$child.$nextRef.'"><a href="'.$arResult[$index]["LINK"].'" title="'.$title.'" itemprop="url"><font itemprop="title">'.$title.'</font></a></li>';
		}
		else
		{
			$strReturn .= '<li><span>'.$title.'</span></li>';
		}
	}

	$strReturn .= '</ul>';
	$strReturn .= '</div>';
	$strReturn .= "\n";
	$strReturn .= '<!-- / breadcrumbs end -->';
}

return $strReturn;
