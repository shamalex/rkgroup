<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
$obCache_trade = new CPHPCache();
$CACHE_ID = 'CACHE_SERVICES_CATALOG';
if ( $obCache_trade->InitCache($arParams['CACHE_TIME'], $CACHE_ID, '/') )
{
	$arResult = $obCache_trade->GetVars();
}
else
{
	
	$obCache_trade->StartDataCache();
	CModule::IncludeModule('iblock');
	/*echo "<pre>";print_r($arParams);echo "</pre>";*/

	$arResult = array();
	/*$res = CIBlockElement::GetList(array("SORT" => "ASC"), array("IBLOCK_ID" => $arParams['IBLOCK_CITY_ID'], "ACTIVE" => "Y","ACTIVE_DATE"=>"Y","CODE"=>$arParams['CITY_CODE']), false, false, array("ID", "NAME", "PREVIEW_TEXT","PREVIEW_PICTURE","DETAIL_PAGE_URL","PROPERTY_DATE"));
    if($arFields = $res->GetNext()) {
    	$arResult['CITY']=$arFields
    }*/
    global $arGeoData;
    $arSections=array();
    $arCatalogSection=array();
    /*echo "<pre>";print_r($arParams);echo "</pre>";*/
    foreach ($arParams['SECTIONS'] as $k => $v) {
    	$arCatalogSection[]=$v['ID'];
    }
	/*echo $arGeoData['CUR_CITY'];*/
	$res = CIBlockElement::GetList(array("SORT" => "ASC"), array("IBLOCK_ID" => $arParams['IBLOCK_ID'], "ACTIVE" => "Y","ACTIVE_DATE"=>"Y","PROPERTY_CITIES"=>$arGeoData['CUR_CITY']['ID'],"PROPERTY_CATEGORY"=>$arCatalogSection), false, false, array("ID", "NAME", "PREVIEW_TEXT","PREVIEW_PICTURE","DETAIL_PAGE_URL","PROPERTY_DATE"));
    while($arFields = $res->GetNext()) {
		$arFields['PREVIEW_PICTURE'] = CFile::GetFileArray($arFields['PREVIEW_PICTURE']);
		$arFields['DETAIL_PICTURE'] = CFile::GetFileArray($arFields['DETAIL_PICTURE']);
		$arResult['SERVICES'][$arFields['ID']]= $arFields;
    }
    /*if(count($arSections)>0){
	    $arResult['SECTIONS']=array();
	    $arFilter = array('IBLOCK_ID' => $arParams['IBLOCK_ID'],"ID"=>$arSections);
		$rsSect = CIBlockSection::GetList(array('left_margin' => 'asc'),$arFilter);
		while ($arSect = $rsSect->GetNext())
		{
			$arResult['SECTIONS'][$arSect['ID']]=$arSect;
		}
	}*/
	
	// saving template name to cache array
	$arResult["__TEMPLATE_FOLDER"] = $this->__folder;
	
	// writing new $arResult to cache file
	$obCache_trade->EndDataCache($arResult);

}
$this->__component->arResult = $arResult; 
?>