<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>

<div class="roll">
<!-- subnav -->
		<div class="subnav brd c">
	
			<ul>
			<?foreach ($arResult['params']['DATE_ACTIVE_FROM']['years'] as $key => $value) {?>
				<li>
					<?if($value==$arResult['query']['DATE_ACTIVE_FROM']['year']){?><span><?=$value?></span>
					<?}else{?>
						<a href="?year=<?=$value?>"><?=$value?></a>
					<?}?>
				</li>
				<li>/</li>
			<?}?>
			</ul>
	
		</div>
		<!-- / subnav end -->

</div>