<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

CModule::IncludeModule("iblock");
/*echo "<pre>";print_r($arParams['PROPERTY_CODE']);echo "</pre>";*/
$filter = array();
foreach ($arParams['PROPERTY_CODE'] as $key => $value) {
	if($value!=""){
		if($value!="DATE_ACTIVE_FROM"&&$value!="DATE_ACTIVE_TO"&&$value!="ACTIVE_FROM"&&$value!="ACTIVE_TO"){
			$prop_value='PROPERTY_'.$value;
			$prop_value_val='PROPERTY_'.$value.'_VALUE';

		}else{
			$prop_value=$value;	
			$prop_value_val=$value;		
		}
		$obEl=CIBlockElement::GetList(
		 Array($prop_value=>"ASC"),
		 Array("IBLOCK_ID"=>$arParams["IBLOCK_ID"],"ACTIVE"=>"Y"),
		 false,
		 array("nTopCount"=>1),
		 Array("ID","NAME",$prop_value)
		);
		if($arEl=$obEl->GetNext())
		{
			
			$date=explode(" ",$arEl[$prop_value_val]);
			$time=$date[1];
			$date=$date[0];	
			$date=explode(".",$date);
			$arResult["params"][$value]["year_begin"]=$date[2];
		}
		$obEl=CIBlockElement::GetList(
		 Array($prop_value=>"DESC"),
		 Array("IBLOCK_ID"=>$arParams["IBLOCK_ID"],"ACTIVE"=>"Y"),
		 false,
		 array("nTopCount"=>1),
		 Array("ID","NAME",$prop_value)
		);
		if($arEl=$obEl->GetNext())
		{			
			$date=explode(" ",$arEl[$prop_value_val]);
			$time=$date[1];
			$date=$date[0];	
			$date=explode(".",$date);
			$arResult["params"][$value]["year_end"]=$date[2];
		}
		for($i=$arResult["params"][$value]["year_end"];$i>=$arResult["params"][$value]["year_begin"];$i--){
			if($value!="DATE_ACTIVE_FROM"&&$value!="DATE_ACTIVE_TO"&&$value!="ACTIVE_FROM"&&$value!="ACTIVE_TO"){
				$f['>='.$prop_value]=$i."-01-01";
				$f['<'.$prop_value]=($i+1)."-01-01";
			}else{
				$f['>='.$prop_value]='01.01.'.$i;
				$f['<'.$prop_value]='01.01.'.($i+1);
			}
			$obEl=CIBlockElement::GetList(
				 Array($prop_value=>"DESC"),
				 array_merge(Array("IBLOCK_ID"=>$arParams["IBLOCK_ID"],"ACTIVE"=>"Y"),$f),
				 false,
				 array("nTopCount"=>1),
				 Array("ID","NAME",$prop_value)
			);
			if($arEl=$obEl->GetNext())
			{
				$date=explode(" ",$arEl[$prop_value_val]);
				$time=$date[1];
				$date=$date[0];	
				$date=explode(".",$date);			
				$arResult["params"][$value]["years"][]=$date[2];
			}
		}

		if ($_GET["year"]!=""){

			$arResult['query'][$value]['year']=$_GET["year"];
			if($value!="DATE_ACTIVE_FROM"&&$value!="DATE_ACTIVE_TO"&&$value!="ACTIVE_FROM"&&$value!="ACTIVE_TO"){
				$filter['>='.$prop_value]=$arResult['query'][$value]['year']."-01-01";
				$filter['<'.$prop_value]=($arResult['query'][$value]['year']+1)."-01-01";
			}else{
				$filter['>='.$prop_value]='01.01.'.$arResult['query'][$value]['year'];
				$filter['<'.$prop_value]='01.01.'.($arResult['query'][$value]['year']+1);
			}
		}else{
			$arResult['query'][$value]['year']=$arResult["params"][$value]["years"][0];
			$filter['>='.$prop_value]="01.01.".$arResult['query'][$value]['year'];
			$filter['<'.$prop_value]="01.01.".($arResult['query'][$value]['year']+1);
		}
	}
}

//print_r($filter);
$FILTER_NAME = $arParams["FILTER_NAME"];

global $$FILTER_NAME;
//echo "<pre>";print_r($$FILTER_NAME);echo "</pre>";
if(!$$FILTER_NAME)$$FILTER_NAME=array();

$$FILTER_NAME=array_merge($filter,$$FILTER_NAME);
unset($$FILTER_NAME["year"]);
#print_r($$FILTER_NAME);
//echo "<pre>";print_r($$FILTER_NAME);echo "</pre>";


$this->__component->arResult = $arResult; 
?>