      $(window).load(function(){
 $('#type_rayon').unbind('change').bind('change',function(){
        // Выбранная опция
        var selected = $(this).find('option').eq(this.selectedIndex);
        var valSearch = selected.val();
        
        changeChosen('type_rayon', 'type_punkt','rayon',valSearch);
           
    });
 $('#type_punkt').unbind('change').bind('change',function(){
        var selected = $(this).find('option').eq(this.selectedIndex);
        var valSearch = selected.val();
        
        changeChosen('type_punkt', 'type_street','punkt',valSearch);

        
         $('#type_street [value=0]').attr("selected", "selected");  
         $('#type_housenum [value=0]').attr("selected", "selected"); 
        $('#type_street').trigger("chosen:updated");   
        $('#type_housenum').trigger("chosen:updated");   
    });
 
$('#type_street').unbind('change').bind('change',function(){
        var selected = $(this).find('option').eq(this.selectedIndex);
        var valSearch = selected.val();
        
        changeChosen('type_street', 'type_house','housenum',valSearch);
          $('#type_house [value=0]').attr("selected", "selected"); 
        $('#type_house').trigger("chosen:updated");   
    });
});

function changeChosen(this_id, next_id, getVal,valueS){
        $.get("/ajax/regionadress.php?"+getVal+"="+valueS, function(data){
             if(data){

                $('#'+next_id).html(data);
                $('#'+next_id).chosen({
                  '.chzn-select'           : {},
                  '.chzn-select-deselect'  : {allow_single_deselect:true},
                  '.chzn-select-no-single' : {disable_search_threshold:10},
                  '.chzn-select-no-results': {no_results_text:'Ничего не найдено!'},
                  '.chzn-select-width'     : {width:"95%"}
                });
                $("#"+next_id).attr('disabled', false).trigger("chosen:updated");
                 if(next_id=="type_punkt"){
                  if(fias_arr['FCITY']!=""){
                    $('#type_punkt [value='+fias_arr['FCITY']+']').attr("selected", "selected");  
                    $('#type_punkt').trigger("chosen:updated");
                    $('#type_punkt').change();
                  }   
                 }
                 if(next_id=="type_street"){
                  if(fias_arr['FSTREET']!=""){
                    $('#type_street [value='+fias_arr['FSTREET']+']').attr("selected", "selected");  
                    $('#type_street').trigger("chosen:updated");
                    $('#type_street').change();
                  }   
                 }
                if(next_id=="type_house"){
                  if(fias_arr['FHOUSE']!=""){
                    $('#type_house [value='+fias_arr['FHOUSE']+']').attr("selected", "selected");  
                    $('#type_house').trigger("chosen:updated");
                    $('#type_house').change();
                  }   

                 }
               if($('#'+next_id+' option').size()==2){
                        $('#'+next_id+' option').eq(1).attr("selected", "selected");
                     $("#"+next_id).attr('disabled', true).trigger("chosen:updated");

                      $('#'+next_id).change();
                    }else{
                if($('#'+next_id+' option').eq(1).html()=="-"){

                    $('#'+next_id+' option').eq(1).attr("selected", "selected");
        
                    if($('#'+next_id+' option').eq(2).html()==undefined){
                      $("#"+next_id).attr('disabled', true).trigger("chosen:updated");
                    }else{
                     $('#'+next_id).trigger("chosen:updated");     
                    }
                    
                      $('#'+next_id).change();
                }else{
       

                    $('#'+next_id).trigger("chosen:updated");    
                
                }
              }
             }
        });
    }