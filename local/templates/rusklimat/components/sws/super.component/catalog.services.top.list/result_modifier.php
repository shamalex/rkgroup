<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
$obCache_trade = new CPHPCache();
$CACHE_ID = 'CACHE_SERVICES_CATALOG';
if ( $obCache_trade->InitCache($arParams['CACHE_TIME'], $CACHE_ID, '/') )
{
	$arResult = $obCache_trade->GetVars();
}
else
{
	
	$obCache_trade->StartDataCache();
	CModule::IncludeModule('iblock');
	
	if(!$arParams["LIMIT"])$arParams["LIMIT"]=5;

	$arResult = array();
	$res = CIBlockElement::GetList(array("SORT" => "ASC"), array("IBLOCK_ID" => $arParams['IBLOCK_ID'], "ACTIVE" => "Y","ACTIVE_DATE"=>"Y","ID"=>$arParams['ID']), false, false, array("ID", "NAME", "PREVIEW_TEXT","PREVIEW_PICTURE","DETAIL_PAGE_URL","PROPERTY_DATE","IBLOCK_SECTION_ID"));
    if($arFields = $res->GetNext()) {
    	$arResult['SERVICE']=$arFields;
    }

    global $arGeoData;
    $arSections=array();
    $arCatalogSection=array();
   
	$filter=array("IBLOCK_ID" => $arParams['IBLOCK_ID'],"!ID"=>$arParams['ID'], "ACTIVE" => "Y","ACTIVE_DATE"=>"Y","PROPERTY_CITIES"=>$arGeoData['CUR_CITY']['ID'],"SECTION_ID"=>$arResult['SERVICE']['IBLOCK_SECTION_ID']);
	if($arParams["FILTER_NAME"]){
		global $$arParams["FILTER_NAME"];
		if(is_array($$arParams["FILTER_NAME"])) 
			$filter=array_merge($filter,$$arParams["FILTER_NAME"]);
	}

	$res = CIBlockElement::GetList(array("SORT" => "ASC"), $filter, false, array('nPageSize'=>$arParams["LIMIT"]), array("ID", "NAME", "PREVIEW_TEXT","PREVIEW_PICTURE","DETAIL_PAGE_URL","PROPERTY_DATE","DATE_ACTIVE_FROM","DETAIL_TEXT"));
    while($arFields = $res->GetNext()) {
		$arFields['PREVIEW_PICTURE'] = CFile::GetFileArray($arFields['PREVIEW_PICTURE']);
		$arFields['DETAIL_PICTURE'] = CFile::GetFileArray($arFields['DETAIL_PICTURE']);
		
        $arResult['SERVICES'][$arFields['ID']]= $arFields;
    }
/*    echo "<pre>";print_r($arResult);echo "</pre>";*/

	// saving template name to cache array
	$arResult["__TEMPLATE_FOLDER"] = $this->__folder;
	
	// writing new $arResult to cache file
	$obCache_trade->EndDataCache($arResult);

}
$this->__component->arResult = $arResult; 
?>