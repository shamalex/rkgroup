<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
?>
<div class="search-page">
<!-- srch-res -->
<div class="srch-res">
	<form class="srch-form c" id="formSearch" action="" method="get">
		<div class="srch-holder">
			<input class="srch-field srch-res-inp" placeholder="например: Мобильный кондиционер Ballu"  name="q" value="<?=$arResult["REQUEST"]["QUERY"]?>" id="searchAutocompleteResults" type="text"/>
			<input type="submit" class="srch-btn" value="" />
			<input type="hidden" name="p" value="1" />
			<!-- autocomplete is dynamically generated, hd.less > .autocomplete-suggestions -->
		</div>
		<fieldset class="checkset inline">
			<div class="checkline">
				<input type="radio" id="srchInName" name="where" value="name" <?if($arParams["COVERAGE"] == 'name'):?>checked<?endif;?>>
				<label for="srchInName">искать фрагмент текста только в наименованиях товаров</label>
			</div>
			<div class="checkline">
				<input type="radio" id="srchInDesc" name="where" value="all" <?if($arParams["COVERAGE"] != 'name'):?>checked<?endif;?>>
				<label for="srchInDesc">искать фрагмент текста в описаниях и характеристиках</label>
			</div>
		</fieldset>
	</form>
</div>

<!-- / srch-res end -->
<?if($arResult && $arResult["NavRecordCount"]):?>
<h2 class="ttl2">Совпадений по запросу <span class="marker">“<?=$arResult["REQUEST"]["QUERY"]?>”</span> <?echo GetMessage("CT_BSP_FOUND")?>: <span class="srch-res-nmbr" id="srchResNmbr"><?echo $arResult["NavRecordCount"];?></span></h2>
<? endif; ?>
<div class="brd tbl-w">
	<!-- tbl-srch-res -->
	<?if($arResult["REQUEST"]["QUERY"] === false):?>
	<?elseif($arResult["ERROR_CODE"]!=0):?>
		<p><?=GetMessage("CT_BSP_ERROR")?></p>
		<?ShowError($arResult["ERROR_TEXT"]);?>
		<p><?=GetMessage("CT_BSP_CORRECT_AND_CONTINUE")?></p>
	<?elseif(count($arResult["SEARCH"])>0):?>
		<table class="tbl tbl-hover tbl-lt tbl-srch-res" id="tblResult">
			<tr>
				<th class="col1">Изображение</th>
				<th class="col2">Наименование товара</th>
				<th class="col3">Цена</th>
				<th class="col4">Наличие</th>
				<th class="col5">совпадение</th>
			</tr>
			<tr class="sp">
				<td colspan="5">&nbsp;</td>
			</tr>
			<?foreach($arResult["SEARCH"] as $arItem):?>
			<? if(!$arItem['IS_PRODUCT']):?>
			<tr>
				<td colspan="4">
					<a href="<?echo $arItem["DETAIL_PAGE_URL"]?>"><?echo $arItem["NAME_FORMATED"]?></a>
				</td>
				<td class="td-where">
					<?if ($arItem["NAME_FORMATED"] == $arItem["NAME"]): ?>
						<span class="marker">в содержимом</span>
					<? else : ?>
						&nbsp;
					<? endif; ?>
				</td>
			</tr>
			<? else :?>
			<tr>
				<!-- <? var_dump($arItem['Ql'],$arItem['Qr']);?>-->
				<td class="td-pic">
					<? if($arItem['IMG']): ?>
					<a href="<?echo $arItem["DETAIL_PAGE_URL"]?>" class="pic" style="background-image:url('<?echo $arItem["IMG"]?>');"></a>
					<?/*  else :  */?>
					<? endif; ?>
				</td>
				<td class="td-name">
					<a href="<?echo $arItem["DETAIL_PAGE_URL"]?>"><?echo $arItem["NAME_FORMATED"]?></a>
				</td>
				<td class="td-price">
					<? if($arItem['PRICE_VALUE'] || $arItem['OLD_PRICE_VALUE']): ?>
						<? if($arItem['PRICE_VALUE']):?>
							<span class="curr"><?echo number_format($arItem["PRICE_VALUE"]["PRICE"], 0, ',', ' ');?> руб.</span>
						<? endif; ?>
						<? if($arItem['OLD_PRICE_VALUE']):?>
							<span class="old"><?echo number_format($arItem["OLD_PRICE_VALUE"]["PRICE"], 0, ',', ' ');?> руб.</span>
						<? endif; ?>
						<? if($arItem['PRICE_VALUE'] && $arItem['OLD_PRICE_VALUE']): ?>
							<br /><span class="profit"><?echo number_format($arItem["OLD_PRICE_VALUE"]["PRICE"]-$arItem["PRICE_VALUE"]["PRICE"], 0, ',', ' ');?> экономии</span>
						<? endif; ?>
					<? else : ?>
					<? endif; ?>
				</td>
				<td class="td-status">
					<?if($arItem['QUANITY'] === 1):?>
						<div class="status st-ok">в наличии</div>
					<?elseif($arItem['QUANITY'] === 2):?>
						<div class="status st-no">под заказ</div>
					<?elseif($arItem['QUANITY'] === 0):?>
						<div class="status st-no st-no-text"></div>
					<?endif;?>
				</td>
				<td class="td-where">
					<?if ($arItem["NAME_FORMATED"] == $arItem["NAME"]): ?>
						<span class="marker">в содержимом</span>
					<? else : ?>
						&nbsp;
					<? endif; ?>
				</td>
			</tr>
			<? endif; ?>
			<tr class="sp">
				<td colspan="5">&nbsp;</td>
			</tr>
			<?endforeach;?>
		</table>
		<div class="btn btn-more btn-g btn-disabled" id="nextPage" data-items="50">Показать еще</div>
	<?else:?>
		<?$APPLICATION->IncludeFile("include/search_error.php", Array(), Array("MODE"=>"html"));?>
	<?endif;?>
	<!-- / tbl-srch-res end -->
</div>
<!-- / brd end -->
</div>
