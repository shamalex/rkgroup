(function($){
	$(document).ready(function () {
		var tblResult = $('#tblResult'),
			formSearch = $('#formSearch'),
			btnNextPage = $('#nextPage'),
			onPageCnt = btnNextPage.data('items'),
			page = 0;
			loadNext = false;
			
		if(!(tblResult && formSearch && btnNextPage))
			return false;
			
		var loadNextPageResult = function(){
			var x = formSearch.serialize(),
				y = new RegExp(/&p=(\d+)&/);
				
			if(y.test(x)){
				page++;
				x = x.replace(/\&p=(\d+)\&/, '&p='+page+'&');
			}
			$.get('?'+x, function(data){
				var rows = $(data).find('#tblResult tr:gt(1)');
				if(rows.length)
					appendResult(rows);
				if(rows.length >= onPageCnt*2)
					btnEnable();
			});
			
			
		}
		
		var btnDisable = function(){
			loadNext = false;
			btnNextPage.prop('disabled', true).addClass('btn-disabled');
		}
		
		var btnEnable = function(){
			loadNext = true;
			btnNextPage.prop('disabled', false).removeClass('btn-disabled');
			
		}
		
		var appendResult = function(arr){
			$.each(arr, function(i){
				$(this).appendTo(tblResult).animate('show');
			});
			
		}
		btnNextPage.on('click', function(){
			if(!loadNext)
				return false;
			btnDisable();
			
			loadNextPageResult();
		});
		
		var rows = tblResult.find('tr:gt(1)');
		if(rows.length >= onPageCnt*2)
			btnEnable();
	});
})(jQuery);