<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/* set property QUANITY, IMAGE & PRICE */
if(is_array($arResult['SEARCH'])){
	foreach($arResult['SEARCH'] as $key => &$item){
		$item['IS_PRODUCT'] = true;
		$item["NAME_FORMATED"] = preg_replace('~('.$arResult["REQUEST"]["QUERY"].')~i', '<b>$1</b>', $item["NAME"]);
		if($item['PROPERTY_SM_PICTURE1_VALUE'])
			$item['IMG'] = CFile::GetPath($item['PROPERTY_SM_PICTURE1_VALUE']);

		$quantity = 0;
		$item['QUANITY'] =& $quantity;
		if((int)$item['PROPERTY_'.($arParams["QUANITY"]["local"]).'_VALUE'] > 0)
			$quantity = 1;
		elseif((int)$item['PROPERTY_'.($arParams["QUANITY"]["regional"]).'_VALUE'] > 0)
			$quantity = 2;
		unset($quantity);
		
		$item['Ql'] = $item['PROPERTY_'.($arParams["QUANITY"]["local"]).'_VALUE'];
		$item['Qr'] = $item['PROPERTY_'.($arParams["QUANITY"]["regional"]).'_VALUE'];

		$item['OLD_PRICE_VALUE'] = GetCatalogProductPrice($item['ID'], $arParams["PRICE"]["old"]);
		$item['PRICE_VALUE'] = GetCatalogProductPrice($item['ID'], $arParams["PRICE"]["now"]);
	}
}

?>