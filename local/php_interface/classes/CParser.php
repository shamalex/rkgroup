<?
function isJSON($string){
   return is_string($string) && is_array(json_decode($string, true)) && (json_last_error() == JSON_ERROR_NONE) ? true : false;
}
class CParser 
{
	/*Проверка существования нужных таблиц инициализация парсера*/
	function Install(){
		global $DB;
		$res=$DB->Query("SHOW TABLES LIKE 'parsers'");
		if(current($res->GetNext())!='parsers'){
			$DB->Query('
			CREATE TABLE IF NOT EXISTS `parsers` (
	  			`CODE` varchar(255) CHARACTER SET utf8 NOT NULL,
	  			`NAME` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
	  			`URL` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
	  			`STATUS` int(11) NOT NULL,
	  			PRIMARY KEY (`CODE`),
	  			UNIQUE KEY `CODE` (`CODE`)
			) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;');
			$DB->Query('
			CREATE TABLE IF NOT EXISTS `parsers_log` (
			  `ID` int(11) NOT NULL AUTO_INCREMENT,
			  `CODE` varchar(255) NOT NULL,
			  `ACTION` varchar(255) NOT NULL,
			  `DATE` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
			  `MESSAGE` text NOT NULL,
			  `DATA` text NOT NULL,
			  PRIMARY KEY (`ID`)
			) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;');
		}
	}
	/*Список парсеров в системе*/
	function GetList($filter=false,$limits=false){
		global $DB;
		$sql="SELECT * from `parsers`";
		if($filter){
			$filterv=array();
			foreach ($filter as $k => $v) {
				$filterv[] = "`".$k."` = '".$v."'";
			}
			$sql.=" where ".implode(" AND ", $filterv);
		}
		if($limits){
			$sql.=' LIMIT '.$limits[0].', '.$limits[1];
		}
		//echo $sql;
		return $DB->Query($sql);
	}
	/*Добавление лога*/
	function AddLog($parser_id,$action,$arParams){
		global $DB;
		$arParams['CODE']=$parser_id;
		$arParams['ACTION']=$action;
		foreach ($arParams as $key => $value) {
			$arParams[$key]="'".$DB->ForSql($value)."'";
			$n++;
		}
		$DB->Insert('parsers_log', $arParams, $err_mess);
		//p($arParams);
		return $err_mess;
	}
	/*Получение логов парсеров*/
	function GetLogs($filter=false,$limits=false,$sort=false){
		global $DB;
		$sql="SELECT * from `parsers_log`";
		if($filter){
			$filterv=array();
			foreach ($filter as $k => $v) {
				$filterv[] = "`".$k."` = '".$v."'";
			}
			$sql.=" where ".implode(" AND ", $filterv);
		}
		if($sort){
			$sortv=array();
			
			foreach ($sort as $key => $value) {
				$sortv[]='`'.$key.'` '.$value;
			}
			$sql.=' ORDER BY '.implode(', ',$sortv);
		}
		if($limits){
			$sql.=' LIMIT '.$limits[0].', '.$limits[1];
		}
		
		//echo $sql;
		return $DB->Query($sql);
	}
	/*Добавление парсера в таблицу парсеров*/
	function Add($parser_id,$arParams){
		global $DB;
		$arParams['CODE']=$parser_id;
		foreach ($arParams as $key => $value) {
			$arParams[$key]="'".$DB->ForSql($value)."'";
			$n++;
		}

		$DB->Insert('parsers', $arParams, $err_mess);
		CParser::AddLog($parser_id,'Add',array('MESSAGE'=>'Создание парсера','DATA'=>json_encode($arParams)));
		return $err_mess;
	}

	/*Создание временной таблице на основе 1 элемента данных*/
	function AddDataTable($parser_id,$data){
		$table_name='parsers_'.$parser_id.'_data';
		global $DB;
		$DB->Query("DROP TABLE IF EXISTS `".$table_name."`;");
		$sql="CREATE TABLE IF NOT EXISTS `".$table_name."` (
			`NUMBER` int(11) NOT NULL
			";

			foreach ($data as $k2 => $v2) {
				$sql.=",`".$k2."` text NOT NULL ";
			}


			$sql.="
			) ENGINE=InnoDB DEFAULT CHARSET=utf8;";
			CParser::AddLog($parser_id,'AddTempTable',array('MESSAGE'=>'Создание временной таблицы для данных','DATA'=>$sql));
			return $DB->Query($sql);
	}
	/*Добавление 1 Элемента данных во временную таблицу*/
	function InsertToDataTable($parser_id,$num,$arData){
		global $DB;
		$arFields=array('NUMBER'=>$num);
		$table_name='parsers_'.$parser_id.'_data';
		foreach ($arData as $key => $value) {
			if(is_array($value)){
				$value=json_encode($value);
			}
			$arFields[$key]="'".$DB->ForSql($value)."'";
		}
		$DB->Insert($table_name, $arFields, $err_mess);
		
		return $err_mess;
	}
	/*Множественное добавление данных*/
	function InsertsToDataTable($parser_id,$arData){
		$n=0;
		foreach ($arData as $k => $v) {
			$n++;
			CParser::InsertToDataTable($parser_id,$n,$v);
		}
		$filename=$parser_id.'_'.date('Y-m-d\TH-i');
		file_put_contents($_SERVER['DOCUMENT_ROOT'].'/upload/parser_files/'.$filename.'.json',json_encode($arData));
		CParser::AddLog($parser_id,'AddDataTempTable',array('MESSAGE'=>'Добавление данных в таблицу','DATA'=>json_encode(array('FILE'=>'/upload/parser_files/'.$filename.'.json','STUMP'=>md5(json_encode($arData))))));
	}
	/*Получение данных из временной таблицы*/
	function GetDataFromTable($parser_id,$filter=false,$limits=false,$sort=false){
		global $DB;
		$table_name='parsers_'.$parser_id.'_data';
		$sql="SELECT * from `".$table_name."`";
		if($filter){
			$filterv=array();
			foreach ($filter as $k => $v) {
				$filterv[] = "`".$k."` = '".$v."'";
			}
			$sql.=" where ".implode(" AND ", $filterv);
		}
		if($sort){
			$sortv=array();
			
			foreach ($sort as $key => $value) {
				$sortv[]=$key.' '.$value;
			}
			$sql.=' ORDER BY '.implode(', ',$sortv);
		}
		if($limits){
			$sql.=' LIMIT '.$limits[0].', '.$limits[1];
		}
		
		//echo $sql;
		return $DB->Query($sql);
	}
	/*Количество данных в таблице*/
	function GetDataCountTable($parser_id,$filter=false,$limits=false){
		global $DB;
		$table_name='parsers_'.$parser_id.'_data';
		$sql="SELECT count(*) from `".$table_name."`";
		if($filter){
			$filterv=array();
			foreach ($filter as $k => $v) {
				$filterv[] = "`".$k."` = '".$v."'";
			}
			$sql.=" where ".implode(" AND ", $filterv);
		}
		if($limits){
			$sql.=' LIMIT '.$limits[0].', '.$limits[1];
		}
		//echo $sql;
		$res=$DB->Query($sql);
		$result=$res->GetNext();
		return intval(current($result));
	}
	/*Получение типов цен*/
	function GetPriceTypes($filter){
		CModule::IncludeModule('catalog');
		$arPriceTypes=array();
		$dbPriceType = CCatalogGroup::GetList(array("SORT" => "ASC"),$filter);
		while ($arPriceType = $dbPriceType->Fetch())
		{
		    $arPriceTypes[$arPriceType['XML_ID']]=$arPriceType;
		}
		return $arPriceTypes;
	}
	function GetProductByXML($XML_ID){
			CModule::IncludeModule('iblock');
			$arSelect = Array("ID", "NAME", "DATE_ACTIVE_FROM");
			$arFilter = Array("IBLOCK_ID"=>IntVal(8), "ACTIVE_DATE"=>"Y", "ACTIVE"=>"Y","XML_ID"=>$XML_ID);
			$res = CIBlockElement::GetList(Array(), $arFilter, false, Array("nPageSize"=>1), $arSelect);
			if($arFields = $res->GetNext())
			{
			 return $arFields;
			}
		}
	function GetCustByXML($XML_ID){
		CModule::IncludeModule('iblock');
		$arSelect = Array("ID", "NAME", "DATE_ACTIVE_FROM");
		$arFilter = Array("IBLOCK_ID"=>IntVal(20), "ACTIVE_DATE"=>"Y", "ACTIVE"=>"Y","XML_ID"=>$XML_ID);
		$res = CIBlockElement::GetList(Array(), $arFilter, false, Array("nPageSize"=>1), $arSelect);
		if($arFields = $res->GetNext())
		{
		 return $arFields;
		}
	}
	function SetStatus($parser,$status){

	}
	function Start($parser){
		
	}
	function GetStatus($parser){

	}

	function get_web_page($url) {
		$res = array();
		$options = array( 
			CURLOPT_RETURNTRANSFER => true,	 // return web page 
			CURLOPT_HEADER		 => false,	// do not return headers 
			CURLOPT_FOLLOWLOCATION => true,	 // follow redirects 
			CURLOPT_USERAGENT	  => "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/45.0.2454.85 Safari/537.36", // who am i 
			CURLOPT_AUTOREFERER	=> true,	 // set referer on redirect 
			CURLOPT_CONNECTTIMEOUT => 120,	  // timeout on connect 
			CURLOPT_TIMEOUT		=> 120,	  // timeout on response 
			CURLOPT_MAXREDIRS	  => 10,	   // stop after 10 redirects 
		); 
		$ch	  = curl_init($url); 
		curl_setopt_array($ch, $options); 
		$content = curl_exec($ch); 
		$err	 = curl_errno($ch); 
		$errmsg  = curl_error($ch); 
		$header  = curl_getinfo($ch); 
		//p($err);
		curl_close($ch); 
		$res['content'] = $content;	 
		$res['url'] = $header['url'];
		return $res; 
	}
	function GetJSON($URL){
		$data= CParser::get_web_page($URL);
		$data = json_decode($data['content'], true);
		$err = json_last_error();
		if ($err == JSON_ERROR_NONE) {
			return $data;
		} else {
			switch (json_last_error()) {
		        case JSON_ERROR_NONE:
		            echo ' - Ошибок нет';
		        break;
		        case JSON_ERROR_DEPTH:
		            echo ' - Достигнута максимальная глубина стека';
		        break;
		        case JSON_ERROR_STATE_MISMATCH:
		            echo ' - Некорректные разряды или не совпадение режимов';
		        break;
		        case JSON_ERROR_CTRL_CHAR:
		            echo ' - Некорректный управляющий символ';
		        break;
		        case JSON_ERROR_SYNTAX:
		            echo ' - Синтаксическая ошибка, не корректный JSON';
		        break;
		        case JSON_ERROR_UTF8:
		            echo ' - Некорректные символы UTF-8, возможно неверная кодировка';
		        break;
		        default:
		            echo ' - Неизвестная ошибка';
		        break;
		    }
		}
	}
	
}
?>