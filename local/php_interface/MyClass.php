<?
AddEventHandler("main", "OnEpilog", Array("MyClass", "OnEpilogHandler"));
AddEventHandler("main", "OnBeforeUserAdd", Array("MyClass", "OnBeforeUserAddHandler"));
AddEventHandler("main", "OnAfterUserAdd", Array("MyClass", "OnAfterUserAddUpdateHandler"));
AddEventHandler("main", "OnBeforeUserUpdate", Array("MyClass", "OnBeforeUserUpdateHandler"));
AddEventHandler("sale", "OnOrderNewSendEmail", Array("MyClass", "OnOrderNewSendEmail"));
AddEventHandler("sale", "OnSaleCalculateOrderDelivery", Array("MyClass", "OnSaleCalculateOrderDeliveryHandler"));
AddEventHandler("sale", "OnSaleCalculateOrderPaySystem", Array("MyClass", "OnSaleCalculateOrderPaySystemHandler"));
AddEventHandler("sale", "OnOrderSave", Array("MyClass", "OnOrderSaveHandler"));
AddEventHandler('sale', 'OnBasketAdd', array('MyClass', 'OnBasketAddHandler'));
AddEventHandler('sale', 'OnBasketUpdate', array('MyClass', 'OnBasketUpdateHandler'));
AddEventHandler('sale', 'OnBasketDelete', array('MyClass', 'OnBasketDeleteHandler'));
AddEventHandler("catalog", "OnGetOptimalPrice", Array("MyClass", "OnGetOptimalPrice"));
AddEventHandler("iblock", "OnBeforeIBlockElementAdd", Array("MyClass", "OnBeforeIBlockElementAddUpdateHandler"));
AddEventHandler("iblock", "OnBeforeIBlockElementUpdate", Array("MyClass", "OnBeforeIBlockElementAddUpdateHandler"));
AddEventHandler("iblock", "OnAfterIBlockElementAdd", Array("MyClass", "OnAfterIBlockElementAddHandler"));
AddEventHandler('main', 'OnBeforeEventSend', array('MyClass', 'OnBeforeEventSend'));
AddEventHandler('main', 'OnAfterUserLogin', array('MyClass', 'OnAfterUserLogin'));
AddEventHandler('main', 'OnAfterUserLogout', array('MyClass', 'OnAfterUserLogout'));
AddEventHandler('form', 'onBeforeResultAdd', array('MyClass', 'onBeforeResultAddHandler'));
AddEventHandler('form', 'onAfterResultAdd', array('MyClass', 'onAfterResultAddHandler'));
AddEventHandler("iblock", "OnIBlockPropertyBuildList", array("CUserTypeSectionLink", "GetUserTypeDescription"));

class CUserTypeSectionLink
{

    private static $Script_included = false;

    function GetUserTypeDescription()
    {
        return array(
            "USER_TYPE_ID" => "SECTION_LINK",
            "CLASS_NAME" => "CUserTypeSectionLink",
            "DESCRIPTION" => "Ссылка на раздел",
            "BASE_TYPE" => "string",
            "PROPERTY_TYPE" => "S",
            "USER_TYPE" => "SECTION_LINK",
            "GetPublicViewHTML" => array("CUserTypeSectionLink","GetPublicViewHTML"),
            "GetPropertyFieldHtml" => array("CUserTypeSectionLink","GetPropertyFieldHtml"),
        );
    }

    public static function GetPublicViewHTML($arProperty, $value, $strHTMLControlName)
    {
        return $value['VALUE'];
    }

    public function GetPropertyFieldHtml($arProperty, $value, $strHTMLControlName)
    {

        $sectionsList=array();
        $ibNames=array();

        $rsIB = CIBlock::GetList();
        while ($ib = $rsIB->GetNext()) {
            $ibNames[$ib['ID']] = $ib['NAME'];
        }

        $rsBindValues = CIBlockSection::GetList(
            array("SORT" => "ASC"),
            array(),
            false,
            array(
                "ID",
                "IBLOCK_ID",
                "IBLOCK_NAME",
                "NAME"
            ),
            false
        );
        while ($bind_value = $rsBindValues->GetNext()) {
            $sectionsList[$bind_value['IBLOCK_ID']]['NAME'] = $ibNames[$bind_value['IBLOCK_ID']];
            $sectionsList[$bind_value['IBLOCK_ID']]['SECTIONS'][] = $bind_value;
        }

        $optionsHTML='<option value=""> -=( не выбрано )=- </option>';

        foreach($sectionsList as $ib){

            $optionsHTML .= '<optgroup label="'.$ib['NAME'].'">';

            foreach($ib['SECTIONS'] as $s){
                $optionsHTML .= '<option value="'.$s["ID"].'"'.
                    ( $value["VALUE"]==$s['ID'] ? ' selected' : '' ).
                    '>'.
                    $s['NAME'].' ['.$s['ID'].']'.
                    '</option>';
            }

            $optionsHTML .= '</optgroup>';

        }

        return  '<select name="'.$strHTMLControlName["VALUE"].'">'.$optionsHTML.'</select>';

    }

}
class MyClass
{
	function OnEpilogHandler()
	{
		global $USER;
		if($USER->IsAuthorized()){
			$rsUser = CUser::GetByID($USER->GetID()); 
			$arUser = $rsUser->Fetch();
//$_SESSION["CATALOG_COMPARE_LIST"][8]["ITEMS"]="";
			$OLD_COMPARE_ELEMENTS=unserialize($arUser["UF_COMPARE"]);
			$COMPARE_ELEMENTS = $_SESSION["CATALOG_COMPARE_LIST"][8]["ITEMS"];
/*
if($USER->IsAdmin()){
	p($OLD_COMPARE_ELEMENTS);
	p($COMPARE_ELEMENTS);
}
*/
			asort($OLD_COMPARE_ELEMENTS);
			asort($COMPARE_ELEMENTS);
			
			if($COMPARE_ELEMENTS!=$OLD_COMPARE_ELEMENTS){
				$user = new CUser;
				$fields = Array( 
					"UF_COMPARE" => serialize($COMPARE_ELEMENTS), 
				); 

				$user->Update($USER->GetID(), $fields);				
			}
		}
/*		if($USER->IsAdmin()){
			p($_SESSION);
		}
*/	}

	// создаем обработчик события "OnBeforeUserAdd"
	function OnBeforeUserAddHandler(&$arFields) {
		if(in_array(1, $arFields["GROUP_ID"])) {
			global $APPLICATION;
			$APPLICATION->throwException("Добавление новых администраторов запрещено.");
			return false;
		}
		
		if($_REQUEST["subj"]){return false;}
		if($_REQUEST["USER_SECOND_NAME"] && !$arFields["SECOND_NAME"])
			$arFields["SECOND_NAME"]=$_REQUEST["USER_SECOND_NAME"];
		if($_REQUEST["USER_PERSONAL_PHONE"] && !$arFields["PERSONAL_PHONE"])
			$arFields["PERSONAL_PHONE"]=$_REQUEST["USER_PERSONAL_PHONE"];
		
		// сохраняем город
		global $arGeoData;
		$arFields["UF_CITY"]=$arGeoData['CUR_CITY']['ID'];
	}

	// создаем обработчик события "OnAfterUserAdd"
	function OnBeforeUserUpdateHandler(&$arFields) {
		if($arFields['EMAIL']){
			SendDataToExternalServer("USER", $arFields);
			global $APPLICATION;
			$APPLICATION->ThrowException('Вы отправили запрос на изменение регистрационных данных. Они будут изменены после проверки менеджером интернет-магазина.'); 
			return false;
		}
	}	
	
	// создаем обработчик события "OnAfterUserAdd"
	function OnAfterUserAddUpdateHandler(&$arFields) {
		if($arFields['EMAIL']){
			// подписка
			$arResultSubscribe = [
					'OK' => 0,
					'ERROR' => '',
				];

			if($_REQUEST["subscribe"]=="Y"){
				$SUBSCRIBERS_IBLOCK_ID = 22;
				
				$EMAIL = $arFields['EMAIL'];

				if (!empty($EMAIL)) {
					$EMAIL = strtolower($EMAIL);
					if (check_email($EMAIL)) {
						$arFilterSubscribe = Array("IBLOCK_ID"=>$SUBSCRIBERS_IBLOCK_ID, 'NAME'=>$EMAIL);
						$arSelectSubscribe = Array("ID", "NAME");
						$arLimitSubscribe = ['nTopCount'=>1];
						$resSubscribe = CIBlockElement::GetList(Array(), $arFilterSubscribe, false, $arLimitSubscribe, $arSelectSubscribe);  
						if ($arFieldsSubscribe = $resSubscribe->Fetch()){
							$arResultSubscribe['OK'] = 1;
						} else {
							$arLoadProductArray = Array(
								"IBLOCK_SECTION_ID" => false,
								"IBLOCK_ID"      	=> $SUBSCRIBERS_IBLOCK_ID,
								"NAME"  		 	=> $EMAIL,
								"CODE"           	=> '',
								"XML_ID"         	=> '',
								"ACTIVE"         	=> "Y",
								"SORT"   		 	=> '500',
							);
							$el = new CIBlockElement;
							if($PRODUCT_ID = $el->Add($arLoadProductArray, false, false, false)) {
								$arResultSubscribe['OK'] = 1;
							} else {
								$arResultSubscribe['ERROR'] = $el->LAST_ERROR;
							}
						}
					} else {
						$arResultSubscribe['ERROR'] = 'Неверно указан адрес email,<br/>пожалуйста, повторите ввод!';
					}
				} else {
					$arResultSubscribe['ERROR'] = 'Заполните ваш email!';
				}	
			}else{
				$SUBSCRIBERS_IBLOCK_ID = 22;
				
				$EMAIL = $arFields['EMAIL'];

				if (!empty($EMAIL)) {
					$EMAIL = strtolower($EMAIL);
					$arFilterSubscribe = Array("IBLOCK_ID"=>$SUBSCRIBERS_IBLOCK_ID, 'NAME'=>$EMAIL);
					$arSelectSubscribe = Array("ID", "NAME");
					$arLimitSubscribe = ['nTopCount'=>1];
					$resSubscribe = CIBlockElement::GetList(Array(), $arFilterSubscribe, false, $arLimitSubscribe, $arSelectSubscribe);  
					if ($arFieldsSubscribe = $resSubscribe->Fetch()){
						if(!CIBlockElement::Delete($arFieldsSubscribe["ID"]))
							$arResultSubscribe['ERROR'] = 'Ошибка отмены подписки';
					}
				} else {
					$arResultSubscribe['ERROR'] = 'Заполните ваш email!';
				}	
				
			}
			
			$arFields["ResultSubscribe"]=$arResultSubscribe;
			
			SendDataToExternalServer("USER", $arFields);
		}
	}
	
	// создаем обработчик события "OnOrderNewSendEmail"
	function OnOrderNewSendEmail($orderID, &$eventName, &$arFields) {
		
		CModule::IncludeModule('iblock');
		CModule::IncludeModule('sale');
		
		$arOrder = CSaleOrder::GetByID($orderID);
		
		$db_vals = CSaleOrderPropsValue::GetList(
			array("SORT" => "ASC"),
			array(
				"ORDER_ID" => $orderID,
			)
		);
		while ($arVals = $db_vals->Fetch()) {
			$arOrder[$arVals['CODE']] = $arVals['VALUE'];
		}

		// получаем телефоны и адрес
		$order_props = CSaleOrderPropsValue::GetOrderProps($orderID);
		$phone="";
		$index = ""; 
		$country_name = "";
		$city_name = "";  
		$address = "";
		$file = "";
		$loc_id="";
		$arAllProps = [];
		while ($arProps = $order_props->Fetch()) {
			$arAllProps[$arProps['CODE']] = $arProps;
			if ($arProps["CODE"] == "PHONE") {
				$phone = htmlspecialcharsEx($arProps["VALUE"]);
			}
			if ($arProps["CODE"] == "LOCATION") {
				$arLocs = CSaleLocation::GetByID($arProps["VALUE"]);
				$country_name =  $arLocs["COUNTRY_NAME_ORIG"];
				$city_name = $arLocs["CITY_NAME_ORIG"];
			}

			if ($arProps["CODE"] == "INDEX") {
				$index = $arProps["VALUE"];   
			}

			if ($arProps["CODE"] == "ADDRESS") {
				$address = $arProps["VALUE"];
			}
			if($arProps["CODE"]=="HIDE_LOCATION_ID"){
				$loc_id = $arProps["VALUE"];
			}

			if ($arProps["CODE"] == "FILE") {
				if (!empty($arProps["VALUE"])) {
					$file = CFile::GetFileArray($arProps["VALUE"]);
					$file = '<a href="http://'.SITE_SERVER_NAME.$file['SRC'].'" style="color:#2e6eb6;">'.htmlspecialcharsEx($file['FILE_NAME']).'</a>';
				}
			}
		}

		$full_address = $country_name." - ".$city_name;

		// получаем название службы доставки
		$arDeliv = CSaleDelivery::GetByID($arOrder["DELIVERY_ID"]);
		$delivery_name = "";
		if ($arDeliv) {
			$arDeliv['STORE'] = unserialize($arDeliv['STORE']);
			$delivery_name = $arDeliv["NAME"];
		}

		// получаем название платежной системы   
		$arPaySystem = CSalePaySystem::GetByID($arOrder["PAY_SYSTEM_ID"]);
		$pay_system_name = "";
		if ($arPaySystem) {
			$pay_system_name = $arPaySystem["NAME"];
		}
		
		// получаем товары
		$res = CSaleBasket::GetList(array(), array("ORDER_ID" => $arOrder["ID"])); // ID заказа

		$orderItems = array();
		while ($arItem = $res->Fetch()) {
			$db_res = CSaleBasket::GetPropsList(
				array(
						"SORT" => "ASC",
						"NAME" => "ASC"
					),
				array("BASKET_ID" => $arItem['ID'])
			);
			while ($ar_res = $db_res->Fetch()) {
				$arItem['PROPS'][] = $ar_res;
			}
			$orderItems[$arItem['PRODUCT_ID']] = $arItem;
		}
		
		if (!empty($orderItems)) {
			$arSort = Array();
			$arFilter = Array("ID"=>array_keys($orderItems));
			$arSelect = Array("ID", "NAME", "CODE", "XML_ID", "DETAIL_PAGE_URL", "PROPERTY_CODE", "PROPERTY_NS_CODE", "PROPERTY_FULL_NAME", "PROPERTY_ITEM_NAME", "CATALOG_GROUP_1");
			$res = CIBlockElement::GetList($arSort, $arFilter, false, false, $arSelect);  
			while($arItem = $res->GetNext()) {
				$orderItems[$arItem['ID']]['DATA'] = $arItem;
			}
		}
		//p($orderItems);die();
		$ORDER_LIST_FULL = [];
		foreach($orderItems as $arItem) {
			$NAME = $arItem['DATA']['NAME'];
			if (!empty($arItem['DATA']['PROPERTY_ITEM_NAME_VALUE'])) {
				$NAME = $arItem['DATA']['PROPERTY_ITEM_NAME_VALUE'];
			} elseif (!empty($arItem['DATA']['PROPERTY_FULL_NAME_VALUE'])) {
				$NAME = $arItem['DATA']['PROPERTY_FULL_NAME_VALUE'];
			}
			$ORDER_WITH_SETUP = false;
			foreach($arItem['PROPS'] as $arProp) {
				if ($arProp['CODE'] == 'ORDER_WITH_SETUP') {
					if ($arProp['VALUE'] == 1) {
						$ORDER_WITH_SETUP = true;
					}
					break;
				}
			}
			$ORDER_LIST_FULL[] = '<a href="http://'.SITE_SERVER_NAME.$arItem['DETAIL_PAGE_URL'].'" style="color:#2e6eb6;">'.htmlspecialcharsEx($NAME).'</a> - '.$arItem['QUANTITY'].' шт.: '.CurrencyFormat(($arItem['PRICE']*$arItem['QUANTITY']), $arItem['CURRENCY']).''.($ORDER_WITH_SETUP ? ' (заказ с установкой)' : '');
			
		}
		$ORDER_LIST_FULL = implode('<br>', $ORDER_LIST_FULL);
		

		// добавляем новые поля в массив результатов
		$arFields["ORDER_DESCRIPTION"] = $arOrder["USER_DESCRIPTION"]; 
		$arFields["PHONE"] = $phone;
		$arFields["EMAIL"] = $arOrder['USER_EMAIL'];
		$arFields["DELIVERY_NAME"] =  $delivery_name;
		$arFields["PAY_SYSTEM_NAME"] =  $pay_system_name;
		$arFields["FULL_ADDRESS"] = $full_address;
		$arFields["ORDER_DATE_FULL"] = $arOrder['DATE_STATUS_FORMAT'];
		$arFields["FILE"] = $file;
		$arFields["ORDER_LIST_FULL"] = $ORDER_LIST_FULL;
		$arFields["LOCK_ID"] = $loc_id;
		$arFields["UR_DATA"] = [];
		
		$arUrikFields = [
			"Форма собственности" => "FIRM_TYPE",
			"Наименование" => "COMPANY",
			"ИНН" => "INN",
			"КПП" => "KPP",
			"Кор. счет" => "CORR",
			"Банк" => "BANK",
			"БИК" => "BIK",
			"Юридический адрес" => "COMPANY_ADR",
		];
		if ($arOrder['PERSON_TYPE_ID'] == 2) {
			foreach($arUrikFields as $arUrikFieldName => $arUrikFieldCode) {
				if (!empty($arOrder[$arUrikFieldCode])) {
					if ($arUrikFieldCode == 'FIRM_TYPE') {
						$arFields["UR_DATA"][] = $arUrikFieldName.': '.($arOrder[$arUrikFieldCode] == 'другое…' ? $arOrder['HIDE_FIRM_TYPE_ANOTHER'] : $arOrder[$arUrikFieldCode]);
					} else {
						$arFields["UR_DATA"][] = $arUrikFieldName.': '.$arOrder[$arUrikFieldCode];
					}
				}
			}
			if (empty($arFields["UR_DATA"])) {
				$arFields["UR_DATA"][] = '[ данные юридического лица не заполнены ]';
			}
		}
		if (!empty($arFields["UR_DATA"])) {
			$arFields["UR_DATA"] = implode("<br />\n<br />\n", $arFields["UR_DATA"])."<br />\n<br />\n";
		} else {
			$arFields["UR_DATA"] = '';
		}
		
		foreach($arOrder as $key => $val) {
			if (strpos($key, 'HIDE_') === 0) {
				if ($key == 'HIDE_LOCATION_ID') {
					if (!empty($val)) {
						$val = 'Пункт самовывоза: '.$val."<br />\n<br />\n";
					} else {
						$val = '';
					}
				} elseif ($key == 'HIDE_DELIVERY_PRICE') {
					if ($val == 0) {
						$val = 'бесплатно';
					}
				}
				$arFields[$key] = $val;
			}
		}
		
		
		$arFields["DEILVERY_ADDR_FULL"] = [];
		$arDelFields = [
			'Улица' => 'HIDE_DELIVERY_D_ADDR',
			'Дом' => 'HIDE_DELIVERY_D_HOUSE',
			'Корпус' => 'HIDE_DELIVERY_D_CORP',
			'Строение' => 'HIDE_DELIVERY_D_BUILD',
			'Квартира / офис' => 'HIDE_DELIVERY_D_ROOM',
		];
		if ($arOrder['HIDE_DELIVERY_NAME_CUR'] != 'Самовывоз') {
			foreach($arDelFields as $arDelFieldName => $arDelFieldCode) {
				if (!empty($arFields[$arDelFieldCode])) {
					$arFields["DEILVERY_ADDR_FULL"][] = $arDelFieldName.': '.$arFields[$arDelFieldCode];
				}
			}
			if (empty($arFields["DEILVERY_ADDR_FULL"])) {
				$arFields["DEILVERY_ADDR_FULL"][] = '[ не заполнено ]';
			}
		}
		if (!empty($arFields["DEILVERY_ADDR_FULL"])) {
			$arFields["DEILVERY_ADDR_FULL"] = "Адрес доставки:<br />\n".implode("<br />\n", $arFields["DEILVERY_ADDR_FULL"])."<br />\n<br />\n";
		} else {
			$arFields["DEILVERY_ADDR_FULL"] = '';
		}
		
		$price_del = intval(preg_replace('/[^\d]/ui', '', $arFields["HIDE_DELIVERY_PRICE"]));
		$price_items = intval(preg_replace('/[^\d]/ui', '', $arFields["PRICE"]));
		
		$arFields["PRICE_TOTAL"] = number_format($price_del+$price_items, 0, '.', ' ').' руб.';
		
		// $arFields["ORDER_DESCRIPTION"] .= '<pre>'.print_r($arFields, 1).'</pre>';
		// $arFields["ORDER_DESCRIPTION"] .= '<pre>'.print_r($arOrder, 1).'</pre>';
		
	}
	
	// не даем пересчитывать цены
	function OnGetOptimalPrice($productID, $quantity = 1, $arUserGroups = array(), $renewal = "N", $arPrices = array(), $siteID = false, $arDiscountCoupons = false) {
		
		global $arGeoData;
		
		CModule::IncludeModule('iblock');
		CModule::IncludeModule('catalog');
		
		/* 
		$dbPriceTypes = [];
		$dbPriceType = CCatalogGroup::GetList(
			array("SORT" => "ASC"),
			array("NAME" => [$CUR_MAIN_CITY_CODE, $CUR_MAIN_CITY_CODE.'_NODISCOUNT'])
		);
		while ($arPriceType = $dbPriceType->Fetch()) {
			// p($arPriceType);
			$dbPriceTypes[$arPriceType['NAME']] = $arPriceType;
		}
		 */
		
		// p('$dbPriceTypes');
		// p($dbPriceTypes);
		
		// p('$productID');
		// p($productID);
		// p('$quantity');
		// p($quantity);
		// p('$arUserGroups');
		// p($arUserGroups);
		// p('$renewal');
		// p($renewal);
		// p('$arPrices');
		// p($arPrices);
		// p('$siteID');
		// p($siteID);
		// p('$arDiscountCoupons');
		// p($arDiscountCoupons);
		
		/* 
		$dbBasketItems = CSaleBasket::GetList(
			array(
				"NAME" => "ASC",
				"ID" => "ASC"
			),
			array(
				"FUSER_ID" => CSaleBasket::GetBasketUserID(),
				"LID" => SITE_ID,
				"ORDER_ID" => "NULL",
				"DELAY" => "N",
				"CAN_BUY" => "Y"
			)
		);
	   
		$arBasketItems = array();
		while ($arItems = $dbBasketItems->Fetch()) {
			if (strlen($arItems["CALLBACK_FUNC"]) > 0) {
				CSaleBasket::UpdatePrice($arItems["ID"], $arItems["QUANTITY"]);
				$arItems = CSaleBasket::GetByID($arItems["ID"]);
			}

			$arBasketItems[] = $arItems;
		}
		// текущая сумма заказа         
		$summ = 0;
		foreach ($arBasketItems as $arBasketItem) {
			$summ += $arBasketItem["PRICE"] * $arBasketItem["QUANTITY"];
		}
		*/

		$dbNewPrice = CPrice::GetListEx(array(), array(
			'PRODUCT_ID' => $productID,
			'CATALOG_GROUP_ID' => $arGeoData['CUR_CITY']['PRICE_ID'],
		));
		
		// p(array(
			// 'PRODUCT_ID' => $productID,
			// 'CATALOG_GROUP_ID' => $arGeoData['CUR_CITY']['PRICE_ID'],
		// ));
		if($newPrice = $dbNewPrice->Fetch()) {
			// p($newPrice);
			return array(
				'PRICE' => $newPrice,
				'DISCOUNT_PRICE' => false,
				'DISCOUNT' => array(),
				'DISCOUNT_LIST' => array(),
			);
		}
		// p('false');
		return false;
		
	}
	
	// создаем обработчик события "OnBeforeIBlockElementAdd" & "OnBeforeIBlockElementUpdate"
	function OnBeforeIBlockElementAddUpdateHandler(&$arFields) {
		// global $APPLICATION;
		// $APPLICATION->throwException("Введите символьный код. (ID:".$arFields["ID"].")");
		// return false;
		
		if ($arFields['IBLOCK_ID'] == 19) {		// РК | Геозависисый контент
		
			$arProps = [];
			$properties = CIBlockProperty::GetList(Array("sort"=>"asc", "name"=>"asc"), Array("IBLOCK_ID"=>$arFields['IBLOCK_ID']));
			while ($prop_fields = $properties->GetNext()) {
				$arProps[$prop_fields['CODE']] = $prop_fields['ID'];
			}
		
			if (!empty($arFields['ID'])) {
				if (!empty($arFields['PROPERTY_VALUES'][$arProps['IS_DEFAULT']][$arFields['ID'].':'.$arProps['IS_DEFAULT']]['VALUE'])) {
					$arFilter = Array("IBLOCK_ID"=>$arFields['IBLOCK_ID'], "!=ID"=>$arFields["ID"], "PROPERTY_URL"=>$arFields['PROPERTY_VALUES'][$arProps['url']][$arFields['ID'].':'.$arProps['url']]['VALUE'], "PROPERTY_IS_DEFAULT"=>1);
					$arLimit = Array('nTopCount'=>1);
					$arSelect = Array("ID");
					$res = CIBlockElement::GetList($arSort, $arFilter, false, $arLimit, $arSelect);  
					if ($arItem = $res->Fetch()) {
						global $APPLICATION;
						$APPLICATION->throwException("Страница по умолчанию уже существует для указанного URL!");
						return false;
					}
				}
			}
			
			// echo '<pre>';
			// var_dump($arFields);
			// echo '</pre>';
			// p($arFields);
			// die();
		
		}
	}
	
	// создаем обработчик события "OnAfterIBlockElementAdd"
	function OnAfterIBlockElementAddHandler(&$arFields) {
		if($arFields['IBLOCK_ID']==13){ // новый отзыв
			$SITE_ID = 's1'; 
			$EVENT_TYPE = 'review_add'; // тип почтового шаблона
			
			$item="";
			$res=CIBlockElement::GetByID($arFields["PROPERTY_VALUES"]["ITEM_LINK"]);
			if($ar_res = $res->GetNext())
				$item=$ar_res['NAME'];
			
			$arMailFields = array(
				"ID"=>$arFields["ID"],
				"grade"=>$arFields["PROPERTY_VALUES"]["grade"]+3,
				"text"=>$arFields["PROPERTY_VALUES"]["text"],
				"author"=>$arFields["PROPERTY_VALUES"]["author"],
				"city"=>$arFields["PROPERTY_VALUES"]["city"],
				"item_id"=>$arFields["PROPERTY_VALUES"]["ITEM_LINK"],
				"item"=>$item
			);
			
			CEvent::SendImmediate($EVENT_TYPE, $SITE_ID, $arMailFields);			
		}
	}
	
	// создаем обработчик события "OnBeforeEventSend"
	function OnBeforeEventSend(&$arFields, &$arTemplate) {
		// Портфолио: Оставить заявку
		if ($arFields['RS_FORM_ID'] == 1) {
			global $arGeoData;
			$arFields['SIMPLE_QUESTION_140'] = $arGeoData['CUR_CITY']['CITY_NAME'].' ['.$arGeoData['CUR_CITY']['CITY_CODE'].']';
			$arFields['SIMPLE_QUESTION_306'] = $arGeoData['CUR_CITY']['REGION'];
			$arFields['SIMPLE_QUESTION_629'] = $arGeoData['CUR_CITY']['NAME'];
			$arFields['SIMPLE_QUESTION_361_RAW'] = preg_replace('/,/ui', ', ', $arFields['SIMPLE_QUESTION_361_RAW']);
			$arFields['SIMPLE_QUESTION_997_RAW'] = preg_replace('/,/ui', ', ', $arFields['SIMPLE_QUESTION_997_RAW']);
		}
	}
	
	function OnAfterUserLogin(&$arFields){
		if($arFields["USER_ID"]){
			global $APPLICATION;
			$rsUser = CUser::GetByID($arFields["USER_ID"]); 
			$arUser = $rsUser->Fetch();

			// объединяем избранное пользователем и избранное из куков, удаляем старые (больше года) записи
			$NEW_FAV_ELEMENTS=[];
			$fav=unserialize($arUser["UF_FAV"]);
			foreach($fav as $el=>$time){
				if(time()-$time<=365*24*60*60){
					$NEW_FAV_ELEMENTS[$el]=$time;
				}
			}
			
			$FAV_ELEMENTS = $APPLICATION->get_cookie('FAVORITES');
			if (!empty($FAV_ELEMENTS)) {
				$FAV_ELEMENTS = explode('.', $FAV_ELEMENTS);
			} else {
				$FAV_ELEMENTS = [];
			}
			foreach($FAV_ELEMENTS as $ELEMENT){
				$NEW_FAV_ELEMENTS[$ELEMENT]=time();
			}
			asort($NEW_FAV_ELEMENTS);
			
			// объединяем просмотренное пользователем и просмотренное из куков, удаляем старые (больше года) записи
			$NEW_SAW_ELEMENTS=[];
			$saw=unserialize($arUser["UF_SAW"]);
			foreach($saw as $el=>$time){
				if(time()-$time<=365*24*60*60){
					$NEW_SAW_ELEMENTS[$el]=$time;
				}
			}
			
			$SAW_ELEMENTS = $APPLICATION->get_cookie('SAW_ELEMENTS');
			if (!empty($SAW_ELEMENTS)) {
				$SAW_ELEMENTS = explode('.', $SAW_ELEMENTS);
			} else {
				$SAW_ELEMENTS = [];
			}
			foreach($SAW_ELEMENTS as $ELEMENT){
				$NEW_SAW_ELEMENTS[$ELEMENT]=time();
			}
			asort($NEW_SAW_ELEMENTS);
			// 
			$user = new CUser;
			$fields = Array( 
				"UF_FAV" => serialize($NEW_FAV_ELEMENTS), 
				"UF_SAW" => serialize($NEW_SAW_ELEMENTS), 
			); 
//			print_r($fields);
//			die();
			$user->Update($arFields["USER_ID"], $fields);
			
			$APPLICATION->set_cookie('FAVORITES', implode(".",array_keys($NEW_FAV_ELEMENTS)));
			$APPLICATION->set_cookie('SAW_ELEMENTS', implode(".",array_keys($NEW_SAW_ELEMENTS)));

			$OLD_COMPARE_ELEMENTS=unserialize($arUser["UF_COMPARE"]);
			$COMPARE_ELEMENTS = $_SESSION["CATALOG_COMPARE_LIST"][8]["ITEMS"];

			asort($OLD_COMPARE_ELEMENTS);
			asort($COMPARE_ELEMENTS);
			$COMPARE_ELEMENTS=ArrayMergeKeepKeys($OLD_COMPARE_ELEMENTS,$COMPARE_ELEMENTS);
		
			if($COMPARE_ELEMENTS && $OLD_COMPARE_ELEMENTS!=$COMPARE_ELEMENTS){
				$user = new CUser;
				$fields = Array( 
					"UF_COMPARE" => serialize($COMPARE_ELEMENTS), 
				); 

				$user->Update($arFields["USER_ID"], $fields);				
			}
			$_SESSION["CATALOG_COMPARE_LIST"][8]["ITEMS"]=$COMPARE_ELEMENTS;
			
			header("Location: ".$APPLICATION->GetCurPageParam("", array("logout","login","logout","register","forgot_password","change_password")));
		}
	}
	
	function OnAfterUserLogout(&$arFields){
		global $APPLICATION;
		$APPLICATION->set_cookie('FAVORITES', "");
		$APPLICATION->set_cookie('SAW_ELEMENTS', "");
	}
	
	function onBeforeResultAddHandler($WEB_FORM_ID, &$arFields, &$arrVALUES){
		global $APPLICATION;
		if($_REQUEST["subj"]) $APPLICATION->ThrowException('unknown error');
	}

	function onAfterResultAddHandler($WEB_FORM_ID, $RESULT_ID)
	{
		// действие обработчика распространяется только на форму с ID=6
		if ($WEB_FORM_ID == 1) 
		{
			$arAnswer = CFormResult::GetDataByID(
				$RESULT_ID, 
				array(),  // вопрос "Какие области знаний вас интересуют?" 
				$arResult, 
				$arAnswer2
			);
/*
			// выведем поля результата
			echo "<pre>"; print_r($arResult); echo "</pre>";

			// выведем значения ответов
			echo "<pre>"; print_r($arAnswer); echo "</pre>";

			// выведем значения ответов в несколько ином формате
			echo "<pre>"; print_r($arAnswer2); echo "</pre>";

			
*/			
			SendDataToExternalServer("PORTFOLIO_FORM",$arAnswer2);

		}
	}	
	
	function OnSaleCalculateOrderDeliveryHandler(&$arOrder){
		if($arOrder["DELIVERY_ID"]==10 && $arOrder['ORDER_PROP']["32"]){
			$delivery_price=intval(str_replace(" руб.","",$arOrder['ORDER_PROP']["32"]));
			$arOrder["PRICE_DELIVERY"] = $arOrder["DELIVERY_PRICE"] = $delivery_price;
			$arOrder["ORDER_PRICE"] = $arOrder["ORDER_PRICE"]+$delivery_price;
		}
		
		SendDataToExternalServer("BASKET", array("delivery"=>$arOrder["DELIVERY_ID"], "paysystem"=>$arOrder["PAY_SYSTEM_ID"]));
		
		AddMessage2Log($arOrder, "OnSaleCalculateOrderDelivery");
	}		 
	
	function OnSaleCalculateOrderPaySystemHandler(&$arOrder){
		SendDataToExternalServer("BASKET", array("delivery"=>$arOrder["DELIVERY_ID"], "paysystem"=>$arOrder["PAY_SYSTEM_ID"]));
	}

	function OnOrderSaveHandler($orderId, $arFields, $arOrder, $isNew){
		if($isNew){
			CModule::IncludeModule("iblock");
			$arBasketItems = array();

			$dbBasketItems = CSaleBasket::GetList(
					array(
							"NAME" => "ASC",
							"ID" => "ASC"
						),
					array(
							"ORDER_ID" => $orderId
						),
					false,
					false,
					array()
				);
			while ($arItems = $dbBasketItems->Fetch())
			{
				$arItems["PRODUCT_BITRIX_ID"]=$arItems["PRODUCT_ID"];
				$obEl=CIBlockElement::GetList(array(),array("ID"=>$arItems["PRODUCT_ID"]),false,false,array("ID","PROPERTY_NS_CODE"));
				if($arEl=$obEl->GetNext()){
					$arItems["PRODUCT_ID"]=$arEl["PROPERTY_NS_CODE_VALUE"]; 
				}else{
					unset($arItems["PRODUCT_ID"]);
				}
				
				$arBasketItems[] = $arItems;
			}

		
			SendDataToExternalServer("ORDER", array("BITRIX_ID"=>$orderId,"FIELDS"=>$arFields,"PROPERTIES"=>$arOrder,"BASKET"=>$arBasketItems));
		}
	
	}
	
	function OnBasketAddHandler($ID,$arFields){
//		AddMessage2Log($arFields, "OnBasketAddHandler", 15);
		$send=true;
		if($_SESSION["basket_from_gateway"]==1){
			$_SESSION["basket_from_gateway"]=0;
			$send=false;
		}		
		if($send) {SendDataToExternalServer("BASKET", array());}
	}
	function OnBasketUpdateHandler($ID,$arFields){
//		AddMessage2Log($arFields, "OnBasketUpdateHandler", 15);
//		AddMessage2Log($_SESSION, "OnBasketUpdateHandler", 15);
		$send=true;
		if($_SESSION["basket_from_gateway"]==1){
			$_SESSION["basket_from_gateway"]=0;
			$send=false;
		}
		if($send) {SendDataToExternalServer("BASKET", array());}
	}
	function OnBasketDeleteHandler($ID){
		$send=true;
		if($_SESSION["basket_from_gateway"]==1){
			$_SESSION["basket_from_gateway"]=0;
			$send=false;
		}		
		if($send) {SendDataToExternalServer("BASKET", array());}
	}
}
