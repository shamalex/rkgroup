<?

function p($arr, $dmp = false) {
	echo '<pre class="debug_pre">';
	if ($dmp) {
		var_dump($arr);
	} else {
		print_r($arr);
	}
	echo '</pre>';
}

function LocalRedirect1($page) {
	p($page);
	die();
}

function getNumEnding($number, $endingArray) {
	$number = $number % 100;
	if ($number>=11 && $number<=19) {
		$ending=$endingArray[2];
	}
	else {
		$i = $number % 10;
		switch ($i)
		{
			case (1): $ending = $endingArray[0]; break;
			case (2):
			case (3):
			case (4): $ending = $endingArray[1]; break;
			default: $ending=$endingArray[2];
		}
	}
	return $ending;
}

function ConvertBytes($number){
	$len = strlen ($number);
	if ($len < 4){ return sprintf("%d б", $number); }
	if ($len>= 4 && $len <=6){ return sprintf("%0.2f Кб", $number/1024); }
	if ($len>= 7 && $len <=9){ return sprintf("%0.2f Мб", $number/1024/1024); }
	return sprintf("%0.2f Гб", $number/1024/1024/1024);
}

function cropStr($str, $size){ 
	$str = substr($str,0, $size); // первым этапом надо отрезать строку четко по заданному количеству символов
	return substr($str, 0, strrpos($str, ' ' ));    //получаем позицию последнего пробела и обрезаем до нее строку
}

function nfGetCurPageParam( $strParam = '', $arParamKill = array(), $get_index_page = NULL, $uri = FALSE ){

   if( NULL === $get_index_page ){

	  if( defined( 'BX_DISABLE_INDEX_PAGE' ) )
		 $get_index_page = !BX_DISABLE_INDEX_PAGE;
	  else
		 $get_index_page = TRUE;

   }

   $sUrlPath = GetPagePath( $uri, $get_index_page );
   $strNavQueryString = nfDeleteParam( $arParamKill, $uri );

   if( $strNavQueryString != '' && $strParam != '' )
	  $strNavQueryString = '&'.$strNavQueryString;

   if( $strNavQueryString == '' && $strParam == '' )
	  return $sUrlPath;
   else
	  return $sUrlPath.'?'.$strParam.$strNavQueryString;

}


function nfDeleteParam( $arParam, $uri = FALSE ){

   $get = array();
   if( $uri && ( $qPos = strpos( $uri, '?' ) ) !== FALSE ){

	  $queryString = substr( $uri, $qPos + 1 );
	  parse_str( $queryString, $get );
	  unset( $queryString );

   }

   if( sizeof( $get ) < 1 )
	  $get = $_GET;

   if( sizeof( $get ) < 1 )
	  return '';

   if( sizeof( $arParam ) > 0 ){

	  foreach( $arParam as $param ){

		 $search	= &$get;
		 $param	 = (array)$param;
		 $lastIndex = sizeof( $param ) - 1;

		 foreach( $param as $c => $key ){

			if( array_key_exists( $key, $search ) ){

			   if( $c == $lastIndex )
				  unset( $search[$key] );
			   else
				  $search = &$search[$key];

			}

		 }

	  }

   }

   return str_replace(
	  array( '%5B', '%5D' ),
	  array( '[', ']' ),
	  http_build_query( $get )
   );

}

function ShowContentWrapHeader() {
	global $APPLICATION;
	$data = '';
	$CONTENT_PAGE = $APPLICATION->GetProperty('CONTENT_PAGE') == 'Y';
	if ($CONTENT_PAGE) {
		ob_start();
		
		$HAS_RIGHT_COL = !empty($GLOBALS['RIGHT_CONTENT_COL']);
		?>
		<!-- content -->
		<div class="content roll c">
			
			<? if($HAS_RIGHT_COL): ?>
				<!-- col-l -->
			<? else: ?>
				<!-- full -->
			<? endif; ?>
			<div class="<?= $HAS_RIGHT_COL ? 'col-l' : 'full' ?> txt-block">
				
				<h1 class="ttl1"><?=$APPLICATION->GetTitle(false)?></h1>
			
			
		<?
		
		$data = ob_get_contents();
		ob_end_clean();
	}
	return $data;
}

function ShowContentWrapFooter() {
	global $APPLICATION;
	$data = '';
	$CONTENT_PAGE = $APPLICATION->GetProperty('CONTENT_PAGE') == 'Y';
	if ($CONTENT_PAGE) {
		ob_start();
		
		$HAS_RIGHT_COL = !empty($GLOBALS['RIGHT_CONTENT_COL']);
		?>

			</div>
			<? if($HAS_RIGHT_COL): ?>
				<!-- / col-l end -->
				
				<!-- col-r -->
				<div class="col-r">
					
					<!-- v-gr -->
					<div class="v-gr">
						<h2 class="ttl2">Другие материалы раздела</h2>
						<!-- item 1 -->
						<a class="item b-flex" href="#">
							<div class="pic" style="background-image:url('<?= SITE_TEMPLATE_PATH ?>/i/blocks/mat1.png');"></div>
							<div class="w">
								<div class="ttl">Комфортная температура дома</div>
							</div>
						</a>
						<!-- / item 1 -->
						
						<!-- item 2 -->
						<a class="item b-flex" href="#">
							<div class="pic" style="background-image:url('<?= SITE_TEMPLATE_PATH ?>/i/blocks/mat2.png');"></div>
							<div class="w">
								<div class="ttl">Ижевского Завода Тепловой Техники при участии специалистов Комфортная температура дома </div>
							</div>
						</a>
						<!-- / item 2 -->
						
						<!-- item 3 -->
						<a class="item b-flex" href="#">
							<div class="pic" style="background-image:url('<?= SITE_TEMPLATE_PATH ?>/i/blocks/mat3.png');"></div>
							<div class="w">
								<div class="ttl">Комфортная температура дома Инверторная сплит система система система система</div>
							</div>
						</a>
						<!-- / item 3 -->
						
						<!-- item 4 -->
						<a class="item b-flex" href="#">
							<div class="pic" style="background-image:url('<?= SITE_TEMPLATE_PATH ?>/i/blocks/mat4.png');"></div>
							<div class="w">
								<div class="ttl">Комфортная температура дома</div>
							</div>
						</a>
						<!-- / item 4 -->
						
						<!-- item 5 -->
						<a class="item b-flex" href="#">
							<div class="pic" style="background-image:url('<?= SITE_TEMPLATE_PATH ?>/i/blocks/mat5.png');"></div>
							<div class="w">
								<div class="ttl-sm">Комфортная температура дома</div>
								<div class="date">12.12.2015</div>
							</div>
						</a>
						<!-- / item 5 -->
						
						<!-- item 6 -->
						<a class="item b-flex" href="#">
							<div class="pic" style="background-image:url('<?= SITE_TEMPLATE_PATH ?>/i/blocks/mat2.png');"></div>
							<div class="w">
								<div class="ttl-sm">Комфортная температура дома и не дома тоже комфортная</div>
								<div class="date">12.12.2015</div>
							</div>
						</a>
						<!-- / item 6 -->
						
					</div>
					<!-- / v-gr end -->
					
					<!-- v-gr -->
					<div class="v-gr">
						<h2 class="ttl2">Горячее предложение</h2>
						<!-- item w/timer -->
						<div class="item b-base">
							<div class="w c">
								
								<div class="timer">
									<div class="sum">Успей сэкономить 80 рублей</div>
									<div class="time" data-countdown="2015/08/08">
										<!-- <b>11</b> <span>дн.</span> <b>11</b> <span>ч.</span> <b>11</b> <span>мин.</span> <b class="sec">11</b> <span>сек.</span> -->
									</div>
								</div>
								
								<a class="lnk" href="card.html">
									<div class="ttl">Инверторная сплит система система система система</div>
									<div class="pic" style="background-image:url('<?= SITE_TEMPLATE_PATH ?>/i/blocks/hot1.png');">
										<div class="ico-hot">Успей<br/>купить</div>
									</div>
									<div class="dsc">Electrolux EACS/I - 13 HO/N3 серии Orlando DC Inverter система</div>
								</a>
								
								<div class="prc">
									<div class="curr">18 990 руб.</div>
									<div class="past">
										<span class="old">89 490</span> <span class="profit">1 666 выгода</span>
									</div>
								</div>
								<div class="btn btn-to-bsk">В корзину</div>
								
							</div>
						</div>
						<!-- / item w/timer -->
					</div>
					<!-- / v-gr end -->
					
					
					<!-- v-gr -->
					<div class="v-gr">
						<a href="card.html" class="pic-offer" style="background-image:url('<?= SITE_TEMPLATE_PATH ?>/i/blocks/offer356x320.png');"></a>
					</div>
					<!-- / v-gr end -->
					
					
					<!-- v-gr -->
					<div class="v-gr">
						<h2 class="ttl2">Требуется помощь?</h2>
						
						<a class="item b-hlp callback-pop" href="#callback">
							<div class="w">
								<div class="ttl">Закажите звонок!</div>
								<p class="dsc">Удобная функция заказа<br/>обратного звонка.</p>
								<div class="ico ico-1">&nbsp;</div>
							</div>
						</a>
						<a class="item b-hlp" href="#">
							<div class="w">
								<div class="ttl">+7 495 777-19-77</div>
								<p class="dsc">Помогаем по любым<br/>вопросам продажи и сервиса.</p>
								<div class="ico ico-2">&nbsp;</div>
							</div>
						</a>
					</div>
					<!-- / v-gr end -->
					
				</div>
				<!-- / col-r end-->
			<? else: ?>
				<!-- / full end -->
			<? endif; ?>
			
			
		</div>
		<!-- / content roll end -->
		<?
		
		$data = ob_get_contents();
		ob_end_clean();
	}
	return $data;
}


function CheckServerIPs() {
	if (in_array($_SERVER['REMOTE_ADDR'], ['109.195.179.212', '176.213.203.188'])) {
		return false;
	}
	return true;
}


function GetMonthForDelivery($time) {
	if (empty($time)) {
		$time = time();
	}
	$cur_month = intval(date('n', $time));
	$months = [
		1 => 	'января',
				'февраля',
				'марта', 
				'апреля',
				'мая',
				'июня',
				'июля',
				'августа',
				'сентября',
				'октября',
				'ноября',
				'декабря',
	];
	return $months[$cur_month];
}

function ChangeBasketItems(){
	
	// здесь должен быть пересчет корзины по остаткам, но нет остатков
	
/*	global $arGeoData;
	//p($arGeoData["CUR_CITY"]);
	CModule::IncludeModule("iblock");
	CModule::IncludeModule("sale");

	$QUANITY_PROP = preg_replace('/\-+/ui', '_', $arGeoData['CUR_CITY']['CITY_CODE1']);
	$QUANITY_PROP = strtoupper('COUNT_'.$QUANITY_PROP);

	$dbBasketItems = CSaleBasket::GetList(
			array(
					"NAME" => "ASC",
					"ID" => "ASC"
				),
			array(
					"FUSER_ID" => CSaleBasket::GetBasketUserID(),
					"LID" => SITE_ID,
					"ORDER_ID" => "NULL",
				),
			false,
			false,
			array("ID", "PRODUCT_ID", "QUANTITY", "PRICE", "CAN_BUY")
		);
	while ($arItems = $dbBasketItems->Fetch())
	{
		$arSelect = Array("ID", "NAME", "XML_ID", "PROPERTY_".$QUANITY_PROP);
		$arFilter = Array("ID"=>$arItems["PRODUCT_ID"]);
		$res = CIBlockElement::GetList(Array(), $arFilter, false, Array("nTopCount"=>1), $arSelect);
		if($arProd = $res->GetNext())
		{
			//print_r($arItems);print_r($arProd);
			if($arProd["PROPERTY_".$QUANITY_PROP."_VALUE"] && $arItems["CAN_BUY"]=="N"){echo "1";
				CSaleBasket::Update($arItems["ID"], array("CAN_BUY" => "Y"));
			}elseif(!$arProd["PROPERTY_".$QUANITY_PROP."_VALUE"] && $arItems["CAN_BUY"]=="Y"){echo "2";
				CSaleBasket::Update($arItems["ID"], array("CAN_BUY" => "N"));
			}
		}
	}
*/	
}

function SendDataToExternalServer($dataType, $arFields, $server=""){
	$default_server=Array(
		"USER" => "http://rusklimat.webway.ru/data/user",
		"CALLBACK" => "http://rusklimat.webway.ru/data/user",
		"PORTFOLIO_FORM" => "http://rusklimat.webway.ru/data/user",
		"ORDER" => "http://rusklimat.webway.ru/api/in/order",
		"ORDER_CANCEL" => "http://rusklimat.webway.ru/api/in/order",
		"ORDER_MESSAGE" => "http://rusklimat.webway.ru/api/in/order",
		"BASKET" => "http://rusklimat.webway.ru/api/in/basket",
	);

	if(!$server) $server=$default_server[$dataType];

	switch($dataType){
		case "USER":
		
			CModule::IncludeModule("iblock");
			// отправка данных на шлюз
			$sendData = array(
				"EMAIL" => $arFields["EMAIL"],
				"LAST_NAME" => $arFields["LAST_NAME"],
				"NAME" => $arFields["NAME"],
				"SECOND_NAME" => $arFields["SECOND_NAME"],
				"ACTIVE" => $arFields["ACTIVE"],
				"BITRIX_ID" => $arFields["ID"],
				"PHONE" => $arFields["PERSONAL_PHONE"],
			);

			if($arFields["ResultSubscribe"]["OK"]==1) $sendData["SUBSCRIBE"]="Y";
			else $sendData["SUBSCRIBE"]=$arFields["ResultSubscribe"]["ERROR"];
			
			if($arFields["UF_CITY"]){
				$arSortCity = ['PROPERTY_region'=>'ASC', 'NAME'=>'ASC'];
				$arFilterCity = Array("IBLOCK_ID"=>15, "ID"=>$arFields["UF_CITY"]);
				// $arSelect = false;
				$arSelectCity = ['ID','XML_ID', 'NAME', 'PROPERTY_region', 'PROPERTY_network_name', 'PROPERTY_network_data_code', 'PROPERTY_network_data_site_id', 'PROPERTY_filial1', 'PROPERTY_filial2', 'PROPERTY_filial1_code', 'PROPERTY_filial2_code'];
				$sendData["CITY"] = [];

				if (CModule::IncludeModule("iblock")) {
					$resCity = CIBlockElement::GetList($arSortCity, $arFilterCity, false, false, $arSelectCity);

					if ($fieldsCity = $resCity->GetNext()) {
						$sendData["CITY"] = array(
							"ID" => $fieldsCity['XML_ID'],
							'BITRIX_ID' => $fieldsCity['ID'],
							'REGION'=>$fieldsCity['PROPERTY_REGION_VALUE'], 
							'NETWORK_NAME' => $fieldsCity['PROPERTY_NETWORK_NAME_VALUE'], 
							'NETWORK_DATA_CODE' => $fieldsCity['PROPERTY_NETWORK_DATA_CODE_VALUE'], 
							'NETWORK_DATA_SITE_ID' => $fieldsCity['PROPERTY_NETWORK_DATA_SITE_ID_VALUE'], 
							'FILIAL1' => $fieldsCity['PROPERTY_FILIAL1_VALUE'], 
							'FILIAL2' => $fieldsCity['PROPERTY_FILIAL2_VALUE'],
							'FILIAL1_CODE' => $fieldsCity['PROPERTY_FILIAL1_CODE_VALUE'],
							'FILIAL2_CODE' => $fieldsCity['PROPERTY_FILIAL2_CODE_VALUE']
						);
					}
				}
			}
			/*
			$sendData["DELIVERY_ADDRESS"] = [];
			$rsAddress=CIBlockElement::GetList(array(), array("IBLOCK_ID"=>30,"PROPERTY_USER"=>$arFields["ID"]), false, false, array("ID","NAME","PROPERTY_city","PROPERTY_street","PROPERTY_house","PROPERTY_housing","PROPERTY_build","PROPERTY_apartment","PROPERTY_USER","PROPERTY_default"));
			while ($arFieldsAdr=$rsAddress->GetNext()) {
				$sendData["DELIVERY_ADDRESS"][]=Array(
					"BITRIX_ID" => $arFieldsAdr["ID"],
					"ADDRESS" => $arFieldsAdr["NAME"],
					"CITY" => $arFieldsAdr['PROPERTY_CITY_VALUE'],
					"STREET" => $arFieldsAdr['PROPERTY_STREET_VALUE'],
					"HOUSE" => $arFieldsAdr['PROPERTY_HOUSE_VALUE'],
					"HOUSING" => $arFieldsAdr['PROPERTY_HOUSING_VALUE'],
					"BUILD" => $arFieldsAdr['PROPERTY_BUILD_VALUE'],
					"APARTMENT" => $arFieldsAdr['PROPERTY_APARTMENT_VALUE'],
					
				);
			}

			$sendData["COMPANY"] = [];
			$rsCompany=CIBlockElement::GetList(array(), array("IBLOCK_ID"=>31,"PROPERTY_USER"=>$arFields["ID"]), false, false, array("ID","NAME","PROPERTY_org_type","PROPERTY_org_name","PROPERTY_inn","PROPERTY_kpp","PROPERTY_bik","PROPERTY_rs","PROPERTY_bank","PROPERTY_ks","PROPERTY_ur_adr","PROPERTY_USER","PROPERTY_default"));
			while ($arFieldsComp=$rsCompany->GetNext()) {
				$sendData["COMPANY"][]=Array(
					"BITRIX_ID" => $arFieldsComp["ID"],
					"ADDRESS" => $arFieldsComp["NAME"],
					"ORG_TYPE" => $arFieldsComp['PROPERTY_ORG_TYPE_VALUE'],
					"ORG_NAME" => $arFieldsComp['PROPERTY_ORG_NAME_VALUE'],
					"INN" => $arFieldsComp['PROPERTY_INN_VALUE'],
					"KPP" => $arFieldsComp['PROPERTY_KPP_VALUE'],
					"BIK" => $arFieldsComp['PROPERTY_BIK_VALUE'],
					"RS" => $arFieldsComp['PROPERTY_RS_VALUE'],
					"BANK" => $arFieldsComp['PROPERTY_BANK_VALUE'],
					"KS" => $arFieldsComp['PROPERTY_KS_VALUE'],
					"UR_ADR" => $arFieldsComp['PROPERTY_UR_ADR_VALUE'],				
				);
			}			
			*/
			break;
		case "CALLBACK":
			$sendData=$arFields;
			if($arFields["CITY_ID"]){
				$arSortCity = ['PROPERTY_region'=>'ASC', 'NAME'=>'ASC'];
				$arFilterCity = Array("IBLOCK_ID"=>15, "ID"=>$sendData["CITY_ID"]);
				// $arSelect = false;
				$arSelectCity = ['ID','XML_ID', 'NAME', 'PROPERTY_region', 'PROPERTY_network_name', 'PROPERTY_network_data_code', 'PROPERTY_network_data_site_id', 'PROPERTY_filial1', 'PROPERTY_filial2', 'PROPERTY_filial1_code', 'PROPERTY_filial2_code'];
				$sendData["CITY"] = [];

				if (CModule::IncludeModule("iblock")) {
					$resCity = CIBlockElement::GetList($arSortCity, $arFilterCity, false, false, $arSelectCity);

					if ($fieldsCity = $resCity->GetNext()) {
						$sendData["CITY"] = array(
							"ID" => $fieldsCity['XML_ID'],
							'BITRIX_ID' => $fieldsCity['ID'],
							'REGION'=>$fieldsCity['PROPERTY_REGION_VALUE'], 
							'NETWORK_NAME' => $fieldsCity['PROPERTY_NETWORK_NAME_VALUE'], 
							'NETWORK_DATA_CODE' => $fieldsCity['PROPERTY_NETWORK_DATA_CODE_VALUE'], 
							'NETWORK_DATA_SITE_ID' => $fieldsCity['PROPERTY_NETWORK_DATA_SITE_ID_VALUE'], 
							'FILIAL1' => $fieldsCity['PROPERTY_FILIAL1_VALUE'], 
							'FILIAL2' => $fieldsCity['PROPERTY_FILIAL2_VALUE'],
							'FILIAL1_CODE' => $fieldsCity['PROPERTY_FILIAL1_CODE_VALUE'],
							'FILIAL2_CODE' => $fieldsCity['PROPERTY_FILIAL2_CODE_VALUE']
						);
					}
				}
			}
			break;
		case "BASKET":
			global $arGeoData;
			CModule::IncludeModule("iblock");
			CModule::IncludeModule("sale");
			$paysystem="";
			if(isset($arFields["paysystem"]) && is_numeric($arFields["paysystem"])){
				$db_ptype = CSalePaySystem::GetList($arOrder = Array("SORT"=>"ASC", "ID"=>"ASC"), Array("ACTIVE"=>"Y", "ID"=>$arFields["paysystem"]),false,array("nTopCount"=>1),array("ID","NAME"));
				if($ptype = $db_ptype->Fetch())
				{
				   $paysystem=$ptype["NAME"];
				}
			}
			if(!$paysystem){
				$db_ptype = CSalePaySystem::GetList($arOrder = Array("SORT"=>"ASC", "ID"=>"ASC"), Array("ACTIVE"=>"Y", "PERSON_TYPE_ID"=>1),false,array("nTopCount"=>1),array("ID","NAME"));
				if($ptype = $db_ptype->Fetch())
				{
				   $paysystem=$ptype["NAME"];
				}		
				else{$paysystem=$arFields;}		
			}
			
			$arBasketItems = array();
			CModule::IncludeModule('iblock');
			$dbBasketItems = CSaleBasket::GetList(
					array(
							"NAME" => "ASC",
							"ID" => "ASC"
						),
					array(
							"FUSER_ID" => CSaleBasket::GetBasketUserID(),
							"LID" => SITE_ID,
							"ORDER_ID" => "NULL",
							"CAN_BUY" => "Y",
							"!DELAY" => "Y"
						),
					false,
					false,
					array("ID", "PRODUCT_ID", "QUANTITY", "PRICE", "BASE_PRICE", "PROPS")
				);
			while ($arItems = $dbBasketItems->Fetch())
			{
				$arSelect = Array("ID", "NAME", "XML_ID");
				$arFilter = Array("ID"=>$arItems["PRODUCT_ID"]);
				$res = CIBlockElement::GetList(Array(), $arFilter, false, Array("nTopCount"=>1), $arSelect);
				if(!$arProd = $res->GetNext())$arProd=array();
				
				$gift=false;
				$db_res = CSaleBasket::GetPropsList(array(),array("BASKET_ID" => $arItems["ID"], "CODE" => "Gift"));
				if ($ar_res = $db_res->Fetch()){
					if($ar_res["VALUE"]=="true")					
						$gift=true;
				}

				$arBasketItems[] = array(
					"НоменклатураИД" => $arProd["XML_ID"],
					"Количество" => $arItems["QUANTITY"],
					"ЦенаПрайс" => round($arItems["BASE_PRICE"]),
					"ЭтоУслуга" => false,				// изменить на основе новых данных
					"НеУчаствуетВАкциях" => false,		// изменить на основе новых данных
					"ЭтоПодарок" => $gift
				);
			}			

			if(count($arBasketItems)>0)
				$sendData=array(
					"ДатаРасчетаАкций" => date("Y-m-d H:i:s"),
					"ИнтернетПлощадкаИД" => "10ed05aa-e8ce-45c6-a116-7eab2cc38220",
					"ЦеновойКустИД" => $arGeoData["CUR_CITY"]["PRICEBUSH_ID"],
					"Промокод" => $_SESSION["CATALOG_USER_COUPONS"],
					"ТипПлатежа" => $paysystem,
					"СоставКорзины" => $arBasketItems
				);			
			else
				$sendData=array();
			break;
		case "ORDER":
		case "PORTFOLIO_FORM":
		case "ORDER_MESSAGE":
		case "ORDER_CANCEL":
			$sendData=$arFields;
			break;
		default:
			break;
	}
	
	if(count($sendData)>0){
		AddMessage2Log($sendData, "send_data.".$dataType, 15);
		
		$opts = array('http' =>
			array(
				'method'  => 'POST',
				'header'  => 'Content-type: application/x-www-form-urlencoded',
				'content' => http_build_query($sendData)
			)
		);
		 
		$context  = stream_context_create($opts);
		$result = json_decode(file_get_contents($server, false, $context),TRUE);
		
		//обработка ответов
		switch($dataType){
			case "BASKET":
				global $arGeoData;
				if($result["status"]==0){
					if($result["errors"]=="null") $result["errors"]=array();
					elseif(!is_array($result["errors"]))$result["errors"]=array($result["errors"]);
					AddMessage2Log("Сервер вернул ошибку при отправке данных в шлюз: ".implode(".",$result["errors"]), "send_data.".$dataType, 15);
				}else{
					$arBasketItems = array();
					CModule::IncludeModule('iblock');
					$dbBasketItems = CSaleBasket::GetList(
							array(
									"NAME" => "ASC",
									"ID" => "ASC"
								),
							array(
									"FUSER_ID" => CSaleBasket::GetBasketUserID(),
									"LID" => SITE_ID,
									"ORDER_ID" => "NULL",
								),
							false,
							false,
							array("ID", "PRODUCT_ID", "QUANTITY", "PRICE")
						);
					while ($arItems = $dbBasketItems->Fetch())
					{
						$gift="false";
						$arSelect = Array("ID", "NAME", "XML_ID");
						$arFilter = Array("ID"=>$arItems["PRODUCT_ID"]);
						$res = CIBlockElement::GetList(Array(), $arFilter, false, Array("nTopCount"=>1), $arSelect);
						if(!$arProd = $res->GetNext())$arProd=array();
						
						$arItems["PROPS"]=array();
						$db_res = CSaleBasket::GetPropsList(array("SORT" => "ASC", "NAME" => "ASC"), array("BASKET_ID" => $arItems["ID"]));
						while ($ar_res = $db_res->Fetch())
						{
							if($ar_res["CODE"]!="Gift")
							   $arItems["PROPS"][]=$ar_res;
							elseif($ar_res["VALUE"]=="true")
								$gift="true";
						}
						
						$arBasketItems[$arProd["XML_ID"]."_gift_".$gift] = $arItems;
					}	
					
					if(isset($result["response"]["СоставЗаказа"]["НоменклатураИД"])){
						$result["response"]["СоставЗаказа"]=array(0=>$result["response"]["СоставЗаказа"]);
					}
					AddMessage2Log($result, "send_result.".$dataType, 15);
					
					$exists_id=array();
					AddMessage2Log($arBasketItems, "arBasketItems.".$dataType, 1);
					foreach($result["response"]["СоставЗаказа"] as $bskItem){
						if($bskItem["ЭтоПодарок"]===true)$bskItem["ЭтоПодарок"]="true";
						elseif($bskItem["ЭтоПодарок"]===false)$bskItem["ЭтоПодарок"]="false";
						//AddMessage2Log($bskItem, "1bskItem.".$dataType, 1);
						
						if(isset($arBasketItems[$bskItem["НоменклатураИД"]."_gift_".$bskItem["ЭтоПодарок"]])){
							$arFields = array(
								"QUANTITY" => $bskItem["Количество"],
								"BASE_PRICE" => $bskItem["ЦенаПрайс"],
								"PRICE" => $bskItem["ЦенаПродажи"],
								"DISCOUNT_PRICE" => $bskItem["ЦенаПрайс"]-$bskItem["ЦенаПродажи"],
								"PROPS" => $arBasketItems[$bskItem["НоменклатураИД"]."_gift_".$bskItem["ЭтоПодарок"]]["PROPS"],
								"CAN_BUY" => "Y",
								"DELAY" => "N",
								"NOTES" => $arGeoData["CUR_CITY"]["CITY_NAME"]
							);
							if($bskItem["ЭтоПодарок"]===true || $bskItem["ЭтоПодарок"]==="true"){
								$arFields["PROPS"][]=array("NAME" => "Подарок", "CODE" => "Gift", "VALUE" => "true", "SORT" => "1");
								$arFields["CUSTOM_PRICE"]="Y";
							}
							$_SESSION["basket_from_gateway"]=1;
							AddMessage2Log($arFields, "1arFields.".$dataType, 1);
							AddMessage2Log($bskItem["НоменклатураИД"]."_gift_".$bskItem["ЭтоПодарок"], "1.".$dataType, 1);

							CSaleBasket::Update($arBasketItems[$bskItem["НоменклатураИД"]."_gift_".$bskItem["ЭтоПодарок"]]["ID"], $arFields);
							$exists_id[]=$bskItem["НоменклатураИД"]."_gift_".$bskItem["ЭтоПодарок"];
						}else{
							$arSelect = Array("ID", "NAME", "XML_ID", "DETAIL_PAGE_URL");
							$arFilter = Array("XML_ID"=>$bskItem["НоменклатураИД"]);
							$res = CIBlockElement::GetList(Array(), $arFilter, false, Array("nTopCount"=>1), $arSelect);
							if(!$arProd = $res->GetNext())$arProd=array();
							
							$arFields = array(
								"LID" => SITE_ID,
								"PRODUCT_ID" => $arProd["ID"],
								"PRODUCT_XML_ID" => $bskItem["НоменклатураИД"],
								"DETAIL_PAGE_URL" => $arProd["DETAIL_PAGE_URL"],
								"NAME" => $arProd["NAME"],
								"QUANTITY" => $bskItem["Количество"],
								"BASE_PRICE" => $bskItem["ЦенаПрайс"],
								"PRICE" => $bskItem["ЦенаПродажи"],
								"DISCOUNT_PRICE" => $bskItem["ЦенаПрайс"]-$bskItem["ЦенаПродажи"],
								"CURRENCY" => "RUB",
								"PROPS" => array(),
								"CAN_BUY" => "Y",
								"DELAY" => "N",
								"NOTES" => $arGeoData["CUR_CITY"]["CITY_NAME"]
							);
							if($bskItem["ЭтоПодарок"]===true || $bskItem["ЭтоПодарок"]==="true"){
								$arFields["PROPS"][]=array("NAME" => "Подарок", "CODE" => "Gift", "VALUE" => "true", "SORT" => "1");
								$arFields["CUSTOM_PRICE"]="Y";
							}
							$_SESSION["basket_from_gateway"]=1;
							//AddMessage2Log($arFields, "3arFields.".$dataType, 1);
							CSaleBasket::Add($arFields);
							
						}							
					}
					$exists_id=array_unique($exists_id);
					foreach($exists_id as $eid)
						unset($arBasketItems[$eid]);
					foreach($arBasketItems as $bskItem){
						//AddMessage2Log($bskItem, "2bskItem.".$dataType, 1);
						
						$arFields = array(
							"PROPS" => $bskItem["PROPS"],
							"CAN_BUY" => "N",
							"DELAY" => "N"
						);
						
						$_SESSION["basket_from_gateway"]=1;
						//AddMessage2Log($arFields, "2arFields.".$dataType, 1);
						CSaleBasket::Update($bskItem["ID"], $arFields);
					}
				}
				
				break;
			default:
				if($result["OK"]==0){
					AddMessage2Log("Сервер вернул ошибку при отправке данных в шлюз: ".$result["ERROR"], "send_data.".$dataType, 15);
				}
				break;
		}
	}
}

Function ArrayMergeKeepKeys() {
      $arg_list = func_get_args();
      foreach((array)$arg_list as $arg){
          foreach((array)$arg as $K => $V){
              $arr[$K]=$V;
          }
      }
    return $arr;
}