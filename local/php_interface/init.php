<?
ini_set('session.gc_maxlifetime','604800');
ini_set('memory_limit', '2048M');
set_time_limit(60);

// redirect form dev host
// if ($_SERVER['HTTP_HOST'] == 'rusklimat.hp.webway.ru') {
	// LocalRedirect('http://www.rusklimat-omsk.ru'.$_SERVER['REQUEST_URI']);
// }

include($_SERVER["DOCUMENT_ROOT"].'/local/php_interface/functions.php');
include($_SERVER["DOCUMENT_ROOT"].'/local/php_interface/MyClass.php');
include_once($_SERVER["DOCUMENT_ROOT"].'/local/php_interface/include/form/validators/name.php');

global $APPLICATION;

// city
$CITY_COOKIE_NAME = 'CITY_ID_WEB';
$PREV_CITY_COOKIE_NAME = 'PREV_CITY_ID_WEB';
$CUR_CITY_ID = $APPLICATION->get_cookie($CITY_COOKIE_NAME);
$PREV_CITY_ID = $APPLICATION->get_cookie($PREV_CITY_COOKIE_NAME);
if($PREV_CITY_ID) $APPLICATION->set_cookie($PREV_CITY_COOKIE_NAME, "");

$arSort = ['PROPERTY_region'=>'ASC', 'NAME'=>'ASC'];
$arFilter = Array("IBLOCK_ID"=>15, "!PROPERTY_region"=>false/*, "PROPERTY_network_data_status"=>"active"*/);
// $arSelect = false;
$arSelect = ['ID', 'NAME', 'PROPERTY_region', 'PROPERTY_network_name', 'PROPERTY_network_data_code', 'PROPERTY_network_data_site_id', 'PROPERTY_filial1_code', 'PROPERTY_filial2_code', 'PROPERTY_ORG_Phone'];
$arGeoData = [];

$obCache = new CPHPCache();
if ($obCache->InitCache(86400, serialize($arFilter), "/")) {	// 86400
	$arGeoData = $obCache->GetVars();
}
elseif ($obCache->StartDataCache())
{
	
	if (CModule::IncludeModule('catalog')) {

		$dbPriceTypes = [];
		$dbPriceType = CCatalogGroup::GetList(
			array("SORT" => "ASC"),
			array()
		);
		while ($arPriceType = $dbPriceType->Fetch()) {
			$dbPriceTypes[$arPriceType['NAME']] = $arPriceType;
		}
	
	}
	
	if (CModule::IncludeModule("iblock")) {
		
		$res = CIBlockElement::GetList($arSort, $arFilter, false, false, $arSelect);

		if(defined("BX_COMP_MANAGED_CACHE")) {
			global $CACHE_MANAGER;
			$CACHE_MANAGER->StartTagCache("/iblock/geo");
		}
		// while ($obFields = $res->GetNextElement()) {
		while ($arFields = $res->Fetch()) {
			if(defined("BX_COMP_MANAGED_CACHE")) {
				$CACHE_MANAGER->RegisterTag("iblock_id_".$arFilter["IBLOCK_ID"]);
			}
			
			// $arFields = $obFields->GetFields();
			// $arFields['PROPERTY_VALUES'] = $obFields->GetProperties();
			
			$arGeoData['CITIES'][$arFields["PROPERTY_REGION_VALUE"]][$arFields["NAME"]] = $arFields["ID"];
			
			$cityArray = [
				'ID' => $arFields["ID"],
				'NAME' => $arFields["NAME"],
				'REGION' => $arFields["PROPERTY_REGION_VALUE"],
				'PRICEBUSH_ID' => $arFields["PROPERTY_NETWORK_DATA_SITE_ID_VALUE"],
				'CITY_NAME' => $arFields["PROPERTY_NETWORK_NAME_VALUE"],
				'CITY_CODE' => $arFields["PROPERTY_NETWORK_DATA_CODE_VALUE"],
				'CITY_CODE1' => $arFields["PROPERTY_FILIAL1_CODE_VALUE"],
				'CITY_CODE2' => $arFields["PROPERTY_FILIAL2_CODE_VALUE"],
				'PRICE_ID' => false,
				'PRICE_NODISCOUNT_ID' => false,
				'PHONE' => $arFields["PROPERTY_ORG_PHONE_VALUE"]
			];
		
			if (!empty($dbPriceTypes[$cityArray['PRICEBUSH_ID']])) {
				$cityArray['PRICE_ID'] = $dbPriceTypes[$cityArray['PRICEBUSH_ID']]['ID'];
			}
			if (!empty($dbPriceTypes[$cityArray['PRICEBUSH_ID'].'_NODISCOUNT'])) {
				$cityArray['PRICE_NODISCOUNT_ID'] = $dbPriceTypes[$cityArray['PRICEBUSH_ID'].'_NODISCOUNT']['ID'];
			}
			
			$arGeoData['CITIES_IDS'][$arFields["ID"]] = $cityArray;
			
			if ($cityArray["NAME"] == 'Москва') {
				$arGeoData['DEF_CITY'] = $cityArray;
			}
			/*p($cityArray);*/
			
		}
		
		if(defined("BX_COMP_MANAGED_CACHE")) {
			$CACHE_MANAGER->EndTagCache();
		}
		
	}
	$obCache->EndDataCache($arGeoData);
}

if (!empty($_REQUEST['CITY'])) {
	$CUR_CITY_CHANGE = intval($_REQUEST['CITY']);
	foreach($arGeoData['CITIES_IDS'] as $arFields) {
		if ($arFields["ID"] == $CUR_CITY_CHANGE) {
			if($CUR_CITY_ID != $CUR_CITY_CHANGE) $APPLICATION->set_cookie($PREV_CITY_COOKIE_NAME, $CUR_CITY_ID);
			$CUR_CITY_ID = $CUR_CITY_CHANGE;
			$arGeoData['CUR_CITY'] = $arFields;
			break;
		}
	}
	unset($CUR_CITY_CHANGE);
}
if (empty($arGeoData['CUR_CITY']) && !empty($CUR_CITY_ID)) {
	foreach($arGeoData['CITIES_IDS'] as $arFields) {
		if ($arFields["ID"] == $CUR_CITY_ID) {
			$arGeoData['CUR_CITY'] = $arFields;
			break;
		}
	}
}

if (empty($arGeoData['CUR_CITY'])) {
	// auto detect
	
	include(__DIR__.'/ip_data/ip_data.php');
	
	if (!empty($_SERVER['HTTP_CLIENT_IP'])) {
		$IP = $_SERVER['HTTP_CLIENT_IP'];
	} elseif (!empty($_SERVER['HTTP_X_FORWARDED_FOR'])) {
		$IP = $_SERVER['HTTP_X_FORWARDED_FOR'];
	} else {
		$IP = $_SERVER['REMOTE_ADDR'];
	}
	$IP_INT = ip2long($IP);
	$CUR_DATA = false;
	foreach($cidr_optim as $data) {
		if ($data[0] <= $IP_INT && $data[1] >= $IP_INT) {
			$CUR_DATA = $data;
			break;
		}
	}
	// if (!empty($CUR_DATA) && $CUR_DATA[3] == 'RU' && !empty($CUR_DATA[4])) {
	if (!empty($CUR_DATA)) {
		$CUR_DATA[4] = $cities[$CUR_DATA[4]];
		
		foreach($arGeoData['CITIES_IDS'] as $arFields) {
			// if ($arFields["REGION"] == $CUR_DATA[4][2] && $arFields["NAME"] == $CUR_DATA[4][1]) {
			if ($arFields["NAME"] == $CUR_DATA[4][1] || $arFields["NAME"] == $CUR_DATA[4][1]." г") {
				$arFields['AUTO_DETECTED'] = true;
				$arFields['CITY_DATA'] = $CUR_DATA;
				$arGeoData['CUR_CITY'] = $arFields;
				break;
			}
		}
	}
	
	unset($cidr_optim);
	unset($cities);
	
	if (empty($arGeoData['CUR_CITY'])) {
		$arGeoData['CUR_CITY'] = $arGeoData['DEF_CITY'];
	}
}
/*$arGeoData['CUR_CITY']['PHONE'] = '';
if (!empty($arGeoData['CUR_CITY']['CITY_CODE']) && CModule::IncludeModule("iblock")) {
	$arSort = [];
	$arFilter = ['IBLOCK_ID' => 20, 'CODE'=>$arGeoData['CUR_CITY']['CITY_CODE'], '!=PROPERTY_PHONE'=>false];
	$arLimit = ['nTopCount'=>1];
	$arSelect = ['ID', 'PROPERTY_PHONE'];
	$res = CIBlockElement::GetList($arSort, $arFilter, false, $arLimit, $arSelect);
	if ($arFields = $res->Fetch()) {
		$arGeoData['CUR_CITY']['PHONE'] = $arFields['PROPERTY_PHONE_VALUE'];
	}
}*/
if (empty($arGeoData['CUR_CITY']['PHONE'])) {
	$arGeoData['CUR_CITY']['PHONE'] = file_get_contents($_SERVER['DOCUMENT_ROOT'].'/local/templates/rusklimat/include/def_phone.php');
}
$arGeoData['CUR_CITY']['PHONE_NUM'] = preg_replace('/[^\d]+/ui', '', $arGeoData['CUR_CITY']['PHONE']);
if($arGeoData['CUR_CITY']['PHONE_NUM']{0}=="8"){
	$arGeoData['CUR_CITY']['PHONE_NUM']="+7".substr($arGeoData['CUR_CITY']['PHONE_NUM'],1,strlen($arGeoData['CUR_CITY']['PHONE_NUM'])-1);
}

$arGeoData['PREV_CITY_ID'] = $PREV_CITY_ID;

$APPLICATION->set_cookie($CITY_COOKIE_NAME, $arGeoData['CUR_CITY']['ID']);
if (!empty($_REQUEST['CITY'])) {
	ChangeBasketItems();
	LocalRedirect(nfGetCurPageParam('', ['CITY']));
}

if ($_SERVER['SCRIPT_NAME'] != '/geo.php') {
	unset($arGeoData['CITIES_IDS']);
	unset($arGeoData['DEF_CITY']);
}

// p($arGeoData);
// p($arGeoData['CUR_CITY']);
