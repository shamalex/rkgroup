<?
// Листинг файла /bitrix/php_interface/include/form/validators/val_num_ex.php

class CFormCustomValidatorNumberEx
{
  function GetDescription()
  {
    return array(
      "NAME"            => "name",                                   // идентификатор
      "DESCRIPTION"     => "Имя",                                 	 // наименование
      "TYPES"           => array("text"),                            // типы полей
      "SETTINGS"        => array("CFormCustomValidatorNumberEx", "GetSettings"), // метод, возвращающий массив настроек
      "CONVERT_TO_DB"   => array("CFormCustomValidatorNumberEx", "ToDB"),        // метод, конвертирующий массив настроек в строку
      "CONVERT_FROM_DB" => array("CFormCustomValidatorNumberEx", "FromDB"),      // метод, конвертирующий строку настроек в массив
      "HANDLER"         => array("CFormCustomValidatorNumberEx", "DoValidate")   // валидатор
    );
  }

  function GetSettings()
  {
    return array(
      "MIN_STR_LEN" => array(
        "TITLE"   => "Минимальная длина строки",
        "TYPE"    => "TEXT",
        "DEFAULT" => "0",
      ),
    );
  }

  function ToDB($arParams)
  {
    // проверка переданных параметров
    $arParams["MIN_STR_LEN"] = intval($arParams["MIN_STR_LEN"]);
    
    // возвращаем сериализованную строку
    return serialize($arParams);
  }

  function FromDB($strParams)
  {
    // никаких преобразований не требуется, просто вернем десериализованный массив
    return unserialize($strParams);
  }
    
  function DoValidate($arParams, $arQuestion, $arAnswers, $arValues)
  {
    global $APPLICATION;
    
    foreach ($arValues as $value)
    {
		// пустые значения пропускаем
		if (strlen($value) <= 0) continue;
      
		// проверим длину строки
		if (strlen($value) < $arParams["MIN_STR_LEN"])
		{
		// вернем ошибку
			$APPLICATION->ThrowException("#FIELD_NAME#: должно быть не менее ".$arParams["MIN_STR_LEN"]." символов");
			return false;
		}
      
		// проверим разрешенные символы
		if (!preg_match('/([А-Яа-яA-Za-z\- ])+/', $value))
		{
			// вернем ошибку
			$APPLICATION->ThrowException("#FIELD_NAME#: некорректное значение");
			return false;
		}
    }
    
    // все значения прошли валидацию, вернем true
    return true;
  }
}

// установим метод CFormCustomValidatorNumberEx в качестве обработчика события
AddEventHandler("form", "onFormValidatorBuildList", array("CFormCustomValidatorNumberEx", "GetDescription"));
?>