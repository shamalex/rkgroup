<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Результаты поиска");
$_REQUEST['q'] = preg_replace('/[\(\)]+/ui', '', $_REQUEST['q']);

$APPLICATION->IncludeComponent(
	"bitrix:search.page", 
	"products", 
	array(
		"RESTART" => "Y",
		"NO_WORD_LOGIC" => "Y",
		"USE_LANGUAGE_GUESS" => "N",
		"CHECK_DATES" => "Y",
		"arrFILTER" => array(
			0 => "iblock_catalog",
		),
		"arrFILTER_iblock_catalog" => array(
			0 => "8",
		),
		"arrFILTER_iblock_news" => array(
			0 => "all",
		),
		"arrFILTER_iblock_services" => array(
			0 => "all",
		),
		"arrFILTER_main" => "",
		"arrWHERE" => array(
			0 => "iblock_catalog",
		),
		"USE_SUGGEST" => "Y",
		"USE_TITLE_RANK" => "Y",
		"DEFAULT_SORT" => "date",
		"FILTER_NAME" => "",
		"SHOW_WHERE" => "Y",
		"SHOW_WHEN" => "N",
		"PAGE_RESULT_COUNT" => "50",
		"DISPLAY_TOP_PAGER" => "N",
		"DISPLAY_BOTTOM_PAGER" => "Y",
		"PAGER_TITLE" => "",
		"PAGER_SHOW_ALWAYS" => "N",
		"PAGER_TEMPLATE" => "",
		"COMPONENT_TEMPLATE" => "products",
		"AJAX_MODE" => "N",
		"AJAX_OPTION_JUMP" => "N",
		"AJAX_OPTION_STYLE" => "Y",
		"AJAX_OPTION_HISTORY" => "N",
		"AJAX_OPTION_ADDITIONAL" => "undefined",
		"CACHE_TYPE" => "A",
		"CACHE_TIME" => "3600",
		"SHOW_ITEM_TAGS" => "Y",
		"TAGS_INHERIT" => "Y",
		"SHOW_ITEM_DATE_CHANGE" => "Y",
		"SHOW_ORDER_BY" => "Y",
		"SHOW_TAGS_CLOUD" => "N",
		"SHOW_RATING" => "",
		"RATING_TYPE" => "",
		"PATH_TO_USER_PROFILE" => ""
	),
	false
); ?><?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>