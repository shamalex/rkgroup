<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Результаты поиска");
global $arGeoData;
/* get from catalog/index.php */
$QUANITY_PROP = preg_replace('/\-+/ui', '_', $arGeoData['CUR_CITY']['CITY_CODE1']);
$QUANITY_PROP = strtoupper('COUNT_'.$QUANITY_PROP);
$QUANITY_PROP2 = preg_replace('/\-+/ui', '_', $arGeoData['CUR_CITY']['CITY_CODE2']);
$QUANITY_PROP2 = strtoupper('COUNT_'.$QUANITY_PROP2);


$_REQUEST['q'] = preg_replace('/[\(\)]+/ui', '', $_REQUEST['q']);

$QUERY = trim($_REQUEST['q']); 

$APPLICATION->IncludeComponent(
	"webway:catalog.search",
	"",
	array(
		"NAME" => "q",
		"ITEMS" => 8,
		"SRCH" => $_REQUEST["q"],
		"PAGE" => intval($_REQUEST["p"]),
		"COVERAGE" => $_REQUEST["where"],
		"SUB_PROPERTYES" => array("sm_picture1", $QUANITY_PROP, $QUANITY_PROP2),
		"ORDERBY" => array("property_".$QUANITY_PROP2 => "desc", "property_".$QUANITY_PROP => "desc", "PRICE"),
		"QUANITY" => array(
			"local" => $QUANITY_PROP2,
			"regional" => $QUANITY_PROP,
		),
		"PRICE" => array(
			"now" => $arGeoData["CUR_CITY"]["PRICE_ID"], 
			"old" => $arGeoData["CUR_CITY"]["PRICE_NODISCOUNT_ID"]
		),
	),
	$component, array("HIDE_ICONS" => "N")
);
?><?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>